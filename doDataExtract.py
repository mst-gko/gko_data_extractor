# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys
from collections import OrderedDict

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import *
from qgis.gui import *

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()
import pyodbc




__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .DataExtractorFunctions.dataextractor_opstart_figurgenerator import *
from .DataExtractorFunctions.backend_functions import *
from .DataExtractorFunctions.dataextractor_kemi_figurgenerator import *
from .DataExtractorFunctions.dataextractor_osd_revision import *
from .DataExtractorFunctions.dataextractor_kemi import *
from .DataExtractorFunctions.dataextractor_pejlinger import *
from .DataExtractorFunctions.dataextractor_grukos import *
from .DataExtractorFunctions.dataextractor_andrefunktioner import *
from .DataExtractorFunctions.dataextractor_gkoplotter import *
from .DataExtractorFunctions.dataextractor_sårbarhed import *
from .DataExtractorFunctions.dataextractor_FOHMprofil import plot_data
from .DataExtractorFunctions.teser2 import YourDialog
from .DataExtractorFunctions.dataextractor_tolkningsvurdering import *
from .install_packages.check_dependencies import check
from .DataExtractorFunctions.dataextractor_vvbeskrivelser import *
try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass

Ui_Dialog = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'gko_data_extractor_dialog_base.ui'))[0]

class Dialog(QDialog, Ui_Dialog):

 inputItems = {}  # {name1 : [layer1, [field_src,field_dsn,Active?], [field_src,field_dsn,Active?], ...] , name2 : [layer2, ...] }
 outputItems = {}  # {name1 : [layer1, [field_src,field_dsn,Active?], [field_src,field_dsn,Active?], ...] , name2 : [layer2, ...] }

 def __init__(self, iface):
  QDialog.__init__(self)
  self.iface = iface

  self.setupUi(self)
  self.tildato.setDateTime(QDateTime.currentDateTime())
  self.listWidget.setSelectionMode(QListWidget.SingleSelection)
  self.threadpool = QThreadPool()

  self.setWindowFlag(Qt.WindowMinimizeButtonHint, True)

  self.btn_selectoutput.clicked.connect(self.select_directory)
  self.lokalefilervlgsti.clicked.connect(self.select_directory2)
  #self.refreshbutton.clicked.connect(self.updaterListe)
  #self.comboBox.layerChanged.connect(self.selected_layer_changed)
  self.comboBox.setFilters(QgsMapLayerProxyModel.PolygonLayer)
  #self.selected_layer_changed(self.layer_combo.currentLayer())

  self.cancelbutton.clicked.connect(self.stopall)
  self.btn_selectoutput2.clicked.connect(self.select_directory3)
  self.pushButton_fifth_2.clicked.connect(self.select_directory4)
  self.pushButton_mappe_2.clicked.connect(self.select_directory7)
  self.comboBox.currentIndexChanged.connect(self.updateFieldsList)

  self.comboBox_lerlag.currentIndexChanged.connect(self.field_select)
  self.comboBox_magpoly.currentIndexChanged.connect(self.field_select2)
  self.comboBox_trin2_vurdering.currentIndexChanged.connect(self.field_select3)
  self.comboBox_trin2_vandtype.currentIndexChanged.connect(self.field_select4)
  self.comboBox_redocgrnse.currentIndexChanged.connect(self.field_select5)
  self.comboBox_watertype.currentIndexChanged.connect(self.field_select6)


  self.pushButton_Andet.clicked.connect(lambda: outFile_andet(self=self))
  self.pushButton_Pejlinger.clicked.connect(lambda: outFile_Pejlinger(self=self))
  self.pushButton_Kemi.clicked.connect(lambda: outFile_Kemi(self=self))
  self.pushButton_GRUKOS.clicked.connect(lambda: outFile_GRUKOS(self=self))


  self.pushButton_first.clicked.connect(lambda: opstart_first(self=self))
  self.pushButton_second.clicked.connect(lambda: opstart_second(self=self))
  self.pushButton_third.clicked.connect(lambda: opstart_third(self=self))



  self.pushButton_fourth.clicked.connect(lambda: kemi_one(self=self))
  self.pushButton_fifth.clicked.connect(lambda: kemi_two(self=self))
  self.pushButton_sixth.clicked.connect(lambda: kemi_three(self=self))


  #self.pushButton_magudbre.clicked.connect(lambda: osd_data0(self=self))
 # self.grundvandsdannelseknap.clicked.connect(lambda: osd_data1(self=self))
 # self.dklagstykkelseknap.clicked.connect(lambda: osd_data2(self=self))
 # self.Vandudvekslingknap.clicked.connect(lambda: osd_data3(self=self))
 # self.Indsatsomrdeknap.clicked.connect(lambda: osd_data4(self=self))
 # self.testerknap.clicked.connect(lambda: osd_two(self=self))
 # self.OSDDone.clicked.connect(lambda: osd_final(self=self))
 # self.OSDTable.itemChanged.connect(self.changedcell)

  self.tabWidget.currentChanged.connect(self.changedtab)

  self.pushButton_GKOPLOTTER.clicked.connect(lambda: PlottingTool_Kemi(self=self))
  self.pushButton_GKOPLOTTER_Hydro.clicked.connect(lambda: PlottingTool_Hydro(self=self))
  self.pushButton_GKOPLOTTER_Indvinding.clicked.connect(lambda: PlottingTool_Indvinding(self=self))

  self.toolButton.clicked.connect(self.select_directory5)

  self.pushButton_srbarhed_trin1.clicked.connect(lambda: trin1_sårbarhed(self=self))
  self.pushButton_trin2.clicked.connect(lambda: trin2_sårbarhed(self=self))
  self.pushButton_KSredox.clicked.connect(lambda: redox_ks(self=self))

  self.refresh2.clicked.connect(self.updaterListe2)


  self.pushButtonnn.clicked.connect(self.opennewwindow)

  self.pushButton_vvbeskrivelser.clicked.connect(lambda: vandværksbeskrivelser(self=self))
  self.groupBox_geologiskeprofiler.hide()
  self.label_39.hide()
  self.checkBox_7.toggled.connect(self.changetext)
  self.comboBox_vv_profiler.currentIndexChanged.connect(self.field_select7)
  self.comboBox_vv_profiler.setFilters(QgsMapLayerProxyModel.LineLayer)

  self.comboBox_vv_iol.setFilters(QgsMapLayerProxyModel.PolygonLayer)
  self.comboBox_vv_bnbo.setFilters(QgsMapLayerProxyModel.PolygonLayer)
  self.comboBox_vv_boringer.setFilters(QgsMapLayerProxyModel.PointLayer)
  self.comboBox_vv_nitrat.setFilters(QgsMapLayerProxyModel.PolygonLayer)
  self.comboBox_vv_gvd.setFilters(QgsMapLayerProxyModel.PointLayer)

  self.comboBox_vv_nfi.setFilters(QgsMapLayerProxyModel.PolygonLayer)
  self.comboBox_vv_io.setFilters(QgsMapLayerProxyModel.PolygonLayer)
  self.comboBox_vv_gvdpt.setFilters(QgsMapLayerProxyModel.PointLayer)
  self.comboBox_vv_pt.setFilters(QgsMapLayerProxyModel.PointLayer)
  self.comboBox_vv_ler.setFilters(QgsMapLayerProxyModel.PointLayer)



  self.pushButton_FOHM.clicked.connect(self.run_fohm_profile_plotter)
  self.groupBox_kriterierfohm.hide()
  mapCanvas = self.iface.mapCanvas()


  self.lineEdit_8.setVisible(False)
  self.lineEdit_9.setVisible(False)
  self.pushButton_specifikfirebird.setVisible(False)
  self.pushButton_specifikboring.setVisible(False)
  self.pushButton_specifikfirebird.clicked.connect(lambda: indhent_geofysik(self=self))
  self.pushButton_specifikboring.clicked.connect(lambda: indhent_boringer(self=self))
  self.foretag_tolkningsvurdering.clicked.connect(lambda: foretag_tolkningsvurderingen(self=self))
  self.comboBox_tolkningsvurdering.currentIndexChanged.connect(lambda: update_settings(self=self))

  self.comboBox_tolkningsvurdering.addItem("0.1: Fynmodellen - enkelt lag")
  self.comboBox_tolkningsvurdering.addItem("0.2: Fynmodellen - alle lag (KRÆVENDE)")
  self.comboBox_tolkningsvurdering.addItem("1.1: FOHM - enkelt lag")
  self.comboBox_tolkningsvurdering.addItem("1.2: FOHM - alle lag (KRÆVENDE)")
  self.comboBox_tolkningsvurdering.addItem("2.1: Benyt specifikke flader indlæst i QGIS")
  self.comboBox_tolkningsvurdering.addItem("2.2: Benyt alle flader fra mappe (KRÆVENDE)")
  self.pushButton_mappe.clicked.connect(self.openflademappe)
  self.lineEdit_10.textChanged.connect(self.lagtypetabel)
  excluded = []
  for i in QgsProject.instance().mapLayers():
      layer = QgsProject.instance().mapLayer(i)
      if ( layer.type() == layer.VectorLayer ):
          excluded.append(layer)

  self.layer_combo.setExceptedLayerList(excluded)
  self.layer_combo_2.setExceptedLayerList(excluded)


  self.inputItems = {}
  for i in range(mapCanvas.layerCount()):
   layer = mapCanvas.layer(i)
   if ( layer.type() == layer.VectorLayer ) and ( layer.geometryType() == QgsWkbTypes.PolygonGeometry ):
    # read point layers
    provider = layer.dataProvider()
    fields = provider.fields()
    theItem = [layer]
    for j in fields:
     theItem += [[str(j.name()), str(j.name()), False]]
    self.inputItems[str(layer.name())] = theItem
    self.comboBox.addItem(layer.name())
    self.inputItemsPath = provider.dataSourceUri()
    feats = layer.getFeatures()
    for kk, feature in enumerate(layer.getFeatures()):
        if kk == 0:
            geom = feature.geometry()
            geomSingleType = QgsWkbTypes.isSingleType(geom.wkbType())
   if layer.type() == layer.VectorLayer and layer.geometryType() == QgsWkbTypes.PolygonGeometry:
          self.comboBox_magpoly.addItem(layer.name(), layer)
        #  field_names = [field.name() for field in layer.fields()]
        #  for x in field_names:
         #   self.comboBox_magpoly_id.addItem(x)

   if layer.type() == layer.VectorLayer and layer.geometryType() == 0:
          self.comboBox_lerlag.addItem(layer.name(), layer)

   if layer.type() == layer.VectorLayer and layer.geometryType() == 0:
          self.comboBox_trin2_vurdering.addItem(layer.name(), layer)

   if layer.type() == layer.VectorLayer and layer.geometryType() == 0:
          self.comboBox_trin2_vandtype.addItem(layer.name(), layer)

   if layer.type() == layer.VectorLayer and layer.wkbType() == 2:
          self.comboBox_3.addItem(layer.name(), layer)

   if layer.type() == layer.VectorLayer and layer.geometryType() == 0:
          self.comboBox_watertype.addItem(layer.name(), layer)

   if layer.type() == layer.RasterLayer or layer.type() == layer.VectorLayer and layer.geometryType() == 0:
       self.comboBox_redocgrnse.addItem(layer.name(), layer)

  self.updateFieldsList()

 def opennewwindow(self):
     self.dialog_instance = YourDialog()
     self.iface.addDockWidget(Qt.RightDockWidgetArea, self.dialog_instance)

   #  self.dialog_instance.exec_()
 def changedtab(self):
     current_index = self.tabWidget.currentIndex()
     if self.tabWidget.tabText(current_index) == 'Sårbarhed' or self.tabWidget.tabText(current_index) == 'Vandværksbeskrivelser' or self.tabWidget.tabText(current_index) == 'FOHM-Profil':
         self.label_2.setEnabled(False)
         self.comboBox.setEnabled(False)
         self.cancelbutton.setEnabled(False)
     else:
         self.label_2.setEnabled(True)
         self.comboBox.setEnabled(True)
         self.cancelbutton.setEnabled(True)

        
 def field_select(self):
     try:
         self.comboBox_lerlag_id.clear()
         self.repaint()
         selectedLayerText = self.comboBox_lerlag.currentText()
         selectedLayer = QgsProject.instance().mapLayersByName(str(selectedLayerText))[0]
         fields = [field.name() for field in selectedLayer.fields()]
         self.comboBox_lerlag_id.addItems(fields)
         self.repaint()

     except:
         pass

 def field_select2(self):
     try:
         self.comboBox_magpoly_id.clear()
         self.repaint()
         selectedLayerText = self.comboBox_magpoly.currentText()
         selectedLayer = QgsProject.instance().mapLayersByName(str(selectedLayerText))[0]
         fields = [field.name() for field in selectedLayer.fields()]
         self.comboBox_magpoly_id.addItems(fields)
         self.repaint()

     except:
         pass

 def changetext(self):
     if self.checkBox_7.isChecked():
         self.label_38.setText("Vælg mappe hvor profilerne skal gemmes:")
         self.label_39.hide()
     else:
         self.label_38.setText("Vælg mappe hvor profilerne er:")
         self.label_39.show()


 def field_select3(self):
     try:
         self.comboBox_trin2_vurdering_id.clear()
         self.repaint()
         selectedLayerText = self.comboBox_trin2_vurdering.currentText()
         selectedLayer = QgsProject.instance().mapLayersByName(str(selectedLayerText))[0]
         fields = [field.name() for field in selectedLayer.fields()]
         self.comboBox_trin2_vurdering_id.addItems(fields)
         self.repaint()

     except:
         pass

 def field_select4(self):
     try:
         self.comboBox_trin2_vandtype_id.clear()
         self.comboBox_trin2_vandtype_wt.clear()
         selectedLayerText = self.comboBox_trin2_vandtype.currentText()
         selectedLayer = QgsProject.instance().mapLayersByName(str(selectedLayerText))[0]
         fields = [field.name() for field in selectedLayer.fields()]
         self.comboBox_trin2_vandtype_id.addItems(fields)
         self.comboBox_trin2_vandtype_wt.addItems(fields)
         self.repaint()

     except:
         pass

 def field_select5(self):
     try:
         self.comboBox_redoxkolonne.clear()
         selectedLayerText = self.comboBox_redocgrnse.currentText()
         selectedLayer = QgsProject.instance().mapLayersByName(str(selectedLayerText))[0]
         fields = [field.name() for field in selectedLayer.fields()]
         self.comboBox_redoxkolonne.addItems(fields)
         #else:
             #self.comboBox_redoxkolonne.addItems(["RASTER"])
         self.repaint()
     except:
         pass

 def field_select6(self):
     try:
         self.comboBox_watertypecolumn.clear()
         self.comboBox_filtertopkolonne.clear()
         selectedLayerText = self.comboBox_watertype.currentText()
         selectedLayer = QgsProject.instance().mapLayersByName(str(selectedLayerText))[0]
         fields = [field.name() for field in selectedLayer.fields()]
         self.comboBox_watertypecolumn.addItems(fields)
         self.comboBox_filtertopkolonne.addItems(fields)
         self.repaint()

     except:
         pass

 def field_select7(self):
     try:
         self.comboBox_profillinjeID.clear()
         self.repaint()
         selectedLayerText = self.comboBox_vv_profiler.currentText()
         selectedLayer = QgsProject.instance().mapLayersByName(str(selectedLayerText))[0]
         fields = [field.name() for field in selectedLayer.fields()]
         self.comboBox_profillinjeID.addItems(fields)
         self.repaint()

     except:
         pass

 def updaterListe2(self):
  self.comboBox_trin2_vurdering.clear()
  #self.comboBox_trin2_vurdering_id.clear()
  self.comboBox_trin2_vandtype.clear()
  #self.comboBox_trin2_vandtype_id.clear()
  #self.comboBox_trin2_vandtype_wt.clear()
  mapCanvas = self.iface.mapCanvas()
  for i in range(mapCanvas.layerCount()):
      layer = mapCanvas.layer(i)
      if (layer.type() == layer.VectorLayer) and (layer.geometryType() == 0):
          self.comboBox_trin2_vurdering.addItem(layer.name(), layer)
          self.comboBox_trin2_vandtype.addItem(layer.name(), layer)
  self.repaint()

 def updateFieldsList(self):
      #self.comboBox.clear()
      if not self.comboBox.count(): return
      i = self.comboBox.currentText()
      self.repaint()



 def updaterListe(self):
     self.comboBox.clear()
     mapCanvas = self.iface.mapCanvas()
     self.inputItems = {}
     for i in range(mapCanvas.layerCount()):
         layer = mapCanvas.layer(i)
         if (layer.type() == layer.VectorLayer) and (layer.geometryType() == QgsWkbTypes.PolygonGeometry):
             # read point layers
             provider = layer.dataProvider()
             fields = provider.fields()
             theItem = [layer]
             for j in fields:
                 theItem += [[str(j.name()), str(j.name()), False]]
             self.inputItems[str(layer.name())] = theItem
             self.comboBox.addItem(layer.name())
             self.inputItemsPath = provider.dataSourceUri()
             feats = layer.getFeatures()
             for kk, feature in enumerate(layer.getFeatures()):
                 if kk == 0:
                     geom = feature.geometry()
                     geomSingleType = QgsWkbTypes.isSingleType(geom.wkbType())
     self.updateFieldsList()

 def stopall(self):
     QgsTaskManager().cancelAll()
     tasks = QgsApplication.taskManager().activeTasks()
     for task in tasks:
         task.cancel()

 def select_directory(self):
     try:
        plugin_path = QFileDialog.getExistingDirectory(
            self, caption='Vælg sti hvor figurer skal gemmes')
        self.outputdirectory.setText(plugin_path)
     except:
         pass

 def select_directory2(self):
     try:
        plugin_path = QFileDialog.getExistingDirectory(
            self, caption='Vælg sti hvor filer skal gemmes')
        self.lokalefilersti.setText(plugin_path)
     except:
         pass

 def select_directory3(self):
     try:
        plugin_path = QFileDialog.getExistingDirectory(
            self, caption='Vælg sti hvor jupiter_extract skal gemmes')
        self.outputdirectory_2.setText(plugin_path)
     except:
         pass

 def select_directory7(self):
     try:
        plugin_path = QFileDialog.getExistingDirectory(
            self, caption='Vælg sti hvor dine profiler er (PNG)')
        self.lineEdit_vv_geologiskeprofiler.setText(plugin_path)
     except:
         pass

 def openflademappe(self):
     try:
        plugin_path = QFileDialog.getExistingDirectory(
            self, caption='Vælg sti hvor fladerne er')
        self.lineEdit_10.setText(plugin_path)
     except:
         pass

 def run_fohm_profile_plotter(self):
        self.pushButton_FOHM.setText("Tegn tværsnit på QGIS kort")
        self.pushButton_FOHM.setEnabled(False)
        self.repaint()
        if self.checkBox_16.isChecked() and len(self.comboBox_3.currentText()) > 0:
            crs_dest = QgsCoordinateReferenceSystem(25832)
            crs_input = qgis.utils.iface.mapCanvas().mapSettings().destinationCrs()
            transform = QgsCoordinateTransform(crs_input, crs_dest, QgsProject.instance())
            lay = QgsProject.instance().mapLayersByName('{}'.format(self.comboBox_3.currentText()))[0]

            vertices = processing.run("native:extractspecificvertices",
                           {'INPUT': lay,
                            'VERTICES': '0, -1', 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

            for idx, feature in enumerate(vertices.getFeatures()):
                if idx == 0:
                    a = feature.geometry().asPoint()
                else:
                    b=feature.geometry().asPoint()

            startPoint = a
            endPoint  = b
         #  startPoint_transformed = transform.transform(startPoint)
          #  endPoint_transformed = transform.transform(endPoint)

            if self.comboBox_4.currentText() == 'FOHM - Jylland':
                område = 'jylland'
            elif self.comboBox_4.currentText() == 'FOHM - Fyn':
                område = 'fyn'
            else:
                område = 'sjaelland'

            points = [startPoint, endPoint]
            plot_data(dialog=self, line_points=points, område=område, number_of_points=self.spinBox_punkterlangslinje.value())
        else:
            self.rect_tool = ScriptLinetool(self.iface, dialog=self, tag="FOHM")
            self.rect_tool.connectTool()
            self.iface.mapCanvas().setMapTool(self.rect_tool.tool)

 def lagtypetabel(self):

     print(self.lineEdit_10.text())
     rows = []
     for file in os.listdir("{}".format(self.lineEdit_10.text())):
         if file.endswith(".asc") or file.endswith(".grd") or file.endswith(".tif"):
             if "terrain" in str(file).lower() or "topo" in str(file).lower():
                 pass
             else:
                rows.append(file)
     rows.sort()
     rækker = len(rows)
     self.tableWidget_magasin.setRowCount(rækker)
     self.tableWidget_magasin.setColumnCount(1)

     self.tableWidget_magasin.setVerticalHeaderLabels(rows)
     self.tableWidget_magasin.setHorizontalHeaderLabels(["Lagtype"])
     for i, row in enumerate(rows):
        comboBox = QtWidgets.QComboBox()
        comboBox.addItem("Vandstandsende lag")
        comboBox.addItem("Vandførende lag")
        layer_id = int(re.search(r"\d+", row).group())
        if row[:3].isnumeric():
            vandfoerende = [120, 140, 200, 400, 1200, 1400, 2100, 2300, 5200, 5400, 5600, 5800, 6000, 6200, 6400, 6600, 6800,
                    7000, 7200, 7400, 7600, 7800, 8500, 9000]
            if layer_id in vandfoerende:
                comboBox.setCurrentIndex(1)
            else:
                comboBox.setCurrentIndex(0)
        else:
            if (i % 2) == 0:
                comboBox.setCurrentIndex(1)
            else:
                comboBox.setCurrentIndex(0)

        self.tableWidget_magasin.setCellWidget(i, 0, comboBox)

     self.tableWidget_magasin.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
     self.tableWidget_magasin.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)



 def select_directory4(self):
        if True:
             self.pushButton_fifth_2.setText("Kører")
             self.pushButton_fifth_2.setEnabled(False)
             self.repaint()
        try:
            plugin_path,_filter = QFileDialog.getOpenFileName(self, caption='Vælg excel-arket med udtræk (jupiter_extract)')
            self.lineEdit.setText(str(plugin_path))
            xls = pd.ExcelFile("{}".format(self.lineEdit.text()))
            df = pd.read_excel(xls, 'Arbejdsdata')
            rows = df['magasin'].unique().tolist()
            rows = [str(item).replace('nan', "Ukendt magasin") for item in rows]

            for x in list(df.columns.values):
                self.x_combo.addItem(x)

            def custom_sort(item):
                # Use regular expression to extract the prefix and the numeric part
                match = re.match(r'([A-Z]+)(\d+)', item)
                if match:
                    prefix, number = match.groups()
                    # Create a tuple to use as the sorting key
                    return (int(number), prefix)
                else:
                    # Return a default value for items that don't match the expected format
                    return (float('inf'), '')

            rows.sort(key=custom_sort)
            rækker = len(rows)
            self.tableWidget.setRowCount(rækker)
            self.tableWidget.setVerticalHeaderLabels(rows)

            self.point_types = OrderedDict([
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/circle.svg')), self.tr('Cirkel')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/square.svg')), self.tr('Firkant')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/diamond.svg')), self.tr('Diamant')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/cross.svg')), self.tr('Kryds')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/triangle.svg')), self.tr('Trekant')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/penta.svg')), self.tr('Femkant')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/star.svg')), self.tr('Stjerne')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/filledarrowhead.svg')), self.tr('Pilehoved')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/semicircle.svg')), self.tr('Halvcirkel')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/rectangle.svg')), self.tr('Rektangel')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/righthalftriangle.svg')), self.tr('Højre-halv trekant')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/lefthalftriangle.svg')), self.tr('Venstre-halv trekant')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/quartercircle.svg')), self.tr('Kvartcirkel')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/diagonalhalfspace.svg')), self.tr('Diagonal trekant')),
                (QIcon(os.path.join(
                    os.path.dirname(__file__), 'DataExtractorFunctions/octagon.svg')), self.tr('Oktagon')),
            ])

            for i in range(self.tableWidget.rowCount()):
                comboBox = QtWidgets.QComboBox()
                comboBox.clear()
                for k, v in self.point_types.items():
                    comboBox.addItem(k, v)

                self.tableWidget.setCellWidget(i, 1, comboBox)




            self.pushButton_fifth_2.setText("Færdig ✓")
            self.repaint()
        except:
            pass

 def select_directory5(self):
        try:
            plugin_path,_filter = QFileDialog.getOpenFileName(self, caption='Vælg excel-arket med kemiudtræk (jupiter_extract)')
            self.lineEdit_7.setText(str(plugin_path))
            self.repaint()
        except:
            pass

 def changedcell(self, item):
    def write_qtable_to_df(table):
        col_count = table.columnCount()
        row_count = table.rowCount()
        headers = [str(table.horizontalHeaderItem(i).text()) for i in range(col_count)]

        df_list = []
        for row in range(row_count):
            df_list2 = []
            for col in range(col_count):
                table_item = table.item(row, col)
                df_list2.append('' if table_item is None else str(table_item.text()))
            df_list.append(df_list2)

        df = pd.DataFrame(df_list, columns=headers)

        return df

    osd = write_qtable_to_df(self.OSDTable)

    if item.column() == 2:
        for row in range(self.OSDTable.rowCount()):
            try:
                procentuelvægt = (int(osd.iloc[row, 2]) / osd["Vægtning"].astype(int).sum()) * 100
            except:
                procentuelvægt = 0
            try:
                chkBoxItem = QTableWidgetItem(f"{round(procentuelvægt, 2)}")
                chkBoxItem.setFlags(Qt.ItemIsEnabled)
                chkBoxItem.setTextAlignment(Qt.AlignCenter)

                self.OSDTable.setItem(row, 3, chkBoxItem)
                self.repaint()

            except:
                pass

    self.repaint()









