
from builtins import str, range

import os, time, sys

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import (
    QgsFields,
    QgsField,
    QgsFeature,
QgsLayerTreeGroup,
    QgsGeometry,
    QgsFeatureRequest,
    QgsProject,
QgsSimpleMarkerSymbolLayerBase,
    QgsRaster,
    QgsTask,
    QgsPrintLayout,
    QgsReadWriteContext,
    QgsRasterFileWriter,
    QgsMapLayer,
    QgsLayoutItemLegend,
    QgsLayoutItemMap,
    QgsLegendRenderer,
    QgsExpression,
    QgsLegendStyle,
    QgsLayoutExporter,
    QgsLayoutItemPicture,
    QgsLayoutPoint,
    QgsLayoutSize,
    QgsUnitTypes,
    QgsRasterBandStats,
    QgsCategorizedSymbolRenderer,
    QgsRendererCategory,
    QgsSymbol,
    QgsCoordinateTransformContext,
QgsRuleBasedRenderer,
    QgsRandomColorRamp,
    QgsMapThemeCollection,
    QgsLayerDefinition,
    QgsPointXY,
    QgsApplication,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsExpressionContextScope,
    QgsCoordinateReferenceSystem,
    QgsProcessingFeatureSourceDefinition,
    QgsRectangle,
    QgsTaskManager,
    QgsLayout,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsRasterLayer,
    QgsWkbTypes,
    QgsDataSourceUri
)
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()

import pyodbc



__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *
from ..doDataExtract import *


def tester_one(self):
    self.tableWidget_2.setRowCount(0)
    self.tableWidget_3.setRowCount(0)

    mapCanvas = self.iface.mapCanvas()
    comboboxes = []

    def type_change():
        for i in range(self.tableWidget_2.rowCount()):
                try:
                    widget = self.tableWidget_2.cellWidget(i, 1)
                except:
                    widget = None
                if widget is not None:
                    if widget.currentText() == "Boolesk (ja/nej)":
                        self.tableWidget_2.removeCellWidget(i, 2)
                        chkBoxItem = QTableWidgetItem("X")
                        chkBoxItem.setFlags(Qt.ItemIsEnabled)
                        chkBoxItem.setTextAlignment(Qt.AlignCenter)
                        self.tableWidget_2.setItem(i, 2, chkBoxItem)
                    else:
                        spinBox = QtWidgets.QSpinBox()
                        spinBox.valueChanged.connect(lambda: tester_two(self=self))
                        self.tableWidget_2.setCellWidget(i, 2, spinBox)



    for i in range(self.spinBox_6.value()):
        rowPosition = self.tableWidget_2.rowCount()
        self.tableWidget_2.insertRow(rowPosition)
        self.tableWidget_2.setItem(i, 0, QtWidgets.QTableWidgetItem())
        comboBox = QtWidgets.QComboBox()
        #comboBox.currentIndexChanged.connect(lambda: type_change())
        #comboBox.currentIndexChanged.connect(lambda: tester_two(self=self))

        comboBox2 = QtWidgets.QComboBox()
        comboBox2.currentIndexChanged.connect(lambda: type_change())
        comboBox2.currentIndexChanged.connect(lambda: tester_two(self=self))
        for k in range(mapCanvas.layerCount()):
            layer = mapCanvas.layer(k)

            if layer.type() == layer.RasterLayer or layer.type() == layer.VectorLayer and layer.geometryType() == 0:
                comboBox.addItem(layer.name(), layer)


        comboBox2.addItem("Værdibaseret stiger ift. vægtning")
        comboBox2.addItem("Værdibaseret falder ift. vægtning")
        comboBox2.addItem("Boolesk (ja/nej)")

        comboboxes.append(comboBox2)

        self.tableWidget_2.setCellWidget(i, 0, comboBox)
        self.tableWidget_2.setCellWidget(i, 1, comboBox2)





    type_change()

    for x in comboboxes:
        x.currentIndexChanged.connect(lambda: type_change())
        x.currentIndexChanged.connect(lambda: tester_two(self=self))


#lambda: tester_two(self=self)

def tester_two(self):
         self.tableWidget_3.setRowCount(0)


         parametre = []

         for i in range(self.tableWidget_2.rowCount()):
             widget = self.tableWidget_2.cellWidget(i, 0)
             if isinstance(widget, QComboBox):
                parametre.append(widget.currentText())

         length = []
         xout = []
         for i in range(self.tableWidget_2.rowCount()):
             widget2 = self.tableWidget_2.cellWidget(i, 2)
             try:
                 if isinstance(widget2, QSpinBox):
                     length.append(widget2.value())
                     xout.append(1)
                 elif self.tableWidget_2.item(i, 2).text() == "X":
                     length.append(2)
                     xout.append(2)
                 else:
                     length.append(0)
                     xout.append(1)
             except:
                 length.append(0)
                 xout.append(1)

         self.tableWidget_3.setRowCount(sum(length))

         if sum(length) == 0:
             pass
         else:
             for i, para in enumerate(parametre):
                 if i == 0:
                     self.tableWidget_3.setSpan(0, 0, length[i], 1)
                     newItem = QTableWidgetItem("{}".format(para))
                     newItem.setFlags(Qt.ItemIsEnabled)
                     newItem.setTextAlignment(Qt.AlignCenter)

                     self.tableWidget_3.setItem(0, 0, newItem)


                     self.tableWidget_3.setSpan(0, 4, length[i], 1)
                     sp = QDoubleSpinBox()
                     sp.setDecimals(0)
                     sp.setMinimum(0)
                     sp.setMaximum(100)
                     sp.setValue(0)
                     sp.setAlignment(Qt.AlignHCenter)

                     self.tableWidget_3.setCellWidget(0, 4, sp)

                     #chkBoxItem = QTableWidgetItem("")

                     #chkBoxItem.setTextAlignment(Qt.AlignCenter)
                    # self.tableWidget_3.setItem(0, 4, chkBoxItem)



                 else:
                     start = sum(length[:i])
                     self.tableWidget_3.setSpan(start, 0, length[i], 1)
                     newItem = QTableWidgetItem("{}".format(para))
                     newItem.setFlags(Qt.ItemIsEnabled)
                     newItem.setTextAlignment(Qt.AlignCenter)
                     self.tableWidget_3.setItem(start, 0, newItem)

                     self.tableWidget_3.setSpan(start, 4, length[i], 1)
                     sp = QDoubleSpinBox()
                     sp.setAlignment(Qt.AlignHCenter)
                     sp.setDecimals(0)
                     sp.setMinimum(0)
                     sp.setMaximum(100)
                     sp.setValue(0)
                     self.tableWidget_3.setCellWidget(start, 4, sp)

                    # self.tableWidget_3.setSpan(start, 4, length[i], 1)
                    # chkBoxItem = QTableWidgetItem("")
                    # chkBoxItem.setTextAlignment(Qt.AlignCenter)
                    # self.tableWidget_3.setItem(start, 4, chkBoxItem)


         xs = []
         for i in range(len(xout)):
           #  xs.extend([xout[i]] * length[i])
             xs += [xout[i]] * length[i]
         print(xout)
         print(xs)
         for i, x in enumerate(xs):
             if x==1:
                 comboBox5 = QtWidgets.QComboBox()

                 comboBox5.addItem("<")
                 comboBox5.addItem(">")
                 comboBox5.addItem("<=")
                 comboBox5.addItem(">=")
                 self.tableWidget_3.setCellWidget(i, 1, comboBox5)



                # chkBoxItem = QTableWidgetItem("")
                # chkBoxItem.setTextAlignment(Qt.AlignCenter)
                # self.tableWidget_3.setItem(i, 2, chkBoxItem)

                 sp = QDoubleSpinBox()
                 sp.setAlignment(Qt.AlignHCenter)
                 sp.setDecimals(0)
                 sp.setMinimum(-999999999)
                 sp.setMaximum(999999999)
                 sp.setValue(0)
                 self.tableWidget_3.setCellWidget(i, 2, sp)



             elif x==2:
                 chkBoxItem = QTableWidgetItem("")
                 chkBoxItem.setFlags(Qt.ItemIsEnabled)
                 chkBoxItem.setTextAlignment(Qt.AlignCenter)
                 self.tableWidget_3.setItem(i, 1, chkBoxItem)
                 self.tableWidget_3.item(i, 1).setBackground(QtGui.QColor(128, 128, 128))

                 comboBox323 = QtWidgets.QComboBox()
                 comboBox323.addItem("Indenfor")
                 comboBox323.addItem("Udenfor")
                 self.tableWidget_3.setCellWidget(i, 2, comboBox323)



         self.tableWidget_3.verticalHeader().hide()


         for i in range(self.tableWidget_3.rowCount()):
             sp = QDoubleSpinBox()
             sp.setDecimals(0)
             sp.setMinimum(-999999999)
             sp.setMaximum(999999999)
             sp.setValue(0)
             sp.setAlignment(Qt.AlignHCenter)
             self.tableWidget_3.setCellWidget(i, 3, sp)

            # chkBoxItem = QTableWidgetItem("")
            # chkBoxItem.setTextAlignment(Qt.AlignCenter)
             #self.tableWidget_3.setItem(i, 3, chkBoxItem)














