
# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from collections import OrderedDict

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime,QDate, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication, QDoubleSpinBox
from qgis.core import *
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()
import pyodbc



__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *
from qgis.gui import *

import os


from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.font_manager import FontProperties
from matplotlib.collections import PatchCollection


class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)


FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'PlottingTool2.ui'))

class YourDialog(QgsDockWidget, FORM_CLASS):

   def __init__(self, parent=None):
      """Constructor."""
      super(YourDialog, self).__init__(parent)
      self.setupUi(self)
      self.values_sorted = None

      self.layer_combo.layerChanged.connect(self.selected_layer_changed)
      self.layer_combo.setFilters(QgsMapLayerProxyModel.VectorLayer)
      self.x_combo.fieldChanged.connect(self.parameter_changed)
      self.y_combo.fieldChanged.connect(self.parameter_changed)

      self.comboBox.currentIndexChanged.connect(self.updater_layout)
      self.z_combo.fieldChanged.connect(self.find_grupperinger)
      self.updater_layout()
      self.selected_layer_changed(self.layer_combo.currentLayer())
      self.draw_btn.clicked.connect(self.create_plot)
      self.sc = MplCanvas(self, width=6, height=5, dpi=100)
      self.toolbar = NavigationToolbar(self.sc, self)
      self.clear_btn.clicked.connect(self.clear_plot)
      self.toolbar.setVisible(False)
      self.layout = QtWidgets.QVBoxLayout()
      self.save_figure.clicked.connect(self.savefigure)
      self.refreshlayout.clicked.connect(self.refresh)

      self.checkBox.stateChanged.connect(self.updater_layout)

     # buttonClicked = self.sender()
     # index = self.tableWidget.indexAt(buttonClicked.pos())

      self.tableWidget.cellClicked.connect(self.remove_row)

   def savefigure(self):
       pluginpath, _filter = QFileDialog.getSaveFileName(self,
                                                          'Gem figur', "figur.png" ,"PNG(*.png)")

       self.sc.figure.savefig(pluginpath, dpi=300)
   def refresh(self):
        self.sc.figure.tight_layout()
        self.sc.figure.canvas.draw_idle()

   def find_grupperinger(self):





       if self.visible_feature_check.isChecked():
           if self.selected_feature_check.checkState() == Qt.Checked:
               pass
           else:
               self.layer_combo.currentLayer().selectAll()

           selIds = self.layer_combo.currentLayer().selectedFeatureIds()
           visible_region = QgsReferencedRectangle(iface.mapCanvas().extent(),iface.mapCanvas().mapSettings().destinationCrs())
           request = QgsFeatureRequest()
           request.setFilterRect(visible_region)
           ittemp = self.layer_combo.currentLayer().getFeatures(QgsFeatureRequest(request))
           newIds = [i.id() for i in ittemp]
           idsToSel = list(set(selIds).intersection(newIds))
           self.layer_combo.currentLayer().selectByIds([s for s in idsToSel])

       else:
           if self.selected_feature_check.checkState() == Qt.Unchecked:
               self.layer_combo.currentLayer().selectAll()
           else:
               pass

       a = processing.run("native:saveselectedfeatures",
                      {'INPUT': self.layer_combo.currentLayer(),
                       'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

       idx = self.layer_combo.currentLayer().fields().indexOf('{}'.format(self.z_combo.currentText()))
       values = a.uniqueValues(idx)
       self.values_sorted = sorted(values)

       self.tableWidget.setRowCount(len(self.values_sorted))
       cm = plt.get_cmap('rainbow', len(self.values_sorted))
       del a

       for i in range(self.tableWidget.rowCount()):
           newItem = QTableWidgetItem("{}".format(self.values_sorted[i]))
           newItem.setTextAlignment(Qt.AlignCenter)
           newItem.setFlags(Qt.ItemIsEnabled)
           self.tableWidget.setItem(i, 0, newItem)

           newItem = QTableWidgetItem("{}".format(self.values_sorted[i]))
           newItem.setTextAlignment(Qt.AlignCenter)
           self.tableWidget.setItem(i, 1, newItem)

           sp = QgsColorButton()
           sp.setColor(QColor('{}'.format(matplotlib.colors.rgb2hex(cm(i)))))
           self.tableWidget.setCellWidget(i, 2, sp)

           sp = QDoubleSpinBox()
           sp.setAlignment(Qt.AlignHCenter)
           sp.setMinimum(0)
           sp.setMaximum(99)
           sp.setSingleStep(0.1)
           sp.setValue(8)
           self.tableWidget.setCellWidget(i, 3, sp)

           comboBox = QtWidgets.QComboBox()
           comboBox.clear()
           for k, v in self.point_types.items():
               comboBox.addItem(k, v)

           self.tableWidget.setCellWidget(i, 4, comboBox)

           sp = QgsColorButton()
           sp.setColor(QColor('{}'.format(matplotlib.colors.rgb2hex(cm(i)))))
           self.tableWidget.setCellWidget(i, 5, sp)

           sp = QDoubleSpinBox()
           sp.setAlignment(Qt.AlignHCenter)
           sp.setMinimum(0)
           sp.setMaximum(99)
           sp.setSingleStep(0.1)
           sp.setValue(1)
           self.tableWidget.setCellWidget(i, 6, sp)

           comboBox = QtWidgets.QComboBox()
           comboBox.clear()
           for k, v in self.line_types.items():
               comboBox.addItem(k, v)

           self.tableWidget.setCellWidget(i, 7, comboBox)

           chkBoxItem = QTableWidgetItem("X")
           chkBoxItem.setFlags(Qt.ItemIsEnabled)
           chkBoxItem.setTextAlignment(Qt.AlignCenter)
           self.tableWidget.setItem(i, 8, chkBoxItem)


      # for x in toolbuttons.values():
      #     x.clicked.connect(lambda: remove_row(button=x))
   def remove_row(self, row, column):
       if column == 8:
           self.tableWidget.removeRow(row)


   def parameter_changed(self):
       self.x_axis_title.setText(f'{self.x_combo.currentText()}')
       self.y_axis_title.setText(f'{self.y_combo.currentText()}')
       if self.comboBox.currentText() == "Barplot":
           self.legend_title.setText(f'{self.y_combo.currentText()}')

   def updater_layout(self):
       if self.comboBox.currentText() == "Scatterplot":
           if self.checkBox.checkState() == Qt.Checked:
               self.properties_group_box.setVisible(True)
               self.tableWidget.setVisible(True)
               self.in_color_lab.setVisible(False)
               self.in_color_combo.setVisible(False)
               self.in_color_defined_button.setVisible(False)
               self.line_combo.setVisible(False)
               self.line_lab.setVisible(False)
               self.marker_size.setVisible(False)
               self.marker_size_lab.setVisible(False)
               self.marker_width.setVisible(False)
               self.marker_width_lab.setVisible(False)
               self.point_combo.setVisible(False)
               self.point_lab.setVisible(False)
               self.size_defined_button.setVisible(False)
               self.stroke_defined_button.setVisible(False)
               self.z_combo.setVisible(True)
               self.z_label.setVisible(True)
               self.checkBox.setVisible(True)
               self.out_color_lab.setVisible(False)
               self.out_color_combo.setVisible(False)
               self.out_color_defined_button.setVisible(False)
               self.bar_label.setVisible(False)
               self.bar_orientation.setVisible(False)
               self.marker_type_lab.setVisible(True)
               self.marker_type_combo.setVisible(True)
               self.marker_size.setValue(8)
               self.marker_width.setValue(1)
               self.x_axis_mode_combo.setVisible(True)
               self.x_axis_mode_label.setVisible(True)
               self.y_axis_mode_combo.setVisible(True)
               self.y_axis_mode_label.setVisible(True)
           else:
               self.properties_group_box.setVisible(True)
               self.tableWidget.setVisible(False)
               self.in_color_lab.setVisible(True)
               self.in_color_combo.setVisible(True)
               self.in_color_defined_button.setVisible(True)
               self.line_combo.setVisible(True)
               self.line_lab.setVisible(True)
               self.marker_size.setVisible(True)
               self.marker_size_lab.setVisible(True)
               self.marker_width.setVisible(True)
               self.marker_width_lab.setVisible(True)
               self.point_combo.setVisible(True)
               self.point_lab.setVisible(True)
               self.size_defined_button.setVisible(True)
               self.stroke_defined_button.setVisible(True)
               self.z_combo.setVisible(True)
               self.z_label.setVisible(True)
               self.checkBox.setVisible(True)
               self.out_color_lab.setVisible(True)
               self.out_color_combo.setVisible(True)
               self.out_color_defined_button.setVisible(True)
               self.bar_label.setVisible(False)
               self.bar_orientation.setVisible(False)
               self.marker_type_lab.setVisible(True)
               self.marker_type_combo.setVisible(True)
               self.in_color_lab.setText("Markør farve")
               self.marker_size_lab.setText("Markør str.")
               self.marker_width_lab.setText("Stroketykkelse")
               self.marker_size.setValue(8)
               self.marker_width.setValue(1)
               self.x_axis_mode_combo.setVisible(True)
               self.x_axis_mode_label.setVisible(True)
               self.y_axis_mode_combo.setVisible(True)
               self.y_axis_mode_label.setVisible(True)

       elif self.comboBox.currentText() == "Barplot":
           self.properties_group_box.setVisible(True)
           self.tableWidget.setVisible(False)
           self.in_color_lab.setVisible(True)
           self.in_color_combo.setVisible(True)
           self.in_color_defined_button.setVisible(True)
           self.marker_type_lab.setVisible(False)
           self.marker_type_combo.setVisible(False)
           self.line_combo.setVisible(False)
           self.line_lab.setVisible(False)
           self.marker_size.setVisible(True)
           self.marker_size_lab.setVisible(True)
           self.marker_width.setVisible(True)
           self.marker_width_lab.setVisible(True)
           self.point_combo.setVisible(False)
           self.point_lab.setVisible(False)
           self.size_defined_button.setVisible(True)
           self.stroke_defined_button.setVisible(True)
           self.z_combo.setVisible(False)
           self.z_label.setVisible(False)
           self.checkBox.setVisible(False)
           self.out_color_lab.setVisible(True)
           self.out_color_combo.setVisible(True)
           self.out_color_defined_button.setVisible(True)
           self.bar_label.setVisible(True)
           self.bar_orientation.setVisible(True)
           self.x_axis_mode_combo.setVisible(False)
           self.x_axis_mode_label.setVisible(False)
           self.y_axis_mode_combo.setVisible(False)
           self.y_axis_mode_label.setVisible(False)
           self.in_color_lab.setText("Bar farve")
           self.marker_size_lab.setText("Bar bredde")
           self.marker_width_lab.setText("Stroke str.")
           self.marker_size.setValue(0.5)

   def selected_layer_changed(self, layer):
       """
       Trigger actions after selected layer changes
       """
       self.x_combo.setLayer(layer)
       self.y_combo.setLayer(layer)
       self.z_combo.setLayer(layer)
       self.additional_info_combo.setLayer(layer)

       buttons = self.findChildren(QgsPropertyOverrideButton)
       for button in buttons:
           button.setVectorLayer(layer)
       try:
        self.legend_title.setText("{}".format(layer.name()))
       except:
           pass

       self.x_axis_mode_combo.clear()
       self.x_axis_mode_combo.addItem(self.tr('Lineær'), 'linear')
       self.x_axis_mode_combo.addItem(self.tr('Logaritmisk'), 'log')
       self.y_axis_mode_combo.clear()
       self.y_axis_mode_combo.addItem(self.tr('Lineær'), 'linear')
       self.y_axis_mode_combo.addItem(self.tr('Logaritmisk'), 'log')

       self.in_color_combo.setColor(QColor('#2ca25f'))
       self.out_color_combo.setColor(QColor('#2ca25f'))
       self.verticalline_color.setColor(QColor('#000000'))
       self.horisontalline_color.setColor(QColor('#000000'))

       self.font_title_color.setColor(QColor('#000000'))
       self.font_xlabel_color.setColor(QColor('#000000'))
       self.font_xticks_color.setColor(QColor('#000000'))
       self.font_ylabel_color.setColor(QColor('#000000'))
       self.font_yticks_color.setColor(QColor('#000000'))


       self.font_title_style.setCurrentFont(QFont('Arial', 18, QFont.Bold))
       self.font_title_style.setMode(QgsFontButton.ModeQFont)
       self.font_xlabel_style.setCurrentFont(QFont('Arial', 12, QFont.Bold))
       self.font_xlabel_style.setMode(QgsFontButton.ModeQFont)

       self.font_xticks_style.setCurrentFont(QFont('Arial', 10))
       self.font_xticks_style.setMode(QgsFontButton.ModeQFont)

       self.font_ylabel_style.setCurrentFont(QFont('Arial', 12, QFont.Bold))
       self.font_ylabel_style.setMode(QgsFontButton.ModeQFont)

       self.font_yticks_style.setCurrentFont(QFont('Arial', 10))
       self.font_yticks_style.setMode(QgsFontButton.ModeQFont)

       self.x_axis_min.setRange(sys.float_info.max * -1, sys.float_info.max)
       self.x_axis_max.setRange(sys.float_info.max * -1, sys.float_info.max)
       self.y_axis_min.setRange(sys.float_info.max * -1, sys.float_info.max)
       self.y_axis_max.setRange(sys.float_info.max * -1, sys.float_info.max)
       self.layout_grid_axis_color.setColor(QColor('#bdbfc0'))



       self.point_types = OrderedDict([
           (QIcon(os.path.join(
            os.path.dirname(__file__),'circle.svg')), self.tr('Cirkel')),
           (QIcon(os.path.join(
            os.path.dirname(__file__),'square.svg')), self.tr('Firkant')),
           (QIcon(os.path.join(
            os.path.dirname(__file__),'diamond.svg')), self.tr('Diamant')),
           (QIcon(os.path.join(
            os.path.dirname(__file__),'cross.svg')), self.tr('Kryds')),
           (QIcon(os.path.join(
            os.path.dirname(__file__),'triangle.svg')), self.tr('Trekant')),
           (QIcon(os.path.join(
            os.path.dirname(__file__),'penta.svg')), self.tr('Femkant')),
           (QIcon(os.path.join(
            os.path.dirname(__file__),'star.svg')), self.tr('Stjerne')),
       ])


       self.point_combo.clear()
       for k, v in self.point_types.items():
           self.point_combo.addItem(k, v)


       self.line_types = OrderedDict([
           (QIcon(os.path.join(
               os.path.dirname(__file__), 'solid.png')), self.tr('Linje')),
           (QIcon(os.path.join(
               os.path.dirname(__file__), 'dot.png')), self.tr('Dotted Linje')),
           (QIcon(os.path.join(
               os.path.dirname(__file__), 'dash.png')), self.tr('Dashed Linje'))
       ])

       self.line_combo.clear()
       for k, v in self.line_types.items():
            self.line_combo.addItem(k, v)

   def clear_plot(self):
       try:
           self.sc.axes.clear()
           self.sc.axes.figure.canvas.draw_idle()
       except:
           pass

   def create_plot(self):
       try:
      #     self.sc.axes.clear()
           self.sc.axes.figure.canvas.draw_idle()
           self.sc.axes.reset_position()
       except:
           pass
#

       lyr = self.layer_combo.currentLayer()


       context = QgsExpressionContext()
       context.appendScopes(QgsExpressionContextUtils.globalProjectLayerScopes(lyr))

       def add_source_field_or_expression(field_or_expression):
           field_index = lyr.fields().lookupField(field_or_expression)
           if field_index == -1:
               expression = QgsExpression(field_or_expression)
               if not expression.hasParserError():
                   expression.prepare(context)
               return expression, expression.needsGeometry(), expression.referencedColumns()

           return None, False, {field_or_expression}

       x_expression, x_needs_geom, x_attrs = add_source_field_or_expression(self.x_combo.currentText()) if len(self.x_combo.currentText()) > 0 else (None, False, set())
       y_expression, y_needs_geom, y_attrs = add_source_field_or_expression(self.y_combo.currentText()) if len(self.y_combo.currentText()) > 0 else (None, False, set())


       if self.selected_feature_check.checkState() == Qt.Checked:
           self.selected_features_only = True
       else:
           self.selected_features_only = False


       if self.selected_features_only is True:
           it = lyr.getSelectedFeatures()
       else:
           it = lyr.getFeatures()

       if self.visible_feature_check.isChecked():
           if self.selected_features_only is True:
               pass
           else:
               lyr.selectAll()

           selIds = lyr.selectedFeatureIds()
           visible_region = QgsReferencedRectangle(iface.mapCanvas().extent(),
                                                   iface.mapCanvas().mapSettings().destinationCrs())
           request = QgsFeatureRequest()
           request.setFilterRect(visible_region)
           ittemp = lyr.getFeatures(QgsFeatureRequest(request))
           newIds = [i.id() for i in ittemp]
           idsToSel = list(set(selIds).intersection(newIds))
           lyr.selectByIds([s for s in idsToSel])

           it = lyr.getSelectedFeatures()


       if self.comboBox.currentText() == "Scatterplot" and self.checkBox.checkState() == Qt.Checked:
           pass
       else:
           xx = []
           yy = []

           for f in it:
               context.setFeature(f)

               x = None
               if x_expression:
                   x = x_expression.evaluate(context)
                   if x == NULL or x is None:
                       continue
               elif len(self.x_combo.currentText()) > 0:
                   x = f[self.x_combo.currentText()]
                   if x == NULL or x is None:
                       continue
               y = None
               if y_expression:
                   y = y_expression.evaluate(context)
                   if y == NULL or y is None:
                       continue
               elif len(self.y_combo.currentText()) > 0:
                   y = f[self.y_combo.currentText()]
                   if y == NULL or y is None:
                       continue

               if x is not None:
                   xx.append(x)
               if y is not None:
                   yy.append(y)

           try:
               if isinstance(xx[0], QDateTime):
                   xx = [x.toPyDateTime() for x in xx]
           except:
               pass
           try:
               if isinstance(xx[0], QDate):
                   xx = [x.toPyDate() for x in xx]
           except:
               pass
           try:
               if isinstance(yy[0], QDateTime):
                   yy = [y.toPyDateTime() for y in yy]
           except:
               pass
           try:
               if isinstance(yy[0], QDate):
                   yy = [y.toPyDate() for y in yy]
           except:
               pass


       if self.comboBox.currentText() == "Scatterplot":
           opacity = self.opacity_widget.opacity()

           if self.checkBox.checkState() == Qt.Checked:

               selIds = lyr.selectedFeatureIds()
               plotitems = []
               for row in range(self.tableWidget.rowCount()):
                   plotitems.append(self.tableWidget.item(row, 0).text())

               for i, val in enumerate(plotitems):
                   expr = QgsExpression(f"\"{self.z_combo.currentText()}\"= '{val}'")
                   ittemp = lyr.getFeatures(QgsFeatureRequest(expr))
                   newIds = [i.id() for i in ittemp]
                   idsToSel = list(set(selIds).intersection(newIds))
                   it2 = lyr.getFeatures(idsToSel)

                 #  if self.visible_feature_check.isChecked():
                 #      selIds = lyr.selectedFeatureIds()
                 #      visible_region = QgsReferencedRectangle(iface.mapCanvas().extent(),
                 #                                              iface.mapCanvas().mapSettings().destinationCrs())
                 #      request = QgsFeatureRequest()
                 #      request.setFilterRect(visible_region)
                 #      ittemp = lyr.getFeatures(QgsFeatureRequest(request))
                 #      newIds = [i.id() for i in ittemp]
                 #      idsToSel = list(set(selIds).intersection(newIds))
                 #      lyr.selectByIds([s for s in idsToSel])
                 #      it2 = lyr.getSelectedFeatures()

                   xx = []
                   yy = []

                   for f in it2:
                       context.setFeature(f)

                       x = None
                       if x_expression:
                           x = x_expression.evaluate(context)
                           if x == NULL or x is None:
                               continue
                       elif len(self.x_combo.currentText()) > 0:
                           x = f[self.x_combo.currentText()]
                           if x == NULL or x is None:
                               continue
                       y = None
                       if y_expression:
                           y = y_expression.evaluate(context)
                           if y == NULL or y is None:
                               continue
                       elif len(self.y_combo.currentText()) > 0:
                           y = f[self.y_combo.currentText()]
                           if y == NULL or y is None:
                               continue

                       if x is not None:
                           xx.append(x)
                       if y is not None:
                           yy.append(y)

                   try:
                       if isinstance(xx[0], QDateTime):
                           xx = [x.toPyDateTime() for x in xx]
                   except:
                       pass
                   try:
                       if isinstance(xx[0], QDate):
                           xx = [x.toPyDate() for x in xx]
                   except:
                       pass
                   try:
                       if isinstance(yy[0], QDateTime):
                           yy = [y.toPyDateTime() for y in yy]
                   except:
                       pass
                   try:
                       if isinstance(yy[0], QDate):
                           yy = [y.toPyDate() for y in yy]
                   except:
                       pass



                   markercolor = self.tableWidget.cellWidget(i, 2).color().name()
                   markersize = self.tableWidget.cellWidget(i, 3).value()
                   label = self.tableWidget.item(i, 1).text()

                   markerstyle_text = self.tableWidget.cellWidget(i, 4).currentText()

                   if markerstyle_text == "Cirkel" and self.marker_type_combo.currentText() != "Linjer":
                       markerstyle = "o"
                   elif markerstyle_text == "Firkant" and self.marker_type_combo.currentText() != "Linjer":
                       markerstyle = "s"
                   elif markerstyle_text == "Diamant" and self.marker_type_combo.currentText() != "Linjer":
                       markerstyle = "D"
                   elif markerstyle_text == "Kryds" and self.marker_type_combo.currentText() != "Linjer":
                       markerstyle = "x"
                   elif markerstyle_text == "Trekant" and self.marker_type_combo.currentText() != "Linjer":
                       markerstyle = "^"
                   elif markerstyle_text == "Femkant" and self.marker_type_combo.currentText() != "Linjer":
                       markerstyle = "p"
                   elif markerstyle_text == "Stjerne" and self.marker_type_combo.currentText() != "Linjer":
                       markerstyle = "*"
                   else:
                       markerstyle = ""

                   color = self.tableWidget.cellWidget(i, 5).color().name()
                   linewidth = self.tableWidget.cellWidget(i, 6).value()

                   linestyle_text = self.tableWidget.cellWidget(i, 7).currentText()

                   if linestyle_text == "Linje" and self.marker_type_combo.currentText() != "Markører":
                       linestyle = "-"
                   elif linestyle_text == "Dotted Linje" and self.marker_type_combo.currentText() != "Markører":
                       linestyle = ":"
                   elif linestyle_text == "Dashed Linje" and self.marker_type_combo.currentText() != "Markører":
                       linestyle = "--"
                   else:
                       linestyle = ""

                   self.sc.axes.plot(xx, yy, color=color, linestyle=linestyle, marker=markerstyle, label=label,
                                     markerfacecolor=markercolor, linewidth=linewidth, markersize=markersize,
                                     alpha=opacity)



           else:
               if self.point_combo.currentText() == "Cirkel" and self.marker_type_combo.currentText() != "Linjer":
                   markerstyle = "o"
               elif self.point_combo.currentText() == "Firkant" and self.marker_type_combo.currentText() != "Linjer":
                   markerstyle = "s"
               elif self.point_combo.currentText() == "Diamant" and self.marker_type_combo.currentText() != "Linjer":
                   markerstyle = "D"
               elif self.point_combo.currentText() == "Kryds" and self.marker_type_combo.currentText() != "Linjer":
                   markerstyle = "x"
               elif self.point_combo.currentText() == "Trekant" and self.marker_type_combo.currentText() != "Linjer":
                   markerstyle = "^"
               elif self.point_combo.currentText() == "Femkant" and self.marker_type_combo.currentText() != "Linjer":
                   markerstyle = "p"
               elif self.point_combo.currentText() == "Stjerne" and self.marker_type_combo.currentText() != "Linjer":
                   markerstyle = "*"
               else:
                   markerstyle = ""

               if self.line_combo.currentText() == "Linje" and self.marker_type_combo.currentText() != "Markører":
                   linestyle = "-"
               elif self.line_combo.currentText() == "Dotted Linje" and self.marker_type_combo.currentText() != "Markører":
                   linestyle = ":"
               elif self.line_combo.currentText() == "Dashed Linje" and self.marker_type_combo.currentText() != "Markører":
                   linestyle = "--"
               else:
                   linestyle = ""

               markercolor = '{}'.format(self.in_color_combo.color().name())
               color = '{}'.format(self.out_color_combo.color().name())
               markersize = self.marker_size.value()
               linewidth = self.marker_width.value()
               label = self.legend_title.text()

               self.sc.axes.plot(xx, yy, color=color, linestyle=linestyle, marker=markerstyle, label=label, markerfacecolor=markercolor, linewidth=linewidth, markersize=markersize, alpha=opacity)


       elif self.comboBox.currentText() == "Barplot":#Barplot
           barcolor = '{}'.format(self.in_color_combo.color().name())
           color = '{}'.format(self.out_color_combo.color().name())
           barsize = self.marker_size.value()
           linewidth = self.marker_width.value()
           label = self.legend_title.text()
           if self.bar_orientation.currentText() == "Vertikal":
                self.sc.axes.bar(xx, yy, width=barsize, color=barcolor, edgecolor=color, linewidth=linewidth, label=label)
           else:
                self.sc.axes.barh(xx, yy, height=barsize, color=barcolor, edgecolor=color, linewidth=linewidth,
                                label=label)


       if self.invert_x_check.checkState() == Qt.Checked:
           self.sc.axes.invert_xaxis()
       if self.invert_y_check.checkState() == Qt.Checked:
           self.sc.axes.invert_yaxis()

       if self.comboBox.currentText() == "Scatterplot":
           if self.x_axis_mode_combo.currentText() == "Lineær":
               pass
           else:
               self.sc.axes.set_xscale('log')

           if self.y_axis_mode_combo.currentText() == "Lineær":
               pass
           else:
               self.sc.axes.set_yscale('log')

       if self.x_axis_bounds_check.isChecked():
           self.sc.axes.set_xlim([self.x_axis_min.value(), self.x_axis_max.value()])

       if self.y_axis_bounds_check.isChecked():
           self.sc.axes.set_ylim([self.y_axis_min.value(), self.y_axis_max.value()])

       if self.horisontalline.isChecked():
           self.sc.axes.axhline(y=self.horisontalline_value.value(), linewidth=self.horisontalline_thickness.value(),
                                color=self.horisontalline_color.color().name())

       if self.verticalline_check.isChecked():
           self.sc.axes.axvline(x=self.verticalline_value.value(), linewidth=self.verticalline_thickness.value(),
                                color=self.verticalline_color.color().name())

       self.sc.axes.grid(color=self.layout_grid_axis_color.color().name())

       self.sc.axes.reset_position()


       handles, labels = self.sc.axes.get_legend_handles_labels()

       legfp = FontProperties(family=self.font_title_style.currentFont().family(), size=10)
       if self.combo_text_position.currentText() == "Standard":
           leg = self.sc.axes.legend(handles, labels, prop=legfp)
           if self.comboBox.currentText() == "Scatterplot" and self.checkBox.checkState() == Qt.Checked:
               leg.set_title('{}'.format(self.legend_title.text()), prop=FontProperties(family=self.font_title_style.currentFont().family(), size=12, weight='heavy')) #{'size': 12, 'weight': 'heavy'}

       elif self.combo_text_position.currentText() == "Til højre":
           box = self.sc.axes.get_position()
           self.sc.axes.set_position([box.x0, box.y0, box.width * 0.8, box.height])
           leg = self.sc.axes.legend(handles, labels, loc='center left', bbox_to_anchor=(1, 0.5),fancybox=True, shadow=True, prop=legfp)
       elif self.combo_text_position.currentText() == "Under":
           if len(labels) < 6:
               box = self.sc.axes.get_position()
               self.sc.axes.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
               leg = self.sc.axes.legend(handles, labels, loc='upper center', bbox_to_anchor=(0.5, -0.05),fancybox=True, shadow=True, ncol=len(labels), prop=legfp)
           else:
               box = self.sc.axes.get_position()
               self.sc.axes.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
               leg = self.sc.axes.legend(handles, labels, loc='upper center', bbox_to_anchor=(0.5, -0.05),fancybox=True, shadow=True, ncol=len(labels)/2, prop=legfp)
       elif self.combo_text_position.currentText() == "Over":
           if len(labels) < 6:
               box = self.sc.axes.get_position()
               self.sc.axes.set_position([box.x0, box.y0, box.width, box.height * 0.85])
               leg = self.sc.axes.legend(handles, labels, loc='upper center', bbox_to_anchor=(0.5, 1.35),fancybox=True, shadow=True, ncol=len(labels), prop=legfp)
           else:
               box = self.sc.axes.get_position()
               self.sc.axes.set_position([box.x0, box.y0, box.width, box.height * 0.85])
               leg = self.sc.axes.legend(handles, labels, loc='upper center', bbox_to_anchor=(0.5, 1.35),fancybox=True, shadow=True, ncol=len(labels)/2, prop=legfp)

       leg.set_draggable(state=True)

       if self.comboBox.currentText() == "Scatterplot":
           lines = self.sc.axes.get_lines()

           lined = {}  # Will map legend lines to original lines.
           for legline, origline in zip(leg.get_lines(), lines):
               legline.set_picker(True)
               legline.set_pickradius(5)  # Enable picking on the legend line.
               lined[legline] = origline

           def on_pick(event):
               legline = event.artist
               origline = lined[legline]
               visible = not origline.get_visible()
               origline.set_visible(visible)
               legline.set_alpha(1.0 if visible else 0.2)
               self.sc.figure.canvas.draw()

           self.sc.figure.canvas.mpl_connect('pick_event', on_pick)

           annot = self.sc.axes.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                                         bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                                         arrowprops=dict(arrowstyle="->"))
           annot.set_visible(False)

           def update_annot(line, annot, ind):
               posx, posy = [line.get_xdata()[ind], line.get_ydata()[ind]]

               if len(self.additional_info_combo.currentText()) > 0:
                   lyr.selectByExpression(
                       f"\"{self.x_combo.currentText()}\"= '{posx}' AND \"{self.y_combo.currentText()}\"= '{posy}'")
                   gid = lyr.getSelectedFeatures()
                   list = []
                   for feature in lyr.selectedFeatures():
                       list.append(feature['{}'.format(self.additional_info_combo.currentText())])

                   annot.xy = (posx, posy)
                   text = f'{self.additional_info_combo.currentText()}: {list[0]} \n(x: {posx} - y: {posy})'
                   annot.set_text(text)
                   annot.get_bbox_patch().set_alpha(1)
               else:

                   annot.xy = (posx, posy)
                   text = f'(x: {posx} - y: {posy})'
                   annot.set_text(text)
                   annot.get_bbox_patch().set_alpha(1)

           def hover(event):
               vis = annot.get_visible()
               if event.inaxes in [self.sc.axes]:
                   for line in lines:
                       cont, ind = line.contains(event)
                       if cont:
                           update_annot(line, annot, ind['ind'][0])
                           annot.set_visible(True)
                           self.sc.figure.canvas.draw_idle()
                       else:
                           if vis:
                               annot.set_visible(False)
                               self.sc.figure.canvas.draw_idle()

           self.sc.figure.canvas.mpl_connect('motion_notify_event', hover)

       elif self.comboBox.currentText() == "Barplot":

           lines = [rect for rect in self.sc.axes.get_children() if isinstance(rect, matplotlib.patches.Rectangle)]

          # print(zip(leg.findobj(patches.Rectangle), lines))
           leg_to_fill = {}  # map legend objects to fill plots.

           for leg_obj, fill_plot in zip(leg.findobj(patches.Rectangle), ((lines))):
               leg_obj.set_picker(True)  # Enable picking on the legend object.
               leg_to_fill[leg_obj] = fill_plot


           def on_pick(event):
               leg_obj_orig = event.artist
               fill_plot_orig = leg_to_fill[leg_obj_orig]
               visible = not fill_plot_orig.get_visible()
               fill_plot_orig.set_visible(visible)
               leg_obj_orig.set_alpha(1.0 if visible else 0.2)
               self.sc.figure.canvas.draw()

           self.sc.figure.canvas.mpl_connect('pick_event', on_pick)





       if self.font_title_style.currentFont().style() == 1:
           fp = FontProperties(family=self.font_title_style.currentFont().family(), size=
           self.font_title_style.currentFont().pointSize(), weight=self.font_title_style.currentFont().weight() * 10, style='italic')
       else:
           fp = FontProperties(family=self.font_title_style.currentFont().family(), size=
           self.font_title_style.currentFont().pointSize(), weight=self.font_title_style.currentFont().weight() * 10)

       self.sc.figure.suptitle("{}".format(self.plot_title_line.text()), fontproperties=fp, color=self.font_title_color.color().name())


       if self.font_xlabel_style.currentFont().style() == 1:
           fp2 = FontProperties(family=self.font_xlabel_style.currentFont().family(), size=
           self.font_xlabel_style.currentFont().pointSize(), weight=self.font_xlabel_style.currentFont().weight() * 10, style='italic')
       else:
           fp2 = FontProperties(family=self.font_xlabel_style.currentFont().family(), size=
           self.font_xlabel_style.currentFont().pointSize(), weight=self.font_xlabel_style.currentFont().weight() * 10)

       self.sc.axes.set_xlabel("{}".format(self.x_axis_title.text()), fontproperties=fp2, color=self.font_xlabel_color.color().name())


       if self.font_ylabel_style.currentFont().style() == 1:
           fp3 = FontProperties(family=self.font_ylabel_style.currentFont().family(), size=
           self.font_ylabel_style.currentFont().pointSize(), weight=self.font_ylabel_style.currentFont().weight() * 10, style='italic')
       else:
           fp3 = FontProperties(family=self.font_ylabel_style.currentFont().family(), size=
           self.font_ylabel_style.currentFont().pointSize(), weight=self.font_ylabel_style.currentFont().weight() * 10)

       self.sc.axes.set_ylabel("{}".format(self.y_axis_title.text()), fontproperties=fp3, color=self.font_ylabel_color.color().name())


       if self.font_xticks_style.currentFont().style() == 1:
           fp4 = FontProperties(family=self.font_xticks_style.currentFont().family(), size=
           self.font_xticks_style.currentFont().pointSize(), weight=self.font_xticks_style.currentFont().weight() * 10, style='italic')
       else:
           fp4 = FontProperties(family=self.font_xticks_style.currentFont().family(), size=
           self.font_xticks_style.currentFont().pointSize(), weight=self.font_xticks_style.currentFont().weight() * 10)


       for label in self.sc.axes.get_xticklabels():
           label.set_fontproperties(fp4)
       self.sc.axes.tick_params(axis='x', colors=self.font_xticks_color.color().name())

       #self.sc.axes.xticks(fontproperties=fp4)

       if self.font_yticks_style.currentFont().style() == 1:
           fp5 = FontProperties(family=self.font_yticks_style.currentFont().family(), size=
           self.font_yticks_style.currentFont().pointSize(), weight=self.font_yticks_style.currentFont().weight() * 10, style='italic')
       else:
           fp5 = FontProperties(family=self.font_yticks_style.currentFont().family(), size=
           self.font_yticks_style.currentFont().pointSize(), weight=self.font_yticks_style.currentFont().weight() * 10)

       #self.sc.axes.yticks(fontproperties=fp5, color=self.font_yticks_color.color().name())
       for label in self.sc.axes.get_yticklabels():
           label.set_fontproperties(fp5)
       self.sc.axes.tick_params(axis='y', colors=self.font_yticks_color.color().name())


      # if self.selected_feature_check.checkState() != Qt.Checked:
        #   self.layer_combo.currentLayer().removeSelection()
       self.toolbar.setVisible(True)
       self.layout.addWidget(self.toolbar)
       self.layout.addWidget(self.sc)
       self.plot_view.setLayout(self.layout)
       self.tabWidget.setCurrentIndex(1)













