
# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys
import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
#matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"
from itertools import cycle, islice
import pyodbc

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import (
    QgsFields,
    QgsField,
    QgsFeature,
QgsLayerTreeGroup,
    QgsGeometry,
    QgsFeatureRequest,
    QgsProject,
QgsSimpleMarkerSymbolLayerBase,
    QgsRaster,
    QgsTask,
    QgsPrintLayout,
    QgsReadWriteContext,
    QgsRasterFileWriter,
    QgsMapLayer,
    QgsLayoutItemLegend,
    QgsLayoutItemMap,
    QgsLegendRenderer,
    QgsExpression,
    QgsLegendStyle,
    QgsLayoutExporter,
    QgsLayoutItemPicture,
    QgsLayoutPoint,
    QgsLayoutSize,
    QgsUnitTypes,
    QgsRasterBandStats,
    QgsCategorizedSymbolRenderer,
    QgsRendererCategory,
    QgsSymbol,
    QgsCoordinateTransformContext,
QgsRuleBasedRenderer,
    QgsRandomColorRamp,
    QgsMapThemeCollection,
    QgsLayerDefinition,
    QgsPointXY,
    QgsApplication,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsExpressionContextScope,
    QgsCoordinateReferenceSystem,
    QgsProcessingFeatureSourceDefinition,
    QgsRectangle,
    QgsTaskManager,
    QgsLayout,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsRasterLayer,
    QgsWkbTypes,
    QgsDataSourceUri
)
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()
import pyodbc

from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass


__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *
from ..doDataExtract import *


def PlottingTool_Kemi(self):
    if len(self.comboBox.currentText()) > 0:
        checked_plots = []
        for index in range(self.listWidget_3.count()):
            if self.listWidget_3.item(index).checkState() == Qt.Checked:
                checked_plots.append(self.listWidget_3.item(index).text())
        if len(checked_plots) > 0:
            if True:
                self.pushButton_GKOPLOTTER.setText("Kører")
                self.pushButton_GKOPLOTTER.setEnabled(False)
                self.repaint()
                QApplication.processEvents()

            nitratbaggrund = self.spinBox_4.value()
            sulfatbaggrund = self.spinBox_3.value()
            project = QgsProject.instance()
            key = self.comboBox.currentText()
            layer = project.mapLayersByName('{}'.format(key))[0]

            for kk, feature in enumerate(layer.getFeatures()):
                if kk == 0:
                    geom = feature.geometry()
                else:
                    geom = geom.combine(feature.geometry())

            crstrue = int("".join(filter(str.isdigit, str(layer.crs().authid()))))  # int(filter(str.isdigit, str(layer.crs().authid())))

            sti = ""

            def fetch_chemsamp(crs, data_type, geom):
                """

                :param srid:
                :return:
                """
                sql = f'''
                        WITH 
                            compounds AS 
                                (
                                    SELECT c.code::NUMERIC AS compoundno, c.longtext AS compound
                                    FROM jupiter.code c 
                                    WHERE c.codetype = 221	
                                        AND LOWER(longtext) = '{data_type}'
                                ),
                            chem_unit AS
                                (
                                    SELECT c.code::NUMERIC AS unit, c.longtext AS unit_descr
                                    FROM jupiter.code c 
                                    WHERE c.codetype = 752
                                )
                        SELECT DISTINCT ON (gcs.guid_intake) 
                            gcs.guid_intake, 
                            gcs.sampledate, 
                            CASE
                                WHEN gca.attribute LIKE '<%' 
                                    OR gca.attribute LIKE 'A%' 
                                    OR gca.attribute LIKE 'o%' 
                                    OR gca.attribute LIKE '0%' 
                                    THEN 0::NUMERIC
                                WHEN gca.attribute LIKE 'B%' 
                                    OR gca.attribute LIKE 'C%' 
                                    OR gca.attribute LIKE 'D%' 
                                    OR gca.attribute LIKE 'S%' 
                                    OR gca.attribute LIKE '!%' 
                                    OR gca.attribute LIKE '/%' 
                                    OR gca.attribute LIKE '*%' 
                                    OR gca.attribute LIKE '>%' 
                                    THEN -99::NUMERIC
                                ELSE gca.amount
                                END AS {data_type},
                            u.unit_descr,
                            concat(z.layer_num, '_', z.litho) AS fohm_layer,
                            b.purpose,
                            b.use,
            				s.top as filtertop,
                            concat(REPLACE(s.boreholeno, ' ', ''), '_', s.intakeno) AS dgu_indtag,
                            b.xutm32euref89,
                            b.yutm32euref89
                        FROM jupiter.grwchemanalysis gca
                        INNER JOIN jupiter.grwchemsample gcs USING (sampleid)
                        LEFT JOIN temp_simak.fohm_filter_lith_aquifer z USING (boreholeid) 
                        INNER JOIN jupiter.borehole b USING (boreholeid)
                        INNER JOIN jupiter.screen s USING (boreholeid)
                        INNER JOIN compounds USING (compoundno)
                        LEFT JOIN chem_unit u ON gca.unit = u.unit
                        WHERE COALESCE(gcs.watertype, 0) NOT IN (1,3,4,5)
                            AND COALESCE(upper(b.purpose), 'K') NOT IN ('L', 'VA')
                            AND ST_Intersects(ST_Transform(b.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
                        ORDER BY gcs.guid_intake, gcs.sampledate DESC
                        ;
                    '''
                print(sql)
                db = Database(database='JUPITER')
                df = db.sql_to_df(sql)

                return df

            def fetch_watertype(crs, geom):
                """

                :param srid:
                :return:
                """
                sql = f'''
                        WITH 
                        prøver AS (
                            SELECT *
                            FROM jupiter.borehole b
                            WHERE ST_Intersects(ST_Transform(b.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
                            ),
                        chemsampcrosstabjson AS
        (SELECT 	
			sampleid,
		   	jsonb_object_agg(compoundno, mængde ORDER BY compoundno) AS samples_json
		FROM (
		     	SELECT 
		     		gca.sampleid, 
		     		gca.compoundno, 
		     		CASE
						WHEN gca.attribute LIKE '<%' 
							OR gca.attribute LIKE 'A%' 
							OR gca.attribute LIKE 'o%' 
							OR gca.attribute LIKE '0%' 
							THEN 0::NUMERIC
						WHEN gca.attribute LIKE 'B%'
							OR gca.attribute LIKE 'C%'
							OR gca.attribute LIKE 'D%' 
							OR gca.attribute LIKE 'S%' 
							OR gca.attribute LIKE '!%' 
							OR gca.attribute LIKE '/%' 
							OR gca.attribute LIKE '*%' 
							OR gca.attribute LIKE '>%' 
							THEN -99::NUMERIC
						ELSE gca.amount
						END AS mængde
		        FROM jupiter.grwchemanalysis gca
		   ) as s
		   INNER JOIN jupiter.grwchemsample using (sampleid)
		   INNER JOIN prøver p using (boreholeid)
		GROUP BY sampleid
	),
	 inorganiccrosstab AS (
	 WITH json_to_crosstab AS 
				(
					SELECT 
						sampleid,
						(samples_json ->> '242')::NUMERIC AS Ammonium_N ,
						(samples_json ->> '280')::NUMERIC AS Calcium,
						(samples_json ->> '56')::NUMERIC AS Carbonat,
						(samples_json ->> '297')::NUMERIC AS Chlorid,
						(samples_json ->> '308')::NUMERIC AS Fluorid,
						(samples_json ->> '59')::NUMERIC AS Hydrogencarbonat,
						(samples_json ->> '312')::NUMERIC AS Jern,
						(samples_json ->> '317')::NUMERIC AS Kalium,
						(samples_json ->> '321')::NUMERIC AS Magnesium,
						(samples_json ->> '322')::NUMERIC AS Mangan,
						(samples_json ->> '356')::NUMERIC AS Methan,
						(samples_json ->> '324')::NUMERIC AS Natrium,
						(samples_json ->> '246')::NUMERIC AS Nitrat,
						(samples_json ->> '50')::NUMERIC AS Oxygen_indhold,
						(samples_json ->> '13')::NUMERIC AS pH,
						(samples_json ->> '335')::NUMERIC AS Sulfat
					FROM chemsampcrosstabjson
				)
			SELECT 
				t.*
			FROM json_to_crosstab AS t
		),
		
    chem_samp AS 
		(
			SELECT 	
				cs.sampleid,
				cs.guid_intake,
			 	concat(bh.boreholeno, '_', take.intakeno) AS DGU_Indtag,
				bh.boreholeno,
				take.stringno,
		        take.intakeno,
				cs.sampledate,
				cs.sampledate::DATE AS Dato,
				bh.use,
				bh.purpose,
				s.top AS dybde_filtertop,
				s.bottom AS dybde_filterbund,
				bh.elevation - s.top AS kote_filtertop,
				bh.elevation - s.bottom AS kote_filterbund,
				bh.xutm32euref89,
				bh.yutm32euref89,  
				bh.geom
			FROM jupiter.grwchemsample cs
			LEFT JOIN jupiter.intake take ON cs.guid_intake = take.guid 
			LEFT  JOIN jupiter.screen s ON cs.guid_intake = s.guid 
			INNER JOIN jupiter.borehole bh ON cs.guid_borehole = bh.guid 
			WHERE ST_Intersects(ST_Transform(bh.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
			ORDER BY Dato DESC
		),
                        
                        samples AS 
					(
						SELECT
							cs.*,
							ic.nitrat AS Nitrat_mgprl,		
							ic.jern AS Jern_mgprl,	
							ic.sulfat AS sulfat_mgprl,
							ic.oxygen_indhold AS Ilt_mgprl,
							ic.mangan AS mangan_mgprl,
							ic.ammonium_n AS ammonium_n_mgprl,
							CASE
					            WHEN 
						            ic.nitrat > 1
									AND ic.jern >= 0.2 
						            THEN 'X'::text
								WHEN 
						            ic.nitrat > 1 
						            AND ic.jern < 0.2
									AND ic.jern >=0
									AND (ic.oxygen_indhold IS NULL)-- OR ic.oxygen_indhold <= 0)
						            THEN 'AB'::text
								WHEN 
						            ic.nitrat > 1 
						            AND ic.jern < 0.2
									AND ic.jern >= 0
						            AND ic.oxygen_indhold < 1 
						            THEN 'B'::text
								WHEN 
						            ic.nitrat > 1 
						            AND ic.jern < 0.2
									AND ic.jern >= 0
						            AND ic.oxygen_indhold >= 1 
						            THEN 'A'::text
								WHEN
									ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern < 0.2
									AND ic.jern >= 0
									AND ic.oxygen_indhold >= 1
									AND ic.mangan < 0.01
									AND ic.mangan >= 0
									AND ic.ammonium_n < 0.01
									AND ic.ammonium_n >= 0
						            THEN 'AY'::text
					            WHEN 
						            ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern < 0.2
									AND ic.jern >= 0
						            THEN 'Y'::text
					            WHEN 
						            ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern >= 0.2
						            AND ic.sulfat < 20
									AND ic.sulfat >= 0
						            THEN 'D'::text
					            WHEN 
						            ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern >= 0.2
						            AND ic.sulfat >= 20 AND ic.sulfat < 70 
									THEN 'C1'::text
								WHEN
									ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern >= 0.2
						            AND ic.sulfat >= 70  
						            THEN 'C2'::text
					            ELSE NULL
								END AS vandtype
						FROM inorganiccrosstab ic
						INNER JOIN chem_samp cs USING (sampleid)
						WHERE ic.jern IS NOT NULL
							AND ic.nitrat IS NOT NULL
							AND ic.sulfat IS NOT NULL
							AND cs.geom IS NOT NULL
					),
				kemi as (	
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
)  
            SELECT DISTINCT ON (vt.guid_intake) 
                vt.guid_intake,
                concat(REPLACE(vt.boreholeno, ' ', ''), '_', vt.intakeno) AS dgu_indtag, 
                vt.sampledate, 
                vt.vandtype,
                concat(z.layer_num, '_', z.litho) AS fohm_layer,
                s.top as filtertop,
                b.purpose,
                b.use,
                b.xutm32euref89,
				b.yutm32euref89
            FROM kemi vt
            INNER JOIN jupiter.grwchemsample gcs USING (sampleid)
            INNER JOIN jupiter.grwchemanalysis gca USING (sampleid) 
            INNER JOIN jupiter.borehole b USING (boreholeid)
            INNER JOIN jupiter.screen s using (boreholeid)
            LEFT JOIN temp_simak.fohm_filter_lith_aquifer z ON gcs.boreholeid = z.boreholeid
            WHERE vt.vandtype IS NOT NULL
                AND COALESCE(gcs.watertype, 0) NOT IN (1,3,4,5)
                AND COALESCE(upper(b.purpose), 'K') NOT IN ('L', 'VA')
                AND ST_Intersects(ST_Transform(b.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
            ORDER BY vt.guid_intake, vt.sampledate DESC
            ;
                    '''

                db = Database(database='JUPITER')
                df = db.sql_to_df(sql)

                return df

            def fetch_sulfatnitrat(crs, geom):
                    """

                    :param srid:
                    :return:
                    """
                    sql_wl = f'''
         WITH 
        prøver AS (
            SELECT *
            FROM jupiter.borehole b
            WHERE ST_Intersects(ST_Transform(b.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
            ),
            
        chemsampcrosstabjson AS
        (SELECT 	
			sampleid,
		   	jsonb_object_agg(compoundno, mængde ORDER BY compoundno) AS samples_json
		FROM (
		     	SELECT 
		     		gca.sampleid, 
		     		gca.compoundno, 
		     		CASE
						WHEN gca.attribute LIKE '<%' 
							OR gca.attribute LIKE 'A%' 
							OR gca.attribute LIKE 'o%' 
							OR gca.attribute LIKE '0%' 
							THEN 0::NUMERIC
						WHEN gca.attribute LIKE 'B%'
							OR gca.attribute LIKE 'C%'
							OR gca.attribute LIKE 'D%' 
							OR gca.attribute LIKE 'S%' 
							OR gca.attribute LIKE '!%' 
							OR gca.attribute LIKE '/%' 
							OR gca.attribute LIKE '*%' 
							OR gca.attribute LIKE '>%' 
							THEN -99::NUMERIC
						ELSE gca.amount
						END AS mængde
		        FROM jupiter.grwchemanalysis gca
		   ) as s
		   INNER JOIN jupiter.grwchemsample using (sampleid)
		   INNER JOIN prøver p using (boreholeid)
		GROUP BY sampleid
	),
	 inorganiccrosstab AS (
	 WITH json_to_crosstab AS 
				(
					SELECT 
						sampleid,
						(samples_json ->> '242')::NUMERIC AS Ammonium_N ,
						(samples_json ->> '280')::NUMERIC AS Calcium,
						(samples_json ->> '56')::NUMERIC AS Carbonat,
						(samples_json ->> '297')::NUMERIC AS Chlorid,
						(samples_json ->> '308')::NUMERIC AS Fluorid,
						(samples_json ->> '59')::NUMERIC AS Hydrogencarbonat,
						(samples_json ->> '312')::NUMERIC AS Jern,
						(samples_json ->> '317')::NUMERIC AS Kalium,
						(samples_json ->> '321')::NUMERIC AS Magnesium,
						(samples_json ->> '322')::NUMERIC AS Mangan,
						(samples_json ->> '356')::NUMERIC AS Methan,
						(samples_json ->> '324')::NUMERIC AS Natrium,
						(samples_json ->> '246')::NUMERIC AS Nitrat,
						(samples_json ->> '13')::NUMERIC AS pH,
						(samples_json ->> '335')::NUMERIC AS Sulfat
					FROM chemsampcrosstabjson
				)
			SELECT 
				t.*
			FROM json_to_crosstab AS t
		),
		
    chem_samp AS 
		(
			SELECT 	
				cs.sampleid,
				cs.guid_intake,
			 	concat(bh.boreholeno, '_', take.intakeno) AS DGU_Indtag,
				bh.boreholeno,
				take.stringno,
		        take.intakeno,
				cs.sampledate,
				cs.sampledate::DATE AS Dato,
				bh.use,
				bh.purpose,
				s.top AS dybde_filtertop,
				s.bottom AS dybde_filterbund,
				bh.elevation - s.top AS kote_filtertop,
				bh.elevation - s.bottom AS kote_filterbund,
				bh.xutm32euref89,
				bh.yutm32euref89,  
				bh.geom
			FROM jupiter.grwchemsample cs
			LEFT JOIN jupiter.intake take ON cs.guid_intake = take.guid 
			LEFT  JOIN jupiter.screen s ON cs.guid_intake = s.guid 
			INNER JOIN jupiter.borehole bh ON cs.guid_borehole = bh.guid 
			WHERE ST_Intersects(ST_Transform(bh.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
			ORDER BY Dato DESC
		),

				samples AS 
					(
						SELECT
							cs.*,
							ic.nitrat AS nitrat_mgprl,	
							ic.sulfat AS sulfat_mgprl
						FROM inorganiccrosstab ic
						INNER JOIN chem_samp cs USING (sampleid)
						WHERE ic.nitrat IS NOT NULL
							AND ic.sulfat IS NOT NULL
							AND cs.geom IS NOT NULL
					),
				kemi as (	
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
)
        
        
                SELECT DISTINCT ON (ns.guid_intake) 
                ns.guid_intake, 
                concat(REPLACE(ns.boreholeno, ' ', ''), '_', ns.intakeno) AS dgu_indtag,
                ns.sampledate, 
                ns.nitrat_mgprl as nitrat,
                ns.sulfat_mgprl as sulfat,
                concat(z.layer_num, '_', z.litho) AS fohm_layer,
                s.top as filtertop,
                b.purpose,
                b.use,
                b.xutm32euref89,
				b.yutm32euref89
            FROM kemi ns
            INNER JOIN jupiter.grwchemsample gcs USING (sampleid)
            INNER JOIN jupiter.borehole b USING (boreholeid)
            INNER JOIN jupiter.screen s using (boreholeid)
            LEFT JOIN temp_simak.fohm_filter_lith_aquifer z ON gcs.boreholeid = z.boreholeid
            WHERE ns.nitrat_mgprl > 0
                AND ns.sulfat_mgprl > 0
                AND COALESCE(gcs.watertype, 0) NOT IN (1,3,4,5)
                AND COALESCE(upper(b.purpose), 'K') NOT IN ('L', 'VA')
                AND ST_Intersects(ST_Transform(b.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
            ORDER BY ns.guid_intake, ns.sampledate DESC
            ;
        '''

                    db = Database(database='JUPITER')
                    df_wl = db.sql_to_df(sql_wl)

                    return df_wl

            def fetch_sulfatchlorid(crs, geom):
                        """

                        :param srid:
                        :return:
                        """
                        sql_wl = f'''
        WITH 
        prøver AS (
            SELECT *
            FROM jupiter.borehole b
            WHERE ST_Intersects(ST_Transform(b.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
            ),
            
        chemsampcrosstabjson AS
        (SELECT 	
			sampleid,
		   	jsonb_object_agg(compoundno, mængde ORDER BY compoundno) AS samples_json
		FROM (
		     	SELECT 
		     		gca.sampleid, 
		     		gca.compoundno, 
		     		CASE
						WHEN gca.attribute LIKE '<%' 
							OR gca.attribute LIKE 'A%' 
							OR gca.attribute LIKE 'o%' 
							OR gca.attribute LIKE '0%' 
							THEN 0::NUMERIC
						WHEN gca.attribute LIKE 'B%'
							OR gca.attribute LIKE 'C%'
							OR gca.attribute LIKE 'D%' 
							OR gca.attribute LIKE 'S%' 
							OR gca.attribute LIKE '!%' 
							OR gca.attribute LIKE '/%' 
							OR gca.attribute LIKE '*%' 
							OR gca.attribute LIKE '>%' 
							THEN -99::NUMERIC
						ELSE gca.amount
						END AS mængde
		        FROM jupiter.grwchemanalysis gca
		   ) as s
		   INNER JOIN jupiter.grwchemsample using (sampleid)
		   INNER JOIN prøver p using (boreholeid)
		GROUP BY sampleid
	),
	 inorganiccrosstab AS (
	 WITH json_to_crosstab AS 
				(
					SELECT 
						sampleid,
						(samples_json ->> '242')::NUMERIC AS Ammonium_N ,
						(samples_json ->> '280')::NUMERIC AS Calcium,
						(samples_json ->> '56')::NUMERIC AS Carbonat,
						(samples_json ->> '297')::NUMERIC AS Chlorid,
						(samples_json ->> '308')::NUMERIC AS Fluorid,
						(samples_json ->> '59')::NUMERIC AS Hydrogencarbonat,
						(samples_json ->> '312')::NUMERIC AS Jern,
						(samples_json ->> '317')::NUMERIC AS Kalium,
						(samples_json ->> '321')::NUMERIC AS Magnesium,
						(samples_json ->> '322')::NUMERIC AS Mangan,
						(samples_json ->> '356')::NUMERIC AS Methan,
						(samples_json ->> '324')::NUMERIC AS Natrium,
						(samples_json ->> '246')::NUMERIC AS Nitrat,
						(samples_json ->> '13')::NUMERIC AS pH,
						(samples_json ->> '335')::NUMERIC AS Sulfat
					FROM chemsampcrosstabjson
				)
			SELECT 
				t.*
			FROM json_to_crosstab AS t
		),
		
    chem_samp AS 
		(
			SELECT 	
				cs.sampleid,
				cs.guid_intake,
			 	concat(bh.boreholeno, '_', take.intakeno) AS DGU_Indtag,
				bh.boreholeno,
				take.stringno,
		        take.intakeno,
				cs.sampledate,
				cs.sampledate::DATE AS Dato,
				bh.use,
				bh.purpose,
				s.top AS dybde_filtertop,
				s.bottom AS dybde_filterbund,
				bh.elevation - s.top AS kote_filtertop,
				bh.elevation - s.bottom AS kote_filterbund,
				bh.xutm32euref89,
				bh.yutm32euref89,  
				bh.geom
			FROM jupiter.grwchemsample cs
			LEFT JOIN jupiter.intake take ON cs.guid_intake = take.guid 
			LEFT  JOIN jupiter.screen s ON cs.guid_intake = s.guid 
			INNER JOIN jupiter.borehole bh ON cs.guid_borehole = bh.guid 
			WHERE ST_Intersects(ST_Transform(bh.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
			ORDER BY Dato DESC
		),

				samples AS 
					(
						SELECT
							cs.*,
							ic.chlorid AS klorid_mgprl,	
							ic.sulfat AS sulfat_mgprl
						FROM inorganiccrosstab ic
						INNER JOIN chem_samp cs USING (sampleid)
						WHERE ic.chlorid IS NOT NULL
							AND ic.sulfat IS NOT NULL
							AND cs.geom IS NOT NULL
					),
				kemi as (	
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
)
        
                SELECT DISTINCT ON (ns.guid_intake) 
                ns.guid_intake,                 
                concat(REPLACE(ns.boreholeno, ' ', ''), '_', ns.intakeno) AS dgu_indtag,
                ns.sampledate, 
                ns.klorid_mgprl as chlorid,
                ns.sulfat_mgprl as sulfat,
                concat(z.layer_num, '_', z.litho) AS fohm_layer,
                s.top as filtertop,
                b.purpose,
                b.use,
                b.xutm32euref89,
                b.yutm32euref89
            FROM kemi ns
            INNER JOIN jupiter.grwchemsample gcs USING (sampleid)
            INNER JOIN jupiter.borehole b USING (boreholeid)
            INNER JOIN jupiter.screen s using (boreholeid)
            LEFT JOIN temp_simak.fohm_filter_lith_aquifer z ON gcs.boreholeid = z.boreholeid
            WHERE ns.klorid_mgprl > 75
                AND ns.sulfat_mgprl > 20
                AND COALESCE(gcs.watertype, 0) NOT IN (1,3,4,5)
                AND ST_Intersects(ST_Transform(b.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
            ORDER BY ns.guid_intake, ns.sampledate DESC
            ;
        '''

                        db = Database(database='JUPITER')
                        df_wl = db.sql_to_df(sql_wl)

                        return df_wl


            if self.checkBox_13.checkState() == Qt.Checked and len(self.lineEdit_7.text()) == 0:
                    QMessageBox.warning(None, 'Fejl', "Vælg dit Excel-ark (jupiter extract)")
            else:
                try:
                    xls = pd.ExcelFile("{}".format(self.lineEdit_7.text()))
                    df = pd.read_excel(xls, 'Arbejdsdata')
                    df['dato'] = pd.to_datetime(df['dato'])
                    df = df.sort_values(['dgu_indtag', 'dato'])
                except:
                    pass
                if "Nitrat" in checked_plots:
                    if self.checkBox_13.checkState() == Qt.Checked and len(self.lineEdit_7.text()) > 0:
                        nitrat = df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                     'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin',
                                     'dybde_filtertop',
                                     'dybde_filterbund',
                                     'xutm32euref89', 'yutm32euref89', 'nitrat', 'nitrit', 'jern', 'methan',
                                     'ammoniak_ammonium', 'sulfat']][~df['nitrat'].isnull()].drop_duplicates(
                            subset=['dgu_indtag'], keep='last')
                    else:
                        nitrat = fetch_chemsamp(crstrue, "nitrat", geom)

                    dp_nitrat = DybdeplotNitratSulfat(geom.asWkt(), crstrue, "nitrat", sti, nitrat, nitratbaggrund, sulfatbaggrund)
                    dp_nitrat.plot_dybdeplot()

                if "Sulfat" in checked_plots:
                    if self.checkBox_13.checkState() == Qt.Checked and len(self.lineEdit_7.text()) > 0:
                        sulfat = df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                     'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin',
                                     'dybde_filtertop',
                                     'dybde_filterbund',
                                     'xutm32euref89', 'yutm32euref89', 'sulfat', 'nitrat', 'chlorid']][
                            ~df['sulfat'].isnull()].drop_duplicates(subset=['dgu_indtag'], keep='last')
                    else:
                        sulfat = fetch_chemsamp(crstrue,"sulfat", geom)

                    dp_sulfat = DybdeplotNitratSulfat(geom.asWkt(), crstrue, "sulfat", sti, sulfat, nitratbaggrund, sulfatbaggrund)
                    dp_sulfat.plot_dybdeplot()

                if "Chlorid" in checked_plots:
                    if self.checkBox_13.checkState() == Qt.Checked and len(self.lineEdit_7.text()) > 0:
                        chlorid = df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                     'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin',
                                     'dybde_filtertop', 'dybde_filterbund', 'xutm32euref89', 'yutm32euref89',
                                     'ionbalance', 'natrium', 'kalium', 'sulfat', 'nitrat', 'chlorid', 'ionexchange']][
                            ~df['chlorid'].isnull()].drop_duplicates(
                            subset=['dgu_indtag'], keep='last')
                    else:
                        chlorid = fetch_chemsamp(crstrue, "chlorid", geom)

                    dp_klorid = DybdeplotNitratSulfat(geom.asWkt(), crstrue, "chlorid", sti, chlorid, nitratbaggrund, sulfatbaggrund)
                    dp_klorid.plot_dybdeplot()
                if "Vandtype" in checked_plots:
                    if self.checkBox_13.checkState() == Qt.Checked and len(self.lineEdit_7.text()) > 0:
                        vandtype =  df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                      'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin',
                                      'dybde_filtertop', 'dybde_filterbund', 'xutm32euref89', 'yutm32euref89', 'Vandtype',
                                      'sulfat', 'nitrat', 'jern', 'oxygen_indhold', 'methan', 'ammonium_n']][
                                        ~df['Vandtype'].isnull()].drop_duplicates(subset=['dgu_indtag'], keep='last')

                    else:
                        vandtype = fetch_watertype(crstrue, geom)

                    dp_vandtype = DybdePlotVandType(geom.asWkt(),crstrue,sti,"vandtype", vandtype)
                    dp_vandtype.plot_dybdeplot()

                if "Nitrat-Sulfat" in checked_plots:
                    if self.checkBox_13.checkState() == Qt.Checked and len(self.lineEdit_7.text()) > 0:
                        nitratsulfat = df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                     'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin',
                                     'dybde_filtertop',
                                     'dybde_filterbund',
                                     'xutm32euref89', 'yutm32euref89', 'nitrat', 'nitrit', 'jern', 'methan',
                                     'ammoniak_ammonium', 'sulfat']][~df['nitrat'].isnull()].drop_duplicates(
                            subset=['dgu_indtag'], keep='last')
                    else:
                        nitratsulfat = fetch_sulfatnitrat(crstrue, geom)

                    sp_nitratsulfat = ScatterSulfatNitrat(geom.asWkt(),crstrue,sti,"nitratsulfat", nitratsulfat, nitratbaggrund, sulfatbaggrund)
                    sp_nitratsulfat.plot_sulfatnitrat()


                if "Sulfat-Chlorid" in checked_plots:
                    if self.checkBox_13.checkState() == Qt.Checked and len(self.lineEdit_7.text()) > 0:
                        sulfatchlorid = df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                     'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin',
                                     'dybde_filtertop',
                                     'dybde_filterbund',
                                     'xutm32euref89', 'yutm32euref89', 'sulfat', 'nitrat', 'chlorid']][
                            ~df['sulfat'].isnull()].drop_duplicates(subset=['dgu_indtag'], keep='last')
                    else:
                        sulfatchlorid = fetch_sulfatchlorid(crstrue, geom)

                    sp_sulfatchlorid = SulfatKlorid(geom.asWkt(),crstrue,sti,"nitratsulfat", sulfatchlorid, nitratbaggrund, sulfatbaggrund)
                    sp_sulfatchlorid.plot_sulfatklorid()


                if "Pesticider" in checked_plots:
                    def parse_geometry(geometry):
                        regex = r'[0-9-\.]+'
                        parsed_geom = re.findall(regex, geometry)
                        parsed_geom = [float(i) for i in parsed_geom]
                        return min(parsed_geom[::2]), max(parsed_geom[::2]), min(parsed_geom[1::2]), max(
                            parsed_geom[1::2])

                    boundingbox = parse_geometry(geom.asWkt())
                    boundingbox = list(boundingbox)

                    url = "pagingEnabled='true' preferCoordinatesForWfsT11='True' srsname='{}' typename='mc_grp_analyse' url='https://data.geus.dk/geusmap/ows/{}.jsp?whoami=makyn@mst.dk&LAYERS=mc_grp_analyse&mapname=grundvand&bbox={},{},{},{}&url='https://data.geus.dk/geusmap/ows/{}.jsp?whoami=makyn@mst.dk&LAYERS=mc_grp_analyse&version=1.0.0&mapname=grundvand&bbox={},{},{},{}'".format(
                        layer.crs().authid(), crstrue, boundingbox[0], boundingbox[2], boundingbox[1],
                        boundingbox[3], crstrue, boundingbox[0], boundingbox[2],
                        boundingbox[1], boundingbox[3])

                    vlayer = QgsVectorLayer(url, "Pesticider GEUS", "wfs")
                    vlayer.updateFields()

                    aaa = processing.run("native:clip", {'INPUT': vlayer,
                                                         'OVERLAY': layer, 'OUTPUT': 'TEMPORARY_OUTPUT'})
                    aaa["OUTPUT"].updateFields()

                    aa = processing.run("native:extractbyattribute", {'INPUT': aaa["OUTPUT"],
                                                                      'FIELD': 'stofgruppe', 'OPERATOR': 0,
                                                                      'VALUE': '50', 'OUTPUT': 'TEMPORARY_OUTPUT'})[
                        "OUTPUT"]

                    aa.updateFields()

                    cols = [f.name() for f in aa.fields()]
                    datagen = ([f[col] for col in cols] for f in aa.getFeatures())
                    dfpest = pd.DataFrame.from_records(data=datagen, columns=cols)

                    project.removeMapLayer(aa)
                    project.removeMapLayer(aaa["OUTPUT"])

                    if self.checkBox_13.checkState() == Qt.Checked and len(self.lineEdit_7.text()) > 0:
                        df["dgu2"] = df["dgu_indtag"].astype(str).str.strip()
                        df["dgu2"].astype(str).str.strip()
                        df["dgu2"] = df["dgu2"].str.replace(' ', '')

                        dfpest["dgu_nr"].astype(str)
                        dfpest["indtag_nr"].astype(str)
                        dfpest["dgu2"] = dfpest["dgu_nr"].astype(str) + "_" + dfpest["indtag_nr"].astype(str)
                        dfpest["dgu2"].astype(str).str.strip()
                        dfpest["dgu2"] = dfpest["dgu2"].str[:-2]

                        dfpest2 = pd.merge(dfpest, df[['dgu2', 'magasin', 'dybde_filtertop', 'use', 'purpose']], how='inner',
                                 on='dgu2')
                        dp_pest = DybdePesticider(geom.asWkt(), crstrue, sti, "pesticider", dfpest2)
                        dp_pest.plot_dybdeplot()
                    else:
                        dp_pest = DybdePesticider(geom.asWkt(),crstrue,sti,"pesticider", dfpest)
                        dp_pest.plot_dybdeplot()



            self.pushButton_GKOPLOTTER.setText("Generér Plots")
            self.pushButton_GKOPLOTTER.setEnabled(True)
            self.repaint()

            plt.show()
        else:
            QMessageBox.warning(None, 'Fejl', "Vælg dit output.")
    else:
        QMessageBox.warning(None, 'Fejl', "Intet input (områdepolygon).")


def PlottingTool_Hydro(self):
    if len(self.comboBox.currentText()) > 0:
        checked_plots_hydro = []
        for index in range(self.listWidget_4.count()):
            if self.listWidget_4.item(index).checkState() == Qt.Checked:
                checked_plots_hydro.append(self.listWidget_4.item(index).text())

        if len(checked_plots_hydro) > 0:
            if True:
                self.pushButton_GKOPLOTTER_Hydro.setText("Kører")
                self.pushButton_GKOPLOTTER_Hydro.setEnabled(False)
                self.repaint()
                QApplication.processEvents()


            project = QgsProject.instance()
            key = self.comboBox.currentText()
            layer = project.mapLayersByName('{}'.format(key))[0]

            for kk, feature in enumerate(layer.getFeatures()):
                if kk == 0:
                    geom = feature.geometry()
                else:
                    geom = geom.combine(feature.geometry())

            crstrue = int("".join(filter(str.isdigit, str(layer.crs().authid()))))

            print(checked_plots_hydro)

            if "Antal boringsindtag m. pejlinger" in checked_plots_hydro:
                class AntalBoringsindtagArbejde(QgsTask):
                    def __init__(self, desc, Dialog):
                        self.desc = desc
                        self.Dialog = Dialog
                        self.result_layer = None
                        QgsTask.__init__(self, self.desc)

                    def run(self):
                        data = Pejlinger(geom.asWkt(), crstrue).fetch_pejlinger()
                        data['year'] = data['year'].astype(int)

                        df = data.groupby(['year'])['guid_intake'].nunique()
                        self.result_layer = df

                    def finished(self, result):
                        if self.isCanceled():
                            QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                        else:
                            df = self.result_layer
                            fig4, ax4 = plt.subplots(figsize=(12, 7))

                            df.plot(kind='bar', stacked=True, color='#167dfa',ax = ax4, legend=None)
                            ax4.set_title("Antal boringsindtag m. pejlinger", fontweight='bold', fontsize=20)
                            ax4.set_ylabel("Antal boringsindtag", fontweight='bold', fontsize=12)
                            ax4.set_xlabel("")
                            ax4.grid(axis='y')

                            ax4.set_axisbelow(True)
                            ax4.set_xticklabels(ax4.get_xticklabels(), rotation=25, fontsize=9)
                            ax4.set_yticklabels(ax4.get_yticklabels(), fontsize=9)
                            ax4.get_yaxis().set_major_formatter(
                                matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
                            plt.show()

                        self.Dialog.pushButton_GKOPLOTTER_Hydro.setText("Generér Plots")
                        self.Dialog.pushButton_GKOPLOTTER_Hydro.setEnabled(True)
                        self.Dialog.repaint()

                globals()['taskantalboringstagmpejl'] = AntalBoringsindtagArbejde("Plotter antal boringsindtag m. pejlinger pr. år...", self)
                QgsApplication.taskManager().addTask(globals()['taskantalboringstagmpejl'])


            if "Antal pejlinger pr. år" in checked_plots_hydro:
                class AntalPejlingerArbejde(QgsTask):
                    def __init__(self, desc, Dialog):
                        self.desc = desc
                        self.Dialog = Dialog
                        self.result_layer = None
                        QgsTask.__init__(self, self.desc)
                    def run(self):
                        data = Pejlinger(geom.asWkt(), crstrue).fetch_pejlinger()
                        data['year'] = data['year'].astype(int)
                        if self.isCanceled():
                            return False
                        df = data.groupby(['year'])['watlevelid'].nunique()
                        self.result_layer = df
                        return True


                    def finished(self, result):
                        if self.isCanceled():
                            QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                        else:
                            fig5, ax5 = plt.subplots(figsize=(16, 7))

                            df = self.result_layer
                            df.plot(kind='bar', stacked=True, color='#167dfa', ax=ax5, legend=None)
                            ax5.set_title("Antal pejlinger pr. år", fontweight='bold', fontsize=20)
                            ax5.set_ylabel("Antal pejlinger", fontweight='bold', fontsize=12)
                            ax5.set_xlabel("")
                            ax5.grid(axis='y')

                            ax5.set_axisbelow(True)
                            ax5.set_xticklabels(ax5.get_xticklabels(), rotation=25, fontsize=9)
                            ax5.set_yticklabels(ax5.get_yticklabels(), fontsize=9)
                            ax5.get_yaxis().set_major_formatter(
                                matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
                            plt.show()

                        self.Dialog.pushButton_GKOPLOTTER_Hydro.setText("Generér Plots")
                        self.Dialog.pushButton_GKOPLOTTER_Hydro.setEnabled(True)
                        self.Dialog.repaint()

                globals()['taskantalpejlinger'] = AntalPejlingerArbejde("Plotter antal pejlinger pr. år...", self)
                QgsApplication.taskManager().addTask(globals()['taskantalpejlinger'])


            if "Nedbør, temperatur og fordampning" in checked_plots_hydro or "Nedbør" in checked_plots_hydro:
                class DMIArbejde(QgsTask):
                    def __init__(self, desc, Dialog, checked_plots_hydro):
                        self.desc = desc
                        self.Dialog = Dialog
                        self.result_layer = None
                        self.result_layer2 = None
                        self.result_layer3 = None
                        self.nedbørsgrid = None
                        self.tempsgrid = None
                        self.checked_plots_hydro = checked_plots_hydro
                        QgsTask.__init__(self, self.desc)

                    def run(self):
                        nedbørgrids = processing.run("native:extractbylocation", {
                            'INPUT': 'F:/GKO/data/Data til Grundvandsmodeller/01_Indput_data/Klima/DMI/Database og Grid til GV modeller og klimadata_ brug denne/Grid til data/10km.shp',
                            'PREDICATE': [0, 5, 6],
                            'INTERSECT': layer,
                            'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

                        nedbørgridsamlet = []
                        for feature in nedbørgrids.getFeatures():
                            layer_fields = nedbørgrids.fields()
                            idx = layer_fields.indexFromName("gridID")
                            print(feature.attributes()[idx])
                            nedbørgridsamlet.append(feature.attributes()[idx])

                        self.nedbørsgrid = nedbørgridsamlet
                        project.removeMapLayer(nedbørgrids)


                        tempgrids = processing.run("native:extractbylocation", {
                            'INPUT': 'F:/GKO/data/Data til Grundvandsmodeller/01_Indput_data/Klima/DMI/Database og Grid til GV modeller og klimadata_ brug denne/Grid til data/20km.shp',
                            'PREDICATE': [0, 5, 6],
                            'INTERSECT': layer,
                            'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

                        tempgridsamlet = []
                        for feature in tempgrids.getFeatures():
                            layer_fields = tempgrids.fields()
                            idx = layer_fields.indexFromName("gridID")
                            print(feature.attributes()[idx])
                            tempgridsamlet.append(feature.attributes()[idx])
                        self.tempsgrid = tempgridsamlet

                        project.removeMapLayer(tempgrids)

                        conn = pyodbc.connect(
                            r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=F:\GKO\data\Data til Grundvandsmodeller\01_Indput_data\Klima\DMI\Database og Grid til GV modeller og klimadata_ brug denne\Klima_01021989_08312022\Klima_1989_2020.mdb;')

                        query = "select GridID, Value_, Dato from Nedbor_1989_2018 WHERE GridID in ({})".format(
                            str(nedbørgridsamlet).replace('[', '').replace(']', ''))
                        if self.isCanceled():
                            return False

                        nedbør = pd.read_sql(query, conn)
                        if self.isCanceled():
                            return False
                        nedbør = nedbør[nedbør['GridID'].isin(nedbørgridsamlet)]
                        nedbør['Dato'] = pd.to_datetime(nedbør['Dato'], format='%Y%m%d')
                        nedbør['year'] = nedbør['Dato'].dt.year

                        self.result_layer = nedbør

                        if self.isCanceled():
                            return False

                        if "Nedbør, temperatur og fordampning" in self.checked_plots_hydro:

                            query2 = "select GridID, Value_, year from Temp_c_1989_2018 WHERE GridID in ({})".format(str(tempgridsamlet).replace('[', '').replace(']', ''))
                            temperatur = pd.read_sql(query2, conn)
                            if self.isCanceled():
                                return False

                            query3 = "select GridID, Value_, year from EP_mm_1989_2018 WHERE GridID in ({})".format(str(tempgridsamlet).replace('[', '').replace(']', ''))
                            fordampning = pd.read_sql(query3, conn)
                            if self.isCanceled():
                                return False
                            conn.close()
                            self.result_layer2 = temperatur
                            self.result_layer3 = fordampning

                        return True

                    def finished(self, result):
                        if self.isCanceled():
                            QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                        else:
                            nedbør = self.result_layer
                            temperatur = self.result_layer2
                            fordampning = self.result_layer3

                            if "Nedbør, temperatur og fordampning" in self.checked_plots_hydro:
                                gennemsnit_nedbør = nedbør.groupby(["year"])["Value_"].sum() / (len(self.nedbørsgrid))

                                temperatur = temperatur[temperatur['GridID'].isin(self.tempsgrid)]

                                gennemsnit_temperatur = temperatur.groupby(["år"])["Value_"].mean()

                                fordampning = fordampning[fordampning['GridID'].isin(self.tempsgrid)]

                                gennemsnit_fordampning = fordampning.groupby(["år"])["Value_"].sum() / (
                                    len(self.tempsgrid))

                                middel_fordampning = gennemsnit_fordampning.mean()
                                middel_nedbør = gennemsnit_nedbør.mean()

                                fig, ax = plt.subplots(figsize=(12, 7))
                                ax.yaxis.grid(zorder=0)

                                ax = gennemsnit_nedbør.plot.bar(label="Nedbør", ax=ax, color="#3ba0ff")

                                gennemsnit_fordampning.plot.bar(ax=ax, color="#fc4947", alpha=0.4,
                                                                label="Potentiel fordampning")  # kind='bar', edgecolor="red", alpha=0

                                ax1 = plt.gca()
                                ax1.yaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())
                                ax1.set_axisbelow(True)
                                ax1.yaxis.grid()

                                ax1.axhline(y=middel_fordampning, color='#f7312f', linestyle='--',
                                            label="Middel potentiel fordampning")
                                ax1.axhline(y=middel_nedbør, color='#0782f5', linestyle='--', label="Middel nedbør")

                                ax1.set_ylabel('Nedbør & fordampning (mm/år)', fontsize=12, fontweight='bold')

                                print(f"Middel nedbør: {round(middel_nedbør)}")
                                print(f"Middel fordampning: {round(middel_fordampning)}")



                               # textstr = '\n'.join((f"Middel nedbør: {round(middel_nedbør)}",
                               #                      f"Middel pot. fordampning: {round(middel_fordampning)}"))

                                #props = dict(boxstyle='round', facecolor='white', alpha=1)

                                # place a text box in upper left in axes coords
                                #ax1.text(0.80, 0.95, textstr, transform=ax.transAxes, fontsize=8, fontweight='bold',
                                #        verticalalignment='top', bbox=props)




                                ax2 = ax1.twinx()
                                ax2.set_ylim(0, 12)

                                ax2.plot(ax.get_xticks(), gennemsnit_temperatur.to_list(), color="green",
                                         label="Årlig middeltemperatur")

                                ax2.set_ylabel('Temperatur (°C)', fontsize=12, fontweight='bold')
                                ax1.set_xlabel('Tid (år)', fontsize=12, fontweight='bold', labelpad=20)
                                ax2.set_xlabel('Tid (år)', fontsize=12, fontweight='bold', labelpad=20)
                                plt.xticks(rotation=90)

                                h1, l1 = ax1.get_legend_handles_labels()
                                h2, l2 = ax2.get_legend_handles_labels()
                                handles = h1 + h2
                                labels = l1 + l2

                                # Shrink current axis's height by 10% on the bottom
                                box = ax1.get_position()
                                ax1.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])

                                # Arrangerer legenden med nedbør, fordampning og temperatur
                                order = [2, 1, 3, 0, 4]

                                ax1.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
                                           loc='upper center', bbox_to_anchor=(0.5, 1.1),
                                           fancybox=True, shadow=True, ncol=5)

                                fig_txt = 'Nedbørsgrid: {} \nTemperatur- & fordampningsgrid: {}'.format(
                                    str(self.nedbørsgrid).replace('[', '').replace(']', '').replace("'", ""),
                                    str(self.tempsgrid).replace('[', '').replace(']', '').replace("'", ""))

                                plt.figtext(0.01, 0.02, fig_txt, fontsize=6, weight='bold')
                                plt.show()

                            if "Nedbør" in self.checked_plots_hydro:
                                gennemsnit = nedbør.groupby(["year"])["Value_"].sum() / (len(self.nedbørsgrid))
                                gennemsnit2 = gennemsnit.groupby(["year"])

                                nedboer = nedbør.groupby(["GridID", "year"])["Value_"].sum()
                                nedboer2 = nedboer.groupby(["GridID"])

                                years = nedbør['year'].unique().tolist()
                                nedboer4 = np.convolve(gennemsnit, np.ones(5) / 5, mode='valid0')
                                nedboer5 = {'År': years[4:], 'Value': nedboer4}
                                nedboer6 = pd.DataFrame.from_dict(nedboer5)
                                nedboer6.index += 4

                                fig2, ax3 = plt.subplots(figsize=(12, 7))

                                nedboer2.plot(x="Tid (år)", y="Nedbør (mm/år)", ax=ax3, alpha=0.5, figsize=[12, 7])
                                ax3.plot(gennemsnit2, linewidth=3, c='black')
                                ax3.plot(nedboer6["Value"], linewidth=4, c='green')

                                ax3.legend(["Gennemsnitligt årsmiddelnedbør", "Glidende 5 års middel af nedbøren"])

                                ax = plt.gca()
                                leg = ax.get_legend()
                                leg.legendHandles[0].set_color('black')
                                leg.legendHandles[1].set_color('green')
                                leg.legendHandles[0].set_alpha(1)
                                leg.legendHandles[1].set_alpha(1)
                                lines = ax.get_lines()

                                lined = {}  # Will map legend lines to original lines.
                                for legline, origline in zip(leg.get_lines(), lines[len(self.nedbørsgrid) + 1:]):
                                    legline.set_picker(True)
                                    legline.set_pickradius(5)  # Enable picking on the legend line.
                                    lined[legline] = origline

                                def on_pick(event):
                                    legline = event.artist
                                    origline = lined[legline]
                                    visible = not origline.get_visible()
                                    origline.set_visible(visible)
                                    legline.set_alpha(1.0 if visible else 0.2)
                                    fig.canvas.draw()

                                fig2.canvas.mpl_connect('pick_event', on_pick)
                                ax.lines.remove(lines[len(self.nedbørsgrid)])

                                plt.xticks(ticks=[1, 6, 11, 16, 21, 26, 31],
                                           labels=["1990", "1995", "2000", "2005", "2010", "2015", "2020"])
                                print(gennemsnit2)
                                ax.set_xlim([11, 32])
                                ax.set_ylim([400, 1400])

                                plt.ylabel('Nedbør (mm/år)', fontsize=12, fontweight='bold')
                                plt.xlabel('Tid (år)', fontsize=12, fontweight='bold')

                                plt.title("Nedbør", fontweight='bold', fontsize=22)
                                ax.grid(alpha=0.6, axis='y')

                                fig_txt = 'Nedbørsgrid: {}'.format(
                                    str(self.nedbørsgrid).replace('[', '').replace(']', '').replace("'", ""))

                                plt.figtext(0.01, 0.01, fig_txt, fontsize=6, weight='bold')

                                plt.show()
                        self.Dialog.pushButton_GKOPLOTTER_Hydro.setText("Generér Plots")
                        self.Dialog.pushButton_GKOPLOTTER_Hydro.setEnabled(True)
                        self.Dialog.repaint()

                globals()['taskdmi'] = DMIArbejde("Plotter klimadata...", self, checked_plots_hydro)
                QgsApplication.taskManager().addTask(globals()['taskdmi'])

            else:
                pass

            del checked_plots_hydro
        else:
            QMessageBox.warning(None, 'Fejl', "Vælg dit output.")
    else:
        QMessageBox.warning(None, 'Fejl', "Intet input (områdepolygon).")


def PlottingTool_Indvinding(self):

    if len(self.comboBox.currentText()) > 0:
        checked_types = []
        for index in range(self.listWidget_5.count()):
            if self.listWidget_5.item(index).checkState() == Qt.Checked:
                checked_types.append(self.listWidget_5.item(index).text())

        if len(checked_types) > 0:
            print(checked_types)
            if True:
                self.pushButton_GKOPLOTTER_Indvinding.setText("Kører")
                self.pushButton_GKOPLOTTER_Indvinding.setEnabled(False)
                self.repaint()
                QApplication.processEvents()


            project = QgsProject.instance()
            key = self.comboBox.currentText()
            layer = project.mapLayersByName('{}'.format(key))[0]

            for kk, feature in enumerate(layer.getFeatures()):
                if kk == 0:
                    geom = feature.geometry()
                else:
                    geom = geom.combine(feature.geometry())

            crstrue = int("".join(filter(str.isdigit, str(layer.crs().authid()))))

            class ÅrligIndvinding(QgsTask):
                def __init__(self, desc, Dialog, check):
                    self.desc = desc
                    self.Dialog = Dialog
                    self.result_layer = None
                    self.indvindingstyper = None
                    self.checked_types = check

                    QgsTask.__init__(self, self.desc)

                def run(self):

                    data = Vandindvinding(geom.asWkt(), crstrue).fetch_indvinding()

                    indvindingstyper_orig = {'Vandværker': ['V01', 'V02'],
                                        'Husholdninger': ['V03', 'VO4', 'VO5', 'VO6', 'V07', 'V95'],
                                        'Erhverv, institutioner, hotel/camping, sportsplads & havevanding': ['V80','V30','V70','V41','V42'],
                                        'Dambrug': ['V60'],
                                        'Markvanding & gartneri': ['V40', 'V50', 'V52', 'V53'],
                                        'Husdyr & mælkeleverandør': ['V85', 'V88']}
                    indvindingstyper = {}
                    for key in indvindingstyper_orig:
                        test_list = [''.join(key)]
                        if (any(x in test_list for x in self.checked_types)):
                            indvindingstyper["{}".format(test_list[0])] =indvindingstyper_orig.get("{}".format(test_list[0]))
                        else:
                            pass

                    self.indvindingstyper = indvindingstyper

                    frames = []

                    for key, value in indvindingstyper.items():
                        df = data[data["companytype"].isin(value)]
                        df["label"] = key
                        # dataandet = df.groupby(['years'])['plantamount'].sum()
                        frames.append(df.groupby(['years'])['plantamount'].sum())

                    datasamlet = pd.concat(frames, axis=1)

                    self.result_layer = datasamlet


                def finished(self, result):
                    if self.isCanceled():
                        QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                    else:
                        datasamlet = self.result_layer

                        indvindingstyper_orig_colors = {'Vandværker': '#167dfa',
                                                        'Husholdninger': '#f7a436',
                                                        'Erhverv, institutioner, hotel/camping, sportsplads & havevanding': '#f75736',
                                                        'Dambrug': '#bd81fc',
                                                        'Markvanding & gartneri': '#22944a',
                                                        'Husdyr & mælkeleverandør': '#75ffdd'}
                        indvindingstyper_colors = {}

                        for key in indvindingstyper_orig_colors:
                            test_list = [''.join(key)]
                            if (any(x in test_list for x in self.checked_types)):
                                indvindingstyper_colors["{}".format(test_list[0])] = indvindingstyper_orig_colors.get(
                                    "{}".format(test_list[0]))
                            else:
                                pass

                        my_colors = []
                        for i in indvindingstyper_colors.values():
                            my_colors.append(i)

                        fig12, ax12 = plt.subplots(figsize=(14, 7))

                        datasamlet.plot(kind='bar', stacked=True, color=my_colors, ax=ax12)
                        ax12.set_title("Grundvandsindvinding", fontweight='bold', fontsize=20)
                        ax12.set_ylabel("Grundvandsindvinding (m³/år)", fontweight='bold', fontsize=12)
                        ax12.set_xlabel("")
                        plt.legend(list(self.indvindingstyper.keys()))
                        ax12.grid(axis='y')

                        ax12.set_axisbelow(True)
                        ax12.set_xticklabels(ax12.get_xticklabels(), rotation=25, fontsize=9)
                        ax12.set_yticklabels(ax12.get_yticklabels(), fontsize=9)
                        ax12.get_yaxis().set_major_formatter(
                            matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))

                        plt.show()

                    self.Dialog.pushButton_GKOPLOTTER_Indvinding.setText("Generér Plots")
                    self.Dialog.pushButton_GKOPLOTTER_Indvinding.setEnabled(True)
                    self.Dialog.repaint()

            globals()['taskårligindvinding'] = ÅrligIndvinding("Plotter årlig indvinding...", self, checked_types)
            QgsApplication.taskManager().addTask(globals()['taskårligindvinding'])

            del checked_types
        else:
            QMessageBox.warning(None, 'Fejl', "Vælg dit output.")
    else:
        QMessageBox.warning(None, 'Fejl', "Intet input (områdepolygon).")



