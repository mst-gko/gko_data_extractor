# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys
import matplotlib
from scipy.interpolate import UnivariateSpline
from scipy.signal import savgol_filter

matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
#matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import (
    QgsFields,
    QgsField,
    QgsFeature,
QgsLayerTreeGroup,
    QgsGeometry,
    QgsFeatureRequest,
    QgsProject,
QgsSimpleMarkerSymbolLayerBase,
    QgsRaster,
    QgsTask,
    QgsPrintLayout,
    QgsReadWriteContext,
    QgsRasterFileWriter,
    QgsMapLayer,
    QgsLayoutItemLegend,
    QgsLayoutItemMap,
    QgsLegendRenderer,
    QgsExpression,
    QgsLegendStyle,
    QgsLayoutExporter,
    QgsLayoutItemPicture,
    QgsLayoutPoint,
    QgsLayoutSize,
    QgsUnitTypes,
    QgsRasterBandStats,
    QgsCategorizedSymbolRenderer,
    QgsRendererCategory,
    QgsSymbol,
    QgsCoordinateTransformContext,
QgsRuleBasedRenderer,
    QgsRandomColorRamp,
    QgsMapThemeCollection,
    QgsLayerDefinition,
    QgsPointXY,
    QgsApplication,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsExpressionContextScope,
    QgsCoordinateReferenceSystem,
    QgsProcessingFeatureSourceDefinition,
    QgsRectangle,
    QgsTaskManager,
    QgsLayout,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsRasterLayer,
    QgsWkbTypes,
    QgsDataSourceUri
)
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()
import pyodbc

from PyQt5.QtWidgets import QMessageBox
from qgis.PyQt.QtCore import pyqtSignal, Qt
from qgis.PyQt.QtGui import QColor, QCursor
from qgis.PyQt.QtWidgets import QApplication, QTreeWidgetItemIterator

from qgis.PyQt import QtGui, uic, QtCore

import qgis
from qgis.core import QgsGeometry, QgsPointXY, QgsRectangle, Qgis, \
    QgsWkbTypes, QgsCoordinateReferenceSystem, QgsCoordinateTransform, \
    QgsProject, QgsLineString
from qgis.gui import QgsMapToolEmitPoint, QgsRubberBand, QgsMapTool, QgsVertexMarker



from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass

__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'


class Database():
    """Class handles all database related function"""
    def __init__(self, database):
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/reader/reader.ini'
        self.database = database
        self.usr_read, self.pw_read, self.host, self.port, self.database = self.parse_db_credentials()

    def parse_db_credentials(self):
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.database}READER']['userid']
        pw_read = config[f'{self.database}READER']['password']
        host = config[f'{self.database}READER']['host']
        port = config[f'{self.database}READER']['port']
        dbname = config[f'{self.database}READER']['databasename']

        return usr_read, pw_read, host, port, dbname

    def connect_to_pg_db(self):
        try:
            pg_con = pg.connect(
                host=self.host,
                port=self.port,
                database=self.database,
                user=self.usr_read,
                password=self.pw_read
            )
        except Exception as e:
            print(e)
            raise ConnectionError('Could not connect to database')
        else:
            print('Connected to database: ' + self.database)

        return pg_con

    def sql_to_df(self, sql):
        try:
            with self.connect_to_pg_db() as con:
                df = pd.read_sql(sql=sql, con=con)
        except Exception as e:
            print(e)
            df = None

        return df


class Database_matrikel():
    """Class handles all database related function"""
    def __init__(self, database_name, usr):
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.ini_database_name = database_name.upper()

    def parse_db_credentials(self):
        """
        fetches db credentials from ini file
        :return: db credentials
        """
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.ini_database_name}']['userid']
        pw_read = config[f'{self.ini_database_name}']['password']
        host = config[f'{self.ini_database_name}']['host']
        port = config[f'{self.ini_database_name}']['port']
        dbname = config[f'{self.ini_database_name}']['databasename']

        credentials = dict()
        credentials['usr'] = usr_read
        credentials['pw'] = pw_read
        credentials['host'] = host
        credentials['port'] = port
        credentials['dbname'] = dbname

        return credentials

    def connect_to_mssql_db(self):
        cred = self.parse_db_credentials()
        database = cred['dbname']
        try:
           # mssql_conn = pymssql.connect(cred['host'], cred['usr'], cred['pw'], cred['dbname'])
            mssql_conn = pyodbc.connect('DRIVER={SQL Server};'+'SERVER={};DATABASE={};UID={};PWD={}'.format(cred['host'], cred['dbname'], cred['usr'], cred['pw']))

            del cred
        except Exception as e:
            print(e)
            raise ValueError(f'Unable to connect to mssql database: {database}')
        else:
            return mssql_conn, database

    def import_mssql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query for the import
        :return: pandas dataframe containing output from sql query
        """

        conn, database = self.connect_to_mssql_db()
        try:
            df_mssql = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n{e}')
            raise ValueError(f'Unable to fetch dataframe from database: {database}')
        else:
            return df_mssql


class DybdePesticider:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""

    def __init__(self,  shpgeom, crs, sti, data_type_val, df):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.shpgeom = shpgeom
        # self.df2 = df2
        self.crs = crs
        self.sti = sti
        self.data_type = data_type_val
        self.df = df

    def plot_dybdeplot(self):
        df_wl = self.df

        if 'magasin' in df_wl.columns:
            lokalfil = 1
            df_wl['magasin'] = df_wl['magasin'].replace('', 'Ukendt lag')
            df_wl.sort_values(['magasin'], ascending=[True], inplace=True)
        else:
            lokalfil = 0
            df_wl['dgu2'] = df_wl['dgu_nr'].astype(str) +"_"+ df_wl["indtag_nr"].astype(int).astype(str)
            df_wl['purpose'] = df_wl['boringsformaal'].astype(str)
            df_wl['use'] = df_wl['boringsanvendelse'].astype(str)

            def flag_df(df):

                if "Kvar" in df['indtags_bjergart']:
                    return 'Kvartært magasin'
                elif "Gli" in df['indtags_bjergart']:
                    return 'Prækvartært magasin'
                elif "Ikk" in df['indtags_bjergart']:
                    return 'Prækvartært magasin'
                elif "Danien" in df['indtags_bjergart']:
                    return 'Prækvartært magasin'
                elif "Skrive" in df['indtags_bjergart']:
                    return 'Prækvartært magasin'
                else:
                    return 'Andet/Ukendt magasin'

            df_wl['magasin'] = df_wl.apply(flag_df, axis=1)

           # df_wl['magasin'] = df_wl['indtags_bjergart'].str.apply(lambda x: 'Prækvartært magasin' if 'Gli' in x else 'Andet/Ukendt magasin')
          #  df_wl['magasin'] = df_wl['indtags_bjergart'].str.apply(lambda x: 'Prækvartært magasin' if 'Ikk' in x else 'Andet/Ukendt magasin')
          #  df_wl['magasin'] = df_wl['indtags_bjergart'].str.apply(lambda x: 'Prækvartært magasin' if 'Danien' in x else 'Andet/Ukendt magasin')
         #   df_wl['magasin'] = df_wl['indtags_bjergart'].str.apply(lambda x: 'Prækvartært magasin' if 'Skrive' in x else 'Andet/Ukendt magasin')


        if lokalfil == 1:
            pass
        else:
            df_wl['dybde_filtertop'] = df_wl['indtag_top_dybde']

        df_wl = df_wl[df_wl['dybde_filtertop'] > 0]

        df_wl_grp = df_wl.groupby(['magasin'], sort=False)

        fig, ax = plt.subplots(figsize=[16, 8])

        n = sum(df_wl_grp["magasin"].nunique())

        color = iter(cm.rainbow(np.linspace(0, 1, n)))

        y = [1, 2, 3, 4, 5]
        x = ['Intet nu, intet tidligere', 'Intet nu, men tidl fund', 'Aktuelt fund u. grv.', 'Fund u. grv. Før over', 'Aktuelt over grv.']
        sentinel, = ax.plot(['Intet nu, intet tidligere', 'Intet nu, men tidl fund', 'Aktuelt fund u. grv.', 'Fund u. grv. Før over', 'Aktuelt over grv.'], np.linspace(min(y), max(y), len(x)))
        sentinel.remove()

        for name, grp in df_wl_grp:
                c = next(color)
                line, = ax.plot(grp['status_indtag_forklaret'].astype(str), grp['dybde_filtertop'].astype(int), marker='o',
                                linestyle='', label=name, c=c)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        lines = ax.get_lines()
        leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5))
        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), lines):
            legline.set_picker(True)
            legline.set_pickradius(5)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            legline = event.artist
            origline = lined[legline]
            visible = not origline.get_visible()
            origline.set_visible(visible)
            legline.set_alpha(1.0 if visible else 0.2)
            fig.canvas.draw()

        plt.xticks \
            (['Intet nu, intet tidligere', 'Intet nu, men tidl fund', 'Aktuelt fund u. grv.', 'Fund u. grv. Før over', 'Aktuelt over grv.'], ['Intet nu, intet tidligere', 'Intet nu, men tidl fund', 'Aktuelt fund u. grv.', 'Fund u. grv. Før over', 'Aktuelt over grv.'],
                   fontsize=10, fontweight='bold')
        plt.title('Pesticider - Status \n', fontweight='bold', fontsize=14)
        plt.grid(visible=None, which='both', axis='both')
        ax.margins(y=0.05)
        ax.margins(x=0.05)
        plt.ylabel('Filtertop (mut)', fontweight='bold', fontsize=12)
        plt.gca().invert_yaxis()
        ax.tick_params(labelbottom=False, labeltop=True)
        ax.xaxis.set_ticks_position('top')
        ax.tick_params(labelbottom=False, labeltop=True)

        fig.canvas.mpl_connect('pick_event', on_pick)
        annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        def update_annot(line, annot, ind):
            posx, posy = [line.get_xdata()[ind], line.get_ydata()[ind]]

            gid = df_wl.loc[(df_wl['status_indtag_forklaret'].astype(str) == posx) &
                        (df_wl['dybde_filtertop'] == posy), 'dgu2'].iloc[0]
            gid2 = df_wl.loc[(df_wl['status_indtag_forklaret'].astype(str) == posx) & (
                        df_wl['dybde_filtertop'] == posy), 'purpose'].iloc[0]
            gid3 = df_wl.loc[(df_wl['status_indtag_forklaret'].astype(str) == posx) & (
                        df_wl['dybde_filtertop'] == posy), 'use'].iloc[0]

            annot.xycoords = line.axes.transData

            z = 0
            if posx == 'Intet nu, men tidl fund':
                z = 1
            if posx == 'Aktuelt fund u. grv.':
                z = 2
            if posx == 'Fund u. grv. Før over':
                z = 3
            if posx == 'Aktuelt over grv.':
                z = 4

            annot.xy = (z, posy)  # posx
            text = f'{gid} ({gid2} - {gid3})'
            annot.set_text(text)
            annot.get_bbox_patch().set_alpha(1)

        def hover(event):
            vis = annot.get_visible()
            if event.inaxes in [ax]:
                for line in lines:
                    cont, ind = line.contains(event)
                    if cont:
                        update_annot(line, annot, ind['ind'][0])
                        annot.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            annot.set_visible(False)
                            fig.canvas.draw_idle()

        fig.canvas.mpl_connect('motion_notify_event', hover)

        # plt.savefig(str(self.sti) + "/DybdeplotPesticider.png", dpi=300)
        plt.show()


class DybdePlotVandType:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""

    def __init__(self, shpgeom, crs, sti, data_type_val, df):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.shpgeom = shpgeom
        self.crs = crs
        self.sti = sti
        self.data_type = data_type_val
        self.df = df

    def plot_dybdeplot(self):
        df_wl = self.df
        print(df_wl)
        try:
            df_wl['Vandtype'] = df_wl['vandtype']
        except:
            pass

        try:
            df_wl['dybde_filtertop'] = df_wl['filtertop']
        except:
            pass

        if 'magasin' in df_wl.columns:
            df_wl['length'] = df_wl['magasin'].str.len()
            df_wl['magasin'] = df_wl['magasin'].replace('', 'Ukendt magasin')
        else:
            df_wl['length'] = df_wl['fohm_layer'].str.len()
            df_wl['magasin'] = df_wl['fohm_layer'].replace('_', 'Ukendt lag')


        df_wl['length'] = df_wl['magasin'].str.len()
        # df_wl['magasin'] = df_wl['magasin'].replace('', 'Ukendt lag')
        df_wl.sort_values(['length', 'magasin', 'Vandtype'], ascending=[True, True, True], inplace=True)
        df_wl.dropna(subset=['Vandtype'], inplace=True)
        df_wl_grp = df_wl.groupby(['magasin'], sort=False)

        fig, ax = plt.subplots(figsize=[12, 8])

        n = sum(df_wl_grp["magasin"].nunique())

        color = iter(cm.rainbow(np.linspace(0, 1, n)))
        y = [1, 2, 3, 4, 5, 6, 7]
        x = ['A', 'AB', 'B', 'C2', 'C1', 'D', 'X', 'Y']
        sentinel, = ax.plot(['A', 'AB', 'B', 'C2', 'C1', 'D', 'X', 'Y'], np.linspace(min(y), max(y), len(x)))
        sentinel.remove()

        for name, grp in df_wl_grp:
            c = next(color)
            line, = ax.plot(grp['Vandtype'].astype(str), grp['dybde_filtertop'], marker='o',
                            linestyle='', label=name, c=c)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        lines = ax.get_lines()
        leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5))
        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), lines):
            legline.set_picker(True)
            legline.set_pickradius(5)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            legline = event.artist
            origline = lined[legline]
            visible = not origline.get_visible()
            origline.set_visible(visible)
            legline.set_alpha(1.0 if visible else 0.2)
            fig.canvas.draw()

        plt.xticks(['A', 'AB', 'B', 'C2', 'C1', 'D', 'X', 'Y'], ['A', 'AB', 'B', 'C2', 'C1', 'D', 'X', 'Y'],
                   fontsize=10, fontweight='bold')
        plt.title('Vandtype', fontweight='bold', fontsize=12)
        plt.grid(visible=None, which='both', axis='both')
        ax.margins(y=0.05)
        ax.margins(x=0.05)
        plt.ylabel('Filtertop (mut)', fontweight='bold', fontsize=12)
        plt.gca().invert_yaxis()
        ax.tick_params(labelbottom=False, labeltop=True)
        ax.xaxis.set_ticks_position('top')
        ax.tick_params(labelbottom=False, labeltop=True)

        fig.canvas.mpl_connect('pick_event', on_pick)
        annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        def update_annot(line, annot, ind):
            posx, posy = [line.get_xdata()[ind], line.get_ydata()[ind]]

            gid = \
            df_wl.loc[(df_wl['Vandtype'].astype(str) == posx) & (df_wl['dybde_filtertop'] == posy), 'dgu_indtag'].iloc[
                0]
            gid2 = \
            df_wl.loc[(df_wl['Vandtype'].astype(str) == posx) & (df_wl['dybde_filtertop'] == posy), 'purpose'].iloc[0]
            gid3 = df_wl.loc[(df_wl['Vandtype'].astype(str) == posx) & (df_wl['dybde_filtertop'] == posy), 'use'].iloc[
                0]
            annot.xycoords = line.axes.transData
            z = 0
            if posx == 'AB':
                z = 1
            if posx == 'B':
                z = 2
            if posx == 'C2':
                z = 3
            if posx == 'C1':
                z = 4
            if posx == 'D':
                z = 5
            if posx == 'X':
                z = 6
            if posx == 'Y':
                z = 7
            annot.xy = (z, posy)  # posx
            text = f'{gid} ({gid2} - {gid3})'
            annot.set_text(text)
            annot.get_bbox_patch().set_alpha(1)

        def hover(event):
            vis = annot.get_visible()
            if event.inaxes in [ax]:
                for line in lines:
                    cont, ind = line.contains(event)
                    if cont:
                        update_annot(line, annot, ind['ind'][0])
                        annot.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            annot.set_visible(False)
                            fig.canvas.draw_idle()

        fig.canvas.mpl_connect('motion_notify_event', hover)

        plt.show()
        # plt.savefig(str(self.sti) + "/DybdeplotVandtype.png", dpi=300)


class ScatterSulfatNitrat:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""

    def __init__(self, shpgeom, crs, sti, data_type_val, df, nitratbaggrund, sulfatbaggrund):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.shpgeom = shpgeom
        self.crs = crs
        self.sti = sti
        self.data_type = data_type_val
        self.df = df
        self.nitratbaggrund = nitratbaggrund
        self.sulfatbaggrund = sulfatbaggrund

    def plot_sulfatnitrat(self):
        """

        :return:
        """
        df_wl = self.df

        try:
            df_wl['dybde_filtertop'] = df_wl['filtertop']
        except:
            pass
        if 'magasin' in df_wl.columns:
            df_wl['length'] = df_wl['magasin'].str.len()
            df_wl['magasin'] = df_wl['magasin'].replace('', 'Ukendt magasin')
        else:
            df_wl['length'] = df_wl['fohm_layer'].str.len()
            df_wl['magasin'] = df_wl['fohm_layer'].replace('_', 'Ukendt lag')



        df_wl.sort_values(['length', 'magasin'], ascending=[True, True], inplace=True)
        df_wl_grp = df_wl.groupby(['magasin'], sort=False)

        fig, ax = plt.subplots(figsize=[12, 8])

        n = sum(df_wl_grp["magasin"].nunique())
        color = iter(cm.rainbow(np.linspace(0, 1, n)))

        for name, grp in df_wl_grp:
            c = next(color)
            line, = ax.plot(grp['sulfat'].astype(float), grp['nitrat'].astype(float), marker='o',
                            linestyle='', label=name, c=c)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        lines = ax.get_lines()
        leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5))
        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), lines):
            legline.set_picker(True)
            legline.set_pickradius(5)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            legline = event.artist
            origline = lined[legline]
            visible = not origline.get_visible()
            origline.set_visible(visible)
            legline.set_alpha(1.0 if visible else 0.2)
            fig.canvas.draw()

        plt.title('Scatterplot - Sulfat/Nitrat', fontweight='bold', fontsize=12)

        plt.grid(visible=None, which='both', axis='both')
        ax.margins(y=0.05)
        ax.margins(x=0.05)
        plt.xlim([0, max(df_wl['sulfat']) + 10])

        plt.text(.05, .05, "I", fontsize=11, fontweight='bold', color='#380000', ha='right', va='top',
                 transform=ax.transAxes
                 )
        plt.text(.95, .05, "II", fontsize=11, fontweight='bold', color='#380000', ha='right', va='top',
                 transform=ax.transAxes
                 )
        plt.text(.95, .95, "III", fontsize=11, fontweight='bold', color='#380000', ha='right', va='top',
                 transform=ax.transAxes
                 )
        plt.text(.05, .95, "IV", fontsize=11, fontweight='bold', color='#380000', ha='right', va='top',
                 transform=ax.transAxes
                 )
        plt.ylabel('Nitrat [mg/L]', fontweight='bold', fontsize=12)
        plt.xlabel('Sulfat [mg/L]', fontweight='bold', fontsize=12)
        fig.canvas.mpl_connect('pick_event', on_pick)

        ax.set_yscale('log')
        from matplotlib.ticker import ScalarFormatter
        for axis in [ax.yaxis]:
            axis.set_major_formatter(ScalarFormatter())


        z = plt.axvline(x=self.sulfatbaggrund, color='r', linestyle='-')
        z2 = plt.axhline(y=self.nitratbaggrund, color='r', linestyle='-')


        annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        def update_annot(line, annot, ind):
            posx, posy = [line.get_xdata()[ind], line.get_ydata()[ind]]

            gid = df_wl.loc[
                (df_wl['sulfat'] == posx) & (df_wl['nitrat'] == posy), 'dgu_indtag'].iloc[0]
            gid2 = \
                df_wl.loc[(df_wl['sulfat'] == posx) & (df_wl['nitrat'] == posy), 'purpose'].iloc[
                    0]
            gid3 = \
                df_wl.loc[(df_wl['sulfat'] == posx) & (df_wl['nitrat'] == posy), 'use'].iloc[0]

            annot.xycoords = line.axes.transData
            annot.xy = (posx, posy)
            text = f'{gid} ({gid2} - {gid3})'
            annot.set_text(text)
            annot.get_bbox_patch().set_alpha(1)

        def hover(event):
            vis = annot.get_visible()
            if event.inaxes in [ax]:
                for line in lines:
                    cont, ind = line.contains(event)
                    if cont:
                        update_annot(line, annot, ind['ind'][0])
                        annot.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            annot.set_visible(False)
                            fig.canvas.draw_idle()

        fig.canvas.mpl_connect('motion_notify_event', hover)

        plt.show()


class SulfatKlorid:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""

    def __init__(self, shpgeom, crs, sti, data_type_val, df, nitratbaggrund, sulfatbaggrund):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.shpgeom = shpgeom
        self.crs = crs
        self.sti = sti
        self.data_type = data_type_val
        self.df = df
        self.nitratbaggrund = nitratbaggrund
        self.sulfatbaggrund = sulfatbaggrund

    def plot_sulfatklorid(self):
        """

        :return:
        """
        df_wl = self.df

        try:
            df_wl['dybde_filtertop'] = df_wl['filtertop']
        except:
            pass
        if 'magasin' in df_wl.columns:
            df_wl['length'] = df_wl['magasin'].str.len()
            df_wl['magasin'] = df_wl['magasin'].replace('', 'Ukendt magasin')
        else:
            df_wl['length'] = df_wl['fohm_layer'].str.len()
            df_wl['magasin'] = df_wl['fohm_layer'].replace('_', 'Ukendt lag')

        df_wl.sort_values(['length', 'magasin'], ascending=[True, True], inplace=True)


        df_wl_grp = df_wl.groupby(['magasin'], sort=False)


        fig, ax = plt.subplots(figsize=[12, 8])

        n = sum(df_wl_grp["magasin"].nunique())
        color = iter(cm.rainbow(np.linspace(0, 1, n)))

        for name, grp in df_wl_grp:
            c = next(color)
            line, = ax.plot(grp['chlorid'].astype(float), grp['sulfat'].astype(float), marker='o',
                            linestyle='', label=name, c=c)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        lines = ax.get_lines()
        leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5))
        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), lines):
            legline.set_picker(True)
            legline.set_pickradius(5)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            legline = event.artist
            origline = lined[legline]
            visible = not origline.get_visible()
            origline.set_visible(visible)
            legline.set_alpha(1.0 if visible else 0.2)
            fig.canvas.draw()

        plt.title('Sulfat/Klorid', fontweight='bold', fontsize=12)
        plt.grid(visible=None, which='both', axis='both')
        plt.xlim(-3, (max(df_wl['chlorid'].astype(float)) + 5))
        plt.ylim(-3, (max(df_wl['sulfat'].astype(float)) + 5))
        plt.axline((0, 0), slope=1 / 7, color="blue", linestyle=(0, (5, 5)))

        plt.ylabel('Sulfat [mg/L]', fontweight='bold', fontsize=12)
        plt.xlabel('Klorid [mg/L]', fontweight='bold', fontsize=12)
        # plt.savefig(str(self.sti) + "/SulfatChlorid.png", dpi=300)

        fig.canvas.mpl_connect('pick_event', on_pick)
        annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        def update_annot(line, annot, ind):
            posx, posy = [line.get_xdata()[ind], line.get_ydata()[ind]]

            gid = df_wl.loc[
                (df_wl['chlorid'] == posx) & (df_wl['sulfat'] == posy), 'dgu_indtag'].iloc[0]
            gid2 = \
                df_wl.loc[(df_wl['chlorid'] == posx) & (df_wl['sulfat'] == posy), 'purpose'].iloc[
                    0]
            gid3 = \
                df_wl.loc[(df_wl['chlorid'] == posx) & (df_wl['sulfat'] == posy), 'use'].iloc[0]

            annot.xycoords = line.axes.transData
            annot.xy = (posx, posy)
            text = f'{gid} ({gid2} - {gid3})'
            annot.set_text(text)
            annot.get_bbox_patch().set_alpha(1)

        def hover(event):
            vis = annot.get_visible()
            if event.inaxes in [ax]:
                for line in lines:
                    cont, ind = line.contains(event)
                    if cont:
                        update_annot(line, annot, ind['ind'][0])
                        annot.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            annot.set_visible(False)
                            fig.canvas.draw_idle()

        fig.canvas.mpl_connect('motion_notify_event', hover)

        plt.show()


class DybdeplotNitratSulfat:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""

    def __init__(self, shpgeom, crs, data_type_val, sti, df, nitratbaggrund, sulfatbaggrund):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.shpgeom = shpgeom
        self.crs = crs
        self.sti = sti
        self.data_type = data_type_val
        self.df = df
        self.nitratbaggrund = nitratbaggrund
        self.sulfatbaggrund = sulfatbaggrund

    def plot_dybdeplot(self):
        """

        :return:
        """
        df_wl = self.df
        if self.data_type == 'nitrat':
            df_wl['amount_edit'] = df_wl['nitrat']
        if self.data_type == 'chlorid':
            df_wl['amount_edit'] = df_wl['chlorid']
        if self.data_type == 'sulfat':
            df_wl['amount_edit'] = df_wl['sulfat']
        df_wl = df_wl.drop(df_wl.index[df_wl['amount_edit'] < 0])
        try:
            df_wl['dybde_filtertop'] = df_wl['filtertop']
        except:
            pass
        if 'magasin' in df_wl.columns:
            df_wl['length'] = df_wl['magasin'].str.len()
            df_wl['magasin'] = df_wl['magasin'].replace('', 'Ukendt magasin')
        else:
            df_wl['length'] = df_wl['fohm_layer'].str.len()
            df_wl['magasin'] = df_wl['fohm_layer'].replace('_', 'Ukendt lag')

        df_wl.sort_values(['length', 'magasin'], ascending=[True, True], inplace=True)
        df_wl_grp = df_wl.groupby(['magasin'], sort=False)


        from matplotlib import pyplot as plt, patches, rcParams, cm
        import numpy as np

        fig, ax = plt.subplots(figsize=[12, 8])

        n = sum(df_wl_grp["magasin"].nunique())
        color = iter(cm.rainbow(np.linspace(0, 1, n)))

        for name, grp in df_wl_grp:
            c = next(color)
            line, = ax.plot(grp['amount_edit'].astype(float), grp['dybde_filtertop'].astype(float), marker='o',
                            linestyle='', label=name, c=c)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        lines = ax.get_lines()
        leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5))
        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), lines):
            legline.set_picker(True)
            legline.set_pickradius(5)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            legline = event.artist
            origline = lined[legline]
            visible = not origline.get_visible()
            origline.set_visible(visible)
            legline.set_alpha(1.0 if visible else 0.2)
            fig.canvas.draw()



        if self.data_type == 'nitrat':
            plt.axvline(x=self.nitratbaggrund, color='r', linestyle='-')
        if self.data_type == 'sulfat':
            plt.axvline(x=self.sulfatbaggrund, color='r', linestyle='-')
        if self.data_type == 'chlorid':
            plt.axvline(x=250, color='r', linestyle='-')

        plt.title(f'{self.data_type.capitalize()} [mg/L]', fontweight='bold', fontsize=12)
        plt.grid(visible=None, which='both', axis='both')
        ax.margins(y=0.05)

        ax.margins(x=0.05)
        plt.ylabel('Filtertop (mut)', fontweight='bold', fontsize=12)
        plt.gca().invert_yaxis()
        ax.tick_params(labelbottom=False, labeltop=True)
        ax.xaxis.set_ticks_position('top')
        ax.tick_params(labelbottom=False, labeltop=True)
        fig.canvas.mpl_connect('pick_event', on_pick)
        annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        def update_annot(line, annot, ind):
            posx, posy = [line.get_xdata()[ind], line.get_ydata()[ind]]

            gid = df_wl.loc[
                (df_wl['amount_edit'] == posx) & (df_wl['dybde_filtertop'] == posy), 'dgu_indtag'].iloc[0]
            gid2 = \
                df_wl.loc[(df_wl['amount_edit'] == posx) & (df_wl['dybde_filtertop'] == posy), 'purpose'].iloc[
                    0]
            gid3 = \
                df_wl.loc[(df_wl['amount_edit'] == posx) & (df_wl['dybde_filtertop'] == posy), 'use'].iloc[0]

            annot.xycoords = line.axes.transData
            annot.xy = (posx, posy)
            text = f'{gid} ({gid2} - {gid3})'
            annot.set_text(text)
            annot.get_bbox_patch().set_alpha(1)

        def hover(event):
            vis = annot.get_visible()
            if event.inaxes in [ax]:
                for line in lines:
                    cont, ind = line.contains(event)
                    if cont:
                        update_annot(line, annot, ind['ind'][0])
                        annot.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            annot.set_visible(False)
                            fig.canvas.draw_idle()

        fig.canvas.mpl_connect('motion_notify_event', hover)

        plt.show()


class Waterlevel():
    """class handles the plotting of waterlevel data from pcjupiterxl"""

    def __init__(self, select, wkt, crs, fra, til, mindybde, minantal, maksz, fjernsløjfede, kendttop, kendtbund):
        self.select = select
        self.wkt = wkt
        self.crs = crs
        self.fra = fra
        self.til = til
        self.mindybde = mindybde
        self.minantal = minantal
        self.maksz = maksz
        self.fjernsløjfede = fjernsløjfede
        self.kendttop = kendttop
        self.kendtbund = kendtbund

    def fetch_waterlevel(self, srid=25832):
        sql_wl = f'''
            {self.select} 
                ROW_NUMBER() over () AS rownumber,
                wl.guid_intake, 
                concat(REPLACE(wl.boreholeno, ' ', ''), '_', wl.intakeno) AS dgu_intake, 
                wl.timeofmeas AS pejledato,
                al.antal,
                b.purpose,
                b.use,
                b.drilldepth,
                wl.quality,
                b.ELEVAQUALI,
                b.locatmetho,
                b.LOCATQUALI,
                s.top,
                s.bottom,
                wl.watlevelid,
                wl.situation,
                EXTRACT(YEAR FROM wl.timeofmeas) as Year,
                wl.watlevmsl AS watlevmsl,
                concat(z.layer_num, '_', z.litho) AS fohm_layer,
                b.XUTM32EUREF89,
                b.YUTM32EUREF89,
                bhq.quality_litho_position as boringskvalitet,
                CONCAT(bhq.quality_litho_position, ': ', bhq.quality_litho_position_descr) as boringskvalitet_beskrivelse,
               -- lcq.quality_waterchemistry_complete as vandkemikvalitet,
                --CONCAT(lcq.quality_waterchemistry_complete, ': ', lcq.quality_waterchemistry_complete_descr) as vandkemikvalitet_beskrivelse,
                b.geom
            FROM jupiter.watlevel wl
            INNER JOIN jupiter.borehole b USING (boreholeid)
            LEFT JOIN mstjupiter.waterlevel_count al ON b.boreholeno = al.boreholeno
            --LEFT JOIN temp_makyn.limited_chemistry_quality lcq on wl.boreholeid = lcq.boreholeid
            LEFT JOIN temp_simak.fohm_filter_lith_aquifer z ON b.boreholeid = z.boreholeid
            LEFT JOIN mstjupiter.borehole_quality bhq ON b.boreholeid = bhq.boreholeid
            LEFT JOIN jupiter.screen s ON b.boreholeid = s.boreholeid
            WHERE wl.watlevmsl IS NOT NULL
                AND wl.watlevmsl < 500
                AND wl.watlevmsl > -500
                AND COALESCE(wl.situation, 0) NOT IN (1)
                AND ST_Intersects(ST_Transform(b.geom, {self.crs}), 'SRID={self.crs};{self.wkt}')
                AND wl.timeofmeas >= '{self.fra}'
                AND wl.timeofmeas <=  '{self.til}'
                AND al.antal >= {self.minantal}
                {self.mindybde}
                {self.maksz}
                {self.fjernsløjfede}
                {self.kendttop}
                {self.kendtbund}
            ORDER BY guid_intake, timeofmeas

        '''
        print(sql_wl)
        return sql_wl


class Kemi():
    """class handles the plotting of waterlevel data from pcjupiterxl"""

    def __init__(self, table_name, wkt, crs):
        self.table_name = table_name
        self.wkt = wkt
        self.crs = crs

    def fetch_kemi(self, srid=25832):
        sql_kemi = f'''
            SELECT
            ROW_NUMBER() over () AS rownumber,
                t.*,
                bhq.quality_litho_position as boringskvalitet,
                CONCAT(bhq.quality_litho_position, ': ', bhq.quality_litho_position_descr) as boringskvalitet_beskrivelse
                --,
               -- qa.is_approved
                --lcq.quality_waterchemistry_complete as vandkemikvalitet,
                --CONCAT(lcq.quality_waterchemistry_complete, ': ', lcq.quality_waterchemistry_complete_descr) as vandkemikvalitet_beskrivelse
            FROM mstjupiter.{self.table_name} t
            LEFT JOIN mstjupiter.borehole_quality bhq ON t.boreholeno = bhq.boreholeno
            --LEFT JOIN jupiter.grwchemsample g ON t.boreholeno = g.boreholeno 
           -- LEFT JOIN jupiter.grwchemanalysis grw ON g.sampleid  = grw.sampleid 
          --  LEFT JOIN dataquality.qa_grwchemanalysis qa ON grw.guid_grwchemanalysis = qa.guid_grwchemanalysis
            --LEFT JOIN temp_makyn.limited_chemistry_quality lcq on t.guid_intake = lcq.guid_intake
            WHERE ST_Intersects(ST_Transform(t.geom, {self.crs}), 'SRID={self.crs};{self.wkt}')
        '''
        return sql_kemi


class GRUKOS():
    """class handles the plotting of waterlevel data from pcjupiterxl"""

    def __init__(self, table_name, wkt, crs):
        self.table_name = table_name
        self.wkt = wkt
        self.crs = crs

    def fetch_grukos(self, srid=25832):
        sql_grukos = f'''
            SELECT
            ROW_NUMBER() over () AS rownumber,
                t.*
            FROM grukos.{self.table_name} t
            WHERE ST_Intersects(ST_Transform(t.geom, {self.crs}), 'SRID={self.crs};{self.wkt}')
        '''
        return sql_grukos

    def get_styleqml(self):
        tb = self.table_name[self.table_name.find('.') + 1:]

        sql = "SELECT styleqml FROM public.layer_styles WHERE stylename = '{}' AND f_table_schema = 'gvkort'".format(
            str(tb))

        return sql


class Matrikler():
    """class handles the plotting of waterlevel data from pcjupiterxl"""

    def __init__(self, wkt, crs):
        self.wkt = wkt
        self.crs = crs

    def fetch_sql(self, srid=25832):
        sql_lois = f'''
        WITH 
            geoms AS
                (
                    SELECT geometry::STGeomFromText('{self.wkt}', {self.crs}) AS geom
                )
            SELECT DISTINCT 
                ROW_NUMBER() OVER(ORDER BY j.Matrikelnummer) AS rownumber,
                j.Matrikelnummer,
                j.BFEnummer,
                ev.KomEjdNr,
                j.Kommunekode AS matr_kommunenr,
                VEJ_NAVN AS matr_vejnavn, 
                HUS_NR AS matr_hus_nr,
                SupBynavn AS matr_supbynavn, 
                PostNr AS matr_postnr, 
                PostByNavn AS matr_postbynavn, 
                VejKode AS matr_vejkode, 
                KomEjerlavKode AS komEjerlavKode_matr, 
                LandsejerlavKode AS landsejerlavKode_matr, 
                ev.EJER_STATUS_KODE_T,
                ev.EJER_NAVN,
                ev.EJER_CONAVN,
                ev.EJER_ADR,
                ev.EJER_UDV_ADR,
                ev.EJER_POSTADR, 
                ev.EJER_ADR_BESKYT_T, 
                ev.EJERFORHOLD_KODE_T, 
                cvr.navn_tekst AS NAVN_CVR,
                cvr.virksomhed_cvrnr AS CVR,
                cvr.telefonnummer_kontaktoplysning AS TLF_CVR,
                j.Geometri.STAsText() AS geom_wkt
            FROM OIS_CFK.MATRIKEL.JordstykkeGeoView j
            INNER JOIN geoms ON j.Geometri.STIntersects(geoms.geom) = 1
            LEFT JOIN OIS_CFK.BBR.EjendomsrelationView erv ON j.BFEnummer = erv.BFEnummer 
            LEFT JOIN OIS_CFK.dbo.ESREjerView ev ON erv.KommuneKode = ev.KOMMUNE_NR AND ev.EJD_NR = erv.Ejendomsnummer 
            LEFT JOIN OIS_CFK.dbo.CO42000T matr_adr ON RIGHT(matr_adr.komkode, 3) = erv.KommuneKode
            LEFT JOIN CVR_PROD_ENHGeoView cvr ON cvr.navn_tekst = j.PrimaerKontakt_Navn
                AND j.PrimaerKontakt_Vejnavn = cvr.beliggenhedsadresse_vejnavn
                AND j.PrimaerKontakt_HusNrTal BETWEEN cvr.belig_adresse_husnummerFra AND COALESCE(cvr.belig_adresse_husnummerTil, cvr.belig_adresse_husnummerFra)
                AND j.PrimaerKontakt_Postnr  = cvr.beliggenhedsadresse_postnr
                AND j.PrimaerKontakt_PostDistrikt = cvr.belig_adresse_postdistrikt
                AND matr_adr.esrejdnr = erv.Ejendomsnummer

        '''
        print(sql_lois)
        return sql_lois


class Vandindvinding:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""

    def __init__(self,  shpgeom, crs, ):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.shpgeom = shpgeom
        self.crs = crs


    def fetch_indvinding(self):
        """

        :param srid:
        :return:
        """
        sql_wl = f'''
         WITH 
	-- aarstal: udtraekker tidsserie med aarstal fra 1990 til aaret foer nu
	aarstal AS 
		(
			SELECT generate_series(1990, (EXTRACT(YEAR FROM now()))::integer - 1) AS years
		),
	-- anlaegs_info: udtrækker information vedr. anlaeg
	anlaegs_info AS 
		(
			SELECT 
				p.plantid,
				p.plantname, 
				ct.COMPANYTYPE, 
				p.MUNICIPALITYNO2007,
				p.active, 
				m."name" AS kommune_navn,
				COALESCE(XUTM32EUREF89, XUTM) AS X, 
				COALESCE(YUTM32EUREF89, YUTM) AS Y,
				ST_SetSRID(ST_MakePoint(COALESCE(XUTM32EUREF89, XUTM), COALESCE(YUTM32EUREF89,YUTM)), 25832) AS geom
			FROM jupiter.DRWPLANT p
			LEFT JOIN jupiter.DRWPLANTCOMPANYTYPE ct ON ct.PLANTID = p.PLANTID
			LEFT JOIN jupiter.MUNICIPALITY2007 m ON m.MUNICIPALITYNO2007 = p.MUNICIPALITYNO2007
		),
	-- plant_info_og_aarstal: kobler aarstal og anlaegs_info så hvert anlaeg har alle aarstal fra 1990 og frem
	plant_info_og_aarstal AS 
		(
			SELECT *
			FROM aarstal, anlaegs_info 
		),
	-- indvinding_per_anlaeg: Udtraekker højest mulige aktuelle indvindingsmaengde for hvert anlaeg per aar
	indvinding_per_anlaeg AS 
		(
				SELECT 
					PLANTID, 
					EXTRACT(YEAR FROM STARTDATE) AS years, 
					MAX(AMOUNT) AS AMOUNT 
				FROM jupiter.WRRCATCHMENT
				GROUP BY EXTRACT(YEAR FROM STARTDATE), PLANTID
		),
	-- sum_indtagsinvinding_per_anlaeg: Udtraekker den aktuelle indvindingsmaengde paa indtagsniveau per aar summeret for hvert anlaeg
	sum_indtagsinvinding_per_anlaeg AS 
		(
			SELECT 
				EXTRACT(YEAR FROM INTAKECATCHMENT.ENDDATE) AS years, 
				SUM(INTAKECATCHMENT.VOLUME) AS VOLUME, 
				DRWPLANTINTAKE.PLANTID 
			FROM jupiter.intakecatchment
			LEFT JOIN jupiter.DRWPLANTINTAKE 
				ON INTAKECATCHMENT.INTAKEPLANTID = DRWPLANTINTAKE.INTAKEPLANTID
			GROUP BY EXTRACT(YEAR FROM INTAKECATCHMENT.ENDDATE), DRWPLANTINTAKE.PLANTID
		),
	-- combined_indvinding_anlaeg_og_indtag: Udtraekker samler aktuel indving for hhv. anlaeg og indtag og finder den største af de to
	combined_indvinding_anlaeg_og_indtag AS 
		(
			SELECT 
				COALESCE(plant.PLANTID, intake.PLANTID) AS PLANTID, 
				COALESCE(plant.years, intake.years) AS years, 
				plant.AMOUNT AS PLANTAMOUNT, 
				intake.VOLUME AS INTAKEAMOUNT,
				GREATEST(plant.AMOUNT, intake.VOLUME) AS MAXAMOUNT
			FROM indvinding_per_anlaeg AS plant
			FULL OUTER JOIN sum_indtagsinvinding_per_anlaeg AS intake 
				ON intake.PLANTID = plant.PLANTID 
				AND plant.years = intake.years
		),
	-- tilladelser_per_anlaeg: udtraek en tilladelse per anlaeg og aarstal
	tilladelser_per_anlaeg AS 
		(
			SELECT DISTINCT ON (plantid, years)
				cp.plantid, 
				t.years, 
				cp.amountperyear,
				cp.permissionid,
				cp.startdate,
				cp.enddate
			FROM jupiter.catchperm cp
			LEFT JOIN aarstal t ON years >= EXTRACT(YEAR FROM startdate)
				AND years <= EXTRACT(YEAR FROM COALESCE(enddate, now()))
			ORDER BY cp.plantid, 
				t.years,
				(CASE WHEN COALESCE(cp.amountperyear, 0) > 0 THEN 1 ELSE 2 END), -- prioriter tilladelser der ikke er 0 eller null
				(CASE WHEN EXTRACT(YEAR FROM cp.enddate) - EXTRACT(YEAR FROM cp.startdate) <= 30 THEN 1 ELSE 2 END), -- prioriter tilladelser der løber i max 30 år
				cp.startdate DESC NULLS LAST, 
				cp.enddate DESC NULLS LAST, 
				cp.permissionid DESC NULLS LAST
		)

SELECT
	t1.plantid, 
	t1.years, 
	t1.plantname, 
	t1.companytype,
	t1.active,
	t1.kommune_navn, 
	t2.plantamount, 
	t2.intakeamount,
	t3.amountperyear,
	t3.startdate,
	t3.enddate,
	t1.x, 
	t1.y, 
	t1.geom 
FROM plant_info_og_aarstal t1
LEFT JOIN combined_indvinding_anlaeg_og_indtag t2 USING (plantid, years)
LEFT JOIN tilladelser_per_anlaeg t3 USING (plantid, years)
WHERE plantamount IS NOT NULL
	AND years >= 1990
	AND ST_Intersects(t1.geom, 'SRID={self.crs};{self.shpgeom}')
	ORDER BY plantid, plantname, years DESC

            ;
        '''
        db = Database(database='JUPITER')
        df_wl = db.sql_to_df(sql_wl)

        return df_wl


class Indvindingsoversigt:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""

    def __init__(self, shpgeom, crs):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.shpgeom = shpgeom
        self.crs = crs

    def fetch_indvindingsoversigt(self):
        """

        :param srid:
        :return:
        """
        sql_wl = f'''
         WITH 
	-- aarstal: udtraekker tidsserie med aarstal fra 1990 til aaret foer nu
	aarstal AS 
		(
			SELECT generate_series(1990, (EXTRACT(YEAR FROM now()))::integer - 1) AS years
		),
	-- anlaegs_info: udtrækker information vedr. anlaeg
	anlaegs_info AS 
		(
			SELECT 
				p.plantid,
				p.plantname, 
				ct.COMPANYTYPE, 
				p.MUNICIPALITYNO2007,
				p.active, 
				m."name" AS kommune_navn,
				COALESCE(XUTM32EUREF89, XUTM) AS X, 
				COALESCE(YUTM32EUREF89, YUTM) AS Y,
				ST_SetSRID(ST_MakePoint(COALESCE(XUTM32EUREF89, XUTM), COALESCE(YUTM32EUREF89,YUTM)), 25832) AS geom
			FROM jupiter.DRWPLANT p
			LEFT JOIN jupiter.DRWPLANTCOMPANYTYPE ct ON ct.PLANTID = p.PLANTID
			LEFT JOIN jupiter.MUNICIPALITY2007 m ON m.MUNICIPALITYNO2007 = p.MUNICIPALITYNO2007
		),
	-- plant_info_og_aarstal: kobler aarstal og anlaegs_info så hvert anlaeg har alle aarstal fra 1990 og frem
	plant_info_og_aarstal AS 
		(
			SELECT *
			FROM aarstal, anlaegs_info 
		),
	-- indvinding_per_anlaeg: Udtraekker højest mulige aktuelle indvindingsmaengde for hvert anlaeg per aar
	indvinding_per_anlaeg AS 
		(
				SELECT 
					PLANTID, 
					EXTRACT(YEAR FROM STARTDATE) AS years, 
					MAX(AMOUNT) AS AMOUNT 
				FROM jupiter.WRRCATCHMENT
				GROUP BY EXTRACT(YEAR FROM STARTDATE), PLANTID
		),
	-- sum_indtagsinvinding_per_anlaeg: Udtraekker den aktuelle indvindingsmaengde paa indtagsniveau per aar summeret for hvert anlaeg
	sum_indtagsinvinding_per_anlaeg AS 
		(
			SELECT 
				EXTRACT(YEAR FROM INTAKECATCHMENT.ENDDATE) AS years, 
				SUM(INTAKECATCHMENT.VOLUME) AS VOLUME, 
				DRWPLANTINTAKE.PLANTID 
			FROM jupiter.intakecatchment
			LEFT JOIN jupiter.DRWPLANTINTAKE 
				ON INTAKECATCHMENT.INTAKEPLANTID = DRWPLANTINTAKE.INTAKEPLANTID
			GROUP BY EXTRACT(YEAR FROM INTAKECATCHMENT.ENDDATE), DRWPLANTINTAKE.PLANTID
		),
	-- combined_indvinding_anlaeg_og_indtag: Udtraekker samler aktuel indving for hhv. anlaeg og indtag og finder den største af de to
	combined_indvinding_anlaeg_og_indtag AS 
		(
			SELECT 
				COALESCE(plant.PLANTID, intake.PLANTID) AS PLANTID, 
				COALESCE(plant.years, intake.years) AS years, 
				plant.AMOUNT AS PLANTAMOUNT, 
				intake.VOLUME AS INTAKEAMOUNT,
				GREATEST(plant.AMOUNT, intake.VOLUME) AS MAXAMOUNT
			FROM indvinding_per_anlaeg AS plant
			FULL OUTER JOIN sum_indtagsinvinding_per_anlaeg AS intake 
				ON intake.PLANTID = plant.PLANTID 
				AND plant.years = intake.years
		),
	-- tilladelser_per_anlaeg: udtraek en tilladelse per anlaeg og aarstal
	tilladelser_per_anlaeg AS 
		(
			SELECT DISTINCT ON (plantid, years)
				cp.plantid, 
				t.years, 
				cp.amountperyear,
				cp.permissionid,
				cp.startdate,
				cp.enddate
			FROM jupiter.catchperm cp
			LEFT JOIN aarstal t ON years >= EXTRACT(YEAR FROM startdate)
				AND years <= EXTRACT(YEAR FROM COALESCE(enddate, now()))
			ORDER BY cp.plantid, 
				t.years,
				(CASE WHEN COALESCE(cp.amountperyear, 0) > 0 THEN 1 ELSE 2 END), -- prioriter tilladelser der ikke er 0 eller null
				(CASE WHEN EXTRACT(YEAR FROM cp.enddate) - EXTRACT(YEAR FROM cp.startdate) <= 30 THEN 1 ELSE 2 END), -- prioriter tilladelser der løber i max 30 år
				cp.startdate DESC NULLS LAST, 
				cp.enddate DESC NULLS LAST, 
				cp.permissionid DESC NULLS LAST
		),
		rigtige_anlæg AS (
-- sidst kobineres anlaegsinfo, aktuel indvinding og tilladelser
SELECT DISTINCT ON (t1.plantid)
	t1.plantid, 
	t1.years, 
	t1.plantname, 
	t1.companytype,
	t1.active,
	t1.kommune_navn, 
	t2.plantamount, 
	t2.intakeamount,
	t3.amountperyear,
	t3.startdate,
	t3.enddate,
	t1.x, 
	t1.y, 
	t1.geom 
FROM plant_info_og_aarstal t1
LEFT JOIN combined_indvinding_anlaeg_og_indtag t2 USING (plantid, years)
LEFT JOIN tilladelser_per_anlaeg t3 USING (plantid, years)
WHERE plantamount IS NOT NULL
	AND active = 1
	AND ST_Intersects(t1.geom, 'SRID={self.crs};{self.shpgeom}')
	ORDER BY plantid, plantname, years DESC
)


SELECT DISTINCT ON (dgu_intake)
an.plantid AS anlægsid,
an.plantname AS anlægsnavn,
an.companytype AS indvindingstype,
an.years AS indvindingsmængde_år,
an.plantamount AS indvindingsmængde,
an.amountperyear AS tilladelse,

--an.enddate as tilladelse_slutdato,
EXTRACT(YEAR FROM an.enddate) as tilladelse_sludato,
 concat(REPLACE(drw.boreholeno, ' ', ''), '_', drw.intakeno) AS dgu_intake,
drw.boreholeno AS dgu_nummer,
REPLACE(drw.boreholeno, ' ', '')::TEXT AS dgu_nummer_u_mellemrum,
b.purpose as boringsformål,
b.use as boringsanvendelse,
concat(z.layer_num, '_', z.litho) AS fohm_layer
FROM jupiter.drwplantintake drw
LEFT JOIN temp_simak.fohm_filter_lith_aquifer z ON drw.boreholeid = z.boreholeid
INNER JOIN rigtige_anlæg an USING (plantid)
INNER JOIN jupiter.borehole b ON b.boreholeid = drw.boreholeid
WHERE upper(b.use) NOT IN ('S')
;
            ;
        '''
        db = Database(database='JUPITER')
        df_wl = db.sql_to_df(sql_wl)

        return df_wl



class Pejlinger:
    def __init__(self, wkt, crs):
        self.wkt = wkt
        self.crs = crs

    def fetch_pejlinger(self):
        sql_wl = f'''
            SELECT
                ROW_NUMBER() over () AS rownumber,
                wl.guid_intake, 
                concat(REPLACE(wl.boreholeno, ' ', ''), '_', wl.intakeno) AS dgu_intake, 
                wl.timeofmeas AS pejledato,
                al.antal,
                b.purpose,
                b.use,
                b.drilldepth,
                wl.quality,
                wl.watlevelid,
                b.ELEVAQUALI,
                b.locatmetho,
                b.LOCATQUALI,
                wl.situation,
                EXTRACT(YEAR FROM wl.timeofmeas) as year,
                wl.watlevmsl AS watlevmsl,
                concat(z.layer_num, '_', z.litho) AS fohm_layer,
                b.XUTM32EUREF89,
                b.YUTM32EUREF89,
                bhq.quality_litho_position as boringskvalitet,
                CONCAT(bhq.quality_litho_position, ': ', bhq.quality_litho_position_descr) as boringskvalitet_beskrivelse,
               -- lcq.quality_waterchemistry_complete as vandkemikvalitet,
                --CONCAT(lcq.quality_waterchemistry_complete, ': ', lcq.quality_waterchemistry_complete_descr) as vandkemikvalitet_beskrivelse,
                b.geom
            FROM jupiter.watlevel wl
            INNER JOIN jupiter.borehole b USING (boreholeid)
            LEFT JOIN mstjupiter.waterlevel_count al ON b.boreholeno = al.boreholeno
            --LEFT JOIN temp_makyn.limited_chemistry_quality lcq on wl.boreholeid = lcq.boreholeid
            LEFT JOIN temp_simak.fohm_filter_lith_aquifer z ON b.boreholeid = z.boreholeid
            LEFT JOIN mstjupiter.borehole_quality bhq ON b.boreholeid = bhq.boreholeid
            LEFT JOIN jupiter.screen s ON b.boreholeid = s.boreholeid
            WHERE wl.watlevmsl IS NOT NULL
                AND wl.watlevmsl < 500
                AND wl.watlevmsl > -500
                AND EXTRACT(YEAR FROM wl.timeofmeas) >= 1990
                AND COALESCE(wl.situation, 0) NOT IN (1)
                AND ST_Intersects(ST_Transform(b.geom, {self.crs}), 'SRID={self.crs};{self.wkt}')
            ORDER BY guid_intake, timeofmeas

        '''
        db = Database(database='JUPITER')
        df_pejl = db.sql_to_df(sql_wl)

        return df_pejl



class ProfiletoolMapTool(QgsMapTool):

    moved = pyqtSignal(dict)
    rightClicked = pyqtSignal(dict)
    leftClicked = pyqtSignal(dict)
    doubleClicked = pyqtSignal(dict)
    desactivate = pyqtSignal()

    def __init__(self, canvas):
        QgsMapTool.__init__(self, canvas)
        self.canvas = canvas
        self.cursor = QCursor(Qt.CrossCursor)

    def canvasMoveEvent(self, event):
        self.moved.emit({"x": event.pos().x(), "y": event.pos().y()})

    def canvasReleaseEvent(self, event):
        if event.button() == Qt.RightButton:
            self.rightClicked.emit({"x": event.pos().x(), "y": event.pos().y()})
        else:
            self.leftClicked.emit({"x": event.pos().x(), "y": event.pos().y()})

    def canvasDoubleClickEvent(self, event):
        self.doubleClicked.emit({"x": event.pos().x(), "y": event.pos().y()})

    def activate(self):
        QgsMapTool.activate(self)
        self.canvas.setCursor(self.cursor)


    def deactivate(self):
        self.desactivate.emit()
        QgsMapTool.deactivate(self)

    def isZoomTool(self):
        return False

    def setCursor(self, cursor):
        self.cursor = QCursor(cursor)


class ScriptLinetool:

    def __init__(self, iface, dialog, tag):

        self.iface = iface
        self.Dialog = dialog
        self.canvas = iface.mapCanvas()
        self.tag = tag
        self.tool = ProfiletoolMapTool(self.canvas)  # the mouselistener
        self.pointstoDraw = []  # Polyline being drawn in freehand mode

        self.dblclktemp = None  # enable disctinction between leftclick and doubleclick

       # QgsMapToolEmitPoint.__init__(self, self.canvas)

        self.rubberBand = QgsRubberBand(self.canvas, QgsWkbTypes.PolygonGeometry)
        self.rubberBand.setColor(Qt.red)
        self.rubberBand.setOpacity(0.30)
        self.rubberBand.setWidth(4)

        self.rubberbandpoint = QgsVertexMarker(self.iface.mapCanvas())
        self.rubberbandpoint.setColor(QColor(Qt.red))
        self.rubberbandpoint.setIconSize(5)
        self.rubberbandpoint.setIconType(QgsVertexMarker.ICON_BOX)  # or ICON_CROSS, ICON_X
        self.rubberbandpoint.setPenWidth(3)

        self.rubberbandbuf = QgsRubberBand(self.iface.mapCanvas())
        self.rubberbandbuf.setWidth(1)
        self.rubberbandbuf.setColor(QColor(Qt.blue))

        self.isEmittingPoint = None

        self.Dialog.setVisible(False)
       # self.setSelectionMethod(0)

    def resetRubberBand(self):
        self.rubberBand.reset(QgsWkbTypes.LineGeometry)

    # ************************************* Mouse listener actions ***********************************************

    def moved(self, position):  # draw the polyline on the temp layer (rubberband)
        if len(self.pointstoDraw) > 0:
            # Get mouse coords
            mapPos = self.canvas.getCoordinateTransform().toMapCoordinates(position["x"], position["y"])
            # Draw on temp layer
            self.resetRubberBand()
            for i in range(0, len(self.pointstoDraw)):
                self.rubberBand.addPoint(QgsPointXY(self.pointstoDraw[i][0], self.pointstoDraw[i][1]))
            self.rubberBand.addPoint(QgsPointXY(mapPos.x(), mapPos.y()))

    def rightClicked(self, position):  # used to quit the current action
        # self.profiletool.clearProfil()
        self.cleaning()

    def leftClicked(self, position):  # Add point to analyse
        mapPos = self.canvas.getCoordinateTransform().toMapCoordinates(position["x"], position["y"])
        newPoints = [[mapPos.x(), mapPos.y()]]
        # if self.profiletool.doTracking:
        #      self.rubberbandpoint.hide()

        if newPoints == self.dblclktemp:
            self.dblclktemp = None
            return
        else:
            if len(self.pointstoDraw) == 0:
                self.resetRubberBand()
            self.pointstoDraw += newPoints

    def doubleClicked(self, position):
        from .dataextractor_FOHMprofil import plot_data

        self.isEmittingPoint = False

        mapPos = self.canvas.getCoordinateTransform().toMapCoordinates(position["x"], position["y"])
        newPoints = [[mapPos.x(), mapPos.y()]]
        self.pointstoDraw += newPoints
        # launch analyses
        self.iface.mainWindow().statusBar().showMessage(str(self.pointstoDraw))
        # Reset
        # temp point to distinct leftclick and dbleclick
        self.dblclktemp = newPoints

        if self.Dialog.comboBox_4.currentText() == 'FOHM - Jylland':
            område = 'jylland'
        elif self.Dialog.comboBox_4.currentText() == 'FOHM - Fyn':
            område = 'fyn'
        else:
            område = 'sjaelland'
        # Get coordinate reference system value of the qgis project
        crs_input = qgis.utils.iface.mapCanvas().mapSettings().destinationCrs()

        # Transform to EPSG:25832
      #  crs_dest = QgsCoordinateReferenceSystem(25832)
    # transform = QgsCoordinateTransform(crs_input, crs_dest, QgsProject.instance())
        #startPoint_transformed = transform.transform(self.startPoint)
        #endPoint_transformed = transform.transform(self.endPoint)

       # points = [startPoint_transformed, endPoint_transformed]
        #print(points)

        if self.pointstoDraw is not None:
            plot_data(self.Dialog, self.pointstoDraw, område, number_of_points=self.Dialog.spinBox_punkterlangslinje.value())
            #self.reset()
            #self.deactivate()

    def setSelectionMethod(self, method):
        self.cleaning()
        self.selectionmethod = method
        self.tool.setCursor(Qt.CrossCursor)
        self.iface.mainWindow().statusBar().showMessage(self.textquit0)

    def setBufferGeometry(self, geoms):
        self.rubberbandbuf.reset()
        for g in geoms:
            self.rubberbandbuf.addGeometry(g, None)

    def cleaning(self):  # used on right click
        self.pointstoDraw = []
        self.rubberbandpoint.hide()
        self.resetRubberBand()
        self.rubberbandbuf.reset()
        self.iface.mainWindow().statusBar().showMessage("")

    def connectTool(self):
        self.tool.moved.connect(self.moved)
        self.tool.rightClicked.connect(self.rightClicked)
        self.tool.leftClicked.connect(self.leftClicked)
        self.tool.doubleClicked.connect(self.doubleClicked)
        self.tool.desactivate.connect(self.deactivate)

    def deactivate(self):  # enable clean exit of the plugin
        self.cleaning()
        self.tool.moved.disconnect(self.moved)
        self.tool.rightClicked.disconnect(self.rightClicked)
        self.tool.leftClicked.disconnect(self.leftClicked)
        self.tool.doubleClicked.disconnect(self.doubleClicked)
        self.tool.desactivate.disconnect(self.deactivate)
        self.canvas.unsetMapTool(self.tool)


class AutomaticCrossSection():
    """
        A simple run-function to minimize the code within qgis script
        :param line_points: list of tuples, the vertices of the profile line in srid 25832
                eg. [(513425, 6296131), (529416, 6310901)]
        :param number_of_points: int, number of profile nodes along the profile line
        :param display_fig_descr: bool, if true: add figure description to the plot
        :param smooth_line: bool, if true: smoothing using pchip interpolation between the profile points
                if false: matplotlibs standard linear interpolation between the profile points
        :param display_profile_points: bool, if true: add lines displaying the position of the profile points
        :param database_name: string, name of the database containing the fohm layers
        :param schema_name: string, schema name with in the database containing the fohm layers
    """

    def __init__(
            self,
            line_points,
            område,
            id,
            savepath,
            number_of_points=300,
            smooth_line=True,
            display_fig_descr=True,
            display_profile_points=True,
            database_name='GRUKOS',  # fohm
            schema_name='mstfohm'  # fohm_layer
    ):
        self.database_name = database_name
        self.db = Database(self.database_name)
        self.db_jup = Database('JUPITER')
        # self.Dialog = dialog
        self.område = område
        self.schema_name = schema_name
        self.points = line_points
        self.line_wkt = self.points_to_line_wkt()
        self.srid = 25832
        self.n = number_of_points
        self.smooth_lines = smooth_line
        self.display_fig_descr = display_fig_descr
        self.display_profile_points = display_profile_points
        self.df_table_name = self.fetch_table_names()
        self.sql = self.fetch_cross_section_sql()
        self.fig_txt = ''
        self.id = id
        self.savepath = savepath


        self.vertexpoint = QgsVertexMarker(iface.mapCanvas())
        self.vertexpoint.setColor(QColor(Qt.red))
        self.vertexpoint.setIconSize(7)
        self.vertexpoint.setIconType(QgsVertexMarker.ICON_BOX)
        self.vertexpoint.setPenWidth(5)

        self.vertexboring = QgsVertexMarker(iface.mapCanvas())
        self.vertexboring.setColor(QColor(Qt.blue))
        self.vertexboring.setIconSize(5)
        self.vertexboring.setIconType(QgsVertexMarker.ICON_BOX)
        self.vertexboring.setPenWidth(3)

    @staticmethod
    def interpolate_layers(x_data, df, n_interp=1000, smoothing_factor=10, window_length=151, polyorder=2):
        """
        Interpolate all columns of a pandas dataframe (y_data) and x_data using spline interpolation.
        :param x_data: numpy array, data (1st axis) used in the interpolation
        :param df: pandas dataframe, each row is data (2nd axis) used in the interpolation
        :param n_interp: int, number of nodes used in the interpolation
        :param smoothing_factor: float, smoothing factor for spline interpolation
        :param window_length: int, the length of the filter window (must be a positive odd integer)
        :param polyorder: int, the order of the polynomial used to fit the samples (must be less than window_length)
        :return: numpy array (1st axis) and pandas dataframe (2nd axis)
        """
        x_interp = np.linspace(x_data.min(), x_data.max(), n_interp)
        df_interp = pd.DataFrame()

        for col_name, col_val in df.items():
            # Spline interpolation with smoothing factor
            spline = UnivariateSpline(x_data, col_val, s=smoothing_factor)
            y_interp = spline(x_interp)

            # Apply Savitzky-Golay filter to smooth the interpolated data
            y_smooth = savgol_filter(y_interp, window_length=window_length, polyorder=polyorder)

            df_interp[col_name] = y_smooth

        return x_interp, df_interp

    def points_to_line_wkt(self):
        """
            Converts a list of tuple containing x,y coords to WKT linestring.
            :return: string, WKT linestring
        """
        points_txt = ''
        for i, (x, y) in enumerate(self.points):
            if i == 0:
                points_txt += f'{x} {y}'
            else:
                points_txt += f', {x} {y}'

        line_wkt = f'LINESTRING({points_txt})'

        return line_wkt

    def fetch_anlaeg(self, boringsliste):
        #   boringslisten = ' ,'.join(boringsliste)
        # print(boringslisten)

        for i, boring in enumerate(boringsliste):
            if i == 0:

                sql_anlaeg = f'''
                SELECT DISTINCT ON (plantid)
                p.plantid 
                FROM jupiter.DRWPLANT p
                INNER JOIN jupiter.drwplantintake drw USING (plantid)
                INNER JOIN jupiter.borehole b ON b.boreholeid = drw.boreholeid
                WHERE REPLACE(b.boreholeno, ' ', '') IN ('{boring}')
                    '''
            else:
                sql_anlaeg += f'''\nOR REPLACE(b.boreholeno, ' ', '') IN ('{boring}')'''

        return sql_anlaeg

    def fetch_lithology(self, boringskvalitet, boringskvalitet_kilde, mindybde):
        if True:  # self.Dialog.checkBox_vvboringer.checkState() == Qt.Checked:
            vv = "AND b.use in ('V', 'VV')"
        else:
            vv = ''

        sql_lith = f'''
                WITH               
                waterlevel as(
    	                        SELECT DISTINCT ON (wl.WATLEVELID)
                                b.boreholeno,
                                b.boreholeid,
                                wl.intakeno,
                                wl.guid_intake,
                                wl.watlevgrsu AS vandst_mut, --Det målte vandspejl beregnet som meter under terrænoverfladen
                                wl.watlevmsl AS vandstkote,  --Det målte vandspejl beregnet som meter over havniveau (online DVR90 ved download det valgt kotesystem)
                                wl.timeofmeas::DATE AS pejledato,
                                wl.situation,
                                b.XUTM32EUREF89 as X,
                                b.YUTM32EUREF89 as Y,
                                ST_Distance(ST_StartPoint(ST_GeomFromText('{self.line_wkt}', {self.srid})), ST_ClosestPoint(ST_GeomFromText('{self.line_wkt}', {self.srid}),b.geom)) AS dist,
                                ST_Distance(ST_ClosestPoint(ST_GeomFromText('{self.line_wkt}', {self.srid}),b.geom),b.geom) as afstand,

                                b.geom
                                FROM jupiter.borehole b
                                INNER JOIN jupiter.watlevel wl USING (boreholeid)
                                WHERE b.geom && ST_Buffer(ST_GeomFromText('{self.line_wkt}', {self.srid}) , 1000, 'endcap=flat join=round')

                                ORDER BY wl.WATLEVELID, pejledato DESC),
                vand as (
                SELECT DISTINCT ON (guid_intake)
    	  	        row_number() OVER () AS row_id,
    	                *
    	            FROM waterlevel
    	             ORDER BY guid_intake, boreholeno, pejledato DESC NULLS LAST
                ),              

                    tmp AS 
                        (
                            SELECT 
                                replace(b.boreholeno, ' ', '') as boreholeno,
                                b.boreholeid, sc.top as topfilter, sc.bottom as bottomfilter,
                                ls.top, 
                                b.elevation,
                                wl.vandst_mut as waterlevel,
                                wl.dist,
                                wl.afstand,
                                b.drilendate,
                                wl.X,
                                wl.Y,
                                COALESCE(
                                    ls.bottom, 
                                    lag(ls.top, -1) OVER (PARTITION BY boreholeid ORDER BY ls.top), 
                                    b.drilldepth, 
                                    ls.top+10
                                    ) AS bottom, 
                                ls.rocksymbol
                            FROM jupiter.lithsamp ls
                            INNER JOIN jupiter.borehole b USING (boreholeid)
                            Left JOIN vand wl using (boreholeid)
                            left join jupiter.screen sc USING (boreholeid)
                            {boringskvalitet_kilde}
                            WHERE b.geom && ST_Buffer(ST_GeomFromText('{self.line_wkt}', {self.srid}) , 1000, 'endcap=flat join=round')
                            {boringskvalitet}
                            {mindybde}
                            {vv}
                        ),
                    tmp2 AS 
                        (
                            SELECT DISTINCT 
                                boreholeno
                            FROM tmp
                            ORDER BY boreholeno
                        ),
                    tmp3 AS 
                        (
                            SELECT 
                                ROW_NUMBER() OVER () AS fig_number,
                                boreholeno
                            FROM tmp2
                        )
                SELECT 
                    tmp3.fig_number,
                    boreholeno, 
                    boreholeid, 
                    top, 
                    bottom,
                    dist,
                    afstand,
                    elevation,
                    drilendate,
                    bottomfilter,
                    topfilter,
                    waterlevel,
                    X,
                    Y,
                    rocksymbol,
                    CASE 
                        WHEN RIGHT(rocksymbol, 1) = 's'
                            THEN '#ff5400'
                        WHEN RIGHT(rocksymbol, 1) = 'g'
                            THEN '#ff5400'
                        WHEN RIGHT(rocksymbol, 1) = 'q'
                            THEN '#ff00ff'
                        WHEN RIGHT(rocksymbol, 1) = 'a'
                            THEN '#ff8000'
                        WHEN RIGHT(rocksymbol, 1) = 'r'    
                            THEN '#ccb3ff'
                        WHEN rocksymbol = 'ml'
                            THEN '#dbb800'
                        WHEN rocksymbol = 'dl'
                            THEN '#ffba33'
                        WHEN rocksymbol IN ('ol', 'pl', 'll')
                            THEN '#000cff'
                        WHEN rocksymbol = 'gl'
                            THEN '#b3ffff'
                        WHEN rocksymbol = 'ed'
                            THEN '#52ffff'
                        WHEN rocksymbol NOT IN ('dl', 'ml', 'ol', 'pl', 'll')
                            AND RIGHT(rocksymbol, 1) = 'l'
                            THEN '#9d711f' 
                        WHEN RIGHT(rocksymbol, 1) = 'j'
                            THEN '#9d711f'
                        WHEN RIGHT(rocksymbol, 1) = 'i'
                            THEN '#faf59e'
                        WHEN RIGHT(rocksymbol, 1) = 'k'
                            THEN '#80ff99'
                        WHEN RIGHT(rocksymbol, 1) = 't'
                            THEN '#deff00'   
                        WHEN RIGHT(rocksymbol, 1) = 'p'
                            THEN '#c16544'  
                        ELSE '#efefef'
                        END AS html_color                    
                FROM tmp
                INNER JOIN tmp3 USING (boreholeno)
                where afstand <= 1000
                ORDER BY boreholeno, top
            '''

        return sql_lith

    def fetch_table_names(self):
        """
            Returns the table_names within the fohm database/schema
            :return: string, sql
        """
        if self.område == 'fynsmodellen':
            sql_table_name = f'''
            SELECT table_name AS table_name
            FROM information_schema.tables
            WHERE table_schema='pilotprojekt'
                AND table_type='BASE TABLE'
                AND table_name LIKE 'fynmodel%'
                AND table_name NOT LIKE '%borehole%'
            ORDER BY table_name;
        '''
        else:
            sql_table_name = f'''
                SELECT table_name AS table_name
                FROM information_schema.tables
                WHERE table_schema='{self.schema_name}'
                    AND table_type='BASE TABLE'
                    AND table_name LIKE '{self.område}%'
                    AND table_name NOT LIKE '%slice%'
                ORDER BY table_name;
            '''

        df_table_name = self.db.sql_to_df(sql_table_name)
        return df_table_name

    def fetch_cross_section_sql(self):
        """
        return sql for querying the fohm raster layers as point along the line
        :return: string, sql
        """
        n_inv = 1 / self.n

        sql_join = ''
        sql_ele = ''
        for i, (index, row) in enumerate(self.df_table_name.iterrows()):
            table_name = row['table_name']
            layer_name = re.search("(\d+.[A-Za-z0-9ÆØÅæøå]+.[A-Za-z0-9ÆØÅæøå]+)", table_name)[0]
            # sql_join += f"\nINNER JOIN (SELECT rid, rast FROM fohm.fohm_grids WHERE filename = '{table_name}') r{i} ON st_intersects(r{i}.rast, b.geom)" #f'\n INNER JOIN mstfohm.'{table_name}" r{i} ON st_intersects(r{i}.rast, b.geom)'   #fohm_layer
            sql_join += f'\n INNER JOIN mstfohm."{table_name}" r{i} ON st_intersects(r{i}.rast, b.geom)'  # fohm_layer
            sql_ele += f'\n(ST_ValueCount(ST_Clip(r{i}.rast,b.geom))).value AS "{layer_name}",'

        sql = f'''
            WITH
                lines AS 
                    (
                        SELECT
                            ST_GeomFromText('{self.line_wkt}', {self.srid}) AS temp_line
                    ),
                points_from_line AS 
                    (
                        SELECT 
                            (ST_DumpPoints(ST_LineInterpolatePoints(temp_line, {n_inv}))).geom AS geom
                        FROM lines 
                    ),
                profile_dist AS 
                    (
                        SELECT 
                            row_number() OVER () AS row_num,
                            geom,
                            round((COALESCE(ST_Distance(geom, lag(geom,1) OVER ()), 0))::NUMERIC, 0) AS dist
                        FROM points_from_line
                    ),
                profile_dist_cumsum AS 
                    (
                        SELECT 
                            geom,
                            sum(dist) OVER (ORDER BY row_num) AS distance
                        FROM profile_dist 
                    )         
            SELECT
                {sql_ele}
                distance
            FROM profile_dist_cumsum b                
            {sql_join}
            ;
        '''
        return sql

    def plot_layers(self):
        """
            Plot the fohm layers using matplotlib's fill_between function
            The layers are only plotted if the layer if different than the layer above (previous)
            :return: None
        """

        sql = self.fetch_cross_section_sql()
        df = self.db.sql_to_df(sql)

        # put profile distance in separate var and del from dataframe
        x_dist = np.sort(df['distance'].to_numpy())
        x_dist_orig = x_dist
        df.pop('distance')
        rcParams['figure.dpi'] = 200
        # set vars for deactivating layers on plot legend
        fill_plots = []

        # create figure and plot
        if True:
            fig, ax = plt.subplots(figsize=(23, 8)) #17

        # define colors of fohm layers
        if self.område == 'jylland':
            colors = {
                '0000_terraen': 'black',
                '0050_antropocaen': 'lavender',
                '0100_postglacial_toerv': 'peru',
                '0120_postglacial_sand': 'red',
                '0130_postglacial_littorinaler': '#5d1902',
                '0140_senglacial_yoldiasand': 'purple',
                '0150_senglacial_yoldialer': '#3f1a18',
                '0200_kvartaer_sand': '#ffa3f9',
                '0300_kvartaer_ler': '#f5ac51',
                '0400_kvartaer_sand': '#ff00ff',
                '1100_kvartaer_ler': '#d99036',
                '1200_kvartaer_sand': '#ffff66',
                '1300_kvartaer_ler': '#b36d17',
                '1400_kvartaer_sand': '#ff99cc',
                '1500_kvartaer_ler': '#653308',
                '2100_kvartaer_sand': 'red',
                '2200_kvartaer_ler': '#5d1902',
                '2300_kvartaer_sand': '#ff6b6b',
                '2400_kvartaer_ler': '#3f1a18',
                '5100_maadegruppen_gram': '#63ffef',
                '5200_øvre_odderup': 'deepskyblue',
                '5300_øvre_arnum': '#63ffef',
                '5400_nedre_odderup': 'deepskyblue',
                '5500_nedre_arnum': '#63ffef',
                '5600_bastrup_sand': 'deepskyblue',
                '5700_klintinghoved_ler': '#63ffef',
                '5800_bastrup_sand': 'deepskyblue',
                '5900_klintinghoved_ler': '#63ffef',
                '6000_bastrup_sand': 'deepskyblue',
                '6100_klintinghoved_ler': '#63ffef',
                '6200_bastrup_sand': 'deepskyblue',
                '6300_klintinghoved_ler': '#63ffef',
                '6400_bastrup_sand': 'deepskyblue',
                '6500_klintinghoved_ler': '#63ffef',
                '6600_bastrup_sand': 'deepskyblue',
                '6700_klintinghoved_ler': '#63ffef',
                '6800_billund_sand': 'deepskyblue',
                '6900_vejle_fjord': '#63ffef',
                '7000_billund_sand': 'deepskyblue',
                '7100_vejle_fjord': '#63ffef',
                '7200_billund_sand': 'deepskyblue',
                '7300_vejle_fjord': '#63ffef',
                '7400_billund_sand': 'deepskyblue',
                '7500_vejle_fjord': '#63ffef',
                '7600_billund_sand': 'deepskyblue',
                '7700_vejle_fjord': '#63ffef',
                '7800_billund_sand': 'deepskyblue',
                '8000_palaeogen_ler': '#1532ed',
                '8500_danien_kalk': 'darkgreen',
                '9000_skrivekridt': 'lawngreen',
                '9500_stensalt': 'black',
                '9499_dummy_layer': 'lightgrey'
            }
        elif self.område == 'fyn':
            colors = {
                '0001_topo': 'black',
                '0002_kl1': '#f5ac51',
                '0003_ks1': '#ffa3f9',
                '0004_kl2': '#d99036',
                '0005_ks2': '#ff00ff',
                '0006_kl3': '#b36d17',
                '0007_ks3': '#ffff66',
                '0008_kl4': '#3f1a18',
                '0009_pl1': '#3732c9',
                '0010_kalk': 'darkgreen',

            }
        else:
            colors = {
                '0001_topo': 'black',
                '0002_kl1': '#f5ac51',
                '0003_ks1': '#ffa3f9',
                '0004_kl2': '#d99036',
                '0005_ks2': '#ff00ff',
                '0006_kl3': '#b36d17',
                '0007_ks3': '#ffff66',
                '0008_kl4': '#3f1a18',
                '0009_ks4': 'red',
                '0010_kl5': '#653308',
                '0011_pl1': '#3732c9',
                '0012_gk1b': 'darkgreen',
                '0013_dk1b': 'lawngreen',
                '0014_sk1': 'black',
            }

        # smooth lines if selected
        if self.smooth_lines:
            x_dist, df = self.interpolate_layers(x_dist, df)

        # loop through all layers
        column_names = df.columns
        for i, column_name in enumerate(column_names):
            bot = df[column_names[i]]  # layer bot as bot
            top = df[column_names[i - 1]]
            layer_id = re.search('\d+', column_name)[0]  # layer number e.g. 0000_terrain => 0000
            # plot layer if not a zero-thickness layer
            if not bot.equals(top):
                alpha = 0.85
                if layer_id == '9500' or layer_id == '0014':  # layer 9500 is a top-layer not bottom-layer
                    ax.plot(np.array(x_dist), np.array(bot), zorder=3, color='deeppink', label=column_name, linewidth=1)
                    if self.område == 'jylland':
                        column_name = '9499_dummy_layer'

                    alpha = 0.4

                if layer_id != '0000' and layer_id != '0001':  # discard terrain plot
                    pl = ax.fill_between(
                        x_dist, top, bot,
                        label=column_name, zorder=2, color=colors[column_name],  # colors[column_name]
                        alpha=alpha, linewidth=0.2, edgecolor='black'
                    )
                    fill_plots.append(pl)

        # plot

        # dislay points where fohm layers are extracted
        bot_min = df.min().min()
        top_max = df.max().max()

        # set grid on plot
        ax.grid('both')
        if self.område == 'jylland':
            plt.ylim(ymax=df["0000_terraen"].max() + 20, ymin=df["2400_kvartaer_ler"].min() - 30)
        elif self.område == 'fyn':
            plt.ylim(ymax=df["0001_topo"].max() + 20, ymin=df["0008_kl4"].min() - 30)
        else:
            pass
        # plt.ylim(ymax=df["0001_topo"].max() + 20, ymin=df["0010_kl5"].min() - 30)

        # add title and labels to figure
        # plt.title(f'FOHM Profil', fontsize=12, fontweight='bold')
        plt.xlabel('Længde [m]', fontsize=14)
        plt.ylabel('Kote [m]', fontsize=14)

        if True:
            boringskvalitet = ""
            boringskvalitet_kilde = ""
            mindybde = ""

            sql_lith = self.fetch_lithology(boringskvalitet=boringskvalitet,
                                            boringskvalitet_kilde=boringskvalitet_kilde, mindybde=mindybde)
            df_lith = self.db_jup.sql_to_df(sql_lith)

            df_lith_grp = df_lith.groupby(['boreholeno'])
            boringsafstand = 200  # self.Dialog.spinBox_5.value()
            global boringsliste
            boringsliste = []
            maxdepth = []

            for key, grp in df_lith_grp:
                unikke = grp["rocksymbol"].unique().tolist()
                if unikke == ["b", "x"] or unikke == ["x", "b"] or unikke == ["b"] or unikke == ["x"]:
                    pass
                else:
                    for i, (row_index, row) in enumerate(grp.iterrows()):
                        if row["afstand"] < boringsafstand:
                            try:
                                fig_number = row["fig_number"]

                                z = ax.fill_between \
                                    (x=[row["dist"] - (max(x_dist_orig) / 280), row["dist"] + (max(x_dist_orig) / 280)],
                                     y1=row['elevation'] - row['bottom'], y2=row['elevation'] - row['top'],
                                     edgecolor='black',
                                     color=row['html_color'], zorder=100, label="Boring - {}".format(row["boreholeno"]),
                                     picker=0.01)
                                navn = row["boreholeno"]
                                boringsliste.append(navn.replace(" ", ""))
                                maxdepth.append(row['elevation'] - row['bottom'])

                                start = row["elevation"]
                                afstand2 = row["afstand"]

                                if row["topfilter"] is None:
                                    pass
                                else:
                                    plt.vlines(x=row["dist"] + (max(x_dist_orig) / 280),
                                               ymin=row['elevation'] - row["topfilter"],
                                               ymax=row['elevation'] - row["bottomfilter"],
                                               color='black', linewidth=3, label='Filter')

                                if True:  # self.Dialog.checkBox_18.checkState() == Qt.Unchecked:
                                    if row["waterlevel"] is None:
                                        pass
                                    else:
                                        plt.plot(row["dist"] + (max(x_dist_orig) / 180),
                                                 row['elevation'] - row["waterlevel"], color='blue', marker='<',
                                                 markersize=2.75)

                                if True:  # self.Dialog.checkBox_18.checkState() == Qt.Checked and self.Dialog.checkBox_inklpejlinger.checkState() == Qt.Checked:
                                    if row["waterlevel"] is None:
                                        pass
                                    else:
                                        plt.plot(row["dist"] + (max(x_dist_orig) / 180),
                                                 row['elevation'] - row["waterlevel"], color='blue', marker='<',
                                                 markersize=2.75)

                                minimum = grp['elevation'].iloc[0] - grp['bottom'].iloc[-1]
                                maximum = grp['elevation'].iloc[0] - grp["top"].iloc[0]
                                x0 = grp["dist"].iat[0] - (max(x_dist_orig) / 280)
                                x1 = grp["dist"].iat[0] + (max(x_dist_orig) / 280)

                                depth = grp['bottom'].max()

                                if True:  # self.Dialog.checkBox_18.checkState() == Qt.Unchecked:
                                    if abs(maximum - minimum) > 5:
                                        if row['bottom'] == depth:
                                            ax.text((x0 + x1) / 2, row['elevation'] - row['bottom'] + 1,
                                                    row['rocksymbol'], fontsize=7.9, clip_on=True, ha='center',
                                                    zorder=100)
                                        else:
                                            j = int(i + 1)
                                            if grp.iloc[j]['rocksymbol'] != row['rocksymbol']:
                                                ax.text((x0 + x1) / 2, row['elevation'] - row['bottom'] + 1,
                                                        row['rocksymbol'], fontsize=7.9, clip_on=True, ha='center',
                                                        zorder=100)

                                if True:  # self.Dialog.checkBox_18.checkState() == Qt.Checked and self.Dialog.checkBox_inkllithobeskrivelser.checkState() == Qt.Checked:
                                    if abs(maximum - minimum) > 5:
                                        if row['bottom'] == depth:
                                            ax.text((x0 + x1) / 2, row['elevation'] - row['bottom'] + 1,
                                                    row['rocksymbol'], fontsize=7.9, clip_on=True, ha='center',
                                                    zorder=100)
                                        else:
                                            j = int(i + 1)
                                            if grp.iloc[j]['rocksymbol'] != row['rocksymbol']:
                                                ax.text((x0 + x1) / 2, row['elevation'] - row['bottom'] + 1,
                                                        row['rocksymbol'], fontsize=7.9, clip_on=True, ha='center',
                                                        zorder=100)

                                if i == 0:
                                    z2 = ax.text((x0 + x1) / 2, start + 5, "{}".format(navn), fontsize=9, ha='center',
                                                 color='black', rotation=90, weight='light', clip_on=True)
                                    # z2.set_bbox(dict(facecolor='white', alpha=1, edgecolor='black'))

                                    z3 = ax.text((x0 + x1) / 2, minimum - 2, "({})".format(int(afstand2)), ha='center',
                                                 va='center', fontsize=7.4, color='black', weight='light', clip_on=True)
                                    ax = plt.gca()
                                    rect = patches.Rectangle((x0, minimum), x1 - x0, maximum - minimum, linewidth=0.5,
                                                             edgecolor='black', facecolor='none',
                                                             zorder=120, label="Boring")
                                    ax.add_patch(rect)
                                else:
                                    pass
                                """
                                if fig_number == temp:
                                    z2 = ax.text((x0 + x1) / 2, start + 5, "{}".format(navn), fontsize=7, ha='center',
                                                 color='black', rotation=90, weight='light', clip_on=True)
                                    # z2.set_bbox(dict(facecolor='white', alpha=1, edgecolor='black'))

                                    z3 = ax.text((x0 + x1) / 2, minimum - 2, "({})".format(int(afstand2)), ha='center',
                                                 va='center', fontsize=5.4, color='black', weight='light', clip_on=True)
                                    ax = plt.gca()
                                    rect = patches.Rectangle((x0, minimum), x1 - x0, maximum - minimum, linewidth=0.5,
                                                             edgecolor='black', facecolor='none',
                                                             zorder=120, label="Boring")
                                    ax.add_patch(rect)
                                else:
                                    pass
                                temp = fig_number
                                """
                            except:
                                pass

        # plt.show()

        ymin, ymax = ax.get_ylim()
        try:
            ax.set_ylim(min(maxdepth)-40, ymax)
        except:
            pass

        plt.tight_layout()
        plt.savefig(
            self.savepath + '/' + str(self.id) + '.png',
            transparent=True)
        plt.close()

        rcParams['figure.dpi'] = 100

        # self.Dialog.pushButton_FOHM.setText("Tegn FOHM-profil")
        # self.Dialog.pushButton_FOHM.setEnabled(True)
        # self.Dialog.repaint()
        # self.Dialog.iface.actionPan().trigger()
        # self.Dialog.setVisible(True)


def calculate_total_intersection_length(layer, wkt_geometry):
    total_length = 0
    wkt_geom = QgsGeometry.fromWkt(wkt_geometry)

    for feature in layer.getFeatures():
        feature_geom = feature.geometry()
        if feature_geom.intersects(wkt_geom):
            intersection_length = feature_geom.intersection(wkt_geom).length()
            total_length += intersection_length

    return total_length


# Function to find the best overlapping WKT for the entire layer
def find_best_overlap_for_layer(layer, wkt_geometries, names):
    # Get the QGIS layer by name

    # Ensure the layer has features
    if not layer:
        raise ValueError(f"Layer '{layer.name()}' not found.")
    if layer.featureCount() == 0:
        raise ValueError(f"No features found in layer '{layer.name()}'.")

    intersections = [calculate_total_intersection_length(layer, wkt) for wkt in wkt_geometries]

    # Determine the best overlapping WKT geometry
    best_overlap_index = intersections.index(max(intersections))
    best_wkt_name = names[best_overlap_index]

    return best_wkt_name, intersections
