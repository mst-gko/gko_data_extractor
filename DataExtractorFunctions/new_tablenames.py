import os
import glob
import sys
import numpy as np

inFile = sys.argv[1]
fileext = sys.argv[2]
path = f'{inFile}/*.{fileext}'
for fname in glob.glob(path):
    old_filename=os.path.basename(fname)
    new_filename = old_filename.replace("æ", "ae").replace("ø", "oe").replace("å", "aa").replace(" ", "_").lower()
    num=""
    for c in new_filename:
        if c.isdigit():
            num=num+c
    if len(num)==1:
        num2="0"+num
        new_filename = old_filename.replace("æ", "ae").replace("ø", "oe").replace("å", "aa").replace(" ", "_").replace(num,num2).lower()
    os.rename(f'{inFile}/{old_filename}', f'{inFile}/{new_filename}')
