# -*- coding: utf-8 -*-
import subprocess
# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import *
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()

import pyodbc
from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass


__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *
from ..doDataExtract import *

def update_settings(self):
    # Designopdateringer af plugin UI
    if self.comboBox_tolkningsvurdering.currentText() == '1.1: FOHM - enkelt lag':
        self.groupBox.setEnabled(True)

        self.label_18.setEnabled(False)
        self.layer_combo.setEnabled(False)
        self.label_19.setEnabled(False)
        self.layer_combo_2.setEnabled(False)
        self.label_22.setEnabled(False)
        self.lineEdit_10.setEnabled(False)
        self.pushButton_mappe.setEnabled(False)
        self.comboBox_area.setEnabled(False)
        self.label_23.setEnabled(False)

        self.label_24.setVisible(False)
        self.comboBox_lagtype.setVisible(False)
        self.tableWidget_magasin.setVisible(False)

        self.label_21.setEnabled(True)
        self.comboBox_wanted.setEnabled(True)
    elif self.comboBox_tolkningsvurdering.currentText() == '2.1: Benyt specifikke flader indlæst i QGIS':
        self.groupBox.setEnabled(True)

        self.label_18.setEnabled(True)
        self.layer_combo.setEnabled(True)
        self.label_19.setEnabled(True)
        self.layer_combo_2.setEnabled(True)
        self.label_22.setEnabled(False)
        self.lineEdit_10.setEnabled(False)
        self.pushButton_mappe.setEnabled(False)
        self.comboBox_area.setEnabled(False)
        self.label_23.setEnabled(False)
        self.label_21.setEnabled(False)
        self.comboBox_wanted.setEnabled(False)

        self.label_24.setVisible(True)
        self.comboBox_lagtype.setVisible(True)
        self.tableWidget_magasin.setVisible(False)
    elif self.comboBox_tolkningsvurdering.currentText() == '2.2: Benyt alle flader fra mappe (KRÆVENDE)':
        self.groupBox.setEnabled(True)
        self.label_18.setEnabled(False)
        self.layer_combo.setEnabled(False)
        self.label_19.setEnabled(False)
        self.layer_combo_2.setEnabled(False)
        self.label_21.setEnabled(False)
        self.comboBox_wanted.setEnabled(False)
        self.comboBox_area.setEnabled(False)
        self.label_23.setEnabled(False)

        self.label_24.setVisible(False)
        self.comboBox_lagtype.setVisible(False)
        self.tableWidget_magasin.setVisible(True)

        self.label_22.setEnabled(True)
        self.lineEdit_10.setEnabled(True)
        self.pushButton_mappe.setEnabled(True)
    elif self.comboBox_tolkningsvurdering.currentText() == '1.2: FOHM - alle lag (KRÆVENDE)':
        self.groupBox.setEnabled(True)
        self.label_22.setEnabled(False)
        self.lineEdit_10.setEnabled(False)
        self.pushButton_mappe.setEnabled(False)

        self.label_18.setEnabled(False)
        self.layer_combo.setEnabled(False)
        self.label_19.setEnabled(False)
        self.layer_combo_2.setEnabled(False)

        self.label_24.setVisible(False)
        self.comboBox_lagtype.setVisible(False)
        self.tableWidget_magasin.setVisible(False)

        self.label_21.setEnabled(False)
        self.comboBox_wanted.setEnabled(False)

        self.comboBox_area.setEnabled(True)
        self.label_23.setEnabled(True)

#Custom geofysik database
def indhent_geofysik(self):
    global plugin_path_geofysik
    # filter = "Shapefil(*.shp);;GeoPackage(*.gpkg);;Raster(*.tiff);;Raster(*.asc)"
    plugin_path_geofysik, _filter = QFileDialog.getOpenFileNames(self, caption='Vælg geofysikdatabase-fil.')  # , filter=filter
    self.lineEdit_8.setText(str(plugin_path_geofysik))

#Custom borings database
def indhent_boringer(self):
    global plugin_path_boringer
    # filter = "Shapefil(*.shp);;GeoPackage(*.gpkg);;Raster(*.tiff);;Raster(*.asc)"
    plugin_path_boringer, _filter = QFileDialog.getOpenFileNames(self, caption='Vælg boringdsdatabase-fil.')  # , filter=filter
    self.lineEdit_9.setText(str(plugin_path_boringer))

def foretag_tolkningsvurderingen(self):

    if len(self.comboBox.currentText()) > 0: #Hvis områdepolygon er indhentet
        self.foretag_tolkningsvurdering.setText("Kører")
        self.foretag_tolkningsvurdering.setEnabled(False)
        self.repaint()
        project = QgsProject.instance()

        # Indhenter geometri og CRS af områdepolygon (layer)
        key = self.comboBox.currentText()
        layer = project.mapLayersByName('{}'.format(key))[0]

        for kk, feature in enumerate(layer.getFeatures()):
            if kk == 0:
                geom = feature.geometry()
            else:
                geom = geom.combine(feature.geometry())

        crs = int(
            "".join(filter(str.isdigit, str(layer.crs().authid()))))  # int(filter(str.isdigit, str(layer.crs().authid())))


        def fetch_table_names(område):
            """
                Returns the table_names within the fohm database/schema
                :return: string, sql
            """
            sql_table_name = f'''
                SELECT table_name AS table_name
                FROM information_schema.tables
                WHERE table_schema='mstfohm'
                    AND table_type='BASE TABLE'
                    AND table_name LIKE '{område}%'
                    AND table_name NOT LIKE '%slice%'
                ORDER BY table_name;
            '''
            db = Database(database='GRUKOS')
            df_table_name = db.sql_to_df(sql_table_name)
            return df_table_name

        def geofysik_fetch_fohm_all(crs, geom, alllayers):
            sql_join = ''
            sql_ele = ''
            sql_where = ''
            prev = ''
            max_iterations = len(alllayers)

            for i, lay in enumerate(alllayers):
                table_name = lay
                layer_name = re.search("(\d+.[A-Za-z0-9ÆØÅæøå]+.[A-Za-z0-9ÆØÅæøå]+)", lay)[0]

                sql_join += f'\n INNER JOIN mstfohm."{table_name}" r{i} ON st_intersects(r{i}.rast, gp.geom)'  # Laver Joins
                sql_ele += f'\n(ST_ValueCount(ST_Clip(r{i}.rast,ST_Transform(gp.geom, {crs})))).value AS "{layer_name}",' #Indhenter lag-værdi for geofysik punktet

                # SQL_Where sikrer at der kun indhentes over DOI og indenfor det specifikke lag.
                if i == 0:
                    sql_where += f' '
                elif i == max_iterations-1:
                    sql_where += f'\n (((doi <= lagtopkote OR doi IS NULL) AND "{prev}"!="{layer_name}" AND lagtopkote <= "{prev}" AND lagtopkote >= "{layer_name}")  OR ((doi <= lagtopkote OR doi IS NULL) AND "{prev}"!="{layer_name}" AND lagbottomkote <= "{prev}" AND lagbottomkote >= "{layer_name}"))'
                else:
                    sql_where += f'\n (((doi <= lagtopkote OR doi IS NULL) AND "{prev}"!="{layer_name}" AND lagtopkote <= "{prev}" AND lagtopkote >= "{layer_name}")  OR ((doi <= lagtopkote OR doi IS NULL) AND "{prev}"!="{layer_name}" AND lagbottomkote <= "{prev}" AND lagbottomkote >= "{layer_name}")) OR'


                prev = layer_name

            sql = f'''
    
                            WITH geofysik_points AS (
                            SELECT DISTINCT ON (o.dataset, position, model) 
                            model, POSITION, o.dataset, geom, datatype_model
                            FROM mstgerda.geophysical_model_all mms 
                            INNER JOIN gerda.odvmodse o USING (model)
                            WHERE ST_Intersects(ST_Transform(mms.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
                            AND datatype_model IN ('skytem', 'ttem')
                            ),
    
                            fladekorrigeret AS (
                            SELECT gp.model, gp.POSITION, gp.dataset, o.layer, o.rho, ST_X(geom), ST_Y(geom), 
                            {sql_ele}
                            (ST_ValueCount(ST_Clip(r0.rast,ST_Transform(gp.geom, {crs})))).value-doi.doiupper as doi,
                            (ST_ValueCount(ST_Clip(r0.rast,ST_Transform(gp.geom, {crs})))).value-o.DEPTOP AS lagtopkote, 
                            (ST_ValueCount(ST_Clip(r0.rast,ST_Transform(gp.geom, {crs})))).value-o.DEPBOTTOM AS lagbottomkote
                            FROM geofysik_points gp
                            LEFT JOIN  gerda.odvlayer o ON o.model = gp.model AND o.position = gp.position
                            left join gerda.odvdoi doi on doi.model = gp.model and doi."position" = gp.position
    
                            {sql_join}
    
                            )
                            SELECT * FROM fladekorrigeret 
                            WHERE {sql_where}
    
    
    
                            '''
            db = Database(database='GRUKOS')
            print(sql)
            #df_gerda = db.sql_to_df(sql)
            return df_gerda

        def geofysik_fetch_fohm_custom(crs, geom, toplayer, bottomlayer, område):
            if område == 'jylland':
                topo = 'jylland_0000_terraen'
            elif område == 'fyn':
                topo = 'fyn_0001_topo'
            else:
                topo = 'sjaelland_0001_topo'
            sql = f'''
                  
                    WITH geofysik_points AS (
                    SELECT DISTINCT ON (o.dataset, position, model) 
                    model, POSITION, o.dataset, geom, datatype_model
                    FROM mstgerda.geophysical_model_all mms 
                    INNER JOIN gerda.odvmodse o USING (model)
                    WHERE ST_Intersects(ST_Transform(mms.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
                    AND datatype_model IN ('skytem', 'ttem')
                    ),
                    
                    fladekorrigeret AS (
                    SELECT gp.model, gp.POSITION, gp.dataset, o.layer, o.rho, ST_X(geom), ST_Y(geom),  
                    (ST_ValueCount(ST_Clip(ft.rast,ST_Transform(gp.geom, {crs})))).value-doi.doiupper as doi,
                    (ST_ValueCount(ST_Clip(ft.rast,ST_Transform(gp.geom, {crs})))).value-o.DEPTOP AS lagtopkote, 
                    (ST_ValueCount(ST_Clip(ft.rast,ST_Transform(gp.geom, {crs})))).value-o.DEPBOTTOM AS lagbottomkote, 
                    
                    (ST_ValueCount(ST_Clip(r1.rast,ST_Transform(gp.geom, {crs})))).value AS top,
                    (ST_ValueCount(ST_Clip(r2.rast,ST_Transform(gp.geom, {crs})))).value AS bottom
                    
                    FROM geofysik_points gp
                    LEFT JOIN  gerda.odvlayer o ON o.model = gp.model AND o.position = gp.position
                    left join gerda.odvdoi doi on doi.model = gp.model and doi."position" = gp.position
    
                    LEFT JOIN mstfohm.{topo} ft ON st_intersects(ft.rast, ST_Transform(gp.geom, {crs}))
                    LEFT JOIN mstfohm.{toplayer} r1 ON st_intersects(r1.rast, ST_Transform(gp.geom, {crs}))
                    LEFT JOIN mstfohm.{bottomlayer} r2 ON st_intersects(r2.rast, ST_Transform(gp.geom, {crs}))
                    
                    )
                    SELECT * FROM fladekorrigeret WHERE ((lagtopkote <= top AND lagtopkote >= bottom) OR (lagbottomkote <= top AND lagbottomkote >= bottom))
                    AND top != bottom
                    AND (doi <= lagtopkote OR doi IS NULL)
    
                    '''

            db = Database(database='GRUKOS')
            print(sql)

            df_gerda = db.sql_to_df(sql)
            return df_gerda

        def boringer_fetch_fohm_all(crs, geom, layer, område):

                layername = layer
                if isinstance(layername, str):
                    layer_id = int(re.search(r"\d+", layername).group())
                else:
                    layer_id = layername

                sql_comp = f'''
                               with 	
                               fohm_depth as ( --get the depth of fohm and top point of fohm
                                select DISTINCT ON (flba.boreholeid, flb.layer_num)
                                    flba.boreholeid,
                                    flba.terraintop,
                                    layer_top,
                                    layer_bot,
                                    case -- hvis ingen elevation er målt for boringen, rykkes toppen af boringen til at matche toppen af modellen
                                        when b.elevation is null
                                            then flba.terraintop
                                        else b.elevation
                                    end as borehole_elev
                                from (
                                select
                                flb.boreholeid,
                                max(flb.layer_top) as terraintop
                                from fohm_boreholes.fohm_boreholes_{område} flb
                                group by flb.boreholeid
                                ) flba
                                left join jupiter_fdw.borehole b using (boreholeid)
                                right join fohm_boreholes.fohm_boreholes_{område} flb using (boreholeid) 
                                where layer_num = {layer_id}
                                AND flb.boringskvalitet >= 3
                                AND ST_Intersects(ST_Transform(b.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
                            ),
                            ler_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('bfe','bmf','brf','fe','mfe','rfe')
                                            then 'fed ler'
                                        when ls.texture is null
                                            then 'ler'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'dl', 'fl', 'hl', 'il', 'ql',						
                                'l', 'll', 'j', 'ml', 'ol',
                                'rl', 'sl', 'u', 'vl', 'zl',
                                'yl', 'tl', 'xl', 'gl', 'pl',
                                'pr', 'ed', 'e', 'ee'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            silt_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('uso','fmg','uof','uog','mgu','gou','fgu','fmu','fou','mou')
                                            then 'usorteret silt'
                                        when ls.texture in ('bfi','fof','fin','fom','fmf','feg','ftm','fgr','fog','fgf','fgg')
                                            then 'fint silt'
                                        when ls.texture in ('mof','mgf','mgr','mog','mel','mgg','mtg','bme')
                                            then 'mellem silt'
                                        when ls.texture in ('mgo','gof','gog','gro','bgr')
                                            then 'groft silt'
                                        when ls.texture is null
                                            then 'silt'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'di', 'fi', 'hi', 'i', 'ii',						
                                'mi', 'qi', 'oi', 'yi', 'zi',
                                'ti', 'gi', 'pi'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            sand_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('uso','fmg','uof','uog','mgu','gou','fgu','fmu','fou','mou')
                                            then 'usorteret sand'
                                        when ls.texture in ('bfi','fof','fin','fom','fmf','feg','ftm','fgr','fog','fgf','fgg')
                                            then 'fint sand'
                                        when ls.texture in ('mof','mgf','mgr','mog','mel','mgg','mtg','bme')
                                            then 'mellem sand'
                                        when ls.texture in ('mgo','gof','gog','gro','bgr')
                                            then 'groft sand'
                                        when ls.texture is null
                                            then 'sand'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'ds', 'es', 'hs', 'fs', 'is',						
                                'ks', 'ms', 'os', 's', 'zs',
                                'ys', 'ts', 'qs', 'gs', 'pq',
                                'ps'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            grus_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('uso','fmg','uof','uog','mgu','gou','fgu','fmu','fou','mou')
                                            then 'usorteret grus'
                                        when ls.texture in ('bfi','fof','fin','fom','fmf','feg','ftm','fgr','fog','fgf','fgg')
                                            then 'fint grus'
                                        when ls.texture in ('mof','mgf','mgr','mog','mel','mgg','mtg','bme')
                                            then 'mellem grus'
                                        when ls.texture in ('mgo','gof','gog','gro','bgr')
                                            then 'groft grus'
                                        when ls.texture is null
                                            then 'grus'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'dg', 'fg', 'hg', 'ig', 'kg',						
                                'g', 'mg', 'qg', 'yg', 'tg'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            sten_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('uso','fmg','uof','uog','mgu','gou','fgu','fmu','fou','mou')
                                            then 'usorteret sten'
                                        when ls.texture in ('bfi','fof','fin','fom','fmf','feg','ftm','fgr','fog','fgf','fgg')
                                            then 'fint sten'
                                        when ls.texture in ('mof','mgf','mgr','mog','mel','mgg','mtg','bme')
                                            then 'mellem sten'
                                        when ls.texture in ('mgo','gof','gog','gro','bgr')
                                            then 'groft sten'
                                        when ls.texture is null
                                            then 'sten'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'dz', 'mz', 'z'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            kalk_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('sla','sts','svs','tæt')
                                            then 'slammet kalk'
                                        when ls.texture is null
                                            then 'kalk'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'dk', 'kk', 'bk', 'ak', 'as',						
                                'k', 'lk', 'sk', 'zk', 'tk',
                                'gk', 'pk', 'fk'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            organisk_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('sla','sts','svs','tæt')
                                            then 'fed organisk'
                                        when ls.texture is null
                                            then 'organisk'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'hp', 'ht', 'id', 'fp', 'c',						
                                'ip', 'it', 'p', 'tp', 't',
                                'yp', 'yt', 'tt', 'gc', 'qp',
                                'qt', 'ft', 'gp'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            vekslende_interval as (
                                select 			
                                    fd.boreholeid,
                                    'vekslende' as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'dv', 'hv', 'iv', 'mv', 'yv',						
                                'v', 'tv', 'qv', 'gv', 'fv',
                                'ev', 'pv', 'av', 'o', 'ij',
                                'fj', 'm'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            ukendt_interval as (
                                select 			
                                    fd.boreholeid,
                                    'ukendt' as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'b', 'x', 'h'
                                )
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            andet_interval as (
                                select 			
                                    fd.boreholeid,
                                    'andet' as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'dr', 'cj', 'ck', 'cl', 'cq', 'cr', 'cv', 'cw', 'eq', 'ar',
                                'kj', 'kl', 'kq', 'kr', 'jc', 'ji', 'jj', 'jl', 'jq', 'jr',
                                'js', 'jv', 'ka', 'nq', 'nr', 'nw', 'r', 'mk', 'nd', 'nj',
                                'nk', 'nl', 'oq', 'or', 'q', 'rq', 'rr', 'rs', 'rv', 'sj',						
                                'sr', 'tr', 'wr', 'xk', 'xq', 'xr', 'wl', 'qk', 'rc', 'rg',
                                'rj', 'rk', 'vc', 'vi', 'vj', 'vk', 'vq', 'vr', 'vs', 'vv',
                                'w', 'wk', 'ok', 'oe', 'gr', 'ls', 'gf', 'ek', 'bl', 'bs',
                                'bv', 'd', 'pj', 'uc', 'ui', 'uj', 'ul', 'uq', 'ur', 'us',
                                'uv', 'el', 'f', 'a', 'pa', 'pd', 'af', 'al'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            fohm_soil_types as (
                                select			
                                    boreholeid,
                                    soil_type,
                                    sum(tykkelse)  as tykkelse_fohmlag -- alle ens lithologier i fohmlagets interval i en boring lægges sammen
                                from ler_interval
                                full outer join silt_interval using (boreholeid,soil_type,tykkelse)
                                full outer join sand_interval using (boreholeid,soil_type,tykkelse)
                                full outer join grus_interval using (boreholeid,soil_type,tykkelse)
                                full outer join sten_interval using (boreholeid,soil_type,tykkelse)
                                full outer join kalk_interval using (boreholeid,soil_type,tykkelse)
                                full outer join organisk_interval using (boreholeid,soil_type,tykkelse)
                                full outer join vekslende_interval using (boreholeid,soil_type,tykkelse)
                                full outer join ukendt_interval using (boreholeid,soil_type,tykkelse)
                                full outer join andet_interval using (boreholeid,soil_type,tykkelse)
                                group by boreholeid,soil_type
                                ),
                                slutprodukt AS (

                    select 
                        b.boreholeno, st_X(b.geom), st_Y(b.geom),
                        fst.*,
                        tykkelse_fohmlag/(fd.layer_top-fd.layer_bot)*100 as proc_fohmlag --omregn tykkelse af lithologi i boringen til procentdel af fohmlagets totale tykkelse
                    from fohm_soil_types fst
                    left join fohm_depth fd using (boreholeid)
                    left join jupiter_fdw.borehole b using (boreholeid)
                    )


                    SELECT boreholeno,
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'fed ler'), 0)  AS "fed ler",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'ler'), 0)  AS "ler",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'silt'), 0)  AS "silt",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'fint silt'), 0)  AS "fint silt",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'mellem silt'), 0)  AS "mellem silt",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'grov silt'), 0)  AS "grov silt",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'usorteret silt'), 0)  AS "usorteret silt",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'sand'), 0)  AS "sand",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'fint sand'), 0)  AS "fint sand",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'mellem sand'), 0)  AS "mellem sand",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'grov sand'), 0)  AS "grov sand",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'usorteret sand'), 0)  AS "usorteret sand",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'grus'), 0)  AS "grus",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'fint grus'), 0)  AS "fint grus",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'mellem grus'), 0)  AS "mellem grus",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'grov grus'), 0)  AS "grov grus",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'usorteret grus'), 0)  AS "usorteret grus",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'sten'), 0)  AS "sten",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'fint sten'), 0)  AS "fint sten",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'mellem sten'), 0)  AS "mellem sten",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'grov sten'), 0)  AS "grov sten",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'usorteret sten'), 0)  AS "usorteret sten",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'kalk'), 0)  AS "kalk",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'slammet kalk'), 0)  AS "slammet kalk",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'organisk'), 0)  AS "organisk",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'fed organisk'), 0)  AS "fed organisk",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'vekslende'), 0)  AS "vekslende",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'ukendt'), 0)  AS "ukendt",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'andet'), 0)  AS "andet",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'dårligt beskrevet, usikker'), 0)  AS "dårligt beskrevet, usikker",
                    st_X, st_Y
                    FROM slutprodukt
                    group by boreholeno, st_X, st_Y
                    ;

                            '''
                db = Database(database='GRUKOS')
                print(sql_comp)
                #df_fohm = db.sql_to_df(sql_comp)
                return df_fohm

        # Mulighed 1 - FOHM - enkelt lag
        if self.comboBox_tolkningsvurdering.currentText() == '1.1: FOHM - enkelt lag':
            wanted_layer = self.comboBox_wanted.currentText()
            #Sand og kalk numre definres - benyttes til forventet "vandførende" og "vandstandsende" lag.
            if wanted_layer.startswith('Fyn'):
                område = 'fyn'
                ident = wanted_layer.split(' ')[-1] #identifier til boringsudtræk
                ident_geofysik = wanted_layer.split(' ')[2] #identifier til geofysikudtræk
                sand = [3,5,7]
                kalk = [10]
            elif wanted_layer.startswith('Sjæl'):
                område = 'sjaelland'
                ident = wanted_layer.split(' ')[-1] #identifier til boringsudtræk
                ident_geofysik = wanted_layer.split(' ')[2] #identifier til geofysikudtræk

                sand = [3, 5, 7, 9]
                kalk = [12, 13]
            else:
                område = 'jylland'
                ident = wanted_layer.split(' ')[0] #identifier til boringsudtræk
                ident_geofysik = wanted_layer.split(' ')[0] #identifier til geofysikudtræk

                sand = [120, 140, 200, 400, 1200, 1400, 2100, 2300, 5200, 5400, 5600, 5800, 6000, 6200, 6400, 6600, 6800,
                        7000, 7200, 7400, 7600, 7800]
                kalk = [10, 8500, 9000]

            #### BORINGER ####
            # Boringsudtræk og load til QGIS
            try:
                df_boreholes = boringer_fetch_fohm_all(crs, geom, ident, område) #Boringsudtræk
                layer_id = int(re.search(r"\d+", ident).group()) #Finder Layer_id

                # Summen af alle lithologitabeller (da disse ikke giver 100, benyttes summen)
                df_boreholes['sum'] = df_boreholes[["fed ler", "ler","silt", "fint silt", "mellem silt", "grov silt",   "usorteret silt","sand",  "fint sand",   "mellem sand","grov sand", "usorteret sand",
                                                     "grus", "fint grus", "mellem grus","grov grus", "usorteret grus","sten", "fint sten", "mellem sten","grov sten","usorteret sten",
                                                     "kalk", "slammet kalk","organisk","fed organisk", "vekslende", "ukendt", "andet"]].sum(axis=1)

                #Summen af de aktuelle lithoglogier findes
                if layer_id in sand:
                    df_boreholes['wanted_sum'] = df_boreholes.filter(regex='sand|grus|sten').sum(axis=1)
                elif layer_id in kalk:
                    df_boreholes['wanted_sum'] = df_boreholes.filter(like='kalk').sum(axis=1)
                else:
                    df_boreholes['wanted_sum'] = df_boreholes.filter(regex='ler|silt').sum(axis=1)


                # Her udregnes afvigelsen mellem den aktuelle/forventede/ønskede sum (df_boreholes['wanted_sum']) og den samlede sum (df_boreholes['sum'])
                df_boreholes['afvigelse'] = abs((df_boreholes['wanted_sum']-df_boreholes['sum'])/df_boreholes['sum']) *100

                # QGIS temporary lag opstilles
                temp_boringer = QgsVectorLayer('Point?crs=epsg:25832',
                                      "BoringerSikkerhed - {}".format(self.comboBox_wanted.currentText()), 'memory')
                temp_boringer_data = temp_boringer.dataProvider()
                temp_boringer.startEditing()
                #Der tilføjes attributer til QGIS temporary laget
                temp_boringer.addAttribute(QgsField("boreholeno", QVariant.String))
                temp_boringer.addAttribute(QgsField("afvigelse_procentvis", QVariant.Double))
                temp_boringer.updateFields()

                #Geometrien for hvert geofysisk punkt indhentes og tilføjes som feature til QGIS temporary laget
                for row in df_boreholes.itertuples():
                    f = QgsFeature()
                    f.setFields(temp_boringer.fields())
                    point = QgsPointXY(row[-5], row[-4])
                    f.setGeometry(QgsGeometry.fromPointXY(point))
                    temp_boringer.dataProvider().addFeature(f)

                temp_boringer.updateExtents()
                temp_boringer.commitChanges()


                # Attributværdi for boreholeno og afvigelse tilføjes til QGIS temporary laget
                temp_boringer.startEditing()
                for i, f in enumerate(temp_boringer.getFeatures()):
                    id = str(df_boreholes.iloc[i]["boreholeno"])
                    temp_boringer.changeAttributeValue(f.id(), temp_boringer.fields().indexFromName("boreholeno"), id)

                    value = float(df_boreholes.iloc[i]["afvigelse"].astype(float))

                    if abs(value) >= 0 and abs(value) < 100000:
                        temp_boringer.changeAttributeValue(f.id(), temp_boringer.fields().indexFromName("afvigelse_procentvis"),
                                                  value)  # (feature id, field index, value)

                temp_boringer.updateExtents()
                temp_boringer.commitChanges()

                #Laget tilføjes til QGIS
                project.addMapLayer(temp_boringer)
                temp_boringer.setName("BoringerSikkerhed - {}".format(self.comboBox_wanted.currentText()))
            except:
                pass

            #### GEOFYSIK ####
            # Geofysisk udtræk og load til QGIS
            try:
                df_table_name = fetch_table_names(område)
                alllayers = df_table_name['table_name'].tolist() #Alle lag findes

                bundlayer_index = [idx for idx, s in enumerate(alllayers) if ident_geofysik.lower() in s][0] #Det ønskede lag "WANTED LAYER"'s index i alllayers findes
                toplayer_index =  bundlayer_index-1 #Laget ovenover (altså top af lag)

                df_gerda = geofysik_fetch_fohm_custom(crs, geom, alllayers[toplayer_index], alllayers[bundlayer_index], område) #Geofysik udtrækket udføres
                df_gerda_grp = df_gerda.groupby(['dataset', 'position', 'model']) #Udtræk grupperes

                #Tomme lister opstilles så de nemt kan indlæses i QGIS laget
                id = []
                geometri_x = []
                geometri_y = []
                doi = []
                modstanden = []
                samlet_middelmodstand = []
                samlet_median = []

                #Itererer geofysik punkter
                for key, grp in df_gerda_grp:
                    max_index = len(grp)-1


                    id.append(key) #Boreholeno gemmes
                    geometri_x.append(grp.iloc[0]['st_x']) #X-koordinat gemmes
                    geometri_y.append(grp.iloc[0]['st_y']) #Y-koordinat gemmes
                    model_middelmodstand = df_gerda[df_gerda['dataset'] == key[0]]["rho"].mean() #Den samlede middelmodstand for hele datasættet udregnes
                    model_median = df_gerda[df_gerda['dataset'] == key[0]]["rho"].median() #Den samlede median for hele datasættet udregnes
                    samlet_middelmodstand.append(model_middelmodstand) # Den samlede middelmodstand gemmes (bare en liste af den samme værdi hver gang)
                    samlet_median.append(model_median) # Den samlede median gemmes (bare en liste af den samme værdi hver gang)
                    doi.append(grp.iloc[0]['doi']) #DOI gemmes

                    new_rho = [] #Liste til middelmodstand for sonderingen indenfor laget

                    # Vi itererer lagene per sondering
                    for index, (row_index, row) in enumerate(grp.iterrows()):
                        tykkelse = abs(row['top'] - row['bottom']) #Tykkelsen af FOHM-laget
                        #Her udregnes middelmodstanden for sonderingen. Dette gøres relativt til tykkelserne af lagene:
                        if index == 0: #Første lag (toppen af fohm til bunden af det første lag)
                            new_rho.append((abs(row['top'] - row['lagbottomkote']) / tykkelse) * abs(row['rho']))
                        elif index == max_index: #Sidste lag (toppen af sidste geofysik lag til bunden af FOHM-laget)
                            new_rho.append((abs(row['lagtopkote'] - row['bottom']) / tykkelse) * abs(row['rho']))
                        else: #Lag inde imellem første og sidste lag (blot tykkelsen af det pågældende lag)
                            new_rho.append((abs(row['lagtopkote'] - row['lagbottomkote']) / tykkelse) * abs(row['rho']))


                    modstanden.append(sum(new_rho)) # Alle disse lagtykkelse-relative modstand summeres til 1 middelmodstand.

                #Der opstilles en DataFrame for alle disse lister
                new_dataframe = pd.DataFrame(list(zip(id, geometri_x, geometri_y, modstanden, samlet_middelmodstand, samlet_median)),
                                             columns=['ID', 'X', 'Y', 'Middelmodstand', 'samlet_middelmodstand', 'samlet_median'])


                #Vi laver et nyt QGIS temporary layer.
                temp = QgsVectorLayer('Point?crs=epsg:25832', "GeofysiskSikkerhed - {}".format(self.comboBox_wanted.currentText()), 'memory')

                temp_data = temp.dataProvider()
                temp.startEditing()

                #TIlføjer columns til QGIS laget
                temp.addAttribute(QgsField("ID", QVariant.String))
                temp.addAttribute(QgsField("Middelmodstand", QVariant.Double))
                temp.addAttribute(QgsField("samlet_middelmodstand", QVariant.Double))
                temp.addAttribute(QgsField("samlet_median", QVariant.Double))
                if self.resistivitestboundaries_specific.isChecked():
                    temp.addAttribute(QgsField("min", QVariant.Double))
                    temp.addAttribute(QgsField("max", QVariant.Double))

                #temp.addAttribute(QgsField("doi", QVariant.Double))

                temp.updateFields()

                #Tildeler geometri og laver features
                for row in new_dataframe.itertuples():
                     f = QgsFeature()
                     f.setFields(temp.fields())
                     point = QgsPointXY(row[2], row[3])
                     f.setGeometry(QgsGeometry.fromPointXY(point))
                     temp.dataProvider().addFeature(f)

                temp.updateExtents()
                temp.commitChanges()

                temp.startEditing()

                #Indsætter attributværdier til features
                for i, f in enumerate(temp.getFeatures()):
                    id = str(new_dataframe.iloc[i]["ID"])
                    temp.changeAttributeValue(f.id(), temp.fields().indexFromName("ID"), id)

                    value = float(new_dataframe.iloc[i]["Middelmodstand"].astype(float))
                    value2 = float(new_dataframe.iloc[i]["samlet_middelmodstand"].astype(float))
                    value3 = float(new_dataframe.iloc[i]["samlet_median"].astype(float))
                  #  value4 = float(new_dataframe.iloc[i]["doi"].astype(float))

                    if value > 0 and value < 100000:
                        temp.changeAttributeValue(f.id(), temp.fields().indexFromName("Middelmodstand"), value)  # (feature id, field index, value)
                    if value2 > 0 and value2 < 100000:
                        temp.changeAttributeValue(f.id(), temp.fields().indexFromName("samlet_middelmodstand"), value2)
                    if value3 > 0 and value3 < 100000:
                        temp.changeAttributeValue(f.id(), temp.fields().indexFromName("samlet_median"), value3)

                    if self.resistivitestboundaries_specific.isChecked():
                        temp.changeAttributeValue(f.id(), temp.fields().indexFromName("min"), self.resspecific_min.value())
                        temp.changeAttributeValue(f.id(), temp.fields().indexFromName("max"), self.resspecific_max.value())

                 #   if abs(value4) > 0 and abs(value4) < 100000:
                    #    temp.changeAttributeValue(f.id(), temp.fields().indexFromName("doi"), value4)
                temp.updateExtents()
                temp.commitChanges()

                # Udregner afvigelse fra samlet middelmostand og median på geofysikpunkter
                temp1 = processing.run("native:fieldcalculator",
                               {'INPUT': temp,
                                'FIELD_NAME': 'afvigelse_middelmodstand', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0,
                                'FIELD_PRECISION': 0,
                                'FORMULA': 'abs(  (( "Middelmodstand" - "samlet_middelmodstand" )/ "samlet_middelmodstand")*100  )',
                                'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

                geofysikpunkt = processing.run("native:fieldcalculator", {
                    'INPUT': temp1,
                    'FIELD_NAME': 'afvigelse_median', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
                    'FORMULA': 'abs((( "Middelmodstand" - "samlet_median" )/ "samlet_median" )*100)',
                    'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']



                geofysikpunkter = processing.run("native:fieldcalculator", {
                    'INPUT': geofysikpunkt,
                    'FIELD_NAME': 'afvigelse_range', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
                    'FORMULA': 'CASE WHEN "Middelmodstand" <= "max" and "Middelmodstand" >= "min" THEN 0 WHEN "Middelmodstand" <= "max" and "Middelmodstand" <= "min" then abs((( "Middelmodstand" - "min" )/ "min" )*100) WHEN "Middelmodstand" >= "max" and "Middelmodstand" >= "min" then abs((( "Middelmodstand" - "max" )/ "max" )*100) END',
                    'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']



                project.addMapLayer(geofysikpunkter)
                geofysikpunkter.setName("GeofysiskSikkerhed - {}".format(self.comboBox_wanted.currentText()))
            except:
                pass




            # Laver grid
            grid = processing.run("native:creategrid", {'TYPE': 2,
                                                 'EXTENT': layer.extent(),
                                                 'HSPACING': 100, 'VSPACING': 100, 'HOVERLAY': 0, 'VOVERLAY': 0,
                                                 'CRS': QgsCoordinateReferenceSystem('EPSG:{}'.format(crs)),
                                                 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            grid.startEditing()
            grid.addAttribute(QgsField("geofysik_mean_afvigelse_middelmodstand", QVariant.Double))
            grid.addAttribute(QgsField("geofysik_mean_afvigelse_median", QVariant.Double))
            grid.addAttribute(QgsField("geofysik_mean_afvigelse_range", QVariant.Double))
            grid.addAttribute(QgsField("boringer_mean_afvigelse", QVariant.Double))


            grid.updateFields()



            project.addMapLayer(grid)

            grid.setName("Grid - Usikkerhedsvurdering")


            #Iterer igennem gridceller
            for feature in grid.getFeatures():
                grid.select(feature.id()) #Vælger cellen

                #Vælger geofysiskepunkter indenfor cellen
                processing.run("native:selectbylocation",
                               {'INPUT': geofysikpunkter,
                                'PREDICATE': [0, 6], 'INTERSECT': QgsProcessingFeatureSourceDefinition(
                                   grid.source(), selectedFeaturesOnly=True, featureLimit=-1,geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid), 'METHOD': 0})

                #Vælger boringspunkter indenfor cellen
                processing.run("native:selectbylocation",
                               {'INPUT': temp_boringer,
                                'PREDICATE': [0, 6], 'INTERSECT': QgsProcessingFeatureSourceDefinition(
                                   grid.source(), selectedFeaturesOnly=True, featureLimit=-1,
                                   geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid), 'METHOD': 0})

                #Udregner den gennemsnitlige værdi af afvigelse af middelmodstand indenfor cellen
                statistics = processing.run("qgis:basicstatisticsforfields", {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                    geofysikpunkter.source(),
                    selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),
                                                                 'FIELD_NAME': 'afvigelse_middelmodstand',
                                                                 'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})


                # Udregner den gennemsnitlige værdi af afvigelse af boring indenfor cellen
                statistics_boring = processing.run("qgis:basicstatisticsforfields",
                                            {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                temp_boringer.source(),
                                                selectedFeaturesOnly=True, featureLimit=-1,
                                                geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),
                                                'FIELD_NAME': 'afvigelse_procentvis',
                                                'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})

                boringer_mean_afvigelse = statistics_boring["MEAN"]

                mean_afvigelse_middelmodstand = statistics["MEAN"]

                # Udregner den gennemsnitlige værdi af afvigelse af median indenfor cellen
                statistics2 = processing.run("qgis:basicstatisticsforfields",
                                            {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                geofysikpunkter.source(),
                                                selectedFeaturesOnly=True, featureLimit=-1,
                                                geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),
                                                'FIELD_NAME': 'afvigelse_median',
                                                'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})



                mean_afvigelse_median = statistics2["MEAN"]

                statistics3 = processing.run("qgis:basicstatisticsforfields",
                                             {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                 geofysikpunkter.source(),
                                                 selectedFeaturesOnly=True, featureLimit=-1,
                                                 geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),
                                                 'FIELD_NAME': 'afvigelse_range',
                                                 'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})

                mean_afvigelse_range = statistics3["MEAN"]

                # Tilføjer celle afgrænsedede afvigelser til grid celler i QGIS
                grid.changeAttributeValue(feature.id(), grid.fields().indexFromName("geofysik_mean_afvigelse_middelmodstand"), mean_afvigelse_middelmodstand)
                grid.changeAttributeValue(feature.id(), grid.fields().indexFromName("geofysik_mean_afvigelse_median"), mean_afvigelse_median)
                grid.changeAttributeValue(feature.id(), grid.fields().indexFromName("geofysik_mean_afvigelse_range"), mean_afvigelse_range)

                grid.changeAttributeValue(feature.id(), grid.fields().indexFromName("boringer_mean_afvigelse"), boringer_mean_afvigelse)

                #Fjerne selection for at køre igen
                grid.removeSelection()
                geofysikpunkter.removeSelection()
                temp_boringer.removeSelection()


            #Udregner den samlede afvigelse (Middelværdi af middelmodstand og afvigelse i boringer)
            grid.updateExtents()
            grid.commitChanges()
          #  grid_final = processing.run("native:fieldcalculator", {
          #      'INPUT': grid,
          #      'FIELD_NAME': 'samlet_afvigelse', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
          #      'FORMULA': 'CASE\r\nWHEN "geofysik_mean_afvigelse_middelmodstand" = \'nan\' and "boringer_mean_afvigelse" = \'nan\' then \'nan\' \r\nWHEN "geofysik_mean_afvigelse_middelmodstand" != \'nan\' and "boringer_mean_afvigelse" = \'nan\' then   "geofysik_mean_afvigelse_middelmodstand"\r\nWHEN "geofysik_mean_afvigelse_middelmodstand" = \'nan\' and "boringer_mean_afvigelse" != \'nan\' then "boringer_mean_afvigelse"\r\nelse "geofysik_mean_afvigelse_middelmodstand"+ "boringer_mean_afvigelse" END',
          #      'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            grid_final = processing.run("native:fieldcalculator", {
                'INPUT': grid,
                'FIELD_NAME': 'samlet_afvigelse', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
                'FORMULA': 'CASE\r\nWHEN "geofysik_mean_afvigelse_range" = \'nan\' and "boringer_mean_afvigelse" = \'nan\' then \'nan\' \r\nWHEN "geofysik_mean_afvigelse_range" != \'nan\' and "boringer_mean_afvigelse" = \'nan\' then   "geofysik_mean_afvigelse_range"\r\nWHEN "geofysik_mean_afvigelse_range" = \'nan\' and "boringer_mean_afvigelse" != \'nan\' then "boringer_mean_afvigelse"\r\nelse "geofysik_mean_afvigelse_range"+ "boringer_mean_afvigelse" END',
                'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            project.removeMapLayer(grid)

            project.addMapLayer(grid_final)
            grid_final.setName("Grid - Usikkerhedsvurdering")


            grid_final.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Grid - Usikkerhedsvurdering.qml")


        elif self.comboBox_tolkningsvurdering.currentText() == '1.2: FOHM - alle lag (KRÆVENDE)':

            #Opstiller vandførende lag og definerer områder
            if self.comboBox_area.currentText() == 'Jylland': #Definerer område
                område = 'jylland'
                sand = [120, 140, 200, 400, 1200, 1400, 2100, 2300, 5200, 5400, 5600, 5800, 6000, 6200, 6400, 6600,
                        6800,
                        7000, 7200, 7400, 7600, 7800]
                kalk = [10, 8500, 9000]
            elif self.comboBox_area.currentText() == 'Fyn':
                område = 'fyn'
                sand = [3, 5, 7]
                kalk = [10]
            else:
                område = 'sjaelland'
                sand = [3, 5, 7, 9]
                kalk = [12, 13]

            # Indhenter alle lag
            df_table_name = fetch_table_names(område)
            alllayers = df_table_name['table_name'].tolist()
            for lay in alllayers[1:]:
                df_boreholes = boringer_fetch_fohm_all(crs, geom, lay, område)
                layer_id = int(re.search(r"\d+", lay).group())

                if len(df_boreholes) != 0:
                    df_boreholes['sum'] = df_boreholes[
                        ["fed ler", "ler", "silt", "fint silt", "mellem silt", "grov silt", "usorteret silt", "sand",
                         "fint sand", "mellem sand", "grov sand", "usorteret sand",
                         "grus", "fint grus", "mellem grus", "grov grus", "usorteret grus", "sten", "fint sten",
                         "mellem sten", "grov sten", "usorteret sten",
                         "kalk", "slammet kalk", "organisk", "fed organisk", "vekslende", "ukendt", "andet"]].sum(
                        axis=1)

                    if layer_id in sand:
                        df_boreholes['wanted_sum'] = df_boreholes.filter(regex='sand|grus|sten').sum(axis=1)
                    elif layer_id in kalk:
                        df_boreholes['wanted_sum'] = df_boreholes.filter(like='kalk').sum(axis=1)
                    else:
                        df_boreholes['wanted_sum'] = df_boreholes.filter(regex='ler|silt').sum(axis=1)

                    df_boreholes['afvigelse'] = abs(
                        (df_boreholes['wanted_sum'] - df_boreholes['sum']) / df_boreholes['sum']) * 100

                    temp_boringer = QgsVectorLayer('Point?crs=epsg:25832',
                                                   "BoringerSikkerhed - {}".format(self.comboBox_wanted.currentText()),
                                                   'memory')

                    temp_boringer_data = temp_boringer.dataProvider()
                    temp_boringer.startEditing()
                    temp_boringer.addAttribute(QgsField("boreholeno", QVariant.String))
                    temp_boringer.addAttribute(QgsField("afvigelse_procentvis", QVariant.Double))

                    # temp.addAttribute(QgsField("doi", QVariant.Double))

                    temp_boringer.updateFields()
                    # Addition of features
                    # [1] because i don't want the indexes
                    for row in df_boreholes.itertuples():
                        f = QgsFeature()
                        f.setFields(temp_boringer.fields())
                        point = QgsPointXY(row[-5], row[-4])
                        f.setGeometry(QgsGeometry.fromPointXY(point))
                        temp_boringer.dataProvider().addFeature(f)

                    temp_boringer.updateExtents()
                    temp_boringer.commitChanges()
                    temp_boringer.startEditing()

                    for i, f in enumerate(temp_boringer.getFeatures()):
                        id = str(df_boreholes.iloc[i]["boreholeno"])
                        temp_boringer.changeAttributeValue(f.id(), temp_boringer.fields().indexFromName("boreholeno"),
                                                           id)

                        value = float(df_boreholes.iloc[i]["afvigelse"].astype(float))

                        if abs(value) >= 0 and abs(value) < 100000:
                            temp_boringer.changeAttributeValue(f.id(),
                                                               temp_boringer.fields().indexFromName(
                                                                   "afvigelse_procentvis"),
                                                               value)  # (feature id, field index, value)

                    temp_boringer.updateExtents()
                    temp_boringer.commitChanges()

                    project.addMapLayer(temp_boringer)
                    temp_boringer.setName("BoringerSikkerhed - {}".format(lay))

                else:
                    pass
            #Indhenter geofysisk data for alle lagene
            df_gerda = geofysik_fetch_fohm_all(crs, geom, alllayers)

            # Geofysik
            for i, lay in enumerate(alllayers[1:]):  # Undgå Topografien
                #OPstiller lister til nemmere at indlæse data i QGIS
                id = []
                geometri_x = []
                geometri_y = []

                modstanden = []
                samlet_middelmodstand = []
                samlet_median = []

                #Finder toppen af laget
                previous_layer = re.search("(\d+.[A-Za-z0-9ÆØÅæøå]+.[A-Za-z0-9ÆØÅæøå]+)", alllayers[i])[0]
                #Finder bunden af laget
                layername = re.search("(\d+.[A-Za-z0-9ÆØÅæøå]+.[A-Za-z0-9ÆØÅæøå]+)", lay)[0]

                #Sortetrer og opstiller en ny dataframe kun for det specifikke lag
                df_gerda_layer_specific = df_gerda[((df_gerda["lagtopkote"] <= df_gerda[
                    "{}".format(previous_layer)]) & (df_gerda["lagtopkote"] >= df_gerda["{}".format(layername)])) | ((
                      df_gerda["lagbottomkote"] <= df_gerda["{}".format(previous_layer)]) & (df_gerda["lagbottomkote"] >= df_gerda["{}".format(layername)]))]

                #Sørger for at top og bund ikke er det samme (altså at laget er tilstede (burde være overflødig))
                df_gerda_layer_specific = df_gerda_layer_specific[(df_gerda_layer_specific["{}".format(previous_layer)] != df_gerda_layer_specific["{}".format(layername)])]

                #Hvis der er geofysik for laget så fortsætter koden
                if len(df_gerda_layer_specific) != 0:
                    #Gruppering og iterer over de enkelte sonderinger
                    df_gerda_layer_grp = df_gerda_layer_specific.groupby(['dataset', 'position', 'model'])

                    for key, grp in df_gerda_layer_grp:
                        max_index = len(grp) - 1
                        id.append(key)  #Gemmer boreholeno
                        geometri_x.append(grp.iloc[0]['st_x']) #Gemmer x-koordinatet
                        geometri_y.append(grp.iloc[0]['st_y']) #Gemmer y-koordinatet
                        model_middelmodstand = df_gerda_layer_specific[df_gerda_layer_specific['dataset'] == key[0]]["rho"].mean() #Gemmer samlet middelmodstand for hele datasættet
                        model_median = df_gerda_layer_specific[df_gerda_layer_specific['dataset'] == key[0]]["rho"].median() # Gemmer samlet median for hele datasættet
                        samlet_middelmodstand.append(model_middelmodstand)
                        samlet_median.append(model_median)

                        #Udregnes gennemsnitlig middelmodstand for sonderingen
                        new_rho = []
                        for index, (row_index, row) in enumerate(grp.iterrows()):
                            tykkelse = abs(row["{}".format(previous_layer)] - row["{}".format(layername)])
                            if index == 0:
                                new_rho.append(
                                    (abs(row['{}'.format(previous_layer)] - row['lagbottomkote']) / tykkelse) * row['rho'])
                            elif index == max_index:
                                new_rho.append(
                                    (abs(row['{}'.format(layername)] - row['lagtopkote']) / tykkelse) * row['rho'])
                            else:
                                new_rho.append((abs(row['lagtopkote'] - row['lagbottomkote']) / tykkelse) * row['rho'])

                        modstanden.append(sum(new_rho))

                    #Opstiller ny dataframe med hvad der skal ind i QGIS
                    new_dataframe = pd.DataFrame(
                        list(zip(id, geometri_x, geometri_y, modstanden, samlet_middelmodstand, samlet_median)),
                        columns=['ID', 'X', 'Y', 'Middelmodstand', 'samlet_middelmodstand', 'samlet_median'])

                    #Laver temporært QGIS lag
                    temp = QgsVectorLayer('Point?crs=epsg:25832',
                                          "GeofysiskSikkerhed - {}".format(lay), 'memory')

                    temp_data = temp.dataProvider()
                    temp.startEditing()

                    #Tilføjer attributter/columns til laget
                    temp.addAttribute(QgsField("ID", QVariant.String))
                    temp.addAttribute(QgsField("Middelmodstand", QVariant.Double))
                    temp.addAttribute(QgsField("samlet_middelmodstand", QVariant.Double))
                    temp.addAttribute(QgsField("samlet_median", QVariant.Double))

                    temp.updateFields()

                    #Tilføjer features til laget og giver dem en geometri
                    for row in new_dataframe.itertuples():
                        f = QgsFeature()
                        f.setFields(temp.fields())
                        point = QgsPointXY(row[2], row[3])
                        f.setGeometry(QgsGeometry.fromPointXY(point))
                        temp.dataProvider().addFeature(f)

                    temp.updateExtents()
                    temp.commitChanges()

                    temp.startEditing()

                    #Tilføjer attributværdier til lagets features
                    for i, f in enumerate(temp.getFeatures()):
                        id = str(new_dataframe.iloc[i]["ID"])
                        temp.changeAttributeValue(f.id(), temp.fields().indexFromName("ID"), id)

                        value = float(new_dataframe.iloc[i]["Middelmodstand"].astype(float))
                        value2 = float(new_dataframe.iloc[i]["samlet_middelmodstand"].astype(float))
                        value3 = float(new_dataframe.iloc[i]["samlet_median"].astype(float))

                        if value > 0 and value < 100000:
                            temp.changeAttributeValue(f.id(), temp.fields().indexFromName("Middelmodstand"),value)
                        if value2 > 0 and value < 100000:
                            temp.changeAttributeValue(f.id(), temp.fields().indexFromName("samlet_middelmodstand"), value2)
                        if value3 > 0 and value < 100000:
                            temp.changeAttributeValue(f.id(), temp.fields().indexFromName("samlet_median"), value3)

                    temp.updateExtents()
                    temp.commitChanges()

                    # Udregner afvigelse fra samlet middelmostand og median på geofysikpunkter
                    temp1 = processing.run("native:fieldcalculator",
                                           {'INPUT': temp,
                                            'FIELD_NAME': 'afvigelse_middelmodstand', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0,
                                            'FIELD_PRECISION': 0,
                                            'FORMULA': 'abs((( "Middelmodstand" - "samlet_middelmodstand" )/ "samlet_middelmodstand" )*100)',
                                            'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                    geofysikpunkter = processing.run("native:fieldcalculator", {
                        'INPUT': temp1,
                        'FIELD_NAME': 'afvigelse_median', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
                        'FORMULA': 'abs((( "Middelmodstand" - "samlet_median" )/ "samlet_median" )*100)',
                        'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                    geofysikpunkter.setName("GeofysiskSikkerhed - {}".format(lay))

                    QgsProject.instance().addMapLayer(geofysikpunkter)

            # Boringer




            # Laver grid
            for i, lay in enumerate(alllayers[1:]):
                try:
                    geofysikpunkter = project.mapLayersByName("GeofysiskSikkerhed - {}".format(lay))[0]
                except:
                    geofysikpunkter = None
                try:
                    temp_boringer = project.mapLayersByName("BoringerSikkerhed - {}".format(lay))[0]
                except:
                    temp_boringer = None



                if geofysikpunkter != None or temp_boringer != None:
                    grid = processing.run("native:creategrid", {'TYPE': 2,
                                                         'EXTENT': layer.extent(),
                                                         'HSPACING': 100, 'VSPACING': 100, 'HOVERLAY': 0, 'VOVERLAY': 0,
                                                         'CRS': QgsCoordinateReferenceSystem('EPSG:{}'.format(crs)),
                                                         'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                    grid.startEditing()
                    grid.addAttribute(QgsField("geofysik_mean_afvigelse_middelmodstand", QVariant.Double))
                    grid.addAttribute(QgsField("geofysik_mean_afvigelse_median", QVariant.Double))
                    grid.addAttribute(QgsField("boringer_mean_afvigelse", QVariant.Double))

                    grid.updateFields()

                    project.addMapLayer(grid)



                    for feature in grid.getFeatures():
                        grid.select(feature.id())

                        if temp_boringer != None:
                            processing.run("native:selectbylocation",
                                           {'INPUT': temp_boringer,
                                            'PREDICATE': [0, 6], 'INTERSECT': QgsProcessingFeatureSourceDefinition(
                                               grid.source(), selectedFeaturesOnly=True, featureLimit=-1,
                                               geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid), 'METHOD': 0})
                            statistics_boring = processing.run("qgis:basicstatisticsforfields",
                                                               {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                                   temp_boringer.source(),
                                                                   selectedFeaturesOnly=True, featureLimit=-1,
                                                                   geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),
                                                                   'FIELD_NAME': 'afvigelse_procentvis',
                                                                   'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})
                            boringer_mean_afvigelse = statistics_boring["MEAN"]
                            grid.changeAttributeValue(feature.id(), grid.fields().indexFromName("boringer_mean_afvigelse"),
                                                      boringer_mean_afvigelse)
                            temp_boringer.removeSelection()

                        else:
                            grid.changeAttributeValue(feature.id(), grid.fields().indexFromName("boringer_mean_afvigelse"),
                                                      'nan')

                        if geofysikpunkter != None:
                            processing.run("native:selectbylocation",
                                           {'INPUT': geofysikpunkter,
                                            'PREDICATE': [0, 6], 'INTERSECT': QgsProcessingFeatureSourceDefinition(
                                               grid.source(), selectedFeaturesOnly=True, featureLimit=-1,
                                               geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid), 'METHOD': 0})
                            statistics = processing.run("qgis:basicstatisticsforfields",
                                                        {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                            geofysikpunkter.source(),
                                                            selectedFeaturesOnly=True, featureLimit=-1,
                                                            geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),
                                                            'FIELD_NAME': 'afvigelse_middelmodstand',
                                                            'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})
                            mean_afvigelse_middelmodstand = statistics["MEAN"]
                            grid.changeAttributeValue(feature.id(),
                                                      grid.fields().indexFromName("geofysik_mean_afvigelse_middelmodstand"),
                                                      mean_afvigelse_middelmodstand)
                            statistics2 = processing.run("qgis:basicstatisticsforfields",
                                                         {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                             geofysikpunkter.source(),
                                                             selectedFeaturesOnly=True, featureLimit=-1,
                                                             geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),
                                                             'FIELD_NAME': 'afvigelse_median',
                                                             'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})
                            mean_afvigelse_median = statistics2["MEAN"]
                            grid.changeAttributeValue(feature.id(),
                                                      grid.fields().indexFromName("geofysik_mean_afvigelse_median"),
                                                      mean_afvigelse_median)
                            geofysikpunkter.removeSelection()
                        else:
                            grid.changeAttributeValue(feature.id(),
                                                      grid.fields().indexFromName("geofysik_mean_afvigelse_median"),
                                                      'nan')

                        grid.removeSelection()

                    grid.updateExtents()
                    grid.commitChanges()
                    grid_final = processing.run("native:fieldcalculator", {
                        'INPUT': grid,
                        'FIELD_NAME': 'samlet_afvigelse', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
                        'FORMULA': 'CASE\r\nWHEN "geofysik_mean_afvigelse_middelmodstand" = \'nan\' and "boringer_mean_afvigelse" = \'nan\' then \'nan\' \r\nWHEN "geofysik_mean_afvigelse_middelmodstand" != \'nan\' and "boringer_mean_afvigelse" = \'nan\' then   "geofysik_mean_afvigelse_middelmodstand"\r\nWHEN "geofysik_mean_afvigelse_middelmodstand" = \'nan\' and "boringer_mean_afvigelse" != \'nan\' then "boringer_mean_afvigelse"\r\nelse "geofysik_mean_afvigelse_middelmodstand"+ "boringer_mean_afvigelse" END',
                        'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                    project.removeMapLayer(grid)

                    project.addMapLayer(grid_final)
                    grid_final.setName("Grid - Usikkerhedsvurdering - {}".format(lay))


                    grid_final.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Grid - Usikkerhedsvurdering.qml")




        elif self.comboBox_tolkningsvurdering.currentText() == '2.1: Benyt specifikke flader indlæst i QGIS':
            top = self.layer_combo.currentLayer()
            bottom = self.layer_combo_2.currentLayer()
            buffer = processing.run("native:buffer", {'INPUT':layer,'DISTANCE':150,'SEGMENTS':5,'END_CAP_STYLE':0,'JOIN_STYLE':0,'MITER_LIMIT':2,'DISSOLVE':False,'OUTPUT':'TEMPORARY_OUTPUT'})["OUTPUT"]

            clipped_top = processing.run("gdal:cliprasterbyextent",
                           {'INPUT': top,
                            'PROJWIN': buffer.extent(),
                            'OVERCRS': False, 'NODATA': None, 'OPTIONS': '', 'DATA_TYPE': 0, 'EXTRA': '',
                            'OUTPUT': f'C:/Temp/{top.name()[:-5]}.grd'})['OUTPUT']



            clipped_bottom = processing.run("gdal:cliprasterbyextent",
                                         {'INPUT': top,
                                          'PROJWIN': buffer.extent(),
                                          'OVERCRS': False, 'NODATA': None, 'OPTIONS': '', 'DATA_TYPE': 0, 'EXTRA': '',
                                          'OUTPUT': f'C:/Temp/{bottom.name()[:-5]}.grd'})['OUTPUT']



           # project.addMapLayer(QgsRasterLayer(clipped_bottom))
           # project.addMapLayer(QgsRasterLayer(clipped_top))


            grdpositions = [clipped_top, clipped_bottom]

            for grd in grdpositions:
                print(grd)
                z = grd.split('.')[0]
                print(z)
                grd_folder = grd.replace('/', '\\')
                direct = os.path.dirname(__file__)

                f = open(direct + r'\save_grd_to_DB_template.bat', 'r')
                filedata = f.read()
                f.close()

                newdata = filedata.replace('__path__', grd_folder)
                f = open(direct + r'\save_grd_to_DB.bat', 'w')
                f.write(newdata)
                f.close()

                subprocess.call([direct + r'\save_grd_to_DB.bat'])
                os.remove(direct + r'\save_grd_to_DB.bat')







        elif self.comboBox_tolkningsvurdering.currentText() == '2.2: Benyt alle flader fra mappe (KRÆVENDE)':
            print("HEJ")

        self.foretag_tolkningsvurdering.setText("Foretag tolkningsvurdering")
        self.foretag_tolkningsvurdering.setEnabled(True)
        self.repaint()
    else:
        QMessageBox.warning(None, 'Fejl', "Intet input (områdepolygon).")





