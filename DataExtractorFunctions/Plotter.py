from .. import pyqtgraph as pgs


class CustomWidget(pgs.GraphicsWindow):
    def __init__(self, parent=None, **kargs):
        pgs.GraphicsWindow.__init__(self, **kargs)
        self.setParent(parent)
        self.setWindowTitle('pyqtgraph example: Scrolling Plots')
        p1 = self.addPlot(labels={'left': 'Voltage', 'bottom': 'Time'})

