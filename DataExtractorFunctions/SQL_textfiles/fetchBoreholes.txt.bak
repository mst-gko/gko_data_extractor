with 	
                               fohm_depth as ( --get the depth of fohm and top point of fohm
                                select DISTINCT ON (flba.boreholeid, flb.layer_num)
                                    flba.boreholeid,
                                    flba.terraintop,
                                    layer_top,
                                    layer_bot,
                                    case -- hvis ingen elevation er målt for boringen, rykkes toppen af boringen til at matche toppen af modellen
                                        when b.elevation is null
                                            then flba.terraintop
                                        else b.elevation
                                    end as borehole_elev
                                from (
                                select
                                flb.boreholeid,
                                max(flb.layer_top) as terraintop
                                from fohm_boreholes.fohm_boreholes_{område} flb
                                group by flb.boreholeid
                                ) flba
                                left join jupiter_fdw.borehole b using (boreholeid)
                                right join fohm_boreholes.fohm_boreholes_{område} flb using (boreholeid) 
                                where layer_num = {layer_id}
                                AND flb.boringskvalitet >= 3
                                AND ST_Intersects(ST_Transform(b.geom, {crs}), 'SRID={crs};{geom}')
                            ),
                            ler_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('bfe','bmf','brf','fe','mfe','rfe')
                                            then 'fed ler'
                                        when ls.texture is null
                                            then 'ler'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'dl', 'fl', 'hl', 'il', 'ql',						
                                'l', 'll', 'j', 'ml', 'ol',
                                'rl', 'sl', 'u', 'vl', 'zl',
                                'yl', 'tl', 'xl', 'gl', 'pl',
                                'pr', 'ed', 'e', 'ee'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            silt_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('uso','fmg','uof','uog','mgu','gou','fgu','fmu','fou','mou')
                                            then 'usorteret silt'
                                        when ls.texture in ('bfi','fof','fin','fom','fmf','feg','ftm','fgr','fog','fgf','fgg')
                                            then 'fint silt'
                                        when ls.texture in ('mof','mgf','mgr','mog','mel','mgg','mtg','bme')
                                            then 'mellem silt'
                                        when ls.texture in ('mgo','gof','gog','gro','bgr')
                                            then 'groft silt'
                                        when ls.texture is null
                                            then 'silt'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'di', 'fi', 'hi', 'i', 'ii',						
                                'mi', 'qi', 'oi', 'yi', 'zi',
                                'ti', 'gi', 'pi'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            sand_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('uso','fmg','uof','uog','mgu','gou','fgu','fmu','fou','mou')
                                            then 'usorteret sand'
                                        when ls.texture in ('bfi','fof','fin','fom','fmf','feg','ftm','fgr','fog','fgf','fgg')
                                            then 'fint sand'
                                        when ls.texture in ('mof','mgf','mgr','mog','mel','mgg','mtg','bme')
                                            then 'mellem sand'
                                        when ls.texture in ('mgo','gof','gog','gro','bgr')
                                            then 'groft sand'
                                        when ls.texture is null
                                            then 'sand'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'ds', 'es', 'hs', 'fs', 'is',						
                                'ks', 'ms', 'os', 's', 'zs',
                                'ys', 'ts', 'qs', 'gs', 'pq',
                                'ps'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            grus_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('uso','fmg','uof','uog','mgu','gou','fgu','fmu','fou','mou')
                                            then 'usorteret grus'
                                        when ls.texture in ('bfi','fof','fin','fom','fmf','feg','ftm','fgr','fog','fgf','fgg')
                                            then 'fint grus'
                                        when ls.texture in ('mof','mgf','mgr','mog','mel','mgg','mtg','bme')
                                            then 'mellem grus'
                                        when ls.texture in ('mgo','gof','gog','gro','bgr')
                                            then 'groft grus'
                                        when ls.texture is null
                                            then 'grus'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'dg', 'fg', 'hg', 'ig', 'kg',						
                                'g', 'mg', 'qg', 'yg', 'tg'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            sten_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('uso','fmg','uof','uog','mgu','gou','fgu','fmu','fou','mou')
                                            then 'usorteret sten'
                                        when ls.texture in ('bfi','fof','fin','fom','fmf','feg','ftm','fgr','fog','fgf','fgg')
                                            then 'fint sten'
                                        when ls.texture in ('mof','mgf','mgr','mog','mel','mgg','mtg','bme')
                                            then 'mellem sten'
                                        when ls.texture in ('mgo','gof','gog','gro','bgr')
                                            then 'groft sten'
                                        when ls.texture is null
                                            then 'sten'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'dz', 'mz', 'z'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            kalk_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('sla','sts','svs','tæt')
                                            then 'slammet kalk'
                                        when ls.texture is null
                                            then 'kalk'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'dk', 'kk', 'bk', 'ak', 'as',						
                                'k', 'lk', 'sk', 'zk', 'tk',
                                'gk', 'pk', 'fk'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            organisk_interval as (
                                select 			
                                    fd.boreholeid,
                                    case 
                                        when ls.texture in ('sla','sts','svs','tæt')
                                            then 'fed organisk'
                                        when ls.texture is null
                                            then 'organisk'
                                        else 'dårlig beskrevet, usikker'
                                    end as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'hp', 'ht', 'id', 'fp', 'c',						
                                'ip', 'it', 'p', 'tp', 't',
                                'yp', 'yt', 'tt', 'gc', 'qp',
                                'qt', 'ft', 'gp'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            vekslende_interval as (
                                select 			
                                    fd.boreholeid,
                                    'vekslende' as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'dv', 'hv', 'iv', 'mv', 'yv',						
                                'v', 'tv', 'qv', 'gv', 'fv',
                                'ev', 'pv', 'av', 'o', 'ij',
                                'fj', 'm'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            ukendt_interval as (
                                select 			
                                    fd.boreholeid,
                                    'ukendt' as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'b', 'x', 'h'
                                )
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            andet_interval as (
                                select 			
                                    fd.boreholeid,
                                    'andet' as soil_type,
                                    abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
                                from fohm_depth fd
                                inner join jupiter_fdw.lithsamp ls using (boreholeid)
                                inner join jupiter_fdw.borehole b using (boreholeid)
                                where ls.rocksymbol in (
                                'dr', 'cj', 'ck', 'cl', 'cq', 'cr', 'cv', 'cw', 'eq', 'ar',
                                'kj', 'kl', 'kq', 'kr', 'jc', 'ji', 'jj', 'jl', 'jq', 'jr',
                                'js', 'jv', 'ka', 'nq', 'nr', 'nw', 'r', 'mk', 'nd', 'nj',
                                'nk', 'nl', 'oq', 'or', 'q', 'rq', 'rr', 'rs', 'rv', 'sj',						
                                'sr', 'tr', 'wr', 'xk', 'xq', 'xr', 'wl', 'qk', 'rc', 'rg',
                                'rj', 'rk', 'vc', 'vi', 'vj', 'vk', 'vq', 'vr', 'vs', 'vv',
                                'w', 'wk', 'ok', 'oe', 'gr', 'ls', 'gf', 'ek', 'bl', 'bs',
                                'bv', 'd', 'pj', 'uc', 'ui', 'uj', 'ul', 'uq', 'ur', 'us',
                                'uv', 'el', 'f', 'a', 'pa', 'pd', 'af', 'al'
                                ) 
                                and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
                                and ls.bottom is not null
                                and ls.top is not null
                                ),
                            fohm_soil_types as (
                                select			
                                    boreholeid,
                                    soil_type,
                                    sum(tykkelse)  as tykkelse_fohmlag -- alle ens lithologier i fohmlagets interval i en boring lægges sammen
                                from ler_interval
                                full outer join silt_interval using (boreholeid,soil_type,tykkelse)
                                full outer join sand_interval using (boreholeid,soil_type,tykkelse)
                                full outer join grus_interval using (boreholeid,soil_type,tykkelse)
                                full outer join sten_interval using (boreholeid,soil_type,tykkelse)
                                full outer join kalk_interval using (boreholeid,soil_type,tykkelse)
                                full outer join organisk_interval using (boreholeid,soil_type,tykkelse)
                                full outer join vekslende_interval using (boreholeid,soil_type,tykkelse)
                                full outer join ukendt_interval using (boreholeid,soil_type,tykkelse)
                                full outer join andet_interval using (boreholeid,soil_type,tykkelse)
                                group by boreholeid,soil_type
                                ),
                                slutprodukt AS (

                    select 
                        b.boreholeno, st_X(b.geom), st_Y(b.geom),
                        fst.*,
                        tykkelse_fohmlag/(fd.layer_top-fd.layer_bot)*100 as proc_fohmlag --omregn tykkelse af lithologi i boringen til procentdel af fohmlagets totale tykkelse
                    from fohm_soil_types fst
                    left join fohm_depth fd using (boreholeid)
                    left join jupiter_fdw.borehole b using (boreholeid)
                    )


                    SELECT boreholeno,
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'fed ler'), 0)  AS "fed ler",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'ler'), 0)  AS "ler",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'silt'), 0)  AS "silt",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'fint silt'), 0)  AS "fint silt",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'mellem silt'), 0)  AS "mellem silt",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'grov silt'), 0)  AS "grov silt",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'usorteret silt'), 0)  AS "usorteret silt",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'sand'), 0)  AS "sand",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'fint sand'), 0)  AS "fint sand",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'mellem sand'), 0)  AS "mellem sand",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'grov sand'), 0)  AS "grov sand",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'usorteret sand'), 0)  AS "usorteret sand",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'grus'), 0)  AS "grus",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'fint grus'), 0)  AS "fint grus",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'mellem grus'), 0)  AS "mellem grus",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'grov grus'), 0)  AS "grov grus",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'usorteret grus'), 0)  AS "usorteret grus",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'sten'), 0)  AS "sten",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'fint sten'), 0)  AS "fint sten",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'mellem sten'), 0)  AS "mellem sten",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'grov sten'), 0)  AS "grov sten",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'usorteret sten'), 0)  AS "usorteret sten",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'kalk'), 0)  AS "kalk",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'slammet kalk'), 0)  AS "slammet kalk",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'organisk'), 0)  AS "organisk",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'fed organisk'), 0)  AS "fed organisk",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'vekslende'), 0)  AS "vekslende",

                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'ukendt'), 0)  AS "ukendt",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'andet'), 0)  AS "andet",
                    COALESCE(sum(proc_fohmlag) FILTER (WHERE soil_type = 'dårligt beskrevet, usikker'), 0)  AS "dårligt beskrevet, usikker",
                    st_X, st_Y
                    FROM slutprodukt
                    group by boreholeno, st_X, st_Y
                    ;