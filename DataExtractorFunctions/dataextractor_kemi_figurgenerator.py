# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import (
    QgsFields,
    QgsField,
    QgsFeature,
    QgsLayerTreeGroup,
    QgsGeometry,
    QgsFeatureRequest,
    QgsProject,
    QgsSimpleMarkerSymbolLayerBase,
    QgsRaster,
    QgsTask,
    QgsPrintLayout,
    QgsReadWriteContext,
    QgsRasterFileWriter,
    QgsMapLayer,
    QgsLayoutItemLegend,
    QgsLayoutItemMap,
    QgsLegendRenderer,
    QgsExpression,
    QgsLegendStyle,
    QgsLayoutExporter,
    QgsLayoutItemPicture,
    QgsLayoutPoint,
    QgsLayoutSize,
    QgsUnitTypes,
    QgsRasterBandStats,
    QgsCategorizedSymbolRenderer,
    QgsRendererCategory,
    QgsSymbol,
    QgsCoordinateTransformContext,
    QgsRuleBasedRenderer,
    QgsRandomColorRamp,
    QgsMapThemeCollection,
    QgsLayerDefinition,
    QgsPointXY,
    QgsMapLayerLegendUtils,
    QgsApplication,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsExpressionContextScope,
    QgsCoordinateReferenceSystem,
    QgsProcessingFeatureSourceDefinition,
    QgsRectangle,
    QgsTaskManager,
    QgsLayout,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsRasterLayer,
    QgsWkbTypes,
    QgsDataSourceUri
)
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()
import pyodbc

from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass


__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *


def kemi_one(self):
    if len(self.comboBox.currentText()) > 0:

        if len(self.outputdirectory_2.text()) > 0:
            if True:
                self.pushButton_fourth.setText("Kører")
                self.pushButton_fourth.setEnabled(False)
                self.repaint()

            lokal = str(self.outputdirectory_2.text())

            try:
                os.makedirs("{}/Kemiudtraek".format(lokal))
            except:
                pass
            lokal = str(self.outputdirectory_2.text()) + "/Kemiudtraek"

            project = QgsProject.instance()
            key = self.comboBox.currentText()
            områdelayer = project.mapLayersByName('{}'.format(key))[0]

            for kk, feature in enumerate(områdelayer.getFeatures()):
                if kk == 0:
                    geom = feature.geometry()
                else:
                    geom = geom.combine(feature.geometry())

            crstrue = int("".join(filter(str.isdigit, str(områdelayer.crs().authid()))))
            path_output = str(lokal) + '/Kemiudtraek - {}.xlsm'.format(self.comboBox.currentText())
            table_infos = [
                (1, 'Alle Data', 'mstmvw_substance_all_data', 'A1', 0),
                (1, 'Arbejdsdata', 'mstmvw_substance_all_data_filtered', 'A1', 0),
                # (1, 'Magasin', 'mstmvw_substance_aquifer', 'A1', 0),
                #(1, 'Nitrat seneste', 'mstmvw_substance_nitrate_latest', 'A1', 0),
                #(1, 'Sulfat seneste', 'mstmvw_substance_sulphate_latest', 'A1', 0),
                #(1, 'Sulfat-nitrat seneste', 'mstmvw_substance_nitrate_and_sulphate_latest', 'A1', 0),
                #(1, 'Klorid seneste', 'mstmvw_substance_chloride_latest', 'A1', 0),
                #(1, 'Klorid-sulfat seneste', 'mstmvw_substance_chloride_and_sulphate_latest', 'A1', 0),
                #(1, 'Ionbytning seneste', 'mstmvw_substance_sodium_chloride_ionexchange_latest', 'A1', 0),
                #(1, 'NVOC seneste', 'mstmvw_substance_nvoc_latest', 'A1', 0),
                #(1, 'Arsen seneste', 'mstmvw_substance_arsenic_latest', 'A1', 0),
                #(1, 'Vandtype seneste', 'mstmvw_substance_watertype_latest', 'A1', 0),
                #(1, 'Jern seneste', 'mstmvw_substance_iron_latest', 'A1', 0),
                #(1, 'Strontium seneste', 'mstmvw_substance_strontium_latest', 'A1', 0),
                (1, 'Database', 'mstmvw_substance_database', 'E1', 1)]

            db = Database(database='JUPITER')

            class Xlsx:
                def __init__(self, xlsx_path, template_xlsx=str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/dependencies/template_udtraek_kemidatabase.xlsm"):
                    self.xlsx_path = xlsx_path
                    self.template_xlsx = template_xlsx

                def copy_template_xlsx(self):
                    if os.path.exists(self.xlsx_path):
                        os.remove(self.xlsx_path)
                    shutil.copyfile(self.template_xlsx, self.xlsx_path)

                def append_df_to_xlsm(self, df, sheetname, startfromcell, protect=0):
                    with xw.App() as app:
                        wb = xw.Book(self.xlsx_path)
                        try:
                            wb.sheets.add(sheetname, after='Arbejdsdata')
                        except ValueError as V:
                            pass
                        ws = wb.sheets[sheetname]

                        if protect == 1:
                            ws.api.Unprotect()

                        ws[f"{startfromcell}"].options(pd.DataFrame, header=1, index=False, expand='table').value = df
                        ws[f"{startfromcell}"].expand("right").api.Font.Bold = True
                        ws[f"{startfromcell}"].expand("right").api.Borders.Weight = 2

                        if protect == 1:
                            ws.api.Protect()

                        wb.save(self.xlsx_path)
                        wb.close()

            xlsx = Xlsx(path_output)
            xlsx.copy_template_xlsx()
            for i, table_info in enumerate(reversed(table_infos)):
                incl_sheet = table_info[0]  # 1 if sheet should be included in xlsx, 0 if not
                sheet_name = table_info[1]
                table_name = table_info[2]
                startfromcell = table_info[3]
                protect_sheet = table_info[4]

                if incl_sheet == 1:
                    sql = f'''
                            SELECT t.* 
                            FROM mstjupiter.{table_name} t
                            WHERE ST_Intersects(ST_Transform(t.geom, {crstrue}), 'SRID={crstrue};{geom.asWkt()}')
                    '''
                    df = db.sql_to_df(sql=sql)
                    remove_columns = [
                        'geom', 'row_id', 'lith_in_screen', 'fohm_layer', 'guid_intake'
                    ]
                    for column_name in remove_columns:
                        if column_name in df:
                            df.pop(column_name)

                    xlsx.append_df_to_xlsm(df=df, sheetname=sheet_name, startfromcell=startfromcell,
                                           protect=protect_sheet)

                if sheet_name == 'Arbejdsdata':
                    liste = ['sumanoinscalculated',	'sumcationscalculated',	'ionbalance',	'forvitgrad',	'ionexchange',	'alkalinitet_total_ta',	'aluminium',	'ammoniak_ammonium',	'ammonium_n',	'anioner_total',	'arsen',	'barium',	'bly',	'bor',	'bromid',	'calcium',	'carbon_organisk_nvoc',	'carbonat',	'carbondioxid_aggr',	'chlorid',	'dihydrogensulfid',	'fluorid',	'hydrogencarbonat',	'inddampningsrest',	'jern',	'kalium',	'kationer_total',	'kobber',	'kobolt_co',	'konduktivitet',	'magnesium',	'mangan',	'methan',	'natrium',	'natriumhydrogencarbonat',	'nikkel',	'nitrat',	'nitrit',	'orthophosphat',	'ortho_phosphat_p',	'oxygen_indhold',	'permanganattal_kmno4',	'ph',	'phosphor_total_p',	'redoxpotentiale',	'siliciumdioxid',	'strontium',	'sulfat',	'sulfit_s',	'temperatur',	'tørstof_total',	'zink']

                    df = df.drop_duplicates(subset=liste,keep='last').reset_index(drop=True)



            self.pushButton_fourth.setText("Færdig ✓")
            self.repaint()
            os.startfile(str(lokal))

        else:
            QMessageBox.warning(None, 'Fejl', "Vælg et output directory for udtræk og figurer.")

    else:
        QMessageBox.warning(None, 'Fejl', "Intet input (områdepolygon).")


def kemi_two(self):
    if len(self.comboBox.currentText()) > 0:
        if len(self.lineEdit.text()) > 0:
            def write_qtable_to_df(table):
                col_count = table.columnCount()
                row_count = table.rowCount()
                headers = [str(table.horizontalHeaderItem(i).text()) for i in range(col_count)]

                df_list = []
                for row in range(row_count):
                    df_list2 = []
                    for col in range(col_count):
                        table_item = table.item(row, col)
                        #if table_item is None:
                        #    try:
                        #        table_item = table.cellWidget(row, col).currentText()
                        #        textitem = str(table_item)
                        #    except:
                        #        textitem = ''
                        #else:
                        #    try:
                        #        textitem = str(table_item.text())
                        #    except:
                        #        textitem = ''
                        try:
                            if table_item is None:
                                table_item = table.cellWidget(row, col).currentText()
                                textitem = str(table_item)
                            else:
                                textitem = str(table_item.text())
                        except:
                            QMessageBox.warning(None, 'Fejl', "Udfyld tabellen.")

                        df_list2.append(textitem)
                    df_list.append(df_list2)

                df = pd.DataFrame(df_list, columns=headers)

                return df

            magasintildelingdf = write_qtable_to_df(self.tableWidget)

            rows = self.tableWidget.rowCount()
            columns = self.tableWidget.columnCount()
            z = []
            for row in range(rows):
              #  for col in range(columns):
                    try:
                        z.append(self.tableWidget.item(row, 0).text())
                    except:
                        z.append(None)


            if None in z:
                QMessageBox.warning(None, 'Fejl', "Udfyld tabellen.")
            else:
                if True:
                    self.pushButton_fifth.setText("Kører")
                    self.pushButton_fifth.setEnabled(False)
                    self.repaint()
                project = QgsProject.instance()
                key = self.comboBox.currentText()

                manager = QgsProject.instance().layoutManager()

                layouts_list = manager.printLayouts()

                for layout in layouts_list:
                    if "Kemi - Seneste" in layout.name():
                        layouts_list.remove(layout)

                områdelayer = project.mapLayersByName('{}'.format(key))[0]

                for kk, feature in enumerate(områdelayer.getFeatures()):
                    if kk == 0:
                        geom = feature.geometry()
                    else:
                        geom = geom.combine(feature.geometry())

                nodeområdelayer = project.layerTreeRoot().findLayer(områdelayer.id())
                if nodeområdelayer:
                    nodeområdelayer.setItemVisibilityChecked(False)

                lokal = os.path.dirname(self.lineEdit.text())

                try:
                    os.makedirs("{}/Tempfiler".format(lokal))
                except:
                    pass
                temp = lokal + "/Tempfiler"

                try:
                    os.makedirs("{}/Shapefiler".format(lokal))
                except:
                    pass
                gem = lokal + "/Shapefiler"

                root = project.layerTreeRoot()

                groupkemi2 = "Generelt"
                root = project.layerTreeRoot()
                groupkemi = root.insertGroup(0, groupkemi2)

                groupstiltempaltes = root.insertGroup(0, "Stilarter")

                memlay = områdelayer.clone()
                _writer = QgsVectorFileWriter.writeAsVectorFormat(memlay, str(gem) + '/Område.gpkg',
                                                                  'Område', memlay.crs())
                memlay = QgsVectorLayer(str(gem) + '/Område.gpkg', 'Område', 'ogr')
                memlay.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Projektområde.qml")
                memlay.setName('Område')

                project.addMapLayer(memlay, False)  # False is the key
                groupkemi.addLayer(memlay)


                QgsLayerDefinition().loadLayerDefinition("F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/GKO bekendtgørelser gældende/01~Drikkevandsinteresser.qlr", project, groupkemi)
                områdesærligedrikke = project.mapLayersByName('di - Gældende udpegning')[0]
                OSD = processing.run("native:clip",
                                     {'INPUT': områdesærligedrikke,
                                      'OVERLAY': memlay, 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

                _writer = QgsVectorFileWriter.writeAsVectorFormat(OSD, str(gem) + '/OSD.gpkg', 'OSD', OSD.crs())
                OSD = QgsVectorLayer(str(gem) + '/OSD.gpkg', 'OSD', 'ogr')

                OSD.setName("OSD")
                OSD.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - OSD.qml")
                project.addMapLayer(OSD, False)  # False is the key
                groupkemi.addLayer(OSD)
                project.removeMapLayer(områdesærligedrikke)



                QgsLayerDefinition().loadLayerDefinition(
                    str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/dependencies/SenesteFluoridTemplate.qlr", project,
                    groupstiltempaltes)
                QgsLayerDefinition().loadLayerDefinition(
                    str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/dependencies/SenesteStrontiumTemplate.qlr", project,
                    groupstiltempaltes)
                QgsLayerDefinition().loadLayerDefinition(
                    str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/dependencies/SenesteKloridTemplate.qlr", project, groupstiltempaltes)
                QgsLayerDefinition().loadLayerDefinition(
                    str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/dependencies/SenesteVandtypeTemplate.qlr", project,
                    groupstiltempaltes)
                QgsLayerDefinition().loadLayerDefinition(
                    str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/dependencies/SenesteSulfatTemplate.qlr", project, groupstiltempaltes)
                QgsLayerDefinition().loadLayerDefinition(
                    str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/dependencies/SenestePesticiderTemplate.qlr", project,
                    groupstiltempaltes)
                QgsLayerDefinition().loadLayerDefinition(
                    str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenestePFASTemplate.qlr",project,groupstiltempaltes)
                QgsLayerDefinition().loadLayerDefinition(
                    str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/dependencies/SenesteNitratTemplate.qlr", project, groupstiltempaltes)

                QgsLayerDefinition().loadLayerDefinition(
                    str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteArsenTemplate.qlr", project,
                    groupstiltempaltes)

                QgsLayerDefinition().loadLayerDefinition(
                    str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteJernTemplate.qlr",
                    project,
                    groupstiltempaltes)

                QgsLayerDefinition().loadLayerDefinition(
                    str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/Tidsserier.qlr",
                    project, groupstiltempaltes)

                QgsLayerDefinition().loadLayerDefinition(
                    str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/MagasinTemplate.qlr", project,
                    groupstiltempaltes)




                for layers in groupstiltempaltes.children():
                    layers.setItemVisibilityChecked(False)

                xls = pd.ExcelFile("{}".format(self.lineEdit.text()))
                df = pd.read_excel(xls, 'Arbejdsdata')
                df['dato'] = pd.to_datetime(df['dato'])
                df = df.sort_values(['dgu_indtag', 'dato'])
               # altdata = pd.read_excel(xls, 'Alle Data')

                nitratseneste = df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                    'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin', 'dybde_filtertop',
                                    'dybde_filterbund',
                                    'xutm32euref89', 'yutm32euref89', 'nitrat', 'nitrit', 'jern', 'methan',
                                    'ammoniak_ammonium', 'sulfat']][~df['nitrat'].isnull()].drop_duplicates(
                    subset=['dgu_indtag'], keep='last')

                sulfatseneste = df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                    'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin', 'dybde_filtertop',
                                    'dybde_filterbund',
                                    'xutm32euref89', 'yutm32euref89', 'sulfat', 'nitrat', 'chlorid']][
                    ~df['sulfat'].isnull()].drop_duplicates(subset=['dgu_indtag'], keep='last')

                vandtypeseneste = df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                      'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin',
                                      'dybde_filtertop', 'dybde_filterbund', 'xutm32euref89', 'yutm32euref89', f'{self.x_combo.currentText()}',
                                      'sulfat', 'nitrat', 'jern', 'oxygen_indhold', 'methan', 'ammonium_n']][
                    ~df['Vandtype'].isnull()].drop_duplicates(subset=['dgu_indtag'], keep='last')

                kloridseneste = df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                    'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin',
                                    'dybde_filtertop', 'dybde_filterbund', 'xutm32euref89', 'yutm32euref89',
                                    'ionbalance', 'natrium', 'kalium', 'sulfat', 'nitrat', 'chlorid', 'ionexchange']][
                    ~df['chlorid'].isnull()].drop_duplicates(
                    subset=['dgu_indtag'], keep='last')

                strontiumseneste = df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                       'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin',
                                       'dybde_filtertop', 'dybde_filterbund', 'xutm32euref89', 'yutm32euref89',
                                       'strontium', 'chlorid', 'fluorid', 'sulfat', 'hydrogencarbonat', 'natrium']][
                    ~df['strontium'].isnull()].drop_duplicates(subset=['dgu_indtag'], keep='last')

                fluoridseneste = df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                     'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin',
                                     'dybde_filtertop', 'dybde_filterbund', 'xutm32euref89', 'yutm32euref89',
                                     'fluorid', 'hydrogencarbonat', 'chlorid',
                                     'natrium', 'ionexchange']][~df['fluorid'].isnull()].drop_duplicates(
                    subset=['dgu_indtag'], keep='last')

                arsenseneste = df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                     'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin',
                                     'dybde_filtertop', 'dybde_filterbund', 'xutm32euref89', 'yutm32euref89',
                                     'arsen', 'ionexchange']][~df['arsen'].isnull()].drop_duplicates(subset=['dgu_indtag'], keep='last')

                jernseneste = df[['sampleid', 'dgu_indtag', 'boreholeno', 'stringno', 'intakeno', 'sampledate',
                                   'dato', 'aarstal', 'unik', 'project', 'use', 'purpose', 'magasin',
                                   'dybde_filtertop', 'dybde_filterbund', 'xutm32euref89', 'yutm32euref89',
                                   'jern', 'ionexchange']][~df['jern'].isnull()].drop_duplicates(
                    subset=['dgu_indtag'], keep='last')

                df_grp = df.groupby(['dgu_indtag'])
                nitrattidsserier = pd.DataFrame().reindex_like(df)
                sulfattidsserier = pd.DataFrame().reindex_like(df)


                ########## LIDT SKETCHY ###############
                def estimate_coef(x, y):
                    # number of observations/points
                    n = np.size(x)

                    # mean of x and y vector
                    m_x = np.mean(x)
                    m_y = np.mean(y)

                    # calculating cross-deviation and deviation about x
                    SS_xy = np.sum(y * x) - n * m_y * m_x
                    SS_xx = np.sum(x * x) - n * m_x * m_x

                    # calculating regression coefficients
                    a = SS_xy / SS_xx
                    b = m_y - a * m_x

                    return (a, b)

                for name, grp in df_grp:
                    print(grp)
                    datem = max(grp['sampledate']).year
                    datemin = min(grp['sampledate']).year

                    mean_nitrat = grp['nitrat'].dropna().dropna().mean()
                    norm_nitrat = []
                    for x in grp['nitrat'].dropna():
                        if x > mean_nitrat:
                            norm_nitrat.append(x - mean_nitrat)
                        else:
                            norm_nitrat.append(x - mean_nitrat)

                    mean_sulfat = grp['sulfat'].dropna().dropna().mean()
                    norm_sulfat = []
                    for x in grp['sulfat'].dropna():
                        if x > mean_sulfat:
                            norm_sulfat.append(x - mean_sulfat)
                        else:
                            norm_sulfat.append(x - mean_sulfat)


                    #########NITRAT###################
                    if len(grp) >= 3 and relativedelta(max(grp['sampledate']), min(grp['sampledate'])).years > 3 and \
                            datem >= 2000 and (sum(grp['nitrat'].dropna().dropna()) / len(grp)) > 1:
                        if len(grp) > 10:
                            coefficients_nitrat = estimate_coef(grp['aarstal'][3:], grp['nitrat'].dropna().dropna()[3:])
                        elif len(grp) <= 10 and len(grp) > 5:
                            coefficients_nitrat = estimate_coef(grp['aarstal'][1:], grp['nitrat'].dropna().dropna()[1:])
                        else:
                            coefficients_nitrat = estimate_coef(grp['aarstal'], grp['nitrat'].dropna().dropna())

                        udvikling_nitrat = (((coefficients_nitrat[0] * datem + coefficients_nitrat[1]) - (
                                coefficients_nitrat[0] * datemin + coefficients_nitrat[1])) / (
                                                    coefficients_nitrat[0] * datemin + coefficients_nitrat[1])) * 100

                        if len(grp) > 10:
                            sidste50 = grp['nitrat'].tolist()[round(len(grp['nitrat'].dropna().dropna()) / 2) - 3:]
                        else:
                            sidste50 = grp['nitrat'].dropna().dropna().tolist()

                        positive = np.where(np.diff(sidste50) > 0)
                        negative = np.where(np.diff(sidste50) < 0)

                        positivsum = abs(np.sum(np.where(np.diff(sidste50) > 0, np.diff(sidste50), 0)))
                        negativsum = abs(np.sum(np.where(np.diff(sidste50) < 0, np.diff(sidste50), 0)))
                        std_nit = np.std(grp['nitrat'].tolist())

                        if any(abs(y) > 5 for y in norm_nitrat) and sum(grp["nitrat"].dropna().tolist()[-3:]) != 0:
                            if udvikling_nitrat > 20 and norm_nitrat[-1] >= norm_nitrat[0] + 5:
                                if len(norm_nitrat) > 10:
                                    if len(positive[0]) >= len(negative[0]) + 2 and positivsum > negativsum * 1.3:
                                        if grp['nitrat'].dropna().dropna().tolist()[-1] < \
                                                grp['nitrat'].dropna().dropna().tolist()[-2] and abs(
                                                grp['nitrat'].dropna().dropna().tolist()[-1] -
<<<<<<< HEAD
                                                grp['nitrat'].dropna().dropna().tolist()[-2]) < std_nit:
=======
                                                grp['nitrat'].dropna().dropna().tolist()[-2]) < std:
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb
                                            grp["Tendens"] = "Stigende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                                elif 5 < len(norm_nitrat) <= 10:
                                    if len(positive[0]) >= len(negative[0]) + 1 and positivsum > negativsum * 1.3:
                                        if grp['nitrat'].dropna().dropna().tolist()[-1] < \
                                                grp['nitrat'].dropna().dropna().tolist()[-2] and abs(
                                                grp['nitrat'].dropna().dropna().tolist()[-1] -
<<<<<<< HEAD
                                                grp['nitrat'].dropna().dropna().tolist()[-2]) < std_nit:
=======
                                                grp['nitrat'].dropna().dropna().tolist()[-2]) < std:
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb
                                            grp["Tendens"] = "Stigende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                                else:
                                    if len(positive[0]) >= len(negative[0]) and positivsum > negativsum * 1.3:
                                        if grp['nitrat'].tolist()[-1] >= grp['nitrat'].tolist()[-2]:
                                            grp["Tendens"] = "Stigende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                            elif udvikling_nitrat < -20 and norm_nitrat[-1] <= norm_nitrat[0] - 5:
                                if len(norm_nitrat) > 10:
                                    if len(negative[0]) >= len(positive[0]) + 2 and negativsum > positivsum * 1.3:
                                        if grp['nitrat'].tolist()[-1] > grp['nitrat'].tolist()[-2] and abs(
<<<<<<< HEAD
                                                grp['nitrat'].tolist()[-1] - grp['nitrat'].tolist()[-2]) < std_nit:
=======
                                                grp['nitrat'].tolist()[-1] - grp['nitrat'].tolist()[-2]) < std:
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb
                                            grp["Tendens"] = "Faldende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                                elif 5 < len(norm_nitrat) <= 10:
                                    if len(negative[0]) >= len(positive[0]) + 1 and negativsum > positivsum * 1.3:
                                        if grp['nitrat'].tolist()[-1] > grp['nitrat'].tolist()[-2] and abs(
<<<<<<< HEAD
                                                grp['nitrat'].tolist()[-1] - grp['nitrat'].tolist()[-2]) < std_nit:
=======
                                                grp['nitrat'].tolist()[-1] - grp['nitrat'].tolist()[-2]) < std:
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb
                                            grp["Tendens"] = "Faldende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                                else:
                                    if len(negative[0]) >= len(positive[0]) and negativsum > positivsum * 1.3:
                                        if grp['nitrat'].tolist()[-1] <= grp['nitrat'].tolist()[-2]:
                                            grp["Tendens"] = "Faldende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                            else:
                                grp["Tendens"] = "Varierende"
                        else:
                            grp["Tendens"] = "Stabil"

                        '''
                        if any(abs(y) > 5 for y in norm_nitrat) and sum(grp["nitrat"].dropna().tolist()[-3:]) != 0:
                            if udvikling_nitrat > 20 and norm_nitrat[-1] >= norm_nitrat[0] + 5:
                                if len(norm_nitrat) > 10:
                                    if len(positive[0]) >= len(negative[0]) + 2 and positivsum > negativsum * 1.3:  # (len(sidste50)/5)
                                        if grp['nitrat'].dropna().dropna().tolist()[-1] < grp['nitrat'].dropna().dropna().tolist()[-2] and abs(grp['nitrat'].dropna().dropna().tolist()[-1] - grp['nitrat'].dropna().dropna().tolist()[
                                                    -2]) < std:  # norm_nitrat[-1] > norm_nitrat[-2] > norm_nitrat[-3] > norm_nitrat[-4]:
                                            grp["Tendens"] = "Stigende"
                                        elif grp['nitrat'].dropna().dropna().tolist()[-1] > grp['nitrat'].dropna().dropna().tolist()[-2]:
                                            grp["Tendens"] = "Stigende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                                elif len(norm_nitrat) <= 10 and len(norm_nitrat) > 5:
                                    if len(positive[0]) >= len(negative[0]) + 1 and positivsum > negativsum * 1.3:
                                        if grp['nitrat'].dropna().dropna().tolist()[-1] < grp['nitrat'].dropna().dropna().tolist()[-2] and abs(grp['nitrat'].dropna().dropna().tolist()[-1] - grp['nitrat'].dropna().dropna().tolist()[
                                                    -2]) < std:  # norm_nitrat[-1] > norm_nitrat[-2] > norm_nitrat[-3] > norm_nitrat[-4]:
                                            grp["Tendens"] = "Stigende"
                                        elif grp['nitrat'].dropna().dropna().tolist()[-1] >= grp['nitrat'].dropna().dropna().tolist()[-2]:
                                            grp["Tendens"] = "Stigende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                                elif len(norm_nitrat) <= 5:
                                    if len(positive[0]) >= len(negative[0]) and positivsum > negativsum * 1.3:
                                        if grp['nitrat'].tolist()[-1] >= grp['nitrat'].tolist()[-2]:
                                            grp["Tendens"] = "Stigende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                                else:
                                    grp["Tendens"] = "Varierende"
                            elif udvikling_nitrat < -20 and norm_nitrat[-1] <= norm_nitrat[0] - 5:
                                if len(norm_nitrat) > 10:
                                    if len(negative[0]) >= len(positive[0]) + 2 and negativsum > positivsum * 1.3:  # (len(sidste50)/5)
                                        if grp['nitrat'].tolist()[-1] > grp['nitrat'].tolist()[-2] and abs(
                                                grp['nitrat'].tolist()[-1] - grp['nitrat'].tolist()[
                                                    -2]) < std:  # norm_nitrat[-1] < norm_nitrat[-2] < norm_nitrat[-3] < norm_nitrat[-4]:
                                            grp["Tendens"] = "Faldende"
                                        elif grp['nitrat'].tolist()[-1] <= grp['nitrat'].tolist()[-2]:
                                            grp["Tendens"] = "Faldende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["NitratTendens"] = "Varierende"
                                elif len(norm_nitrat) <= 10 and len(norm_nitrat) > 5:
                                    if len(negative[0]) >= len(positive[0]) + 1 and negativsum > positivsum * 1.3:
                                        if grp['nitrat'].tolist()[-1] > grp['nitrat'].tolist()[-2] and abs(
                                                grp['nitrat'].tolist()[-1] - grp['nitrat'].tolist()[-2]) < std:
                                            grp["Tendens"] = "Faldende"
                                        elif grp['nitrat'].tolist()[-1] <= grp['nitrat'].tolist()[-2]:
                                            grp["Tendens"] = "Faldende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["NitratTendens"] = "Varierende"
                                elif len(norm_nitrat) <= 5:
                                    if len(negative[0]) >= len(positive[0]) and negativsum > positivsum * 1.3:
                                        if grp['nitrat'].tolist()[-1] <= grp['nitrat'].tolist()[-2]:
                                            grp["Tendens"] = "Faldende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                                else:
                                    grp["Tendens"] = "Varierende"
                            else:
                                grp["Tendens"] = "Varierende"
                        else:
                            grp["Tendens"] = "Stabil"'''

                        nitrattidsserier = pd.concat([nitrattidsserier, grp], ignore_index=False)

                    ################SULFAT######################
                    if len(grp) >= 3 and relativedelta(max(grp['sampledate']),
                                                       min(grp['sampledate'])).years > 3 and datem >= 1970 and (
                            sum(grp['sulfat'].dropna().dropna()) / len(grp)) > 20:

                        if len(grp) > 10:
                            coefficients_sulfat = estimate_coef(grp['aarstal'][3:], grp['sulfat'].dropna()[3:])
                        elif len(grp) <= 10 and len(grp) > 5:
                            coefficients_sulfat = estimate_coef(grp['aarstal'][1:], grp['sulfat'].dropna()[1:])
                        else:
                            coefficients_sulfat = estimate_coef(grp['aarstal'], grp['sulfat'].dropna())

                        udvikling_sulfat = (((coefficients_sulfat[0] * datem + coefficients_sulfat[1]) - (
                                coefficients_sulfat[0] * datemin + coefficients_sulfat[1])) / (
                                                    coefficients_sulfat[0] * datemin + coefficients_sulfat[1])) * 100
                        if len(grp) > 10:
                            sidste50_sul = grp['sulfat'].tolist()[round(len(grp['sulfat'].dropna()) / 2) - 3:]
                        else:
                            sidste50_sul = grp['sulfat'].tolist()

                        positive_sul = np.where(np.diff(sidste50_sul) > 0)
                        negative_sul = np.where(np.diff(sidste50_sul) < 0)

                        positivsum_sul = abs(np.sum(np.where(np.diff(sidste50_sul) > 0, np.diff(sidste50_sul), 0)))
                        negativsum_sul = abs(np.sum(np.where(np.diff(sidste50_sul) < 0, np.diff(sidste50_sul), 0)))
                        std_sul = np.std(grp['sulfat'].tolist())


                        if any(abs(y) > 5 for y in norm_sulfat) and sum(grp["sulfat"].tolist()[-3:]) != 0:
                            if udvikling_sulfat > 20 and norm_sulfat[-1] > norm_sulfat[0] + 5:
                                if len(norm_sulfat) > 10:
                                    if len(positive_sul[0]) >= len(
                                            negative_sul[0]) + 2 and positivsum_sul > negativsum_sul * 1.3:
                                        if grp['sulfat'].tolist()[-1] < grp['sulfat'].tolist()[-2] and abs(
                                                grp['sulfat'].tolist()[-1] - grp['sulfat'].tolist()[-2]) < std_sul:
                                            grp["Tendens"] = "Stigende"
                                        else:
<<<<<<< HEAD
                                            grp["Tendens"] = "Varierende"
=======
                                            grp["Tendens"] = "Stigende"
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb
                                    else:
                                        grp["Tendens"] = "Varierende"
                                elif 5 < len(norm_sulfat) <= 10:
                                    if len(positive_sul[0]) >= len(
                                            negative_sul[0]) + 1 and positivsum_sul > negativsum_sul * 1.3:
                                        if grp['sulfat'].tolist()[-1] < grp['sulfat'].tolist()[-2] and abs(
                                                grp['sulfat'].tolist()[-1] - grp['sulfat'].tolist()[-2]) < std_sul:
                                            grp["Tendens"] = "Stigende"
                                        else:
<<<<<<< HEAD
                                            grp["Tendens"] = "Varierende"
=======
                                            grp["Tendens"] = "Stigende"
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb
                                    else:
                                        grp["Tendens"] = "Varierende"
                                else:
                                    if len(positive_sul[0]) >= len(
                                            negative_sul[0]) and positivsum_sul > negativsum_sul * 1.3:
                                        if grp['sulfat'].tolist()[-1] >= grp['sulfat'].tolist()[-2]:
                                            grp["Tendens"] = "Stigende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                            elif udvikling_sulfat < -20 and norm_sulfat[-1] < norm_sulfat[0] - 5:
                                if len(norm_sulfat) > 10:
                                    if len(negative_sul[0]) >= len(
                                            positive_sul[0]) + 2 and negativsum_sul > positivsum_sul * 1.3:
                                        if grp['sulfat'].tolist()[-1] > grp['sulfat'].tolist()[-2] and abs(
                                                grp['sulfat'].tolist()[-1] - grp['sulfat'].tolist()[-2]) < std_sul:
                                            grp["Tendens"] = "Faldende"
                                        else:
<<<<<<< HEAD
                                            grp["Tendens"] = "Varierende"
=======
                                            grp["Tendens"] = "Faldende"
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb
                                    else:
                                        grp["Tendens"] = "Varierende"
                                elif 5 < len(norm_sulfat) <= 10:
                                    if len(negative_sul[0]) >= len(
                                            positive_sul[0]) + 1 and negativsum_sul > positivsum_sul * 1.3:
                                        if grp['sulfat'].tolist()[-1] > grp['sulfat'].tolist()[-2] and abs(
                                                grp['sulfat'].tolist()[-1] - grp['sulfat'].tolist()[-2]) < std_sul:
                                            grp["Tendens"] = "Faldende"
                                        else:
<<<<<<< HEAD
                                            grp["Tendens"] = "Varierende"
=======
                                            grp["Tendens"] = "Faldende"
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb
                                    else:
                                        grp["Tendens"] = "Varierende"
                                else:
                                    if len(negative_sul[0]) >= len(
                                            positive_sul[0]) and negativsum_sul > positivsum_sul * 1.3:
                                        if grp['sulfat'].tolist()[-1] <= grp['sulfat'].tolist()[-2]:
                                            grp["Tendens"] = "Faldende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                            else:
                                grp["Tendens"] = "Varierende"
                        else:
                            grp["Tendens"] = "Stabil"

                        '''
                        if any(abs(y) > 5 for y in norm_sulfat) and sum(grp["sulfat"].tolist()[-3:]) != 0:
                            if udvikling_sulfat > 20 and norm_sulfat[-1] > norm_sulfat[0] + 5:
                                if len(norm_sulfat) > 10:
                                    if len(positive_sul[0]) >= len(negative_sul[
                                                                       0]) + 2 and positivsum_sul > negativsum_sul * 1.3:  # (len(sidste50_sul)/5)
                                        if grp['sulfat'].tolist()[-1] < grp['sulfat'].tolist()[-2] and abs(
                                                grp['sulfat'].tolist()[-1] - grp['sulfat'].tolist()[-2]) < std_sul:
                                            grp["Tendens"] = "Stigende"
                                        elif grp['sulfat'].tolist()[-1] >= grp['sulfat'].tolist()[-2]:
                                            grp["Tendens"] = "Stigende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"

                                elif len(norm_sulfat) <= 10 and len(norm_sulfat) > 5:
                                    if len(positive_sul[0]) >= len(
                                            negative_sul[0]) + 1 and positivsum_sul > negativsum_sul * 1.3:
                                        if grp['sulfat'].tolist()[-1] < grp['sulfat'].tolist()[-2] and abs(
                                                grp['sulfat'].tolist()[-1] - grp['sulfat'].tolist()[-2]) < std_sul:
                                            grp["Tendens"] = "Stigende"
                                        elif grp['sulfat'].tolist()[-1] >= grp['sulfat'].tolist()[-2]:
                                            grp["Tendens"] = "Stigende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                                elif len(norm_sulfat) <= 5:
                                    if len(positive_sul[0]) >= len(
                                            negative_sul[0]) and positivsum_sul > negativsum_sul * 1.3:
                                        if grp['sulfat'].tolist()[-1] >= grp['sulfat'].tolist()[-2]:
                                            grp["Tendens"] = "Stigende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                                else:
                                    grp["Tendens"] = "Varierende"
                            elif udvikling_sulfat < -20 and norm_sulfat[-1] < norm_sulfat[0] - 5:
                                if len(norm_sulfat) > 10:
                                    if len(negative_sul[0]) >= len(positive_sul[
                                                                       0]) + 2 and negativsum_sul > positivsum_sul * 1.3:  # (len(sidste50_sul)/5)

                                        if grp['sulfat'].tolist()[-1] > grp['sulfat'].tolist()[-2] and abs(
                                                grp['sulfat'].tolist()[-1] - grp['sulfat'].tolist()[-2]) < std_sul:
                                            grp["Tendens"] = "Faldende"
                                        elif grp['sulfat'].tolist()[-1] <= grp['sulfat'].tolist()[-2]:
                                            grp["Tendens"] = "Faldende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                                elif len(norm_sulfat) <= 10 and len(norm_sulfat) > 5:
                                    if len(negative_sul[0]) >= len(
                                            positive_sul[0]) + 1 and negativsum_sul > positivsum_sul * 1.3:
                                        if grp['sulfat'].tolist()[-1] > grp['sulfat'].tolist()[-2] and abs(
                                                grp['sulfat'].tolist()[-1] - grp['sulfat'].tolist()[-2]) < std_sul:
                                            grp["Tendens"] = "Faldende"
                                        elif grp['sulfat'].tolist()[-1] <= grp['sulfat'].tolist()[-2]:
                                            grp["Tendens"] = "Faldende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                                elif len(norm_sulfat) <= 5:
                                    if len(negative_sul[0]) >= len(
                                            positive_sul[0]) and negativsum_sul > positivsum_sul * 1.3:
                                        if grp['sulfat'].tolist()[-1] <= grp['sulfat'].tolist()[-2]:
                                            grp["Tendens"] = "Faldende"
                                        else:
                                            grp["Tendens"] = "Varierende"
                                    else:
                                        grp["Tendens"] = "Varierende"
                                else:
                                    grp["Tendens"] = "Varierende"
                            else:
                                grp["Tendens"] = "Varierende"
                        else:
                            grp["Tendens"] = "Stabil"
                        '''
                        sulfattidsserier = pd.concat([sulfattidsserier, grp], ignore_index=True)
                ########## LIDT SKETCHY ###############


                nitrattidsserier.to_csv(temp + '/nitrattidsserier.csv', index=False)

                path0 = 'file:///' + temp + '/nitrattidsserier.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                    "utf8", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")

                mylayer0 = QgsVectorLayer(path0, "Nitrat tidsserier", "delimitedtext")

                sulfattidsserier.to_csv(temp + '/sulfattidsserier.csv', index=False)

                path00 = 'file:///' + temp + '/sulfattidsserier.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                    "utf8", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")

                mylayer00 = QgsVectorLayer(path00, "Sulfat tidsserier", "delimitedtext")

                nitratseneste.to_csv(temp + '/nitratseneste.csv', index=False)

                path = 'file:///' + temp + '/nitratseneste.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                    "utf8", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")

                mylayer = QgsVectorLayer(path, "Seneste Nitrat", "delimitedtext")

                sulfatseneste.to_csv(temp + '/sulfatseneste.csv', index=False)

                path2 = 'file:///' + temp + '/sulfatseneste.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                    "utf8", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")

                mylayer2 = QgsVectorLayer(path2, "Seneste Sulfat", "delimitedtext")

                vandtypeseneste.to_csv(temp + '/vandtypeseneste.csv', index=False)

                path3 = 'file:///' + temp + '/vandtypeseneste.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                    "utf8", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")

                mylayer3 = QgsVectorLayer(path3, "Seneste Vandtype", "delimitedtext")

                kloridseneste.to_csv(temp + '/kloridseneste.csv', index=False)

                path4 = 'file:///' + temp + '/kloridseneste.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                    "utf8", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")

                mylayer4 = QgsVectorLayer(path4, "Seneste Klorid", "delimitedtext")

                strontiumseneste.to_csv(temp + '/strontiumseneste.csv', index=False)

                path5 = 'file:///' + temp + '/strontiumseneste.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                    "utf8", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")

                mylayer5 = QgsVectorLayer(path5, "Seneste Strontium", "delimitedtext")

                fluoridseneste.to_csv(temp + '/fluoridseneste.csv', index=False)

                path6 = 'file:///' + temp + '/fluoridseneste.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                    "utf8", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")

                mylayer6 = QgsVectorLayer(path6, "Seneste Fluorid", "delimitedtext")


                arsenseneste.to_csv(temp + '/arsenseneste.csv', index=False)

                path9 = 'file:///' + temp + '/arsenseneste.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                    "utf8", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")

                mylayer9 = QgsVectorLayer(path9, "Seneste Arsen", "delimitedtext")

                jernseneste.to_csv(temp + '/jernseneste.csv', index=False)

                path10 = 'file:///' + temp + '/jernseneste.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
<<<<<<< HEAD
                    "utf8", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")
=======
                    "latin0", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb

                mylayer10 = QgsVectorLayer(path10, "Seneste Jern", "delimitedtext")

                def parse_geometry(geometry):
                    regex = r'[0-9-\.]+'
                    parsed_geom = re.findall(regex, geometry)
                    parsed_geom = [float(i) for i in parsed_geom]
                    return min(parsed_geom[::2]), max(parsed_geom[::2]), min(parsed_geom[1::2]), max(parsed_geom[1::2])

                boundingbox = parse_geometry(geom.asWkt())
                boundingbox = list(boundingbox)

                crstrue = int("".join(filter(str.isdigit, str(områdelayer.crs().authid()))))

                url = "pagingEnabled='true' preferCoordinatesForWfsT11='True' srsname='{}' typename='mc_grp_analyse' url='https://data.geus.dk/geusmap/ows/{}.jsp?whoami=makyn@mst.dk&LAYERS=mc_grp_analyse&mapname=grundvand&bbox={},{},{},{}&url='https://data.geus.dk/geusmap/ows/{}.jsp?whoami=makyn@mst.dk&LAYERS=mc_grp_analyse&version=1.0.0&mapname=grundvand&bbox={},{},{},{}'".format(
                    områdelayer.crs().authid(), crstrue, boundingbox[0], boundingbox[2], boundingbox[1],
                    boundingbox[3], crstrue, boundingbox[0], boundingbox[2],
                    boundingbox[1], boundingbox[3])

                vlayer = QgsVectorLayer(url, "Pesticider GEUS", "wfs")
                vlayer.updateFields()

                aaa = processing.run("native:clip", {'INPUT': vlayer,
                                                     'OVERLAY': områdelayer, 'OUTPUT': 'TEMPORARY_OUTPUT'})
                aaa["OUTPUT"].updateFields()

                aa = processing.run("native:extractbyattribute", {'INPUT': aaa["OUTPUT"],
                    'FIELD': 'stofgruppe', 'OPERATOR': 0, 'VALUE': '50', 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

                aa.updateFields()

                pfas = processing.run("native:extractbyattribute", {'INPUT': aaa["OUTPUT"],
                    'FIELD': 'stofgruppe', 'OPERATOR': 0, 'VALUE': '110', 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]
                pfas.updateFields()

                cols = [f.name() for f in aa.fields()]
                datagen = ([f[col] for col in cols] for f in aa.getFeatures())
                dfpest = pd.DataFrame.from_records(data=datagen, columns=cols)

                cols2 = [f.name() for f in pfas.fields()]
                datagen2 = ([f[col] for col in cols2] for f in pfas.getFeatures())
                dfpfas = pd.DataFrame.from_records(data=datagen2, columns=cols2)


                df["dgu2"] = df["dgu_indtag"].astype(str).str.strip()
                df["dgu2"].astype(str).str.strip()
                df["dgu2"] = df["dgu2"].str.replace(' ', '')
                dfpest.to_csv("C:/Temp/test.csv")
                dfpest["dgu_nr"].astype(str)
                dfpest["indtag_nr"].astype(str)
                dfpest["dgu2"] = dfpest["dgu_nr"].astype(str) + "_" + dfpest["indtag_nr"].astype(str)
                dfpest["dgu2"].astype(str).str.strip()
                dfpest["dgu2"] = dfpest["dgu2"].str[:-2]
                #dfpest["dgu2"] = dfpest["dgu2"].str.replace(' ', '')

                dfpfas["dgu_nr"].astype(str)
                dfpfas["indtag_nr"].astype(str)
                dfpfas["dgu2"] = dfpfas["dgu_nr"].astype(str) + "_" + dfpfas["indtag_nr"].astype(str)
                dfpfas["dgu2"].astype(str).str.strip()
                dfpfas["dgu2"] = dfpfas["dgu2"].str[:-2]           #.replace(".0", '')
                #dfpfas["dgu2"] = dfpfas["dgu2"].str.replace(' ', '')

                dfpest2 = pd.merge(dfpest, df[['dgu2', 'magasin', 'dybde_filtertop', 'use', 'purpose']], how='inner',
                                   on='dgu2')

                dfpest2.to_csv(temp + '/pesticiderseneste.csv', index=False)

                path7 = 'file:///' + temp + '/pesticiderseneste.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                    "utf8", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")

                mylayer7 = QgsVectorLayer(path7, "Seneste Pesticider", "delimitedtext")



                dfpfas2 = pd.merge(dfpfas, df[['dgu2', 'magasin', 'dybde_filtertop', 'use', 'purpose']], how='inner',
                                   on='dgu2')
                dfpfas2.to_csv(temp + '/pfasseneste.csv', index=False)

                path8 = 'file:///' + temp + '/pfasseneste.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                    "utf8", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")

                mylayer8 = QgsVectorLayer(path8, "Seneste Pesticider", "delimitedtext")


                magasintildelingdf = write_qtable_to_df(self.tableWidget)
                rows = df['magasin'].unique().tolist()
                rows = [str(item).replace('nan', "Ukendt magasin") for item in rows]

                def custom_sort(item):
                    # Use regular expression to extract the prefix and the numeric part
                    match = re.match(r'([A-Z]+)(\d+)', item)
                    if match:
                        prefix, number = match.groups()
                        # Create a tuple to use as the sorting key
                        return (int(number), prefix)
                    else:
                        # Return a default value for items that don't match the expected format
                        return (float('inf'), '')

                rows.sort(key=custom_sort)


                magasintildelingdf['Magasin'] = rows
                antaltildelinger = magasintildelingdf['Magasintildeling'].nunique()


                maglayer = QgsProject.instance().mapLayersByName('MagasinTemplate')[0]



                for i, row in magasintildelingdf.iterrows():
                    try:
                        maglayer.renderer().rootRule().children()[i].setLabel('{}'.format(row['Magasin']))
                    except:
                        pass


                groupnitrat = root.insertGroup(0, "Seneste Nitrat")
                groupsulfat = root.insertGroup(0, "Seneste Sulfat")
                groupvandtype = root.insertGroup(0, "Seneste Vandtype")
                groupklorid = root.insertGroup(0, "Seneste Klorid")
                groupstrontium = root.insertGroup(0, "Seneste Strontium")
                groupfluorid = root.insertGroup(0, "Seneste Fluorid")
                groupjern = root.insertGroup(0, "Seneste Jern")

                grouparsen = root.insertGroup(0, "Seneste Arsen")
                grouppest = root.insertGroup(0, "Seneste Pesticider")
                grouppfas = root.insertGroup(0, "Seneste PFAS")


<<<<<<< HEAD

              #  possibleshapes = [QgsSimpleMarkerSymbolLayerBase.Circle,
               #                   QgsSimpleMarkerSymbolLayerBase.Square,
                #                  QgsSimpleMarkerSymbolLayerBase.Triangle,
                 #                 QgsSimpleMarkerSymbolLayerBase.Diamond,
                  #                QgsSimpleMarkerSymbolLayerBase.Star,
                   #               QgsSimpleMarkerSymbolLayerBase.Pentagon,
                    #              QgsSimpleMarkerSymbolLayerBase.Hexagon,
                     #             QgsSimpleMarkerSymbolLayerBase.CrossFill,
                      #            QgsSimpleMarkerSymbolLayerBase.ArrowHeadFilled,
                       #           QgsSimpleMarkerSymbolLayerBase.QuarterCircle,
                        #          QgsSimpleMarkerSymbolLayerBase.HalfSquare,
                         #         QgsSimpleMarkerSymbolLayerBase.RightHalfTriangle,
                          #        QgsSimpleMarkerSymbolLayerBase.LeftHalfTriangle,
                           #       QgsSimpleMarkerSymbolLayerBase.SemiCircle,
                            #      QgsSimpleMarkerSymbolLayerBase.ThirdCircle,
                             #     QgsSimpleMarkerSymbolLayerBase.DiagonalHalfSquare,
                              #    QgsSimpleMarkerSymbolLayerBase.Octagon]

                #possiblesizes = [2.7, 2.5, 3, 3.5, 3.5, 3.3, 3.5, 3.5, 4.5, 4.5, 2.5, 2.9, 2.9, 3, 3, 2.8, 3.2]


                possibleshapes = []
                possiblemagnames = []
=======
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb

              #  possibleshapes = [QgsSimpleMarkerSymbolLayerBase.Circle,
               #                   QgsSimpleMarkerSymbolLayerBase.Square,
                #                  QgsSimpleMarkerSymbolLayerBase.Triangle,
                 #                 QgsSimpleMarkerSymbolLayerBase.Diamond,
                  #                QgsSimpleMarkerSymbolLayerBase.Star,
                   #               QgsSimpleMarkerSymbolLayerBase.Pentagon,
                    #              QgsSimpleMarkerSymbolLayerBase.Hexagon,
                     #             QgsSimpleMarkerSymbolLayerBase.CrossFill,
                      #            QgsSimpleMarkerSymbolLayerBase.ArrowHeadFilled,
                       #           QgsSimpleMarkerSymbolLayerBase.QuarterCircle,
                        #          QgsSimpleMarkerSymbolLayerBase.HalfSquare,
                         #         QgsSimpleMarkerSymbolLayerBase.RightHalfTriangle,
                          #        QgsSimpleMarkerSymbolLayerBase.LeftHalfTriangle,
                           #       QgsSimpleMarkerSymbolLayerBase.SemiCircle,
                            #      QgsSimpleMarkerSymbolLayerBase.ThirdCircle,
                             #     QgsSimpleMarkerSymbolLayerBase.DiagonalHalfSquare,
                              #    QgsSimpleMarkerSymbolLayerBase.Octagon]

                #possiblesizes = [2.7, 2.5, 3, 3.5, 3.5, 3.3, 3.5, 3.5, 4.5, 4.5, 2.5, 2.9, 2.9, 3, 3, 2.8, 3.2]


                possibleshapes = []
                possiblemagnames = []
                for k, x in enumerate(magasintildelingdf['Magasin']):
                    markerstyle_text = magasintildelingdf.loc[k, 'Stilart']

                    if markerstyle_text == "Cirkel":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.Circle
                        possiblesize = 2.7
                    elif markerstyle_text == "Firkant":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.Square
                        possiblesize = 2.5
                    elif markerstyle_text == "Diamant":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.Diamond
                        possiblesize = 3.5
                    elif markerstyle_text == "Kryds":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.CrossFill
                        possiblesize = 3.5
                    elif markerstyle_text == "Trekant":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.Triangle
                        possiblesize = 3
                    elif markerstyle_text == "Femkant":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.Pentagon
                        possiblesize = 3.3
                    elif markerstyle_text == "Stjerne":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.Star
                        possiblesize = 3.5
                    elif markerstyle_text == "Pilehoved":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.ArrowHeadFilled
                        possiblesize = 4.5

                    elif markerstyle_text == "Halvcirkel":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.SemiCircle
                        possiblesize = 3

                    elif markerstyle_text == "Rektangel":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.HalfSquare
                        possiblesize = 2.5

                    elif markerstyle_text == "Højre-halv trekant":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.RightHalfTriangle
                        possiblesize = 2.9

                    elif markerstyle_text == "Venstre-halv trekant":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.LeftHalfTriangle
                        possiblesize = 2.9

                    elif markerstyle_text == "Kvartcirkel":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.QuarterCircle
                        possiblesize = 4.5

                    elif markerstyle_text == "Diagonal trekant":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.DiagonalHalfSquare
                        possiblesize = 2.8

                    elif markerstyle_text == "Oktagon":
                        possibleshape = QgsSimpleMarkerSymbolLayerBase.Octagon
                        possiblesize = 3.2

                    possibleshapes.append(possibleshape)
                    possiblemagnames.append(x)

<<<<<<< HEAD

=======
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb
                    if x == 'Ukendt magasin':
                        mylay2 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer,
                            'EXPRESSION': "magasin is NULL", 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay3 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer2,
                            'EXPRESSION': "magasin is NULL", 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay4 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer3,
                            'EXPRESSION': "magasin is NULL", 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay5 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer4,
                            'EXPRESSION': "magasin is NULL", 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay6 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer5,
                            'EXPRESSION': "magasin is NULL", 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay7 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer6,
                            'EXPRESSION': "magasin is NULL", 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay8 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer7,
                            'EXPRESSION': "magasin is NULL", 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay9 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer8,
                            'EXPRESSION': "magasin is NULL", 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay10 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer9,
                            'EXPRESSION': "magasin is NULL", 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

                        mylay11 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer10,
                            'EXPRESSION': "magasin is NULL", 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

                        mylay0 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer0,
                            'EXPRESSION': "magasin is NULL", 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay00 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer00,
                            'EXPRESSION': "magasin is NULL", 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

                    else:

                        mylay2 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer,
                            'EXPRESSION': "magasin = '{}'".format(x), 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

                        mylay3 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer2,
                            'EXPRESSION': "magasin = '{}'".format(x), 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay4 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer3,
                            'EXPRESSION': "magasin = '{}'".format(x), 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay5 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer4,
                            'EXPRESSION': "magasin = '{}'".format(x), 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay6 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer5,
                            'EXPRESSION': "magasin = '{}'".format(x), 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay7 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer6,
                            'EXPRESSION': "magasin = '{}'".format(x), 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay8 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer7,
                            'EXPRESSION': "magasin = '{}'".format(x), 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay9 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer8,
                            'EXPRESSION': "magasin = '{}'".format(x), 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay10 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer9,
                            'EXPRESSION': "magasin = '{}'".format(x), 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay11 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer10,
                            'EXPRESSION': "magasin = '{}'".format(x), 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

                        mylay0 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer0,
                            'EXPRESSION': "magasin = '{}'".format(x), 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                        mylay00 = processing.run("native:extractbyexpression", {
                            'INPUT': mylayer00,
                            'EXPRESSION': "magasin = '{}'".format(x), 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

                    _writer = QgsVectorFileWriter.writeAsVectorFormat(mylay2,
                                                                      str(gem) + '/Seneste Nitrat - {}.gpkg'.format(x),
                                                                      'Seneste Nitrat', mylay2.crs())

                    nitrat = QgsVectorLayer(str(gem) + '/Seneste Nitrat - {}.gpkg'.format(x), 'Seneste Nitrat', 'ogr')
                    nitrat.setName('Seneste Nitrat - {}'.format(x))

                    _writer = QgsVectorFileWriter.writeAsVectorFormat(mylay3,
                                                                      str(gem) + '/Seneste Sulfat - {}.gpkg'.format(x),
                                                                      'Seneste Sulfat', mylay3.crs())

                    sulfat = QgsVectorLayer(str(gem) + '/Seneste Sulfat - {}.gpkg'.format(x), 'Seneste Sulfat', 'ogr')
                    sulfat.setName('Seneste Sulfat - {}'.format(x))

                    _writer = QgsVectorFileWriter.writeAsVectorFormat(mylay4,
                                                                      str(gem) + '/Seneste Vandtype - {}.gpkg'.format(x),
                                                                      'Seneste Vandtype', mylay4.crs())

                    vandtype = QgsVectorLayer(str(gem) + '/Seneste Vandtype - {}.gpkg'.format(x), 'Seneste Vandtype', 'ogr')
                    vandtype.setName('Seneste Vandtype - {}'.format(x))

                    _writer = QgsVectorFileWriter.writeAsVectorFormat(mylay5,
                                                                      str(gem) + '/Seneste Klorid - {}.gpkg'.format(x),
                                                                      'Seneste Klorid', mylay5.crs())

                    klorid = QgsVectorLayer(str(gem) + '/Seneste Klorid - {}.gpkg'.format(x), 'Seneste Klorid',
                                            'ogr')
                    klorid.setName('Seneste Klorid - {}'.format(x))

                    _writer = QgsVectorFileWriter.writeAsVectorFormat(mylay6,
                                                                      str(gem) + '/Seneste Strontium - {}.gpkg'.format(x),
                                                                      'Seneste Strontium', mylay6.crs())

                    strontium = QgsVectorLayer(str(gem) + '/Seneste Strontium - {}.gpkg'.format(x), 'Seneste Strontium',
                                               'ogr')
                    strontium.setName('Seneste Strontium - {}'.format(x))

                    _writer = QgsVectorFileWriter.writeAsVectorFormat(mylay7,
                                                                      str(gem) + '/Seneste Fluorid - {}.gpkg'.format(x),
                                                                      'Seneste Fluorid', mylay7.crs())

                    fluorid = QgsVectorLayer(str(gem) + '/Seneste Fluorid - {}.gpkg'.format(x), 'Seneste Fluorid',
                                             'ogr')
                    fluorid.setName('Seneste Fluorid - {}'.format(x))

                    _writer = QgsVectorFileWriter.writeAsVectorFormat(mylay8,
                                                                      str(gem) + '/Seneste Pesticider - {}.gpkg'.format(x),
                                                                      'Seneste Pesticider', mylay8.crs())

                    pesticider = QgsVectorLayer(str(gem) + '/Seneste Pesticider - {}.gpkg'.format(x), 'Seneste Pesticider',
                                                'ogr')
                    pesticider.setName('Seneste Pesticider - {}'.format(x))



                    _writer = QgsVectorFileWriter.writeAsVectorFormat(mylay9,
                                                                      str(gem) + '/Seneste PFAS - {}.gpkg'.format(x),
                                                                      'Seneste PFAS', mylay9.crs())

                    pfasser = QgsVectorLayer(str(gem) + '/Seneste PFAS - {}.gpkg'.format(x), 'Seneste PFAS',
                                                'ogr')
                    pfasser.setName('Seneste PFAS - {}'.format(x))




                    _writer = QgsVectorFileWriter.writeAsVectorFormat(mylay10,
                                                                      str(gem) + '/Seneste Arsen - {}.gpkg'.format(x),
                                                                      'Seneste Arsen', mylay10.crs())

                    arsen = QgsVectorLayer(str(gem) + '/Seneste Arsen - {}.gpkg'.format(x), 'Seneste Arsen',
                                             'ogr')
                    arsen.setName('Seneste Arsen - {}'.format(x))

                    _writer = QgsVectorFileWriter.writeAsVectorFormat(mylay11,
                                                                      str(gem) + '/Seneste Jern - {}.gpkg'.format(x),
                                                                      'Seneste Jern', mylay11.crs())

                    jern = QgsVectorLayer(str(gem) + '/Seneste Jern - {}.gpkg'.format(x), 'Seneste Jern', 'ogr')
                    jern.setName('Seneste Jern - {}'.format(x))


                    _writer = QgsVectorFileWriter.writeAsVectorFormat(mylay0,
                                                                      str(gem) + '/Nitrat Tidsserier - {}.gpkg'.format(x),
                                                                      'Nitrat Tidsserier', mylay0.crs())

                    tidsserienitrat = QgsVectorLayer(str(gem) + '/Nitrat Tidsserier - {}.gpkg'.format(x),
                                                     'Nitrat Tidsserier',
                                                     'ogr')
                    tidsserienitrat.setName('Nitrat Tidsserier - {}'.format(x))
                    tidsserienitrat.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Tidsserier.qml")

                    _writer = QgsVectorFileWriter.writeAsVectorFormat(mylay00,
                                                                      str(gem) + '/Sulfat Tidsserier - {}.gpkg'.format(x),
                                                                      'Sulfat Tidsserier', mylay00.crs())

                    tidsseriesulfat = QgsVectorLayer(str(gem) + '/Sulfat Tidsserier - {}.gpkg'.format(x),
                                                     'Sulfat Tidsserier',
                                                     'ogr')
                    tidsseriesulfat.setName('Sulfat Tidsserier - {}'.format(x))
                    tidsseriesulfat.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Tidsserier.qml")

                    project.addMapLayer(nitrat, False)
                    groupnitrat.addLayer(nitrat)
                    project.addMapLayer(tidsserienitrat, False)
                    groupnitrat.addLayer(tidsserienitrat)

                    project.addMapLayer(sulfat, False)
                    groupsulfat.addLayer(sulfat)
                    project.addMapLayer(tidsseriesulfat, False)
                    groupsulfat.addLayer(tidsseriesulfat)

                    project.addMapLayer(vandtype, False)
                    groupvandtype.addLayer(vandtype)

                    project.addMapLayer(klorid, False)
                    groupklorid.addLayer(klorid)

                    project.addMapLayer(strontium, False)
                    groupstrontium.addLayer(strontium)

                    project.addMapLayer(fluorid, False)
                    groupfluorid.addLayer(fluorid)

                    project.addMapLayer(arsen, False)
                    grouparsen.addLayer(arsen)

                    project.addMapLayer(jern, False)
                    groupjern.addLayer(jern)

                    project.addMapLayer(pesticider, False)
                    grouppest.addLayer(pesticider)

                    project.addMapLayer(pfasser, False)
                    grouppfas.addLayer(pfasser)

                    # Get layer object
                    layer6 = QgsProject().instance().mapLayersByName('Seneste Nitrat - {}'.format(x))[0]  #
                    layer7 = QgsProject().instance().mapLayersByName('Seneste Sulfat - {}'.format(x))[0]  #
                    layer8 = QgsProject().instance().mapLayersByName('Seneste Vandtype - {}'.format(x))[0]  #
                    layer9 = QgsProject().instance().mapLayersByName('Seneste Klorid - {}'.format(x))[0]  #
                    layer10 = QgsProject().instance().mapLayersByName('Seneste Strontium - {}'.format(x))[0]  #
                    layer11 = QgsProject().instance().mapLayersByName('Seneste Fluorid - {}'.format(x))[0]  #
                    layer12 = QgsProject().instance().mapLayersByName('Seneste Pesticider - {}'.format(x))[0]  #
                    layer13 = QgsProject().instance().mapLayersByName('Seneste PFAS - {}'.format(x))[0]  #
                    layer14 = QgsProject().instance().mapLayersByName('Seneste Arsen - {}'.format(x))[0]  #
                    layer15 = QgsProject().instance().mapLayersByName('Seneste Jern - {}'.format(x))[0]  #

                    symbol = QgsSymbol.defaultSymbol(nitrat.geometryType())
                    renderer = QgsRuleBasedRenderer(symbol)

                    symbol2 = QgsSymbol.defaultSymbol(sulfat.geometryType())
                    renderer2 = QgsRuleBasedRenderer(symbol2)

                    symbol3 = QgsSymbol.defaultSymbol(vandtype.geometryType())
                    renderer3 = QgsRuleBasedRenderer(symbol3)

                    symbol4 = QgsSymbol.defaultSymbol(klorid.geometryType())
                    renderer4 = QgsRuleBasedRenderer(symbol4)

                    symbol5 = QgsSymbol.defaultSymbol(strontium.geometryType())
                    renderer5 = QgsRuleBasedRenderer(symbol5)

                    symbol6 = QgsSymbol.defaultSymbol(fluorid.geometryType())
                    renderer6 = QgsRuleBasedRenderer(symbol6)

                    symbol7 = QgsSymbol.defaultSymbol(pesticider.geometryType())
                    renderer7 = QgsRuleBasedRenderer(symbol7)

                    symbol8 = QgsSymbol.defaultSymbol(pfasser.geometryType())
                    renderer8 = QgsRuleBasedRenderer(symbol8)

                    symbol9 = QgsSymbol.defaultSymbol(arsen.geometryType())
                    renderer9 = QgsRuleBasedRenderer(symbol9)

                    symbol10 = QgsSymbol.defaultSymbol(jern.geometryType())
                    renderer10 = QgsRuleBasedRenderer(symbol10)

                    rules = [['≤ 1', """"nitrat" <= 1""", possibleshape, '#007df2', possiblesize],
                             [']1-10]', """"nitrat" <= 10 AND nitrat > 1""", possibleshape, 'Green',
                              possiblesize],
                             [']10-25]', """"nitrat" <= 25 AND nitrat > 10""", possibleshape, 'Yellow',
                              possiblesize],
                             [']25-50]', """"nitrat" <= 50 AND nitrat > 25""", possibleshape, 'Orange',
                              possiblesize],
                             ['> 50', """"nitrat" > 50""", possibleshape, 'Red', possiblesize]]

                    rules2 = [['≤ 5', """"sulfat" <= 5""", possibleshape, '#007df2', possiblesize],
                              [']5-20]', """"sulfat" <= 20 AND sulfat > 5""", possibleshape, 'Green',
                               possiblesize],
                              [']20-70]', """"sulfat" <= 70 AND sulfat > 20""", possibleshape, 'Yellow',
                               possiblesize],
                              [']70-250]', """"sulfat" <= 250 AND sulfat > 70""", possibleshape, 'Orange',
                               possiblesize],
                              ['> 250', """"sulfat" > 250""", possibleshape, 'Red', possiblesize]]

                    rules3 = [['A', """"Vandtype" = 'A'""", possibleshape, 'Red', possiblesize],
                              ['AB', """"Vandtype" = 'AB'""", possibleshape, 'Orange', possiblesize],
                              ['B', """"Vandtype" = 'B'""", possibleshape, '#eec886', possiblesize],
                              ['C2', """"Vandtype" = 'C2'""", possibleshape, 'Yellow', possiblesize],
                              ['C1', """"Vandtype" = 'C1'""", possibleshape, 'Green', possiblesize],
                              ['D', """"Vandtype" = 'D'""", possibleshape, '#007df2', possiblesize],
                              ['X', """"Vandtype" = 'X'""", possibleshape, '#ff12bc', possiblesize],
                              ['Y', """"Vandtype" = 'Y'""", possibleshape, '#000000', possiblesize]]

                    rules4 = [['≤ 30', """"chlorid" <= 30""", possibleshape, '#007df2', possiblesize],
                              [']30-75]', """"chlorid" <= 75 AND chlorid > 30""", possibleshape, 'Green',
                               possiblesize],
                              [']75-125]', """"chlorid" <= 125 AND chlorid > 75""", possibleshape, 'Yellow',
                               possiblesize],
                              [']125-250]', """"chlorid" <= 250 AND chlorid > 125""", possibleshape, 'Orange',
                               possiblesize],
                              ['> 250', """"chlorid" > 250""", possibleshape, 'Red', possiblesize]]

                    rules5 = [
                        ['≤ 5000', """"strontium" <= 5000""", possibleshape, 'Green',
                         possiblesize],
                        [']5000-10000]', """"strontium" <= 10000 AND strontium > 5000""", possibleshape, 'Yellow',
                         possiblesize],
                        [']10000-20000]', """"strontium" <= 20000 AND strontium > 10000""", possibleshape, 'Orange',
                         possiblesize],
                        ['> 20000', """"strontium" > 20000""", possibleshape, 'Red', possiblesize]]

                    rules6 = [['≤ 0,5', """"fluorid" <= 0.5""", possibleshape, '#007df2', possiblesize],
                              [']0,5-1]', """"fluorid" <= 1 AND fluorid > 0.5""", possibleshape, 'Green',
                               possiblesize],
                              [']1-1,5]', """"fluorid" <= 1.5 AND fluorid > 1""", possibleshape, 'Yellow',
                               possiblesize],
                              [']1,5-5]', """"fluorid" <= 5 AND fluorid > 1.5""", possibleshape, 'Orange',
                               possiblesize],
                              ['> 5', """"fluorid" > 5""", possibleshape, 'Red', possiblesize]]

                    rules7 = [['Intet nu, intet tidligere', """"status_indtag" = 1""", possibleshape, '#007df2',
                               possiblesize],
                              ['Intet nu, tidligere fundet', """"status_indtag" = 2""", possibleshape, 'Green',
                               possiblesize],
                              ['Aktuelt fund under grænseværdi', """"status_indtag" = 3""", possibleshape, 'Yellow',
                               possiblesize],
                              ['Aktuelt fund under grænseværdi, tidligere over', """"status_indtag" = 4""",
                               possibleshape, 'Orange', possiblesize],
                              ['Aktuelt fund over grænseværdi', """"status_indtag" = 5""", possibleshape, 'Red',
                               possiblesize]]

                    rules8 = [['Intet nu, intet tidligere', """"status_indtag" = 1""", possibleshape, '#007df2',
                               possiblesize],
                              ['Intet nu, tidligere fundet', """"status_indtag" = 2""", possibleshape, 'Green',
                               possiblesize],
                              ['Aktuelt fund under grænseværdi', """"status_indtag" = 3""", possibleshape, 'Yellow',
                               possiblesize],
                              ['Aktuelt fund under grænseværdi, tidligere over', """"status_indtag" = 4""",
                               possibleshape, 'Orange', possiblesize],
                              ['Aktuelt fund over grænseværdi', """"status_indtag" = 5""", possibleshape, 'Red',
                               possiblesize]]


                    rules9 = [['≤ 0,03', """"arsen" <= 0.03""", possibleshape, '#007df2', possiblesize],
                              [']0,03-1]', """"arsen" <= 1 AND arsen > 0.03""", possibleshape, 'Green',
                               possiblesize],
                              [']1-1,5]', """"arsen" <= 2.5 AND arsen > 1""", possibleshape, 'Yellow',
                               possiblesize],
                              [']1,5-5]', """"arsen" <= 5 AND arsen > 1.5""", possibleshape, 'Orange',
                               possiblesize],
                              ['> 5', """"arsen" > 5""", possibleshape, 'Red', possiblesize]]


                    rules10 = [['≤ 50', """"jern" <= 50""", possibleshape, '#007df2', possiblesize],
                              [']50-100]', """"jern" <= 100 AND jern > 50""", possibleshape, 'Green',
                               possiblesize],
                              [']100-150]', """"jern" <= 150 AND jern > 100""", possibleshape, 'Yellow',
                               possiblesize],
                              [']150-200]', """"jern" <= 200 AND jern > 150""", possibleshape, 'Orange',
                               possiblesize],
                              ['> 200', """"jern" > 200""", possibleshape, 'Red', possiblesize]]


                    def rule_based_symbology(layer, renderer, label, expression, symbol, shape, color, size, enhed):
                        if not isinstance(renderer, QgsRuleBasedRenderer):
                            symbol = QgsSymbol.defaultSymbol(layer.geometryType())  # You can adjust this to suit your needs
                            renderer = QgsRuleBasedRenderer(QgsRuleBasedRenderer.Rule(symbol))

                        root_rule = renderer.rootRule()
                        rule = root_rule.children()[0].clone()
                        rule.setLabel(label)
                        rule.setFilterExpression(expression)
                        rule.symbol().symbolLayer(0).setShape(shape)
<<<<<<< HEAD
=======
                        print(type(root_rule))
                        print(type(rule))
                        print(type(rule.symbol()))
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb

                        rule.symbol().setColor(QColor(color))
                        rule.symbol().setSize(size)

                        if enhed == 'transparentstroke':
                            rule.symbol().symbolLayer(0).setStrokeStyle(Qt.PenStyle(Qt.NoPen))

                        root_rule.appendChild(rule)
                        try:
                            renderer.setOrderBy(QgsFeatureRequest.OrderBy([QgsFeatureRequest.OrderByClause(enhed, True)]))
                        except:
                            pass
                        renderer.setOrderByEnabled(True)
                        layer.setRenderer(renderer)
                        layer.triggerRepaint()

                    for rule in rules:
                        try:
                            rule_based_symbology(layer6, renderer, rule[0], rule[1], symbol, rule[2], rule[3], rule[4],
                                                 'nitrat')
                        except:pass
                    for rule in rules2:
                        try:

                            rule_based_symbology(layer7, renderer2, rule[0], rule[1], symbol2, rule[2], rule[3], rule[4],
                                             'sulfat')
                        except:pass

                    for rule in rules3:
                        try:
                            rule_based_symbology(layer8, renderer3, rule[0], rule[1], symbol3, rule[2], rule[3], rule[4],'Vandtype')
                        except:
                            pass

                    for rule in rules4:
                        try:
                            rule_based_symbology(layer9, renderer4, rule[0], rule[1], symbol4, rule[2], rule[3],
                                                 rule[4],
                                                 'chlorid')
                        except:
                            pass



                    for rule in rules5:
                        try:
                            rule_based_symbology(layer10, renderer5, rule[0], rule[1], symbol5, rule[2], rule[3],
                                                 rule[4],
                                                 'strontium')
                        except:
                            pass


                    for rule in rules6:
                        try:
                            rule_based_symbology(layer11, renderer6, rule[0], rule[1], symbol6, rule[2], rule[3],
                                                 rule[4],
                                                 'fluorid')
                        except:
                            pass



                    for rule in rules7:
                        try:
                            rule_based_symbology(layer12, renderer7, rule[0], rule[1], symbol7, rule[2], rule[3],
                                                 rule[4],
                                                 'status_indtag')
                        except:
                            pass


                    for rule in rules8:
                        try:
                            rule_based_symbology(layer13, renderer8, rule[0], rule[1], symbol8, rule[2], rule[3],
                                                 rule[4],
                                                 'status_indtag')
                        except:
                            pass
<<<<<<< HEAD

                    for rule in rules9:
                        try:
                            rule_based_symbology(layer14, renderer9, rule[0], rule[1], symbol9, rule[2], rule[3],
                                                 rule[4], 'arsen')

                        except:
                            pass

                    for rule in rules10:
                        try:
                            rule_based_symbology(layer15, renderer10, rule[0], rule[1], symbol10, rule[2], rule[3],
                                                 rule[4], 'jern')

                        except:
                            pass

                    for renderer_specific in [renderer,renderer2,renderer3,renderer4,renderer5,renderer6,renderer7,renderer8,renderer9,renderer10]:
                        try:
                            renderer_specific.rootRule().removeChildAt(0)
                        except:pass

                    for layer_specific in [layer6, layer7, layer8, layer9, layer10, layer11, layer12, layer13, layer14, layer15]:
                        try:
                            iface.layerTreeView().refreshLayerSymbology(layer_specific.id())
                        except:pass
=======

                    for rule in rules9:
                        try:
                            rule_based_symbology(layer14, renderer9, rule[0], rule[1], symbol9, rule[2], rule[3],
                                                 rule[4], 'arsen')

                        except:
                            pass

                    for rule in rules10:
                        try:
                            rule_based_symbology(layer15, renderer10, rule[0], rule[1], symbol10, rule[2], rule[3],
                                                 rule[4], 'jern')

                        except:
                            pass

                    for renderer_specific in [renderer,renderer2,renderer3,renderer4,renderer5,renderer6,renderer7,renderer8,renderer9,renderer10]:
                        try:
                            renderer_specific.rootRule().removeChildAt(0)
                        except:pass
                  #  renderer2.rootRule().removeChildAt(0)
                  #  renderer3.rootRule().removeChildAt(0)
                  #  renderer4.rootRule().removeChildAt(0)
                  #  renderer5.rootRule().removeChildAt(0)
                  #  renderer6.rootRule().removeChildAt(0)
                  #  renderer7.rootRule().removeChildAt(0)
                  #  renderer8.rootRule().removeChildAt(0)
                  #  renderer9.rootRule().removeChildAt(0)
                  #  renderer10.rootRule().removeChildAt(0)

                    iface.layerTreeView().refreshLayerSymbology(layer6.id())
                    iface.layerTreeView().refreshLayerSymbology(layer7.id())
                    iface.layerTreeView().refreshLayerSymbology(layer8.id())
                    iface.layerTreeView().refreshLayerSymbology(layer9.id())
                    iface.layerTreeView().refreshLayerSymbology(layer10.id())
                    iface.layerTreeView().refreshLayerSymbology(layer11.id())
                    iface.layerTreeView().refreshLayerSymbology(layer12.id())
                    iface.layerTreeView().refreshLayerSymbology(layer13.id())
                    iface.layerTreeView().refreshLayerSymbology(layer14.id())
                    iface.layerTreeView().refreshLayerSymbology(layer15.id())
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb

                symbolmag = QgsSymbol.defaultSymbol(maglayer.geometryType())
                renderermag = QgsRuleBasedRenderer(symbolmag)
                rulesmag = []

                for i, mag in enumerate(possiblemagnames):
                    rulesmag.append([f'{mag}', "", possibleshapes[i], '#000000', 2])

                color22 = QColor(255, 0, 0)  # Red color
                color22.setAlpha(0)
                rulesmag.append(['', "", QgsSimpleMarkerSymbolLayerBase.Circle, color22, 0])


                for rule in rulesmag:
                    rule_based_symbology(maglayer, renderermag, rule[0], rule[1], symbolmag, rule[2], rule[3], rule[4],'transparentstroke')


                renderermag.rootRule().removeChildAt(0)

                root2 = QgsProject.instance().layerTreeRoot()
                nodes = root2.children()

                for n in nodes:
                    if isinstance(n, QgsLayerTreeGroup):
                        if n.isExpanded() == True:
                            n.setExpanded(False)

                for layers in groupnitrat.children():
                    layers.setItemVisibilityChecked(False)
                for layers in groupsulfat.children():
                    layers.setItemVisibilityChecked(False)
                for layers in groupvandtype.children():
                    layers.setItemVisibilityChecked(False)
                for layers in groupklorid.children():
                    layers.setItemVisibilityChecked(False)
                for layers in groupstrontium.children():
                    layers.setItemVisibilityChecked(False)
                for layers in groupfluorid.children():
                    layers.setItemVisibilityChecked(False)
                for layers in groupstiltempaltes.children():
                    layers.setItemVisibilityChecked(False)
                for layers in grouppest.children():
                    layers.setItemVisibilityChecked(False)
                for layers in grouppfas.children():
                    layers.setItemVisibilityChecked(False)
                for layers in grouparsen.children():
                    layers.setItemVisibilityChecked(False)
                for layers in groupjern.children():
                    layers.setItemVisibilityChecked(False)
                composition = QgsPrintLayout(project)
                document = QDomDocument()

                # read template content
                path = str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/layout_template3.qpt"
                template_file = open(path)
                template_content = template_file.read()
                template_file.close()
                document.setContent(template_content)

                # load layout from template and add to Layout Manager
                composition.loadFromTemplate(document, QgsReadWriteContext())
                project.layoutManager().addLayout(composition)

                layout1 = QgsProject.instance().layoutManager().layoutByName("Projektområde - OSD")
                image = layout1.itemById("Logo")

                image.setPicturePath(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Images/miljoeministeriet_dk_rgb-ny.png")

                musthavelayouts = {}
                magasintildelingdf['Magasintildeling'] = magasintildelingdf['Magasintildeling'].astype('int')
                navne = []

                for l in project.mapLayers().values():
                    # if l.name() in navne:
                    QgsLegendRenderer.setNodeLegendStyle(
                        project.layerTreeRoot().findLayer(l.id()), QgsLegendStyle.Hidden)
                for z in range(antaltildelinger):
                    k = magasintildelingdf.loc[magasintildelingdf["Magasintildeling"] == z + 1]
                    col_list = k.Magasin.values.tolist()
                    k = ' '.join(col_list)
                    mylist2 = ["Seneste Nitrat - " + item for item in col_list]
                    mylist3 = ["Seneste Sulfat - " + item for item in col_list]
                    mylist4 = ["Seneste Vandtype - " + item for item in col_list]
                    mylist5 = ["Seneste Klorid - " + item for item in col_list]
                    mylist6 = ["Seneste Strontium - " + item for item in col_list]
                    mylist7 = ["Seneste Fluorid - " + item for item in col_list]
                    mylist8 = ["Seneste Pesticider - " + item for item in col_list]
                    mylist9 = ["Nitrat Tidsserier - " + item for item in col_list]
                    mylist10 = ["Sulfat Tidsserier - " + item for item in col_list]
                    mylist11 = ["Seneste PFAS - " + item for item in col_list]
                    mylist12 = ["Seneste Arsen - " + item for item in col_list]
                    mylist13 = ["Seneste Jern - " + item for item in col_list]

                    # mylist2 = mylist2 + mylist9
                    # mylist3 = mylist3 + mylist10

                    navne.append(mylist2)
                    navne.append(mylist3)
                    navne.append(mylist4)
                    navne.append(mylist5)
                    navne.append(mylist6)
                    navne.append(mylist7)
                    navne.append(mylist8)
                    navne.append(mylist9)
                    navne.append(mylist10)
                    navne.append(mylist11)
                    navne.append(mylist12)
                    navne.append(mylist13)

                    musthavelayouts["Kemi - Seneste Nitrat - {}".format(k)] = ["Område", "OSD", "SenesteNitratTemplate",
                                                                               "Udviklingstendens",
                                                                               "MagasinTemplate"] + mylist2 + mylist9
                    musthavelayouts["Kemi - Seneste Sulfat - {}".format(k)] = ["Område", "OSD", "SenesteSulfatTemplate",
                                                                               "Udviklingstendens",
                                                                               "MagasinTemplate"] + mylist3 + mylist10
                    musthavelayouts["Kemi - Seneste Vandtype - {}".format(k)] = ["Område", "OSD", "SenesteVandtypeTemplate",
                                                                                 "MagasinTemplate"] + mylist4
                    musthavelayouts["Kemi - Seneste Klorid - {}".format(k)] = ["Område", "OSD", "SenesteKloridTemplate",
                                                                               "MagasinTemplate"] + mylist5
                    musthavelayouts["Kemi - Seneste Strontium - {}".format(k)] = ["Område", "OSD",
                                                                                  "SenesteStrontiumTemplate",
                                                                                  "MagasinTemplate"] + mylist6
                    musthavelayouts["Kemi - Seneste Fluorid - {}".format(k)] = ["Område", "OSD", "SenesteFluoridTemplate",
                                                                                "MagasinTemplate"] + mylist7
                    musthavelayouts["Kemi - Seneste Pesticider - {}".format(k)] = ["Område", "OSD",
                                                                                   "SenestePesticiderTemplate",
                                                                                   "MagasinTemplate"] + mylist8
                    musthavelayouts["Kemi - Seneste PFAS - {}".format(k)] = ["Område", "OSD",
                                                                                   "SenestePFASTemplate",
                                                                                   "MagasinTemplate"] + mylist11
                    musthavelayouts["Kemi - Seneste Arsen - {}".format(k)] = ["Område", "OSD",
                                                                             "SenesteArsenTemplate",
                                                                             "MagasinTemplate"] + mylist12
                    musthavelayouts["Kemi - Seneste Jern - {}".format(k)] = ["Område", "OSD",
                                                                              "SenesteJernTemplate",
                                                                              "MagasinTemplate"] + mylist13

                all_col_list = magasintildelingdf.Magasin.values.tolist()
                allmagname = ' '.join(all_col_list)
                for x in ["Nitrat", "Sulfat", "Vandtype", "Klorid", "Strontium", "Fluorid", "Pesticider", "PFAS", "Arsen", "Jern"]:
                    if x == 'Nitrat' or x == 'Sulfat':
                        tidsserie = [f"{x} Tidsserier - " + item for item in all_col_list]
                    else:
                        tidsserie = []

                    nitratliste = [f"Seneste {x} - " + item for item in all_col_list]
                    musthavelayouts[f"Kemi - Seneste {x} - {allmagname}"] = ["Område", "OSD",
                                                                             f"Seneste{x}Template",
                                                                             "MagasinTemplate"] + nitratliste + tidsserie

                root = project.layerTreeRoot()
                manager = project.layoutManager()

                for key in musthavelayouts:
                    groupkemi = root.findGroup("Seneste Fluorid")
                    for layers in groupkemi.children():
                        layers.setItemVisibilityChecked(False)
                    groupkemi = root.findGroup("Seneste Pesticider")
                    for layers in groupkemi.children():
                        layers.setItemVisibilityChecked(False)
                    groupkemi = root.findGroup("Seneste PFAS")
                    for layers in groupkemi.children():
                        layers.setItemVisibilityChecked(False)
                    groupkemi = root.findGroup("Seneste Strontium")
                    for layers in groupkemi.children():
                        layers.setItemVisibilityChecked(False)
                    groupkemi = root.findGroup("Seneste Klorid")
                    for layers in groupkemi.children():
                        layers.setItemVisibilityChecked(False)
                    groupkemi = root.findGroup("Seneste Arsen")
                    for layers in groupkemi.children():
                        layers.setItemVisibilityChecked(False)
                    groupkemi = root.findGroup("Seneste Jern")
                    for layers in groupkemi.children():
                        layers.setItemVisibilityChecked(False)
                    groupkemi = root.findGroup("Seneste Vandtype")
                    for layers in groupkemi.children():
                        layers.setItemVisibilityChecked(False)
                    groupkemi = root.findGroup("Seneste Sulfat")
                    for layers in groupkemi.children():
                        layers.setItemVisibilityChecked(False)
                    groupkemi = root.findGroup("Seneste Nitrat")
                    for layers in groupkemi.children():
                        layers.setItemVisibilityChecked(False)
                    groupgenerelt = root.findGroup("Generelt")
                    for layers in groupgenerelt.children():
                        layers.setItemVisibilityChecked(False)
                    groupgenerelt2 = root.findGroup("Stilarter")
                    for layers in groupgenerelt2.children():
                        layers.setItemVisibilityChecked(False)

                    layout = manager.duplicateLayout(layout1, '{}'.format(key))

                    l = []
                    for count, values in enumerate(musthavelayouts.get(key)):


                        l.append(values)
                        try:
                            l[count] = project.mapLayersByName("{}".format(values))[0]
                            project.layerTreeRoot().findLayer(l[count].id()).setItemVisibilityChecked(True)
                        except:
                            pass

                    mapThemesCollection = project.mapThemeCollection()
                    mapThemes = mapThemesCollection.mapThemes()

                    mapThemeRecord = QgsMapThemeCollection.createThemeFromCurrentState(
                        QgsProject.instance().layerTreeRoot(),
                        iface.layerTreeView().layerTreeModel()
                    )
                    mapThemesCollection.insert("{}".format(key), mapThemeRecord)

                    layers_to_remove = []

                    for ll in navne:
                        for x in ll:
                            layers_to_remove.append(project.mapLayersByName("{}".format(x))[0])

                    layers_to_remove.append(project.mapLayersByName("{}".format(self.comboBox.currentText()))[0])

                    layout = project.layoutManager().layoutByName("{}".format(key))

                    legend = [i for i in layout.items() if isinstance(i, QgsLayoutItemLegend)][0]

                    map = [i for i in layout.items() if isinstance(i, QgsLayoutItemMap)][0]
                    map.setFollowVisibilityPreset(True)
                    map.setFollowVisibilityPresetName("{}".format(key))

                    if key.startswith("Kemi - Seneste Nitrat"):
                        legend.setTitle("Seneste Nitrat [mg/L]")
                        layers_to_remove.append(project.mapLayersByName("SenesteSulfatTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteVandtypeTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteKloridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteStrontiumTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteFluoridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePesticiderTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePFASTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteArsenTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteJernTemplate")[0])

                    if key.startswith("Kemi - Seneste Sulfat"):
                        legend.setTitle("Seneste Sulfat [mg/L]")
                        layers_to_remove.append(project.mapLayersByName("SenesteNitratTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteVandtypeTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteKloridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteStrontiumTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteFluoridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePesticiderTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePFASTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteArsenTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteJernTemplate")[0])

                    if key.startswith("Kemi - Seneste Vandtype"):
                        legend.setTitle("Seneste Vandtype")
                        layers_to_remove.append(project.mapLayersByName("SenesteNitratTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteSulfatTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteKloridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteStrontiumTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteFluoridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePesticiderTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("Udviklingstendens")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePFASTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteArsenTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteJernTemplate")[0])

                    if key.startswith("Kemi - Seneste Klorid"):
                        legend.setTitle("Seneste Klorid [mg/L]")
                        layers_to_remove.append(project.mapLayersByName("SenesteNitratTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteSulfatTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteVandtypeTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteStrontiumTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteFluoridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePesticiderTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("Udviklingstendens")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePFASTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteArsenTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteJernTemplate")[0])


                    if key.startswith("Kemi - Seneste Arsen"):
                        legend.setTitle("Seneste Arsen [μg/L]")
                        layers_to_remove.append(project.mapLayersByName("SenesteNitratTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteSulfatTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteVandtypeTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteStrontiumTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteFluoridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePesticiderTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteKloridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("Udviklingstendens")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePFASTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteJernTemplate")[0])

                    if key.startswith("Kemi - Seneste Strontium"):
                        legend.setTitle("Seneste Strontium *[μg/L]")
                        layers_to_remove.append(project.mapLayersByName("SenesteNitratTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteSulfatTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteVandtypeTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteKloridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteFluoridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePesticiderTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("Udviklingstendens")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePFASTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteArsenTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteJernTemplate")[0])

                    if key.startswith("Kemi - Seneste Fluorid"):
                        legend.setTitle("Seneste Fluorid *[mg/L]")
                        layers_to_remove.append(project.mapLayersByName("SenesteNitratTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteSulfatTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteVandtypeTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteKloridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteStrontiumTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePesticiderTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("Udviklingstendens")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePFASTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteArsenTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteJernTemplate")[0])

                    if key.startswith("Kemi - Seneste Pesticider"):
                        legend.setTitle("Seneste Pesticider")
                        layers_to_remove.append(project.mapLayersByName("SenesteNitratTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteSulfatTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteVandtypeTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteKloridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteStrontiumTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteFluoridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("Udviklingstendens")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePFASTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteArsenTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteJernTemplate")[0])


                    if key.startswith("Kemi - Seneste PFAS"):
                        legend.setTitle("Seneste PFAS")
                        layers_to_remove.append(project.mapLayersByName("SenesteNitratTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteSulfatTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteVandtypeTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteKloridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteStrontiumTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteFluoridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePesticiderTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("Udviklingstendens")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteArsenTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteJernTemplate")[0])

                    if key.startswith("Kemi - Seneste Jern"):
                        legend.setTitle("Seneste Jern *[mg/L]")
                        layers_to_remove.append(project.mapLayersByName("SenesteNitratTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteSulfatTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteVandtypeTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteKloridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteStrontiumTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteFluoridTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePesticiderTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("Udviklingstendens")[0])
                        layers_to_remove.append(project.mapLayersByName("SenesteArsenTemplate")[0])
                        layers_to_remove.append(project.mapLayersByName("SenestePFASTemplate")[0])

                    legend.setAutoUpdateModel(True)
                    legend.setAutoUpdateModel(False)
                    legend.setLegendFilterByMapEnabled(False)

                    # REMOVE LAYERS
                    for l in layers_to_remove:
                        legend.model().rootGroup().removeLayer(l)
                        root_group = legend.model().rootGroup()  # QgsLayerTree object
                        group2 = root_group.findGroup("Seneste Nitrat")  # get reference to group (QgsLayerTreeGroup object)
                        group2.removeLayer(l)
                        group3 = root_group.findGroup("Seneste Sulfat")  # get reference to group (QgsLayerTreeGroup object)
                        group3.removeLayer(l)
                        group4 = root_group.findGroup(
                            "Seneste Vandtype")  # get reference to group (QgsLayerTreeGroup object)
                        group4.removeLayer(l)
                        group5 = root_group.findGroup("Seneste Klorid")  # get reference to group (QgsLayerTreeGroup object)
                        group5.removeLayer(l)
                        group6 = root_group.findGroup(
                            "Seneste Strontium")  # get reference to group (QgsLayerTreeGroup object)
                        group6.removeLayer(l)
                        group7 = root_group.findGroup(
                            "Seneste Fluorid")  # get reference to group (QgsLayerTreeGroup object)
                        group7.removeLayer(l)
                        group8 = root_group.findGroup("Stilarter")  # get reference to group (QgsLayerTreeGroup object)
                        group8.removeLayer(l)
                        group9 = root_group.findGroup(
                            "Seneste Pesticider")  # get reference to group (QgsLayerTreeGroup object)
                        group9.removeLayer(l)
                        group10 = root_group.findGroup("Seneste PFAS")  # get reference to group (QgsLayerTreeGroup object)
                        group10.removeLayer(l)
                        group11 = root_group.findGroup("Seneste Arsen")  # get reference to group (QgsLayerTreeGroup object)
                        group11.removeLayer(l)

                        group12 = root_group.findGroup("Seneste Jern")  # get reference to group (QgsLayerTreeGroup object)
                        group12.removeLayer(l)

                    # HIDE LAYERS
                    for l in project.mapLayers().values():
                        # if l.name() in navne:
                        QgsLegendRenderer.setNodeLegendStyle(
                            project.layerTreeRoot().findLayer(l.id()), QgsLegendStyle.Hidden)

                    hideable = ["SenesteNitratTemplate", "SenesteSulfatTemplate", "SenesteVandtypeTemplate",
                                "SenesteKloridTemplate", "SenestePFASTemplate", "SenesteArsenTemplate",
                                "SenesteStrontiumTemplate", "SenesteFluoridTemplate", "SenestePesticiderTemplate", "SenesteJernTemplate",
                                "MagasinTemplate"]

                    for l in project.mapLayers().values():
                        if l.name() in hideable:
                            QgsLegendRenderer.setNodeLegendStyle(
                                project.layerTreeRoot().findLayer(l.id()), QgsLegendStyle.Hidden)
                            layout.refresh()

                    for count, l in enumerate(legend.model().rootGroup().children()):
                        QgsLegendRenderer.setNodeLegendStyle(legend.model().rootGroup().children()[count],
                                                             QgsLegendStyle.Hidden)

                    if key.startswith("Kemi - Seneste Nitrat") or key.startswith("Kemi - Seneste Sulfat"):
                        udviktendens = project.mapLayersByName('Udviklingstendens')[0]
                        layer_tree_layer = legend.model().rootGroup().findLayer(udviktendens)
                        layer_tree_layer.setUseLayerName(False)
                        layer_tree_layer.setName('Udviklingstendenser:')

                        QgsLegendRenderer.setNodeLegendStyle(layer_tree_layer, QgsLegendStyle.Group)


                    magasintemplate = project.mapLayersByName('MagasinTemplate')[0]
                    magasin_tree_layer = legend.model().rootGroup().findLayer(magasintemplate)
                    QgsLegendRenderer.setNodeLegendStyle(magasin_tree_layer, QgsLegendStyle.Group)
                    magasin_tree_layer.setUseLayerName(False)
                    magasin_tree_layer.setName('Magasiner:')


                    legend = [i for i in layout.items() if isinstance(i, QgsLayoutItemLegend)][0]

                    index = []

                    for i, row in magasintildelingdf.iterrows():
                        if row['Magasin'] in layout.name():
                            index.append(i)


                    index.append(int(len(rulesmag)-1))

                    def set_visible_legend_nodes(layer, layout, indexes):
                        # Get the legend item from layout
                        model = legend.model()
                        # Get the layer node linked to input layer
                        layerNode = model.rootGroup().findLayer(layer)
                        # Set the legend node order
                        QgsMapLayerLegendUtils.setLegendNodeOrder(layerNode, indexes)
                        # Refresh the legend
                        model.refreshLayerLegend(layerNode)

                    set_visible_legend_nodes(magasintemplate, layout, index)

                    legend.updateLegend()  # Update the QgsLayerTreeModel

                    layout.refresh()


                """
                # 1
                df_pesticider_tabel = dfpest2["dominerende_stof"].value_counts().rename_axis('Dominerende stof i seneste analyse').to_frame('Antal')
                df_pesticider_tabel.sort_values(by=['Antal'], ascending=False)

                try:
                    df_pesticider_tabel.drop("NULL")
                except:
                    pass

                # 2
                def fetch_pest(dgunummer):

                    sql_wl = f'''                      
                            WITH pest AS (
                                    SELECT 
                                            row_number() over (order by gcs.boreholeno) as id,
                                            gcs.boreholeno,
                                            gcs.intakeno,
                                            gca.sampleid,
                                            gcs.sampledate,
                                            cpl.long_text compoundname,
                                            gca."attribute" attrib,
                                            gca.amount,
                                            gca.compoundno,
                                            cpl.casno,
                                            b.abandondat,
                                            b.geom,
                                            gcs.project
                                        from jupiter.grwchemanalysis gca
                                        inner join jupiter.grwchemsample gcs using (sampleid)
                                        inner join jupiter.compoundlist cpl using (compoundno)
                                        inner join (
                                            select compoundno
                                            from jupiter.compoundgroup where compoundgroupno = 50) pesticid_compound on pesticid_compound.compoundno = gca.compoundno
                                        inner join jupiter.borehole b using (boreholeid)
                                        WHERE concat(REPLACE(gcs.boreholeno, ' ', ''), '_', gcs.intakeno) IN ('{dgunummer}')
                                        AND gca."attribute" IS NULL
                                        order by gcs.boreholeno, gcs.sampledate desc, gca.amount
                                        )
                                    
                                    SELECT distinct on (boreholeno, intakeno, compoundname) *
                                    FROM pest
                                    '''

                    db = Database(database='JUPITER')
                    df_wl2 = db.sql_to_df(sql_wl)

                    return df_wl2

                df_pesticider_oversigt = \
                dfpest2[["dgu2", "boringsanvendelse", "dato_seneste_analyse", "indtag_top_dybde", "magasin"]][
                    dfpest2['status_indtag'] > 2.5].drop_duplicates()
                df_pesticider_oversigt["Pesticider og koncentration (µg/L)"] = None
                df_pesticider_oversigt['dato_seneste_analyse'] = pd.DatetimeIndex(
                    df_pesticider_oversigt['dato_seneste_analyse']).year

                for i, row in df_pesticider_oversigt.iterrows():
                    z = fetch_pest(row["dgu2"]).sort_values(by=['amount'], ascending=False)
                    pestnames = z['compoundname'].tolist()
                    pestamount = z['amount'].tolist()
                    pest = [m + ": " + str(n) + ' \n' for m, n in zip(pestnames, pestamount)]
                    df_pesticider_oversigt.at[i, "Pesticider og koncentration (µg/L)"] = ' '.join(pest)
                df_pesticider_oversigt = df_pesticider_oversigt.set_index("dgu2")
                df_pesticider_oversigt.sort_values(by=['magasin'], ascending=True)

                df_pesticider_oversigt.rename(columns={'dgu2': 'DGU Indtag', 'dato_seneste_analyse': 'Prøvetagningsår',
                                                       'indtag_top_dybde': 'Indtag top dybde','magasin': 'Magasin'}, inplace=True)
                lokal = os.path.dirname(os.path.realpath(str(self.lineEdit.text()))).replace(os.sep, '/')
                """

                """
                #3 ##FIX##
                statistik_nitrat_data = [[nitratseneste.shape[0]], [nitratseneste[(nitratseneste['nitrat'] > 1) & (nitratseneste['nitrat'] <= 50)].shape[0]],
                                         [nitratseneste[nitratseneste['nitrat'] > 50].shape[0]], max(nitratseneste['nitrat']), nitratseneste['nitrat'].mean(), '50 mg/L']

                statistik_nitrat_df = pd.DataFrame(statistik_nitrat_data, columns=['Antal analyser', 'Antal analyser hvor nitrat er 1-50 mg/L',
                                                                  'Antal analyser hvor nitrat er >50 mg/L', 'Max (mg/L)', 'Gennemsnit (mg/L)', 'Kravværdi (BEK nr 1110 af 30/05/2021)'])

                """



                #writer = pd.ExcelWriter("{}/TabellerKemiskKortlægning.xlsx".format(lokal))

                # Write each dataframe to a different worksheet.
                #df_pesticider_tabel.to_excel(writer, sheet_name="PesticiderTabelDominerendeStof")
                #df_pesticider_oversigt.to_excel(writer, sheet_name="PesticidPåvistFund")
                #statistik_nitrat_df.to_excel(writer, sheet_name="NitratStatistik")

                # Close the Pandas Excel writer and output the Excel file.
               # writer.close()

                if self.checkBox_12.isChecked():
                    for x in ["nitrat", "sulfat", "chlorid"]:
                        # try:
                        if x == "nitrat":
                            mk = DybdeplotNitratSulfat(geom.asWkt(), crstrue, x, lokal + "/Figurer", nitratseneste, 1, 20)
                            mk.plot_dybdeplot()
                        if x == "sulfat":
                            mk = DybdeplotNitratSulfat(geom.asWkt(), crstrue, x, lokal + "/Figurer", sulfatseneste, 1, 20)
                            mk.plot_dybdeplot()
                        if x == "chlorid":
                            mk = DybdeplotNitratSulfat(geom.asWkt(), crstrue, x, lokal + "/Figurer", kloridseneste, 1, 20)
                            mk.plot_dybdeplot()
                    try:
                        mk = ScatterSulfatNitrat(geom.asWkt(), crstrue, lokal + "/Figurer", "SulfatNitrat", nitratseneste)
                        mk.plot_sulfatnitrat()
                    except:
                        pass
                    try:
                        mk2 = DybdePlotVandType(geom.asWkt(), crstrue, lokal + "/Figurer", "Vandtype", vandtypeseneste)
                        mk2.plot_dybdeplot()
                    except:
                        pass
                    try:
                        mk3 = SulfatKlorid(geom.asWkt(), crstrue, lokal + "/Figurer", "SulfatChlorid", sulfatseneste)
                        mk3.plot_sulfatklorid()
                    except:
                        pass
                    try:
                        mk4 = DybdePesticider(geom.asWkt(), crstrue, lokal + "/Figurer", "DybdeplotPesticider", dfpest2)
                        mk4.plot_dybdeplot()
                    except:
                        pass
                else:
                    pass

                self.pushButton_fifth.setText("Færdig ✓")
                self.repaint()
                QMessageBox.information(self, 'OBS',
                                        "Udviklingstendenser er udregnet vha. en lidt fejlbehæftet algoritme, så det anbefales at tjekke udviklingstendenserne igennem.")

        else:
            QMessageBox.warning(None, 'Fejl', "Vælg dit Excel-ark med kemiudtræk (step 1).")

    else:
        QMessageBox.warning(None, 'Fejl', "Intet input (områdepolygon).")


def kemi_three(self):
    if len(self.lineEdit.text()) > 0:
        manager = QgsProject.instance().layoutManager()
        project = QgsProject.instance()

        if len(manager.printLayouts()) > 0:
            if True:
                self.pushButton_sixth.setText("Kører")
                self.pushButton_sixth.setEnabled(False)
                self.repaint()
            lokal = os.path.dirname(os.path.realpath(str(self.lineEdit.text()))).replace(os.sep, '/')

            try:
                os.makedirs("{}/Figurer".format(lokal))
            except:
                pass
            global imagePath

            imagePath = lokal + "/Figurer"
            dpi = 175  # self.dpibox.value()




            class FigureWorker(QgsTask):
                def __init__(self, Dialog):
                    QgsTask.__init__(self)
                    self.Dialog = Dialog

                def run(self):


                    for layout in manager.printLayouts():

                        legend = [i for i in layout.items() if isinstance(i, QgsLayoutItemLegend)][0]
                        legend.setLegendFilterByMapEnabled(True)
                        legend.setLegendFilterByMapEnabled(False)
                        layout.refresh()
                        exporter = QgsLayoutExporter(layout)
                        settings = exporter.ImageExportSettings()
                        settings.dpi = dpi
                        exporter.exportToImage(
                            '{}/{}.png'.format(imagePath, layout.name()), settings)
                    return True

                def finished(self, result):
                    try:
                        self.result_layer.moveToThread(QCoreApplication.instance().thread())
                        project.addMapLayer(self.result_layer)
                    except:
                        pass

                    self.Dialog.pushButton_sixth.setText("Færdig ✓")
                    self.Dialog.repaint()
                    QApplication.processEvents()
                    try:
                        os.remove(str(imagePath) + '/Projektområde - OSD.png')
                    except:
                        pass
                    os.startfile(str(imagePath))

            globals()['taskfiguremaker2'] = FigureWorker(self)
            QgsApplication.taskManager().addTask(globals()['taskfiguremaker2'])
        else:
            QMessageBox.warning(None, 'Fejl', "Ingen figurer i dette projekt.")
    else:
        QMessageBox.warning(None, 'Fejl', "Kør step 1 og 2 først.")
