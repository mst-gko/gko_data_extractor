# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import (
    QgsFields,
    QgsField,
    QgsFeature,
QgsLayerTreeGroup,
    QgsGeometry,
    QgsFeatureRequest,
    QgsProject,
QgsSimpleMarkerSymbolLayerBase,
    QgsRaster,
    QgsTask,
    QgsPrintLayout,
    QgsReadWriteContext,
    QgsRasterFileWriter,
    QgsMapLayer,
    QgsLayoutItemLegend,
    QgsLayoutItemMap,
    QgsLegendRenderer,
    QgsExpression,
    QgsLegendStyle,
    QgsLayoutExporter,
    QgsLayoutItemPicture,
    QgsLayoutPoint,
    QgsLayoutSize,
    QgsUnitTypes,
    QgsRasterBandStats,
    QgsCategorizedSymbolRenderer,
    QgsRendererCategory,
    QgsSymbol,
    QgsCoordinateTransformContext,
QgsRuleBasedRenderer,
    QgsRandomColorRamp,
    QgsMapThemeCollection,
    QgsLayerDefinition,
    QgsPointXY,
    QgsApplication,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsExpressionContextScope,
    QgsCoordinateReferenceSystem,
    QgsProcessingFeatureSourceDefinition,
    QgsRectangle,
    QgsTaskManager,
    QgsLayout,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsRasterLayer,
    QgsWkbTypes,
    QgsDataSourceUri
)
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()
import pyodbc

from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass


__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *

def opstart_first(self):
    if len(self.comboBox.currentText()) > 0:
        if self.lokalefiler.checkState() == Qt.Checked and len(self.lokalefilersti.text()) > 0:
            lokal = str(self.lokalefilersti.text())

            self.pushButton_first.setText("Kører")
            self.pushButton_first.setEnabled(False)
            self.repaint()
            project = QgsProject.instance()
            key = self.comboBox.currentText()
            områdelayer = project.mapLayersByName('{}'.format(key))[0]

            for kk, feature in enumerate(områdelayer.getFeatures()):
                if kk == 0:
                    geom = feature.geometry()
                else:
                    geom = geom.combine(feature.geometry())

            nodeområdelayer = project.layerTreeRoot().findLayer(områdelayer.id())
            if nodeområdelayer:
                nodeområdelayer.setItemVisibilityChecked(False)

            root = project.layerTreeRoot()


            groupName = "Generelt"
            root = project.layerTreeRoot()
            group = root.insertGroup(0, groupName)


            groupstiltempaltes = root.insertGroup(0, "Stilarter")

            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteKloridTemplate.qlr", project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteVandtypeTemplate.qlr",
                project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteSulfatTemplate.qlr", project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenestePesticiderTemplate.qlr",
                project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteArsenTemplate.qlr", project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteNitratTemplate.qlr", project,
                groupstiltempaltes)

            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteJernTemplate.qlr", project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteAnalyseAlder.qlr", project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/StandardMagasinTemplate.qlr",
                project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/StandardMagasinTemplate2.qlr",
                project,
                groupstiltempaltes)


            memlay = områdelayer.clone()
            try:
                os.makedirs("{}/01_Generelt".format(lokal))
            except:
                pass

            _writer = QgsVectorFileWriter.writeAsVectorFormat(memlay, str(lokal) + '/01_Generelt/' + 'Område.shp',
                                                              'Område', memlay.crs(), 'ESRI Shapefile')
            memlay = QgsVectorLayer(str(lokal) + '/01_Generelt/' + 'Område.shp', 'Område', 'ogr')
            memlay.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Projektområde.qml")

            memlay.setName('Område')

            mem_layer = QgsVectorLayer("Polygon?crs=epsg:25832", "Name", "memory")

            coords = [(350971.60, 6432166.961), (967593.96466, 6430251.984), (959934.059, 5986934.977),
                      (213093.313, 6016617.109)]
            polygon = QgsGeometry.fromPolygonXY([[QgsPointXY(pair[0], pair[1]) for pair in coords]])

            feature = QgsFeature()
            feature.setGeometry(polygon)
            mem_layer.dataProvider().addFeatures([feature])
            mem_layer.updateFields()

            a = processing.run("native:symmetricaldifference",
                               {'INPUT': mem_layer,
                                'OVERLAY': områdelayer,
                                'OVERLAY_FIELDS_PREFIX': '', 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            _writer = QgsVectorFileWriter.writeAsVectorFormat(a, str(lokal) + '/01_Generelt/' + 'Område-grå.shp',
                                                              'Område', a.crs(), 'ESRI Shapefile')
            a = QgsVectorLayer(str(lokal) + '/01_Generelt/' + 'Område-grå.shp', 'Område-grå', 'ogr')

            a.setName("Område-grå")
            a.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Projektområde_grå.qml")
            ##############
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Administrative grænser/01~Kommunegrænser.qlr", project,
                group)
            kommunegrænse = project.mapLayersByName('kommunegraense')[0]
            kom = processing.run("native:extractbyexpression",
                                 {'INPUT': kommunegrænse,
                                  'EXPRESSION': 'komnavn is not null', 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]
            _writer = QgsVectorFileWriter.writeAsVectorFormat(layer=kom,fileName=str(lokal) + '/01_Generelt/' + 'Kommunegrænser.shp', fileEncoding="latin1",
                                                              destCRS= kom.crs(), driverName='ESRI Shapefile')

            kom = QgsVectorLayer(str(lokal) + '/01_Generelt/' + 'Kommunegrænser.shp', 'Kommunegrænser', 'ogr')

            kom.setName("Kommunegrænser")
            kom.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Kommunegrænse.qml")
            # Anlæg
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/42~Anlæg_aktive.qlr", project, group)
            anlægaktive = project.mapLayersByName('Anlæg_aktiv')[0]

            aa = processing.run("native:clip",
                                {'INPUT': anlægaktive,
                                 'OVERLAY': områdelayer,
                                 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            _writer = QgsVectorFileWriter.writeAsVectorFormat(aa, str(lokal) + '/01_Generelt/' + 'Anlæg Aktive',
                                                              'Anlæg Aktive', aa.crs())
            aa = QgsVectorLayer(str(lokal) + '/01_Generelt/' + 'Anlæg Aktive.gpkg', 'Anlæg Aktive', 'ogr')
            aa.setName("Anlæg Aktive")
            # aa.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Anlæg Aktive - andet.qml")

            aa.startEditing()
            aa.dataProvider().addAttributes([QgsField('AnlNavn', QVariant.String)])
            aa.updateFields()
            idx = aa.dataProvider().fieldNameIndex('AnlNavn')

            features = [feat for feat in aa.getFeatures()]

            context = QgsExpressionContext()
            scope = QgsExpressionContextScope()
            context.appendScope(scope)
            listOfResults = []

            for f in aa.getFeatures():
                context.setFeature(f)
                exp = QgsExpression(
                    "CASE WHEN virksomhedstype IN ('Gartneri', 'Anden erhvervsvirksomhed', 'Grusvask', 'Hotel, Campingplads o.lign.', 'Husdyrfarm', 'Husholdning, én husstand', 'Husholdninger, flere hustande', 'Levnedmiddelindustri', 'Markvanding', 'Mælkeleverandør', 'Sportsplads, park o.lign') THEN wordwrap(virksomhedstype, 20) ELSE NULL END")
                f['AnlNavn'] = exp.evaluate(context)
                aa.updateFeature(f)

            aa.commitChanges()

            expression = f'"AnlNavn" is NULL'
            aa.selectByExpression(expression)

            # delete selected features
            if aa.selectedFeatureCount() > 0:
                aa.startEditing()
                aa.deleteSelectedFeatures()
                aa.commitChanges()

            unique_values = aa.uniqueValues(aa.fields().indexFromName("AnlNavn"))

            category_list = []
            for value in unique_values:
                symbol = QgsSymbol.defaultSymbol(aa.geometryType())
                category = QgsRendererCategory(value, symbol, str(value))
                category_list.append(category)

            renderer = QgsCategorizedSymbolRenderer('AnlNavn', category_list)
            renderer.updateColorRamp(QgsRandomColorRamp())
            aa.setRenderer(renderer)
            aa.triggerRepaint()




            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/40~Anlæg.qlr",
                project, group)
            anlæg = project.mapLayersByName('Anlæg')[0]

            vvatemp = processing.run("native:clip",
                                     {'INPUT': anlæg,
                                      'OVERLAY': områdelayer,
                                      'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            vva = processing.run("native:extractbyexpression", {
                'INPUT': vvatemp,
                'EXPRESSION': ' "aktiv" = \'Aktiv\'', 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            _writer = QgsVectorFileWriter.writeAsVectorFormat(vva,
                                                              str(lokal) + '/01_Generelt/' + 'Vandværker Aktive',
                                                              'Vandværker Aktive', vva.crs())
            vva = QgsVectorLayer(str(lokal) + '/01_Generelt/' + 'Vandværker Aktive.gpkg', 'Vandværker Aktive',
                                 'ogr')

            vva.setName("Vandværker Aktive")

            vva.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Anlæg Aktive - Vandværker.qml")
            project.addMapLayer(memlay, False)  # False is the key
            group.addLayer(memlay)

            project.addMapLayer(a, False)
            group.addLayer(a)

            project.addMapLayer(kom, False)
            group.addLayer(kom)
            project.removeMapLayer(kommunegrænse)
            project.addMapLayer(vvatemp, False)
            group.addLayer(vvatemp)
            project.addMapLayer(aa, False)
            group.addLayer(aa)
            project.addMapLayer(vva, False)
            group.addLayer(vva)
            project.removeMapLayer(anlægaktive)
            project.removeMapLayer(anlæg)
            project.removeMapLayer(vvatemp)
            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/20~Vandforsyningsboring.qlr", project, group)
            indvindingsboringer = project.mapLayersByName('Vandforsyningsboring')[0]
            ind = processing.run("native:clip",
                                 {'INPUT': indvindingsboringer,
                                  'OVERLAY': områdelayer,
                                  'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            _writer = QgsVectorFileWriter.writeAsVectorFormat(ind,
                                                              str(lokal) + '/01_Generelt/' + 'Indvindingsboringer',
                                                              'Indvindingsboringer', ind.crs())
            ind = QgsVectorLayer(str(lokal) + '/01_Generelt/' + 'Indvindingsboringer.gpkg', 'Indvindingsboringer',
                                 'ogr')
            ind.setName("Indvindingsboringer")

            ind.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Indvindingsboringer.qml")
            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Geologi og jordbund/01~FOHM/02~FOHM - Lagtildeling filter - Alle lag.qlr",
                project, group)
            fohm = project.mapLayersByName('FOHM - Lagtildeling filter - Alle lag')[0]
            lagdelingvfilter = processing.run("native:clip",
                                              {'INPUT': fohm,
                                               'OVERLAY': områdelayer,
                                               'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            _writer = QgsVectorFileWriter.writeAsVectorFormat(lagdelingvfilter,
                                                              str(lokal) + '/01_Generelt/' + 'Filtersætning i FOHM',
                                                              'Filtersætning i FOHM', lagdelingvfilter.crs())

            lagdelingvfilter = QgsVectorLayer(str(lokal) + '/01_Generelt/' + 'Filtersætning i FOHM.gpkg',
                                              'Filtersætning i FOHM', 'ogr')

            lagdelingvfilter.setName("Filtersætning i FOHM")

            lagdelingvfilter.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - FOHM.qml")
            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/GKO Afgrænsninger alle/01~Alle indvindingsoplande.qlr",
                project, group)
            indvindingsoplande = \
                project.mapLayersByName('Alle IOL (både inden- og udenfor OSD) - Gældende og historiske')[
                    0]

            IOL = processing.run("native:joinattributestable", {
                'INPUT': indvindingsoplande,
                'FIELD': 'anl_id',
                'INPUT_2': ind,
                'FIELD_2': 'plantid', 'FIELDS_TO_COPY': [], 'METHOD': 1, 'DISCARD_NONMATCHING': True, 'PREFIX': '',
                'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            #  IOL = processing.run("native:extractbylocation",
            #                      {'INPUT': indvindingsoplande,
            #                       'PREDICATE': [6],
            #                       'INTERSECT': områdelayer,
            #                       'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            # IOL = processing.run("native:clip",
            #                     {'INPUT': indvindingsoplande,
            #                      'OVERLAY': områdelayer,
            #                      'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']


            _writer = QgsVectorFileWriter.writeAsVectorFormat(IOL,
                                                              str(lokal) + '/01_Generelt/' + 'Indvindingsoplande',
                                                              'latin0', IOL.crs())
            IOL = QgsVectorLayer(str(lokal) + '/01_Generelt/' + 'Indvindingsoplande.gpkg', 'ogr')

            IOL.setName("Indvindingsoplande")
            IOL.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Indvindingsoplande.qml")

            IOLGroup = IOL.clone()
            _writer = QgsVectorFileWriter.writeAsVectorFormat(IOLGroup,
                                                              str(lokal) + '/01_Generelt/' + 'Indvindingsoplande Grupperet', 'latin0',
                                                              IOLGroup.crs())
            IOLGroup = QgsVectorLayer(str(lokal) + '/01_Generelt/' + 'Indvindingsoplande Grupperet.gpkg', 'ogr')

            IOLGroup.setName("Indvindingsoplande Grupperet")
            ########

            IOLGroup.startEditing()
            IOLGroup.dataProvider().addAttributes([QgsField('Modelnavn', QVariant.String)])
            IOLGroup.updateFields()
            idx = IOLGroup.dataProvider().fieldNameIndex('Modelnavn')

            features = [feat for feat in IOLGroup.getFeatures()]

            context = QgsExpressionContext()
            scope = QgsExpressionContextScope()
            context.appendScope(scope)
            listOfResults = []

            for f in IOLGroup.getFeatures():
                context.setFeature(f)
                exp = QgsExpression("wordwrap(kortnavn, 20)")
                f['Modelnavn'] = exp.evaluate(context)
                IOLGroup.updateFeature(f)

            IOLGroup.commitChanges()

            unique_values = IOLGroup.uniqueValues(IOLGroup.fields().indexFromName("Modelnavn"))

            category_list = []
            for value in unique_values:
                symbol = QgsSymbol.defaultSymbol(IOLGroup.geometryType())
                symbol.setOpacity(0.7)
                category = QgsRendererCategory(value, symbol, str(value))
                category_list.append(category)

            renderer = QgsCategorizedSymbolRenderer('Modelnavn', category_list)
            renderer.updateColorRamp(QgsRandomColorRamp())
            IOLGroup.setRenderer(renderer)
            IOLGroup.triggerRepaint()
            project.addMapLayer(ind, False)
            group.addLayer(ind)
            project.removeMapLayer(indvindingsboringer)
            project.addMapLayer(lagdelingvfilter, False)
            group.addLayer(lagdelingvfilter)
            project.removeMapLayer(fohm)
            project.addMapLayer(IOL, False)
            group.addLayer(IOL)

            project.addMapLayer(IOLGroup, False)
            group.addLayer(IOLGroup)
            project.removeMapLayer(indvindingsoplande)
            ## OSD
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/GKO Afgrænsninger alle/06~Alle DI.qlr", project,
                group)
            områdesærligedrikke = project.mapLayersByName('di - Gældende og historiske')[0]
            OSD = processing.run("native:clip",
                                 {'INPUT': områdesærligedrikke,
                                  'OVERLAY': områdelayer,
                                  'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            _writer = QgsVectorFileWriter.writeAsVectorFormat(OSD,
                                                              str(lokal) + '/01_Generelt/' + 'OSD.shp',
                                                              'OSD', OSD.crs(),
                                                              'ESRI Shapefile')
            OSD = QgsVectorLayer(str(lokal) + '/01_Generelt/' + 'OSD.shp',
                                 'OSD', 'ogr')

            OSD.setName("OSD")
            OSD.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - OSD.qml")
            ####F:\GKO\data\grukos\kortskabe\QGIS\Kortskab\Geologi og jordbund

            uri = "F:/Nordjylland/Medarbejdere/MAKYN/Ongoing/Automatiseret-Opstartsrapport/QGIS/Lag/PSmed.tif"
            rlayer = QgsRasterLayer(uri, "Per Smed")
            ###
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Geologi og jordbund/10~Begravededale/01~Begravede dale.qlr",
                project,
                group)
            begravedeledale = project.mapLayersByName('begravede_dale')[0]
            dale = processing.run("native:extractbyexpression",
                                  {'INPUT': begravedeledale,
                                   'EXPRESSION': 'fid is not null', 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

            _writer = QgsVectorFileWriter.writeAsVectorFormat(dale,
                                                              str(lokal) + '/01_Generelt/' + 'Begravede dale.shp',
                                                              'Begravede dale', dale.crs(),
                                                              'ESRI Shapefile')
            dale = QgsVectorLayer(str(lokal) + '/01_Generelt/' + 'Begravede dale.shp',
                                  'Begravede dale', 'ogr')

            dale.setName("Begravede Dale")
            dale.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Begravede dale.qml")

            ###
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Geologi og jordbund/40~Jordartskort/12~Jordartskort 1 til 200.000 - GEUS WMS.qlr",
                project,
                group)
            jordartskort = project.mapLayersByName('Jordartskort 1:200.000')[0]

            jordart = jordartskort.clone()

            jordart.setName("Jordartskort")
            project.addMapLayer(OSD, False)
            group.addLayer(OSD)
            project.removeMapLayer(områdesærligedrikke)
            project.addMapLayer(rlayer, False)
            group.addLayer(rlayer)
            project.addMapLayer(dale, False)
            group.addLayer(dale)
            project.removeMapLayer(begravedeledale)

            project.addMapLayer(jordart, False)
            group.addLayer(jordart)
            project.removeMapLayer(jordartskort)

            for layers in group.children():
                layers.setItemVisibilityChecked(False)

            ###############################
            groupName2 = "Geologi"
            groupgeo = root.insertGroup(0, groupName2)

            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Geologi og jordbund/20~Datakvalitet/01~Boringer/01~Boringskvalitet - Lithologi+Position.qlr",
                project,
                group)
            boringskvalitet = project.mapLayersByName('Boringskvalitet - Lithologi+Position')[0]

            bk = processing.run("native:clip",
                                {'INPUT': boringskvalitet,
                                 'OVERLAY': områdelayer,
                                 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            try:
                os.makedirs("{}/02_Geologi".format(lokal))
            except:
                pass

            bk13 = processing.run("native:extractbyexpression",
                                  {'INPUT': bk,
                                   'EXPRESSION': 'quality_litho_position < 3.5', 'OUTPUT': 'TEMPORARY_OUTPUT'})[
                "OUTPUT"]

            _writer = QgsVectorFileWriter.writeAsVectorFormat(bk13,
                                                              str(lokal) + '/02_Geologi/' + 'Boringskvalitet 1-3',
                                                              'Boringskvalitet 1-3', bk13.crs())
            bk13 = QgsVectorLayer(str(lokal) + '/02_Geologi/' + 'Boringskvalitet 1-3.gpkg',
                                  'Boringskvalitet 1-3', 'ogr')

            bk46 = processing.run("native:extractbyexpression",
                                  {'INPUT': bk,
                                   'EXPRESSION': 'quality_litho_position > 3.5', 'OUTPUT': 'TEMPORARY_OUTPUT'})[
                "OUTPUT"]

            _writer = QgsVectorFileWriter.writeAsVectorFormat(bk46,
                                                              str(lokal) + '/02_Geologi/' + 'Boringskvalitet 4-6',
                                                              'Boringskvalitet 4-6', bk46.crs())
            bk46 = QgsVectorLayer(str(lokal) + '/02_Geologi/' + 'Boringskvalitet 4-6.gpkg',
                                  'Boringskvalitet 4-6', 'ogr')

            bk13.setName("Boringskvalitet 1-3")
            bk46.setName("Boringskvalitet 4-6")

            bk13.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Geologi - Boringskvalitet.qml")
            bk46.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Geologi - Boringskvalitet.qml")

            project.addMapLayer(bk13, False)
            groupgeo.addLayer(bk13)
            project.addMapLayer(bk46, False)
            groupgeo.addLayer(bk46)

            b13dybde = bk13.clone()

            _writer = QgsVectorFileWriter.writeAsVectorFormat(b13dybde,
                                                              str(lokal) + '/02_Geologi/' + 'Boringskvalitet 1-3 - Dybde',
                                                              'Boringskvalitet 1-3 - Dybde', b13dybde.crs())
            b13dybde = QgsVectorLayer(str(lokal) + '/02_Geologi/' + 'Boringskvalitet 1-3 - Dybde.gpkg',
                                      'Boringskvalitet 1-3 - Dybde', 'ogr')

            b13dybde.setName("Boringskvalitet 1-3 - Dybde")


            b46dybde = bk46.clone()
            _writer = QgsVectorFileWriter.writeAsVectorFormat(b46dybde,
                                                              str(lokal) + '/02_Geologi/' + 'Boringskvalitet 4-6 - Dybde',
                                                              'Boringskvalitet 4-6 - Dybde', b46dybde.crs())
            b46dybde = QgsVectorLayer(str(lokal) + '/02_Geologi/' + 'Boringskvalitet 4-6 - Dybde.gpkg',
                                      'Boringskvalitet 4-6 - Dybde', 'ogr')

            b46dybde.setName("Boringskvalitet 4-6 - Dybde")

            b13dybde.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Geologi - Boringsdybde.qml")
            b46dybde.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Geologi - Boringsdybde.qml")
            project.addMapLayer(b13dybde, False)
            groupgeo.addLayer(b13dybde)
            project.addMapLayer(b46dybde, False)
            groupgeo.addLayer(b46dybde)

            project.removeMapLayer(boringskvalitet)

            for layers in groupgeo.children():
                layers.setItemVisibilityChecked(False)


            groupName3 = "Geofysik"
            groupfysik = root.insertGroup(2, groupName3)

            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Geologi og jordbund/30~GERDA/98~GEUS WMS Geofysik punkter.qlr",
                project,
                group)
            geofysik = project.mapLayersByName('Lokationer for geofysiske data')[0]

            geofysikk = geofysik.clone()
            try:
                os.makedirs("{}/03_Geofysik".format(lokal))
            except:
                pass

            geofysikk.setName("Geofysisk data")

            osdd = project.mapLayersByName('OSD')[0]
            OSD2 = osdd.clone()

            _writer = QgsVectorFileWriter.writeAsVectorFormat(OSD2,
                                                              str(lokal) + '/03_Geofysik/' + 'OSD2.shp',
                                                              'OSD2', OSD2.crs(),
                                                              'ESRI Shapefile')
            OSD2 = QgsVectorLayer(str(lokal) + '/03_Geofysik/' + 'OSD2.shp',
                                  'OSD   ', 'ogr')

            OSD2.setName("OSD   ")
            OSD2.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - OSD.qml")
            project.addMapLayer(geofysikk, False)
            groupfysik.addLayer(geofysikk)
            project.removeMapLayer(geofysik)

            project.addMapLayer(OSD2, False)
            groupfysik.addLayer(OSD2)

            for layers in groupfysik.children():
                layers.setItemVisibilityChecked(False)

            groupName4 = "Kemi"
            groupkemi = root.insertGroup(0, groupName4)

            def parse_geometry(geometry):
                regex = r'[0-9-\.]+'
                parsed_geom = re.findall(regex, geometry)
                parsed_geom = [float(i) for i in parsed_geom]
                return min(parsed_geom[::2]), max(parsed_geom[::2]), min(parsed_geom[1::2]), max(parsed_geom[1::2])

            boundingbox = parse_geometry(geom.asWkt())
            boundingbox = list(boundingbox)

            crstrue = int("".join(filter(str.isdigit, str(områdelayer.crs().authid()))))

            url = "pagingEnabled='true' preferCoordinatesForWfsT11='True' srsname='{}' typename='mc_grp_analyse' url='https://data.geus.dk/geusmap/ows/{}.jsp?whoami=makyn@mst.dk&LAYERS=mc_grp_analyse&mapname=grundvand&bbox={},{},{},{}&url='https://data.geus.dk/geusmap/ows/{}.jsp?whoami=makyn@mst.dk&LAYERS=mc_grp_analyse&version=1.0.0&mapname=grundvand&bbox={},{},{},{}'".format(
                områdelayer.crs().authid(), crstrue, boundingbox[0], boundingbox[2], boundingbox[1],
                boundingbox[3], crstrue, boundingbox[0], boundingbox[2],
                boundingbox[1], boundingbox[3])

            vlayer = QgsVectorLayer(url, "Pesticider GEUS", "wfs")

            områdelayer = project.mapLayersByName('{}'.format(key))[0]

            aa = processing.run("native:clip", {'INPUT': vlayer,
                                                'OVERLAY': områdelayer, 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

            aaa = processing.run("native:extractbyattribute", {'INPUT': aa,
                'FIELD': 'stofgruppe', 'OPERATOR': 0, 'VALUE': '50', 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

            try:
                os.makedirs("{}/04_Kemi".format(lokal))
            except:
                pass

            _writer = QgsVectorFileWriter.writeAsVectorFormat(aaa,
                                                              str(lokal) + '/04_Kemi/' + 'Kemi - Pesticider',
                                                              'Kemi - Pesticider', aaa.crs())
            aaa = QgsVectorLayer(str(lokal) + '/04_Kemi/' + 'Kemi - Pesticider.gpkg',
                                 'Kemi - Pesticider', 'ogr')

            aaa.setName("Pesticider GEUS")
            aaa.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Pesticider.qml")
            project.addMapLayer(aaa, False)
            groupkemi.addLayer(aaa)


            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/Kemi/40~Enkelte stoffer/01~Seneste Nitrat.qlr",
                project,
                group)
            snitrat = project.mapLayersByName('Seneste Nitrat')[0]
            nitrat = processing.run("native:clip",
                                    {'INPUT': snitrat,
                                     'OVERLAY': områdelayer,
                                     'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            _writer = QgsVectorFileWriter.writeAsVectorFormat(nitrat,
                                                              str(lokal) + '/04_Kemi/' + 'Kemi - Nitrat',
                                                              'Kemi - Nitrat', nitrat.crs())
            nitrat = QgsVectorLayer(str(lokal) + '/04_Kemi/' + 'Kemi - Nitrat.gpkg',
                                    'Kemi - Nitrat', 'ogr')

            nitrat.setName("Seneste Nitrat")
            nitrat.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Nitrat.qml")
            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/Kemi/40~Enkelte stoffer/02~Seneste Sulfat.qlr",
                project,
                group)
            ssulfat = project.mapLayersByName('Seneste Sulfat')[0]
            sulfat = processing.run("native:clip",
                                    {'INPUT': ssulfat,
                                     'OVERLAY': områdelayer,
                                     'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            _writer = QgsVectorFileWriter.writeAsVectorFormat(sulfat,
                                                              str(lokal) + '/04_Kemi/' + 'Kemi - Sulfat',
                                                              'Kemi - Sulfat', sulfat.crs())
            sulfat = QgsVectorLayer(str(lokal) + '/04_Kemi/' + 'Kemi - Sulfat.gpkg',
                                    'Kemi - Sulfat', 'ogr')

            sulfat.setName("Seneste Sulfat")
            sulfat.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Sulfat.qml")

            ##

            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/Kemi/40~Enkelte stoffer/04~Seneste Klorid.qlr",
                project,
                group)
            sklorid = project.mapLayersByName('Seneste Klorid')[0]
            klorid = processing.run("native:clip",
                                    {'INPUT': sklorid,
                                     'OVERLAY': områdelayer,
                                     'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            _writer = QgsVectorFileWriter.writeAsVectorFormat(klorid,
                                                              str(lokal) + '/04_Kemi/' + 'Kemi - Klorid',
                                                              'Kemi - Klorid', klorid.crs())
            klorid = QgsVectorLayer(str(lokal) + '/04_Kemi/' + 'Kemi - Klorid.gpkg',
                                    'Kemi - Klorid', 'ogr')

            klorid.setName("Seneste Klorid")
            klorid.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Klorid.qml")
            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/Kemi/40~Enkelte stoffer/08~Seneste Arsen.qlr",
                project,
                group)
            sarsen = project.mapLayersByName('Seneste Arsen')[0]
            Arsen = processing.run("native:clip",
                                   {'INPUT': sarsen,
                                    'OVERLAY': områdelayer,
                                    'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            _writer = QgsVectorFileWriter.writeAsVectorFormat(Arsen,
                                                              str(lokal) + '/04_Kemi/' + 'Kemi - Arsen',
                                                              'Kemi - Arsen', Arsen.crs())
            Arsen = QgsVectorLayer(str(lokal) + '/04_Kemi/' + 'Kemi - Arsen.gpkg',
                                   'Kemi - Arsen', 'ogr')

            Arsen.setName("Seneste Arsen")
            Arsen.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Arsen.qml")
            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/Kemi/40~Enkelte stoffer/10~Seneste Jern.qlr",
                project,
                group)
            sjern = project.mapLayersByName('Seneste Jern')[0]
            Jern = processing.run("native:clip",
                                  {'INPUT': sjern,
                                   'OVERLAY': områdelayer,
                                   'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            _writer = QgsVectorFileWriter.writeAsVectorFormat(Jern,
                                                              str(lokal) + '/04_Kemi/' + 'Kemi - Jern',
                                                              'Kemi - Jern', Jern.crs())
            Jern = QgsVectorLayer(str(lokal) + '/04_Kemi/' + 'Kemi - Jern.gpkg',
                                  'Kemi - Jern', 'ogr')

            Jern.setName("Seneste Jern")
            Jern.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Jern.qml")
            snitrat = project.mapLayersByName('Seneste Nitrat')[0]
            senesteanalysealder2 = snitrat.clone()
            senesteanalysealder = processing.run("native:clip",
                                                 {'INPUT': senesteanalysealder2,
                                                  'OVERLAY': områdelayer,
                                                  'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            _writer = QgsVectorFileWriter.writeAsVectorFormat(senesteanalysealder,
                                                              str(lokal) + '/04_Kemi/' + 'Kemi - Seneste Analyse Alder',
                                                              'Kemi - Seneste Analyse Alder',
                                                              senesteanalysealder.crs())
            senesteanalysealder = QgsVectorLayer(str(lokal) + '/04_Kemi/' + 'Kemi - Seneste Analyse Alder.gpkg',
                                                 'Kemi - Seneste Analyse Alder', 'ogr')

            senesteanalysealder.setName("Seneste Analyse Alder")
            senesteanalysealder.loadNamedStyle(
                str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Seneste Analyse Alder.qml")
            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/Kemi/40~Enkelte stoffer/09~Seneste Vandtype.qlr",
                project,
                group)
            svandtype = project.mapLayersByName('Seneste Vandtype')[0]
            Vandtype = processing.run("native:clip",
                                      {'INPUT': svandtype,
                                       'OVERLAY': områdelayer,
                                       'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            _writer = QgsVectorFileWriter.writeAsVectorFormat(Vandtype,
                                                              str(lokal) + '/04_Kemi/' + 'Kemi - Vandtype',
                                                              'Kemi - Vandtype', Vandtype.crs())
            Vandtype = QgsVectorLayer(str(lokal) + '/04_Kemi/' + 'Kemi - Vandtype.gpkg',
                                      'Kemi - Vandtype', 'ogr')
            Vandtype.setName("Seneste Vandtype")
            Vandtype.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Vandtype.qml")

            project.addMapLayer(nitrat, False)
            groupkemi.addLayer(nitrat)
            project.removeMapLayer(snitrat)
            project.addMapLayer(sulfat, False)
            groupkemi.addLayer(sulfat)
            project.removeMapLayer(ssulfat)
            project.addMapLayer(klorid, False)
            groupkemi.addLayer(klorid)
            project.removeMapLayer(sklorid)
            project.addMapLayer(Arsen, False)
            groupkemi.addLayer(Arsen)
            project.removeMapLayer(sarsen)
            project.addMapLayer(Jern, False)
            groupkemi.addLayer(Jern)
            project.removeMapLayer(sjern)
            ##
            project.addMapLayer(senesteanalysealder, False)
            groupkemi.addLayer(senesteanalysealder)
            project.addMapLayer(Vandtype, False)
            groupkemi.addLayer(Vandtype)
            project.removeMapLayer(svandtype)
            ##
            for layers in groupkemi.children():
                layers.setItemVisibilityChecked(False)

            groupName5 = "Hydrologi"
            grouphydro = root.insertGroup(0, groupName5)
            sql = f'''
                                               SELECT DISTINCT ON (w.guid_intake)
                                               ROW_NUMBER() over () AS rownumber,
                                               t.antal as antal_pejlinger,
                                               w.*,
                                               b.locatmetho,
                                               s.top as screen_top,
                                               s.bottom as screen_bottom,
                                               b.geom
                                               FROM mstjupiter.waterlevel_count t
                                               INNER JOIN jupiter.watlevel w USING (boreholeno)
                                               LEFT JOIN jupiter.borehole b USING (boreholeno)
                                               LEFT JOIN jupiter.screen s USING (boreholeno)
                                               WHERE ST_Intersects(b.geom, 'SRID=25832;{geom.asWkt()}')
                                               AND COALESCE(w.situation, 0) NOT IN (1)
                                               ORDER BY guid_intake, timeofmeas

                                               '''
            uri2 = QgsDataSourceUri()
            dbb = Database(database='JUPITER')

            uri2.setConnection(str(dbb.host), str(dbb.port), str(dbb.database), str(dbb.usr_read),
                               str(dbb.pw_read))
            uri2.setDataSource('', f'({sql})', 'geom', '', 'rownumber')

            vlayer = QgsVectorLayer(uri2.uri(), "Navn", "postgres")

            pejlinger = vlayer.clone()

            try:
                os.makedirs("{}/05_Hydrologi".format(lokal))
            except:
                pass

            _writer = QgsVectorFileWriter.writeAsVectorFormat(pejlinger,
                                                              str(lokal) + '/05_Hydrologi/' + 'Pejlinger - GPS.shp',
                                                              'Pejlinger - GPS', pejlinger.crs(),
                                                              'ESRI Shapefile')
            pejlinger = QgsVectorLayer(str(lokal) + '/05_Hydrologi/' + 'Pejlinger - GPS.shp',
                                       'Pejlinger - GPS', 'ogr')

            pejlinger.setName("Pejlinger - GPS")
            pejlinger.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Hydrologi - Pejlinger GPS.qml")
            project.addMapLayer(pejlinger, False)
            grouphydro.addLayer(pejlinger)

            pejlinger = project.mapLayersByName('Pejlinger - GPS')[0]

            pejlingerstatus = pejlinger.clone()


            _writer = QgsVectorFileWriter.writeAsVectorFormat(pejlingerstatus,
                                                              str(lokal) + '/05_Hydrologi/' + 'Pejlinger - Status.shp',
                                                              'Pejlinger - Status', pejlingerstatus.crs(),
                                                              'ESRI Shapefile')
            pejlingerstatus = QgsVectorLayer(str(lokal) + '/05_Hydrologi/' + 'Pejlinger - Status.shp',
                                             'Pejlinger - Status', 'ogr')

            pejlingerstatus.setName("Pejlinger - Status på filtersætning")
            pejlingerstatus.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Hydrologi - Pejlinger Status.qml")
            project.addMapLayer(pejlingerstatus, False)
            grouphydro.addLayer(pejlingerstatus)

            pejlingerantal = pejlinger.clone()


            _writer = QgsVectorFileWriter.writeAsVectorFormat(pejlingerantal,
                                                              str(lokal) + '/05_Hydrologi/' + 'Pejlinger - Antal pr. boringsindtag.shp',
                                                              'Pejlinger - Antal pr. boringsindtag', pejlingerantal.crs(),
                                                              'ESRI Shapefile')
            pejlingerantal = QgsVectorLayer(str(lokal) + '/05_Hydrologi/' + 'Pejlinger - Antal pr. boringsindtag.shp',
                                            'Pejlinger - Antal pr. boringsindtag', 'ogr')

            pejlingerantal.setName("Pejlinger - Antal pr. boringsindtag")
            pejlingerantal.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Hydrologi - Pejlinger Antal2.qml")
            project.addMapLayer(pejlingerantal, False)
            grouphydro.addLayer(pejlingerantal)

            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/10~Boring.qlr",
                project,
                group)
            boringer = project.mapLayersByName('Boring')[0]
            Discharg = processing.run("native:clip",
                                      {'INPUT': boringer,
                                       'OVERLAY': områdelayer,
                                       'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            Discharg.setName("Sænkningsydelsesdata temp")

            project.addMapLayer(Discharg, False)
            grouphydro.addLayer(Discharg)
            project.removeMapLayer(boringer)

            uri = QgsDataSourceUri()
            db = Database(database='JUPITER')

            uri.setConnection(str(db.host), str(db.port), str(db.database), str(db.usr_read), str(db.pw_read))
            uri.setDataSource('jupiter', 'discharg', '', '', '')

            dis = QgsVectorLayer(uri.uri(), "DischargeTable", "postgres")
            dis.setName("DischargeTable")
            project.addMapLayer(dis)

            distemp = processing.run("native:joinattributestable", {
                'INPUT': Discharg,
                'FIELD': 'boreholeno',
                'INPUT_2': dis,
                'FIELD_2': 'boreholeno', 'FIELDS_TO_COPY': ['discharge', 'drawdown', 'duration'], 'METHOD': 1,
                'DISCARD_NONMATCHING': False, 'PREFIX': '', 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            distemp.setName("Tempelademad")
            project.addMapLayer(distemp, False)
            grouphydro.addLayer(distemp)

            distemp22 = processing.run("native:joinattributestable", {
                'INPUT': distemp,
                'FIELD': 'boreholeid',
                'INPUT_2': pejlinger,
                'FIELD_2': 'boreholeid', 'FIELDS_TO_COPY': ['locatmetho', 'screen_top', 'screen_bottom'], 'METHOD': 1,
                'DISCARD_NONMATCHING': False, 'PREFIX': '', 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']


            _writer = QgsVectorFileWriter.writeAsVectorFormat(distemp22,
                                                              str(lokal) + '/05_Hydrologi/' + 'Sænkningsydelsesdata',
                                                              'Sænkningsydelsesdata', distemp22.crs())
            distemp22 = QgsVectorLayer(str(lokal) + '/05_Hydrologi/' + 'Sænkningsydelsesdata.gpkg',
                                       'Sænkningsydelsesdata', 'ogr')


            distemp22.setName("Sænkningsydelsesdata")
            distemp22.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Hydrologi - Sænkningsydelsesdata.qml")

            project.addMapLayer(distemp22, False)
            grouphydro.addLayer(distemp22)
            project.removeMapLayer(dis)
            project.removeMapLayer(Discharg)
            project.removeMapLayer(distemp)

            diskvalit = distemp22.clone()

            _writer = QgsVectorFileWriter.writeAsVectorFormat(diskvalit,
                                                              str(lokal) + '/05_Hydrologi/' + 'Sænkningsydelsesdata - Kvalitet',
                                                              'Sænkningsydelsesdata - Kvalitet', diskvalit.crs())
            diskvalit = QgsVectorLayer(str(lokal) + '/05_Hydrologi/' + 'Sænkningsydelsesdata - Kvalitet.gpkg',
                                       'Sænkningsydelsesdata - Kvalitet', 'ogr')

            diskvalit.setName("Sænkningsydelsesdata - Kvalitet")
            diskvalit.loadNamedStyle(
                str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Hydrologi - Sænkningsydelsesdata Kvalitet.qml")
            disgps = distemp22.clone()

            _writer = QgsVectorFileWriter.writeAsVectorFormat(disgps,
                                                              str(lokal) + '/05_Hydrologi/' + 'Sænkningsydelsesdata - GPS',
                                                              'Sænkningsydelsesdata - GPS', disgps.crs())
            disgps = QgsVectorLayer(str(lokal) + '/05_Hydrologi/' + 'Sænkningsydelsesdata - GPS.gpkg',
                                    'Sænkningsydelsesdata - GPS', 'ogr')

            disgps.setName("Sænkningsydelsesdata - GPS")
            disgps.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Hydrologi - Sænkningsydelsesdata GPS.qml")
            project.addMapLayer(diskvalit, False)
            grouphydro.addLayer(diskvalit)
            project.addMapLayer(disgps, False)
            grouphydro.addLayer(disgps)

            for layers in grouphydro.children():
                layers.setItemVisibilityChecked(False)

            nytområde = project.mapLayersByName('Område')[0]
            nodenytområde = project.layerTreeRoot().findLayer(nytområde.id())
            if nodenytområde:
                nodenytområde.setItemVisibilityChecked(True)

            root2 = QgsProject.instance().layerTreeRoot()
            nodes = root2.children()

            for n in nodes:
                if isinstance(n, QgsLayerTreeGroup):
                    if n.isExpanded() == True:
                        n.setExpanded(False)

            self.pushButton_first.setText("Færdig ✓")
            self.lokalefiler.setEnabled(False)
            self.lokalefilersti.setEnabled(False)
            self.lokalefilervlgsti.setEnabled(False)

            self.repaint()


        elif self.lokalefiler.checkState() == Qt.Checked and len(self.lokalefilersti.text()) == 0:
            QMessageBox.warning(None, 'Fejl', "Vælg et output directory for dine filer.")

        else:
            self.pushButton_first.setText("Kører")
            self.pushButton_first.setEnabled(False)
            self.repaint()
            project = QgsProject.instance()
            key = self.comboBox.currentText()
            områdelayer = project.mapLayersByName('{}'.format(key))[0]

            for kk, feature in enumerate(områdelayer.getFeatures()):
                if kk == 0:
                    geom = feature.geometry()

            nodeområdelayer = project.layerTreeRoot().findLayer(områdelayer.id())
            if nodeområdelayer:
                nodeområdelayer.setItemVisibilityChecked(False)
            root = project.layerTreeRoot()


            groupName = "Generelt"
            root = project.layerTreeRoot()
            group = root.insertGroup(0, groupName)

            groupstiltempaltes = root.insertGroup(0, "Stilarter")

            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteKloridTemplate.qlr", project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteVandtypeTemplate.qlr",
                project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteSulfatTemplate.qlr", project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenestePesticiderTemplate.qlr",
                project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteArsenTemplate.qlr", project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteNitratTemplate.qlr", project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteJernTemplate.qlr", project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/SenesteAnalyseAlder.qlr", project,
                groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/StandardMagasinTemplate.qlr",
                project, groupstiltempaltes)
            QgsLayerDefinition().loadLayerDefinition(
                str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/StandardMagasinTemplate2.qlr",
                project,
                groupstiltempaltes)

            memlay = områdelayer.clone()
            memlay.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Projektområde.qml")
            memlay.setName('Område')
            project.addMapLayer(memlay, False)  # False is the key
            group.addLayer(memlay)

            mem_layer = QgsVectorLayer("Polygon?crs=epsg:25832", "Name", "memory")

            coords = [(350971.60, 6432166.961), (967593.96466, 6430251.984), (959934.059, 5986934.977),
                      (213093.313, 6016617.109)]
            polygon = QgsGeometry.fromPolygonXY([[QgsPointXY(pair[0], pair[1]) for pair in coords]])

            feature = QgsFeature()
            feature.setGeometry(polygon)
            mem_layer.dataProvider().addFeatures([feature])
            mem_layer.updateFields()

            a = processing.run("native:symmetricaldifference",
                               {'INPUT': mem_layer,
                                'OVERLAY': områdelayer,
                                'OVERLAY_FIELDS_PREFIX': '', 'OUTPUT': 'TEMPORARY_OUTPUT'})

            a['OUTPUT'].setName("Område-grå")
            project.addMapLayer(a['OUTPUT'], False)  # False is the key
            a['OUTPUT'].loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Projektområde_grå.qml")
            group.addLayer(a['OUTPUT'])

            ##############
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Administrative grænser/01~Kommunegrænser.qlr", project, group)
            kommunegrænse = project.mapLayersByName('kommunegraense')[0]
            kom = processing.run("native:extractbyexpression",
                                 {'INPUT': kommunegrænse,
                                  'EXPRESSION': 'komnavn is not null', 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

            kom.setName("Kommunegrænser")
            kom.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Kommunegrænse.qml")
            project.addMapLayer(kom, False)
            group.addLayer(kom)
            project.removeMapLayer(kommunegrænse)

            # Anlæg
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/42~Anlæg_aktive.qlr", project, group)
            anlægaktive = project.mapLayersByName('Anlæg_aktiv')[0]

            aa = processing.run("native:clip",
                                {'INPUT': anlægaktive,
                                 'OVERLAY': områdelayer,
                                 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            aa.setName("Anlæg Aktive")
            # aa.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Anlæg Aktive - andet.qml")

            aa.startEditing()
            aa.dataProvider().addAttributes([QgsField('AnlNavn', QVariant.String)])
            aa.updateFields()
            idx = aa.dataProvider().fieldNameIndex('AnlNavn')

            features = [feat for feat in aa.getFeatures()]

            context = QgsExpressionContext()
            scope = QgsExpressionContextScope()
            context.appendScope(scope)
            listOfResults = []

            for f in aa.getFeatures():
                context.setFeature(f)
                exp = QgsExpression(
                    "CASE WHEN virksomhedstype IN ('Gartneri', 'Anden erhvervsvirksomhed', 'Grusvask', 'Hotel, Campingplads o.lign.', 'Husdyrfarm', 'Husholdning, én husstand', 'Husholdninger, flere hustande', 'Levnedmiddelindustri', 'Markvanding', 'Mælkeleverandør', 'Sportsplads, park o.lign') THEN wordwrap(virksomhedstype, 20) ELSE NULL END")
                f['AnlNavn'] = exp.evaluate(context)
                aa.updateFeature(f)

            aa.commitChanges()

            aa = processing.run("native:extractbyexpression", {
                'INPUT': aa,
                'EXPRESSION': ' "AnlNavn" is not NULL', 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            aa.setName("Anlæg Aktive")

            unique_values = aa.uniqueValues(aa.fields().indexFromName("AnlNavn"))

            category_list = []
            for value in unique_values:
                symbol = QgsSymbol.defaultSymbol(aa.geometryType())
                category = QgsRendererCategory(value, symbol, str(value))
                category_list.append(category)

            renderer = QgsCategorizedSymbolRenderer('AnlNavn', category_list)
            renderer.updateColorRamp(QgsRandomColorRamp())
            aa.setRenderer(renderer)
            aa.triggerRepaint()

            QgsLayerDefinition().loadLayerDefinition("F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/40~Anlæg.qlr",
                                                     project, group)
            anlæg = project.mapLayersByName('Anlæg')[0]

            vvatemp = processing.run("native:clip",
                                     {'INPUT': anlæg,
                                      'OVERLAY': områdelayer,
                                      'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            project.addMapLayer(vvatemp, False)
            group.addLayer(vvatemp)

            vva = processing.run("native:extractbyexpression", {
                'INPUT': vvatemp,
                'EXPRESSION': ' "aktiv" = \'Aktiv\'', 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            vva.setName("Vandværker Aktive")

            vva.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Anlæg Aktive - Vandværker.qml")

            project.addMapLayer(aa, False)
            group.addLayer(aa)
            project.addMapLayer(vva, False)
            group.addLayer(vva)
            project.removeMapLayer(anlægaktive)
            project.removeMapLayer(anlæg)
            project.removeMapLayer(vvatemp)

            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/20~Vandforsyningsboring.qlr", project, group)
            indvindingsboringer = project.mapLayersByName('Vandforsyningsboring')[0]
            ind = processing.run("native:clip",
                                 {'INPUT': indvindingsboringer,
                                  'OVERLAY': områdelayer,
                                  'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            ind = processing.run("native:clip",
                                 {'INPUT': indvindingsboringer,
                                  'OVERLAY': områdelayer,
                                  'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            ind.setName("Indvindingsboringer")

            ind.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Indvindingsboringer.qml")

            project.addMapLayer(ind, False)
            group.addLayer(ind)
            project.removeMapLayer(indvindingsboringer)

            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Geologi og jordbund/01~FOHM/02~FOHM - Lagtildeling filter - Alle lag.qlr",
                project, group)
            fohm = project.mapLayersByName('FOHM - Lagtildeling filter - Alle lag')[0]
            lagdelingvfilter = processing.run("native:clip",
                                              {'INPUT': fohm,
                                               'OVERLAY': områdelayer,
                                               'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            lagdelingvfilter.setName("Filtersætning i FOHM")

            lagdelingvfilter.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - FOHM.qml")

            project.addMapLayer(lagdelingvfilter, False)
            group.addLayer(lagdelingvfilter)
            project.removeMapLayer(fohm)

            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/GKO bekendtgørelser gældende/05~IOL_alle.qlr",
                project, group)
            indvindingsoplande = project.mapLayersByName('Alle IOL (både inden- og udenfor OSD) - Gældende udpegning')[
                0]

            IOL = processing.run("native:joinattributestable", {
                'INPUT': indvindingsoplande,
                'FIELD': 'anl_id',
                'INPUT_2': ind,
                'FIELD_2': 'plantid', 'FIELDS_TO_COPY': [], 'METHOD': 1, 'DISCARD_NONMATCHING': True, 'PREFIX': '',
                'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']


            # IOL = processing.run("native:extractbylocation",
            #                     {'INPUT': indvindingsoplande,
            #                      'PREDICATE': [6],
            #                      'INTERSECT': områdelayer,
            #                      'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            # IOL = processing.run("native:clip",
            #                     {'INPUT': indvindingsoplande,
            #                      'OVERLAY': områdelayer,
            #                      'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']


            IOL.setName("Indvindingsoplande")
            IOL.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Indvindingsoplande.qml")

            IOLGroup = IOL.clone()
            IOLGroup.setName("Indvindingsoplande Grupperet")
            ########

            IOLGroup.startEditing()
            IOLGroup.dataProvider().addAttributes([QgsField('Modelnavn', QVariant.String)])
            IOLGroup.updateFields()
            idx = IOLGroup.dataProvider().fieldNameIndex('Modelnavn')

            features = [feat for feat in IOLGroup.getFeatures()]

            context = QgsExpressionContext()
            scope = QgsExpressionContextScope()
            context.appendScope(scope)
            listOfResults = []

            for f in IOLGroup.getFeatures():
                context.setFeature(f)
                exp = QgsExpression("wordwrap(kortnavn, 20)")
                f['Modelnavn'] = exp.evaluate(context)
                IOLGroup.updateFeature(f)

            IOLGroup.commitChanges()

            unique_values = IOLGroup.uniqueValues(IOLGroup.fields().indexFromName("Modelnavn"))

            category_list = []
            for value in unique_values:
                symbol = QgsSymbol.defaultSymbol(IOLGroup.geometryType())
                symbol.setOpacity(0.7)
                category = QgsRendererCategory(value, symbol, str(value))
                category_list.append(category)

            renderer = QgsCategorizedSymbolRenderer('Modelnavn', category_list)
            renderer.updateColorRamp(QgsRandomColorRamp())
            IOLGroup.setRenderer(renderer)
            # IOLGroup.setOpacity(0.7)
            #                     layer.setLayerTransparency(95)
            # IOLGroup.renderer().symbol().setOpacity(0.7)
            IOLGroup.triggerRepaint()

            project.addMapLayer(IOL, False)
            group.addLayer(IOL)

            project.addMapLayer(IOLGroup, False)
            group.addLayer(IOLGroup)
            project.removeMapLayer(indvindingsoplande)

            ## OSD
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/GKO bekendtgørelser gældende/01~Drikkevandsinteresser.qlr",
                project, group)
            områdesærligedrikke = project.mapLayersByName('di - Gældende udpegning')[0]
            OSD = processing.run("native:clip",
                                 {'INPUT': områdesærligedrikke,
                                  'OVERLAY': områdelayer,
                                  'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            OSD.setName("OSD")
            OSD.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - OSD.qml")

            project.addMapLayer(OSD, False)
            group.addLayer(OSD)
            project.removeMapLayer(områdesærligedrikke)

            ####F:\GKO\data\grukos\kortskabe\QGIS\Kortskab\Geologi og jordbund

            uri = "F:/Nordjylland/Medarbejdere/MAKYN/Ongoing/Automatiseret-Opstartsrapport/QGIS/Lag/PSmed.tif"
            rlayer = QgsRasterLayer(uri, "Per Smed")
            project.addMapLayer(rlayer, False)
            group.addLayer(rlayer)

            ###
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Geologi og jordbund/10~Begravededale/01~Begravede dale.qlr",
                project,
                group)
            begravedeledale = project.mapLayersByName('begravede_dale')[0]
            dale = processing.run("native:extractbyexpression",
                                  {'INPUT': begravedeledale,
                                   'EXPRESSION': 'fid is not null', 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]
            dale.setName("Begravede Dale")
            dale.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Generelt - Begravede dale.qml")

            project.addMapLayer(dale, False)
            group.addLayer(dale)
            project.removeMapLayer(begravedeledale)

            ###
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Geologi og jordbund/40~Jordartskort/12~Jordartskort 1 til 200.000 - GEUS WMS.qlr",
                project,
                group)
            jordartskort = project.mapLayersByName('Jordartskort 1:200.000')[0]

            jordart = jordartskort.clone()
            jordart.setName("Jordartskort")

            project.addMapLayer(jordart, False)
            group.addLayer(jordart)
            project.removeMapLayer(jordartskort)

            for layers in group.children():
                layers.setItemVisibilityChecked(False)

            ###############################
            groupName2 = "Geologi"
            groupgeo = root.insertGroup(0, groupName2)

            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Geologi og jordbund/20~Datakvalitet/01~Boringer/01~Boringskvalitet - Lithologi+Position.qlr",
                project,
                group)
            boringskvalitet = project.mapLayersByName('Boringskvalitet - Lithologi+Position')[0]

            bk = processing.run("native:clip",
                                {'INPUT': boringskvalitet,
                                 'OVERLAY': områdelayer,
                                 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            bk13 = processing.run("native:extractbyexpression",
                                  {'INPUT': bk,
                                   'EXPRESSION': 'quality_litho_position < 3.5', 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]
            bk46 = processing.run("native:extractbyexpression",
                                  {'INPUT': bk,
                                   'EXPRESSION': 'quality_litho_position > 3.5', 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]
            bk13.setName("Boringskvalitet 1-3")
            bk46.setName("Boringskvalitet 4-6")

            bk13.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Geologi - Boringskvalitet.qml")
            bk46.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Geologi - Boringskvalitet.qml")

            project.addMapLayer(bk13, False)
            groupgeo.addLayer(bk13)
            project.addMapLayer(bk46, False)
            groupgeo.addLayer(bk46)
            project.removeMapLayer(boringskvalitet)

            b13dybde = bk13.clone()
            b13dybde.setName("Boringskvalitet 1-3 - Dybde")
            b46dybde = bk46.clone()
            b46dybde.setName("Boringskvalitet 4-6 - Dybde")

            b13dybde.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Geologi - Boringsdybde.qml")
            b46dybde.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Geologi - Boringsdybde.qml")
            project.addMapLayer(b13dybde, False)
            groupgeo.addLayer(b13dybde)
            project.addMapLayer(b46dybde, False)
            groupgeo.addLayer(b46dybde)

            for layers in groupgeo.children():
                layers.setItemVisibilityChecked(False)

            ###############################
            groupName3 = "Geofysik"
            groupfysik = root.insertGroup(2, groupName3)

            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Geologi og jordbund/30~GERDA/98~GEUS WMS Geofysik punkter.qlr",
                project,
                group)
            geofysik = project.mapLayersByName('Lokationer for geofysiske data')[0]

            geofysikk = geofysik.clone()
            geofysikk.setName("Geofysisk data")

            project.addMapLayer(geofysikk, False)
            groupfysik.addLayer(geofysikk)
            project.removeMapLayer(geofysik)

            osdd = project.mapLayersByName('OSD')[0]
            OSD2 = osdd.clone()
            OSD2.setName("OSD   ")
            project.addMapLayer(OSD2, False)
            groupfysik.addLayer(OSD2)

            for layers in groupfysik.children():
                layers.setItemVisibilityChecked(False)

            groupName4 = "Kemi"
            groupkemi = root.insertGroup(0, groupName4)

            def parse_geometry(geometry):
                regex = r'[0-9-\.]+'
                parsed_geom = re.findall(regex, geometry)
                parsed_geom = [float(i) for i in parsed_geom]
                return min(parsed_geom[::2]), max(parsed_geom[::2]), min(parsed_geom[1::2]), max(parsed_geom[1::2])

            boundingbox = parse_geometry(geom.asWkt())
            boundingbox = list(boundingbox)

            crstrue = int("".join(filter(str.isdigit, str(områdelayer.crs().authid()))))

            url = "pagingEnabled='true' preferCoordinatesForWfsT11='True' srsname='{}' typename='mc_grp_analyse' url='https://data.geus.dk/geusmap/ows/{}.jsp?whoami=makyn@mst.dk&LAYERS=mc_grp_analyse&mapname=grundvand&bbox={},{},{},{}&url='https://data.geus.dk/geusmap/ows/{}.jsp?whoami=makyn@mst.dk&LAYERS=mc_grp_analyse&version=1.0.0&mapname=grundvand&bbox={},{},{},{}'".format(
                områdelayer.crs().authid(), crstrue, boundingbox[0], boundingbox[2], boundingbox[1],
                boundingbox[3], crstrue, boundingbox[0], boundingbox[2],
                boundingbox[1], boundingbox[3])

            vlayer = QgsVectorLayer(url, "Pesticider GEUS", "wfs")

            områdelayer = project.mapLayersByName('{}'.format(key))[0]

            a = processing.run("native:clip", {'INPUT': vlayer,
                                               'OVERLAY': områdelayer, 'OUTPUT': 'TEMPORARY_OUTPUT'})

            aaa = processing.run("native:extractbyattribute", {'INPUT': a["OUTPUT"],
                'FIELD': 'stofgruppe', 'OPERATOR': 0, 'VALUE': '50', 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

            aaa.setName("Pesticider GEUS")
            aaa.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Pesticider.qml")

            project.addMapLayer(aaa, False)
            groupkemi.addLayer(aaa)

            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/Kemi/40~Enkelte stoffer/01~Seneste Nitrat.qlr",
                project,
                group)
            snitrat = project.mapLayersByName('Seneste Nitrat')[0]
            nitrat = processing.run("native:clip",
                                    {'INPUT': snitrat,
                                     'OVERLAY': områdelayer,
                                     'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            nitrat.setName("Seneste Nitrat")
            nitrat.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Nitrat.qml")

            project.addMapLayer(nitrat, False)
            groupkemi.addLayer(nitrat)
            project.removeMapLayer(snitrat)

            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/Kemi/40~Enkelte stoffer/02~Seneste Sulfat.qlr",
                project,
                group)
            ssulfat = project.mapLayersByName('Seneste Sulfat')[0]
            sulfat = processing.run("native:clip",
                                    {'INPUT': ssulfat,
                                     'OVERLAY': områdelayer,
                                     'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            sulfat.setName("Seneste Sulfat")
            sulfat.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Sulfat.qml")

            project.addMapLayer(sulfat, False)
            groupkemi.addLayer(sulfat)
            project.removeMapLayer(ssulfat)
            ##

            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/Kemi/40~Enkelte stoffer/04~Seneste Klorid.qlr",
                project,
                group)
            sklorid = project.mapLayersByName('Seneste Klorid')[0]
            klorid = processing.run("native:clip",
                                    {'INPUT': sklorid,
                                     'OVERLAY': områdelayer,
                                     'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            klorid.setName("Seneste Klorid")
            klorid.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Klorid.qml")

            project.addMapLayer(klorid, False)
            groupkemi.addLayer(klorid)
            project.removeMapLayer(sklorid)
            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/Kemi/40~Enkelte stoffer/08~Seneste Arsen.qlr",
                project,
                group)
            sarsen = project.mapLayersByName('Seneste Arsen')[0]
            Arsen = processing.run("native:clip",
                                   {'INPUT': sarsen,
                                    'OVERLAY': områdelayer,
                                    'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            Arsen.setName("Seneste Arsen")
            Arsen.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Arsen.qml")

            project.addMapLayer(Arsen, False)
            groupkemi.addLayer(Arsen)
            project.removeMapLayer(sarsen)
            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/Kemi/40~Enkelte stoffer/10~Seneste Jern.qlr",
                project,
                group)
            sjern = project.mapLayersByName('Seneste Jern')[0]
            Jern = processing.run("native:clip",
                                  {'INPUT': sjern,
                                   'OVERLAY': områdelayer,
                                   'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            Jern.setName("Seneste Jern")
            Jern.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Jern.qml")

            project.addMapLayer(Jern, False)
            groupkemi.addLayer(Jern)
            project.removeMapLayer(sjern)
            ##

            snitrat = project.mapLayersByName('Seneste Nitrat')[0]
            senesteanalysealder2 = snitrat.clone()
            senesteanalysealder = processing.run("native:clip",
                                                 {'INPUT': senesteanalysealder2,
                                                  'OVERLAY': områdelayer,
                                                  'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            senesteanalysealder.setName("Seneste Analyse Alder")
            senesteanalysealder.loadNamedStyle \
                (str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Seneste Analyse Alder.qml")
            project.addMapLayer(senesteanalysealder, False)
            groupkemi.addLayer(senesteanalysealder)

            ##
            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/Kemi/40~Enkelte stoffer/09~Seneste Vandtype.qlr",
                project,
                group)
            svandtype = project.mapLayersByName('Seneste Vandtype')[0]
            Vandtype = processing.run("native:clip",
                                      {'INPUT': svandtype,
                                       'OVERLAY': områdelayer,
                                       'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            Vandtype.setName("Seneste Vandtype")
            Vandtype.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Vandtype.qml")

            project.addMapLayer(Vandtype, False)
            groupkemi.addLayer(Vandtype)
            project.removeMapLayer(svandtype)
            ##

            for layers in groupkemi.children():
                layers.setItemVisibilityChecked(False)

            groupName5 = "Hydrologi"
            grouphydro = root.insertGroup(0, groupName5)
            sql = f'''
            SELECT DISTINCT ON (w.guid_intake)
            ROW_NUMBER() over () AS rownumber,
            t.antal as antal_pejlinger,
            w.*,
            b.locatmetho,
            s.top as screen_top,
            s.bottom as screen_bottom,
            b.geom
            FROM mstjupiter.waterlevel_count t
            INNER JOIN jupiter.watlevel w USING (boreholeno)
            LEFT JOIN jupiter.borehole b USING (boreholeno)
            LEFT JOIN jupiter.screen s USING (boreholeno)
            WHERE ST_Intersects(b.geom, 'SRID=25832;{geom.asWkt()}')
            ORDER BY guid_intake, timeofmeas

            '''
            uri2 = QgsDataSourceUri()
            dbb = Database(database='JUPITER')

            uri2.setConnection(str(dbb.host), str(dbb.port), str(dbb.database), str(dbb.usr_read), str(dbb.pw_read))
            uri2.setDataSource('', f'({sql})', 'geom', '', 'rownumber')

            vlayer = QgsVectorLayer(uri2.uri(), "Navn", "postgres")

            pejlinger = vlayer.clone()

            pejlinger.setName("Pejlinger - GPS")
            pejlinger.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Hydrologi - Pejlinger GPS.qml")

            project.addMapLayer(pejlinger, False)
            grouphydro.addLayer(pejlinger)
            project.removeMapLayer(vlayer)

            pejlinger = project.mapLayersByName('Pejlinger - GPS')[0]
            pejlingerstatus = pejlinger.clone()
            pejlingerstatus.setName("Pejlinger - Status på filtersætning")
            pejlingerstatus.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Hydrologi - Pejlinger Status.qml")
            project.addMapLayer(pejlingerstatus, False)
            grouphydro.addLayer(pejlingerstatus)

            pejlingerantal = pejlinger.clone()
            pejlingerantal.setName("Pejlinger - Antal pr. boringsindtag")
            pejlingerantal.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Hydrologi - Pejlinger Antal.qml")
            project.addMapLayer(pejlingerantal, False)
            grouphydro.addLayer(pejlingerantal)

            QgsLayerDefinition().loadLayerDefinition(
                "F:/GKO/data/grukos/kortskabe/QGIS/Kortskab - Ny/Jupiter/10~Boring.qlr" ,project ,group)
            boringer = project.mapLayersByName('Boring')[0]
            Discharg = processing.run("native:clip",
                                      {'INPUT': boringer,
                                       'OVERLAY': områdelayer,
                                       'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            Discharg.setName("Sænkningsydelsesdata temp")

            project.addMapLayer(Discharg, False)
            grouphydro.addLayer(Discharg)
            project.removeMapLayer(boringer)

            uri = QgsDataSourceUri()
            db = Database(database='JUPITER')

            uri.setConnection(str(db.host), str(db.port), str(db.database), str(db.usr_read), str(db.pw_read))
            uri.setDataSource('jupiter', 'discharg', '', '', '')

            dis = QgsVectorLayer(uri.uri(), "DischargeTable", "postgres")
            dis.setName("DischargeTable")
            project.addMapLayer(dis)

            distemp = processing.run("native:joinattributestable", {
                'INPUT': Discharg,
                'FIELD': 'boreholeno',
                'INPUT_2': dis,
                'FIELD_2': 'boreholeno', 'FIELDS_TO_COPY': ['discharge', 'drawdown', 'duration'], 'METHOD': 1,
                'DISCARD_NONMATCHING': False, 'PREFIX': '', 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
            distemp.setName("Tempelademad")
            project.addMapLayer(distemp, False)
            grouphydro.addLayer(distemp)

            distemp22 = processing.run("native:joinattributestable", {
                'INPUT': distemp,
                'FIELD': 'boreholeid',
                'INPUT_2': pejlinger,
                'FIELD_2': 'boreholeid', 'FIELDS_TO_COPY': ['locatmetho', 'screen_top', 'screen_bottom'], 'METHOD': 1,
                'DISCARD_NONMATCHING': False, 'PREFIX': '', 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

            distemp22.setName("Sænkningsydelsesdata")
            distemp22.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Hydrologi - Sænkningsydelsesdata.qml")

            project.addMapLayer(distemp22, False)
            grouphydro.addLayer(distemp22)
            project.removeMapLayer(dis)
            project.removeMapLayer(Discharg)
            project.removeMapLayer(distemp)

            diskvalit = distemp22.clone()
            diskvalit.setName("Sænkningsydelsesdata - Kvalitet")
            diskvalit.loadNamedStyle(
                str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Hydrologi - Sænkningsydelsesdata Kvalitet.qml")
            disgps = distemp22.clone()
            disgps.setName("Sænkningsydelsesdata - GPS")
            disgps.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Hydrologi - Sænkningsydelsesdata GPS.qml")
            project.addMapLayer(diskvalit, False)
            grouphydro.addLayer(diskvalit)
            project.addMapLayer(disgps, False)
            grouphydro.addLayer(disgps)

            for layers in grouphydro.children():
                layers.setItemVisibilityChecked(False)

            nytområde = project.mapLayersByName('Område')[0]
            nodenytområde = project.layerTreeRoot().findLayer(nytområde.id())
            if nodenytområde:
                nodenytområde.setItemVisibilityChecked(True)

            root2 = QgsProject.instance().layerTreeRoot()
            nodes = root2.children()

            for n in nodes:
                if isinstance(n, QgsLayerTreeGroup):
                    if n.isExpanded() == True:
                        n.setExpanded(False)

            self.pushButton_first.setText("Færdig ✓")
            self.repaint()
    else:
        QMessageBox.warning(None, 'Fejl', "Intet input (områdepolygon).")

def opstart_second(self):
    # if len(self.comboBox.currentText()) > 0:
    # try:
    if True:
        self.pushButton_second.setText("Kører")
        self.pushButton_second.setEnabled(False)
        self.repaint()
    project = QgsProject.instance()
    key2 = self.comboBox.currentText()
    områdelayer = project.mapLayersByName('{}'.format(key2))[0]

    composition = QgsPrintLayout(project)
    document = QDomDocument()

    # read template content
    path = str(os.path.join(os.path.dirname( __file__ ), '..' )) + "/layout_template2_k.qpt"
    template_file = open(path)
    template_content = template_file.read()
    template_file.close()
    document.setContent(template_content)

    # load layout from template and add to Layout Manager
    composition.loadFromTemplate(document, QgsReadWriteContext())
    project.layoutManager().addLayout(composition)

    layout1 = QgsProject.instance().layoutManager().layoutByName("Projektområde - OSD")
    image = layout1.itemById("Logo")

    image.setPicturePath(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Images/miljoeministeriet_dk_rgb-ny.png")

    ###################################################################################
    l1 = project.mapLayersByName("OSD")[0]
    l2 = project.mapLayersByName("Kommunegrænser")
    l3 = project.mapLayersByName("Område")[0]

    project.layerTreeRoot().findLayer(l1.id()).setItemVisibilityChecked(True)
    for k in l2:
        project.layerTreeRoot().findLayer(k.id()).setItemVisibilityChecked(True)
    project.layerTreeRoot().findLayer(l3.id()).setItemVisibilityChecked(True)

    mapThemesCollection = project.mapThemeCollection()
    mapThemes = mapThemesCollection.mapThemes()

    mapThemeRecord = QgsMapThemeCollection.createThemeFromCurrentState(
        QgsProject.instance().layerTreeRoot(),
        iface.layerTreeView().layerTreeModel()
    )
    mapThemesCollection.insert("Projektområde - OSD", mapThemeRecord)

    layers_to_remove = []
    for layer in project.mapLayers().values():
        if layer.type() == QgsMapLayer.RasterLayer:
            layers_to_remove.append(layer)
    layers_to_remove.append(project.mapLayersByName('{}'.format(key2))[0])
    layers_to_remove.append(project.mapLayersByName("Område-grå")[0])
    layers_to_remove.append(project.mapLayersByName("Per Smed")[0])

    layout1 = project.layoutManager().layoutByName("Projektområde - OSD")
    legend = [i for i in layout1.items() if isinstance(i, QgsLayoutItemLegend)][0]
    legend.setAutoUpdateModel(True)
    legend.setAutoUpdateModel(False)

    map = [i for i in layout1.items() if isinstance(i, QgsLayoutItemMap)][0]
    map.setFollowVisibilityPreset(True)
    map.setFollowVisibilityPresetName("Projektområde - OSD")

    for l in layers_to_remove:
        legend.model().rootGroup().removeLayer(l)

    QgsLegendRenderer.setNodeLegendStyle(
        legend.model().rootGroup().children()[0], QgsLegendStyle.Hidden)

    layout1.refresh()
    ###################################################################################


    musthavelayouts = {}

    # Adding list as value
    musthavelayouts["Projektområde - OSD"] = ["Område", "OSD", "Kommunegrænser"]
    musthavelayouts["Projektområde - Aktive indvindingsboringer"] = ["Område", "OSD", "Indvindingsboringer", "Indvindingsoplande"]
    musthavelayouts["Projektområde - Vandforsyningsanlæg"] = ["Område", "OSD", "Vandværker Aktive"]
    musthavelayouts["Projektområde - Anlæg"] = ["Område", "OSD", "Anlæg Aktive"]
    musthavelayouts["Projektområde - IOL Grupperet"] = ["Område", "OSD", "Indvindingsoplande Grupperet"]
    musthavelayouts["Projektområde - Per Smed"] = ["Område", "Per Smed", "Område-grå"]
    musthavelayouts["Projektområde - Jordartskort"] = ["Område", "Jordartskort", "Område-grå", "Begravede Dale"]
    musthavelayouts["Projektområde - Filtersætning i FOHM"] = ["Område", "OSD", "Filtersætning i FOHM"]
    musthavelayouts["Geologi - Boringskvalitet 1-3"] = ["Område", "OSD", "Boringskvalitet 1-3"]
    musthavelayouts["Geologi - Boringskvalitet 4-6"] = ["Område", "OSD", "Boringskvalitet 4-6"]
    musthavelayouts["Geologi - Boringskvalitet 1-3 - Dybde"] = ["Område", "OSD", "Boringskvalitet 1-3 - Dybde"]
    musthavelayouts["Geologi - Boringskvalitet 4-6 - Dybde"] = ["Område", "OSD", "Boringskvalitet 4-6 - Dybde"]
    musthavelayouts["Geofysik - Modeller"] = ["Område", "Område-grå", "Geofysisk data", "OSD   "]
    musthavelayouts["Kemi - Pesticider"] = ["Område", "OSD", "Pesticider GEUS", "SenestePesticiderTemplate", "MagasinTemplate2"]
    musthavelayouts["Kemi - Seneste Nitrat"] = ["Område", "OSD", "Seneste Nitrat", "SenesteNitratTemplate", "MagasinTemplate"]
    musthavelayouts["Kemi - Seneste Sulfat"] = ["Område", "OSD", "Seneste Sulfat", "SenesteSulfatTemplate", "MagasinTemplate"]
    musthavelayouts["Kemi - Seneste Jern"] = ["Område", "OSD", "Seneste Jern", "SenesteJernTemplate", "MagasinTemplate"]
    musthavelayouts["Kemi - Seneste Klorid"] = ["Område", "OSD", "Seneste Klorid", "SenesteKloridTemplate", "MagasinTemplate"]
    musthavelayouts["Kemi - Seneste Arsen"] = ["Område", "OSD", "Seneste Arsen", "SenesteArsenTemplate", "MagasinTemplate"]
    musthavelayouts["Kemi - Seneste Analyse Alder"] = ["Område", "OSD", "Seneste Analyse Alder", "SenesteAnalyseAlder", "MagasinTemplate"]
    musthavelayouts["Kemi - Seneste Vandtype"] = ["Område", "OSD", "Seneste Vandtype", "SenesteVandtypeTemplate", "MagasinTemplate"]
    musthavelayouts["Hydrologi - Pejlinger GPS"] = ["Område", "OSD", "Pejlinger - GPS"]
    musthavelayouts["Hydrologi - Pejlinger Status på filtersætning"] = ["Område", "OSD", "Pejlinger - Status på filtersætning"]
    musthavelayouts["Hydrologi - Pejlinger Antal pr. boringsindtag"] = ["Område", "OSD", "Pejlinger - Antal pr. boringsindtag"]
    musthavelayouts["Hydrologi - Sænkningsydelsesdata Status på filtersætning"] = ["Område", "OSD", "Sænkningsydelsesdata"]
    musthavelayouts["Hydrologi - Sænkningsydelsesdata Kvalitet"] = ["Område", "OSD", "Sænkningsydelsesdata - Kvalitet"]
    musthavelayouts["Hydrologi - Sænkningsydelsesdata GPS"] = ["Område", "OSD", "Sænkningsydelsesdata - GPS"]


    root = project.layerTreeRoot()
    manager = project.layoutManager()

    for key in musthavelayouts:
        stilartsgruppe = root.findGroup("Stilarter")
        for layers in stilartsgruppe.children():
            layers.setItemVisibilityChecked(False)
        group = root.findGroup("Generelt")
        for layers in group.children():
            layers.setItemVisibilityChecked(False)
        groupgeo = root.findGroup("Geologi")
        for layers in groupgeo.children():
            layers.setItemVisibilityChecked(False)
        groupfysik = root.findGroup("Geofysik")
        for layers in groupfysik.children():
            layers.setItemVisibilityChecked(False)
        groupkemi = root.findGroup("Kemi")
        for layers in groupkemi.children():
            layers.setItemVisibilityChecked(False)
        grouphydro = root.findGroup("Hydrologi")
        for layers in grouphydro.children():
            layers.setItemVisibilityChecked(False)

        layout = manager.duplicateLayout(layout1, '{}'.format(key))

        l = []
        for count, values in enumerate(musthavelayouts.get(key)):
            l.append(values)
            l[count] = project.mapLayersByName("{}".format(values))[0]
            project.layerTreeRoot().findLayer(l[count].id()).setItemVisibilityChecked(True)


        mapThemesCollection = project.mapThemeCollection()
        mapThemes = mapThemesCollection.mapThemes()

        mapThemeRecord = QgsMapThemeCollection.createThemeFromCurrentState(
            QgsProject.instance().layerTreeRoot(),
            iface.layerTreeView().layerTreeModel()
        )
        mapThemesCollection.insert("{}".format(key), mapThemeRecord)

        layers_to_remove = []
        for layer in project.mapLayers().values():
            if layer.type() == QgsMapLayer.RasterLayer:
                layers_to_remove.append(layer)

        layers_to_remove.append(project.mapLayersByName('{}'.format(key2))[0])
        layers_to_remove.append(project.mapLayersByName("Område-grå")[0])
        layers_to_remove.append(project.mapLayersByName("Per Smed")[0])
        layers_to_remove.append(project.mapLayersByName("Pesticider GEUS")[0])
        layers_to_remove.append(project.mapLayersByName("Seneste Nitrat")[0])
        layers_to_remove.append(project.mapLayersByName("Seneste Sulfat")[0])
        layers_to_remove.append(project.mapLayersByName("Seneste Klorid")[0])
        layers_to_remove.append(project.mapLayersByName("Seneste Jern")[0])
        layers_to_remove.append(project.mapLayersByName("Seneste Arsen")[0])
        layers_to_remove.append(project.mapLayersByName("Seneste Analyse Alder")[0])
        layers_to_remove.append(project.mapLayersByName("Seneste Vandtype")[0])

        lays = QgsProject.instance().mapLayers()  # dictionary

        for layer in lays.values():
            if layer.name() in musthavelayouts[key]:
                pass
            else:
                layers_to_remove.append(project.mapLayersByName("{}".format(layer.name()))[0])

        layout = project.layoutManager().layoutByName("{}".format(key))


        legend = [i for i in layout.items() if isinstance(i, QgsLayoutItemLegend)][0]

        map = [i for i in layout.items() if isinstance(i, QgsLayoutItemMap)][0]
        map.setFollowVisibilityPreset(True)
        map.setFollowVisibilityPresetName("{}".format(key))

        if key == "Projektområde - OSD":
            legend.setTitle("Projektområdet")
        elif key =="Projektområde - Aktive indvindingsboringer":
            legend.setTitle("Indvindingsboringer")
        elif key == "Projektområde - Vandforsyningsanlæg":
            legend.setTitle("Vandforsyningsanlæg")
        elif key =="Projektområde - Aktive Anlæg":
            legend.setTitle("Andet Indvinding")
        elif key == "Projektområde - IOL Grupperet":
            legend.setTitle("IOL - Modelområde")
        elif key == "Projektområde - Per Smed":
            legend.setTitle("Per Smed")
            pic = QgsLayoutItemPicture(layout)
            pic.setMode(QgsLayoutItemPicture.FormatRaster)
            pic.setPicturePath(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Images/persmedrigtig.png")
            pic.attemptMove(QgsLayoutPoint(222.7, 27.2, QgsUnitTypes.LayoutMillimeters))
            pic.attemptResize(QgsLayoutSize(*[74, 130], QgsUnitTypes.LayoutMillimeters))
            layout.addLayoutItem(pic)
        elif key == "Projektområde - Jordartskort":
            legend.setTitle("Jordartskort")
            pic = QgsLayoutItemPicture(layout)
            pic.setMode(QgsLayoutItemPicture.FormatRaster)
            pic.setPicturePath(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Images/jordart.png")
            pic.attemptMove(QgsLayoutPoint(223.7, 38.2, QgsUnitTypes.LayoutMillimeters))
            pic.attemptResize(QgsLayoutSize(*[66.75, 148.6], QgsUnitTypes.LayoutMillimeters))
            layout.addLayoutItem(pic)
        elif key == "Geologi - Boringskvalitet 1-3":
            legend.setTitle("Boringskvalitet 1-3")
        elif key == "Geologi - Boringskvalitet 4-6":
            legend.setTitle("Boringskvalitet 4-6")
        elif key == "Geologi - Boringskvalitet 1-3 - Dybde":
            legend.setTitle("Boringskvalitet 1-3 -*Dybde")
        elif key == "Geologi - Boringskvalitet 4-6 - Dybde":
            legend.setTitle("Boringskvalitet 4-6 -*Dybde")
        elif key == "Geofysik - Modeller":
            legend.setTitle("Geofysisk data")
            pic = QgsLayoutItemPicture(layout)
            pic.setMode(QgsLayoutItemPicture.FormatRaster)
            pic.setPicturePath(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Images/Geofysik.png")
            pic.attemptMove(QgsLayoutPoint(223.7, 38.2, QgsUnitTypes.LayoutMillimeters))
            pic.attemptResize(QgsLayoutSize(*[66.75, 148.6], QgsUnitTypes.LayoutMillimeters))
            layout.addLayoutItem(pic)
        elif key == "Kemi - Pesticider":
            legend.setTitle("Seneste Pesticider")
        elif key == "Kemi - Seneste Nitrat":
            legend.setTitle("Seneste Nitrat [mg/L]")
        elif key == "Kemi - Seneste Sulfat":
            legend.setTitle("Seneste Sulfat [mg/L]")
        elif key == "Kemi - Seneste Klorid":
            legend.setTitle("Seneste Klorid [mg/L]")
        elif key == "Kemi - Seneste Jern":
            legend.setTitle("Seneste Jern [μg/L]")
        elif key == "Kemi - Seneste Arsen":
            legend.setTitle("Seneste Arsen [μg/L]")
        elif key == "Kemi - Seneste Analyse Alder":
            legend.setTitle("Seneste Analyse *Alder [år]")
        elif key == "Kemi - Seneste Vandtype":
            legend.setTitle("Seneste Vandtype")
        elif key == "Hydrologi - Pejlinger GPS":
            legend.setTitle("Pejlinger - GPS")
        elif key == "Hydrologi - Pejlinger Status på filtersætning":
            legend.setTitle("Pejlinger - Status*på filtersætning")
        elif key == "Hydrologi - Pejlinger Antal pr. boringsindtag":
            legend.setTitle("Pejlinger - Antal*pr. boringsindtag")
        elif key == "Hydrologi - Sænkningsydelsesdata Status på filtersætning":
            legend.setTitle("Sænkningsydelsesdata - *Status på filtersætning")
        elif key == "Hydrologi - Sænkningsydelsesdata Kvalitet":
            legend.setTitle("Sænkningsydelsesdata - *Kvalitet")
        elif key == "Hydrologi - Sænkningsydelsesdata GPS":
            legend.setTitle("Sænkningsydelsesdata - *GPS")
        elif key == "Projektområde - Filtersætning i FOHM":
            legend.setTitle("Filtersætning i FOHM")
        elif key == "Projektområde - Anlæg":
            legend.setTitle("Andet indvinding")

        legend.setAutoUpdateModel(True)
        legend.setAutoUpdateModel(False)



        for l in layers_to_remove:
            legend.model().rootGroup().removeLayer(l)
            root_group = legend.model().rootGroup()  # QgsLayerTree object

            group = root_group.findGroup("Generelt")  # get reference to group (QgsLayerTreeGroup object)
            group.removeLayer(l)
            group2 = root_group.findGroup("Kemi")  # get reference to group (QgsLayerTreeGroup object)
            group2.removeLayer(l)
            group3 = root_group.findGroup("Geologi")  # get reference to group (QgsLayerTreeGroup object)
            group3.removeLayer(l)
            group4 = root_group.findGroup("Hydrologi")  # get reference to group (QgsLayerTreeGroup object)
            group4.removeLayer(l)
            group5 = root_group.findGroup("Stilarter")  # get reference to group (QgsLayerTreeGroup object)
            group5.removeLayer(l)
            group6 = root_group.findGroup("Geofysik")  # get reference to group (QgsLayerTreeGroup object)
            group6.removeLayer(l)


        hideable = ["Anlæg Aktive", "Vandværker Aktive", "Indvindingsboringer", "Indvindingsoplande Grupperet", "Boringskvalitet 1-3",
                    "Boringskvalitet 4-6", "Boringskvalitet 1-3 - Dybde", "Boringskvalitet 4-6 - Dybde", "Pesticider GEUS", "Seneste Vandtype",
                    "Pejlinger - GPS", "Pejlinger - Status på filtersætning", "Pejlinger - Antal pr. boringsindtag",
                    "Sænkningsydelsesdata", "Sænkningsydelsesdata - Kvalitet", "Sænkningsydelsesdata - GPS", "Filtersætning i FOHM", "Geofysisk data", "SenesteNitratTemplate", "SenesteSulfatTemplate", "SenesteVandtypeTemplate",
                    "SenesteKloridTemplate", "SenesteStrontiumTemplate", "SenesteFluoridTemplate", "SenestePesticiderTemplate", "SenesteJernTemplate", "SenesteAnalyseAlder" ,"MagasinTemplate", "SenesteArsenTemplate", "SenesteAnalyseAlder", "MagasinTemplate2"]





        for l in project.mapLayers().values():
            if l.name() in hideable:
                QgsLegendRenderer.setNodeLegendStyle(
                    project.layerTreeRoot().findLayer(l.id()), QgsLegendStyle.Hidden)
                layout.refresh()

        for key, l in enumerate(legend.model().rootGroup().children()):
            QgsLegendRenderer.setNodeLegendStyle(legend.model().rootGroup().children()[key], QgsLegendStyle.Hidden)

        legend.updateLegend()  # Update the QgsLayerTreeModel

        layout.refresh()

    self.pushButton_second.setText("Færdig ✓")
    self.repaint()

def select_directory(self):
    try:
        plugin_path = QFileDialog.getExistingDirectory(
            self, caption='Vælg sti hvor figurer skal gemmes')
        self.outputdirectory.setText(plugin_path)
    except:
        pass

def select_directory2(self):
    try:
        plugin_path = QFileDialog.getExistingDirectory(
            self, caption='Vælg sti hvor filer skal gemmes')
        self.lokalefilersti.setText(plugin_path)
    except:
        pass

def select_directory3(self):
    try:
        plugin_path = QFileDialog.getExistingDirectory(
            self, caption='Vælg sti hvor jupiter_extract skal gemmes')
        self.outputdirectory_2.setText(plugin_path)
    except:
        pass

def select_directory4(self):
    if True:
        self.pushButton_fifth_2.setText("Kører")
        self.pushButton_fifth_2.setEnabled(False)
        self.repaint()
    try:
        plugin_path ,_filter = QFileDialog.getOpenFileName(self, caption='Vælg excel-arket med udtræk (jupiter_extract)')
        self.lineEdit.setText(str(plugin_path))
        xls = pd.ExcelFile("{}".format(self.lineEdit.text()))
        df = pd.read_excel(xls, 'Arbejdsdata')
        rows = df['magasin'].unique().tolist()
        rows = [str(item).replace('nan', "Ukendt magasin") for item in rows]
        rows.sort()
        rækker = len(rows)
        self.tableWidget.setRowCount(rækker)
        self.tableWidget.setVerticalHeaderLabels(rows)
        self.pushButton_fifth_2.setText("Færdig ✓")
        self.repaint()
    except:
        pass

def opstart_third(self):

    if len(self.outputdirectory.text()) > 0:
        manager = QgsProject.instance().layoutManager()
        if len(manager.printLayouts()) > 0:
            if True:
                self.pushButton_third.setText("Kører")
                self.pushButton_third.setEnabled(False)
                self.repaint()

            global imagePath
            imagePath = self.outputdirectory.text()
            dpi = self.dpibox.value()
            project = QgsProject.instance()

            class FigureWorker(QgsTask):
                def __init__(self, Dialog):
                    QgsTask.__init__(self)
                    self.Dialog = Dialog

                def run(self):
                    for layout in manager.printLayouts():
                        if layout.name() == "Geologi - Boringskvalitet 1-3" or layout.name() == "Geologi - Boringskvalitet 4-6":
                            pass
                        else:
                            legend = [i for i in layout.items() if isinstance(i, QgsLayoutItemLegend)][0]
                            legend.setLegendFilterByMapEnabled(True)
                            legend.setLegendFilterByMapEnabled(False)
                            layout.refresh()
                        exporter = QgsLayoutExporter(layout)
                        settings = exporter.ImageExportSettings()
                        settings.dpi = dpi
                        exporter.exportToImage(
                            '{}/{}.png'.format(imagePath, layout.name()), settings)
                    return True

                def finished(self, result):
                    try:
                        self.result_layer.moveToThread(QCoreApplication.instance().thread())
                        project.addMapLayer(self.result_layer)
                    except:
                        pass
                    self.Dialog.pushButton_third.setText("Færdig ✓")
                    self.Dialog.repaint()
                    QApplication.processEvents()
                    os.startfile(str(imagePath))

            globals()['taskfiguremaker'] = FigureWorker(self)
            QgsApplication.taskManager().addTask(globals()['taskfiguremaker'])
        else:
            QMessageBox.warning(None, 'Fejl', "Ingen figurer i dette projekt.")

    else:
        QMessageBox.warning(None, 'Fejl', "Vælg et output directory for dine figurer.")