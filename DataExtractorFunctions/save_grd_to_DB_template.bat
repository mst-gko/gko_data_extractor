@echo off
setlocal enabledelayedexpansion

REM author: Simon Makwarth <simak@mst.dk>
REM encoding: UTF-8 using Windows (CR LF)
REM descr: to run this script the database credentials must be stored as an environmental variable
REM note: modified by Frederikke Hansen for KS Flader programme

REM set parameters
SET PGCLIENTENCODING=UTF8
SET PGPASSWORD=merkel30rblaser
SET pg_hostname=10.33.131.50
SET pg_username=grukosadmin
SET pg_port=5432
SET dbname=grukos
SET schema_name=temp_grukosreader
SET file_ext=grd

REM set paths
SET path_src=__path__
SET path_files=%~dp0
SET path_grd=%path_files%..\tempfiles

REM Make sure temp folder is empty
del /s /q %path_grd%

REM copy .grd files to temp folder
xcopy /s %path_src% %path_grd%

REM rename grid files
cd %path_files%
python new_tablenames.py %path_grd% %file_ext%

REM loop through grid files and create raster in database, change directory in first line to the one containing grd files
cd %path_grd%
FOR %%f IN (*.grd) DO (
	set full_path=%path_grd%\%%f
	ECHO %full_path%
	set table_name=%%~nf
	echo Exporting !full_path! to !dbname!.!schema_name!.!table_name!...
	cd C:\OSGeo4W\bin
	raster2pgsql -I -C -s 25832 -t 200x200 !full_path! !schema_name!.!table_name! | PGPASSWORD=!PGPASSWORD! psql -h !pg_hostname! -p !pg_port! -U !pg_username! -d !dbname!
)



