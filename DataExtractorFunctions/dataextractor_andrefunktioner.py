# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import (
    QgsFields,
    QgsField,
    QgsFeature,
QgsLayerTreeGroup,
    QgsGeometry,
    QgsFeatureRequest,
    QgsProject,
QgsSimpleMarkerSymbolLayerBase,
    QgsRaster,
    QgsTask,
    QgsPrintLayout,
    QgsReadWriteContext,
    QgsRasterFileWriter,
    QgsMapLayer,
    QgsLayoutItemLegend,
    QgsLayoutItemMap,
    QgsLegendRenderer,
    QgsExpression,
    QgsLegendStyle,
    QgsLayoutExporter,
    QgsLayoutItemPicture,
    QgsLayoutPoint,
    QgsLayoutSize,
    QgsUnitTypes,
    QgsRasterBandStats,
    QgsCategorizedSymbolRenderer,
    QgsRendererCategory,
    QgsSymbol,
    QgsCoordinateTransformContext,
QgsRuleBasedRenderer,
    QgsRandomColorRamp,
    QgsMapThemeCollection,
    QgsLayerDefinition,
    QgsPointXY,
    QgsApplication,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsExpressionContextScope,
    QgsCoordinateReferenceSystem,
    QgsProcessingFeatureSourceDefinition,
    QgsRectangle,
    QgsTaskManager,
    QgsLayout,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsRasterLayer,
    QgsWkbTypes,
    QgsDataSourceUri
)
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()
import pyodbc


from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass


__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *
from ..doDataExtract import *


def outFile_andet(self):
    if len(self.comboBox.currentText()) > 0:
        if True:
            self.pushButton_Andet.setText("Kører")
            self.pushButton_Andet.setEnabled(False)
            self.repaint()

        project = QgsProject.instance()
        key = self.comboBox.currentText()
        layer = project.mapLayersByName('{}'.format(key))[0]
        for kk, feature in enumerate(layer.getFeatures()):
            if kk == 0:
                geom2 = feature.geometry()
            else:
                geom2 = geom2.combine(feature.geometry())
        crstrue = int("".join(
            filter(str.isdigit, str(layer.crs().authid()))))  # int(filter(str.isdigit, str(layer.crs().authid())))

        if self.comboBox_2.currentText() == 'Matrikler':
            class MatrikelArbejde(QgsTask):
                def __init__(self, desc, Dialog):
                    self.desc = desc
                    self.result_layer = None
                    self.result_layer2 = None
                    self.Dialog = Dialog
                    QgsTask.__init__(self, self.desc)

                def run(self):

                    Matri = Matrikler(wkt=geom2.asWkt(), crs=crstrue)
                    sql_lois = Matri.fetch_sql()
                    if self.isCanceled():
                        return False
                    db_msois = Database_matrikel(database_name='MSSQLOIS', usr='reader')
                    if self.isCanceled():
                        return False
                    df = db_msois.import_mssql_to_df(sql_lois)
                    if self.isCanceled():
                        return False


                    df_unique_owners = df.loc[:,
                                       ['EJER_NAVN', 'EJER_CONAVN', 'EJER_POSTADR', 'EJER_ADR', 'CVR', 'NAVN_CVR', 'TLF_CVR']].drop_duplicates()

                    df.to_csv('C:/Temp/matrikeltest.csv', index=False, encoding='utf-8')

                    df_unique_owners.to_csv('C:/Temp/matrikeltestgrupperettileboks.csv', index=False, encoding='utf-8')

                    path = 'file:///' + 'C:/Temp/matrikeltest.csv' + '?encoding=%s&delimiter=%s&wktField=%s&crs=%s' % (
                        "utf-8", ",", "geom_wkt", "epsg:25832")

                    path2 = 'file:///' + 'C:/Temp/matrikeltestgrupperettileboks.csv'
                    if self.isCanceled():
                        return False
                    mylayer = QgsVectorLayer(path, "Matrikeltester", "delimitedtext")

                    mylayer2 = QgsVectorLayer(path2, "Matrikeltestergrupperettileboks", "delimitedtext")

                    a = processing.run("native:extractbyexpression",
                                       {'INPUT': mylayer,
                                        'EXPRESSION': 'rownumber is not null', 'OUTPUT': 'memory:'})

                    a["OUTPUT"].setName("Matrikler - {}".format(key))

                    self.result_layer = a["OUTPUT"]
                    if self.isCanceled():
                        return False
                    a2 = processing.run("native:extractbyexpression",
                                        {'INPUT': mylayer2,
                                         'EXPRESSION': "coalesce('EJER_NAVN', 'EJER_CONAVN', 'EJER_POSTADR', 'EJER_ADR') is not null",
                                         'OUTPUT': 'memory:'})

                    a2["OUTPUT"].setName("Matrikler_grupperet_til_eboks - {}".format(key))
                    self.result_layer2 = a2["OUTPUT"]

                    del (mylayer)
                    del (path)
                    del (mylayer2)
                    del (a2)
                    del (path2)

                    return True

                def finished(self, result):
                    if self.isCanceled():
                        QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                    else:
                        if self.result_layer is None or self.result_layer.featureCount() == 0:
                            QMessageBox.warning(None, 'Meddelse', "Intet data for dette område.")
                        else:
                            self.result_layer.moveToThread(QCoreApplication.instance().thread())
                            self.result_layer2.moveToThread(QCoreApplication.instance().thread())
                            project.addMapLayer(self.result_layer)
                            project.addMapLayer(self.result_layer2)
                            os.remove('C:/Temp/matrikeltest.csv')
                            os.remove('C:/Temp/matrikeltestgrupperettileboks.csv')

                    self.Dialog.pushButton_Andet.setText("Eksportér til QGIS")
                    self.Dialog.pushButton_Andet.setEnabled(True)
                    self.Dialog.repaint()
                    QApplication.processEvents()

            globals()['matr'] = MatrikelArbejde("Indhenter matrikler fra OIS...",self)
            QgsApplication.taskManager().addTask(globals()['matr'])

        elif self.comboBox_2.currentText() == "Vandindvinding (Tabel)":

                        if True:
                            self.pushButton_GKOPLOTTER_Hydro.setText("Kører")
                            self.pushButton_GKOPLOTTER_Hydro.setEnabled(False)
                            self.repaint()
                            QApplication.processEvents()

                        project = QgsProject.instance()
                        key = self.comboBox.currentText()
                        layer = project.mapLayersByName('{}'.format(key))[0]

                       # if project.mapLayersByName('Indvindingsoversigt'):
                        #    project.removeMapLayer(project.mapLayersByName('Indvindingsoversigt')[0])


                        for kk, feature in enumerate(layer.getFeatures()):
                            if kk == 0:
                                geom = feature.geometry()
                            else:
                                geom = geom.combine(feature.geometry())

                        crstrue = int("".join(filter(str.isdigit, str(layer.crs().authid()))))

                        class Indvinding(QgsTask):
                                def __init__(self, desc, Dialog):
                                    self.desc = desc
                                    self.Dialog = Dialog
                                    self.result_layer = None

                                    QgsTask.__init__(self, self.desc)

                                def run(self):

                                    data = Indvindingsoversigt(geom.asWkt(), crstrue).fetch_indvindingsoversigt()

                                    data = data.sort_values("anlægsnavn")

                                    data.to_csv('C:/Temp/indvindingsoversigt.csv', index=False, encoding='utf-8')

                                    path = 'file:///' + 'C:/Temp/indvindingsoversigt.csv'

                                    mylayer = QgsVectorLayer(path, "Indvindingsoversigt", "delimitedtext")
                                    mylayer = processing.run("native:refactorfields",
                                                   {'INPUT': 'delimitedtext://file:///C:/Temp/indvindingsoversigt.csv',
                                                    'FIELDS_MAPPING': [
                                                        {'expression': '"anlægsid"', 'length': 0, 'name': 'anlægsid',
                                                         'precision': 0, 'sub_type': 0, 'type': 2,
                                                         'type_name': 'integer'},
                                                        {'expression': '"anlægsnavn"', 'length': 0,
                                                         'name': 'anlægsnavn', 'precision': 0, 'sub_type': 0,
                                                         'type': 10, 'type_name': 'text'},
                                                        {'expression': '"indvindingstype"', 'length': 0,
                                                         'name': 'indvindingstype', 'precision': 0, 'sub_type': 0,
                                                         'type': 10, 'type_name': 'text'},
                                                        {'expression': '"indvindingsmængde_år"', 'length': 0,
                                                         'name': 'indvindingsmængde_år', 'precision': 0, 'sub_type': 0,
                                                         'type': 2, 'type_name': 'integer'},
                                                        {'expression': '"indvindingsmængde"', 'length': 0,
                                                         'name': 'indvindingsmængde', 'precision': 0, 'sub_type': 0,
                                                         'type': 2, 'type_name': 'integer'},
                                                        {'expression': '"tilladelse"', 'length': 0,
                                                         'name': 'tilladelse', 'precision': 0, 'sub_type': 0, 'type': 6,
                                                         'type_name': 'double precision'},
                                                        {'expression': '"tilladelse_sludato"', 'length': 0,
                                                         'name': 'tilladelse_sludato', 'precision': 0, 'sub_type': 0,
                                                         'type': 6, 'type_name': 'double precision'},
                                                        {'expression': '"dgu_intake"', 'length': 0,
                                                         'name': 'dgu_intake', 'precision': 0, 'sub_type': 0,
                                                         'type': 10, 'type_name': 'text'},
                                                        {'expression': '"dgu_nummer"', 'length': 0,
                                                         'name': 'dgu_nummer', 'precision': 0, 'sub_type': 0,
                                                         'type': 10, 'type_name': 'text'},
                                                        {'expression': '"dgu_nummer_u_mellemrum"', 'length': 0,
                                                         'name': 'dgu_nummer_u_mellemrum', 'precision': 0,
                                                         'sub_type': 0, 'type': 10, 'type_name': 'text'},
                                                        {'expression': '"boringsformål"', 'length': 0,
                                                         'name': 'boringsformål', 'precision': 0,
                                                         'sub_type': 0, 'type': 10, 'type_name': 'text'},
                                                        {'expression': '"boringsanvendelse"', 'length': 0,
                                                         'name': 'boringsanvendelse', 'precision': 0,
                                                         'sub_type': 0, 'type': 10, 'type_name': 'text'},
                                                        {'expression': '"fohm_layer"', 'length': 0,
                                                         'name': 'fohm_layer', 'precision': 0, 'sub_type': 0,
                                                         'type': 10, 'type_name': 'text'}],
                                                    'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]
                                    mylayer.setName("Indvindingsoversigt")
                                    self.result_layer = mylayer


                                def finished(self, result):
                                    if self.isCanceled():
                                        QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                                    else:
                                        self.result_layer.moveToThread(QCoreApplication.instance().thread())
                                        project.addMapLayer(self.result_layer)
                                        try:
                                            os.remove('C:/Temp/indvindingsoversigt.csv')
                                        except:
                                            pass

                                    self.Dialog.pushButton_Andet.setText("Eksportér til QGIS")
                                    self.Dialog.pushButton_Andet.setEnabled(True)
                                    self.Dialog.repaint()

                        globals()['taskindvinding'] = Indvinding("Indhenter indvindingsdata for området...", self)
                        QgsApplication.taskManager().addTask(globals()['taskindvinding'])

        self.repaint()
        QApplication.processEvents()
    else:
        QMessageBox.warning(None, 'Fejl', "Intet input (områdepolygon).")