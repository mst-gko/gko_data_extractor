# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import (
    QgsFields,
    QgsField,
    QgsFeature,
QgsLayerTreeGroup,
    QgsGeometry,
    QgsFeatureRequest,
    QgsProject,
QgsSimpleMarkerSymbolLayerBase,
QgsLayerNotesUtils,
    QgsRaster,
    QgsTask,
    QgsPrintLayout,
    QgsReadWriteContext,
    QgsRasterFileWriter,
    QgsMapLayer,
    QgsLayoutItemLegend,
    QgsLayoutItemMap,
    QgsLegendRenderer,
    QgsExpression,
    QgsLegendStyle,
    QgsLayoutExporter,
    QgsLayoutItemPicture,
    QgsLayoutPoint,
    QgsLayoutSize,
    QgsUnitTypes,
    QgsRasterBandStats,
    QgsCategorizedSymbolRenderer,
    QgsRendererCategory,
    QgsSymbol,
    QgsCoordinateTransformContext,
QgsRuleBasedRenderer,
    QgsRandomColorRamp,
    QgsMapThemeCollection,
    QgsLayerDefinition,
    QgsPointXY,
    QgsRendererRange,
    QgsApplication,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsExpressionContextScope,
    QgsCoordinateReferenceSystem,
    QgsProcessingFeatureSourceDefinition,
    QgsRectangle,
    QgsTaskManager,
    QgsGraduatedSymbolRenderer,
    QgsLayout,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsRasterLayer,
    QgsWkbTypes,
    QgsDataSourceUri
)
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()
import pyodbc

from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass


__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *




def trin1_sårbarhed(self):
        try:
            if True:
                self.pushButton_srbarhed_trin1.setText("Kører")
                self.pushButton_srbarhed_trin1.setEnabled(False)
                self.repaint()
            project = QgsProject.instance()

            class SårbarhedTrin1Arbejde(QgsTask):
                def __init__(self, desc, Dialog):
                    self.result_layer = None
                    self.Dialog = Dialog
                    self.desc = desc
                    QgsTask.__init__(self, self.desc)

                def run(self):

                    maglay = project.mapLayersByName('{}'.format(self.Dialog.comboBox_magpoly.currentText()))[0]
                    lerlay = project.mapLayersByName('{}'.format(self.Dialog.comboBox_lerlag.currentText()))[0]

                    processed_layer = processing.run("qgis:executesql", {
                        'INPUT_DATASOURCES': [maglay, lerlay],
                        'INPUT_QUERY': f'SELECT "{self.Dialog.comboBox_lerlag.currentText()}".*, "{self.Dialog.comboBox_magpoly.currentText()}".{self.Dialog.comboBox_magpoly_id.currentText()}\nFROM "{self.Dialog.comboBox_magpoly.currentText()}"\nINNER JOIN  "{self.Dialog.comboBox_lerlag.currentText()}" on "{self.Dialog.comboBox_magpoly.currentText()}".{self.Dialog.comboBox_magpoly_id.currentText()}= "{self.Dialog.comboBox_lerlag.currentText()}".{self.Dialog.comboBox_lerlag_id.currentText()}\nWHERE st_within("{self.Dialog.comboBox_lerlag.currentText()}".geometry, "{self.Dialog.comboBox_magpoly.currentText()}".geometry)\n',
                        'INPUT_UID_FIELD': '', 'INPUT_GEOMETRY_FIELD': '', 'INPUT_GEOMETRY_TYPE': 2, 'INPUT_GEOMETRY_CRS': None,
                        'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

                    symbols = (('Stor sårbarhed', -9999, 5.0, QColor(255, 0, 0)),
                               ('Nogen sårbarhed', 5.0, 15.0, QColor(255, 255, 0)),
                               ('Lille sårbarhed', 15.0, 9999, QColor(0, 255, 0)))

                    ranges = []
                    for label, lower, upper, color in symbols:
                        sym = QgsSymbol.defaultSymbol(processed_layer.geometryType())
                        sym.setColor(QColor(color))
                        rng = QgsRendererRange(lower, upper, sym, label)
                        ranges.append(rng)

                    field = "tykkelse"
                    renderer = QgsGraduatedSymbolRenderer(field, ranges)
                    processed_layer.setRenderer(renderer)
                    processed_layer.triggerRepaint()
                    processed_layer.setName("Sårbarhedsvurdering")
                    self.result_layer = processed_layer

                    project.layerTreeRoot().findLayer(maglay.id()).setItemVisibilityChecked(False)
                    project.layerTreeRoot().findLayer(lerlay.id()).setItemVisibilityChecked(False)

                def finished(self, result):
                    if self.isCanceled():
                        QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                    else:
                        if self.result_layer is None or self.result_layer.featureCount() == 0:
                            QMessageBox.warning(None, 'Meddelse', "Intet data for dette område.")
                        else:
                            self.result_layer.moveToThread(QCoreApplication.instance().thread())

                            project.addMapLayer(self.result_layer)

                    self.Dialog.pushButton_srbarhed_trin1.setText("Eksportér til QGIS")
                    self.Dialog.pushButton_srbarhed_trin1.setEnabled(True)
                    self.Dialog.repaint()
                    QApplication.processEvents()

            globals()['tasksårbarhedtrin1'] = SårbarhedTrin1Arbejde("Foretager sårbarhedsvurdering...", self)
            QgsApplication.taskManager().addTask(globals()['tasksårbarhedtrin1'])
        except:
            QMessageBox.warning(None, 'Fejl', "Sårbarhedsvurdering mislykkedes.")


        project = QgsProject.instance()

        class SårbarhedTrin1Arbejde(QgsTask):
            def __init__(self, desc, Dialog):
                QgsTask.__init__(self)
                self.result_layer = None
                self.Dialog = Dialog
                self.desc = desc

            def run(self):

                maglay = QgsVectorLayer(str(pluginpathmag[0]), 'maglay')
                lerlay = QgsVectorLayer(str(pluginpathler[0]), 'lerlay')
                project.addMapLayer(maglay)
                project.addMapLayer(lerlay)


                processed_layer = processing.run("qgis:executesql", {
                    'INPUT_DATASOURCES': [maglay, lerlay],
                    'INPUT_QUERY': 'SELECT lerlay.*, maglay.magasinlag\nFROM maglay\nINNER JOIN  lerlay on maglay.magasinlag= lerlay.magasin\nWHERE st_within(lerlay.geometry, maglay.geometry)\n',
                    'INPUT_UID_FIELD': '', 'INPUT_GEOMETRY_FIELD': '', 'INPUT_GEOMETRY_TYPE': 2,
                    'INPUT_GEOMETRY_CRS': None,
                    'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

                symbols = (('Stor sårbarhed', -9999, 5.0, QColor(255, 0, 0)),
                           ('Nogen sårbarhed', 5.0, 15.0, QColor(255, 255, 0)),
                           ('Lille sårbarhed', 15.0, 9999, QColor(0, 255, 0)))

                ranges = []
                for label, lower, upper, color in symbols:
                    sym = QgsSymbol.defaultSymbol(processed_layer.geometryType())
                    sym.setColor(QColor(color))
                    rng = QgsRendererRange(lower, upper, sym, label)
                    ranges.append(rng)

                field = "tykkelse"
                renderer = QgsGraduatedSymbolRenderer(field, ranges)
                processed_layer.setRenderer(renderer)
                processed_layer.triggerRepaint()
                processed_layer.setName("Sårbarhedsvurdering")
                self.result_layer = processed_layer
                project.removeMapLayer(maglay)
                project.removeMapLayer(lerlay)

            def finished(self, result):
                if self.isCanceled():
                    QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                else:
                    if self.result_layer is None or self.result_layer.featureCount() == 0:
                        QMessageBox.warning(None, 'Meddelse', "Intet data for dette område.")
                    else:
                        self.result_layer.moveToThread(QCoreApplication.instance().thread())

                        project.addMapLayer(self.result_layer)

                self.Dialog.pushButton_srbarhed_trin1.setText("Eksportér til QGIS")
                self.Dialog.pushButton_srbarhed_trin1.setEnabled(True)
                self.Dialog.repaint()
                QApplication.processEvents()

        globals()['tasksårbarhedtrin1'] = SårbarhedTrin1Arbejde("Foretager sårbarhedsvurdering...", self)
        QgsApplication.taskManager().addTask(globals()['tasksårbarhedtrin1'])

def trin2_sårbarhed(self):
    if len(self.comboBox_trin2_vurdering.currentText())>0 and len(self.comboBox_trin2_vurdering_id.currentText())>0  and len(self.comboBox_trin2_vandtype.currentText())>0 and len(self.comboBox_trin2_vandtype_id.currentText())>0 and len(self.comboBox_trin2_vandtype_wt.currentText())>0:
        #try:
            if True:
                self.pushButton_trin2.setText("Kører")
                self.pushButton_trin2.setEnabled(False)
                self.repaint()
            project = QgsProject.instance()

            class SårbarhedTrin2Arbejde(QgsTask):
                def __init__(self, desc, Dialog):
                    self.result_layer = None
                    self.Dialog = Dialog
                    self.desc = desc
                    QgsTask.__init__(self, self.desc)

                def run(self):

                    sårbarhedslag = project.mapLayersByName('{}'.format(self.Dialog.comboBox_trin2_vurdering.currentText()))[0]
                    sårbarhedslag_id = self.Dialog.comboBox_trin2_vurdering_id.currentText()
                    vandtypelag = project.mapLayersByName('{}'.format(self.Dialog.comboBox_trin2_vandtype.currentText()))[0]
                    vandtypelag_id = self.Dialog.comboBox_trin2_vandtype_id.currentText()
                    vandtypelag_wt = self.Dialog.comboBox_trin2_vandtype_wt.currentText()


                    first_step = processing.run("native:joinbynearest", {'INPUT': vandtypelag,
                                                            'INPUT_2': sårbarhedslag,
                                                            'FIELDS_TO_COPY': ['tykkelse', '{}'.format(sårbarhedslag_id)],
                                                            'DISCARD_NONMATCHING': False, 'PREFIX': 'sv_',
                                                            'NEIGHBORS': 1, 'MAX_DISTANCE': 100,
                                                            'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

                    expression =  QgsExpression("{} = sv_{}".format(vandtypelag_id, sårbarhedslag_id))
                    second_step = processing.run("native:extractbyexpression", {
                        'INPUT': first_step,
                        'EXPRESSION': "{} = sv_{}".format(vandtypelag_id, sårbarhedslag_id), 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]


                    third_step = processing.run("native:fieldcalculator", {
                        'INPUT': second_step,
                        'FIELD_NAME': 'Trin2', 'FIELD_TYPE': 6, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
                        'FORMULA': 'case\r\n            '
                                   'when  "{}"  = \'A\' and sv_tykkelse < 5 then true\r\n            '
                                   'when "{}" = \'B\' and sv_tykkelse < 5 then true\r\n            '
                                   'when "{}" = \'AB\' and sv_tykkelse < 5 then true\r\n            '
                                   'when "{}" = \'X\' and sv_tykkelse < 5 then true\r\n            '
                                   'when "{}" = \'Y\' and sv_tykkelse between 5 and 15 then true\r\n            '
                                   'when "{}" = \'C1\' and sv_tykkelse > 5 then true\r\n            '
                                   'when "{}" = \'C2\' and sv_tykkelse between 5 and 15 then true\r\n            '
                                   'when "{}" = \'D\' and sv_tykkelse > 15 then true\r\n            '
                                   'else false\r\n\t\t\tend\r\n'.format(vandtypelag_wt, vandtypelag_wt, vandtypelag_wt, vandtypelag_wt, vandtypelag_wt,  vandtypelag_wt, vandtypelag_wt, vandtypelag_wt),
                        'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

                    symbol = QgsSymbol.defaultSymbol(third_step.geometryType())
                    renderer = QgsRuleBasedRenderer(symbol)

                    rules = [['True', """"Trin2" = True""", QgsSimpleMarkerSymbolLayerBase.Circle, 'Green'],
                             ['False', """"Trin2" = False""", QgsSimpleMarkerSymbolLayerBase.Circle, 'Red']]

                    def rule_based_symbology(layer, renderer, label, expression, symbol, shape, color):
                        root_rule = renderer.rootRule()
                        rule = root_rule.children()[0].clone()
                        rule.setLabel(label)
                        rule.setFilterExpression(expression)
                        rule.symbol().symbolLayer(0).setShape(shape)
                        rule.symbol().setColor(QColor(color))
                        root_rule.appendChild(rule)
                        layer.setRenderer(renderer)
                        layer.triggerRepaint()

                    for rule in rules:
                        rule_based_symbology(third_step, renderer, rule[0], rule[1], symbol, rule[2], rule[3])

                    renderer.rootRule().removeChildAt(0)
                    iface.layerTreeView().refreshLayerSymbology(third_step.id())

                    third_step.setName("Sammenhæng - lertykkelse og vandtype")
                    self.result_layer = third_step
                    return True

                def finished(self, result):
                    if self.isCanceled():
                        QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                    else:
                        if self.result_layer is None:
                            QMessageBox.warning(None, 'Meddelse', "Intet data for dette område.")
                        else:
                            self.result_layer.moveToThread(QCoreApplication.instance().thread())
                            project.addMapLayer(self.result_layer)

                    self.Dialog.pushButton_trin2.setText("Eksportér til QGIS")
                    self.Dialog.pushButton_trin2.setEnabled(True)
                    self.Dialog.repaint()
                    QApplication.processEvents()

            globals()['tasksårbarhedtrin2'] = SårbarhedTrin2Arbejde("Foretager sammenligning mellem lertykkelse og vandtype...", self)
            QgsApplication.taskManager().addTask(globals()['tasksårbarhedtrin2'])
        #except:
         #   QMessageBox.warning(None, 'Fejl', "Sårbarhedsvurdering mislykkedes.")

    else:
        QMessageBox.warning(None, 'Fejl', "Tildel korrekte lag og kolonner.")



def redox_ks(self):
    if len(self.comboBox_redocgrnse.currentText()) > 0 and len(self.comboBox_watertype.currentText()) > 0 and len(
            self.comboBox_watertypecolumn.currentText()) > 0 and len(
            self.comboBox_filtertopkolonne.currentText()) > 0:
        # try:
        if True:
            self.pushButton_KSredox.setText("Kører")
            self.pushButton_KSredox.setEnabled(False)
            self.repaint()
        project = QgsProject.instance()

        class RedoxKSArbejde(QgsTask):
            def __init__(self, desc, Dialog):
                self.result_layer = None
                self.Dialog = Dialog
                self.desc = desc
                QgsTask.__init__(self, self.desc)

            def run(self):

                redoxslag = project.mapLayersByName('{}'.format(self.Dialog.comboBox_redocgrnse.currentText()))[0]
                redoxslag_id = self.Dialog.comboBox_redoxkolonne.currentText()
                vandtypelag = project.mapLayersByName('{}'.format(self.Dialog.comboBox_watertype.currentText()))[0]
                vandtypelag_wt = self.Dialog.comboBox_watertypecolumn.currentText()
                vandtypelag_filtertop = self.Dialog.comboBox_filtertopkolonne.currentText()

                if redoxslag.type() == QgsMapLayer.RasterLayer:
                    sampled = processing.run("native:rastersampling", {
                        'INPUT': vandtypelag,
                        'RASTERCOPY': redoxslag,
                        'COLUMN_PREFIX': 'redox_depth', 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

                    calculator = processing.run("native:fieldcalculator", {
                        'INPUT': sampled,
                        'FIELD_NAME': 'redox_ks', 'FIELD_TYPE': 1, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
                        'FORMULA': "CASE\r\n\t"
                                   "WHEN {} > redox_depth1  and {} in ('C1','C2','D', 'Y', 'DY', 'C1Y', 'C2Y') THEN 1\r\n\t"
                                   "WHEN {} < redox_depth1  and {} in ('C1','C2','D', 'Y', 'DY', 'C1Y', 'C2Y') THEN 2\r\n\t"
                                   "WHEN {} < redox_depth1  and {} in ('A','B','X', 'AX', 'BX', 'AB', 'ABX') THEN 3\r\n\t"
                                   "WHEN {} > redox_depth1  and {} in ('A','B','X', 'AX', 'BX', 'AB', 'ABX') THEN 4\r\n"
                                   "END\r\n".format(vandtypelag_filtertop, vandtypelag_wt,
                                                    vandtypelag_filtertop, vandtypelag_wt,
                                                    vandtypelag_filtertop, vandtypelag_wt,
                                                    vandtypelag_filtertop, vandtypelag_wt),
                        'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

                else:

                    sampled = processing.run("native:joinbynearest", {'INPUT': vandtypelag,
                                                                     'INPUT_2': redoxslag,
                                                                     'FIELDS_TO_COPY': ['{}'.format(redoxslag_id)],
                                                                     'DISCARD_NONMATCHING': False, 'PREFIX': 'rd_',
                                                                     'NEIGHBORS': 1, 'MAX_DISTANCE': 100,
                                                                     'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]


                    calculator = processing.run("native:fieldcalculator", {
                        'INPUT': sampled,
                        'FIELD_NAME': 'redox_ks', 'FIELD_TYPE': 1, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
                        'FORMULA': "CASE\r\n\t"
                                   "WHEN {} > rd_{} and {} in ('C1','C2','D') THEN 1\r\n\t"
                                   "WHEN {} < rd_{} and {} in ('C1','C2','D')THEN 2\r\n\t"
                                   "WHEN {} < rd_{} and {} in ('A','B','X', 'Y') THEN 3\r\n\t"
                                   "WHEN {} > rd_{} and {} in ('A','B','X', 'Y') THEN 4\r\n"
                                   "END\r\n".format(vandtypelag_filtertop, redoxslag_id, vandtypelag_wt,
                                                    vandtypelag_filtertop, redoxslag_id, vandtypelag_wt,
                                                    vandtypelag_filtertop, redoxslag_id, vandtypelag_wt,
                                                    vandtypelag_filtertop, redoxslag_id, vandtypelag_wt),
                        'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

                symbol = QgsSymbol.defaultSymbol(calculator.geometryType())
                renderer = QgsRuleBasedRenderer(symbol)

                rules = [['1', """"redox_ks" = 1""", QgsSimpleMarkerSymbolLayerBase.Triangle, 'Green'],
                         ['2', """"redox_ks" = 2""", QgsSimpleMarkerSymbolLayerBase.Triangle, 'Red'],
                         ['3', """"redox_ks" = 3""", QgsSimpleMarkerSymbolLayerBase.Circle, 'Green'],
                         ['4', """"redox_ks" = 4""", QgsSimpleMarkerSymbolLayerBase.Circle, 'Red']]

                def rule_based_symbology(layer, renderer, label, expression, symbol, shape, color):
                    root_rule = renderer.rootRule()
                    rule = root_rule.children()[0].clone()
                    rule.setLabel(label)
                    rule.setFilterExpression(expression)
                    rule.symbol().symbolLayer(0).setShape(shape)
                    rule.symbol().setColor(QColor(color))
                    root_rule.appendChild(rule)
                    layer.setRenderer(renderer)
                    layer.triggerRepaint()

                for rule in rules:
                    rule_based_symbology(calculator, renderer, rule[0], rule[1], symbol, rule[2], rule[3])

                renderer.rootRule().removeChildAt(0)
                iface.layerTreeView().refreshLayerSymbology(calculator.id())
                QgsLayerNotesUtils.setLayerNotes(calculator, "Kategori 1: Filtertop > redox_dybde og vandtype er C1, C2 eller D \n"
                                         "Kategori 2: Filtertop < redox_dybde og vandtype er C1, C2 eller D \n"
                                         "Kategori 3: Filtertop < redox_dybde og vandtype er A, B, X eller Y \n"
                                         "Kategori 4: Filtertop > redox_dybde og vandtype er A, B, X eller Y")


                QgsProject.instance().layerTreeRoot().findLayer(redoxslag.id()).setItemVisibilityChecked(False)
                QgsProject.instance().layerTreeRoot().findLayer(vandtypelag.id()).setItemVisibilityChecked(False)

                calculator.setName("Redox KS")
                self.result_layer = calculator
                return True

            def finished(self, result):
                if self.isCanceled():
                    QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                else:
                    if self.result_layer is None:
                        QMessageBox.warning(None, 'Meddelse', "Intet data for dette område.")
                    else:
                        self.result_layer.moveToThread(QCoreApplication.instance().thread())
                        project.addMapLayer(self.result_layer)

                self.Dialog.pushButton_KSredox.setText("Eksportér til QGIS")
                self.Dialog.pushButton_KSredox.setEnabled(True)
                self.Dialog.repaint()
                QApplication.processEvents()

        globals()['taskredoxks'] = RedoxKSArbejde(
            "Foretager kvalitetssikring af redoxgrænsen med vandtyper...", self)
        QgsApplication.taskManager().addTask(globals()['taskredoxks'])
    # except:
    #   QMessageBox.warning(None, 'Fejl', "Sårbarhedsvurdering mislykkedes.")

    else:
        QMessageBox.warning(None, 'Fejl', "Tildel korrekte lag og kolonner.")