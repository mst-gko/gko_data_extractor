# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import (
    QgsFields,
    QgsField,
    QgsFeature,
QgsLayerTreeGroup,
    QgsGeometry,
    QgsFeatureRequest,
    QgsProject,
QgsSimpleMarkerSymbolLayerBase,
    QgsRaster,
    QgsTask,
    QgsPrintLayout,
    QgsReadWriteContext,
    QgsRasterFileWriter,
    QgsMapLayer,
    QgsLayoutItemLegend,
    QgsLayoutItemMap,
    QgsLegendRenderer,
    QgsExpression,
    QgsLegendStyle,
    QgsLayoutExporter,
    QgsLayoutItemPicture,
    QgsLayoutPoint,
    QgsLayoutSize,
    QgsUnitTypes,
    QgsRasterBandStats,
    QgsCategorizedSymbolRenderer,
    QgsRendererCategory,
    QgsSymbol,
    QgsCoordinateTransformContext,
QgsRuleBasedRenderer,
    QgsRandomColorRamp,
    QgsMapThemeCollection,
    QgsLayerDefinition,
    QgsPointXY,
    QgsApplication,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsExpressionContextScope,
    QgsCoordinateReferenceSystem,
    QgsProcessingFeatureSourceDefinition,
    QgsRectangle,
    QgsTaskManager,
    QgsLayout,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsRasterLayer,
    QgsWkbTypes,
    QgsDataSourceUri
)
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()
import pyodbc


from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass


__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *
from ..doDataExtract import *


def outFile_Pejlinger(self):
    if len(self.comboBox.currentText()) > 0:
        print(self)
        if True:
            self.pushButton_Pejlinger.setText("Kører")
            self.pushButton_Pejlinger.setEnabled(False)
            self.repaint()
        project = QgsProject.instance()
        key = self.comboBox.currentText()
        layer = project.mapLayersByName('{}'.format(key))[0]
        for kk, feature in enumerate(layer.getFeatures()):
            if kk == 0:
                geom = feature.geometry()
            else:
                geom = geom.combine(feature.geometry())

        crstrue = int("".join(
            filter(str.isdigit, str(layer.crs().authid()))))  # int(filter(str.isdigit, str(layer.crs().authid())))

        if self.checkBox_6.isChecked():
            self.mindybde = "AND b.drilldepth >= {}".format(self.spinBoxC.value())
        else:
            self.mindybde = ""

        if self.checkBox_2.isChecked():
            self.maksz = 'AND abs(bhq.z_dhm - bhq.z_jup) <= {}'.format(self.doubleSpinBox.value())
        else:
            self.maksz = ""

        if self.checkBox.isChecked():
            self.fjernsløjfede = "AND COALESCE(upper(b.use), 'K') NOT IN ('S')"
        else:
            self.fjernsløjfede = ""

        if self.checkBox_3.isChecked():
            self.kendttop = "AND s.top IS NOT NULL"
        else:
            self.kendttop = ""

        if self.checkBox_4.isChecked():
            self.kendtbund = "AND s.bottom IS NOT NULL"
        else:
            self.kendtbund = ""

        if self.checkBox_5.isChecked():
            self.select = "SELECT DISTINCT ON (wl.guid_intake)"
        else:
            self.select = "SELECT"

        class PejlingArbejde(QgsTask):
            def __init__(self, desc, select, fradato, tildato, mindybde, minantal, maksx, fjernsløjfed, kendttop, kendtbund,
                         Dialog):
                self.desc = desc
                self.select = select
                self.fradato = fradato
                self.tildato = tildato
                self.mindybde = mindybde
                self.minantal = minantal
                self.maksz = maksx
                self.fjernsløjfede = fjernsløjfed
                self.kendttop = kendttop
                self.kendtbund = kendtbund
                self.result_layer = None
                self.Dialog = Dialog
                QgsTask.__init__(self, self.desc)

            def run(self):

                wl = Waterlevel(select=self.select, wkt=geom.asWkt(), crs=crstrue, fra=self.fradato.date().toPyDate(),
                                til=self.tildato.date().toPyDate(), mindybde=self.mindybde,
                                minantal=self.minantal.value(), maksz=self.maksz, fjernsløjfede=self.fjernsløjfede,
                                kendttop=self.kendttop, kendtbund=self.kendtbund)
                if self.isCanceled():
                    return False
                sql = wl.fetch_waterlevel()
                if self.isCanceled():
                    return False
                uri = QgsDataSourceUri()
                db = Database(database='JUPITER')
                uri.setConnection(str(db.host), str(db.port), str(db.database), str(db.usr_read), str(db.pw_read))
                uri.setDataSource('', f'({sql})', 'geom', '', 'rownumber')
                if self.isCanceled():
                    return False
                vlayer = QgsVectorLayer(uri.uri(), "Alle pejlinger - {}".format(key), "postgres")
                a = processing.run("native:extractbyexpression",
                                   {'INPUT': vlayer,
                                    'EXPRESSION': 'rownumber is not null', 'OUTPUT': 'TEMPORARY_OUTPUT'})
                if self.isCanceled():
                    return False
                a["OUTPUT"].setName("Pejlinger - {}".format(key))

                self.result_layer = a["OUTPUT"]
                return True

            def finished(self, result):
                if self.isCanceled():
                    QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                else:
                    if self.result_layer is None:
                        QMessageBox.warning(None, 'Meddelse', "Intet data for dette område.")
                    else:
                        self.result_layer.moveToThread(QCoreApplication.instance().thread())
                        project.addMapLayer(self.result_layer)
                self.Dialog.pushButton_Pejlinger.setText("Eksportér til QGIS")
                self.Dialog.pushButton_Pejlinger.setEnabled(True)
                self.Dialog.repaint()
                QApplication.processEvents()

        globals()['wtask'] = PejlingArbejde(desc="Indhenter pejlinger...", select=self.select, fradato=self.fradato, tildato=self.tildato,
                                            mindybde=self.mindybde, minantal=self.spinBox_2, maksx=self.maksz,
                                            fjernsløjfed=self.fjernsløjfede, kendttop=self.kendttop,
                                            kendtbund=self.kendtbund, Dialog=self)
        QgsApplication.taskManager().addTask(globals()['wtask'])

        self.repaint()
        QApplication.processEvents()

    else:
        QMessageBox.warning(None, 'Fejl', "Intet input (områdepolygon).")