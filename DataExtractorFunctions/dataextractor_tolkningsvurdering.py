# -*- coding: utf-8 -*-
import subprocess
# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os
import time
import sys
import shutil
import configparser
import re
import datetime
from datetime import date
from time import sleep

import math
import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import *
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface

import pandas as pd
import numpy as np
import psycopg2 as pg

today = date.today()

import pyodbc

from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass


__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *
from ..doDataExtract import *

def update_settings(self):
    # Designopdateringer af plugin UI
    if self.comboBox_tolkningsvurdering.currentText() == '1.1: FOHM - enkelt lag' or self.comboBox_tolkningsvurdering.currentText() == '0.1: Fynmodellen - enkelt lag':
        self.groupBox.setVisible(True)

        self.label_18.setVisible(False)
        self.layer_combo.setVisible(False)
        self.label_19.setVisible(False)
        self.layer_combo_2.setVisible(False)
        self.label_22.setVisible(False)
        self.lineEdit_10.setVisible(False)
        self.pushButton_mappe.setVisible(False)
        self.comboBox_area.setVisible(False)
        self.label_23.setVisible(False)

        self.label_24.setVisible(False)
        self.comboBox_lagtype.setVisible(False)
        self.tableWidget_magasin.setVisible(False)

        self.resistivitestboundaries_specific.setVisible(True)

        self.label_21.setVisible(True)
        self.comboBox_wanted.setVisible(True)
    elif self.comboBox_tolkningsvurdering.currentText() == '2.1: Benyt specifikke flader indlæst i QGIS':
        self.groupBox.setVisible(True)

        self.label_18.setVisible(True)
        self.layer_combo.setVisible(True)
        self.label_19.setVisible(True)
        self.layer_combo_2.setVisible(True)
        self.label_22.setVisible(False)
        self.lineEdit_10.setVisible(False)
        self.pushButton_mappe.setVisible(False)
        self.comboBox_area.setVisible(False)
        self.label_23.setVisible(False)
        self.label_21.setVisible(False)
        self.comboBox_wanted.setVisible(False)
        self.resistivitestboundaries_specific.setVisible(True)

        self.label_24.setVisible(True)
        self.comboBox_lagtype.setVisible(True)
        self.tableWidget_magasin.setVisible(False)
    elif self.comboBox_tolkningsvurdering.currentText() == '2.2: Benyt alle flader fra mappe (KRÆVENDE)':
        self.groupBox.setVisible(True)
        self.label_18.setVisible(False)
        self.layer_combo.setVisible(False)
        self.label_19.setVisible(False)
        self.layer_combo_2.setVisible(False)
        self.label_21.setVisible(False)
        self.comboBox_wanted.setVisible(False)
        self.comboBox_area.setVisible(False)
        self.label_23.setVisible(False)

        self.label_24.setVisible(False)
        self.comboBox_lagtype.setVisible(False)
        self.tableWidget_magasin.setVisible(True)
        self.resistivitestboundaries_specific.setVisible(False)

        self.label_22.setVisible(True)
        self.lineEdit_10.setVisible(True)
        self.pushButton_mappe.setVisible(True)
    elif self.comboBox_tolkningsvurdering.currentText() == '1.2: FOHM - alle lag (KRÆVENDE)' or self.comboBox_tolkningsvurdering.currentText() == '0.2: Fynmodellen - alle lag (KRÆVENDE)':
        self.groupBox.setVisible(True)
        self.label_22.setVisible(False)
        self.lineEdit_10.setVisible(False)
        self.pushButton_mappe.setVisible(False)

        self.label_18.setVisible(False)
        self.layer_combo.setVisible(False)
        self.label_19.setVisible(False)
        self.layer_combo_2.setVisible(False)

        self.label_24.setVisible(False)
        self.comboBox_lagtype.setVisible(False)
        self.tableWidget_magasin.setVisible(False)

        self.resistivitestboundaries_specific.setVisible(False)

        self.label_21.setVisible(False)
        self.comboBox_wanted.setVisible(False)

        self.comboBox_area.setVisible(True)
        self.label_23.setVisible(True)

def fetch_table_names(område): #hvis lag fra database, hent navne fra database
    """
        Returns the table_names within the fohm database/schema
        :return: string, sql
    """
    sql_table_name = f'''
        SELECT table_name AS table_name
        FROM information_schema.tables
        WHERE table_schema='mstfohm'
        AND table_type='BASE TABLE'
        AND table_name LIKE '{område}%'
        AND table_name NOT LIKE '%slice%'
        ORDER BY table_name;
        '''
    db = Database(database='GRUKOS')
    df_table_name = db.sql_to_df(sql_table_name)
    return df_table_name

#Custom geofysik database
def indhent_geofysik(self):
    global plugin_path_geofysik
    # filter = "Shapefil(*.shp);;GeoPackage(*.gpkg);;Raster(*.tiff);;Raster(*.asc)"
    plugin_path_geofysik, _filter = QFileDialog.getOpenFileNames(self, caption='Vælg geofysikdatabase-fil.')  # , filter=filter
    self.lineEdit_8.setText(str(plugin_path_geofysik))

#Custom borings database
def indhent_boringer(self):
    global plugin_path_boringer
    # filter = "Shapefil(*.shp);;GeoPackage(*.gpkg);;Raster(*.tiff);;Raster(*.asc)"
    plugin_path_boringer, _filter = QFileDialog.getOpenFileNames(self, caption='Vælg boringdsdatabase-fil.')  # , filter=filter
    self.lineEdit_9.setText(str(plugin_path_boringer))

def boringer_fetch_fohm_all(crs, geom, layer, område, fohm):
    layername = layer
    if isinstance(layername, str):
        layer_id = int(re.search(r"\d+", layername).group())
    else:
        layer_id = layername

    #Lav SQL der henter boringer
    if fohm == True:
        direct = os.path.dirname(__file__) #har smidt det ind i en textfil fordi SQL'en var så lang...
        f = open(direct + r'\SQL_textfiles\fetchBoreholes.txt', 'r')
        filedata = f.read()
        f.close()
        sql_comp = filedata.format(område=område,layer_id=layer_id,crs=crs,geom=geom.asWkt())
    else:
        tableidentifier = {2 : 'kl1',3 : 'ks1',4 : 'kl2',5 : 'ks2', 6: 'kl3',7:'ks3',8: 'kl4',9: 'pl1', 10:'kalk'}
        sql_comp = f'''
                    SELECT * 
                    FROM pilotprojekt.boringsdata_fynmodel_{tableidentifier.get(layer_id)}
                    WHERE ST_Intersects(ST_SetSRID(ST_MakePoint(st_x, st_y), {crs}), 'SRID={crs};{geom.asWkt()}')
                    and boringskvalitet >= 3
 
                    '''
                
    #Kør SQL
    db = Database(database='GRUKOS')
    print(sql_comp)
    df_fohm = db.sql_to_df(sql_comp)
    return df_fohm

def hent_geofysik(crs, geom, alllayers, flag, område, fohm):
    sql_join = ''
    sql_ele = ''
    sql_where = ''
    prev = ''
    
    if flag == 2: #fetch only one layer
        if område == 'jylland':
            topo = 'jylland_0000_terraen'
        elif område == 'fyn':
            topo = 'fyn_0001_topo'
        else:
            topo = 'sjaelland_0001_topo'
        alllayers=(topo,)+alllayers
                
    max_iterations = len(alllayers)

    for i, lay in enumerate(alllayers):
            table_name = lay
            layer_name = re.search("(\d+.[A-Za-z0-9ÆØÅæøå]+.[A-Za-z0-9ÆØÅæøå]+)", lay)[0]

            sql_join += f'\n INNER JOIN mstfohm."{table_name}" r{i} ON st_intersects(r{i}.rast, gp.geom)'  # Laver Joins

            if flag == 2 and i!=0 and lay == topo:
                sql_ele += f' '
            else:
                sql_ele += f'\n(ST_ValueCount(ST_Clip(r{i}.rast,ST_Transform(gp.geom, {crs})))).value AS "{layer_name}",' #Indhenter lag-værdi for geofysik punktet

            # SQL_Where sikrer at der kun indhentes over DOI og indenfor det specifikke lag.
            if flag == 1 and i != max_iterations-1 and i != 0:
                sql_where += f'\n (((doi <= lagtopkote OR doi IS NULL) AND "{prev}"!="{layer_name}" AND lagtopkote <= "{prev}" AND lagtopkote >= "{layer_name}")  OR ((doi <= lagtopkote OR doi IS NULL) AND "{prev}"!="{layer_name}" AND lagbottomkote <= "{prev}" AND lagbottomkote >= "{layer_name}")) OR'
            elif i == max_iterations-1:
                sql_where += f'\n (((doi <= lagtopkote OR doi IS NULL) AND "{prev}"!="{layer_name}" AND lagtopkote <= "{prev}" AND lagtopkote >= "{layer_name}")  OR ((doi <= lagtopkote OR doi IS NULL) AND "{prev}"!="{layer_name}" AND lagbottomkote <= "{prev}" AND lagbottomkote >= "{layer_name}"))'
            else:
                sql_where += f' '

            prev = layer_name
    if fohm == True:
        sql = f'''
    
                    WITH geofysik_points AS (
                    SELECT DISTINCT ON (o.dataset, position, model) 
                    model, POSITION, o.dataset, geom, datatype_model, proj_ident, datetime_geus, contractor, client
                    FROM mstgerda.geophysical_model_all mms 
                    INNER JOIN gerda.odvmodse o USING (model)
                    WHERE ST_Intersects(ST_Transform(mms.geom, {crs}), 'SRID={crs};{geom.asWkt()}')
                    AND datatype_model IN ('skytem', 'ttem')
                    ),
    
                    fladekorrigeret AS (
                    SELECT gp.*,  o.layer, o.rho, pos.numlayers, ST_X(geom), ST_Y(geom), 
                    {sql_ele}
                     (ST_ValueCount(ST_Clip(r0.rast,ST_Transform(gp.geom, 25832)))).value-doi.doiupper as doiupper,
                            (ST_ValueCount(ST_Clip(r0.rast,ST_Transform(gp.geom, 25832)))).value-doi.doilower as doi,

                    (ST_ValueCount(ST_Clip(r0.rast,ST_Transform(gp.geom, {crs})))).value-o.DEPTOP AS lagtopkote, 
                    (ST_ValueCount(ST_Clip(r0.rast,ST_Transform(gp.geom, {crs})))).value-o.DEPBOTTOM AS lagbottomkote
                    FROM geofysik_points gp
                    LEFT JOIN  gerda.odvlayer o ON o.model = gp.model AND o.position = gp.position
                    left join gerda.odvdoi doi on doi.model = gp.model and doi."position" = gp.position
                     left join gerda.odvpos pos on pos.model = gp.model and pos."position" = gp.position

                    {sql_join}
                    
                    WHERE pos.numlayers > 6
     
                    )
                    SELECT * FROM fladekorrigeret 
                    WHERE {sql_where}
    
    
    
                    '''
    else:
        sql = f'''
        with z AS (
                SELECT * FROM pilotprojekt.geofysisk_fynsmodellen_fyn
                WHERE {sql_where}
                )
                SELECT * FROM z WHERE ST_Intersects(ST_SetSRID(ST_MakePoint(z.st_x, z.st_y), {crs}), 'SRID={crs};{geom.asWkt()}')
               '''


    db = Database(database='GRUKOS')
    print(sql)
    df_gerda = db.sql_to_df(sql)
    return df_gerda

def reshape_geofysik(df_gerda_layer_specific,layer_top,layer_bottom,layername):
    id = []
    geometri_x = []
    geometri_y = []
    modstanden = []
    tykkelsen = []
    samlet_middelmodstand = []
    samlet_median = []
    doi = []
    top = []
    bund = []
    datatype = []
    variationskoefficienten = []
    topografi_kote = []
    df_gerda_layer_grp = df_gerda_layer_specific.groupby(['dataset', 'position', 'model'])
    
    for key, grp in df_gerda_layer_grp:
        max_index = len(grp) - 1
        id.append(key)  #Gemmer boreholeno
        geometri_x.append(grp.iloc[0]['st_x']) #Gemmer x-koordinatet
        geometri_y.append(grp.iloc[0]['st_y']) #Gemmer y-koordinatet
        doi.append(grp.iloc[0]['doi']) #Gemmer doi

       # tykkelsen.append(grp.iloc[0]['thickness'])

        top.append(grp.iloc[0]['lagtopkote']) #Gemmer doi
        bund.append(grp.iloc[0]['lagbottomkote']) #Gemmer doi
        datatype.append(grp.iloc[0]['datatype_model'])


        topografi_kote.append(grp.iloc[0][f"{[col for col in df_gerda_layer_specific.columns if 'topo' in col][0]}"])

        model_middelmodstand = df_gerda_layer_specific[df_gerda_layer_specific['dataset'] == key[0]]["rho"].mean() #Gemmer samlet middelmodstand for hele datasættet
        model_median = df_gerda_layer_specific[df_gerda_layer_specific['dataset'] == key[0]]["rho"].median() # Gemmer samlet median for hele datasættet
        samlet_middelmodstand.append(model_middelmodstand)
        samlet_median.append(model_median)

        topo_col = [col for col in df_gerda_layer_specific.columns if 'topo' in col][0]

        if str(grp.iloc[0]['datatype_model']) == 'ttem' and (grp.iloc[0]['{}'.format(topo_col)]-grp.iloc[0]['lagbottomkote']) <= 2:
            var0 = 10000

        #Udregnes gennemsnitlig middelmodstand for sonderingen

        #Og udregner varianser på baggrund af følgende GEUS variansfunktion:
        # (1) var0 = (0.5·1.2·Resistivitets_model_tykkelse)^2;
        # med variansgrænser på:
        #        tTEM        var0 for (HS-model_dybde<2) = 10000;
        #        SkyTEM      var0 for (HS-model_dybde<5) = 10000;

        new_rho = []
        varianser = []

        for index, (row_index, row) in enumerate(grp.iterrows()):
            if str(row['datatype_model']) == 'ttem' and (row['{}'.format(topo_col)] - row['lagtopkote']) < 2:
                varianser.append(10000)
            elif str(row['datatype_model']) == 'skytem' and (row['{}'.format(topo_col)] - row['lagtopkote']) < 5:
                varianser.append(10000)
            else:
                varianser.append((0.5*1.2*row["thickness"])**2)


            tykkelse = abs(row["{}".format(layer_top)] - row["{}".format(layer_bottom)])

            if index == 0:
                new_rho.append(
                (abs(row['{}'.format(layer_top)] - row['lagbottomkote']) / tykkelse) * row['rho'])

            elif index == max_index:
                    new_rho.append(
                        (abs(row['{}'.format(layer_bottom)] - row['lagtopkote']) / tykkelse) * row['rho'])
            else:
                new_rho.append((abs(row['lagtopkote'] - row['lagbottomkote']) / tykkelse) * row['rho'])


        modstanden.append(sum(new_rho))        
                
        # Variationskoefficient udregnes med nye manuelt udregnede varianser istedet for STD som standard
        variationskoefficienten.append((np.sqrt(np.mean(varianser))/np.mean(new_rho))*100)

    # Check if resistivities are within the expected range for a given layer
    # read expected resistivity file
    direct = os.path.dirname(__file__)
    expect_res = pd.read_csv(direct + r'\data_files\forventet_modstand_AU_simple.csv', ';')
        
    # get expected resistivities for the current layer
    layer_row = expect_res[expect_res['layername'] == layername]    
    
    if not layer_row.empty:
        # Get the min and max bounds for the current layer, defined like this to allow us to add more criteria later
        min_res = layer_row[layer_row['bound'] == 'min']['resistivity'].values[0] if 'min' in layer_row['bound'].values else None
        max_res = layer_row[layer_row['bound'] == 'max']['resistivity'].values[0] if 'max' in layer_row['bound'].values else None

        # Check each resistivity value in the list
        if min_res is not None:
            range_check = modstanden>=min_res
            range_distance = (min_res-modstanden)/min_res #very simple check of how far away the value is
            for i, v in enumerate(range_distance):
                if v < 0: #no distance if value is within range
                    range_distance[i]=0
        elif max_res is not None:
            range_check = modstanden<=max_res
            range_distance = (modstanden-max_res)/max_res
            for i,v in enumerate(range_distance):
                if v < 0:
                    range_distance[i]=0
        else:
            range_check = None

    #Opstiller ny dataframe med hvad der skal ind i QGIS
    new_dataframe = pd.DataFrame(
        list(zip(id, geometri_x, geometri_y, modstanden, samlet_middelmodstand, samlet_median, range_check, range_distance, doi, top, bund, datatype, variationskoefficienten, topografi_kote)),
        columns=['ID', 'X', 'Y', 'Middelmodstand', 'samlet_middelmodstand', 'samlet_median', 'range_check', 'range_distance', 'DOI', 'top', 'bund', 'datatype', 'variationskoefficienten', 'topografi_kote'])



    refrenceliste = ['ID', 'datatype', 'top', 'bund','DOI', 'range_check', 'range_distance', 'Middelmodstand', 'LOGMiddelmodstand','samlet_middelmodstand', 'LOGsamlet_middelmodstand', 'samlet_median', 'LOGsamlet_median', 'variationskoefficienten', 'topografi_kote']

    #Laver temporært QGIS lag
    temp = QgsVectorLayer('Point?crs=epsg:25832',
                          "GeofysiskSikkerhed - {}".format(layername), 'memory')

    temp_data = temp.dataProvider()
    temp.startEditing()

    #Tilføjer attributter/columns til laget
    for x in refrenceliste:
        if x in ('ID', 'datatype'):
            temp.addAttribute(QgsField(f"{x}", QVariant.String))
        else:
            temp.addAttribute(QgsField(f"{x}", QVariant.Double))


    temp.updateFields()

    #Tilføjer features til laget og giver dem en geometri
    for row in new_dataframe.itertuples():
        f = QgsFeature()
        f.setFields(temp.fields())
        point = QgsPointXY(row[2], row[3]) #X- og Y-koordinater
        f.setGeometry(QgsGeometry.fromPointXY(point))
        temp.dataProvider().addFeature(f)

    temp.updateExtents()
    temp.commitChanges()

    temp.startEditing()

    #Tilføjer attributværdier til lagets features
    for i, f in enumerate(temp.getFeatures()):

        for value in refrenceliste:
            if value in ('ID', 'datatype'):
                k = str(new_dataframe.iloc[i]["{}".format(value)])
                temp.changeAttributeValue(f.id(), temp.fields().indexFromName("{}".format(value)), k)
            elif value in ('LOGMiddelmodstand', 'LOGsamlet_middelmodstand', 'LOGsamlet_median'):
                k = float(np.log10(float(new_dataframe.iloc[i]["{}".format(value[3:])].astype(float))))
                if abs(k) > 0 and abs(k) < 100000:
                    temp.changeAttributeValue(f.id(), temp.fields().indexFromName("{}".format(value)), k)
            elif value in ('range_distance', 'range_check'):
                k =  float(new_dataframe.iloc[i]["{}".format(value)].astype(float))
                temp.changeAttributeValue(f.id(), temp.fields().indexFromName("{}".format(value)), k)
            else:
                k =  float(new_dataframe.iloc[i]["{}".format(value)].astype(float))
                if abs(k) > 0 and abs(k) < 100000:
                    temp.changeAttributeValue(f.id(), temp.fields().indexFromName("{}".format(value)), k)


    temp.updateExtents()
    temp.commitChanges()
    # Udregner afvigelse fra samlet middelmostand og median på geofysikpunkter
    temp1 = processing.run("native:fieldcalculator",
                           {'INPUT': temp,
                            'FIELD_NAME': 'log_afvigelse_middelmodstand', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0,
                            'FIELD_PRECISION': 0,
                            'FORMULA': 'abs((("LOGMiddelmodstand" - "LOGsamlet_middelmodstand" )/ "LOGsamlet_middelmodstand" )*100)',
                            'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

    temp2 = processing.run("native:fieldcalculator", {
        'INPUT': temp1,
        'FIELD_NAME': 'log_afvigelse_median', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
        'FORMULA': 'abs((( "LOGMiddelmodstand" - "LOGsamlet_median" )/ "LOGsamlet_median" )*100)',
        'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
    
    # Vægter ift forventede værdier
    temp3 = processing.run("native:fieldcalculator",
                           {'INPUT': temp2,
                            'FIELD_NAME': 'vaegtet_afvigelse_middelmodstand', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0,
                            'FIELD_PRECISION': 0,
                            'FORMULA': '"log_afvigelse_middelmodstand"*2.71828^("range_distance"*10)',
                            'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

    geofysikpunkter = processing.run("native:fieldcalculator", {
        'INPUT': temp3,
        'FIELD_NAME': 'vaegtet_afvigelse_median', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
        'FORMULA': '"log_afvigelse_median"*2.71828^("range_distance"*10)',
        'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

    geofysikpunkter.setName("GeofysiskSikkerhed - {}".format(layername)) 
    return geofysikpunkter


def reshape_boringer(df_boreholes,layername,område):
    if område == 'jylland':
        sand = [120, 140, 200, 400, 1200, 1400, 2100, 2300, 5200, 5400, 5600, 5800, 6000, 6200, 6400, 6600,
                6800,7000, 7200, 7400, 7600, 7800]
        kalk = [10, 8500, 9000]
        ident = layername.split(' ')[0]
    elif område == 'fyn':
        sand = [3, 5, 7]
        kalk = [10]
        ident = layername.split(' ')[-1]

    else:
        sand = [3, 5, 7, 9]
        kalk = [12, 13]
        ident = layername.split(' ')[-1]

    layer_id = int(re.search(r"\d+", ident).group())



    if len(df_boreholes) != 0:
        df_boreholes['sum'] = df_boreholes[
            ["fed ler", "ler", "silt", "fint silt", "mellem silt", "grov silt", "usorteret silt", "sand",
            "fint sand", "mellem sand", "grov sand", "usorteret sand",
            "grus", "fint grus", "mellem grus", "grov grus", "usorteret grus", "sten", "fint sten",
            "mellem sten", "grov sten", "usorteret sten",
            "kalk", "slammet kalk", "organisk", "fed organisk", "vekslende", "ukendt", "andet"]].sum(axis=1)

        if layer_id in sand:
            df_boreholes['wanted_sum'] = df_boreholes.filter(regex='sand|grus|sten|ukendt').sum(axis=1)
        elif layer_id in kalk:
            df_boreholes['wanted_sum'] = df_boreholes.filter(like='kalk|ukendt').sum(axis=1)
        else:
            df_boreholes['wanted_sum'] = df_boreholes.filter(regex='ler|silt|ukendt').sum(axis=1)

        df_boreholes['afvigelse'] = abs((df_boreholes['wanted_sum'] - df_boreholes['sum']) / df_boreholes['sum']) * 100

        temp_boringer = QgsVectorLayer('Point?crs=epsg:25832',
                            "BoringerSikkerhed - {}".format(layername),
                            'memory')

        temp_boringer.startEditing()
        temp_boringer.addAttribute(QgsField("boreholeno", QVariant.String))
        temp_boringer.addAttribute(QgsField("afvigelse_procentvis", QVariant.Double))

        temp_boringer.updateFields()

        # Addition of features
        for row in df_boreholes.itertuples():
            f = QgsFeature()
            f.setFields(temp_boringer.fields())
            point = QgsPointXY(row[-5], row[-4]) #X- og Y-koordinater
            f.setGeometry(QgsGeometry.fromPointXY(point))
            temp_boringer.dataProvider().addFeature(f)

        temp_boringer.updateExtents()
        temp_boringer.commitChanges()
        temp_boringer.startEditing()


        for i, f in enumerate(temp_boringer.getFeatures()):
            id = str(df_boreholes.iloc[i]["boreholeno"])
            temp_boringer.changeAttributeValue(f.id(), temp_boringer.fields().indexFromName("boreholeno"), id)
            value = float(df_boreholes.iloc[i]["afvigelse"].astype(float))

            if isinstance(value, float) and value >= 0 or isinstance(value, int) and value >= 0:
                temp_boringer.changeAttributeValue(f.id(),temp_boringer.fields().indexFromName("afvigelse_procentvis"),value)  # (feature id, field index, value)
            else:
                temp_boringer.deleteFeature(f.id())

        temp_boringer.updateExtents()
        temp_boringer.commitChanges()
        temp_boringer.setName("BoringerSikkerhed - {}".format(layername))

        return temp_boringer

    else:
        pass


def gridning(omr_poly, crs, område, fohm):

    if fohm == False:
        extent = '538550.000000000,619050.000000000,6098450.000000000,6166050.000000000 [EPSG:25832]'
    else:
        if område == 'jylland':
            extent = '438450.0000,663550.0000,6063450.0000,6409950.0000 [EPSG:25832]'
        elif område == 'fyn':
            extent = '542000.0000,628000.0000,6064000.0000,6167000.0000 [EPSG:25832]'
        else:
            extent = '622050.0000,727750.0000,6048550.0000,6231650.0000 [EPSG:25832]'


    grid = processing.run("native:creategrid", {'TYPE': 2,
                                                'EXTENT': extent,
                                                'HSPACING': 100, 'VSPACING': 100, 'HOVERLAY': 0, 'VOVERLAY': 0,
                                                'CRS': QgsCoordinateReferenceSystem('EPSG:{}'.format(crs)),
                                                'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

    # Fjerner gridceller udenfor områdelaget
    processing.run("native:selectbylocation", {
        'INPUT': grid,
        'PREDICATE': [0, 5, 6],
        'INTERSECT': omr_poly, 'METHOD': 0})
    grid.invertSelection()
    grid.startEditing()
    grid.deleteSelectedFeatures()
    grid.commitChanges()


    grid.startEditing()
    grid.addAttribute(QgsField("topografi_kote", QVariant.Double))
    grid.addAttribute(QgsField("top", QVariant.Double))
    grid.addAttribute(QgsField("bund", QVariant.Double))

    grid.addAttribute(QgsField("ttem_mean_afvigelse_middelmodstand", QVariant.Double))
    grid.addAttribute(QgsField("ttem_mean_afvigelse_median", QVariant.Double))
    grid.addAttribute(QgsField("ttem_mean_vaegtet_afvigelse_middelmodstand", QVariant.Double))
    grid.addAttribute(QgsField("ttem_mean_vaegtet_afvigelse_median", QVariant.Double))
    grid.addAttribute(QgsField("ttem_mean_variationskoefficient", QVariant.Double))
    grid.addAttribute(QgsField("ttem_mean_doi", QVariant.Double))
    grid.addAttribute(QgsField("ttem_max_doi", QVariant.Double))

    grid.addAttribute(QgsField("skytem_mean_afvigelse_middelmodstand", QVariant.Double))
    grid.addAttribute(QgsField("skytem_mean_afvigelse_median", QVariant.Double))
    grid.addAttribute(QgsField("skytem_mean_vaegtet_afvigelse_middelmodstand", QVariant.Double))
    grid.addAttribute(QgsField("skytem_mean_vaegtet_afvigelse_median", QVariant.Double))
    grid.addAttribute(QgsField("skytem_mean_variationskoefficient", QVariant.Double))
    grid.addAttribute(QgsField("skytem_mean_doi", QVariant.Double))
    grid.addAttribute(QgsField("skytem_max_doi", QVariant.Double))

    grid.addAttribute(QgsField("boringer_mean_afvigelse", QVariant.Double))

    grid.updateFields()

    QgsProject.instance().addMapLayer(grid)

    return grid

def clone_layer(layer):
    layer.selectAll()
    clone_layer = processing.run("native:saveselectedfeatures", {'INPUT': layer, 'OUTPUT': 'memory:'})['OUTPUT']
    layer.removeSelection()
    return clone_layer


def samlet_vurdering(grid, geofysikpunkter, temp_boringer):
    ######### HER BØR VÆGTNINGEN AF SKYTEM/TTEM FOREGÅ ############

    grid.startEditing()

    # Vælger henholdsvis borings- og geofysikpunkter indenfor cellen, finder gennemsnitettet af afvigelserne af disse og giver dem videre til gridattributten.
    # For geofysik findes gennemsnittet af den logaritmiske afvigelse fra den gennemsnitlige middelmodstand (log_afvigelse_middelmodstand) og den logaritmiske afvigelse fra medianen af middelmodstanden (log_afvigelse_median)
    # For boringer findes den procentvise afvigelse (afvigelse_procentvis)

    for feature in grid.getFeatures():
        grid.select(feature.id())  # Vælger cellen

        for metode in ["skytem", "ttem"]:
                try:
                    # Vælger geofysiskepunkter indenfor cellen
                    processing.run("native:selectbylocation",
                                   {'INPUT': geofysikpunkter,
                                    'PREDICATE': [0, 6], 'INTERSECT': QgsProcessingFeatureSourceDefinition(
                                       grid.source(), selectedFeaturesOnly=True, featureLimit=-1,
                                       geometryCheck=QgsFeatureRequest.GeometrySkipInvalid), 'METHOD': 0})


                    processing.run("qgis:selectbyexpression", {
                        'INPUT': geofysikpunkter,
                        'EXPRESSION':  f'"datatype" = \'{metode}\'', 'METHOD': 3})

                    if geofysikpunkter.selectedFeatureCount() > 0:
                        topografi = processing.run("qgis:basicstatisticsforfields",
                                                   {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                       geofysikpunkter.source(),
                                                       selectedFeaturesOnly=True, featureLimit=-1,
                                                       geometryCheck=QgsFeatureRequest.GeometrySkipInvalid),
                                                       'FIELD_NAME': 'topografi_kote',
                                                       'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})["MEAN"]

                        top = processing.run("qgis:basicstatisticsforfields",
                                             {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                 geofysikpunkter.source(),
                                                 selectedFeaturesOnly=True, featureLimit=-1,
                                                 geometryCheck=QgsFeatureRequest.GeometrySkipInvalid),
                                                 'FIELD_NAME': 'top',
                                                 'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})["MEAN"]

                        bund = processing.run("qgis:basicstatisticsforfields",
                                              {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                  geofysikpunkter.source(),
                                                  selectedFeaturesOnly=True, featureLimit=-1,
                                                  geometryCheck=QgsFeatureRequest.GeometrySkipInvalid),
                                                  'FIELD_NAME': 'bund',
                                                  'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})["MEAN"]


                        # Udregner den gennemsnitlige værdi af afvigelse af middelmodstand indenfor cellen
                        mean_afvigelse_middelmodstand = processing.run("qgis:basicstatisticsforfields",
                                                    {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                        geofysikpunkter.source(),
                                                        selectedFeaturesOnly=True, featureLimit=-1,
                                                        geometryCheck=QgsFeatureRequest.GeometrySkipInvalid),
                                                        'FIELD_NAME': 'log_afvigelse_middelmodstand',
                                                        'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})["MEAN"]

                        # Udregner den gennemsnitlige værdi af afvigelse af median indenfor cellen
                        mean_afvigelse_median = processing.run("qgis:basicstatisticsforfields",
                                                     {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                         geofysikpunkter.source(),
                                                         selectedFeaturesOnly=True, featureLimit=-1,
                                                         geometryCheck=QgsFeatureRequest.GeometrySkipInvalid),
                                                         'FIELD_NAME': 'log_afvigelse_median',
                                                         'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})["MEAN"]

                        # Udregner den gennemsnitlige værdi af afvigelse af middelmodstand indenfor cellen VÆGTET med forventning
                        mean_vaegtet_afvigelse_middelmodstand = processing.run("qgis:basicstatisticsforfields",
                                                    {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                        geofysikpunkter.source(),
                                                        selectedFeaturesOnly=True, featureLimit=-1,
                                                        geometryCheck=QgsFeatureRequest.GeometrySkipInvalid),
                                                        'FIELD_NAME': 'vaegtet_afvigelse_middelmodstand',
                                                        'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})["MEAN"]

                        # Udregner den gennemsnitlige værdi af afvigelse af median indenfor cellen VÆGTET med forventning
                        mean_vaegtet_afvigelse_median = processing.run("qgis:basicstatisticsforfields",
                                                     {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                         geofysikpunkter.source(),
                                                         selectedFeaturesOnly=True, featureLimit=-1,
                                                         geometryCheck=QgsFeatureRequest.GeometrySkipInvalid),
                                                         'FIELD_NAME': 'vaegtet_afvigelse_median',
                                                         'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})["MEAN"]

                        mean_variationskoefficient = processing.run("qgis:basicstatisticsforfields",
                                                               {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                                   geofysikpunkter.source(),
                                                                   selectedFeaturesOnly=True, featureLimit=-1,
                                                                   geometryCheck=QgsFeatureRequest.GeometrySkipInvalid),
                                                                   'FIELD_NAME': 'variationskoefficienten',
                                                                   'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})["MEAN"]

                        mean_doi = processing.run("qgis:basicstatisticsforfields",
                                                                    {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                                        geofysikpunkter.source(),
                                                                        selectedFeaturesOnly=True, featureLimit=-1,
                                                                        geometryCheck=QgsFeatureRequest.GeometrySkipInvalid),
                                                                        'FIELD_NAME': 'DOI',
                                                                        'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})["MEAN"]

                        max_doi = processing.run("qgis:basicstatisticsforfields",
                                                  {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                      geofysikpunkter.source(),
                                                      selectedFeaturesOnly=True, featureLimit=-1,
                                                      geometryCheck=QgsFeatureRequest.GeometrySkipInvalid),
                                                      'FIELD_NAME': 'DOI',
                                                      'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})["MIN"]



                        # Tilføjer celle afgrænsedede afvigelser til grid celler i QGIS
                        grid.changeAttributeValue(feature.id(),grid.fields().indexFromName(f"{metode}_mean_afvigelse_middelmodstand"),mean_afvigelse_middelmodstand)
                        grid.changeAttributeValue(feature.id(), grid.fields().indexFromName(f"{metode}_mean_afvigelse_median"), mean_afvigelse_median)
                        grid.changeAttributeValue(feature.id(),grid.fields().indexFromName(f"{metode}_mean_vaegtet_afvigelse_middelmodstand"), mean_vaegtet_afvigelse_middelmodstand)
                        grid.changeAttributeValue(feature.id(), grid.fields().indexFromName(f"{metode}_mean_vaegtet_afvigelse_median"), mean_vaegtet_afvigelse_median)
                        grid.changeAttributeValue(feature.id(), grid.fields().indexFromName(f"{metode}_mean_variationskoefficient"), mean_variationskoefficient)
                        grid.changeAttributeValue(feature.id(), grid.fields().indexFromName(f"{metode}_mean_doi"), mean_doi)
                        grid.changeAttributeValue(feature.id(), grid.fields().indexFromName(f"{metode}_max_doi"), max_doi)

                        if pd.isna(topografi):
                            grid.deleteFeature(f.id())
                        else:
                            grid.changeAttributeValue(feature.id(), grid.fields().indexFromName(f"topografi_kote"), topografi)

                        if pd.isna(top):
                            grid.deleteFeature(f.id())
                        else:
                            grid.changeAttributeValue(feature.id(), grid.fields().indexFromName(f"top"), top)  
                        
                        grid.changeAttributeValue(feature.id(), grid.fields().indexFromName(f"bund"), bund)


                    geofysikpunkter.removeSelection()

                except:
                    pass
                  # grid.changeAttributeValue(feature.id(),grid.fields().indexFromName(f"{metode}_mean_afvigelse_middelmodstand"),'nan')
                  #  grid.changeAttributeValue(feature.id(),grid.fields().indexFromName(f"{metode}_mean_afvigelse_median"),'nan')
                  #  grid.changeAttributeValue(feature.id(),grid.fields().indexFromName(f"{metode}_mean_variationskoefficient"),'nan')
                  #  grid.changeAttributeValue(feature.id(),grid.fields().indexFromName(f"{metode}_mean_doi"),'nan')
                  #  grid.changeAttributeValue(feature.id(), grid.fields().indexFromName(f"{metode}_max_doi"), 'nan')
                  #  grid.changeAttributeValue(feature.id(), grid.fields().indexFromName(f"topografi_kote"), 'nan')


        if temp_boringer != None:

            # Vælger boringspunkter indenfor cellen
            processing.run("native:selectbylocation",
                           {'INPUT': temp_boringer,
                            'PREDICATE': [0, 6], 'INTERSECT': QgsProcessingFeatureSourceDefinition(
                               grid.source(), selectedFeaturesOnly=True, featureLimit=-1,
                               geometryCheck=QgsFeatureRequest.GeometrySkipInvalid), 'METHOD': 0})

            if temp_boringer.selectedFeatureCount() > 0:

                # Udregner den gennemsnitlige værdi af afvigelse af boring indenfor cellen
                statistics_boring = processing.run("qgis:basicstatisticsforfields",
                                                   {'INPUT_LAYER': QgsProcessingFeatureSourceDefinition(
                                                       temp_boringer.source(),
                                                       selectedFeaturesOnly=True, featureLimit=-1,
                                                       geometryCheck=QgsFeatureRequest.GeometrySkipInvalid),
                                                       'FIELD_NAME': 'afvigelse_procentvis',
                                                       'OUTPUT_HTML_FILE': 'TEMPORARY_OUTPUT'})

                boringer_mean_afvigelse = statistics_boring["MEAN"]

                grid.changeAttributeValue(feature.id(), grid.fields().indexFromName("boringer_mean_afvigelse"),
                                          boringer_mean_afvigelse)

            temp_boringer.removeSelection()
        else:
            pass
         #   grid.changeAttributeValue(feature.id(), grid.fields().indexFromName("boringer_mean_afvigelse"),
                #                      'nan')

        # Fjerne selection for at køre igen
        grid.removeSelection()

    grid.updateExtents()
    grid.commitChanges()

    ########## HER BØR LAVES NY VÆGTNING ###########

    #Benytter variationskoefficienten til at vægte mellem skytem og ttem:
    grid2 = processing.run("native:fieldcalculator", {'INPUT':grid,'FIELD_NAME':'udregnet_geofysik_afvigelse','FIELD_TYPE':0,'FIELD_LENGTH':0,'FIELD_PRECISION':0,
                                                      'FORMULA': 'CASE \r\nWHEN ttem_mean_afvigelse_median > 0 AND skytem_mean_afvigelse_median > 0 THEN \r\n(1-("skytem_mean_variationskoefficient" / ("skytem_mean_variationskoefficient" + "ttem_mean_variationskoefficient"))) * "skytem_mean_afvigelse_median" + (1-("ttem_mean_variationskoefficient" / ("skytem_mean_variationskoefficient" + "ttem_mean_variationskoefficient"))) * "ttem_mean_afvigelse_median"\r\n\r\nELSE array_filter(array(ttem_mean_afvigelse_median,skytem_mean_afvigelse_median), @element != \'nan\', 2)[0]\r\n\r\nEND',
                                                      'OUTPUT':'TEMPORARY_OUTPUT'})['OUTPUT']
<<<<<<< HEAD
    grid3 = processing.run("native:fieldcalculator", {'INPUT':grid2,'FIELD_NAME':'udregnet_geofysik_afvigelse_vaegt','FIELD_TYPE':0,'FIELD_LENGTH':0,'FIELD_PRECISION':0,
                                                      'FORMULA': 'CASE \r\nWHEN ttem_mean_vaegtet_afvigelse_median > 0 AND skytem_mean_vaegtet_afvigelse_median > 0 THEN \r\n(1-("skytem_mean_variationskoefficient" / ("skytem_mean_variationskoefficient" + "ttem_mean_variationskoefficient"))) * "skytem_mean_vaegtet_afvigelse_median" + (1-("ttem_mean_variationskoefficient" / ("skytem_mean_variationskoefficient" + "ttem_mean_variationskoefficient"))) * "ttem_mean_vaegtet_afvigelse_median"\r\n\r\nELSE array_filter(array(ttem_mean_vaegtet_afvigelse_median,skytem_mean_vaegtet_afvigelse_median), @element != \'nan\', 2)[0]\r\n\r\nEND',
                                                      'OUTPUT':'TEMPORARY_OUTPUT'})['OUTPUT']                                                  
=======
>>>>>>> 378eb5e3f21c1e113a6817e4819c7bd22f65c8fb

    # Benytter de to GEUS funktioner til at vægte mellem geofysik og boringer:
    # (1) var0 = (max(1.2, 0.5·0.15·HS-model_dybde) +2)^2       # Geofysik fålagsmodel
    # (2) var0 = K2+(b*z)2;         K = 2, b = 0.03             # Boringsvariansmodel
    grid4 = processing.run("native:fieldcalculator", {
     'INPUT': grid3,
     'FIELD_NAME': 'samlet_afvigelse', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
        'FORMULA': "CASE WHEN udregnet_geofysik_afvigelse > 0 AND boringer_mean_afvigelse > 0 AND topografi_kote-top > 10\r\n\r\nTHEN (1-((max(1.2, 0.5*0.15*(topografi_kote-top))+2)^2\r\n/((max(1.2, 0.5*0.15*(topografi_kote-top))+2)^2     +    (2^2+(0.03*(topografi_kote-top))^2))))\r\n\r\n* udregnet_geofysik_afvigelse + \r\n\r\n(1-((2^2+(0.03*(topografi_kote-top))^2)\r\n/((max(1.2, 0.5*0.15*(topografi_kote-top))+2)^2     +    (2^2+(0.03*(topografi_kote-top))^2))))\r\n\r\n* boringer_mean_afvigelse \r\n\r\nelse array_filter(array(udregnet_geofysik_afvigelse, boringer_mean_afvigelse), @element != 'nan', 2)[0]\r\nend ",
        #'FORMULA': "\r\narray_sum(array_filter(array(ttem_mean_afvigelse_median,skytem_mean_afvigelse_median,boringer_mean_afvigelse), @element != 'nan', 3))",
        'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
    grid_final = processing.run("native:fieldcalculator", {
     'INPUT': grid4,
     'FIELD_NAME': 'samlet_afvigelse_vaegt', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
        'FORMULA': "CASE WHEN udregnet_geofysik_afvigelse_vaegt > 0 AND boringer_mean_afvigelse > 0 AND topografi_kote-top > 10\r\n\r\nTHEN (1-((max(1.2, 0.5*0.15*(topografi_kote-top))+2)^2\r\n/((max(1.2, 0.5*0.15*(topografi_kote-top))+2)^2     +    (2^2+(0.03*(topografi_kote-top))^2))))\r\n\r\n* udregnet_geofysik_afvigelse_vaegt + \r\n\r\n(1-((2^2+(0.03*(topografi_kote-top))^2)\r\n/((max(1.2, 0.5*0.15*(topografi_kote-top))+2)^2     +    (2^2+(0.03*(topografi_kote-top))^2))))\r\n\r\n* boringer_mean_afvigelse \r\n\r\nelse array_filter(array(udregnet_geofysik_afvigelse_vaegt, boringer_mean_afvigelse), @element != 'nan', 2)[0]\r\nend ",
        #'FORMULA': "\r\narray_sum(array_filter(array(ttem_mean_afvigelse_median,skytem_mean_afvigelse_median,boringer_mean_afvigelse), @element != 'nan', 3))",
        'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

    ########## HER BØR LAVES NY VÆGTNING ###########

    QgsProject.instance().removeMapLayer(grid)

    return grid_final

def lav_statistik(geofysikpunkter, temp_boringer):
    #definer destination
    #param = QgsProcessingParameterFileDestination( 'OUTPUT', 'C:\Users\B165456\Pictures\Saved Pictures\out.html')
    # don't show the file overwrite warning when users select a destination file:
    #param.setMetadata( {'widget_wrapper':
    #    { 'dontconfirmoverwrite': True }
    #})
    #lav statistik output
    statistics_boring = processing.run("qgis:vectorlayerhistogram",
                                                   {'INPUT': QgsProcessingFeatureSourceDefinition(
                                                       temp_boringer.source(),
                                                       selectedFeaturesOnly=False, featureLimit=-1,
                                                       geometryCheck=QgsFeatureRequest.GeometrySkipInvalid),
                                                       'FIELD': 'afvigelse_procentvis',
                                                       'BINS': 20,
                                                       'OUTPUT': 'TEMPORARY_OUTPUT'})
    


def foretag_tolkningsvurderingen(self):
    if len(self.comboBox.currentText()) > 0: #Hvis områdepolygon er indhentet
        self.foretag_tolkningsvurdering.setText("Kører")
        self.foretag_tolkningsvurdering.setEnabled(False)
        self.repaint()
        project = QgsProject.instance()

        # Indhenter geometri og CRS af områdepolygon (omr_poly), lav grid
        key = self.comboBox.currentText()
        omr_poly = project.mapLayersByName('{}'.format(key))[0]

        try:
            node = QgsProject.instance().layerTreeRoot().findLayer(omr_poly.id())
            if node:
                node.setItemVisibilityChecked(False)
        except:
            pass


        crs = int(
            "".join(filter(str.isdigit, str(omr_poly.crs().authid()))))  # int(filter(str.isdigit, str(layer.crs().authid())))
        
        if crs != 25832: #hvis koordinatsystemet ikke er i meter virker senere gridning ikke.
            inputlay=omr_poly
            parameter = {
                'INPUT': inputlay,
                 'TARGET_CRS': 'EPSG:25832',
                'OUTPUT': 'memory:Reprojected'
            }
            omr_poly = processing.run('native:reprojectlayer', parameter)['OUTPUT']
            crs = int(
            "".join(filter(str.isdigit, str(omr_poly.crs().authid()))))

        for kk, feature in enumerate(omr_poly.getFeatures()):
            if kk == 0:
                geom = feature.geometry()
            else:
                geom = geom.combine(feature.geometry())

        # Mulighed 1 - FOHM - enkelt lag


        if self.comboBox_tolkningsvurdering.currentText() == '1.1: FOHM - enkelt lag' or self.comboBox_tolkningsvurdering.currentText() == '0.1: Fynmodellen - enkelt lag':
            if self.comboBox_tolkningsvurdering.currentText() == '1.1: FOHM - enkelt lag':
                fohm = True
            else:
                fohm = False

            wanted_layer = self.comboBox_wanted.currentText()
            #Sand og kalk numre definres - benyttes til forventet "vandførende" og "vandstandsende" lag.
            if wanted_layer.startswith('Fyn'):
                område = 'fyn'
                ident = wanted_layer.split(' ')[-1] #identifier til boringsudtræk
                ident_geofysik = wanted_layer.split(' ')[2] #identifier til geofysikudtræk

            elif wanted_layer.startswith('Sjæl'):
                område = 'sjaelland'
                ident = wanted_layer.split(' ')[-1] #identifier til boringsudtræk
                ident_geofysik = wanted_layer.split(' ')[2] #identifier til geofysikudtræk

            else:
                område = 'jylland'
                ident = wanted_layer.split(' ')[0] #identifier til boringsudtræk
                ident_geofysik = wanted_layer.split(' ')[0] #identifier til geofysikudtræk


            #### BORINGER ####
            # Boringsudtræk og load til QGIS
            try:
                df_boreholes = boringer_fetch_fohm_all(crs, geom, ident, område, fohm) #Boringsudtræk
               # print(df_boreholes)
                temp_boringer = reshape_boringer(df_boreholes,self.comboBox_wanted.currentText(),område)
                project.addMapLayer(temp_boringer)
            except:
                pass

            #### GEOFYSIK ####
            # Geofysisk udtræk og load til QGIS
            if True:
                df_table_name = fetch_table_names(område)
                alllayers = df_table_name['table_name'].tolist() #Alle lag findes

                bundlayer_index = [idx for idx, s in enumerate(alllayers) if ident_geofysik.lower() in s][0] #Det ønskede lag "WANTED LAYER"'s index i alllayers findes
                toplayer_index =  bundlayer_index-1 #Laget ovenover (altså top af lag)

                layers_topbund=(alllayers[toplayer_index],alllayers[bundlayer_index])
                df_gerda = hent_geofysik(crs, geom, layers_topbund, 2, område, fohm)

                ####################### Clause FIX #######################
                df_gerda = df_gerda[(df_gerda["{}".format(re.search("(\d+.[A-Za-z0-9ÆØÅæøå]+.[A-Za-z0-9ÆØÅæøå]+)", layers_topbund[0])[0])] != df_gerda["{}".format(re.search("(\d+.[A-Za-z0-9ÆØÅæøå]+.[A-Za-z0-9ÆØÅæøå]+)", layers_topbund[1])[0])]) & (df_gerda["doi"] <= df_gerda["{}".format(re.search("(\d+.[A-Za-z0-9ÆØÅæøå]+.[A-Za-z0-9ÆØÅæøå]+)", layers_topbund[1])[0])])]



                #Finder toppen af laget
                previous_layer = re.search("(\d+.[A-Za-z0-9ÆØÅæøå]+.[A-Za-z0-9ÆØÅæøå]+)", alllayers[toplayer_index])[0]
                #Finder bunden af laget
                layername = re.search("(\d+.[A-Za-z0-9ÆØÅæøå]+.[A-Za-z0-9ÆØÅæøå]+)", alllayers[bundlayer_index])[0]
                
                #Itererer geofysik punkter
                #Hvis der er geofysik for laget så fortsætter koden
                try:
                    if len(df_gerda) != 0:
                        geofysikpunkter = reshape_geofysik(df_gerda,previous_layer,layername,self.comboBox_wanted.currentText())
                        QgsProject.instance().addMapLayer(geofysikpunkter)
                except: 
                    geofysikpunkter = None

            try:
                geofysikpunkter = project.mapLayersByName("GeofysiskSikkerhed - {}".format(wanted_layer))[0]
            except:
                geofysikpunkter = None
            try:
                temp_boringer = project.mapLayersByName("BoringerSikkerhed - {}".format(wanted_layer))[0]
            except:
                temp_boringer = None

            if geofysikpunkter != None or temp_boringer != None:
                """grid = gridning(omr_poly, crs, område, fohm)

                grid_final = samlet_vurdering(grid, geofysikpunkter, temp_boringer)

                project.addMapLayer(grid_final)

                grid_final.setName("Grid - Samlet Usikkerhedsvurdering - {}".format(wanted_layer))

                grid_final.loadNamedStyle(str(os.path.join(os.path.dirname(__file__), '..')) + "/Styles/Grid - Usikkerhedsvurdering.qml")"""

                grid = gridning(omr_poly, crs, område, fohm)

                grid_final = samlet_vurdering(grid, geofysikpunkter, temp_boringer)
                grid_boringer = clone_layer(grid_final)
                grid_geofysik = clone_layer(grid_final)

                project.addMapLayer(grid_boringer)
                project.addMapLayer(grid_geofysik)
                project.addMapLayer(grid_final)

                for lag in [geofysikpunkter, temp_boringer, grid_geofysik, grid_boringer]:
                    try:
                        node = QgsProject.instance().layerTreeRoot().findLayer(lag.id())
                        if node:
                            node.setItemVisibilityChecked(False)
                    except:
                        pass


                grid_boringer.setName("Grid - Boringer Usikkerhedsvurdering - {}".format(wanted_layer))
                grid_geofysik.setName("Grid - Geofysik Usikkerhedsvurdering - {}".format(wanted_layer))
                grid_final.setName("Grid - Samlet Usikkerhedsvurdering - {}".format(wanted_layer))

                grid_boringer.loadNamedStyle(str(os.path.join(os.path.dirname(__file__),
                                                              '..')) + "/Styles/Grid - Boringer Usikkerhedsvurdering.qml")
                grid_geofysik.loadNamedStyle(str(os.path.join(os.path.dirname(__file__),
                                                              '..')) + "/Styles/Grid - Geofysik Usikkerhedsvurdering.qml")
                grid_final.loadNamedStyle(
                    str(os.path.join(os.path.dirname(__file__), '..')) + "/Styles/Grid - Usikkerhedsvurdering.qml")

                #lav_statistik(geofysikpunkter,temp_boringer)



        elif self.comboBox_tolkningsvurdering.currentText() == '1.2: FOHM - alle lag (KRÆVENDE)' or self.comboBox_tolkningsvurdering.currentText() == '0.2: Fynmodellen - alle lag (KRÆVENDE)':
            if self.comboBox_tolkningsvurdering.currentText() == '1.2: FOHM - alle lag (KRÆVENDE)':
                fohm = True
            else:
                fohm = False
            #Opstiller vandførende lag og definerer områder
            if self.comboBox_area.currentText() == 'Jylland': #Definerer område
                område = 'jylland'

            elif self.comboBox_area.currentText() == 'Fyn':
                område = 'fyn'

            else:
                område = 'sjaelland'

            
            # Indhenter alle lag
            df_table_name = fetch_table_names(område)
            alllayers = df_table_name['table_name'].tolist()

            #Indhenter geofysisk data for alle lagene
            df_gerda = hent_geofysik(crs, geom, alllayers, 1, område, fohm)
            root = project.layerTreeRoot()

            # Geofysik
            for i, lay in enumerate(alllayers[1:]):  # Undgå Topografien
                #Opstiller lister til nemmere at indlæse data i QGIS

                if root.findGroup(str(lay)):
                    group = root.findGroup(str(lay))
                else:
                    group = root.insertGroup(0, str(lay))


                    #Finder toppen af laget
                previous_layer = re.search("(\d+.[A-Za-z0-9ÆØÅæøå]+.[A-Za-z0-9ÆØÅæøå]+)", alllayers[i])[0]
                #Finder bunden af laget
                layername = re.search("(\d+.[A-Za-z0-9ÆØÅæøå]+.[A-Za-z0-9ÆØÅæøå]+)", lay)[0]

                #Sorterer og opstiller en ny dataframe kun for det specifikke lag
                df_gerda_layer_specific = df_gerda[((df_gerda["lagtopkote"] <= df_gerda["{}".format(previous_layer)]) & (df_gerda["lagtopkote"] >= df_gerda["{}".format(layername)]) & ((df_gerda["{}".format(layername)] >= df_gerda["doi"]) | (df_gerda["doi"].isnull())))
                                                   | ((df_gerda["lagbottomkote"] <= df_gerda["{}".format(previous_layer)]) & (df_gerda["lagbottomkote"] >= df_gerda["{}".format(layername)]) & ((df_gerda["{}".format(layername)] >= df_gerda["doi"]) | (df_gerda["doi"].isnull())))]

                #Sørger for at top og bund ikke er det samme (altså at laget er tilstede (burde være overflødig))
                df_gerda_layer_specific = df_gerda_layer_specific[(df_gerda_layer_specific["{}".format(previous_layer)] != df_gerda_layer_specific["{}".format(layername)])]

                #Hvis der er geofysik for laget så fortsætter koden
                try:
                    if len(df_gerda_layer_specific) != 0:
                        #Gruppering og itererer over de enkelte sonderinger
                        geofysikpunkter = reshape_geofysik(df_gerda_layer_specific,previous_layer,layername,lay)
                        project.addMapLayer(geofysikpunkter, False)  # False is the key
                        group.addLayer(geofysikpunkter)
                except:
                    geofysikpunkter = None

            # Boringer
            for lay in alllayers[1:]:
                if root.findGroup(str(lay)):
                    group = root.findGroup(str(lay))
                else:
                    group = root.insertGroup(0, str(lay))

                df_boreholes = boringer_fetch_fohm_all(crs, geom, lay, område, fohm)
                temp_boringer = reshape_boringer(df_boreholes,lay,område)
                project.addMapLayer(temp_boringer, False)  # False is the key
                group.addLayer(temp_boringer)

            # Laver grid
            for i, lay in enumerate(alllayers[1:]):
                if root.findGroup(str(lay)):
                    group = root.findGroup(str(lay))
                else:
                    group = root.insertGroup(0, str(lay))

                try:
                    geofysikpunkter = project.mapLayersByName("GeofysiskSikkerhed - {}".format(lay))[0]
                except:
                    geofysikpunkter = None
                try:
                    temp_boringer = project.mapLayersByName("BoringerSikkerhed - {}".format(lay))[0]
                except:
                    temp_boringer = None



                if geofysikpunkter != None or temp_boringer != None:
                    grid = gridning(omr_poly, crs, område, fohm)

                    grid_final = samlet_vurdering(grid, geofysikpunkter, temp_boringer)
                    grid_boringer = clone_layer(grid_final)
                    grid_geofysik = clone_layer(grid_final)


                    project.addMapLayer(grid_boringer, False)  # False is the key
                    group.addLayer(grid_boringer)
                    project.addMapLayer(grid_geofysik, False)  # False is the key
                    group.addLayer(grid_geofysik)
                    project.addMapLayer(grid_final, False)  # False is the key
                    group.addLayer(grid_final)

                    for lag in [geofysikpunkter, temp_boringer, grid_geofysik, grid_boringer]:
                        try:
                            node = QgsProject.instance().layerTreeRoot().findLayer(lag.id())
                            if node:
                                node.setItemVisibilityChecked(False)
                        except:
                            pass




                    grid_boringer.setName("Grid - Boringer Usikkerhedsvurdering - {}".format(lay))
                    grid_geofysik.setName("Grid - Geofysik Usikkerhedsvurdering - {}".format(lay))
                    grid_final.setName("Grid - Samlet Usikkerhedsvurdering - {}".format(lay))

                    grid_boringer.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Grid - Boringer Usikkerhedsvurdering.qml")
                    grid_geofysik.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Grid - Geofysik Usikkerhedsvurdering.qml")
                    grid_final.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Grid - Usikkerhedsvurdering.qml")






        elif self.comboBox_tolkningsvurdering.currentText() == '2.1: Benyt specifikke flader indlæst i QGIS':
            top = self.layer_combo.currentLayer()
            bottom = self.layer_combo_2.currentLayer()
            buffer = processing.run("native:buffer", {'INPUT':omr_poly,'DISTANCE':150,'SEGMENTS':5,'END_CAP_STYLE':0,'JOIN_STYLE':0,'MITER_LIMIT':2,'DISSOLVE':False,'OUTPUT':'TEMPORARY_OUTPUT'})["OUTPUT"]

            clipped_top = processing.run("gdal:cliprasterbyextent",
                           {'INPUT': top,
                            'PROJWIN': buffer.extent(),
                            'OVERCRS': False, 'NODATA': None, 'OPTIONS': '', 'DATA_TYPE': 0, 'EXTRA': '',
                            'OUTPUT': f'C:/Temp/{top.name()[:-5]}.grd'})['OUTPUT']



            clipped_bottom = processing.run("gdal:cliprasterbyextent",
                                         {'INPUT': top,
                                          'PROJWIN': buffer.extent(),
                                          'OVERCRS': False, 'NODATA': None, 'OPTIONS': '', 'DATA_TYPE': 0, 'EXTRA': '',
                                          'OUTPUT': f'C:/Temp/{bottom.name()[:-5]}.grd'})['OUTPUT']


           # project.addMapLayer(QgsRasterLayer(clipped_bottom))
           # project.addMapLayer(QgsRasterLayer(clipped_top))


            grdpositions = [clipped_top, clipped_bottom]

            for grd in grdpositions:
                print(grd)
                name = grd.split("/")[-1]
                print(name)

                rastername = name.split(".")[0] + "_{}".format(time.time())
                print(rastername)


              #  conn = pg.connect(database="dbname", user="postgres", host="localhost", password="dbpass",
               #                         port=5432)
                conn = Database(database='GRUKOS').connect_to_pg_db()

                cursor = conn.cursor()
                cmds = 'raster2pgsql -I -C -F -s 25832 -t 100x100 "' + str(grd) + f' pilotprojekt.{rastername} |psql'
                subprocess.call(cmds, shell=True)


              #  z = grd.split('.')[0]
              #  print(z)
              #  grd_folder = grd.replace('/', '\\')
              #  direct = os.path.dirname(__file__)

               # f = open(direct + r'\save_grd_to_DB_template.bat', 'r')
               # filedata = f.read()
               # f.close()

               # newdata = filedata.replace('__path__', grd_folder)
            #    f = open(direct + r'\save_grd_to_DB.bat', 'w')
            #    f.write(newdata)
            #    f.close()

             #   subprocess.call([direct + r'\save_grd_to_DB.bat'])
             #   os.remove(direct + r'\save_grd_to_DB.bat')

        elif self.comboBox_tolkningsvurdering.currentText() == '2.2: Benyt alle flader fra mappe (KRÆVENDE)':
            print("HEJ")

        self.foretag_tolkningsvurdering.setText("Foretag tolkningsvurdering")
        self.foretag_tolkningsvurdering.setEnabled(True)
        self.repaint()
    else:
        QMessageBox.warning(None, 'Fejl', "Intet input (områdepolygon).")