# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import (
    QgsFields,
    QgsField,
    QgsFeature,
QgsLayerTreeGroup,
    QgsGeometry,
    QgsFeatureRequest,
    QgsProject,
QgsSimpleMarkerSymbolLayerBase,
    QgsRaster,
    QgsTask,
    QgsPrintLayout,
    QgsReadWriteContext,
    QgsRasterFileWriter,
    QgsMapLayer,
    QgsLayoutItemLegend,
    QgsLayoutItemMap,
    QgsLegendRenderer,
    QgsExpression,
    QgsLegendStyle,
    QgsLayoutExporter,
    QgsLayoutItemPicture,
    QgsLayoutPoint,
    QgsLayoutSize,
    QgsUnitTypes,
    QgsRasterBandStats,
    QgsCategorizedSymbolRenderer,
    QgsRendererCategory,
    QgsSymbol,
    QgsCoordinateTransformContext,
QgsRuleBasedRenderer,
    QgsRandomColorRamp,
    QgsMapThemeCollection,
    QgsLayerDefinition,
    QgsPointXY,
    QgsApplication,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsExpressionContextScope,
    QgsCoordinateReferenceSystem,
    QgsProcessingFeatureSourceDefinition,
    QgsRectangle,
    QgsTaskManager,
    QgsLayout,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsRasterLayer,
    QgsWkbTypes,
    QgsDataSourceUri
)
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()
import pyodbc

from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass


__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *
from ..doDataExtract import *


def outFile_GRUKOS(self):
    if len(self.comboBox.currentText()) > 0:
        checked_items2 = []
        for index in range(self.listWidget_2.count()):
            if self.listWidget_2.item(index).checkState() == Qt.Checked:
                checked_items2.append(self.listWidget_2.item(index).text())

        if len(checked_items2) > 0:
            if True:
                self.pushButton_GRUKOS.setText("Kører")
                self.pushButton_GRUKOS.setEnabled(False)
                self.repaint()

            project = QgsProject.instance()
            key = self.comboBox.currentText()
            layer = project.mapLayersByName('{}'.format(key))[0]
            for kk, feature in enumerate(layer.getFeatures()):
                if kk == 0:
                    geome = feature.geometry()
                else:
                    geome = geome.combine(feature.geometry())

            crstrue = int("".join(filter(str.isdigit, str(layer.crs().authid()))))

            checked_items = []
            for index in range(self.listWidget_2.count()):
                if self.listWidget_2.item(index).checkState() == Qt.Checked:
                    checked_items.append(self.listWidget_2.item(index).text())

            class GRUKOSArbejde(QgsTask):
                def __init__(self, desc, tablename, item, Dialog):
                    self.desc = desc
                    self.table_name = tablename
                    self.item = item
                    self.result_layer = None
                    self.Dialog = Dialog
                    QgsTask.__init__(self, self.desc)

                def run(self):
                    gruk = GRUKOS(table_name=self.table_name, wkt=geome.asWkt(), crs=crstrue)
                    if self.isCanceled():
                        return False
                    sql = gruk.fetch_grukos()
                    uri = QgsDataSourceUri()
                    db = Database(database='GRUKOS')
                    if self.isCanceled():
                        return False
                    uri.setConnection(str(db.host), str(db.port), str(db.database), str(db.usr_read), str(db.pw_read))
                    uri.setDataSource('', f'({sql})', 'geom', '', 'rownumber')

                    vlayer = QgsVectorLayer(uri.uri(), "{} - {}".format(self.item, key), "postgres")
                    if self.isCanceled():
                        return False
                    a = processing.run("native:extractbyexpression",
                                       {'INPUT': vlayer,
                                        'EXPRESSION': 'rownumber is not null', 'OUTPUT': 'TEMPORARY_OUTPUT'})

                    a["OUTPUT"].setName("{} - {}".format(self.item, key))

                    styleql = gruk.get_styleqml()
                    style = db.sql_to_df(styleql)
                    if self.isCanceled():
                        return False
                    if style is None:
                        pass
                    else:
                        try:
                            document = QDomDocument()
                            document.setContent(str(style.iloc[0, 0]))
                            a["OUTPUT"].importNamedStyle(document)
                        except:
                            print("No style for this layer")

                    self.result_layer = a["OUTPUT"]

                    return True

                def finished(self, result):
                    if self.isCanceled():
                        QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                    else:
                        if self.result_layer is None or self.result_layer.featureCount() == 0:
                            QMessageBox.warning(None, 'Meddelse', "Intet data for dette område.")
                        else:
                            self.result_layer.moveToThread(QCoreApplication.instance().thread())
                            project.addMapLayer(self.result_layer)
                    self.Dialog.pushButton_GRUKOS.setText("Eksportér til QGIS")
                    self.Dialog.pushButton_GRUKOS.setEnabled(True)
                    self.Dialog.repaint()
                    QApplication.processEvents()

            if 'Lertykkelse (pktgrid)' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Lertykkelse (pktgrid)'
                self.table_name = 'gvkort.lertykkelse_pktgrid'
                globals()['tas1'] = GRUKOSArbejde("Indehenter lertykkelse fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas1'])
            if 'Lertykkelse (poly)' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Lertykkelse (poly)'
                self.table_name = 'gvkort.lertykkelse_poly'
                globals()['tas2'] = GRUKOSArbejde("Indehenter lertykkelse fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas2'])
            if 'Reduceret lertykkelse (pktgrid)' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Reduceret lertykkelse (pktgrid)'
                self.table_name = 'gvkort.reduceret_lertykkelse_pktgrid'
                globals()['tas3'] = GRUKOSArbejde("Indehenter reduceret lertykkelse fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas3'])
            if 'Reduceret lertykkelse (poly)' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Reduceret lertykkelse (poly)'
                self.table_name = 'gvkort.reduceret_lertykkelse_poly'
                globals()['tas4'] = GRUKOSArbejde("Indehenter reduceret lertykkelse fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas4'])
            if 'Magasinlag (pkt)' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Magasinlag (pkt)'
                self.table_name = 'gvkort.magasinlag_pktgrid'
                globals()['tas5'] = GRUKOSArbejde("Indehenter magasinlag fra GRUKOS...", self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas5'])
            if 'Magasinlag (lin)' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Magasinlag (lin)'
                self.table_name = 'gvkort.magasinlag_lin'
                globals()['tas6'] = GRUKOSArbejde("Indehenter magasinlag fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas6'])
            if 'Magasinlag (poly)' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Magasinlag (poly)'
                self.table_name = 'gvkort.magasinlag_poly'
                globals()['tas7'] = GRUKOSArbejde("Indehenter magasinlag fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas7'])
            if 'Geologisk' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Geologisk'
                self.table_name = 'gvkort.modelomrgeologisk'
                globals()['tas8'] = GRUKOSArbejde("Indehenter geologisk modelområde fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas8'])
            if 'Geokemisk' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Geokemisk'
                self.table_name = 'gvkort.modelomrgeokemisk'
                globals()['tas9'] = GRUKOSArbejde("Indehenter geokemisk modelområde fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas9'])
            if 'Hydrologisk' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Hydrologisk'
                self.table_name = 'gvkort.modelomrhydrologisk'
                globals()['tas10'] = GRUKOSArbejde("Indehenter hydrologisk modelområde fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas10'])
            if 'Hydrostratigrafisk' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Hydrostratigrafisk'
                self.table_name = 'gvkort.modelomrhydrostratigrafisk'
                globals()['tas11'] = GRUKOSArbejde("Indehenter hydrostratigrafisk modelområde fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas11'])
            if 'Afgrænsningspolygon' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Afgrænsningspolygon'
                self.table_name = 'gvkort.afgpolygon'
                globals()['tas12'] = GRUKOSArbejde("Indehenter afgrænsningspolygon fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas12'])
            if 'Nitratsårbarhed' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Nitratsårbarhed'
                self.table_name = 'gvkort.saarbarhed_nitrat'
                globals()['tas13'] = GRUKOSArbejde("Indehenter nitratsårbarhed fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas13'])
            if 'Redoxgrænsen (pkt)' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Redoxgrænsen (pkt)'
                self.table_name = 'gvkort.saarbarhed_redox_pkt'
                globals()['tas14'] = GRUKOSArbejde("Indehenter redoxgrænsen fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas14'])
            if 'Redoxgrænsen (poly)' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Redoxgrænsen (poly)'
                self.table_name = 'gvkort.saarbarhed_redox_poly'
                globals()['tas15'] = GRUKOSArbejde("Indehenter redoxgrænsen fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas15'])

            if 'Potentialekort (pkt)' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Potentialekort (pkt)'
                self.table_name = 'gvkort.potentiale_pkt'
                globals()['tas143'] = GRUKOSArbejde("Indehenter potentialekort fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas143'])
            if 'Potentialekort (poly)' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Potentialekort (poly)'
                self.table_name = 'gvkort.potentiale_poly'
                globals()['tas153'] = GRUKOSArbejde("Indehenter potentialekort fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas153'])
            if 'Potentialekort (lin)' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Potentialekort (lin)'
                self.table_name = 'gvkort.potentiale_lin'
                globals()['tas154'] = GRUKOSArbejde("Indehenter potentialekort fra GRUKOS...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas154'])

            if 'Grundvandsdannende opland (partikler)' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Grundvandsdannende opland (partikler)'
                self.table_name = 'gvkort.grddanopl_par'
                globals()['tas15444'] = GRUKOSArbejde("Indehenter grundvandsdannende opland fra GRUKOS...", self.table_name,
                                                    self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas15444'])
            if 'Grundvandsdannende områder' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Grundvandsdannende områder'
                self.table_name = 'gvkort.grdvdanomr'
                globals()['tas15422'] = GRUKOSArbejde("Indehenter grundvandsdannende områder fra GRUKOS...", self.table_name,
                                                    self.item, self)
                QgsApplication.taskManager().addTask(globals()['tas15422'])

            self.repaint()
            QApplication.processEvents()
        else:
            QMessageBox.warning(None, 'Fejl', "Vælg dit output.")
    else:
        QMessageBox.warning(None, 'Fejl', "Intet input (områdepolygon).")