# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import (
    QgsFields,
    QgsField,
    QgsFeature,
QgsLayerTreeGroup,
    QgsGeometry,
    QgsFeatureRequest,
    QgsProject,
QgsSimpleMarkerSymbolLayerBase,
    QgsRaster,
    QgsTask,
    QgsPrintLayout,
    QgsReadWriteContext,
    QgsRasterFileWriter,
    QgsMapLayer,
    QgsLayoutItemLegend,
    QgsLayoutItemMap,
    QgsLegendRenderer,
    QgsExpression,
    QgsLegendStyle,
    QgsLayoutExporter,
    QgsLayoutItemPicture,
    QgsLayoutPoint,
    QgsLayoutSize,
    QgsUnitTypes,
    QgsRasterBandStats,
    QgsCategorizedSymbolRenderer,
    QgsRendererCategory,
    QgsSymbol,
    QgsCoordinateTransformContext,
QgsRuleBasedRenderer,
    QgsRandomColorRamp,
    QgsMapThemeCollection,
    QgsLayerDefinition,
    QgsPointXY,
    QgsApplication,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsExpressionContextScope,
    QgsCoordinateReferenceSystem,
    QgsProcessingFeatureSourceDefinition,
    QgsRectangle,
    QgsTaskManager,
    QgsLayout,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsRasterLayer,
    QgsWkbTypes,
    QgsDataSourceUri
)
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()
import pyodbc
from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass


__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *
from ..doDataExtract import *


def outFile_Kemi(self):
    if len(self.comboBox.currentText()) > 0:
        checked_items2 = []
        for index in range(self.listWidget.count()):
            if self.listWidget.item(index).checkState() == Qt.Checked:
                checked_items2.append(self.listWidget.item(index).text())

        if len(checked_items2) > 0:
            if True:
                self.pushButton_Kemi.setText("Kører")
                self.pushButton_Kemi.setEnabled(False)
                self.repaint()
                QApplication.processEvents()

            project = QgsProject.instance()
            key = self.comboBox.currentText()
            layer = project.mapLayersByName('{}'.format(key))[0]

            for kk, feature in enumerate(layer.getFeatures()):
                if kk == 0:
                    geom = feature.geometry()
                else:
                    geom = geom.combine(feature.geometry())

            crstrue = int("".join(
                filter(str.isdigit, str(layer.crs().authid()))))  # int(filter(str.isdigit, str(layer.crs().authid())))
            checked_items = []
            for index in range(self.listWidget.count()):
                if self.listWidget.item(index).checkState() == Qt.Checked:
                    checked_items.append(self.listWidget.item(index).text())

            pest = 0
            if 'Pesticider (GEUS)' in checked_items:
                checked_items.remove('Pesticider (GEUS)')
                pest = 1
            pfas = 0
            if 'PFAS (GEUS)' in checked_items:
                checked_items.remove('PFAS (GEUS)')
                pfas = 1

            uorganiskesporstoffer = 0
            if 'Uorganiske sporstoffer (GEUS)' in checked_items:
                checked_items.remove('Uorganiske sporstoffer (GEUS)')
                uorganiskesporstoffer = 1

            organiskmikro = 0
            if 'Organisk mikroforurening (GEUS)' in checked_items:
                checked_items.remove('Organisk mikroforurening (GEUS)')
                organiskmikro = 1

            aromatiskekulbrinter = 0
            if 'Aromatiske kulbrinter (GEUS)' in checked_items:
                checked_items.remove('Aromatiske kulbrinter (GEUS)')
                aromatiskekulbrinter = 1

            alifatiskekulbrinter = 0
            if 'Alifatiske kulbrinter (GEUS)' in checked_items:
                checked_items.remove('Alifatiske kulbrinter (GEUS)')
                alifatiskekulbrinter = 1

            haloalifatiskekulbrinter = 0
            if 'Halogenerede alifatiske kulbrinter (GEUS)' in checked_items:
                checked_items.remove('Halogenerede alifatiske kulbrinter (GEUS)')
                haloalifatiskekulbrinter = 1

            blødgørere = 0
            if 'Blødgørere/Phthalater (GEUS)' in checked_items:
                checked_items.remove('Blødgørere/Phthalater (GEUS)')
                blødgørere = 1

            detergenter = 0
            if 'Detergenter/Sæbe (GEUS)' in checked_items:
                checked_items.remove('Detergenter/Sæbe (GEUS)')
                detergenter = 1

            fenoler = 0
            if 'Chlorerede og bromerede fenoler (GEUS)' in checked_items:
                checked_items.remove('Chlorerede og bromerede fenoler (GEUS)')
                fenoler = 1

            dioxiner = 0
            if 'Halogenerede flammehæmmere, PCB og dioxiner (GEUS)' in checked_items:
                checked_items.remove('Halogenerede flammehæmmere, PCB og dioxiner (GEUS)')
                dioxiner = 1

            organometaller = 0
            if 'Organometaller (GEUS)' in checked_items:
                checked_items.remove('Organometaller (GEUS)')
                organometaller = 1

            geus = [pest, pfas, uorganiskesporstoffer, organiskmikro, aromatiskekulbrinter, alifatiskekulbrinter, haloalifatiskekulbrinter, blødgørere, detergenter, fenoler, dioxiner, organometaller]


            QApplication.processEvents()

            class KemiArbejde(QgsTask):
                def __init__(self, desc, tablename, item, Dialog):
                    self.desc = desc
                    self.table_name = tablename
                    self.item = item
                    self.result_layer = None
                    self.Dialog = Dialog
                    QgsTask.__init__(self, self.desc)

                def run(self):
                    kemi = Kemi(table_name=self.table_name, wkt=geom.asWkt(), crs=crstrue)
                    sql = kemi.fetch_kemi()
                    print(sql)
                    uri = QgsDataSourceUri()
                    db = Database(database='JUPITER')
                    if self.isCanceled():
                        return False
                    uri.setConnection(str(db.host), str(db.port), str(db.database), str(db.usr_read), str(db.pw_read))
                    uri.setDataSource('', f'({sql})', 'geom', '', 'rownumber')

                    vlayer = QgsVectorLayer(uri.uri(), "{} seneste - {}".format(self.item, key), "postgres")
                    print(vlayer)

                    a = processing.run("native:extractbyexpression",
                                       {'INPUT': vlayer,
                                        'EXPRESSION': 'rownumber is not null', 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]

                    a.setName("{} seneste - {}".format(self.item, key))

                    self.result_layer = a

                    return True

                def finished(self, result):
                    if self.isCanceled():
                        QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                    else:
                        if self.result_layer is None:
                            QMessageBox.warning(None, 'Meddelse', "Intet data for dette område ({}).".format(self.item))
                        else:
                            self.result_layer.moveToThread(QCoreApplication.instance().thread())
                            try:
                                self.result_layer.loadNamedStyle(
                                    str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - {}.qml".format(self.item))
                            except:
                                pass

                            if self.item == 'Alle data':
                                try:
                                    self.result_layer.loadNamedStyle(
                                        str(os.path.join(os.path.dirname(__file__),
                                                         '..')) + "/Styles/Kemi - Seneste Analyse Alder.qml")
                                except:
                                    pass

                            project.addMapLayer(self.result_layer)

                    self.Dialog.pushButton_Kemi.setText("Eksportér til QGIS")
                    self.Dialog.pushButton_Kemi.setEnabled(True)

                    self.Dialog.repaint()
                    QApplication.processEvents()

            if 'Alle data' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Alle data'
                self.table_name = 'mstmvw_substance_all_data'

                globals()['task1'] = KemiArbejde("Indhenter alle kemiske data...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['task1'])
            if 'Nitrat seneste' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Nitrat'
                self.table_name = 'mstmvw_substance_nitrate_latest'
                globals()['task2'] = KemiArbejde("Indhenter seneste nitratmålinger...", self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['task2'])
            if 'Sulfat seneste' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Sulfat'
                self.table_name = 'mstmvw_substance_sulphate_latest'
                globals()['task3'] = KemiArbejde("Indhenter seneste sulfatmålinger...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['task3'])
            if 'Sulfat-nitrat seneste' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Sulfat-nitrat'
                self.table_name = 'mstmvw_substance_nitrate_and_sulphate_latest'
                globals()['task4'] = KemiArbejde("Indhenter seneste nitrat- og sulfatmålinger...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['task4'])
            if 'Klorid seneste' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Klorid'
                self.table_name = 'mstmvw_substance_chloride_latest'
                globals()['task3chloride'] = KemiArbejde("Indhenter seneste kloridmålinger...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['task3chloride'])
            if 'Klorid-sulfat seneste' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Klorid-sulfat'

                self.table_name = 'mstmvw_substance_chloride_and_sulphate_latest'
                globals()['task5'] = KemiArbejde("Indhenter seneste klorid- og sulfatmålinger...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['task5'])
            if 'Jern seneste' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Jern'

                self.table_name = 'mstmvw_substance_iron_latest'
                globals()['task6'] = KemiArbejde("Indhenter seneste jernmålinger...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['task6'])
            if 'NVOC seneste' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'NVOC'

                self.table_name = 'mstmvw_substance_nvoc_latest'
                globals()['task7'] = KemiArbejde("Indhenter seneste NVOC-målinger...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['task7'])
            if 'Arsen seneste' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Arsen'

                self.table_name = 'mstmvw_substance_arsenic_latest'
                globals()['task8'] = KemiArbejde("Indhenter seneste arsenmålinger...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['task8'])
            if 'Strontium seneste' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Strontium'

                self.table_name = 'mstmvw_substance_strontium_latest'
                globals()['task9'] = KemiArbejde("Indhenter seneste strontiummålinger...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['task9'])
            if 'Ionbytning seneste' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Ionbytning'

                self.table_name = 'mstmvw_substance_sodium_chloride_ionexchange_latest'
                globals()['task10'] = KemiArbejde("Indhenter seneste ionbytningsmålinger...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['task10'])
            if 'Vandtype seneste' in checked_items:
                self.repaint()
                QApplication.processEvents()
                self.item = 'Vandtype'

                self.table_name = 'mstmvw_substance_watertype_latest'
                globals()['task11'] = KemiArbejde("Indhenter seneste vandtyper...",self.table_name, self.item, self)
                QgsApplication.taskManager().addTask(globals()['task11'])

            if 1 in geus:
                class KemiArbejde2(QgsTask):
                    def __init__(self, desc, Dialog, stofgruppe, navn):
                        self.desc = desc
                        self.result_layer = None
                        self.Dialog = Dialog
                        self.vlayer = None
                        self.stofgruppe = stofgruppe
                        self.navn = navn
                        QgsTask.__init__(self, self.desc)

                    def run(self):
                        def parse_geometry(geometry):
                            regex = r'[0-9-\.]+'
                            parsed_geom = re.findall(regex, geometry)
                            parsed_geom = [float(i) for i in parsed_geom]
                            return min(parsed_geom[::2]), max(parsed_geom[::2]), min(parsed_geom[1::2]), max(
                                parsed_geom[1::2])

                        boundingbox = parse_geometry(geom.asWkt())
                        boundingbox = list(boundingbox)

                        url = "pagingEnabled='true' preferCoordinatesForWfsT11='True' srsname='{}' typename='mc_grp_analyse' url='https://data.geus.dk/geusmap/ows/{}.jsp?whoami=makyn@mst.dk&LAYERS=mc_grp_analyse&mapname=grundvand&bbox={},{},{},{}&url='https://data.geus.dk/geusmap/ows/{}.jsp?whoami=makyn@mst.dk&LAYERS=mc_grp_analyse&version=1.0.0&mapname=grundvand&bbox={},{},{},{}'".format(
                            layer.crs().authid(), crstrue, boundingbox[0], boundingbox[2], boundingbox[1],
                            boundingbox[3], crstrue, boundingbox[0], boundingbox[2],
                            boundingbox[1], boundingbox[3])

                        if self.isCanceled():
                            return False
                        vlayer = QgsVectorLayer(url, "{}".format(self.navn), "wfs")
                        vlayer.updateFields()

                        l = 0
                        for k, feat in enumerate(vlayer.getFeatures()):
                            l += 1

                        self.vlayer = vlayer
                        try:
                            if vlayer.featureCount() == 20000:
                                msg = QMessageBox.warning(None, 'OBS',
                                                          "Område er for stort. Ingen garanti for at alt data er med.")
                                msg.exec_()
                            else:
                                pass
                        except:
                            pass

                        if self.isCanceled():
                            return False

                        områdelayer = project.mapLayersByName('{}'.format(key))[0]

                        a = processing.run("native:clip", {'INPUT': vlayer,
                                                           'OVERLAY': områdelayer, 'OUTPUT': 'TEMPORARY_OUTPUT'})
                        a["OUTPUT"].updateFields()

                        aa = processing.run("native:extractbyattribute", {'INPUT': a["OUTPUT"],
                                'FIELD': 'stofgruppe', 'OPERATOR': 0, 'VALUE': '{}'.format(self.stofgruppe), 'OUTPUT': 'TEMPORARY_OUTPUT'})

                        aa["OUTPUT"].updateFields()

                        aa["OUTPUT"].setName("{} - {}".format(self.navn, key))
                        try:
                            feats_count = aa["OUTPUT"].featureCount()
                        except:
                            pass

                        if feats_count > 0.5:
                            self.result_layer = aa["OUTPUT"]
                        else:
                            self.result_layer = None

                        return True

                    def finished(self, result):
                        if self.isCanceled():
                            QMessageBox.warning(None, 'Meddelse', "Opgaven blev stoppet af bruger.")
                        else:
                            try:
                                if self.vlayer.featureCount() > 18000 and self.result_layer is None:
                                    QMessageBox.warning(None, 'OBS', 'Område er for stort.')
                                    pass
                                else:
                                    if self.result_layer is None:
                                        QMessageBox.warning(None, 'Meddelse', "Intet data for dette område ({}).".format(self.navn))
                                        pass
                                    else:
                                        self.result_layer.moveToThread(QCoreApplication.instance().thread())
                                        self.result_layer.loadNamedStyle(
                                            str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/Kemi - Pesticider.qml")
                                        project.addMapLayer(self.result_layer)
                                        del self.vlayer
                            except:
                                QMessageBox.warning(None, 'Meddelse', "Intet data for dette område ({}).".format(self.navn))
                        self.Dialog.pushButton_Kemi.setText("Eksportér til QGIS")
                        self.Dialog.pushButton_Kemi.setEnabled(True)
                        self.Dialog.repaint()
                        QApplication.processEvents()

                self.repaint()
                QApplication.processEvents()
                if pest == 1:
                    globals()['task12'] = KemiArbejde2("Indhenter pesticider...",self, stofgruppe=50, navn="Pesticider GEUS")
                    QgsApplication.taskManager().addTask(globals()['task12'])
                if pfas == 1:
                    globals()['task123'] = KemiArbejde2("Indhenter PFAS...",self, stofgruppe=110, navn="PFAS GEUS")
                    QgsApplication.taskManager().addTask(globals()['task123'])
                if uorganiskesporstoffer == 1:
                    globals()['task1234'] = KemiArbejde2("Indhenter uorganiske sporstoffer...",self, stofgruppe=30, navn="Uorganiske sporstoffer GEUS")
                    QgsApplication.taskManager().addTask(globals()['task1234'])
                if organiskmikro == 1:
                    globals()['task12345'] = KemiArbejde2("Indhenter organisk mikroforurening...",self, stofgruppe=40, navn="Organisk mikroforurening GEUS")
                    QgsApplication.taskManager().addTask(globals()['task12345'])
                if aromatiskekulbrinter == 1:
                    globals()['task123456'] = KemiArbejde2("Indhenter aromatiske kulbrinter...",self, stofgruppe=41, navn="Aromatiske kulbrinter GEUS")
                    QgsApplication.taskManager().addTask(globals()['task123456'])
                if blødgørere == 1:
                    globals()['task1234567'] = KemiArbejde2("Indhenter phthalater...",self, stofgruppe=42, navn="Blødgørere/Phthalater GEUS")
                    QgsApplication.taskManager().addTask(globals()['task1234567'])
                if detergenter == 1:
                    globals()['task12345678'] = KemiArbejde2("Indhenter detergenter...",self, stofgruppe=43, navn="Detergenter (sæbe) GEUS")
                    QgsApplication.taskManager().addTask(globals()['task12345678'])
                if fenoler == 1:
                    globals()['task123456789'] = KemiArbejde2("Indhenter chlorerede og bromerede fenoler...",self, stofgruppe=44, navn="Chlorerede og bromerede fenoler GEUS")
                    QgsApplication.taskManager().addTask(globals()['task123456789'])
                if alifatiskekulbrinter == 1:
                    globals()['task1234567891'] = KemiArbejde2("Indhenter alifatiske kulbrinter...",self, stofgruppe=45, navn="Alifatiske kulbrinter GEUS")
                    QgsApplication.taskManager().addTask(globals()['task1234567891'])
                if dioxiner == 1:
                    globals()['task12345678912'] = KemiArbejde2("Indhenter halogenerede flammehæmmere, PCB'er og dioxiner...",self, stofgruppe=48, navn="Halogenerede flammehæmmere, PCB og dioxiner GEUS")
                    QgsApplication.taskManager().addTask(globals()['task12345678912'])
                if organometaller == 1:
                    globals()['task123456789123'] = KemiArbejde2("Indhenter organometaller...",self, stofgruppe=49, navn="Organometaller GEUS")
                    QgsApplication.taskManager().addTask(globals()['task123456789123'])
                if haloalifatiskekulbrinter == 1:
                    globals()['task1234567891234'] = KemiArbejde2("Indhenter halogenerede alifatiske kulbrinter...",self, stofgruppe=100, navn="Halogenerede alifatiske kulbrinter GEUS")
                    QgsApplication.taskManager().addTask(globals()['task1234567891234'])
            self.repaint()
            QApplication.processEvents()
        else:
            QMessageBox.warning(None, 'Fejl', "Vælg dit output.")
    else:
        QMessageBox.warning(None, 'Fejl', "Intet input (områdepolygon).")