# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import (
    QgsFields,
    QgsField,
    QgsFeature,
QgsLayerTreeGroup,
    QgsGeometry,
    QgsFeatureRequest,
    QgsProject,
QgsSimpleMarkerSymbolLayerBase,
    QgsRaster,
    QgsTask,
    QgsPrintLayout,
    QgsReadWriteContext,
    QgsRasterFileWriter,
    QgsMapLayer,
    QgsLayoutItemLegend,
    QgsLayoutItemMap,
    QgsLegendRenderer,
    QgsExpression,
    QgsLegendStyle,
    QgsLayoutExporter,
    QgsLayoutItemPicture,
    QgsLayoutPoint,
    QgsLayoutSize,
    QgsUnitTypes,
    QgsRasterBandStats,
    QgsCategorizedSymbolRenderer,
    QgsRendererCategory,
    QgsSymbol,
    QgsCoordinateTransformContext,
QgsRuleBasedRenderer,
    QgsRandomColorRamp,
    QgsMapThemeCollection,
    QgsLayerDefinition,
    QgsPointXY,
    QgsApplication,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsExpressionContextScope,
    QgsCoordinateReferenceSystem,
    QgsProcessingFeatureSourceDefinition,
    QgsRectangle,
    QgsTaskManager,
    QgsLayout,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsRasterLayer,
    QgsWkbTypes,
    QgsDataSourceUri
)
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()

import pyodbc

from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass

__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *
from ..doDataExtract import *


def osd_one(self):
    if True:
        self.OSDMagasinButton.setText("Kører")
        self.OSDMagasinButton.setEnabled(False)
        self.repaint()
    try:
        global pluginpath
        pluginpath, _filter = QFileDialog.getOpenFileNames(self,
                                                           caption='Vælg magasinudbredelse-fil(er).')
        global values
        if len(pluginpath) == 1 and pluginpath[0].endswith('shp') or len(pluginpath) == 1 and pluginpath[0].endswith(
                'gpkg'):
            memlay = QgsVectorLayer(str(pluginpath[0]), 'ogr')
            idx = memlay.fields().indexOf('magasinlag')
            values = memlay.uniqueValues(idx)
        elif len(pluginpath) > 1 and pluginpath[0].endswith('shp') or len(pluginpath) > 1 and pluginpath[0].endswith(
                'gpkg'):
            values = []
            for fil in pluginpath:
                memlay = QgsVectorLayer(str(fil), 'ogr')
                idx = memlay.fields().indexOf('magasinlag')
                values.append(', '.join(memlay.uniqueValues(idx)))
        else:
            values = []
            for fil in pluginpath:
                start = fil.rfind("/")
                slut = fil.rfind(".")
                values.append(fil[start + 1:slut])

        rækker = len(values)
        self.magasinTable.setRowCount(rækker)
        self.magasinTable.setVerticalHeaderLabels(values)
        self.OSDMagasinButton.setText("Færdig ✓")
        self.repaint()

    except:
        pass




def osd_data0(self):
    self.tabWidget_4.setCurrentIndex(0)
    global pluginpath
    pluginpath, _filter = QFileDialog.getOpenFileNames(self,
                                                       caption='Vælg magasinudbredelse-fil(er).')
    self.lineEdit_6.setText(str(pluginpath))
    global values
    if len(pluginpath) == 1 and pluginpath[0].endswith('shp') or len(pluginpath) == 1 and pluginpath[0].endswith(
            'gpkg'):
        memlay = QgsVectorLayer(str(pluginpath[0]), 'ogr')
        idx = memlay.fields().indexOf('magasinlag')
        values = memlay.uniqueValues(idx)
    elif len(pluginpath) > 1 and pluginpath[0].endswith('shp') or len(pluginpath) > 1 and pluginpath[0].endswith(
            'gpkg'):
        values = []
        for fil in pluginpath:
            memlay = QgsVectorLayer(str(fil), 'ogr')
            idx = memlay.fields().indexOf('magasinlag')
            values.append(', '.join(memlay.uniqueValues(idx)))
    else:
        values = []
        for fil in pluginpath:
            start = fil.rfind("/")
            slut = fil.rfind(".")
            values.append(fil[start + 1:slut])

    rækker = len(values)

    self.magudbred.setRowCount(rækker)
    self.magudbred.setVerticalHeaderLabels(values)

    for rows in range(self.magudbred.rowCount()):
        item = QtWidgets.QTableWidgetItem()
        item.setCheckState(Qt.Unchecked)
        self.magudbred.setItem(rows, 0, item)


def osd_data1(self):
    global plugin_path_grundvandsdannelse
    # filter = "Shapefil(*.shp);;GeoPackage(*.gpkg);;Raster(*.tiff);;Raster(*.asc)"
    plugin_path_grundvandsdannelse, _filter = QFileDialog.getOpenFileNames(self,
                                                                           caption='Vælg grundvandsdannelse-fil.')  # , filter=filter
    self.lineEdit_2.setText(str(plugin_path_grundvandsdannelse))


def osd_data2(self):
    self.tabWidget_4.setCurrentIndex(1)
    global plugin_path_dæklagstykkelse
    plugin_path_dæklagstykkelse, _filter = QFileDialog.getOpenFileNames(self,
                                                                        caption='Vælg dæklagstykkelse-fil.')
    self.lineEdit_3.setText(str(plugin_path_dæklagstykkelse))
    global lerlagellermagasin
    global valuesdæklag
    if len(plugin_path_dæklagstykkelse) == 1 and plugin_path_dæklagstykkelse[0].endswith('shp') or len(
            plugin_path_dæklagstykkelse) == 1 and plugin_path_dæklagstykkelse[0].endswith('gpkg'):
        memlay = QgsVectorLayer(str(plugin_path_dæklagstykkelse[0]), 'ogr')
        idx = memlay.fields().indexOf('magasin')
        idy = memlay.fields().indexOf('list_lerla')
        valuesdæklag = memlay.uniqueValues(idx)
        lerlagellermagasin = memlay.uniqueValues(idy)
    elif len(plugin_path_dæklagstykkelse) > 1 and plugin_path_dæklagstykkelse[0].endswith('shp') or len(
            plugin_path_dæklagstykkelse) > 1 and plugin_path_dæklagstykkelse[0].endswith('gpkg'):
        valuesdæklag = []
        for fil in plugin_path_dæklagstykkelse:
            memlay = QgsVectorLayer(str(fil), 'ogr')
            idx = memlay.fields().indexOf('magasin')
            idy = memlay.fields().indexOf('list_lerla')
            valuesdæklag.append(', '.join(memlay.uniqueValues(idx)))
            lerlagellermagasin.append(', '.join(memlay.uniqueValues(idy)))
    else:
        valuesdæklag = []
        for fil in plugin_path_dæklagstykkelse:
            start = fil.rfind("/")
            slut = fil.rfind(".")
            valuesdæklag.append(fil[start + 1:slut])

    rækker = len(valuesdæklag)
    self.dklagstable.setRowCount(rækker)
    self.dklagstable.setVerticalHeaderLabels(valuesdæklag)

    def write_qtable_to_df(table):
        col_count = table.columnCount()
        row_count = table.rowCount()
        headers = [str(table.horizontalHeaderItem(i).text()) for i in range(col_count)]

        df_list = []
        for row in range(row_count):
            df_list2 = []
            for col in range(col_count):
                table_item = table.item(row, col)
                df_list2.append('' if table_item is None else str(table_item.text()))
            df_list.append(df_list2)

        df = pd.DataFrame(df_list, columns=headers)

        return df

    global dæklagstabel
    dæklagstabel = write_qtable_to_df(self.dklagstable)
    dæklagstabel["Magasinnavn"] = valuesdæklag

    for rows in range(self.dklagstable.rowCount()):
        item = QtWidgets.QTableWidgetItem()
        item.setCheckState(Qt.Unchecked)
        self.dklagstable.setItem(rows, 0, item)


def osd_data3(self):
    self.tabWidget_4.setCurrentIndex(2)
    global plugin_path_vandudvekseling
    plugin_path_vandudvekseling, _filter = QFileDialog.getOpenFileNames(self,
                                                                        caption='Vælg vandudvekseling-fil.')
    self.lineEdit_4.setText(str(plugin_path_vandudvekseling))
    global valuesvandudveksling
    if len(plugin_path_vandudvekseling) == 1 and plugin_path_vandudvekseling[0].endswith('shp') or len(
            plugin_path_vandudvekseling) == 1 and plugin_path_vandudvekseling[0].endswith('gpkg'):
        memlay = QgsVectorLayer(str(plugin_path_vandudvekseling[0]), 'ogr')
        idx = memlay.fields().indexOf('magasin')
        valuesvandudveksling = memlay.uniqueValues(idx)
    elif len(plugin_path_vandudvekseling) > 1 and plugin_path_vandudvekseling[0].endswith('shp') or len(
            plugin_path_vandudvekseling) > 1 and plugin_path_vandudvekseling[0].endswith('gpkg'):
        valuesvandudveksling = []
        for fil in plugin_path_vandudvekseling:
            memlay = QgsVectorLayer(str(fil), 'ogr')
            idx = memlay.fields().indexOf('magasin')
            valuesvandudveksling.append(', '.join(memlay.uniqueValues(idx)))
    else:
        valuesvandudveksling = []
        for fil in plugin_path_vandudvekseling:
            start = fil.rfind("/")
            slut = fil.rfind(".")
            valuesvandudveksling.append(fil[start + 1:slut])

    rækker = len(valuesvandudveksling)

    self.vandudvekslingstable.setRowCount(rækker)
    self.vandudvekslingstable.setVerticalHeaderLabels(valuesvandudveksling)

    def write_qtable_to_df(table):
        col_count = table.columnCount()
        row_count = table.rowCount()
        headers = [str(table.horizontalHeaderItem(i).text()) for i in range(col_count)]

        df_list = []
        for row in range(row_count):
            df_list2 = []
            for col in range(col_count):
                table_item = table.item(row, col)
                df_list2.append('' if table_item is None else str(table_item.text()))
            df_list.append(df_list2)

        df = pd.DataFrame(df_list, columns=headers)

        return df

    global vandudvekslingstabel
    vandudvekslingstabel = write_qtable_to_df(self.vandudvekslingstable)
    vandudvekslingstabel["Magasinnavn2"] = vandudvekslingstabel

    for rows in range(self.vandudvekslingstable.rowCount()):
        item = QtWidgets.QTableWidgetItem()

        item.setCheckState(Qt.Unchecked)
        self.vandudvekslingstable.setItem(rows, 0, item)


def osd_data4(self):
    global plugin_path_indsatsområdepolygon
    plugin_path_indsatsområdepolygon, _filter = QFileDialog.getOpenFileNames(self,
                                                                             caption='Vælg indsatsområdepolygon-fil.')
    self.lineEdit_5.setText(str(plugin_path_indsatsområdepolygon))


def osd_two(self):
    if self.checkBox_7.isChecked() and len(self.lineEdit_2.text()) < 3 or self.checkBox_8.isChecked() and len(
            self.lineEdit_3.text()) < 3 or self.checkBox_9.isChecked() and len(
            self.lineEdit_4.text()) < 3 or self.checkBox_10.isChecked() and len(
            self.lineEdit_6.text()) < 3 or self.checkBox_11.isChecked() and len(self.lineEdit_6.text()) < 3:
        QMessageBox.warning(None, 'Fejl', "Tjek dine afkrydsninger.")
    else:

        self.testerknap.setText("Kører")
        self.testerknap.setEnabled(False)
        self.repaint()
        try:
            os.makedirs("C:/Temp/OSDRevision")
        except:
            pass

        project = QgsProject.instance()
        key = self.comboBox.currentText()

        områdelayer = project.mapLayersByName('{}'.format(key))[0]

        for kk, feature in enumerate(områdelayer.getFeatures()):
            if kk == 0:
                geom = feature.geometry()
            else:
                geom = geom.combine(feature.geometry())

        root = project.layerTreeRoot()
        group = root.insertGroup(0, "Temporary")

        def write_qtable_to_df(table):
            col_count = table.columnCount()
            row_count = table.rowCount()
            headers = [str(table.horizontalHeaderItem(i).text()) for i in range(col_count)]

            df_list = []
            for row in range(row_count):
                df_list2 = []
                for col in range(col_count):
                    table_item = table.item(row, col)
                    df_list2.append('' if table_item is None else str(table_item.text()))
                df_list.append(df_list2)

            df = pd.DataFrame(df_list, columns=headers)

            return df

        try:
            for row in range(self.magudbred.rowCount()):
                item = self.magudbred.item(row, 0)
                if item.checkState() != 0:
                    self.magudbred.item(row, 0).setText("1")

            magasintildeling = write_qtable_to_df(self.magudbred)
            magasintildeling["Magasinnavn"] = values

            magasin = magasintildeling.loc[magasintildeling['Magasiner'] == "1"]
            magasintykkelsesamling = magasin["Magasinnavn"].tolist()
        except:
            pass

        parametre = 0
        global para
        para = []
        global Magasintykkelse
        Magasintykkelse = []

        if self.checkBox_11.isChecked():
            parametre += 1
            para.append("Samlet Magasintykkelse")
            try:
                for x in pluginpath:
                    if x.endswith('shp') or x.endswith('gpkg'):
                        start = x.rfind("/")
                        slut = x.rfind(".")
                        navn = x[start + 1:slut]
                        memselay = QgsVectorLayer(str(x), str(navn))
                        try:
                            for k in magasintykkelsesamling:
                                mee = processing.run("native:extractbyattribute", {
                                    'INPUT': memselay,
                                    'FIELD': 'magasinlag', 'OPERATOR': 0, 'VALUE': k, 'OUTPUT': 'TEMPORARY_OUTPUT'})[
                                    'OUTPUT']

                                meemm = processing.run("gdal:rasterize",
                                                       {'INPUT': mee, 'FIELD': 'tykkelse', 'BURN': 0, 'USE_Z': False,
                                                        'UNITS': 1, 'WIDTH': 100,
                                                        'HEIGHT': 100, 'EXTENT': None, 'NODATA': 0, 'OPTIONS': '',
                                                        'DATA_TYPE': 5,
                                                        'INIT': None, 'INVERT': False, 'EXTRA': '',
                                                        'OUTPUT': 'C:/Temp/OSDRevision/Magasintykkelsetemp - {} - {}.tif'.format(
                                                            key, k)})

                                processing.run("native:fillnodata",
                                               {'INPUT': 'C:/Temp/OSDRevision/Magasintykkelsetemp - {} - {}.tif'.format(
                                                   key, k),
                                                'BAND': 1,
                                                'FILL_VALUE': 0,
                                                'OUTPUT': 'C:/Temp/OSDRevision/Magasintykkelse - {} - {}.tif'.format(
                                                    key, k)})

                                meem = QgsRasterLayer(
                                    'C:/Temp/OSDRevision/Magasintykkelse - {} - {}.tif'.format(key, k),
                                    'Magasintykkelse - {} - {}'.format(key, k))

                                project.addMapLayer(meem, False)
                                group.addLayer(meem)
                                Magasintykkelse.append('Magasintykkelse - {} - {}'.format(key, k))
                        except:
                            pass
                    else:
                        start = x.rfind("/")
                        slut = x.rfind(".")
                        navn = str(x[start + 1:slut])
                        if navn in magasintykkelsesamling:
                            memlay = QgsRasterLayer(str(x), navn)
                            Magasintykkelse.append(navn)
                            project.addMapLayer(memlay, False)
                            group.addLayer(memlay)
                        else:
                            pass
            except:
                pass

        global Grundvandsdannelse
        Grundvandsdannelse = []

        if self.checkBox_7.isChecked():
            Grundvandsdannelse.append("Grundvandsdannelse - {}".format(key))
            parametre += 1
            para.append("Grundvandsdannelse")
            for x in plugin_path_grundvandsdannelse:
                if x.endswith('shp') or x.endswith('gpkg'):
                    print(x)
                    zlayy = QgsVectorLayer(str(x), "Grundvandsdannelse_temp")
                    zlay222 = processing.run("gdal:rasterize",
                                             {'INPUT': zlayy, 'FIELD': 'flux', 'BURN': 0, 'USE_Z': False, 'UNITS': 1,
                                              'WIDTH': 100, 'HEIGHT': 100, 'EXTENT': None, 'NODATA': 0, 'OPTIONS': '',
                                              'DATA_TYPE': 5, 'INIT': None, 'INVERT': False, 'EXTRA': '',
                                              'OUTPUT': 'C:/Temp/OSDRevision/Grundvandsdannelsetemp - {}.tif'.format(
                                                  key)})
                    processing.run("native:fillnodata",
                                   {'INPUT': 'C:/Temp/OSDRevision/Grundvandsdannelsetemp - {}.tif'.format(key),
                                    'BAND': 1,
                                    'FILL_VALUE': 0,
                                    'OUTPUT': 'C:/Temp/OSDRevision/Grundvandsdannelse - {}.tif'.format(key)})

                    zlay = QgsRasterLayer('C:/Temp/OSDRevision/Grundvandsdannelse - {}.tif'.format(key),
                                          'Grundvandsdannelse - {}'.format(key))

                    project.addMapLayer(zlay, False)
                    group.addLayer(zlay)
                else:
                    zlay = QgsRasterLayer(str(x), "Grundvandsdannelse - {}".format(key))
                    project.addMapLayer(zlay, False)
                    group.addLayer(zlay)

        try:
            for row in range(self.dklagstable.rowCount()):
                item = self.dklagstable.item(row, 0)
                if item.checkState() != 0:
                    self.dklagstable.item(row, 0).setText("1")
            magasintildelingdf2 = write_qtable_to_df(self.dklagstable)
            magasintildelingdf2["Magasinnavn"] = valuesdæklag
            magasin2 = magasintildelingdf2.loc[magasintildelingdf2['Magasiner'] == "1"]

            dæklagtykkelsesamling = magasin2["Magasinnavn"].tolist()
            print(dæklagtykkelsesamling)
            print(plugin_path_dæklagstykkelse)
        except:
            pass

        global Dæklagstykkelse
        Dæklagstykkelse = []

        if self.checkBox_8.isChecked():
            parametre += 1
            para.append("Samlet Dæklagstykkelse")
            for x in plugin_path_dæklagstykkelse:
                if x.endswith('shp') or x.endswith('gpkg'):
                    start = x.rfind("/")
                    slut = x.rfind(".")
                    navn = x[start + 1:slut]
                    zlay2 = QgsVectorLayer(str(x), str(navn))
                    try:
                        for x in dæklagtykkelsesamling:
                            if 2 in lerlagellermagasin and 3 not in lerlagellermagasin:
                                mee2 = processing.run("native:extractbyattribute", {
                                    'INPUT': zlay2,
                                    'FIELD': 'lerlag', 'OPERATOR': 0, 'VALUE': x, 'OUTPUT': 'TEMPORARY_OUTPUT'})[
                                    'OUTPUT']
                            elif 2 in lerlagellermagasin and 3 in lerlagellermagasin:
                                mee2 = processing.run("native:extractbyattribute", {
                                    'INPUT': zlay2,
                                    'FIELD': 'magasin', 'OPERATOR': 0, 'VALUE': x, 'OUTPUT': 'TEMPORARY_OUTPUT'})[
                                    'OUTPUT']
                            else:
                                mee2 = processing.run("native:extractbyattribute", {
                                    'INPUT': zlay2,
                                    'FIELD': 'magasin', 'OPERATOR': 0, 'VALUE': x, 'OUTPUT': 'TEMPORARY_OUTPUT'})[
                                    'OUTPUT']

                            meem22 = processing.run("gdal:rasterize",
                                                    {'INPUT': mee2, 'FIELD': 'tykkelse', 'BURN': 0, 'USE_Z': False,
                                                     'UNITS': 1, 'WIDTH': 100, 'HEIGHT': 100, 'EXTENT': None,
                                                     'NODATA': 0,
                                                     'OPTIONS': '', 'DATA_TYPE': 5, 'INIT': None, 'INVERT': False,
                                                     'EXTRA': '',
                                                     'OUTPUT': 'C:/Temp/OSDRevision/Dæklagstykkelsetemp - {} - {}.tif'.format(
                                                         key, x)})

                            processing.run("native:fillnodata",
                                           {'INPUT': 'C:/Temp/OSDRevision/Dæklagstykkelsetemp - {} - {}.tif'.format(key,
                                                                                                                    x),
                                            'BAND': 1,
                                            'FILL_VALUE': 0,
                                            'OUTPUT': 'C:/Temp/OSDRevision/Dæklagstykkelse - {} - {}.tif'.format(key,
                                                                                                                 x)})

                            meem2 = QgsRasterLayer('C:/Temp/OSDRevision/Dæklagstykkelse - {} - {}.tif'.format(key, x),
                                                   'Dæklagstykkelse - {} - {}'.format(key, x))
                            project.addMapLayer(meem2, False)
                            group.addLayer(meem2)
                            Dæklagstykkelse.append('Dæklagstykkelse - {} - {}'.format(key, x))
                    except:
                        pass
                else:
                    start = x.rfind("/")
                    slut = x.rfind(".")
                    navn = str(x[start + 1:slut])
                    if navn in dæklagtykkelsesamling:
                        zlay2 = QgsRasterLayer(str(x), str(navn))
                        Dæklagstykkelse.append(navn)
                        project.addMapLayer(zlay2, False)
                        group.addLayer(zlay2)
                #  project.addMapLayer(zlay2, False)
                #  group.addLayer(zlay2)

        try:
            for row in range(self.vandudvekslingstable.rowCount()):
                item = self.vandudvekslingstable.item(row, 0)
                if item.checkState() != 0:
                    self.vandudvekslingstable.item(row, 0).setText("1")
            magasintildelingdf3 = write_qtable_to_df(self.vandudvekslingstable)
            magasintildelingdf3["Magasinnavn2"] = valuesvandudveksling
            magasin3 = magasintildelingdf3.loc[magasintildelingdf3['Magasiner'] == "1"]

            vandudvekslingsamling = magasin3["Magasinnavn2"].tolist()
        except:
            pass

        global Vandudveksling
        Vandudveksling = []

        if self.checkBox_9.isChecked():
            parametre += 1
            para.append("Vandudveksling")

            for x in plugin_path_vandudvekseling:
                if x.endswith('shp') or x.endswith('gpkg'):
                    start = x.rfind("/")
                    slut = x.rfind(".")
                    navn = x[start + 1:slut]
                    zlayyy = QgsVectorLayer(str(x), str(navn))
                    try:
                        for x in vandudvekslingsamling:
                            print(x)
                            mee22 = processing.run("native:extractbyattribute", {
                                'INPUT': zlayyy,
                                'FIELD': 'magasin', 'OPERATOR': 0, 'VALUE': x, 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']
                            meem222 = processing.run("gdal:rasterize",
                                                     {'INPUT': mee22, 'FIELD': 'flux', 'BURN': 0, 'USE_Z': False,
                                                      'UNITS': 1, 'WIDTH': 100, 'HEIGHT': 100, 'EXTENT': None,
                                                      'NODATA': 0,
                                                      'OPTIONS': '', 'DATA_TYPE': 5, 'INIT': None, 'INVERT': False,
                                                      'EXTRA': '',
                                                      'OUTPUT': 'C:/Temp/OSDRevision/Vandudvekslingtemp - {} - {}.tif'.format(
                                                          key, x)})

                            processing.run("native:fillnodata",
                                           {'INPUT': 'C:/Temp/OSDRevision/Vandudvekslingtemp - {} - {}.tif'.format(key,
                                                                                                                   x),
                                            'BAND': 1,
                                            'FILL_VALUE': -999999999,
                                            'OUTPUT': 'C:/Temp/OSDRevision/Vandudveksling - {} - {}.tif'.format(key,
                                                                                                                x)})

                            meem2 = QgsRasterLayer('C:/Temp/OSDRevision/Vandudveksling - {} - {}.tif'.format(key, x),
                                                   'Vandudveksling - {} - {}'.format(key, x))
                            project.addMapLayer(meem2, False)
                            group.addLayer(meem2)
                            Vandudveksling.append('Vandudveksling - {} - {}'.format(key, x))


                    except:
                        pass
                else:
                    start = x.rfind("/")
                    slut = x.rfind(".")
                    navn = str(x[start + 1:slut])
                    zlay3 = QgsRasterLayer(str(x), str(navn))
                    if navn in vandudvekslingsamling:
                        processing.run("native:fillnodata",
                                       {'INPUT': zlay3, 'BAND': 1,
                                        'FILL_VALUE': -999999999,
                                        'OUTPUT': 'C:/Temp/OSDRevision/{}.tif'.format(str(navn))})
                        zlay33 = QgsRasterLayer('C:/Temp/OSDRevision/{}.tif'.format(str(navn)), str(navn))

                        project.addMapLayer(zlay33, False)
                        group.addLayer(zlay33)
                        Vandudveksling.append(navn)
                    else:
                        pass

                # Vandudveksling.append('Vandudveksling - {}'.format(x))

        global Indsatsområder
        Indsatsområder = []

        if self.checkBox_10.isChecked():
            parametre += 1
            para.append("Indsatsområder")
            Indsatsområder.append("Indsatsområder - {}".format(key))

            for x in plugin_path_indsatsområdepolygon:
                og = QgsVectorLayer(str(x), "Indsatsområder")
                og.selectAll()
                zlay44 = processing.run("native:saveselectedfeatures", {'INPUT': og, 'OUTPUT': 'memory:'})['OUTPUT']
                og.removeSelection()

                zlay44.startEditing()
                zlay44.dataProvider().addAttributes([QgsField('tykkelse', QVariant.String)])
                zlay44.updateFields()
                idx = zlay44.dataProvider().fieldNameIndex('tykkelse')

                features = [feat for feat in zlay44.getFeatures()]

                context = QgsExpressionContext()
                scope = QgsExpressionContextScope()
                context.appendScope(scope)
                listOfResults = []

                for f in zlay44.getFeatures():
                    context.setFeature(f)
                    exp = QgsExpression("1")
                    f['tykkelse'] = exp.evaluate(context)
                    zlay44.updateFeature(f)

                zlay44.commitChanges()

                zlay444 = processing.run("gdal:rasterize",
                                         {'INPUT': zlay44, 'FIELD': 'tykkelse', 'BURN': 0, 'USE_Z': False, 'UNITS': 1,
                                          'WIDTH': 80, 'HEIGHT': 80, 'EXTENT': områdelayer, 'NODATA': 0, 'OPTIONS': '',
                                          'DATA_TYPE': 5, 'INIT': None, 'INVERT': False, 'EXTRA': '',
                                          'OUTPUT': 'C:/Temp/OSDRevision/Indsatsområdertemp - {}.tif'.format(key)})

                processing.run("native:fillnodata",
                               {'INPUT': 'C:/Temp/OSDRevision/Indsatsområdertemp - {}.tif'.format(key), 'BAND': 1,
                                'FILL_VALUE': 0,
                                'OUTPUT': 'C:/Temp/OSDRevision/Indsatsområder - {}.tif'.format(key)})

                zlay4 = QgsRasterLayer('C:/Temp/OSDRevision/Indsatsområder - {}.tif'.format(key),
                                       'Indsatsområder - {}'.format(key))
                project.addMapLayer(zlay4, False)
                group.addLayer(zlay4)

        root2 = QgsProject.instance().layerTreeRoot()
        nodes = root2.children()

        for n in nodes:
            if isinstance(n, QgsLayerTreeGroup):
                if n.isExpanded() == True:
                    n.setExpanded(False)

        self.OSDTable.setRowCount(parametre)
        self.OSDTable.setVerticalHeaderLabels(para)

        for row, string in enumerate(para, 0):
            if "Indsatsområder" in string:
                chkBoxItem = QTableWidgetItem("X")
                chkBoxItem.setFlags(Qt.ItemIsEnabled)
                chkBoxItem.setTextAlignment(Qt.AlignCenter)

                chkBoxItem2 = QTableWidgetItem("X")
                chkBoxItem2.setFlags(Qt.ItemIsEnabled)
                chkBoxItem2.setTextAlignment(Qt.AlignCenter)

                self.OSDTable.setItem(row, 0, chkBoxItem)
                self.OSDTable.setItem(row, 1, chkBoxItem2)

        for row in range(self.OSDTable.rowCount()):
            try:
                chkBoxItem = QTableWidgetItem(f"{0}")
                chkBoxItem.setTextAlignment(Qt.AlignCenter)
                self.OSDTable.setItem(row, 2, chkBoxItem)
            except:
                pass

        self.testerknap.setText("Færdig ✓")

        self.repaint()
        self.tabWidget_3.setCurrentIndex(1)


def osd_final(self):
    def write_qtable_to_df(table):
        col_count = table.columnCount()
        row_count = table.rowCount()
        headers = [str(table.horizontalHeaderItem(i).text()) for i in range(col_count)]

        df_list = []
        for row in range(row_count):
            df_list2 = []
            for col in range(col_count):
                table_item = table.item(row, col)
                df_list2.append('' if table_item is None else str(table_item.text()))
            df_list.append(df_list2)

        df = pd.DataFrame(df_list, columns=headers)

        return df

    osd = write_qtable_to_df(self.OSDTable)
    osd["Parameter"] = para

    rows = self.OSDTable.rowCount()
    z = 0
    sum = 3 * int(rows)

    for column in range(3):
        for row in range(rows):
            it = osd.iat[row, column]
            if it:  # and it.text()
                z += 1

    if z == sum:
        self.OSDDone.setText("Kører")
        self.OSDDone.setEnabled(False)
        self.repaint()
        try:
            os.makedirs("C:/Temp/OSDRevision")
        except:
            pass
        # path = 'C:/Temp/OSDRevision'
        project = QgsProject.instance()
        key = self.comboBox.currentText()

        områdelayer = project.mapLayersByName('{}'.format(key))[0]

        for kk, feature in enumerate(områdelayer.getFeatures()):
            if kk == 0:
                geom = feature.geometry()
            else:
                geom = geom.combine(feature.geometry())

        root = project.layerTreeRoot()
        groupold = root.findGroup("Temporary")

        for layers in groupold.children():
            layers.setItemVisibilityChecked(False)
        global iteration

        if root.findGroup("Sammenligning"):
            group = root.findGroup("Sammenligning")
            iteration += 1
        else:
            group = root.insertGroup(0, "Sammenligning")
            iteration = 1

        try:
            magasintykkelse_min = float(osd.loc[osd['Parameter'] == "Samlet Magasintykkelse", 'Minimum'].iloc[0])
            magasintykkelse_max = float(osd.loc[osd['Parameter'] == "Samlet Magasintykkelse", 'Maksimum'].iloc[0])
            magasintykkelse_vægt = (float(osd.loc[osd['Parameter'] == "Samlet Magasintykkelse", 'Vægtning'].iloc[0]) /
                                    osd["Vægtning"].astype(float).sum()) * 100
        except:
            pass
        try:
            dæklagstykkelse_min = float(osd.loc[osd['Parameter'] == "Samlet Dæklagstykkelse", 'Minimum'].iloc[0])
            dæklagstykkelse_max = float(osd.loc[osd['Parameter'] == "Samlet Dæklagstykkelse", 'Maksimum'].iloc[0])
            dæklagstykkelse_vægt = (float(osd.loc[osd['Parameter'] == "Samlet Dæklagstykkelse", 'Vægtning'].iloc[0]) /
                                    osd["Vægtning"].astype(float).sum()) * 100
        except:
            pass
        try:
            grundvandsdannelse_min = float(osd.loc[osd['Parameter'] == "Grundvandsdannelse", 'Minimum'].iloc[0])
            grundvandsdannelse_max = float(osd.loc[osd['Parameter'] == "Grundvandsdannelse", 'Maksimum'].iloc[0])
            grundvandsdannelse_vægt = (float(osd.loc[osd['Parameter'] == "Grundvandsdannelse", 'Vægtning'].iloc[0]) /
                                       osd["Vægtning"].astype(float).sum()) * 100
        except:
            pass
        try:
            vandudveksling_min = float(osd.loc[osd['Parameter'] == "Vandudveksling", 'Minimum'].iloc[0])
            vandudveksling_max = float(osd.loc[osd['Parameter'] == "Vandudveksling", 'Maksimum'].iloc[0])
            vandudveksling_vægt = (float(osd.loc[osd['Parameter'] == "Vandudveksling", 'Vægtning'].iloc[0]) / osd[
                "Vægtning"].astype(int).sum()) * 100
        except:
            pass
        try:
            indsatsområde_vægt = (float(osd.loc[osd['Parameter'] == "Indsatsområder", 'Vægtning'].iloc[0]) / osd[
                "Vægtning"].astype(int).sum()) * 100
        except:
            pass

        if Magasintykkelse:
            if project.mapLayersByName("SamletMagasinTykkelse - {}".format(key)):
                pass
            else:
                expr = ''
                rasters = []
                if len(Magasintykkelse) == 1:
                    for i, suffix in enumerate(Magasintykkelse):
                        inputrasterfile = suffix  # QgsRasterLayer(inputpath + suffix + ".tif", suffix)
                        rasters.append(inputrasterfile)
                        expr = expr + '"{}@1"'.format(suffix)
                else:
                    for i, suffix in enumerate(Magasintykkelse):
                        inputrasterfile = suffix  # QgsRasterLayer(inputpath + suffix + ".tif", suffix)
                        rasters.append(inputrasterfile)

                        expr = expr + '"{}@1"+'.format(suffix)

                    expr = expr.rstrip('+')

                alg_params = {
                    'CELLSIZE': 0,
                    'CRS': None,
                    'EXPRESSION': expr,
                    'EXTENT': områdelayer,
                    'LAYERS': rasters,
                    'OUTPUT': 'C:/Temp/OSDRevision/SamletMagasinTykkelse - {}.tif'.format(key)}

                result = processing.run('qgis:rastercalculator', alg_params)
                laydat = QgsRasterLayer('C:/Temp/OSDRevision/SamletMagasinTykkelse - {}.tif'.format(key),
                                        "SamletMagasinTykkelse - {}".format(key))
                project.addMapLayer(laydat, False)
                group.addLayer(laydat)

        if Dæklagstykkelse and self.checkBox_8.isChecked():
            if project.mapLayersByName("SamletDæklagsTykkelse - {}".format(key)):
                pass
            else:
                expr = ''
                rasters = []
                if len(Dæklagstykkelse) == 1:
                    for i, suffix in enumerate(Dæklagstykkelse):
                        inputrasterfile = suffix  # QgsRasterLayer(inputpath + suffix + ".tif", suffix)
                        rasters.append(inputrasterfile)
                        expr = expr + '"{}@1"'.format(suffix)
                else:

                    for i, suffix in enumerate(Dæklagstykkelse):
                        inputrasterfile = suffix  # QgsRasterLayer(inputpath + suffix + ".tif", suffix)
                        rasters.append(inputrasterfile)

                        expr = expr + '"{}@1"+'.format(suffix)

                    expr = expr.rstrip('+')

                alg_params = {
                    'CELLSIZE': 0,
                    'CRS': None,
                    'EXPRESSION': expr,
                    'EXTENT': områdelayer,
                    'LAYERS': rasters,
                    'OUTPUT': 'C:/Temp/OSDRevision/SamletDæklagsTykkelse - {}.tif'.format(key)}

                result = processing.run('qgis:rastercalculator', alg_params)

                laydat2 = QgsRasterLayer('C:/Temp/OSDRevision/SamletDæklagsTykkelse - {}.tif'.format(key),
                                         "SamletDæklagsTykkelse - {}".format(key))
                project.addMapLayer(laydat2, False)
                group.addLayer(laydat2)

        if Vandudveksling and self.checkBox_9.isChecked():
            if project.mapLayersByName("SamletVandudveksling - {}".format(key)):
                pass
            else:
                expr = ''
                rasters = []
                if len(Vandudveksling) == 1:
                    for i, suffix in enumerate(Vandudveksling):
                        inputrasterfile = suffix  # QgsRasterLayer(inputpath + suffix + ".tif", suffix)
                        rasters.append(inputrasterfile)
                        expr = expr + '"{}@1"'.format(suffix)
                else:

                    for i, suffix in enumerate(Vandudveksling):
                        inputrasterfile = suffix  # QgsRasterLayer(inputpath + suffix + ".tif", suffix)
                        rasters.append(inputrasterfile)

                        expr = expr + '"{}@1"+'.format(suffix)

                    expr = expr.rstrip('+')

                alg_params = {
                    'CELLSIZE': 0,
                    'CRS': None,
                    'EXPRESSION': expr,
                    'EXTENT': områdelayer,
                    'LAYERS': rasters,
                    'OUTPUT': 'C:/Temp/OSDRevision/SamletVandudveksling - {}.tif'.format(key)}

                processing.run('qgis:rastercalculator', alg_params)

                laydat2 = QgsRasterLayer('C:/Temp/OSDRevision/SamletVandudveksling - {}.tif'.format(key),
                                         "SamletVandudveksling - {}".format(key))
                project.addMapLayer(laydat2, False)
                group.addLayer(laydat2)

        if Grundvandsdannelse and self.checkBox_7.isChecked():
            if project.mapLayersByName("SamletGrundvandsdannelse - {}".format(key)):
                pass
            else:
                expr = ''
                rasters = []
                print(Grundvandsdannelse)
                if len(Grundvandsdannelse) == 1:
                    for i, suffix in enumerate(Grundvandsdannelse):
                        inputrasterfile = suffix  # QgsRasterLayer(inputpath + suffix + ".tif", suffix)
                        rasters.append(inputrasterfile)

                        expr = expr + '"{}@1"'.format(suffix)
                else:
                    for i, suffix in enumerate(Grundvandsdannelse):
                        inputrasterfile = suffix  # QgsRasterLayer(inputpath + suffix + ".tif", suffix)
                        rasters.append(inputrasterfile)

                        expr = expr + '"{}@1"+'.format(suffix)
                    expr = expr.rstrip('+')

                alg_params = {
                    'CELLSIZE': 0,
                    'CRS': None,
                    'EXPRESSION': expr,
                    'EXTENT': områdelayer,
                    'LAYERS': rasters,
                    'OUTPUT': 'C:/Temp/OSDRevision/SamletGrundvandsdannelse - {}.tif'.format(key)}

                result = processing.run('qgis:rastercalculator', alg_params)

                laydat23 = QgsRasterLayer('C:/Temp/OSDRevision/SamletGrundvandsdannelse - {}.tif'.format(key),
                                          "SamletGrundvandsdannelse - {}".format(key))
                project.addMapLayer(laydat23, False)
                group.addLayer(laydat23)

        if Indsatsområder and self.checkBox_10.isChecked():
            if project.mapLayersByName("SamletIO - {}".format(key)):
                pass
            else:
                expr = ''
                rasters = []
                if len(Indsatsområder) == 1:
                    for i, suffix in enumerate(Indsatsområder):
                        inputrasterfile = suffix  # QgsRasterLayer(inputpath + suffix + ".tif", suffix)
                        rasters.append(inputrasterfile)
                        expr = expr + '"{}@1"'.format(suffix)
                else:
                    for i, suffix in enumerate(Indsatsområder):
                        inputrasterfile = suffix  # QgsRasterLayer(inputpath + suffix + ".tif", suffix)
                        rasters.append(inputrasterfile)
                        expr = expr + '"{}@1"+'.format(suffix)
                    expr = expr.rstrip('+')
                print(expr)
                alg_params = {
                    'CELLSIZE': 0,
                    'CRS': None,
                    'EXPRESSION': expr,
                    'EXTENT': områdelayer,
                    'LAYERS': rasters,
                    'OUTPUT': 'C:/Temp/OSDRevision/SamletIO - {}.tif'.format(key)}

                result = processing.run('qgis:rastercalculator', alg_params)

                laydat22 = QgsRasterLayer('C:/Temp/OSDRevision/SamletIO - {}.tif'.format(key),
                                          "SamletIO - {}".format(key))
                project.addMapLayer(laydat22, False)
                group.addLayer(laydat22)

        for layers in group.children():
            layers.setItemVisibilityChecked(False)

        if root.findGroup("OSD"):
            grouposd2 = root.findGroup("OSD")
        else:
            grouposd2 = root.insertGroup(0, "OSD")

        grouposd = grouposd2.insertGroup(0, "{}. Iteration".format(iteration))

        if True:
            rasters = []
            expr = ''

            for layers in grouposd.children():
                layers.setItemVisibilityChecked(False)

            if Dæklagstykkelse and self.checkBox_8.isChecked():
                for i, suffix in enumerate(Dæklagstykkelse):
                    inputrasterfile = suffix
                    rasters.append(inputrasterfile)
                for i, x in reversed(list(enumerate(
                        np.linspace(dæklagstykkelse_min, dæklagstykkelse_max, int(dæklagstykkelse_vægt))))):
                    i += 1
                    expr = expr + f'if("SamletDæklagsTykkelse - {key}@1">={x},{i},'

                expr = expr + '0'

                for x in np.linspace(dæklagstykkelse_min, dæklagstykkelse_max, int(dæklagstykkelse_vægt)):
                    expr = expr + ')'

            if Vandudveksling and self.checkBox_9.isChecked():
                expr = expr + ' + '

                for i, suffix in enumerate(Vandudveksling):
                    inputrasterfile = suffix
                    rasters.append(inputrasterfile)
                for i, x in reversed(
                        list(enumerate(np.linspace(vandudveksling_min, vandudveksling_max, int(vandudveksling_vægt))))):
                    i += 1
                    expr = expr + f'if("SamletVandudveksling - {key}@1">={x},{i},'

                expr = expr + '0'

                for x in np.linspace(vandudveksling_min, vandudveksling_max, int(vandudveksling_vægt)):
                    expr = expr + ')'

            if Magasintykkelse:
                expr = expr + ' + '

                for i, suffix in enumerate(Magasintykkelse):
                    inputrasterfile = suffix
                    rasters.append(inputrasterfile)
                for i, x in reversed(list(enumerate(
                        np.linspace(magasintykkelse_min, magasintykkelse_max, int(magasintykkelse_vægt))))):
                    i += 1
                    expr = expr + f'if("SamletMagasinTykkelse - {key}@1">={x},{i},'
                expr = expr + '0'
                for x in np.linspace(magasintykkelse_min, magasintykkelse_max, int(magasintykkelse_vægt)):
                    expr = expr + ')'

            if Grundvandsdannelse and self.checkBox_7.isChecked():
                expr = expr + ' + '
                for i, suffix in enumerate(Grundvandsdannelse):
                    inputrasterfile = suffix
                    rasters.append(inputrasterfile)
                for i, x in reversed(list(enumerate(
                        np.linspace(grundvandsdannelse_min, grundvandsdannelse_max, int(grundvandsdannelse_vægt))))):
                    i += 1
                    expr = expr + f'if("SamletGrundvandsdannelse - {key}@1">={x},{i},'
                expr = expr + '0'
                for x in np.linspace(grundvandsdannelse_min, grundvandsdannelse_max, int(grundvandsdannelse_vægt)):
                    expr = expr + ')'

            if Indsatsområder and self.checkBox_10.isChecked():
                expr = expr + ' + '
                for i, suffix in enumerate(Indsatsområder):
                    inputrasterfile = suffix
                    rasters.append(inputrasterfile)
                expr = expr + f'if("SamletIO - {key}@1">=1,{indsatsområde_vægt},0)'

            print(expr)

            alg_params = {
                'CELLSIZE': 0,
                'CRS': None,
                'EXPRESSION': expr,
                'EXTENT': områdelayer,
                'LAYERS': rasters,
                'OUTPUT': 'C:/Temp/OSDRevision/OSD_revision {} - {}.tif'.format(key, iteration)}

            result = processing.run('qgis:rastercalculator', alg_params)
            laydat222 = QgsRasterLayer('C:/Temp/OSDRevision/OSD_revision {} - {}.tif'.format(key, iteration),
                                       'OSD_revision - {} - {}'.format(key, iteration))
            laydat3 = laydat222.clone()
            laydat3.loadNamedStyle(str(os.path.join( os.path.dirname( __file__ ), '..' )) + "/Styles/OSD_Revision.qml")

            project.addMapLayer(laydat3, False)
            grouposd.addLayer(laydat3)

            osd.to_csv('C:/Temp/OSDRevision/{}-Iteration.csv'.format(iteration), index=False, encoding='utf-8')

            path2 = 'file:///' + 'C:/Temp/OSDRevision/{}-Iteration.csv'.format(iteration)

            mylayer2 = QgsVectorLayer(path2, "{}-Iteration".format(iteration), "delimitedtext")

            project.addMapLayer(mylayer2, False)
            grouposd.addLayer(mylayer2)

            days_ago = time.time() - (3 * 86400)
            root = 'C:/Temp/OSDRevision'
            # List all files and folders in the root directory
            for i in os.listdir(root):
                path = os.path.join(root, i)

                if os.stat(path).st_mtime <= days_ago:
                    if os.path.isfile(path):
                        try:
                            os.remove(path)
                        except:
                            print("Could not remove file: ", i)
                    else:
                        pass

            root2 = QgsProject.instance().layerTreeRoot()
            nodes = root2.children()

            for n in nodes:
                if isinstance(n, QgsLayerTreeGroup):
                    if n.isExpanded() == True:
                        n.setExpanded(False)

            sammenligningslag = []
            for tree_layer in group.findLayers():
                sammenligningslag.append(tree_layer.layer().name())

            self.StatistikTabel.setRowCount(len(sammenligningslag))
            self.StatistikTabel.setVerticalHeaderLabels(sammenligningslag)

            for række, layer in enumerate(sammenligningslag):

                rlayer = project.mapLayersByName('{}'.format(layer))[0]
                stats = rlayer.dataProvider().bandStatistics(1, QgsRasterBandStats.All, områdelayer.extent())

                print(stats.minimumValue)
                startcolumn = 0

                for statistik in [stats.minimumValue, stats.maximumValue, stats.mean, stats.stdDev]:
                    print(statistik)
                    chkBoxItem = QTableWidgetItem(f"{round(statistik, 2)}")
                    chkBoxItem.setFlags(Qt.ItemIsEnabled)
                    chkBoxItem.setTextAlignment(Qt.AlignCenter)
                    self.StatistikTabel.setItem(række, startcolumn, chkBoxItem)
                    startcolumn += 1

            # path = 'C:\Temp\OSDRevision'
            # now = datetime.datetime.now()
            # for f in os.listdir(path):
            #    if os.stat(os.path.join(path, f)).st_mtime < now - 1 * 86400:
            #          os.remove(os.path.join(path, f))

            self.OSDDone.setText("Lav OSD-klassifikation")
            self.OSDDone.setEnabled(True)
            self.repaint()
        #
    else:
        QMessageBox.warning(None, 'Fejl', "Udfyld tabellen.")
