# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui import *

matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolbar2"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, \
    QThreadPool, QPoint
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, \
    QApplication
from qgis.core import (
    QgsFields,
    QgsField,
    QgsFeature,
    QgsLayerTreeGroup,
    QgsGeometry,
    QgsFeatureRequest,
    QgsProject,
    QgsSimpleMarkerSymbolLayerBase,
    QgsPoint,
    QgsRaster,
    QgsTask,
    QgsPrintLayout,
    QgsReadWriteContext,
    QgsRasterFileWriter,
    QgsMapLayer,
    QgsLayoutItemLegend,
    QgsLayoutItemMap,
    QgsLegendRenderer,
    QgsExpression,
    QgsLegendStyle,
    QgsLayoutExporter,
    QgsLayoutItemPicture,
    QgsLayoutPoint,
    QgsLayoutSize,
    QgsUnitTypes,
    QgsRasterBandStats,
    QgsCategorizedSymbolRenderer,
    QgsRendererCategory,
    QgsSymbol,
    QgsCoordinateTransformContext,
    QgsRuleBasedRenderer,
    QgsRandomColorRamp,
    QgsMapThemeCollection,
    QgsLayerDefinition,
    QgsPointXY,
    QgsRendererRange,
    QgsApplication,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsExpressionContextScope,
    QgsCoordinateReferenceSystem,
    QgsProcessingFeatureSourceDefinition,
    QgsRectangle,
    QgsTaskManager,
    QgsGraduatedSymbolRenderer,
    QgsLayout,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsRasterLayer,
    QgsWkbTypes,
    QgsDataSourceUri
)
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions, QgsMapTool, QgsMapTip, QgsVertexMarker
)
#from matplotlib.backends.backend_qtagg import NavigationToolbar2QT
import webbrowser

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep

today = date.today()
import pyodbc

from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass
__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

import re
import subprocess
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.lines import Line2D
import psycopg2 as pg
import pandas as pd
import configparser
from matplotlib import pyplot as plt, patches, rcParams
from scipy.interpolate import PchipInterpolator
import numpy as np
from matplotlib.widgets import Slider, Button, TextBox
from .backend_functions import *


class Database:
    """Class handles all database related function"""

    def __init__(self, database_name, usr):
        """
        Initiates the classe Database
        :param database_name: string used to specify which database name on MST-Grundvand server to connect to
        :param usr: string used to specify the user, e.g. reader og writer
        """
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.ini_database_name = database_name.upper()

    @property
    def parse_db_credentials(self):
        """
        fetches db credentials from ini file
        :return: dict containing db credentials
        """
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.ini_database_name}']['userid']
        pw_read = config[f'{self.ini_database_name}']['password']
        host = config[f'{self.ini_database_name}']['host']
        port = config[f'{self.ini_database_name}']['port']
        dbname = config[f'{self.ini_database_name}']['databasename']

        credentials = dict()
        credentials['usr'] = usr_read
        credentials['pw'] = pw_read
        credentials['host'] = host
        credentials['port'] = port
        credentials['dbname'] = dbname

        return credentials

    def connect_to_pgsql_db(self):
        """
        Connects to postgresql server database
        :return: postgresql psycopg2 connection and string database name
        """
        cred = self.parse_db_credentials
        pg_database_name = cred['dbname']
        pg_con = None
        try:
            pg_con = pg.connect(
                host=cred['host'],
                port=cred['port'],
                database=cred['dbname'],
                user=cred['usr'],
                password=cred['pw']
            )
        except Exception as e:
            print(e)
        else:
            del cred
            # print(f'Connected to database: {pg_database_name}')

        return pg_con, pg_database_name

    def import_pgsql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query string for the import
        :return: pandas dataframe containing output from sql query
        """
        conn, database = self.connect_to_pgsql_db()
        try:
            # print(f'Fetching table from {database} to dataframe...')
            df_pgsql = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n {e}')
            raise ValueError(f'Unable to fetch dataframe from database: {database}')
        else:
            # print(f'Table loaded into dataframe from database: {database}')
            pass

        return df_pgsql

    def execute_pg_sql(self, sql):
        """
        connect to postgresql database and execute sql
        :param sql: string with sql query
        :return: None
        """
        cred = self.parse_db_credentials
        pgdatabase = cred['dbname']
        try:
            with pg.connect(
                    host=cred['host'],
                    port=cred['port'],
                    database=cred['dbname'],
                    user=cred['usr'],
                    password=cred['pw']
            ) as con:
                with con.cursor() as cur:
                    cur.execute(sql)
                    con.commit()
        except Exception as e:
            print(f'Unable to execute sql: {pgdatabase}:')
            print(e)
        else:
            del cred


class CrossSection():
    """
        A simple run-function to minimize the code within qgis script
        :param line_points: list of tuples, the vertices of the profile line in srid 25832
                eg. [(513425, 6296131), (529416, 6310901)]
        :param number_of_points: int, number of profile nodes along the profile line
        :param display_fig_descr: bool, if true: add figure description to the plot
        :param smooth_line: bool, if true: smoothing using pchip interpolation between the profile points
                if false: matplotlibs standard linear interpolation between the profile points
        :param display_profile_points: bool, if true: add lines displaying the position of the profile points
        :param database_name: string, name of the database containing the fohm layers
        :param schema_name: string, schema name with in the database containing the fohm layers
    """

    def __init__(
            self,
            dialog,
            line_points,
            område,
            number_of_points=100,
            smooth_line=False,
            display_fig_descr=True,
            display_profile_points=True,
            database_name='grukos',  # fohm
            schema_name='mstfohm'  # fohm_layer
    ):
        self.database_name = database_name
        self.db = Database(database_name=self.database_name, usr='reader')
        self.db_jup = Database(database_name='JUPITER', usr='reader')
        self.Dialog = dialog
        self.område = område
        self.schema_name = schema_name
        self.points = line_points
        self.line_wkt = self.points_to_line_wkt()
        self.srid = 25832
        self.n = number_of_points
        self.smooth_lines = smooth_line
        self.display_fig_descr = display_fig_descr
        self.display_profile_points = display_profile_points
        self.df_table_name = self.fetch_table_names()
        self.sql = self.fetch_cross_section_sql()
        self.fig_txt = ''

        self.vertexpoint = QgsVertexMarker(iface.mapCanvas())
        self.vertexpoint.setColor(QColor(Qt.red))
        self.vertexpoint.setIconSize(7)
        self.vertexpoint.setIconType(QgsVertexMarker.ICON_BOX)
        self.vertexpoint.setPenWidth(5)

        self.vertexboring = QgsVertexMarker(iface.mapCanvas())
        self.vertexboring.setColor(QColor(Qt.blue))
        self.vertexboring.setIconSize(5)
        self.vertexboring.setIconType(QgsVertexMarker.ICON_BOX)
        self.vertexboring.setPenWidth(3)

    @staticmethod
    def interpolate_layers(x_data, df, n_interp=1000):
        """
        interpolate all columns of a pandas dataframe (y_data) and x_data using pchip interpolation
        :param x_data: numpy array, data (1st axis) used in the interpolation
        :param df: pandas dataframe, each row is data (2nd axis) used in the interpolation
        :param n_interp: int, number of nodes used in the interpolation
        :return: numpy array (1st axis) and pandas dataframe (2nd axis)
        """
        x_interp = None
        df_interp = pd.DataFrame()

        for (col_name, col_val) in df.items():
            y_f = PchipInterpolator(x_data, col_val)  # , kind='cubic')
            x_interp = np.linspace(x_data.min(), x_data.max(), n_interp)
            y_interp = y_f(x_interp)
            df_interp[col_name] = y_interp

        return x_interp, df_interp

    def points_to_line_wkt(self):
        """
            Converts a list of tuple containing x,y coords to WKT linestring.
            :return: string, WKT linestring
        """
        points_txt = ''
        for i, (x, y) in enumerate(self.points):
            if i == 0:
                points_txt += f'{x} {y}'
            else:
                points_txt += f', {x} {y}'

        line_wkt = f'LINESTRING({points_txt})'

        return line_wkt

    def fetch_anlaeg(self, boringsliste):
        #   boringslisten = ' ,'.join(boringsliste)
        # print(boringslisten)

        for i, boring in enumerate(boringsliste):
            if i == 0:

                sql_anlaeg = f'''
                SELECT DISTINCT ON (plantid)
                p.plantid 
                FROM jupiter.DRWPLANT p
                INNER JOIN jupiter.drwplantintake drw USING (plantid)
                INNER JOIN jupiter.borehole b ON b.boreholeid = drw.boreholeid
                WHERE REPLACE(b.boreholeno, ' ', '') IN ('{boring}')
                    '''
            else:
                sql_anlaeg += f'''\nOR REPLACE(b.boreholeno, ' ', '') IN ('{boring}')'''

        return sql_anlaeg

    def fetch_particles(self, anlaegsliste, boringsafstand):

        for i, boring in enumerate(anlaegsliste):
            if i == 0:
                sql_particles = f'''

                                      SELECT
                                       st_x(geom) AS x,
                                        ST_Distance(ST_StartPoint(ST_GeomFromText('{self.line_wkt}', {self.srid})), ST_ClosestPoint(ST_GeomFromText('{self.line_wkt}', {self.srid}),ipp.geom)) AS dist,
                                       st_y(geom) AS y,
                                       z_kote AS z,
                                       anl_id , anl_navn , part_id, transptid
                                       FROM gvkort.iol_partikelbaner_pkt ipp
                                      WHERE ST_Distance(ST_ClosestPoint(ST_GeomFromText('{self.line_wkt}', {self.srid}),ipp.geom),ipp.geom) < {boringsafstand * 1}
                                      AND (ipp.anl_id in ('{boring}')
                                       '''
            else:
                sql_particles += f'''\nOR ipp.anl_id IN ('{boring}')'''

        sql_particles += ")"

        print(sql_particles)
        return sql_particles

    def fetch_lithology(self, boringskvalitet, boringskvalitet_kilde, mindybde):
        if self.Dialog.checkBox_vvboringer.checkState() == Qt.Checked:
            vv = "AND b.use in ('V', 'VV')"
        else:
            vv = ''

        sql_lith = f'''
                WITH               
                waterlevel as(
    	                        SELECT DISTINCT ON (wl.WATLEVELID)
                                b.boreholeno,
                                b.boreholeid,
                                wl.intakeno,
                                wl.guid_intake,
                                wl.watlevgrsu AS vandst_mut, --Det målte vandspejl beregnet som meter under terrænoverfladen
                                wl.watlevmsl AS vandstkote,  --Det målte vandspejl beregnet som meter over havniveau (online DVR90 ved download det valgt kotesystem)
                                wl.timeofmeas::DATE AS pejledato,
                                wl.situation,
                                b.XUTM32EUREF89 as X,
                                b.YUTM32EUREF89 as Y,
                                ST_Distance(ST_StartPoint(ST_GeomFromText('{self.line_wkt}', {self.srid})), ST_ClosestPoint(ST_GeomFromText('{self.line_wkt}', {self.srid}),b.geom)) AS dist,
                                ST_Distance(ST_ClosestPoint(ST_GeomFromText('{self.line_wkt}', {self.srid}),b.geom),b.geom) as afstand,

                                b.geom
                                FROM jupiter.borehole b
                                INNER JOIN jupiter.watlevel wl USING (boreholeid)
                                WHERE b.geom && ST_Buffer(ST_GeomFromText('{self.line_wkt}', {self.srid}) , 1000, 'endcap=flat join=round')

                                ORDER BY wl.WATLEVELID, pejledato DESC),
                vand as (
                SELECT DISTINCT ON (guid_intake)
    	  	        row_number() OVER () AS row_id,
    	                *
    	            FROM waterlevel
    	             ORDER BY guid_intake, boreholeno, pejledato DESC NULLS LAST
                ),              

                    tmp AS 
                        (
                            SELECT 
                                replace(b.boreholeno, ' ', '') as boreholeno,
                                b.boreholeid, sc.top as topfilter, sc.bottom as bottomfilter,
                                ls.top, 
                                b.elevation,
                                wl.vandst_mut as waterlevel,
                                wl.dist,
                                wl.afstand,
                                b.drilendate,
                                wl.X,
                                wl.Y,
                                COALESCE(
                                    ls.bottom, 
                                    lag(ls.top, -1) OVER (PARTITION BY boreholeid ORDER BY ls.top), 
                                    b.drilldepth, 
                                    ls.top+10
                                    ) AS bottom, 
                                ls.rocksymbol
                            FROM jupiter.lithsamp ls
                            INNER JOIN jupiter.borehole b USING (boreholeid)
                            Left JOIN vand wl using (boreholeid)
                            left join jupiter.screen sc USING (boreholeid)
                            {boringskvalitet_kilde}
                            WHERE b.geom && ST_Buffer(ST_GeomFromText('{self.line_wkt}', {self.srid}) , 1000, 'endcap=flat join=round')
                            {boringskvalitet}
                            {mindybde}
                            {vv}
                        ),
                    tmp2 AS 
                        (
                            SELECT DISTINCT 
                                boreholeno
                            FROM tmp
                            ORDER BY boreholeno
                        ),
                    tmp3 AS 
                        (
                            SELECT 
                                ROW_NUMBER() OVER () AS fig_number,
                                boreholeno
                            FROM tmp2
                        )
                SELECT 
                    tmp3.fig_number,
                    boreholeno, 
                    boreholeid, 
                    top, 
                    bottom,
                    dist,
                    afstand,
                    elevation,
                    drilendate,
                    bottomfilter,
                    topfilter,
                    waterlevel,
                    X,
                    Y,
                    rocksymbol,
                    CASE 
                        WHEN RIGHT(rocksymbol, 1) = 's'
                            THEN '#ff5400'
                        WHEN RIGHT(rocksymbol, 1) = 'g'
                            THEN '#ff5400'
                        WHEN RIGHT(rocksymbol, 1) = 'q'
                            THEN '#ff00ff'
                        WHEN RIGHT(rocksymbol, 1) = 'a'
                            THEN '#ff8000'
                        WHEN RIGHT(rocksymbol, 1) = 'r'    
                            THEN '#ccb3ff'
                        WHEN rocksymbol = 'ml'
                            THEN '#dbb800'
                        WHEN rocksymbol = 'dl'
                            THEN '#ffba33'
                        WHEN rocksymbol IN ('ol', 'pl', 'll')
                            THEN '#000cff'
                        WHEN rocksymbol = 'gl'
                            THEN '#b3ffff'
                        WHEN rocksymbol = 'ed'
                            THEN '#52ffff'
                        WHEN rocksymbol NOT IN ('dl', 'ml', 'ol', 'pl', 'll')
                            AND RIGHT(rocksymbol, 1) = 'l'
                            THEN '#9d711f' 
                        WHEN RIGHT(rocksymbol, 1) = 'j'
                            THEN '#9d711f'
                        WHEN RIGHT(rocksymbol, 1) = 'i'
                            THEN '#faf59e'
                        WHEN RIGHT(rocksymbol, 1) = 'k'
                            THEN '#80ff99'
                        WHEN RIGHT(rocksymbol, 1) = 't'
                            THEN '#deff00'   
                        WHEN RIGHT(rocksymbol, 1) = 'p'
                            THEN '#c16544'  
                        ELSE '#efefef'
                        END AS html_color                    
                FROM tmp
                INNER JOIN tmp3 USING (boreholeno)
                where afstand <= 1000
                ORDER BY boreholeno, top
            '''

        return sql_lith

    def fetch_table_names(self):
        """
            Returns the table_names within the fohm database/schema
            :return: string, sql
        """
        sql_table_name = f'''
            SELECT table_name AS table_name
            FROM information_schema.tables
            WHERE table_schema='{self.schema_name}'
                AND table_type='BASE TABLE'
                AND table_name LIKE '{self.område}%'
                AND table_name NOT LIKE '%slice%'
            ORDER BY table_name;
        '''

        df_table_name = self.db.import_pgsql_to_df(sql_table_name)
        return df_table_name

    def fetch_cross_section_sql(self):
        """
        return sql for querying the fohm raster layers as point along the line
        :return: string, sql
        """
        n_inv = 1 / self.n

        sql_join = ''
        sql_ele = ''
        for i, (index, row) in enumerate(self.df_table_name.iterrows()):
            table_name = row['table_name']
            layer_name = re.search("(\d+.[A-Za-z0-9ÆØÅæøå]+.[A-Za-z0-9ÆØÅæøå]+)", table_name)[0]
            # sql_join += f"\nINNER JOIN (SELECT rid, rast FROM fohm.fohm_grids WHERE filename = '{table_name}') r{i} ON st_intersects(r{i}.rast, b.geom)" #f'\n INNER JOIN mstfohm.'{table_name}" r{i} ON st_intersects(r{i}.rast, b.geom)'   #fohm_layer
            sql_join += f'\n INNER JOIN mstfohm."{table_name}" r{i} ON st_intersects(r{i}.rast, b.geom)'  # fohm_layer
            sql_ele += f'\n(ST_ValueCount(ST_Clip(r{i}.rast,b.geom))).value AS "{layer_name}",'

        sql = f'''
            WITH
                lines AS 
                    (
                        SELECT
                            ST_GeomFromText('{self.line_wkt}', {self.srid}) AS temp_line
                    ),
                points_from_line AS 
                    (
                        SELECT 
                            (ST_DumpPoints(ST_LineInterpolatePoints(temp_line, {n_inv}))).geom AS geom
                        FROM lines 
                    ),
                profile_dist AS 
                    (
                        SELECT 
                            row_number() OVER () AS row_num,
                            geom,
                            round((COALESCE(ST_Distance(geom, lag(geom,1) OVER ()), 0))::NUMERIC, 0) AS dist
                        FROM points_from_line
                    ),
                profile_dist_cumsum AS 
                    (
                        SELECT 
                            geom,
                            sum(dist) OVER (ORDER BY row_num) AS distance
                        FROM profile_dist 
                    )         
            SELECT
                {sql_ele}
                distance
            FROM profile_dist_cumsum b                
            {sql_join}
            ;
        '''
        return sql

    def plot_layers(self):
        """
            Plot the fohm layers using matplotlib's fill_between function
            The layers are only plotted if the layer if different than the layer above (previous)
            :return: None
        """
        try:
            QgsProject.instance().removeMapLayer(QgsProject.instance().mapLayersByName("Profillinje - GKO")[0])
        except:
            pass
        try:
            QgsProject.instance().removeMapLayer(QgsProject.instance().mapLayersByName("Profillinje buffer - GKO")[0])
        except:
            pass

        temp = QgsVectorLayer("LineString?crs=epsg:25832", "Profillinje - GKO", "memory")

        temp.startEditing()
        geom = QgsGeometry()
        geom = QgsGeometry.fromWkt(self.line_wkt)
        feat = QgsFeature()
        feat.setGeometry(geom)
        temp.dataProvider().addFeatures([feat])
        temp.commitChanges()

        buffer = processing.run("native:buffer",
                                {'INPUT': temp,
                                 'DISTANCE': self.Dialog.spinBox_5.value(), 'SEGMENTS': 5, 'END_CAP_STYLE': 1,
                                 'JOIN_STYLE': 0, 'MITER_LIMIT': 2,
                                 'DISSOLVE': False, 'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]
        QgsProject.instance().addMapLayer(buffer)
        buffer.setName("Profillinje buffer - GKO")

        QgsProject.instance().addMapLayer(temp)

        buffer.loadNamedStyle(str(os.path.join(os.path.dirname(__file__), '..')) + "/Styles/FOHM_PROFILE_BUFFER.qml")
        temp.loadNamedStyle(str(os.path.join(os.path.dirname(__file__), '..')) + "/Styles/FOHM_PROFILE_LINE.qml")

        sql = self.fetch_cross_section_sql()
        df = self.db.import_pgsql_to_df(sql)

        # put profile distance in separate var and del from dataframe
        x_dist = np.sort(df['distance'].to_numpy())
        x_dist_orig = x_dist
        df.pop('distance')
        rcParams['figure.dpi'] = 150
        # set vars for deactivating layers on plot legend
        fill_plots = []

        # create figure and plot
        fig, ax = plt.subplots(figsize=(11, 7))
        fig.canvas.set_window_title('Crosssection - FOHM')

        # define colors of fohm layers
        if self.område == 'jylland':
            colors = {
                '0000_terraen': 'black',
                '0050_antropocaen': 'lavender',
                '0100_postglacial_toerv': 'peru',
                '0120_postglacial_sand': 'red',
                '0130_postglacial_littorinaler': '#5d1902',
                '0140_senglacial_yoldiasand': 'purple',
                '0150_senglacial_yoldialer': '#3f1a18',
                '0200_kvartaer_sand': '#ffa3f9',
                '0300_kvartaer_ler': '#f5ac51',
                '0400_kvartaer_sand': '#ff00ff',
                '1100_kvartaer_ler': '#d99036',
                '1200_kvartaer_sand': '#ffff66',
                '1300_kvartaer_ler': '#b36d17',
                '1400_kvartaer_sand': '#ff99cc',
                '1500_kvartaer_ler': '#653308',
                '2100_kvartaer_sand': 'red',
                '2200_kvartaer_ler': '#5d1902',
                '2300_kvartaer_sand': '#ff6b6b',
                '2400_kvartaer_ler': '#3f1a18',
                '5100_maadegruppen_gram': '#63ffef',
                '5200_øvre_odderup': 'deepskyblue',
                '5300_øvre_arnum': '#63ffef',
                '5400_nedre_odderup': 'deepskyblue',
                '5500_nedre_arnum': '#63ffef',
                '5600_bastrup_sand': 'deepskyblue',
                '5700_klintinghoved_ler': '#63ffef',
                '5800_bastrup_sand': 'deepskyblue',
                '5900_klintinghoved_ler': '#63ffef',
                '6000_bastrup_sand': 'deepskyblue',
                '6100_klintinghoved_ler': '#63ffef',
                '6200_bastrup_sand': 'deepskyblue',
                '6300_klintinghoved_ler': '#63ffef',
                '6400_bastrup_sand': 'deepskyblue',
                '6500_klintinghoved_ler': '#63ffef',
                '6600_bastrup_sand': 'deepskyblue',
                '6700_klintinghoved_ler': '#63ffef',
                '6800_billund_sand': 'deepskyblue',
                '6900_vejle_fjord': '#63ffef',
                '7000_billund_sand': 'deepskyblue',
                '7100_vejle_fjord': '#63ffef',
                '7200_billund_sand': 'deepskyblue',
                '7300_vejle_fjord': '#63ffef',
                '7400_billund_sand': 'deepskyblue',
                '7500_vejle_fjord': '#63ffef',
                '7600_billund_sand': 'deepskyblue',
                '7700_vejle_fjord': '#63ffef',
                '7800_billund_sand': 'deepskyblue',
                '8000_palaeogen_ler': '#1532ed',
                '8500_danien_kalk': 'darkgreen',
                '9000_skrivekridt': 'lawngreen',
                '9500_stensalt': 'black',
                '9499_dummy_layer': 'lightgrey'
            }
        elif self.område == 'fyn':
            colors = {
                '0001_topo': 'black',
                '0002_kl1': '#f5ac51',
                '0003_ks1': '#ffa3f9',
                '0004_kl2': '#d99036',
                '0005_ks2': '#ff00ff',
                '0006_kl3': '#b36d17',
                '0007_ks3': '#ffff66',
                '0008_kl4': '#3f1a18',
                '0009_pl1': '#3732c9',
                '0010_kalk': 'darkgreen',

            }
        else:
            colors = {
                '0001_topo': 'black',
                '0002_kl1': '#f5ac51',
                '0003_ks1': '#ffa3f9',
                '0004_kl2': '#d99036',
                '0005_ks2': '#ff00ff',
                '0006_kl3': '#b36d17',
                '0007_ks3': '#ffff66',
                '0008_kl4': '#3f1a18',
                '0009_ks4': 'red',
                '0010_kl5': '#653308',
                '0011_pl1': '#3732c9',
                '0012_gk1b': 'darkgreen',
                '0013_dk1b': 'lawngreen',
                '0014_sk1': 'black',
            }

        """
        colors = {
            '0000_terraen': 'black',
            '0050_antropocaen': 'lavender',
            '0100_postglacial_toerv': 'peru',
            '0200_kvartaer_sand': '#ffa3f9',
            '0300_kvartaer_ler': '#f5ac51',
            '0400_kvartaer_sand': '#ff00ff',
            '1100_kvartaer_ler': '#d99036',
            '1200_kvartaer_sand': '#ffff66',
            '1300_kvartaer_ler': '#b36d17',
            '1400_kvartaer_sand': '#ff99cc',
            '1500_kvartaer_ler': '#653308',
            '2100_kvartaer_sand': 'red',
            '2200_kvartaer_ler': '#5d1902',
            '2300_kvartaer_sand': '#ff6b6b',
            '2400_kvartaer_ler': '#3f1a18',
            '5100_maadegruppen_gram_og_hodde': '#63ffef',
            '5200_øvre_odderup_sand': 'deepskyblue',
            '5300_øvre_arnum_ler': '#63ffef',
            '5400_nedre_odderup_sand': 'deepskyblue',
            '5500_nedre_arnum_ler': '#63ffef',
            '5600_bastrup_sand': 'deepskyblue',
            '5700_klintinghoved_ler': '#63ffef',
            '5800_bastrup_sand': 'deepskyblue',
            '5900_klintinghoved_ler': '#63ffef',
            '6000_bastrup_sand': 'deepskyblue',
            '6100_klintinghoved_ler': '#63ffef',
            '6200_bastrup_sand': 'deepskyblue',
            '6300_klintinghoved_ler': '#63ffef',
            '6400_bastrup_sand': 'deepskyblue',
            '6500_klintinghoved_ler': '#63ffef',
            '6600_bastrup_sand': 'deepskyblue',
            '6700_klintinghoved_ler': '#63ffef',
            '6800_billund_sand': 'deepskyblue',
            '6900_vejle_fjord_ler': '#63ffef',
            '7000_billund_sand': 'deepskyblue',
            '7100_vejle_fjord_ler': '#63ffef',
            '7200_billund_sand': 'deepskyblue',
            '7300_vejle_fjord_ler': '#63ffef',
            '7400_billund_sand': 'deepskyblue',
            '7500_vejle_fjord_ler': '#63ffef',
            '7600_billund_sand': 'deepskyblue',
            '7700_vejle_fjord_ler': '#63ffef',
            '7800_billund_sand': 'deepskyblue',
            '8000_palaeogen_ler': '#1532ed',
            '8500_danien_kalk': 'darkgreen',
            '9000_skrivekridt': 'lawngreen',
            '9500_stensalt': 'black',
            '9499_dummy_layer': 'lightgrey'
        }
        """
        # smooth lines if selected
        if self.smooth_lines:
            x_dist, df = self.interpolate_layers(x_dist, df)

        # loop through all layers
        column_names = df.columns
        for i, column_name in enumerate(column_names):
            bot = df[column_names[i]]  # layer bot as bot
            top = df[column_names[i - 1]]
            layer_id = re.search('\d+', column_name)[0]  # layer number e.g. 0000_terrain => 0000
            # plot layer if not a zero-thickness layer
            if not bot.equals(top):
                alpha = 0.85
                if layer_id == '9500' or layer_id == '0014':  # layer 9500 is a top-layer not bottom-layer
                    ax.plot(np.array(x_dist), np.array(bot), zorder=3, color='deeppink', label=column_name, linewidth=1)
                    if self.område == 'jylland':
                        column_name = '9499_dummy_layer'

                    alpha = 0.4

                if layer_id != '0000' and layer_id != '0001':  # discard terrain plot
                    pl = ax.fill_between(
                        x_dist, top, bot,
                        label=column_name, zorder=2, color=colors[column_name],  # colors[column_name]
                        alpha=alpha, linewidth=0.2, edgecolor='black'
                    )
                    fill_plots.append(pl)

        # plot legend
        # dislay points where fohm layers are extracted
        bot_min = df.min().min()
        top_max = df.max().max()
        if self.display_profile_points:
            plts = []
            for k, x in enumerate(x_dist_orig):
                tmp_y = [bot_min - 10, top_max + 10]
                tmp_x = [x, x]

                pl, = ax.plot(
                    tmp_x, tmp_y,
                    label='Profile points', color='gray',
                    linestyle='--', zorder=1, alpha=0.5, linewidth=1
                )
                pl.set_dashes([2, 2, 2, 2])
                plts.append(pl)
            plt.setp(plts[1:], label="_")

        # sort legend labels and handles by labels
        handles, labels = ax.get_legend_handles_labels()
        labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))

        annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        # set legend on plot and place it outside plot
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        leg = ax.legend(handles, labels, fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5),
                        fontsize=6.5)

        leg_to_fill = {}  # map legend objects to fill plots.
        for leg_obj, fill_plot in zip(leg.findobj(patches.Rectangle), fill_plots):
            leg_obj.set_picker(True)  # Enable picking on the legend object.
            leg_to_fill[leg_obj] = fill_plot

        def on_pick(event):
            leg_obj_orig = event.artist
            fill_plot_orig = leg_to_fill[leg_obj_orig]
            visible = not fill_plot_orig.get_visible()
            fill_plot_orig.set_visible(visible)
            leg_obj_orig.set_alpha(1.0 if visible else 0.2)
            leg_obj_orig.set_color(fill_plot_orig.get_facecolor())  # needed if run directly
            fig.canvas.draw()

        # set grid on plot
        ax.grid('both')
        if self.område == 'jylland':
            plt.ylim(ymax=df["0000_terraen"].max() + 20, ymin=df["2400_kvartaer_ler"].min() - 30)
        elif self.område == 'fyn':
            plt.ylim(ymax=df["0001_topo"].max() + 20, ymin=df["0008_kl4"].min() - 30)
        else:
            pass
        # plt.ylim(ymax=df["0001_topo"].max() + 20, ymin=df["0010_kl5"].min() - 30)

        # add title and labels to figure
        plt.title(f'FOHM Profil', fontsize=12, fontweight='bold')
        plt.xlabel('Længde [m]', fontsize=9)
        plt.ylabel('Kote [m]', fontsize=9)

        # add figure description if activated
        if self.smooth_lines:
            smooth_text = 'pchip'
        else:
            smooth_text = 'None'

        """if self.display_fig_descr:
            self.fig_txt += f'PROFILE-LINE: {self.line_wkt}'
            self.fig_txt += f'\nSRID: {self.srid}'
            self.fig_txt += f'\nNUMBER OF POINTS ALONG PROFILE: {self.n}'
            self.fig_txt += f'\nDISTANCE BETWEEN PROFILE POINTS: {round(x_dist_orig[1], 2)} m'
            self.fig_txt += f'\nLAYER SMOOTH: {smooth_text}'
            # print(f'\n {self.fig_txt}')
            plt.figtext(0.01, 0.91, self.fig_txt, fontsize=5, weight='bold')"""

        fig.canvas.mpl_connect('pick_event', on_pick)

        if self.Dialog.checkBox_15.checkState() == Qt.Checked:
            if self.Dialog.checkBox_18.checkState() == Qt.Checked:
                if self.Dialog.checkBox_minimumkvali.checkState() == Qt.Checked:
                    boringskvalitet = "AND bhq.quality_litho_position >= {}".format(
                        self.Dialog.spinBox_minimumkvali.value())
                    boringskvalitet_kilde = "LEFT JOIN mstjupiter.borehole_quality bhq USING (boreholeid)"
                else:
                    boringskvalitet = ""
                    boringskvalitet_kilde = ""
                if self.Dialog.checkBox_fohm_minimumdybde.checkState() == Qt.Checked:
                    mindybde = "AND b.drilldepth >= {}".format(self.Dialog.spinBox_fohm_minimumdybde.value())
                else:
                    mindybde = ""
            else:
                boringskvalitet = ""
                boringskvalitet_kilde = ""
                mindybde = ""

            sql_lith = self.fetch_lithology(boringskvalitet=boringskvalitet,
                                            boringskvalitet_kilde=boringskvalitet_kilde, mindybde=mindybde)
            df_lith = self.db_jup.import_pgsql_to_df(sql_lith)

            df_lith_grp = df_lith.groupby(['boreholeno'])
            boringsafstand = self.Dialog.spinBox_5.value()
            global boringsliste
            boringsliste = []

            maxdepth = []
            for key, grp in df_lith_grp:
                unikke = grp["rocksymbol"].unique().tolist()
                if unikke == ["b", "x"] or unikke == ["x", "b"] or unikke == ["b"] or unikke == ["x"]:
                    pass
                else:
                    for i, (row_index, row) in enumerate(grp.iterrows()):
                        if row["afstand"] < boringsafstand:
                            try:
                                fig_number = row["fig_number"]

                                z = ax.fill_between \
                                    (x=[row["dist"] - (max(x_dist_orig) / 280), row["dist"] + (max(x_dist_orig) / 280)],
                                     y1=row['elevation'] - row['bottom'], y2=row['elevation'] - row['top'],
                                     edgecolor='black',
                                     color=row['html_color'], zorder=100, label="Boring - {}".format(row["boreholeno"]),
                                     picker=0.01)
                                navn = row["boreholeno"]
                                boringsliste.append(navn.replace(" ", ""))
                                maxdepth.append(row['elevation'] - row['bottom'])
                                start = row["elevation"]
                                afstand2 = row["afstand"]

                                if row["topfilter"] is None:
                                    pass
                                else:
                                    plt.vlines(x=row["dist"] + (max(x_dist_orig) / 280),
                                               ymin=row['elevation'] - row["topfilter"],
                                               ymax=row['elevation'] - row["bottomfilter"],
                                               color='black', linewidth=3, label='Filter')

                                if self.Dialog.checkBox_18.checkState() == Qt.Unchecked:
                                    if row["waterlevel"] is None:
                                        pass
                                    else:
                                        plt.plot(row["dist"] + (max(x_dist_orig) / 180),
                                                 row['elevation'] - row["waterlevel"], color='blue', marker='<',
                                                 markersize=2.75)

                                if self.Dialog.checkBox_18.checkState() == Qt.Checked and self.Dialog.checkBox_inklpejlinger.checkState() == Qt.Checked:
                                    if row["waterlevel"] is None:
                                        pass
                                    else:
                                        plt.plot(row["dist"] + (max(x_dist_orig) / 180),
                                                 row['elevation'] - row["waterlevel"], color='blue', marker='<',
                                                 markersize=2.75)

                                minimum = grp['elevation'].iloc[0] - grp['bottom'].iloc[-1]
                                maximum = grp['elevation'].iloc[0] - grp["top"].iloc[0]
                                x0 = grp["dist"].iat[0] - (max(x_dist_orig) / 280)
                                x1 = grp["dist"].iat[0] + (max(x_dist_orig) / 280)

                                depth = grp['bottom'].max()
                                if self.Dialog.checkBox_18.checkState() == Qt.Unchecked:
                                    if abs(maximum - minimum) > 5:
                                        if row['bottom'] == depth:
                                            ax.text((x0 + x1) / 2, row['elevation'] - row['bottom'] + 1,
                                                    row['rocksymbol'], fontsize=4.9, clip_on=True, ha='center',
                                                    zorder=100)
                                        else:
                                            j = int(i + 1)
                                            if grp.iloc[j]['rocksymbol'] != row['rocksymbol']:
                                                ax.text((x0 + x1) / 2, row['elevation'] - row['bottom'] + 1,
                                                        row['rocksymbol'], fontsize=4.9, clip_on=True, ha='center',
                                                        zorder=100)

                                if self.Dialog.checkBox_18.checkState() == Qt.Checked and self.Dialog.checkBox_inkllithobeskrivelser.checkState() == Qt.Checked:
                                    if abs(maximum - minimum) > 5:
                                        if row['bottom'] == depth:
                                            ax.text((x0 + x1) / 2, row['elevation'] - row['bottom'] + 1,
                                                    row['rocksymbol'], fontsize=4.9, clip_on=True, ha='center',
                                                    zorder=100)
                                        else:
                                            j = int(i + 1)
                                            if grp.iloc[j]['rocksymbol'] != row['rocksymbol']:
                                                ax.text((x0 + x1) / 2, row['elevation'] - row['bottom'] + 1,
                                                        row['rocksymbol'], fontsize=4.9, clip_on=True, ha='center',
                                                        zorder=100)

                                if i == 0:
                                    z2 = ax.text((x0 + x1) / 2, start + 5, "{}".format(navn), fontsize=7, ha='center',
                                                 color='black', rotation=90, weight='light', clip_on=True)
                                    # z2.set_bbox(dict(facecolor='white', alpha=1, edgecolor='black'))

                                    z3 = ax.text((x0 + x1) / 2, minimum - 2, "({})".format(int(afstand2)), ha='center',
                                                 va='center', fontsize=5.4, color='black', weight='light', clip_on=True)
                                    ax = plt.gca()
                                    rect = patches.Rectangle((x0, minimum), x1 - x0, maximum - minimum, linewidth=0.5,
                                                             edgecolor='black', facecolor='none',
                                                             zorder=120, label="Boring")
                                    ax.add_patch(rect)
                                else:
                                    pass
                                """
                                if fig_number == temp:
                                    z2 = ax.text((x0 + x1) / 2, start + 5, "{}".format(navn), fontsize=7, ha='center',
                                                 color='black', rotation=90, weight='light', clip_on=True)
                                    # z2.set_bbox(dict(facecolor='white', alpha=1, edgecolor='black'))

                                    z3 = ax.text((x0 + x1) / 2, minimum - 2, "({})".format(int(afstand2)), ha='center',
                                                 va='center', fontsize=5.4, color='black', weight='light', clip_on=True)
                                    ax = plt.gca()
                                    rect = patches.Rectangle((x0, minimum), x1 - x0, maximum - minimum, linewidth=0.5,
                                                             edgecolor='black', facecolor='none',
                                                             zorder=120, label="Boring")
                                    ax.add_patch(rect)
                                else:
                                    pass
                                temp = fig_number
                                """
                            except:
                                pass


            print(maxdepth)
            print(min(maxdepth))
        def onpick1(event):
            if event.artist.get_label().startswith("Boring"):
                dgu = str(event.artist.get_label()).split(" - ")[1]
                z = df_lith.loc[df_lith["boreholeno"] == dgu]
                zgrp = z.groupby(['boreholeno'])
                if "DGU nr. {}".format(dgu.replace(" ", "")) in plt.get_figlabels():
                    pass
                else:
                    boringspoint = QgsPointXY(z["x"].iloc[0], z["y"].iloc[0])
                    self.vertexboring.setCenter(boringspoint)
                    fig2, ax2 = plt.subplots(figsize=(3.7, 7), num="DGU nr. {}".format(dgu.replace(" ", "")))

                    for key, grp in zgrp:
                        i = 0
                        for row_index, row in grp.iterrows():
                            # print(row['rocksymbol'], row['top'], row['bottom'], row['html_color'])
                            lithologi = {'a': ' Grundfjeld', 'af': ' Turonien \n  konglomerat',
                                         'ak': ' Turonien \n kalk',
                                         'al': ' Turonien ler', 'ar': ' Kambrium \n alunskifer ',
                                         'as': ' Cenomanien sand', 'av': ' Turonien \n vekslende lag',
                                         'b': ' Brønd', 'bk': ' Danien bryozokalk, \n koralkalk',
                                         'bl': ' Coniacien-santonien \n ler', 'bs': ' Coniacien-santonien \n sand ',
                                         'bv': ' Coniacien-santonien \n vekslende lag', 'c': ' Kul, brunkul',
                                         'cj': ' Trias \n lersten', 'ck': ' Trias \n kalksten', 'cl': ' Trias ler',
                                         'cq': ' Trias sandsten', 'cr': ' Trias skifer ',
                                         'cv': ' Trias \n vekslende lag',
                                         'cw': ' Trias \n evaporitter',
                                         'd': ' Diatomeaflejringer \n (ikke postglaciale)',
                                         'dg': ' Glacial \n smeltevandsgrus', 'di': ' Glacial \n smeltevandssilt',
                                         'dk': ' Campanien-maastrich\n -tien kalksten',
                                         'dl': ' Glacial \n smeltevandsler',
                                         'dr': ' Ordovicium \n dicellograptus \n skifer',
                                         'ds': ' Glacial \n smeltevandssand',
                                         'dv': ' Glacial vekslende \n smeltevandslag',
                                         'dz': ' Glacial \n smeltevandssten',
                                         'e': ' Vulkansk aske', 'ed': ' Eocæn moler', 'ee': ' Eocæn vulkansk \n aske',
                                         'ek': ' Kambrium eksulans \n kalk', 'el': ' Nedre kridt/øvre \n jura ler',
                                         'eq': ' Kambrium nexø \n sandsten', 'es': ' Postglacial \n flyvesand',
                                         'ev': ' Eocæn \n vekslende lag', 'f': ' Konglomerat, \n fosforitkonglomerat',
                                         'fg': ' Postglacial \n ferskvandsgrus', 'fi': ' Postglacial \n ferskvandssilt',
                                         'fj': ' Postglacial \n okker', 'fk': ' Postglacial \n kildekalk',
                                         'fl': ' Postglacial \n ferskvandsler', 'fp': ' Postglacial \n ferskvandsgytje',
                                         'fs': ' Postglacial \n ferskvandssand', 'ft': ' Postglacial \n ferskvandstørv',
                                         'fv': ' Postglacial \n vekslende \n ferskvandslag', 'g': ' Grus, sand og grus',
                                         'gc': ' Oligocæn-miocæn\n -pliocæn brunkul ',
                                         'gf': ' Cenomanien \n konglomerater ',
                                         'gi': ' Oligocæn-miocæn\n -pliocæn glimmersilt', 'gk': ' Cenomanien \n kalk ',
                                         'gl': ' Oligocæn-miocæn\n -pliocæn glimmerler',
                                         'gp': ' Oligocæn-miocæn\n -pliocæn brunkul ',
                                         'gr': ' Ordovicium dictyonema \n skifer',
                                         'gs': ' Oligocæn-miocæn\n -pliocæn glimmersand ',
                                         'gv': ' Oligocæn-miocæn\n -pliocæn \n vekslende lag', 'h': ' Forgravet',
                                         'hg': ' Postglacial \n saltvandsgrus', 'hi': ' Postglacial saltvandssilt',
                                         'hl': ' Postglacial \n saltvandsler',
                                         'hp': ' Postglacial \n saltvandsgytje \n (inkl. diatomegytje)',
                                         'hs': ' Postglacial \n saltvandssand', 'ht': ' Postglacial \n saltvandstørv',
                                         'hv': ' Postglacial \n vekslende \n saltvandslag', 'i': ' Silt klæg',
                                         'id': ' Interglacial \n ferskvandsdiatomegytje',
                                         'ig': ' Interglacial \n ferskvandsgrus',
                                         'ii': ' Interglacial \n ferskvandssilt', 'ij': ' Interglacial okker',
                                         'il': ' Interglacial \n ferskvandsler', 'ip': ' Interglacial ferskvandsgytje',
                                         'is': ' Interglacial \n ferskvandssand', 'it': ' Interglacial ferskvandstørv',
                                         'iv': ' Interglacial vekslende \n ferskvandslag', 'j': ' Lersten, siltsten',
                                         'jc': ' Lias kul', 'ji': ' Lias silt', 'jj': ' Lias lerjernsten',
                                         'jl': ' Lias ler', 'jq': ' Lias sandsten', 'jr': ' Lias skifer',
                                         'js': ' Lias sand', 'jv': ' Lias vekslende \n små lag',
                                         'k': ' Kalk, kridt kalksten',
                                         'ka': ' Kaolin', 'kg': ' Miocæn kvartsgrus',
                                         'kj': ' Kambrium \n grønne skifre',
                                         'kk': ' Danien \n kalksandskalk', 'kl': ' Kambrium \n kalby ler',
                                         'kq': ' Kambrium \n balka sandsten', 'kr': ' Kambrium skifer',
                                         'ks': ' Miocæn kvartssand', 'l': ' Ler',
                                         'lk': ' Danien slamkalk,\n skrivekridt',
                                         'll': ' Eocæn ler, \n lillebælt ler, \n plastisk ler',
                                         'ls': ' Nedre kridt/øvre \n jura sand', 'm': ' Muld',
                                         'mg': ' Glacial morænegrus',
                                         'mi': ' Glacial morænesilt', 'mk': ' Silur kalksten',
                                         'ml': ' Glacial moræneler',
                                         'ms': ' Glacial morænesand ', 'mv': ' Glacial vekslende små morænelag',
                                         'mz': ' Glacial morænesten', 'nd': ' Perm diabas, basalt',
                                         'nj': ' Perm lersten, siltsten ', 'nk': ' Perm kalksten',
                                         'nl': ' Cenomanien ler',
                                         'nq': ' Perm sandsten', 'nr': ' Perm skifer', 'nw': ' Perm evaporitter',
                                         'o': ' Fyld ', 'oe': ' Ordovicium \n skiferbentonit', 'oi': ' Oligocæn silt',
                                         'ok': ' Ordovicium \n kalksten', 'ol': ' Oligocæn ler',
                                         'oq': ' Oligocæn sandsten',
                                         'or': ' Ordovicium skifer', 'os': ' Oligocæn sand', 'p': ' Gytje, dynd, slam',
                                         'pa': ' Prækambrium gnejs, \n granit, pegmatit',
                                         'pd': ' Prækambrium diabas, \n basalt',
                                         'pi': ' Selandien silt, \n palæocæn silt ',
                                         'pj': ' Selandien/palæocæn \n lersten, siltsten',
                                         'pk': ' Selandien kalk, \n palæocæn grønsandskalk',
                                         'pl': ' Selandien ler, \n palæocæn ler, \n kerteminde mergel',
                                         'pq': ' Selandien sandsten, \n palæocæn grønsandsten',
                                         'pr': ' Selandien skifer, \n palæocæn skifer',
                                         'ps': ' Selandien sand, \n palæocæn grønsand',
                                         'pv': ' Selandien \n vekslende lag',
                                         'q': ' Sandsten', 'qg': ' Interglacial, interstadial \n saltvandsgrus',
                                         'qi': ' Interglacial, interstadial \n saltvandssilt',
                                         'qk': ' Ordovicium \n orthoceratitkalk',
                                         'ql': ' Interglacial, interstadial \n saltvandsler',
                                         'qp': ' Interglacial, interstadial \n saltvandsgytje',
                                         'qs': ' Interglacial, interstadial \n saltvandssand',
                                         'qt': ' Interglacial, interstadial \n smeltevandstørv',
                                         'qv': ' Interglacial, interstadial \n vekslende saltvandslag ', 'r': ' Skifer',
                                         'rc': ' Nedre kridt/øvre \n jura kul', 'rg': ' Nedre kridt/øvre \n jura grus',
                                         'rj': ' Nedre kridt/øvre \n jura lerjernsten', 'rk': ' Kambrium andrarum kalk',
                                         'rl': ' Eocæn røsnæs ler', 'rq': ' Kambrium sandsten',
                                         'rr': ' Nedre kridt/øvre \n jura skifer',
                                         'rs': ' Nedre kridt/øvre \n jura sand',
                                         'rv': ' Nedre kridt/øvre \n jura vekslende lag', 's': ' Sand',
                                         'sj': ' Silur lersten, \n siltsten', 'sk': ' Skrivekridt',
                                         'sl': ' Eocæn søvind mergel', 'sr': ' Silur skifer', 't': ' Tørv',
                                         'tg': ' Senglacial \n ferskvandsgrus', 'ti': ' Senglacial \n ferskvandssilt',
                                         'tk': ' Coniacien-santonien \n kalksten', 'tl': ' Senglacial \n ferskvandsler',
                                         'tp': ' Senglacial \n ferskvandsgytje',
                                         'tr': ' Ordovicium \n tretaspis skifer',
                                         'ts': ' Senglacial \n ferskvandssand', 'tt': ' Senglacial \n ferskvandstørv',
                                         'tv': ' Senglacial \n vekslende ferskvandslag', 'u': ' Ler, sand og grus',
                                         'uc': ' Dogger-malm kul ', 'ui': ' Dogger-malm silt ',
                                         'uj': ' Dogger-malm \n lerjernsten', 'ul': ' Dogger-malm ler',
                                         'uq': ' Dogger-malm sandsten', 'ur': ' Dogger-malm skifer',
                                         'us': ' Dogger-malm sand', 'uv': ' Dogger-malm vekslende lag',
                                         'v': ' Vekslende lag', 'vc': ' Nedre kridt kul', 'vi': ' Nedre kridt silt',
                                         'vj': ' Nedre kridt lersten', 'vk': ' Nedre kridt kalksten',
                                         'vl': ' Oligocæn ler',
                                         'vq': ' Nedre kridt sandsten', 'vr': ' Nedre kridt skifer',
                                         'vs': ' Nedre kridt sand', 'vv': ' Nedre kridt vekslende lag',
                                         'w': ' Evaporitter',
                                         'wk': ' Devon kalksten', 'wl': ' Nedre kridt ler', 'wr': ' Devon skifer',
                                         'x': ' Ukendt lag', 'xk': ' carbon kalksten', 'xl': ' Oligocæn ler',
                                         'xq': ' Carbon sandsten', 'xr': ' Carbon skifer',
                                         'yg': ' Senglacial \n saltvandsgrus', 'yi': ' Senglacial \n saltvandssilt',
                                         'yl': ' Senglacial \n saltvandsler', 'yp': ' Senglacial \n saltvandsgytje',
                                         'ys': ' Senglacial \n saltvandssand', 'yt': ' Senglacial \n saltvandstørv',
                                         'yv': ' Senglacial \n vekslende saltvandslag ', 'z': ' Sten, flint',
                                         'zi': ' Nedskylssilt', 'zk': ' Danien kalk, kalk og flint',
                                         'zl': ' Nedskylsler',
                                         'zs': ' Nedskylssand'}

                            ax2.fill_between(x=[0, 1], y1=row['bottom'], y2=row['top'], color=row['html_color'],
                                             label="{} ({})".format(lithologi.get(str(row['rocksymbol'])),
                                                                    row['rocksymbol']))

                            ax2.get_xaxis().set_visible(False)
                            ax2.set_title(f'DGU:{key}')
                            if row["topfilter"] is None:
                                pass
                            else:
                                ax2.vlines(x=0.99991, ymin=row["topfilter"], ymax=row["bottomfilter"],
                                           color='black', linewidth=4)

                            if row["waterlevel"] is None:
                                pass
                            else:
                                ax2.plot(0.99991, row["waterlevel"], color='blue', marker='<', markersize=10)

                            # waterlevel = row["waterlevel"]

                            # plt.plot(1.01, waterlevel, color='blue', marker='<', markersize=8)

                            ax2.set_ylabel('Dybde [m]')

                            depth = grp['bottom'].max()
                            if row['bottom'] == depth:
                                ax2.text(0.5, row['bottom'], row['rocksymbol'], fontsize=9)
                            else:
                                j = int(i + 1)
                                if grp.iloc[j]['rocksymbol'] != row['rocksymbol']:
                                    ax2.text(0.5, row['bottom'], row['rocksymbol'], fontsize=9)
                            i += 1

                            # box = ax2.get_position()
                            # ax2.set_position([box.x0, box.y0, box.width * 0.9, box.height])
                            handles, labels = plt.gca().get_legend_handles_labels()

                            if row["topfilter"] is None:
                                pass
                            else:
                                handles.append(Line2D([], [], color='black'))
                                labels.append("Filter")

                            if row["waterlevel"] is None:
                                pass
                            else:
                                handles.append(Line2D([], [], color='blue', marker='<', linestyle='None'))
                                labels.append("Pejling af \n grundvandsstanden")

                            by_label = dict(zip(labels, handles))
                            plt.legend(by_label.values(), by_label.keys(), fancybox=True, loc='center left',
                                       bbox_to_anchor=(1, 0.8), fontsize=6.5)
                            plt.tight_layout(w_pad=1, pad=1)

                    ax2.invert_yaxis()

                    # xposition, yposition, width, height
                    ax_button = plt.axes([0.8, 0.1, 0.18, 0.05])
                    # properties of the button
                    grid_button = Button(ax_button, 'Jupiter', color='white', hovercolor='grey')

                    # enabling/disabling the grid
                    def grid(val):
                        url = 'https://data.geus.dk/JupiterWWW/borerapport.jsp?dgunr={}'.format(key.replace(" ", ""))
                        webbrowser.open(url)  # redraw the figure

                    # triggering event is the clicking
                    grid_button.on_clicked(grid)
                    ax_button._button = grid_button

                    plt.show()

            # url = 'https://data.geus.dk/JupiterWWW/borerapport.jsp?dgunr={}'.format(dgu.replace(" ", ""))
            # webbrowser.open(url)

        if self.Dialog.checkBox_partikelbaner.checkState() == Qt.Checked and boringsliste is not None:
            sql_anlæg = self.fetch_anlaeg(boringsliste)
            df_anlaeg = self.db_jup.import_pgsql_to_df(sql_anlæg)

            anlaegsliste = df_anlaeg["plantid"].tolist()

            sql_particels = self.fetch_particles(anlaegsliste, boringsafstand)
            df_particels = self.db.import_pgsql_to_df(sql_particels)

            conditions = [
                (df_particels['transptid'] <= 10),
                (df_particels['transptid'] > 10) & (df_particels['transptid'] <= 25),
                (df_particels['transptid'] > 25) & (df_particels['transptid'] <= 50),
                (df_particels['transptid'] > 50) & (df_particels['transptid'] <= 75),
                (df_particels['transptid'] > 75) & (df_particels['transptid'] <= 100),
                (df_particels['transptid'] > 100) & (df_particels['transptid'] <= 150),
                (df_particels['transptid'] > 150) & (df_particels['transptid'] <= 200)
            ]

            # create a list of the values we want to assign for each condition
            values = ['#ff0000', '#ff8040', '#ffff00', '#00ff40', '#00ffff', '#0080ff', '#0000ff']

            def f(row):
                if row['transptid'] <= 10:
                    val = '#ff0000'
                elif (row['transptid'] > 10) & (row['transptid'] <= 25):
                    val = '#ff8040'
                elif (row['transptid'] > 25) & (row['transptid'] <= 50):
                    val = '#ffff00'
                elif (row['transptid'] > 50) & (row['transptid'] <= 75):
                    val = '#00ff40'
                elif (row['transptid'] > 75) & (row['transptid'] <= 100):
                    val = '#00ffff'
                elif (row['transptid'] > 100) & (row['transptid'] <= 150):
                    val = '#0080ff'
                elif (row['transptid'] > 150) & (row['transptid'] <= 200):
                    val = '#0000ff'
                else:
                    val = 0
                return val

                # create a new column and use np.select to assign values to it using our lists as arguments
            try:
                df_particels['color'] = df_particels.apply(f, axis=1)
                df_particels_grp = df_particels.groupby(['part_id'])

                for key, grp in df_particels_grp:
                        x = grp["dist"].tolist()
                        y = grp["z"].tolist()
                        color = grp["color"].tolist()
                        for i, col in enumerate(color):
                          if col != 0:
                            if i == 0:
                                pass
                            else:
                                ax.plot([x[i - 1], x[i]], [y[i - 1], y[i]], '.-', c=col, zorder=120, markersize=4)

                red_patch = matplotlib.patches.Patch(color='#ff0000', label='<10 år')
                red_patch2 = matplotlib.patches.Patch(color='#ff8040', label='10-25 år')
                red_patch3 = matplotlib.patches.Patch(color='#ffff00', label='25-50 år')
                red_patch4 = matplotlib.patches.Patch(color='#00ff40', label='50-75 år')
                red_patch5 = matplotlib.patches.Patch(color='#00ffff', label='75-100 år')
                red_patch6 = matplotlib.patches.Patch(color='#0080ff', label='100-150 år')
                red_patch7 = matplotlib.patches.Patch(color='#0000ff', label='150-200 år')

                legenden = plt.legend(handles=[red_patch, red_patch2, red_patch3, red_patch4, red_patch5, red_patch6, red_patch7],
                               fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.1), ncol=3,
                               fontsize=6.5)
                ax.add_artist(legenden)
            except:
                pass

            del boringsliste
        ax.add_artist(leg)

        fig = plt.gcf()
        fig.canvas.mpl_connect('pick_event', onpick1)

        maxafstand = x_dist_orig[-1]
        minaftsand = x_dist_orig[1]

        sqll = f'''
                               SELECT st_x(st_endpoint(ST_Intersection(ST_GeomFromText('{self.line_wkt}',{self.srid}), ST_BUFFER(ST_StartPoint(ST_GeomFromText('{self.line_wkt}',{self.srid})), {maxafstand})))) as MaxX,
                                      st_y(st_endpoint(ST_Intersection(ST_GeomFromText('{self.line_wkt}',{self.srid}), ST_BUFFER(ST_StartPoint(ST_GeomFromText('{self.line_wkt}',{self.srid})), {maxafstand})))) as MaxY,
                                      st_x(st_endpoint(ST_Intersection(ST_GeomFromText('{self.line_wkt}',{self.srid}), ST_BUFFER(ST_StartPoint(ST_GeomFromText('{self.line_wkt}',{self.srid})), {minaftsand})))) as MinX,
                                      st_y(st_endpoint(ST_Intersection(ST_GeomFromText('{self.line_wkt}',{self.srid}), ST_BUFFER(ST_StartPoint(ST_GeomFromText('{self.line_wkt}',{self.srid})), {minaftsand})))) as MinY

                                '''
        df_point = self.db.import_pgsql_to_df(sqll)

       # x_cord_min = df_point['minx'].iloc[0]
       # x_cord_max = df_point['maxx'].iloc[0]
       ## y_cord_min = df_point['miny'].iloc[0]
       # y_cord_max = df_point['maxy'].iloc[0]
       ## print(x_cord_min, y_cord_min)
      #  print(x_cord_max, y_cord_max)
      #  Equidistance_X = np.linspace(x_cord_min, x_cord_max, self.n + 1)
        #Equidistance_Y = np.linspace(y_cord_min, y_cord_max, self.n + 1)

        def mouse_move(event):
            x, z = event.xdata, event.ydata
            if x is not None:
                #index = np.argmin(np.abs(np.array(x_dist_orig) - x))
                #x_cord = x_dist_orig[index]
                #y_cord = y[index]

                #points = QgsPointXY(x_cord, y_cord)
                points = [QgsPointXY(*p) for p in self.points]
                geom = QgsGeometry.fromPolylineXY(points)
                #try:
                if True:
                    if len(points) > 1:
                        pointprojected = geom.interpolate(x).asPoint()
                    else:
                        pointprojected = points[0]
                #except (IndexError, AttributeError, ValueError):
                  #  print()
                    #pointprojected = None

                self.vertexpoint.setCenter(pointprojected)

        self.vertexpoint.show()
        self.vertexboring.show()
        fig.canvas.mpl_connect('motion_notify_event', mouse_move)

        def close_plot(event):
            for ver in [i for i in iface.mapCanvas().scene().items() if
                        issubclass(type(i), QgsVertexMarker)]:
                if ver in iface.mapCanvas().scene().items():
                    iface.mapCanvas().scene().removeItem(ver)

        fig.canvas.mpl_connect('close_event', close_plot)

        plt.show()

        rcParams['figure.dpi'] = 100

        del k

        self.Dialog.pushButton_FOHM.setText("Tegn FOHM-profil")
        self.Dialog.pushButton_FOHM.setEnabled(True)
        self.Dialog.repaint()
        self.Dialog.iface.actionPan().trigger()
        self.Dialog.setVisible(True)


def plot_data(dialog, line_points, område, number_of_points, display_fig_descr=True, smooth_line=True,
              display_profile_points=True):
    """
    A simple run-function to minimize the code within qgis script
    :param line_points: list of tuples, the vertices of the profile line in srid 25832
            eg. [(513425, 6296131), (529416, 6310901)]
    :param number_of_points: int, number of profile nodes along the profile line
    :param display_fig_descr: bool, if true: add figure description to the plot
    :param smooth_line: bool, if true: smoothing using pchip interpolation between the profile points
            if false: matplotlibs standard piecewise linear interpolation between the profile points
    :param display_profile_points: bool, if true: add lines displaying the position of the profile points
    """
    point1 = line_points[0]
    point2 = line_points[1]
    if point1 != point2:
        CrossSection(
            dialog=dialog,
            line_points=line_points,
            område=område,
            number_of_points=number_of_points,
            display_fig_descr=display_fig_descr,
            smooth_line=smooth_line,
            display_profile_points=display_profile_points
        ).plot_layers()
    else:
        print('ERROR! Profile start-point is the same as end-point')
