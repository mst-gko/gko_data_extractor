# -*- coding: utf-8 -*-

# ***************************************************************************
# GKO - Data Extractor
#
# A QGIS plugin for extracting data from Jupiter, OIS and GRUKOS
#
# Copyright (C) 2022 - Martin Kynde
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


from builtins import str, range

import os, time, sys

import matplotlib.pyplot as plt
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider
import matplotlib
from dateutil.relativedelta import relativedelta
from matplotlib import patches, rcParams, cm
from PyQt5 import uic, QtWidgets
from PyQt5.QtXml import QDomDocument
from PyQt5.QtGui  import *
matplotlib.use('qt5agg')
matplotlib.rcParams["toolbar"] = "toolmanager"

from PyQt5.QtCore import Qt, QFile, QFileInfo, QVariant, QDateTime, QThread, QObject, QCoreApplication, pyqtSignal, QThreadPool
from PyQt5.QtWidgets import QDialog, QFileDialog, QInputDialog, QMessageBox, QTableWidgetItem, QListWidget, QMainWindow, QApplication
from qgis.core import (
    QgsFields,
    QgsField,
    QgsFeature,
QgsLayerTreeGroup,
    QgsGeometry,
    QgsFeatureRequest,
    QgsProject,
QgsSimpleMarkerSymbolLayerBase,
    QgsRaster,
    QgsTask,
    QgsPrintLayout,
    QgsReadWriteContext,
    QgsRasterFileWriter,
    QgsMapLayer,
    QgsLayoutItemLegend,
    QgsLayoutItemMap,
    QgsLegendRenderer,
    QgsExpression,
    QgsLegendStyle,
    QgsLayoutExporter,
    QgsLayoutItemPicture,
    QgsLayoutPoint,
    QgsLayoutSize,
    QgsUnitTypes,
    QgsRasterBandStats,
    QgsCategorizedSymbolRenderer,
    QgsRendererCategory,
    QgsSymbol,
    QgsCoordinateTransformContext,
QgsRuleBasedRenderer,
    QgsRandomColorRamp,
    QgsMapThemeCollection,
    QgsLayerDefinition,
    QgsPointXY,
    QgsApplication,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsExpressionContextScope,
    QgsCoordinateReferenceSystem,
    QgsProcessingFeatureSourceDefinition,
    QgsRectangle,
    QgsTaskManager,
    QgsLayout,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsRasterLayer,
    QgsWkbTypes,
    QgsDataSourceUri
)
from qgis.gui import (
    QgsLayerTreeView,
    QgsLayerTreeViewDefaultActions,
)

import processing
from qgis.utils import iface
import shutil

import sys
import configparser
import re
import time
import datetime
import os

import pandas as pd
import numpy as np
import datetime
from datetime import date
import psycopg2 as pg
import configparser
from time import sleep
today = date.today()

from pathlib import Path


from ..install_packages.check_dependencies import check

try:
    check(['xlwings'])
finally:

    try:
        import xlwings as xw
    except:
        pass


__author__ = 'Martin Kynde <makyn@mst.dk>'
__maintainer__ = 'Martin Kynde <makyn@mst.dk>'
__licence__ = 'GNU GPL v3'

from .backend_functions import *
from ..doDataExtract import *


def plot_automatic_crosssections(line_points, område, number_of_points, id, savepath, display_fig_descr=True, smooth_line=True,
              display_profile_points=True):
    point1 = line_points[0]
    point2 = line_points[1]
    if point1 != point2:
        AutomaticCrossSection(
            line_points=line_points,
            område=område,
            number_of_points=number_of_points,
            id=id,
            savepath=savepath,
            display_fig_descr=display_fig_descr,
            smooth_line=smooth_line,
            display_profile_points=display_profile_points
        ).plot_layers()
    else:
        print('ERROR! Profile start-point is the same as end-point')



def vandværksbeskrivelser(self):
    project = QgsProject.instance()
    self.pushButton_vvbeskrivelser.setText("Opstiller vandværksbeskrivelser")
    self.pushButton_vvbeskrivelser.setEnabled(False)
    self.repaint()

    mapThemesCollection = project.mapThemeCollection()
    mapThemes = mapThemesCollection.mapThemes()

    map_theme_names_to_remove = ["kort1", "kort2", "kort3", "kort4", "kort5", "kort6", "kort7", "kort8", "bnbo"]
    for theme_name in map_theme_names_to_remove:
        if theme_name in mapThemes:
            mapThemes.remove(theme_name)
            print(f"Removed map theme: {theme_name}")
        else:
            print(f"Map theme not found: {theme_name}")

    profilID = self.comboBox_profillinjeID.currentText()
    Boringer_name = self.comboBox_vv_boringer.currentText()
    Nitratsårbarhed_name = self.comboBox_vv_nitrat.currentText()
    Grundvandsdannelse_name = self.comboBox_vv_gvd.currentText()
    NFI_name = self.comboBox_vv_nfi.currentText()
    IO_name = self.comboBox_vv_io.currentText()
    GVDPT_name = self.comboBox_vv_gvdpt.currentText()
    PT_name = self.comboBox_vv_pt.currentText()
    Ler_name = self.comboBox_vv_ler.currentText()

    # Get the layout manager
    layout_manager = project.layoutManager()

    # Find the layout by name and remove it
    for layout in layout_manager.layouts():
        if layout.name() == "Vandværksbeskrivelser" or  layout.name() == "BNBOBeskrivelser":
            layout_manager.removeLayout(layout)


    QgsLayerDefinition().loadLayerDefinition(
        str(os.path.join(os.path.dirname(__file__), '..')) + "/dependencies/VandværksbeskrivelserBasiskort.qlr",
        project, project.layerTreeRoot())


    IOL = project.mapLayersByName("{}".format(self.comboBox_vv_iol.currentText()))[0]
    BNBO_og = project.mapLayersByName("{}".format(self.comboBox_vv_bnbo.currentText()))[0]

    if "areal_m2_str" not in BNBO_og.fields():
        BNBO_og.startEditing()
        BNBO_og.addAttribute(QgsField("areal_m2_str", QVariant.String, len=0))
        BNBO_og.updateFields()
        # Create an expression to calculate values for the new field
        expression = QgsExpression("$area")  # Example expression

        # Get the index of the new field
        new_field_index = BNBO_og.fields().indexOf("areal_m2_str")

        # Update the new field with the calculated values
        for feature in BNBO_og.getFeatures():
            context = QgsExpressionContext()
            context.appendScopes(QgsExpressionContextUtils.globalProjectLayerScopes(BNBO_og))
            context.setFeature(feature)
            feature[new_field_index] = expression.evaluate(context)
            BNBO_og.updateFeature(feature)

        # Commit the changes
        BNBO_og.commitChanges()



    BNBO = processing.run("native:fieldcalculator", {'INPUT': BNBO_og, 'FIELD_NAME':
        'tilladelser', 'FIELD_TYPE': 2, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0, 'FORMULA': '"tilladelse"',
                                                      'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

    project.addMapLayer(BNBO)
    project.removeMapLayer(BNBO_og)




    Boringer = project.mapLayersByName("{}".format(Boringer_name))[0]
    Nitratsårbarhed = project.mapLayersByName("{}".format(Nitratsårbarhed_name))[0]
    Grundvandsdannelse = project.mapLayersByName("{}".format(Grundvandsdannelse_name))[0]
    NFI = project.mapLayersByName("{}".format(NFI_name))[0]
    IO = project.mapLayersByName("{}".format(IO_name))[0]
    GVDPT = project.mapLayersByName("{}".format(GVDPT_name))[0]
    PT = project.mapLayersByName("{}".format(PT_name))[0]
    Ler = project.mapLayersByName("{}".format(Ler_name))[0]
    Fredskov = project.mapLayersByName("FREDSKOV")[0]
    BeskyttetNatur = project.mapLayersByName("Beskyttede naturtyper")[0]
    ArealAnvendelse = project.mapLayersByName("AIS Arealanvendelse")[0]
    OSD = project.mapLayersByName("di_template")[0]
    Baggrundskort = project.mapLayersByName("Skærmkort - grå")[0]






    existing_field_names = [field.name() for field in Boringer.fields()]
    if "dgu_indtag" not in existing_field_names:
        Boringer.startEditing()
        Boringer.addAttribute(QgsField("dgu_indtag", QVariant.String, len=100))
        Boringer.updateFields()
        # Create an expression to calculate values for the new field
        expression = QgsExpression("\"dgu_nr\" || '_' || \"indtag\"")  # Example expression

        # Get the index of the new field
        new_field_index = Boringer.fields().indexOf("dgu_indtag")

        # Update the new field with the calculated values
        for feature in Boringer.getFeatures():
                context = QgsExpressionContext()
                context.appendScopes(QgsExpressionContextUtils.globalProjectLayerScopes(Boringer))
                context.setFeature(feature)
                feature[new_field_index] = expression.evaluate(context)
                Boringer.updateFeature(feature)

        # Commit the changes
        Boringer.commitChanges()


    if "dgu_list" not in existing_field_names:
        Boringer.startEditing()
        Boringer.addAttribute(QgsField("dgu_list", QVariant.String, len=255))
        Boringer.updateFields()
        # Create an expression to calculate values for the new field
        expression = QgsExpression("concatenate_unique(\"dgu_indtag\", group_by:=\"anl_id\", concatenator:=', ') ")  # Example expression
        #
        # Get the index of the new field
        new_field_index = Boringer.fields().indexOf("dgu_list")

        # Update the new field with the calculated values
        for feature in Boringer.getFeatures():
                context = QgsExpressionContext()
                context.appendScopes(QgsExpressionContextUtils.globalProjectLayerScopes(Boringer))
                context.setFeature(feature)
                feature[new_field_index] = expression.evaluate(context)
                Boringer.updateFeature(feature)

        # Commit the changes
        Boringer.commitChanges()



    pathh = str(os.path.join(os.path.dirname(__file__), '..'))
    processing.run("native:joinattributestable", {
        'INPUT': IOL,
        'FIELD': 'anl_id',
        'INPUT_2': Boringer,
        'FIELD_2': 'anl_id', 'FIELDS_TO_COPY': ['dgu_list'], 'METHOD': 1, 'DISCARD_NONMATCHING': False, 'PREFIX': '',
        'OUTPUT': str(Path(__file__).resolve().parent.parent)+ "/tempfiles/IOL.shp"})
    project.removeMapLayer(IOL)

    IOL = QgsVectorLayer(str(Path(__file__).resolve().parent.parent) + "/tempfiles/IOL.shp", "IOL" ,'ogr')
    project.addMapLayer(IOL)

    existing_field_names_IOL = [field.name() for field in IOL.fields()]
    if "anlmag" not in existing_field_names_IOL:
        IOL.startEditing()
        IOL.addAttribute(QgsField("anlmag", QVariant.String, len=100))
        IOL.updateFields()
        # Create an expression to calculate values for the new field
        expression = QgsExpression("\"anl_id\" || '_' || \"magasin\"")  # Example expression

        # Get the index of the new field
        new_field_index = IOL.fields().indexOf("anlmag")

        # Update the new field with the calculated values
        for feature in IOL.getFeatures():
            context = QgsExpressionContext()
            context.appendScopes(QgsExpressionContextUtils.globalProjectLayerScopes(Boringer))
            context.setFeature(feature)
            feature[new_field_index] = expression.evaluate(context)
            IOL.updateFeature(feature)

        # Commit the changes
        IOL.commitChanges()




    original_fields = BNBO.fields()


    # Helper function to find the field that best matches a list of patterns
    def find_best_matching_field(patterns, field_names):
        from difflib import get_close_matches
        matches = {}
        for pattern in patterns:
            closest_matches = get_close_matches(pattern, field_names)
            print(closest_matches)
            if closest_matches:
                matches[pattern] = closest_matches[0]
        return matches

    concat_fields = ["dgunr", "tilladelser", "areal_m2_str"]
    unique_concat_fields = ["kommentar"]
    group_by_field = "anlaegid"

    # Find the best matching fields
    field_names = [field.name() for field in original_fields]
    concat_field_matches = find_best_matching_field(concat_fields, field_names)
    unique_concat_field_matches = find_best_matching_field(unique_concat_fields, field_names)
    group_by_field_matches = find_best_matching_field(["anlaegid"], field_names)#"anlaegid"
    print(group_by_field_matches)
    print(concat_field_matches)
    print(unique_concat_field_matches)
    # Define the aggregation expressions
    aggregations = []

    # Add concatenation aggregations
    for field in concat_field_matches.values():
        relfield = BNBO.fields().field(field)
        z = f'"{relfield.name()}"'
        type = relfield.type()
        precision = relfield.precision()
        aggregations.append({
            'aggregate': 'concatenate',
            'delimiter': ', ',
            'input': z,
            'length': 0,
            'name': relfield.name(),
            'precision': precision,
            'sub_type': 0,
            'type': type,
            'type_name': relfield.type()
        })

    # Add unique concatenation aggregations
    for field in unique_concat_field_matches.values():
        relfield = BNBO.fields().field(field)
        z = relfield.name()

        aggregations.append({
            'aggregate': 'concatenate_unique',
            'delimiter': ', ',
            'input': f'"{z}"',
            'length': 254,
            'name': relfield.name(),
            'precision': relfield.precision(),
            'sub_type': 0,
            'type': relfield.type(),
            'type_name': relfield.type()
        })

    # Add first value aggregations for all other fields
    for field in BNBO.fields():
        if field.name() not in concat_fields + unique_concat_fields:
            aggregations.append({
                'aggregate': 'first_value',
                'delimiter': ',',
                'input': f'"{field.name()}"',
                'length': 254,
                'name': field.name(),
                'precision': field.precision(),
                'sub_type': 0,
                'type': field.type(),
                'type_name': field.type()
            })


    # Run the aggregate processing algorithm
    BNBO_temp = processing.run("native:aggregate", {
        'INPUT': BNBO,
        'GROUP_BY': list(group_by_field_matches.values())[0],
        'AGGREGATES': aggregations,
        'OUTPUT': 'TEMPORARY_OUTPUT'})["OUTPUT"]


    BNBO_temp.setName(f"BNBO")
    project.addMapLayer(BNBO_temp)
    project.removeMapLayer(BNBO)

    del BNBO
    BNBO = BNBO_temp


    # Get the root layer tree
    root = project.layerTreeRoot()

    group_name = "Vandværksbeskrivelser"
    group = root.insertGroup(0, group_name)


    if self.checkBox_vv_geologiskeprofiler.isChecked() and len(self.lineEdit_vv_geologiskeprofiler.text()) > 1:
        profillinjer = project.mapLayersByName("{}".format(self.comboBox_vv_profiler.currentText()))[0]
        profill_mappe = self.lineEdit_vv_geologiskeprofiler.text()

        if self.checkBox_7.isChecked():
            wkt_geometries = [
                'Polygon ((602693.33280902425758541 6406196.00123407691717148, 635895.99882872845046222 6353329.62038716953247786, 623646.47155932313762605 6253399.26634728349745274, 603338.044770571985282 6176678.54292311239987612, 566589.46296235569752753 6161850.16780751664191484, 547006.33713034563697875 6155886.58216320257633924, 547127.22062313579954207 6152421.25536988396197557, 542896.2983754794113338 6152945.0838386407122016, 541969.52493075467646122 6149520.05154291912913322, 545072.2012457026867196 6144765.30082650482654572, 546401.9196663947077468 6132314.3010691162198782, 550673.13641164789441973 6124215.10705217346549034, 560827.34980602352879941 6100844.29844607133418322, 579846.35267167922575027 6066996.92046481929719448, 379018.57664958515670151 6051201.47740690130740404, 416089.51443857525009662 6328427.62087239325046539, 484751.33834340039175004 6445120.48591251950711012, 602693.33280902425758541 6406196.00123407691717148))',  # Jylland
                'Polygon ((542997.0346194711746648 6152844.34759465325623751, 544427.48928415507543832 6152723.46410186309367418, 546442.21416399150621146 6152360.81362349260598421, 547248.10411592607852072 6152320.51912589650601149, 547187.66236953088082373 6155725.40417282003909349, 555266.70913767500314862 6158263.9575214134529233, 564574.73808251926675439 6161124.86685078125447035, 603297.75027297507040203 6175872.65297118481248617, 622316.75313863065093756 6150728.88647082634270191, 630698.00863875017967075 6110273.21088371053338051, 623767.35505211295094341 6075942.29893129784613848, 615386.09955199342221022 6056439.76209448184818029, 580571.65362842008471489 6067399.86544079147279263, 560585.5828204428544268 6103181.37930668611079454, 551076.08138761494774371 6125101.58599930629134178, 547207.80961832904722542 6132676.95154749136418104, 545273.67373368609696627 6144845.88982170354574919, 542110.55567234288901091 6149560.3460405208170414, 542997.0346194711746648 6152844.34759465325623751))',  # Fyn
                'Polygon ((606904.10780788213014603 6176376.33419114165008068, 640106.77382758620660752 6227630.93513418082147837, 685558.96711669582873583 6247939.36192293185740709, 715538.07332866173237562 6247294.64996138401329517, 731011.16040580533444881 6233755.69876888301223516, 762924.40250241430476308 6093208.49115149490535259, 694907.29055913677439094 6024546.66724666953086853, 619798.34703883517067879 6054848.129439408890903, 625600.75469276402145624 6075156.5562281608581543, 629469.02646204992197454 6091274.35526685137301683, 633014.9422505620168522 6109648.64617095980793238, 628501.95851972850505263 6134792.41267131827771664, 623988.97478889487683773 6152844.34759465232491493, 606904.10780788213014603 6176376.33419114165008068))'  # Sjælland
            ]
            names = ['jylland', 'fyn', 'sjaelland']  # Corresponding names for the WKT geometries

            # Find the best overlap for the entire layer
            best_wkt_name, intersections = find_best_overlap_for_layer(profillinjer, wkt_geometries, names)

            område = best_wkt_name

            for feature in profillinjer.getFeatures():
                profillinjer.removeSelection()
                profillinjer.select(feature.id())

                vertices = processing.run("native:extractvertices", {
                    'INPUT': QgsProcessingFeatureSourceDefinition(profillinjer.id(), selectedFeaturesOnly=True, featureLimit=-1,
                                                                  geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),
                    'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

                points = []
                for idx, feature in enumerate(vertices.getFeatures()):
                    points.append(feature.geometry().asPoint())

                plot_automatic_crosssections(line_points=points, område=område, number_of_points=300, id=str(feature[f'{profilID}']), savepath=profill_mappe)
                profillinjer.removeSelection()

            plt.close('all')

    else:
        profillinjer = None
        profill_mappe = None


    # Set the custom layer order
    if profillinjer != None:
        order = [Baggrundskort, ArealAnvendelse, OSD, Fredskov, BeskyttetNatur, Grundvandsdannelse, Ler, Nitratsårbarhed, NFI, IO, GVDPT, PT, IOL, profillinjer, BNBO, Boringer]
    else:
        order = [Baggrundskort, ArealAnvendelse, OSD, Fredskov, BeskyttetNatur, Grundvandsdannelse, Ler, Nitratsårbarhed, NFI, IO, GVDPT, PT, IOL, BNBO, Boringer]

    for layer in reversed(order):
        cloned_layer_tree_layer = layer.clone()
        project.removeMapLayer(layer)
        project.addMapLayer(cloned_layer_tree_layer, False)  # False is the key
        group.addLayer(cloned_layer_tree_layer)

    allayers = [layer for layer in project.mapLayers().values()]

    IOL = project.mapLayersByName("IOL")[0]
    BNBO = project.mapLayersByName("BNBO")[0]
    Boringer = project.mapLayersByName("{}".format(Boringer_name))[0]
    Nitratsårbarhed = project.mapLayersByName("{}".format(Nitratsårbarhed_name))[0]
    Grundvandsdannelse = project.mapLayersByName("{}".format(Grundvandsdannelse_name))[0]
    NFI = project.mapLayersByName("{}".format(NFI_name))[0]
    IO = project.mapLayersByName("{}".format(IO_name))[0]
    GVDPT = project.mapLayersByName("{}".format(GVDPT_name))[0]
    PT = project.mapLayersByName("{}".format(PT_name))[0]
    Ler = project.mapLayersByName("{}".format(Ler_name))[0]
    Fredskov = project.mapLayersByName("FREDSKOV")[0]
    BeskyttetNatur = project.mapLayersByName("Beskyttede naturtyper")[0]
    ArealAnvendelse = project.mapLayersByName("AIS Arealanvendelse")[0]
    OSD = project.mapLayersByName("di_template")[0]
    Baggrundskort = project.mapLayersByName("Skærmkort - grå")[0]

    if self.checkBox_vv_geologiskeprofiler.isChecked() and len(self.lineEdit_vv_geologiskeprofiler.text()) > 1:
        profillinjer = project.mapLayersByName("{}".format(self.comboBox_vv_profiler.currentText()))[0]


    laydict = {IOL: 'IOL',
               BNBO: 'BNBO',
               Boringer: 'Boringer',
               Nitratsårbarhed: 'Nitratsårbarhed',
               Grundvandsdannelse: 'Grundvandsdannelse',
               NFI: 'NFI',
               IO: 'IO',
               GVDPT: 'Grundvandsdannende partikler',
               PT: 'Partikler til indvindingsoplandene',
               Ler: 'Akummuleret lertykkelse'}

    if profillinjer != None:
        laydict[profillinjer] = 'Profillinjer'

    print(laydict)

    for lay, name in laydict.items():
        #try:
            lay.loadNamedStyle(str(os.path.join(os.path.dirname(__file__), '..')) + f"/Styles/Vandværksbeskrivelser/{name}.qml")
            lay.setName(f'{name}')
            lay.triggerRepaint()
       # except Exception as error:
        #    print("An error occurred:", error)  # An error occurred: name 'x' is not defined
    Nitratsårbarhed.loadNamedStyle(str(os.path.join(os.path.dirname(__file__), '..')) + f"/Styles/Vandværksbeskrivelser/Nitratsårbarhed.qml")

    layout = QgsPrintLayout(project)
    layout.initializeDefaults()

    with open(str(os.path.dirname(__file__)) + "/Vandværksbeskrivelser.qpt") as f:
        template_content = f.read()
        doc = QDomDocument()
        doc.setContent(template_content)
        items, ok = layout.loadFromTemplate(doc, QgsReadWriteContext(), True)
        layout.setName('Vandværksbeskrivelser')
        project.layoutManager().addLayout(layout)

    layout.refresh()

    layout_bnbo = QgsPrintLayout(project)
    layout_bnbo.initializeDefaults()

    with open(str(os.path.dirname(__file__)) + "/BNBOBeskrivelser.qpt") as f:
        template_content = f.read()
        doc = QDomDocument()
        doc.setContent(template_content)
        items, ok = layout_bnbo.loadFromTemplate(doc, QgsReadWriteContext(), True)
        layout_bnbo.setName('BNBOBeskrivelser')
        project.layoutManager().addLayout(layout_bnbo)

    layout_bnbo.refresh()

    try:
        profilimage = layout.itemById('Profil')
        your_atlas_field = 'anlmag'
        expression = f"'{profill_mappe}/'+ to_string(attribute(@atlas_feature, '{your_atlas_field}')) + '.png'"
        profilimage.setPicturePath(expression)
    except Exception as error:
        print(error)


    musthavelayouts = {}

    musthavelayouts["kort1"] = [Baggrundskort, IOL, Boringer]
    musthavelayouts["kort2"] = [Baggrundskort, IOL, Boringer, Nitratsårbarhed]
    musthavelayouts["kort3"] = [Baggrundskort, IOL, Boringer, IO, NFI, OSD, Grundvandsdannelse]
    musthavelayouts["kort4"] = [Baggrundskort, IOL, Boringer, IO, Fredskov, OSD, BeskyttetNatur]
    musthavelayouts["kort5"] = [Baggrundskort, IOL, Boringer, Ler]
    musthavelayouts["kort6"] = [Baggrundskort, IOL, Boringer, PT]
    musthavelayouts["kort7"] = [Baggrundskort, IOL, Boringer, GVDPT]
    musthavelayouts["kort8"] = [Baggrundskort, IOL, Boringer, ArealAnvendelse]
    musthavelayouts["bnbo"] = [Baggrundskort, IOL, Boringer, BNBO]

    if profillinjer != None:
        for mhlay in [musthavelayouts["kort5"], musthavelayouts["kort6"], musthavelayouts["kort7"],musthavelayouts["kort8"]]:
            mhlay += [profillinjer]


    allayers = [layer for layer in project.mapLayers().values()]
    for key in musthavelayouts:
        for layer in allayers:
            project.layerTreeRoot().findLayer(layer.id()).setItemVisibilityChecked(False)


        for count, values in enumerate(musthavelayouts.get(key)):
            project.layerTreeRoot().findLayer(values.id()).setItemVisibilityChecked(True)

        for item in layout.items():
            if isinstance(item, QgsLayoutItemLegend):
                item.setAutoUpdateModel(False)

        mapThemeRecord = QgsMapThemeCollection.createThemeFromCurrentState(
            QgsProject.instance().layerTreeRoot(),
            iface.layerTreeView().layerTreeModel()
        )
        mapThemesCollection.insert("{}".format(key), mapThemeRecord)

        for layoutitems in layout.items():
            if isinstance(layoutitems, QgsLayoutItemMap):
                if layoutitems.id() == key:
                    layoutitems.setFollowVisibilityPreset(True)
                    layoutitems.setFollowVisibilityPresetName("{}".format(key))


        layout.refresh()

    layout.refresh()

    self.pushButton_vvbeskrivelser.setText("Vandværksbeskrivelserne er færdige!")
    #QgsMessageLog.logMessage(f'''Vandværksbeskrivelserne er færdige, og kan tilgåes under "Projekt" -> "Layout" -> "Vandværksbeskrivelser" (derudover er der også lavet "BNBO-Beskrivelser")''')

    #self.pushButton_vvbeskrivelser.setEnabled(True)
    #self.repaint()
