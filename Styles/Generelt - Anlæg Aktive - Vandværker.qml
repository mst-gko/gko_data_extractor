<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis labelsEnabled="0" maxScale="0" simplifyMaxScale="1" styleCategories="AllStyleCategories" symbologyReferenceScale="-1" hasScaleBasedVisibilityFlag="0" simplifyAlgorithm="0" simplifyDrawingTol="1" readOnly="0" version="3.26.3-Buenos Aires" simplifyLocal="1" minScale="100000000" simplifyDrawingHints="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endExpression="" limitMode="0" accumulate="0" endField="" mode="0" fixedDuration="0" startExpression="" durationUnit="min" enabled="0" startField="" durationField="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zscale="1" extrusionEnabled="0" clamping="Terrain" symbology="Line" respectLayerSymbol="1" showMarkerSymbolInSurfacePlots="0" extrusion="0" binding="Centroid" type="IndividualFeatures" zoffset="0">
    <data-defined-properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="" type="line" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleLine" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="align_dash_pattern" type="QString"/>
            <Option value="square" name="capstyle" type="QString"/>
            <Option value="5;2" name="customdash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
            <Option value="MM" name="customdash_unit" type="QString"/>
            <Option value="0" name="dash_pattern_offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
            <Option value="0" name="draw_inside_polygon" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="190,178,151,255" name="line_color" type="QString"/>
            <Option value="solid" name="line_style" type="QString"/>
            <Option value="0.6" name="line_width" type="QString"/>
            <Option value="MM" name="line_width_unit" type="QString"/>
            <Option value="0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="0" name="ring_filter" type="QString"/>
            <Option value="0" name="trim_distance_end" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_end_unit" type="QString"/>
            <Option value="0" name="trim_distance_start" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_start_unit" type="QString"/>
            <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
            <Option value="0" name="use_custom_dash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="190,178,151,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.6"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="" type="fill" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleFill" enabled="1" pass="0">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale" type="QString"/>
            <Option value="190,178,151,255" name="color" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="136,127,108,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.2" name="outline_width" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="solid" name="style" type="QString"/>
          </Option>
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="190,178,151,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="136,127,108,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="190,178,151,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="diamond" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="136,127,108,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.2" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="190,178,151,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="136,127,108,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <userNotes value="&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;>&#xa;&lt;html>&lt;head>&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; />&lt;style type=&quot;text/css&quot;>&#xa;p, li { white-space: pre-wrap; }&#xa;&lt;/style>&lt;/head>&lt;body style=&quot; font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;&quot;>&#xa;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>Aktive anlæg er udvalgt ved at anlæg (drwplant) efter følgende kriterium:&lt;/p>&#xa;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>&lt;br />&lt;/p>&#xa;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>WHERE &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#9876aa;&quot;>aktiv &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>not ilike &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#6a8759;&quot;>'Inaktiv' &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>AND &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#9876aa;&quot;>tilladelsesstatus &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>not ilike &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>(&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#6a8759;&quot;>'Tilladelse bortfaldet'&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>)&lt;/span>&lt;/p>&#xa;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>&lt;br />&lt;/p>&#xa;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>Endvidere skal der inden for det seneste afsluttede kalender år være indberettet en indvunden vandmængde større end nul:&lt;/p>&#xa;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>&lt;br />&lt;/p>&#xa;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>SELECT &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#ffc66d;&quot;>* &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>FROM &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>jupiter.wrrcatchment&lt;br />&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>WHERE &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#9876aa;&quot;>amount &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>&amp;gt; &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#6897bb;&quot;>0 &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>AND &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#ffc66d;&quot;>EXTRACT&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>(&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>YEAR FROM &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#ffc66d;&quot;>now&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>())-&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#6897bb;&quot;>1 &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>= &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#ffc66d;&quot;>EXTRACT&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>(&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>YEAR FROM &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#9876aa;&quot;>enddate&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>)&lt;/span>&lt;/p>&#xa;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>&lt;br />&lt;/p>&#xa;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>Udtræk er defineret i github\pgjupiter\qlr_view_kortskab.sql under &lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6; background-color:#2b2b2b;&quot;>mstjupiter.anlaeg_aktiv&lt;/span>&lt;/p>&#xa;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>&lt;br />&lt;/p>&#xa;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>20211005 JAKLA&lt;/p>&lt;/body>&lt;/html>"/>
  <renderer-v2 symbollevels="0" referencescale="-1" enableorderby="0" type="RuleRenderer" forceraster="0">
    <rules key="{26cabbc1-6da2-4048-b6c4-33943f92069f}">
      <rule label="Offentlige fælles *vandforsyningsanlæg" symbol="0" key="{d42bef7d-9790-445a-88e3-c9d57a717ade}" filter="&quot;virksomhedstype&quot; = 'Offentlige fælles vandforsyningsanlæg'"/>
      <rule label="Private fælles *vandforsyningsanlæg" symbol="1" key="{d50b05cd-aa6c-49af-8669-c7631a643f2e}" filter="&quot;virksomhedstype&quot; = 'Private fælles vandforsyningsanlæg'"/>
    </rules>
    <symbols>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="0" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="219,30,42,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="triangle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="128,17,25,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="4" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="219,30,42,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="triangle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="128,17,25,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="4"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="1" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="84,176,74,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="triangle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="61,128,53,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="4" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="84,176,74,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="triangle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="61,128,53,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="4"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option value="0" name="embeddedWidgets/count" type="int"/>
      <Option value="&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;>&#xa;&lt;html>&lt;head>&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; />&lt;style type=&quot;text/css&quot;>&#xa;p, li { white-space: pre-wrap; }&#xa;&lt;/style>&lt;/head>&lt;body style=&quot; font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;&quot;>&#xa;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>Aktive anlæg er udvalgt ved at anlæg (drwplant) efter følgende kriterium:&lt;/p>&#xa;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>&lt;br />&lt;/p>&#xa;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>WHERE &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#9876aa;&quot;>aktiv &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>not ilike &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#6a8759;&quot;>'Inaktiv' &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>AND &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#9876aa;&quot;>tilladelsesstatus &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>not ilike &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>(&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#6a8759;&quot;>'Tilladelse bortfaldet'&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>)&lt;/span>&lt;/p>&#xa;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>&lt;br />&lt;/p>&#xa;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>Endvidere skal der inden for det seneste afsluttede kalender år være indberettet en indvunden vandmængde større end nul:&lt;/p>&#xa;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>&lt;br />&lt;/p>&#xa;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>SELECT &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#ffc66d;&quot;>* &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>FROM &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>jupiter.wrrcatchment&lt;br />&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>WHERE &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#9876aa;&quot;>amount &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>&amp;gt; &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#6897bb;&quot;>0 &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>AND &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#ffc66d;&quot;>EXTRACT&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>(&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>YEAR FROM &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#ffc66d;&quot;>now&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>())-&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#6897bb;&quot;>1 &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>= &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#ffc66d;&quot;>EXTRACT&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>(&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#cc7832;&quot;>YEAR FROM &lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#9876aa;&quot;>enddate&lt;/span>&lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>)&lt;/span>&lt;/p>&#xa;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6;&quot;>&lt;br />&lt;/p>&#xa;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>Udtræk er defineret i github\pgjupiter\qlr_view_kortskab.sql under &lt;span style=&quot; font-family:'JetBrains Mono','monospace'; font-size:9.8pt; color:#a9b7c6; background-color:#2b2b2b;&quot;>mstjupiter.anlaeg_aktiv&lt;/span>&lt;/p>&#xa;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>&lt;br />&lt;/p>&#xa;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>20211005 JAKLA&lt;/p>&lt;/body>&lt;/html>" name="userNotes" type="QString"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory maxScaleDenominator="1e+08" sizeType="MM" penWidth="0" spacingUnitScale="3x:0,0,0,0,0,0" rotationOffset="270" height="15" enabled="0" labelPlacementMethod="XHeight" spacing="5" opacity="1" scaleBasedVisibility="0" diagramOrientation="Up" penAlpha="255" sizeScale="3x:0,0,0,0,0,0" spacingUnit="MM" width="15" direction="0" backgroundAlpha="255" scaleDependency="Area" barWidth="5" minimumSize="0" showAxis="1" minScaleDenominator="0" penColor="#000000" lineSizeType="MM" backgroundColor="#ffffff" lineSizeScale="3x:0,0,0,0,0,0">
      <fontProperties strikethrough="0" bold="0" italic="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0"/>
      <attribute label="" color="#000000" field="" colorOpacity="1"/>
      <axisSymbol>
        <symbol frame_rate="10" is_animated="0" alpha="1" name="" type="line" clip_to_extent="1" force_rhr="0">
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
          <layer locked="0" class="SimpleLine" enabled="1" pass="0">
            <Option type="Map">
              <Option value="0" name="align_dash_pattern" type="QString"/>
              <Option value="square" name="capstyle" type="QString"/>
              <Option value="5;2" name="customdash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
              <Option value="MM" name="customdash_unit" type="QString"/>
              <Option value="0" name="dash_pattern_offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
              <Option value="0" name="draw_inside_polygon" type="QString"/>
              <Option value="bevel" name="joinstyle" type="QString"/>
              <Option value="35,35,35,255" name="line_color" type="QString"/>
              <Option value="solid" name="line_style" type="QString"/>
              <Option value="0.26" name="line_width" type="QString"/>
              <Option value="MM" name="line_width_unit" type="QString"/>
              <Option value="0" name="offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="offset_unit" type="QString"/>
              <Option value="0" name="ring_filter" type="QString"/>
              <Option value="0" name="trim_distance_end" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_end_unit" type="QString"/>
              <Option value="0" name="trim_distance_start" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_start_unit" type="QString"/>
              <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
              <Option value="0" name="use_custom_dash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" priority="0" zIndex="0" showAll="1" placement="0" dist="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anlaegsid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="supplant" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="kommunenr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anlaegnavn" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anlaegadr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anlaegpostnr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dataejer" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="aktiv" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="virksomhedstype" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anlaegstype" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="indvindingsformaal" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="vandtype" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="url_plant" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="kommentar" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="amount" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="startdate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="enddate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="id" index="0"/>
    <alias name="" field="anlaegsid" index="1"/>
    <alias name="" field="supplant" index="2"/>
    <alias name="" field="kommunenr" index="3"/>
    <alias name="" field="anlaegnavn" index="4"/>
    <alias name="" field="anlaegadr" index="5"/>
    <alias name="" field="anlaegpostnr" index="6"/>
    <alias name="" field="dataejer" index="7"/>
    <alias name="" field="aktiv" index="8"/>
    <alias name="" field="virksomhedstype" index="9"/>
    <alias name="" field="anlaegstype" index="10"/>
    <alias name="" field="indvindingsformaal" index="11"/>
    <alias name="" field="vandtype" index="12"/>
    <alias name="" field="url_plant" index="13"/>
    <alias name="" field="kommentar" index="14"/>
    <alias name="" field="amount" index="15"/>
    <alias name="" field="startdate" index="16"/>
    <alias name="" field="enddate" index="17"/>
  </aliases>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="anlaegsid" applyOnUpdate="0"/>
    <default expression="" field="supplant" applyOnUpdate="0"/>
    <default expression="" field="kommunenr" applyOnUpdate="0"/>
    <default expression="" field="anlaegnavn" applyOnUpdate="0"/>
    <default expression="" field="anlaegadr" applyOnUpdate="0"/>
    <default expression="" field="anlaegpostnr" applyOnUpdate="0"/>
    <default expression="" field="dataejer" applyOnUpdate="0"/>
    <default expression="" field="aktiv" applyOnUpdate="0"/>
    <default expression="" field="virksomhedstype" applyOnUpdate="0"/>
    <default expression="" field="anlaegstype" applyOnUpdate="0"/>
    <default expression="" field="indvindingsformaal" applyOnUpdate="0"/>
    <default expression="" field="vandtype" applyOnUpdate="0"/>
    <default expression="" field="url_plant" applyOnUpdate="0"/>
    <default expression="" field="kommentar" applyOnUpdate="0"/>
    <default expression="" field="amount" applyOnUpdate="0"/>
    <default expression="" field="startdate" applyOnUpdate="0"/>
    <default expression="" field="enddate" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" unique_strength="0" field="id" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="anlaegsid" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="supplant" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="kommunenr" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="anlaegnavn" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="anlaegadr" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="anlaegpostnr" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="dataejer" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="aktiv" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="virksomhedstype" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="anlaegstype" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="indvindingsformaal" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="vandtype" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="url_plant" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="kommentar" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="amount" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="startdate" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="enddate" constraints="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="id"/>
    <constraint exp="" desc="" field="anlaegsid"/>
    <constraint exp="" desc="" field="supplant"/>
    <constraint exp="" desc="" field="kommunenr"/>
    <constraint exp="" desc="" field="anlaegnavn"/>
    <constraint exp="" desc="" field="anlaegadr"/>
    <constraint exp="" desc="" field="anlaegpostnr"/>
    <constraint exp="" desc="" field="dataejer"/>
    <constraint exp="" desc="" field="aktiv"/>
    <constraint exp="" desc="" field="virksomhedstype"/>
    <constraint exp="" desc="" field="anlaegstype"/>
    <constraint exp="" desc="" field="indvindingsformaal"/>
    <constraint exp="" desc="" field="vandtype"/>
    <constraint exp="" desc="" field="url_plant"/>
    <constraint exp="" desc="" field="kommentar"/>
    <constraint exp="" desc="" field="amount"/>
    <constraint exp="" desc="" field="startdate"/>
    <constraint exp="" desc="" field="enddate"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column width="-1" name="id" hidden="0" type="field"/>
      <column width="-1" name="anlaegsid" hidden="0" type="field"/>
      <column width="-1" name="supplant" hidden="0" type="field"/>
      <column width="-1" name="kommunenr" hidden="0" type="field"/>
      <column width="-1" name="anlaegnavn" hidden="0" type="field"/>
      <column width="-1" name="anlaegadr" hidden="0" type="field"/>
      <column width="-1" name="anlaegpostnr" hidden="0" type="field"/>
      <column width="-1" name="dataejer" hidden="0" type="field"/>
      <column width="-1" name="aktiv" hidden="0" type="field"/>
      <column width="-1" name="virksomhedstype" hidden="0" type="field"/>
      <column width="-1" name="anlaegstype" hidden="0" type="field"/>
      <column width="-1" name="indvindingsformaal" hidden="0" type="field"/>
      <column width="-1" name="vandtype" hidden="0" type="field"/>
      <column width="-1" name="url_plant" hidden="0" type="field"/>
      <column width="-1" name="kommentar" hidden="0" type="field"/>
      <column width="-1" hidden="1" type="actions"/>
      <column width="-1" name="amount" hidden="0" type="field"/>
      <column width="-1" name="startdate" hidden="0" type="field"/>
      <column width="-1" name="enddate" hidden="0" type="field"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="aktiv" editable="1"/>
    <field name="amount" editable="1"/>
    <field name="anlaegadr" editable="1"/>
    <field name="anlaegnavn" editable="1"/>
    <field name="anlaegpostnr" editable="1"/>
    <field name="anlaegsid" editable="1"/>
    <field name="anlaegstype" editable="1"/>
    <field name="dataejer" editable="1"/>
    <field name="enddate" editable="1"/>
    <field name="id" editable="1"/>
    <field name="indvindingsformaal" editable="1"/>
    <field name="kommentar" editable="1"/>
    <field name="kommunenr" editable="1"/>
    <field name="startdate" editable="1"/>
    <field name="supplant" editable="1"/>
    <field name="url_plant" editable="1"/>
    <field name="vandtype" editable="1"/>
    <field name="virksomhedstype" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="aktiv"/>
    <field labelOnTop="0" name="amount"/>
    <field labelOnTop="0" name="anlaegadr"/>
    <field labelOnTop="0" name="anlaegnavn"/>
    <field labelOnTop="0" name="anlaegpostnr"/>
    <field labelOnTop="0" name="anlaegsid"/>
    <field labelOnTop="0" name="anlaegstype"/>
    <field labelOnTop="0" name="dataejer"/>
    <field labelOnTop="0" name="enddate"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="indvindingsformaal"/>
    <field labelOnTop="0" name="kommentar"/>
    <field labelOnTop="0" name="kommunenr"/>
    <field labelOnTop="0" name="startdate"/>
    <field labelOnTop="0" name="supplant"/>
    <field labelOnTop="0" name="url_plant"/>
    <field labelOnTop="0" name="vandtype"/>
    <field labelOnTop="0" name="virksomhedstype"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="aktiv"/>
    <field reuseLastValue="0" name="amount"/>
    <field reuseLastValue="0" name="anlaegadr"/>
    <field reuseLastValue="0" name="anlaegnavn"/>
    <field reuseLastValue="0" name="anlaegpostnr"/>
    <field reuseLastValue="0" name="anlaegsid"/>
    <field reuseLastValue="0" name="anlaegstype"/>
    <field reuseLastValue="0" name="dataejer"/>
    <field reuseLastValue="0" name="enddate"/>
    <field reuseLastValue="0" name="id"/>
    <field reuseLastValue="0" name="indvindingsformaal"/>
    <field reuseLastValue="0" name="kommentar"/>
    <field reuseLastValue="0" name="kommunenr"/>
    <field reuseLastValue="0" name="startdate"/>
    <field reuseLastValue="0" name="supplant"/>
    <field reuseLastValue="0" name="url_plant"/>
    <field reuseLastValue="0" name="vandtype"/>
    <field reuseLastValue="0" name="virksomhedstype"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"anlaegnavn"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
