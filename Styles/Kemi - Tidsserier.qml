<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyAlgorithm="0" maxScale="0" readOnly="0" symbologyReferenceScale="-1" labelsEnabled="0" simplifyDrawingTol="1" simplifyLocal="1" simplifyMaxScale="1" minScale="100000000" version="3.26.3-Buenos Aires" simplifyDrawingHints="0" hasScaleBasedVisibilityFlag="0" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal durationUnit="min" durationField="" mode="0" enabled="0" fixedDuration="0" startField="" endField="dato" startExpression="" limitMode="0" endExpression="" accumulate="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zscale="1" extrusion="0" zoffset="0" clamping="Terrain" type="IndividualFeatures" extrusionEnabled="0" showMarkerSymbolInSurfacePlots="0" symbology="Line" respectLayerSymbol="1" binding="Centroid">
    <data-defined-properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" name="" type="line" is_animated="0" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="square"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="114,155,111,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="0.6"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop v="0" k="align_dash_pattern"/>
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="114,155,111,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.6" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="trim_distance_end"/>
          <prop v="3x:0,0,0,0,0,0" k="trim_distance_end_map_unit_scale"/>
          <prop v="MM" k="trim_distance_end_unit"/>
          <prop v="0" k="trim_distance_start"/>
          <prop v="3x:0,0,0,0,0,0" k="trim_distance_start_map_unit_scale"/>
          <prop v="MM" k="trim_distance_start_unit"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" name="" type="fill" is_animated="0" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="color" type="QString" value="114,155,111,255"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="81,111,79,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="style" type="QString" value="solid"/>
          </Option>
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="114,155,111,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="81,111,79,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.2" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" name="" type="marker" is_animated="0" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="114,155,111,255"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="diamond"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="81,111,79,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="3"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="114,155,111,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="diamond" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="81,111,79,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.2" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="3" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 referencescale="-1" forceraster="0" symbollevels="0" type="RuleRenderer" enableorderby="0">
    <rules key="{edd91dfa-608a-4de3-8777-bc7eaad1caf7}">
      <rule label="Faldende" key="{4dcc9823-cbcf-4a33-9fa5-270dcf40d2f3}" symbol="0" filter=" &quot;Tendens&quot; = 'Faldende'"/>
      <rule label="Stabil" key="{cd77a3f2-ce96-467e-9283-61d2fc82c2e5}" symbol="1" filter=" &quot;Tendens&quot; = 'Stabil'"/>
      <rule label="Varierende" key="{cd77a3f2-ce96-467e-9283-61d2fc82c2e5}" symbol="2" filter=" &quot;Tendens&quot; = 'Varierende'"/>
      <rule label="Stigende" key="{87ab2b4e-86bd-4875-8889-09b19205739e}" symbol="3" filter=" &quot;Tendens&quot; = 'Stigende'"/>
    </rules>
    <symbols>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" name="0" type="marker" is_animated="0" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="255,255,255,0"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="circle"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="0,125,242,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.6"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="5"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="255,255,255,0" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0,125,242,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.6" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="5" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" name="1" type="marker" is_animated="0" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="255,255,255,0"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="circle"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="0,128,0,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.6"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="5"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="255,255,255,0" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0,128,0,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.6" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="5" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" name="2" type="marker" is_animated="0" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="255,255,255,0"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="circle"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="255,222,0,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.6"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="5"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="255,255,255,0" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="255,222,0,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.6" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="5" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" name="3" type="marker" is_animated="0" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="255,255,255,0"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="circle"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="255,0,0,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.6"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="5"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="255,255,255,0" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="255,0,0,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.6" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="5" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option name="dualview/previewExpressions" type="List">
        <Option type="QString" value="&quot;Frasorter stoffer&quot;"/>
      </Option>
      <Option name="embeddedWidgets/count" type="int" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory opacity="1" labelPlacementMethod="XHeight" barWidth="5" showAxis="1" penAlpha="255" scaleDependency="Area" backgroundColor="#ffffff" scaleBasedVisibility="0" sizeScale="3x:0,0,0,0,0,0" diagramOrientation="Up" penColor="#000000" maxScaleDenominator="1e+08" penWidth="0" minimumSize="0" width="15" lineSizeType="MM" lineSizeScale="3x:0,0,0,0,0,0" rotationOffset="270" spacingUnitScale="3x:0,0,0,0,0,0" height="15" enabled="0" sizeType="MM" minScaleDenominator="0" spacingUnit="MM" spacing="5" direction="0" backgroundAlpha="255">
      <fontProperties strikethrough="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" bold="0" italic="0" style=""/>
      <attribute field="" label="" color="#000000" colorOpacity="1"/>
      <axisSymbol>
        <symbol alpha="1" clip_to_extent="1" force_rhr="0" name="" type="line" is_animated="0" frame_rate="10">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" pass="0" enabled="1" locked="0">
            <Option type="Map">
              <Option name="align_dash_pattern" type="QString" value="0"/>
              <Option name="capstyle" type="QString" value="square"/>
              <Option name="customdash" type="QString" value="5;2"/>
              <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="customdash_unit" type="QString" value="MM"/>
              <Option name="dash_pattern_offset" type="QString" value="0"/>
              <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
              <Option name="draw_inside_polygon" type="QString" value="0"/>
              <Option name="joinstyle" type="QString" value="bevel"/>
              <Option name="line_color" type="QString" value="35,35,35,255"/>
              <Option name="line_style" type="QString" value="solid"/>
              <Option name="line_width" type="QString" value="0.26"/>
              <Option name="line_width_unit" type="QString" value="MM"/>
              <Option name="offset" type="QString" value="0"/>
              <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="offset_unit" type="QString" value="MM"/>
              <Option name="ring_filter" type="QString" value="0"/>
              <Option name="trim_distance_end" type="QString" value="0"/>
              <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_end_unit" type="QString" value="MM"/>
              <Option name="trim_distance_start" type="QString" value="0"/>
              <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_start_unit" type="QString" value="MM"/>
              <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
              <Option name="use_custom_dash" type="QString" value="0"/>
              <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            </Option>
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="trim_distance_end"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_end_map_unit_scale"/>
            <prop v="MM" k="trim_distance_end_unit"/>
            <prop v="0" k="trim_distance_start"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_start_map_unit_scale"/>
            <prop v="MM" k="trim_distance_start_unit"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" obstacle="0" linePlacementFlags="18" showAll="1" placement="0" zIndex="0" dist="0">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="fid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="Frasorter stoffer">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="frasorterede_stoffer">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="previous_user">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sampleid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="dgu_indtag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="boreholeno">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="stringno">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="intakeno">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sampledate">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="dato">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="dato_vaerdi">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="aarstal">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="unik">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="project">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="use">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="purpose">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="magasin">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="dybde_filtertop">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="dybde_filterbund">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="kote_filtertop">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="kote_filterbund">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="xutm32euref89">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="yutm32euref89">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sumanoinscalculated">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sumcationscalculated">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ionbalance">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="forvitgrad">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ionexchange">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="alkalinitet_total_ta">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="aluminium">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ammoniak_ammonium">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ammonium_n">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="anioner_total">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="arsen">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="barium">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="bly">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="bor">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="bromid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="calcium">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="carbon_organisk_nvoc">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="carbonat">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="carbondioxid_aggr">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="chlorid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="dihydrogensulfid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="fluorid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="hydrogencarbonat">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="inddampningsrest">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="jern">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="kalium">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="kationer_total">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="kobber">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="kobolt_co">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="konduktivitet">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="magnesium">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mangan">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="methan">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="natrium">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="natriumhydrogencarbonat">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nikkel">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nitrat">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nitrit">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="orthophosphat">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ortho_phosphat_p">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="oxygen_indhold">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="permanganattal_kmno4">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ph">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="phosphor_total_p">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="redoxpotentiale">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="siliciumdioxid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="strontium">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sulfat">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sulfit_s">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="temperatur">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="tÃžrstof_total">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="zink">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="Vandtype">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="SulfatTendens">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="NitratTendens">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="fid" name="" index="0"/>
    <alias field="Frasorter stoffer" name="" index="1"/>
    <alias field="frasorterede_stoffer" name="" index="2"/>
    <alias field="previous_user" name="" index="3"/>
    <alias field="sampleid" name="" index="4"/>
    <alias field="dgu_indtag" name="" index="5"/>
    <alias field="boreholeno" name="" index="6"/>
    <alias field="stringno" name="" index="7"/>
    <alias field="intakeno" name="" index="8"/>
    <alias field="sampledate" name="" index="9"/>
    <alias field="dato" name="" index="10"/>
    <alias field="dato_vaerdi" name="" index="11"/>
    <alias field="aarstal" name="" index="12"/>
    <alias field="unik" name="" index="13"/>
    <alias field="project" name="" index="14"/>
    <alias field="use" name="" index="15"/>
    <alias field="purpose" name="" index="16"/>
    <alias field="magasin" name="" index="17"/>
    <alias field="dybde_filtertop" name="" index="18"/>
    <alias field="dybde_filterbund" name="" index="19"/>
    <alias field="kote_filtertop" name="" index="20"/>
    <alias field="kote_filterbund" name="" index="21"/>
    <alias field="xutm32euref89" name="" index="22"/>
    <alias field="yutm32euref89" name="" index="23"/>
    <alias field="sumanoinscalculated" name="" index="24"/>
    <alias field="sumcationscalculated" name="" index="25"/>
    <alias field="ionbalance" name="" index="26"/>
    <alias field="forvitgrad" name="" index="27"/>
    <alias field="ionexchange" name="" index="28"/>
    <alias field="alkalinitet_total_ta" name="" index="29"/>
    <alias field="aluminium" name="" index="30"/>
    <alias field="ammoniak_ammonium" name="" index="31"/>
    <alias field="ammonium_n" name="" index="32"/>
    <alias field="anioner_total" name="" index="33"/>
    <alias field="arsen" name="" index="34"/>
    <alias field="barium" name="" index="35"/>
    <alias field="bly" name="" index="36"/>
    <alias field="bor" name="" index="37"/>
    <alias field="bromid" name="" index="38"/>
    <alias field="calcium" name="" index="39"/>
    <alias field="carbon_organisk_nvoc" name="" index="40"/>
    <alias field="carbonat" name="" index="41"/>
    <alias field="carbondioxid_aggr" name="" index="42"/>
    <alias field="chlorid" name="" index="43"/>
    <alias field="dihydrogensulfid" name="" index="44"/>
    <alias field="fluorid" name="" index="45"/>
    <alias field="hydrogencarbonat" name="" index="46"/>
    <alias field="inddampningsrest" name="" index="47"/>
    <alias field="jern" name="" index="48"/>
    <alias field="kalium" name="" index="49"/>
    <alias field="kationer_total" name="" index="50"/>
    <alias field="kobber" name="" index="51"/>
    <alias field="kobolt_co" name="" index="52"/>
    <alias field="konduktivitet" name="" index="53"/>
    <alias field="magnesium" name="" index="54"/>
    <alias field="mangan" name="" index="55"/>
    <alias field="methan" name="" index="56"/>
    <alias field="natrium" name="" index="57"/>
    <alias field="natriumhydrogencarbonat" name="" index="58"/>
    <alias field="nikkel" name="" index="59"/>
    <alias field="nitrat" name="" index="60"/>
    <alias field="nitrit" name="" index="61"/>
    <alias field="orthophosphat" name="" index="62"/>
    <alias field="ortho_phosphat_p" name="" index="63"/>
    <alias field="oxygen_indhold" name="" index="64"/>
    <alias field="permanganattal_kmno4" name="" index="65"/>
    <alias field="ph" name="" index="66"/>
    <alias field="phosphor_total_p" name="" index="67"/>
    <alias field="redoxpotentiale" name="" index="68"/>
    <alias field="siliciumdioxid" name="" index="69"/>
    <alias field="strontium" name="" index="70"/>
    <alias field="sulfat" name="" index="71"/>
    <alias field="sulfit_s" name="" index="72"/>
    <alias field="temperatur" name="" index="73"/>
    <alias field="tÃžrstof_total" name="" index="74"/>
    <alias field="zink" name="" index="75"/>
    <alias field="Vandtype" name="" index="76"/>
    <alias field="SulfatTendens" name="" index="77"/>
    <alias field="NitratTendens" name="" index="78"/>
  </aliases>
  <defaults>
    <default field="fid" expression="" applyOnUpdate="0"/>
    <default field="Frasorter stoffer" expression="" applyOnUpdate="0"/>
    <default field="frasorterede_stoffer" expression="" applyOnUpdate="0"/>
    <default field="previous_user" expression="" applyOnUpdate="0"/>
    <default field="sampleid" expression="" applyOnUpdate="0"/>
    <default field="dgu_indtag" expression="" applyOnUpdate="0"/>
    <default field="boreholeno" expression="" applyOnUpdate="0"/>
    <default field="stringno" expression="" applyOnUpdate="0"/>
    <default field="intakeno" expression="" applyOnUpdate="0"/>
    <default field="sampledate" expression="" applyOnUpdate="0"/>
    <default field="dato" expression="" applyOnUpdate="0"/>
    <default field="dato_vaerdi" expression="" applyOnUpdate="0"/>
    <default field="aarstal" expression="" applyOnUpdate="0"/>
    <default field="unik" expression="" applyOnUpdate="0"/>
    <default field="project" expression="" applyOnUpdate="0"/>
    <default field="use" expression="" applyOnUpdate="0"/>
    <default field="purpose" expression="" applyOnUpdate="0"/>
    <default field="magasin" expression="" applyOnUpdate="0"/>
    <default field="dybde_filtertop" expression="" applyOnUpdate="0"/>
    <default field="dybde_filterbund" expression="" applyOnUpdate="0"/>
    <default field="kote_filtertop" expression="" applyOnUpdate="0"/>
    <default field="kote_filterbund" expression="" applyOnUpdate="0"/>
    <default field="xutm32euref89" expression="" applyOnUpdate="0"/>
    <default field="yutm32euref89" expression="" applyOnUpdate="0"/>
    <default field="sumanoinscalculated" expression="" applyOnUpdate="0"/>
    <default field="sumcationscalculated" expression="" applyOnUpdate="0"/>
    <default field="ionbalance" expression="" applyOnUpdate="0"/>
    <default field="forvitgrad" expression="" applyOnUpdate="0"/>
    <default field="ionexchange" expression="" applyOnUpdate="0"/>
    <default field="alkalinitet_total_ta" expression="" applyOnUpdate="0"/>
    <default field="aluminium" expression="" applyOnUpdate="0"/>
    <default field="ammoniak_ammonium" expression="" applyOnUpdate="0"/>
    <default field="ammonium_n" expression="" applyOnUpdate="0"/>
    <default field="anioner_total" expression="" applyOnUpdate="0"/>
    <default field="arsen" expression="" applyOnUpdate="0"/>
    <default field="barium" expression="" applyOnUpdate="0"/>
    <default field="bly" expression="" applyOnUpdate="0"/>
    <default field="bor" expression="" applyOnUpdate="0"/>
    <default field="bromid" expression="" applyOnUpdate="0"/>
    <default field="calcium" expression="" applyOnUpdate="0"/>
    <default field="carbon_organisk_nvoc" expression="" applyOnUpdate="0"/>
    <default field="carbonat" expression="" applyOnUpdate="0"/>
    <default field="carbondioxid_aggr" expression="" applyOnUpdate="0"/>
    <default field="chlorid" expression="" applyOnUpdate="0"/>
    <default field="dihydrogensulfid" expression="" applyOnUpdate="0"/>
    <default field="fluorid" expression="" applyOnUpdate="0"/>
    <default field="hydrogencarbonat" expression="" applyOnUpdate="0"/>
    <default field="inddampningsrest" expression="" applyOnUpdate="0"/>
    <default field="jern" expression="" applyOnUpdate="0"/>
    <default field="kalium" expression="" applyOnUpdate="0"/>
    <default field="kationer_total" expression="" applyOnUpdate="0"/>
    <default field="kobber" expression="" applyOnUpdate="0"/>
    <default field="kobolt_co" expression="" applyOnUpdate="0"/>
    <default field="konduktivitet" expression="" applyOnUpdate="0"/>
    <default field="magnesium" expression="" applyOnUpdate="0"/>
    <default field="mangan" expression="" applyOnUpdate="0"/>
    <default field="methan" expression="" applyOnUpdate="0"/>
    <default field="natrium" expression="" applyOnUpdate="0"/>
    <default field="natriumhydrogencarbonat" expression="" applyOnUpdate="0"/>
    <default field="nikkel" expression="" applyOnUpdate="0"/>
    <default field="nitrat" expression="" applyOnUpdate="0"/>
    <default field="nitrit" expression="" applyOnUpdate="0"/>
    <default field="orthophosphat" expression="" applyOnUpdate="0"/>
    <default field="ortho_phosphat_p" expression="" applyOnUpdate="0"/>
    <default field="oxygen_indhold" expression="" applyOnUpdate="0"/>
    <default field="permanganattal_kmno4" expression="" applyOnUpdate="0"/>
    <default field="ph" expression="" applyOnUpdate="0"/>
    <default field="phosphor_total_p" expression="" applyOnUpdate="0"/>
    <default field="redoxpotentiale" expression="" applyOnUpdate="0"/>
    <default field="siliciumdioxid" expression="" applyOnUpdate="0"/>
    <default field="strontium" expression="" applyOnUpdate="0"/>
    <default field="sulfat" expression="" applyOnUpdate="0"/>
    <default field="sulfit_s" expression="" applyOnUpdate="0"/>
    <default field="temperatur" expression="" applyOnUpdate="0"/>
    <default field="tÃžrstof_total" expression="" applyOnUpdate="0"/>
    <default field="zink" expression="" applyOnUpdate="0"/>
    <default field="Vandtype" expression="" applyOnUpdate="0"/>
    <default field="SulfatTendens" expression="" applyOnUpdate="0"/>
    <default field="NitratTendens" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint field="fid" notnull_strength="1" constraints="3" exp_strength="0" unique_strength="1"/>
    <constraint field="Frasorter stoffer" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="frasorterede_stoffer" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="previous_user" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="sampleid" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="dgu_indtag" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="boreholeno" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="stringno" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="intakeno" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="sampledate" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="dato" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="dato_vaerdi" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="aarstal" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="unik" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="project" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="use" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="purpose" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="magasin" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="dybde_filtertop" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="dybde_filterbund" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="kote_filtertop" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="kote_filterbund" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="xutm32euref89" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="yutm32euref89" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="sumanoinscalculated" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="sumcationscalculated" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="ionbalance" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="forvitgrad" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="ionexchange" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="alkalinitet_total_ta" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="aluminium" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="ammoniak_ammonium" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="ammonium_n" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="anioner_total" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="arsen" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="barium" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="bly" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="bor" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="bromid" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="calcium" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="carbon_organisk_nvoc" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="carbonat" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="carbondioxid_aggr" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="chlorid" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="dihydrogensulfid" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="fluorid" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="hydrogencarbonat" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="inddampningsrest" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="jern" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="kalium" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="kationer_total" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="kobber" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="kobolt_co" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="konduktivitet" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="magnesium" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="mangan" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="methan" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="natrium" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="natriumhydrogencarbonat" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="nikkel" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="nitrat" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="nitrit" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="orthophosphat" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="ortho_phosphat_p" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="oxygen_indhold" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="permanganattal_kmno4" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="ph" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="phosphor_total_p" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="redoxpotentiale" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="siliciumdioxid" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="strontium" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="sulfat" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="sulfit_s" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="temperatur" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="tÃžrstof_total" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="zink" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="Vandtype" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="SulfatTendens" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="NitratTendens" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="fid" exp="" desc=""/>
    <constraint field="Frasorter stoffer" exp="" desc=""/>
    <constraint field="frasorterede_stoffer" exp="" desc=""/>
    <constraint field="previous_user" exp="" desc=""/>
    <constraint field="sampleid" exp="" desc=""/>
    <constraint field="dgu_indtag" exp="" desc=""/>
    <constraint field="boreholeno" exp="" desc=""/>
    <constraint field="stringno" exp="" desc=""/>
    <constraint field="intakeno" exp="" desc=""/>
    <constraint field="sampledate" exp="" desc=""/>
    <constraint field="dato" exp="" desc=""/>
    <constraint field="dato_vaerdi" exp="" desc=""/>
    <constraint field="aarstal" exp="" desc=""/>
    <constraint field="unik" exp="" desc=""/>
    <constraint field="project" exp="" desc=""/>
    <constraint field="use" exp="" desc=""/>
    <constraint field="purpose" exp="" desc=""/>
    <constraint field="magasin" exp="" desc=""/>
    <constraint field="dybde_filtertop" exp="" desc=""/>
    <constraint field="dybde_filterbund" exp="" desc=""/>
    <constraint field="kote_filtertop" exp="" desc=""/>
    <constraint field="kote_filterbund" exp="" desc=""/>
    <constraint field="xutm32euref89" exp="" desc=""/>
    <constraint field="yutm32euref89" exp="" desc=""/>
    <constraint field="sumanoinscalculated" exp="" desc=""/>
    <constraint field="sumcationscalculated" exp="" desc=""/>
    <constraint field="ionbalance" exp="" desc=""/>
    <constraint field="forvitgrad" exp="" desc=""/>
    <constraint field="ionexchange" exp="" desc=""/>
    <constraint field="alkalinitet_total_ta" exp="" desc=""/>
    <constraint field="aluminium" exp="" desc=""/>
    <constraint field="ammoniak_ammonium" exp="" desc=""/>
    <constraint field="ammonium_n" exp="" desc=""/>
    <constraint field="anioner_total" exp="" desc=""/>
    <constraint field="arsen" exp="" desc=""/>
    <constraint field="barium" exp="" desc=""/>
    <constraint field="bly" exp="" desc=""/>
    <constraint field="bor" exp="" desc=""/>
    <constraint field="bromid" exp="" desc=""/>
    <constraint field="calcium" exp="" desc=""/>
    <constraint field="carbon_organisk_nvoc" exp="" desc=""/>
    <constraint field="carbonat" exp="" desc=""/>
    <constraint field="carbondioxid_aggr" exp="" desc=""/>
    <constraint field="chlorid" exp="" desc=""/>
    <constraint field="dihydrogensulfid" exp="" desc=""/>
    <constraint field="fluorid" exp="" desc=""/>
    <constraint field="hydrogencarbonat" exp="" desc=""/>
    <constraint field="inddampningsrest" exp="" desc=""/>
    <constraint field="jern" exp="" desc=""/>
    <constraint field="kalium" exp="" desc=""/>
    <constraint field="kationer_total" exp="" desc=""/>
    <constraint field="kobber" exp="" desc=""/>
    <constraint field="kobolt_co" exp="" desc=""/>
    <constraint field="konduktivitet" exp="" desc=""/>
    <constraint field="magnesium" exp="" desc=""/>
    <constraint field="mangan" exp="" desc=""/>
    <constraint field="methan" exp="" desc=""/>
    <constraint field="natrium" exp="" desc=""/>
    <constraint field="natriumhydrogencarbonat" exp="" desc=""/>
    <constraint field="nikkel" exp="" desc=""/>
    <constraint field="nitrat" exp="" desc=""/>
    <constraint field="nitrit" exp="" desc=""/>
    <constraint field="orthophosphat" exp="" desc=""/>
    <constraint field="ortho_phosphat_p" exp="" desc=""/>
    <constraint field="oxygen_indhold" exp="" desc=""/>
    <constraint field="permanganattal_kmno4" exp="" desc=""/>
    <constraint field="ph" exp="" desc=""/>
    <constraint field="phosphor_total_p" exp="" desc=""/>
    <constraint field="redoxpotentiale" exp="" desc=""/>
    <constraint field="siliciumdioxid" exp="" desc=""/>
    <constraint field="strontium" exp="" desc=""/>
    <constraint field="sulfat" exp="" desc=""/>
    <constraint field="sulfit_s" exp="" desc=""/>
    <constraint field="temperatur" exp="" desc=""/>
    <constraint field="tÃžrstof_total" exp="" desc=""/>
    <constraint field="zink" exp="" desc=""/>
    <constraint field="Vandtype" exp="" desc=""/>
    <constraint field="SulfatTendens" exp="" desc=""/>
    <constraint field="NitratTendens" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column name="fid" type="field" hidden="0" width="-1"/>
      <column name="Frasorter stoffer" type="field" hidden="0" width="-1"/>
      <column name="frasorterede_stoffer" type="field" hidden="0" width="-1"/>
      <column name="previous_user" type="field" hidden="0" width="-1"/>
      <column name="sampleid" type="field" hidden="0" width="-1"/>
      <column name="dgu_indtag" type="field" hidden="0" width="-1"/>
      <column name="boreholeno" type="field" hidden="0" width="-1"/>
      <column name="stringno" type="field" hidden="0" width="-1"/>
      <column name="intakeno" type="field" hidden="0" width="-1"/>
      <column name="sampledate" type="field" hidden="0" width="-1"/>
      <column name="dato" type="field" hidden="0" width="-1"/>
      <column name="dato_vaerdi" type="field" hidden="0" width="-1"/>
      <column name="aarstal" type="field" hidden="0" width="-1"/>
      <column name="unik" type="field" hidden="0" width="-1"/>
      <column name="project" type="field" hidden="0" width="-1"/>
      <column name="use" type="field" hidden="0" width="-1"/>
      <column name="purpose" type="field" hidden="0" width="-1"/>
      <column name="magasin" type="field" hidden="0" width="-1"/>
      <column name="dybde_filtertop" type="field" hidden="0" width="-1"/>
      <column name="dybde_filterbund" type="field" hidden="0" width="-1"/>
      <column name="kote_filtertop" type="field" hidden="0" width="-1"/>
      <column name="kote_filterbund" type="field" hidden="0" width="-1"/>
      <column name="xutm32euref89" type="field" hidden="0" width="-1"/>
      <column name="yutm32euref89" type="field" hidden="0" width="-1"/>
      <column name="sumanoinscalculated" type="field" hidden="0" width="-1"/>
      <column name="sumcationscalculated" type="field" hidden="0" width="-1"/>
      <column name="ionbalance" type="field" hidden="0" width="-1"/>
      <column name="forvitgrad" type="field" hidden="0" width="-1"/>
      <column name="ionexchange" type="field" hidden="0" width="-1"/>
      <column name="alkalinitet_total_ta" type="field" hidden="0" width="-1"/>
      <column name="aluminium" type="field" hidden="0" width="-1"/>
      <column name="ammoniak_ammonium" type="field" hidden="0" width="-1"/>
      <column name="ammonium_n" type="field" hidden="0" width="-1"/>
      <column name="anioner_total" type="field" hidden="0" width="-1"/>
      <column name="arsen" type="field" hidden="0" width="-1"/>
      <column name="barium" type="field" hidden="0" width="-1"/>
      <column name="bly" type="field" hidden="0" width="-1"/>
      <column name="bor" type="field" hidden="0" width="-1"/>
      <column name="bromid" type="field" hidden="0" width="-1"/>
      <column name="calcium" type="field" hidden="0" width="-1"/>
      <column name="carbon_organisk_nvoc" type="field" hidden="0" width="-1"/>
      <column name="carbonat" type="field" hidden="0" width="-1"/>
      <column name="carbondioxid_aggr" type="field" hidden="0" width="-1"/>
      <column name="chlorid" type="field" hidden="0" width="-1"/>
      <column name="dihydrogensulfid" type="field" hidden="0" width="-1"/>
      <column name="fluorid" type="field" hidden="0" width="-1"/>
      <column name="hydrogencarbonat" type="field" hidden="0" width="-1"/>
      <column name="inddampningsrest" type="field" hidden="0" width="-1"/>
      <column name="jern" type="field" hidden="0" width="-1"/>
      <column name="kalium" type="field" hidden="0" width="-1"/>
      <column name="kationer_total" type="field" hidden="0" width="-1"/>
      <column name="kobber" type="field" hidden="0" width="-1"/>
      <column name="kobolt_co" type="field" hidden="0" width="-1"/>
      <column name="konduktivitet" type="field" hidden="0" width="-1"/>
      <column name="magnesium" type="field" hidden="0" width="-1"/>
      <column name="mangan" type="field" hidden="0" width="-1"/>
      <column name="methan" type="field" hidden="0" width="-1"/>
      <column name="natrium" type="field" hidden="0" width="-1"/>
      <column name="natriumhydrogencarbonat" type="field" hidden="0" width="-1"/>
      <column name="nikkel" type="field" hidden="0" width="-1"/>
      <column name="nitrat" type="field" hidden="0" width="-1"/>
      <column name="nitrit" type="field" hidden="0" width="-1"/>
      <column name="orthophosphat" type="field" hidden="0" width="-1"/>
      <column name="ortho_phosphat_p" type="field" hidden="0" width="-1"/>
      <column name="oxygen_indhold" type="field" hidden="0" width="-1"/>
      <column name="permanganattal_kmno4" type="field" hidden="0" width="-1"/>
      <column name="ph" type="field" hidden="0" width="-1"/>
      <column name="phosphor_total_p" type="field" hidden="0" width="-1"/>
      <column name="redoxpotentiale" type="field" hidden="0" width="-1"/>
      <column name="siliciumdioxid" type="field" hidden="0" width="-1"/>
      <column name="strontium" type="field" hidden="0" width="-1"/>
      <column name="sulfat" type="field" hidden="0" width="-1"/>
      <column name="sulfit_s" type="field" hidden="0" width="-1"/>
      <column name="temperatur" type="field" hidden="0" width="-1"/>
      <column name="tÃžrstof_total" type="field" hidden="0" width="-1"/>
      <column name="zink" type="field" hidden="0" width="-1"/>
      <column name="Vandtype" type="field" hidden="0" width="-1"/>
      <column name="SulfatTendens" type="field" hidden="0" width="-1"/>
      <column name="NitratTendens" type="field" hidden="0" width="-1"/>
      <column type="actions" hidden="1" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="Frasorter stoffer" editable="1"/>
    <field name="NitratTendens" editable="1"/>
    <field name="SulfatTendens" editable="1"/>
    <field name="Tendens" editable="1"/>
    <field name="Vandtype" editable="1"/>
    <field name="aarstal" editable="1"/>
    <field name="alkalinitet_total_ta" editable="1"/>
    <field name="aluminium" editable="1"/>
    <field name="ammoniak_ammonium" editable="1"/>
    <field name="ammonium_n" editable="1"/>
    <field name="anioner_total" editable="1"/>
    <field name="arsen" editable="1"/>
    <field name="barium" editable="1"/>
    <field name="bly" editable="1"/>
    <field name="bor" editable="1"/>
    <field name="boreholeno" editable="1"/>
    <field name="bromid" editable="1"/>
    <field name="calcium" editable="1"/>
    <field name="carbon_organisk_nvoc" editable="1"/>
    <field name="carbonat" editable="1"/>
    <field name="carbondioxid_aggr" editable="1"/>
    <field name="chlorid" editable="1"/>
    <field name="dato" editable="1"/>
    <field name="dato_vaerdi" editable="1"/>
    <field name="dgu_indtag" editable="1"/>
    <field name="dihydrogensulfid" editable="1"/>
    <field name="dybde_filterbund" editable="1"/>
    <field name="dybde_filtertop" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="fluorid" editable="1"/>
    <field name="forvitgrad" editable="1"/>
    <field name="frasorterede_stoffer" editable="1"/>
    <field name="hydrogencarbonat" editable="1"/>
    <field name="inddampningsrest" editable="1"/>
    <field name="intakeno" editable="1"/>
    <field name="ionbalance" editable="1"/>
    <field name="ionexchange" editable="1"/>
    <field name="jern" editable="1"/>
    <field name="kalium" editable="1"/>
    <field name="kationer_total" editable="1"/>
    <field name="kobber" editable="1"/>
    <field name="kobolt_co" editable="1"/>
    <field name="konduktivitet" editable="1"/>
    <field name="kote_filterbund" editable="1"/>
    <field name="kote_filtertop" editable="1"/>
    <field name="magasin" editable="1"/>
    <field name="magnesium" editable="1"/>
    <field name="mangan" editable="1"/>
    <field name="methan" editable="1"/>
    <field name="natrium" editable="1"/>
    <field name="natriumhydrogencarbonat" editable="1"/>
    <field name="nikkel" editable="1"/>
    <field name="nitrat" editable="1"/>
    <field name="nitrit" editable="1"/>
    <field name="ortho_phosphat_p" editable="1"/>
    <field name="orthophosphat" editable="1"/>
    <field name="oxygen_indhold" editable="1"/>
    <field name="permanganattal_kmno4" editable="1"/>
    <field name="ph" editable="1"/>
    <field name="phosphor_total_p" editable="1"/>
    <field name="previous_user" editable="1"/>
    <field name="project" editable="1"/>
    <field name="purpose" editable="1"/>
    <field name="redoxpotentiale" editable="1"/>
    <field name="sampledate" editable="1"/>
    <field name="sampleid" editable="1"/>
    <field name="siliciumdioxid" editable="1"/>
    <field name="stringno" editable="1"/>
    <field name="strontium" editable="1"/>
    <field name="sulfat" editable="1"/>
    <field name="sulfit_s" editable="1"/>
    <field name="sumanoinscalculated" editable="1"/>
    <field name="sumcationscalculated" editable="1"/>
    <field name="temperatur" editable="1"/>
    <field name="tÃžrstof_total" editable="1"/>
    <field name="unik" editable="1"/>
    <field name="use" editable="1"/>
    <field name="xutm32euref89" editable="1"/>
    <field name="yutm32euref89" editable="1"/>
    <field name="zink" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="Frasorter stoffer"/>
    <field labelOnTop="0" name="NitratTendens"/>
    <field labelOnTop="0" name="SulfatTendens"/>
    <field labelOnTop="0" name="Tendens"/>
    <field labelOnTop="0" name="Vandtype"/>
    <field labelOnTop="0" name="aarstal"/>
    <field labelOnTop="0" name="alkalinitet_total_ta"/>
    <field labelOnTop="0" name="aluminium"/>
    <field labelOnTop="0" name="ammoniak_ammonium"/>
    <field labelOnTop="0" name="ammonium_n"/>
    <field labelOnTop="0" name="anioner_total"/>
    <field labelOnTop="0" name="arsen"/>
    <field labelOnTop="0" name="barium"/>
    <field labelOnTop="0" name="bly"/>
    <field labelOnTop="0" name="bor"/>
    <field labelOnTop="0" name="boreholeno"/>
    <field labelOnTop="0" name="bromid"/>
    <field labelOnTop="0" name="calcium"/>
    <field labelOnTop="0" name="carbon_organisk_nvoc"/>
    <field labelOnTop="0" name="carbonat"/>
    <field labelOnTop="0" name="carbondioxid_aggr"/>
    <field labelOnTop="0" name="chlorid"/>
    <field labelOnTop="0" name="dato"/>
    <field labelOnTop="0" name="dato_vaerdi"/>
    <field labelOnTop="0" name="dgu_indtag"/>
    <field labelOnTop="0" name="dihydrogensulfid"/>
    <field labelOnTop="0" name="dybde_filterbund"/>
    <field labelOnTop="0" name="dybde_filtertop"/>
    <field labelOnTop="0" name="fid"/>
    <field labelOnTop="0" name="fluorid"/>
    <field labelOnTop="0" name="forvitgrad"/>
    <field labelOnTop="0" name="frasorterede_stoffer"/>
    <field labelOnTop="0" name="hydrogencarbonat"/>
    <field labelOnTop="0" name="inddampningsrest"/>
    <field labelOnTop="0" name="intakeno"/>
    <field labelOnTop="0" name="ionbalance"/>
    <field labelOnTop="0" name="ionexchange"/>
    <field labelOnTop="0" name="jern"/>
    <field labelOnTop="0" name="kalium"/>
    <field labelOnTop="0" name="kationer_total"/>
    <field labelOnTop="0" name="kobber"/>
    <field labelOnTop="0" name="kobolt_co"/>
    <field labelOnTop="0" name="konduktivitet"/>
    <field labelOnTop="0" name="kote_filterbund"/>
    <field labelOnTop="0" name="kote_filtertop"/>
    <field labelOnTop="0" name="magasin"/>
    <field labelOnTop="0" name="magnesium"/>
    <field labelOnTop="0" name="mangan"/>
    <field labelOnTop="0" name="methan"/>
    <field labelOnTop="0" name="natrium"/>
    <field labelOnTop="0" name="natriumhydrogencarbonat"/>
    <field labelOnTop="0" name="nikkel"/>
    <field labelOnTop="0" name="nitrat"/>
    <field labelOnTop="0" name="nitrit"/>
    <field labelOnTop="0" name="ortho_phosphat_p"/>
    <field labelOnTop="0" name="orthophosphat"/>
    <field labelOnTop="0" name="oxygen_indhold"/>
    <field labelOnTop="0" name="permanganattal_kmno4"/>
    <field labelOnTop="0" name="ph"/>
    <field labelOnTop="0" name="phosphor_total_p"/>
    <field labelOnTop="0" name="previous_user"/>
    <field labelOnTop="0" name="project"/>
    <field labelOnTop="0" name="purpose"/>
    <field labelOnTop="0" name="redoxpotentiale"/>
    <field labelOnTop="0" name="sampledate"/>
    <field labelOnTop="0" name="sampleid"/>
    <field labelOnTop="0" name="siliciumdioxid"/>
    <field labelOnTop="0" name="stringno"/>
    <field labelOnTop="0" name="strontium"/>
    <field labelOnTop="0" name="sulfat"/>
    <field labelOnTop="0" name="sulfit_s"/>
    <field labelOnTop="0" name="sumanoinscalculated"/>
    <field labelOnTop="0" name="sumcationscalculated"/>
    <field labelOnTop="0" name="temperatur"/>
    <field labelOnTop="0" name="tÃžrstof_total"/>
    <field labelOnTop="0" name="unik"/>
    <field labelOnTop="0" name="use"/>
    <field labelOnTop="0" name="xutm32euref89"/>
    <field labelOnTop="0" name="yutm32euref89"/>
    <field labelOnTop="0" name="zink"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="Frasorter stoffer" reuseLastValue="0"/>
    <field name="NitratTendens" reuseLastValue="0"/>
    <field name="SulfatTendens" reuseLastValue="0"/>
    <field name="Tendens" reuseLastValue="0"/>
    <field name="Vandtype" reuseLastValue="0"/>
    <field name="aarstal" reuseLastValue="0"/>
    <field name="alkalinitet_total_ta" reuseLastValue="0"/>
    <field name="aluminium" reuseLastValue="0"/>
    <field name="ammoniak_ammonium" reuseLastValue="0"/>
    <field name="ammonium_n" reuseLastValue="0"/>
    <field name="anioner_total" reuseLastValue="0"/>
    <field name="arsen" reuseLastValue="0"/>
    <field name="barium" reuseLastValue="0"/>
    <field name="bly" reuseLastValue="0"/>
    <field name="bor" reuseLastValue="0"/>
    <field name="boreholeno" reuseLastValue="0"/>
    <field name="bromid" reuseLastValue="0"/>
    <field name="calcium" reuseLastValue="0"/>
    <field name="carbon_organisk_nvoc" reuseLastValue="0"/>
    <field name="carbonat" reuseLastValue="0"/>
    <field name="carbondioxid_aggr" reuseLastValue="0"/>
    <field name="chlorid" reuseLastValue="0"/>
    <field name="dato" reuseLastValue="0"/>
    <field name="dato_vaerdi" reuseLastValue="0"/>
    <field name="dgu_indtag" reuseLastValue="0"/>
    <field name="dihydrogensulfid" reuseLastValue="0"/>
    <field name="dybde_filterbund" reuseLastValue="0"/>
    <field name="dybde_filtertop" reuseLastValue="0"/>
    <field name="fid" reuseLastValue="0"/>
    <field name="fluorid" reuseLastValue="0"/>
    <field name="forvitgrad" reuseLastValue="0"/>
    <field name="frasorterede_stoffer" reuseLastValue="0"/>
    <field name="hydrogencarbonat" reuseLastValue="0"/>
    <field name="inddampningsrest" reuseLastValue="0"/>
    <field name="intakeno" reuseLastValue="0"/>
    <field name="ionbalance" reuseLastValue="0"/>
    <field name="ionexchange" reuseLastValue="0"/>
    <field name="jern" reuseLastValue="0"/>
    <field name="kalium" reuseLastValue="0"/>
    <field name="kationer_total" reuseLastValue="0"/>
    <field name="kobber" reuseLastValue="0"/>
    <field name="kobolt_co" reuseLastValue="0"/>
    <field name="konduktivitet" reuseLastValue="0"/>
    <field name="kote_filterbund" reuseLastValue="0"/>
    <field name="kote_filtertop" reuseLastValue="0"/>
    <field name="magasin" reuseLastValue="0"/>
    <field name="magnesium" reuseLastValue="0"/>
    <field name="mangan" reuseLastValue="0"/>
    <field name="methan" reuseLastValue="0"/>
    <field name="natrium" reuseLastValue="0"/>
    <field name="natriumhydrogencarbonat" reuseLastValue="0"/>
    <field name="nikkel" reuseLastValue="0"/>
    <field name="nitrat" reuseLastValue="0"/>
    <field name="nitrit" reuseLastValue="0"/>
    <field name="ortho_phosphat_p" reuseLastValue="0"/>
    <field name="orthophosphat" reuseLastValue="0"/>
    <field name="oxygen_indhold" reuseLastValue="0"/>
    <field name="permanganattal_kmno4" reuseLastValue="0"/>
    <field name="ph" reuseLastValue="0"/>
    <field name="phosphor_total_p" reuseLastValue="0"/>
    <field name="previous_user" reuseLastValue="0"/>
    <field name="project" reuseLastValue="0"/>
    <field name="purpose" reuseLastValue="0"/>
    <field name="redoxpotentiale" reuseLastValue="0"/>
    <field name="sampledate" reuseLastValue="0"/>
    <field name="sampleid" reuseLastValue="0"/>
    <field name="siliciumdioxid" reuseLastValue="0"/>
    <field name="stringno" reuseLastValue="0"/>
    <field name="strontium" reuseLastValue="0"/>
    <field name="sulfat" reuseLastValue="0"/>
    <field name="sulfit_s" reuseLastValue="0"/>
    <field name="sumanoinscalculated" reuseLastValue="0"/>
    <field name="sumcationscalculated" reuseLastValue="0"/>
    <field name="temperatur" reuseLastValue="0"/>
    <field name="tÃžrstof_total" reuseLastValue="0"/>
    <field name="unik" reuseLastValue="0"/>
    <field name="use" reuseLastValue="0"/>
    <field name="xutm32euref89" reuseLastValue="0"/>
    <field name="yutm32euref89" reuseLastValue="0"/>
    <field name="zink" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"Frasorter stoffer"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
