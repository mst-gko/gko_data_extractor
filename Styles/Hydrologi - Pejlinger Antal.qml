<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" simplifyMaxScale="1" readOnly="0" maxScale="0" simplifyLocal="1" labelsEnabled="1" styleCategories="AllStyleCategories" simplifyDrawingHints="0" simplifyDrawingTol="1" version="3.26.3-Buenos Aires" minScale="100000000" symbologyReferenceScale="-1" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startExpression="" mode="0" limitMode="0" enabled="0" fixedDuration="0" accumulate="0" startField="" endField="" durationField="" endExpression="" durationUnit="min">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zscale="1" extrusion="0" binding="Centroid" clamping="Terrain" zoffset="0" type="IndividualFeatures" showMarkerSymbolInSurfacePlots="0" extrusionEnabled="0" symbology="Line" respectLayerSymbol="1">
    <data-defined-properties>
      <Option type="Map">
        <Option name="name" value="" type="QString"/>
        <Option name="properties"/>
        <Option name="type" value="collection" type="QString"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol name="" alpha="1" frame_rate="10" type="line" force_rhr="0" clip_to_extent="1" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" class="SimpleLine" locked="0">
          <Option type="Map">
            <Option name="align_dash_pattern" value="0" type="QString"/>
            <Option name="capstyle" value="square" type="QString"/>
            <Option name="customdash" value="5;2" type="QString"/>
            <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="customdash_unit" value="MM" type="QString"/>
            <Option name="dash_pattern_offset" value="0" type="QString"/>
            <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="dash_pattern_offset_unit" value="MM" type="QString"/>
            <Option name="draw_inside_polygon" value="0" type="QString"/>
            <Option name="joinstyle" value="bevel" type="QString"/>
            <Option name="line_color" value="114,155,111,255" type="QString"/>
            <Option name="line_style" value="solid" type="QString"/>
            <Option name="line_width" value="0.6" type="QString"/>
            <Option name="line_width_unit" value="MM" type="QString"/>
            <Option name="offset" value="0" type="QString"/>
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="offset_unit" value="MM" type="QString"/>
            <Option name="ring_filter" value="0" type="QString"/>
            <Option name="trim_distance_end" value="0" type="QString"/>
            <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="trim_distance_end_unit" value="MM" type="QString"/>
            <Option name="trim_distance_start" value="0" type="QString"/>
            <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="trim_distance_start_unit" value="MM" type="QString"/>
            <Option name="tweak_dash_pattern_on_corners" value="0" type="QString"/>
            <Option name="use_custom_dash" value="0" type="QString"/>
            <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
          </Option>
          <prop v="0" k="align_dash_pattern"/>
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="114,155,111,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.6" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="trim_distance_end"/>
          <prop v="3x:0,0,0,0,0,0" k="trim_distance_end_map_unit_scale"/>
          <prop v="MM" k="trim_distance_end_unit"/>
          <prop v="0" k="trim_distance_start"/>
          <prop v="3x:0,0,0,0,0,0" k="trim_distance_start_map_unit_scale"/>
          <prop v="MM" k="trim_distance_start_unit"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol name="" alpha="1" frame_rate="10" type="fill" force_rhr="0" clip_to_extent="1" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" class="SimpleFill" locked="0">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="color" value="114,155,111,255" type="QString"/>
            <Option name="joinstyle" value="bevel" type="QString"/>
            <Option name="offset" value="0,0" type="QString"/>
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="offset_unit" value="MM" type="QString"/>
            <Option name="outline_color" value="81,111,79,255" type="QString"/>
            <Option name="outline_style" value="solid" type="QString"/>
            <Option name="outline_width" value="0.2" type="QString"/>
            <Option name="outline_width_unit" value="MM" type="QString"/>
            <Option name="style" value="solid" type="QString"/>
          </Option>
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="114,155,111,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="81,111,79,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.2" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol name="" alpha="1" frame_rate="10" type="marker" force_rhr="0" clip_to_extent="1" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" value="0" type="QString"/>
            <Option name="cap_style" value="square" type="QString"/>
            <Option name="color" value="114,155,111,255" type="QString"/>
            <Option name="horizontal_anchor_point" value="1" type="QString"/>
            <Option name="joinstyle" value="bevel" type="QString"/>
            <Option name="name" value="diamond" type="QString"/>
            <Option name="offset" value="0,0" type="QString"/>
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="offset_unit" value="MM" type="QString"/>
            <Option name="outline_color" value="81,111,79,255" type="QString"/>
            <Option name="outline_style" value="solid" type="QString"/>
            <Option name="outline_width" value="0.2" type="QString"/>
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="outline_width_unit" value="MM" type="QString"/>
            <Option name="scale_method" value="diameter" type="QString"/>
            <Option name="size" value="3" type="QString"/>
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="size_unit" value="MM" type="QString"/>
            <Option name="vertical_anchor_point" value="1" type="QString"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="114,155,111,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="diamond" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="81,111,79,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.2" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="3" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 symbollevels="1" forceraster="0" enableorderby="1" type="RuleRenderer" referencescale="-1">
    <rules key="{10966ace-abce-4ca5-ad15-f2a4e293239e}">
      <rule filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;antal_pejlinger&quot; = 1" symbol="0" key="{651c50fb-6b83-477d-a7f7-d886bc74c4ca}" label="1"/>
      <rule filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;antal_pejlinger&quot; >= 2.000000 AND &quot;antal_pejlinger&quot; &lt;= 5" symbol="1" key="{3d5e1181-f220-49eb-b623-e3d79fdb7aae}" label="2-5"/>
      <rule filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;antal_pejlinger&quot; > 5.500000 AND &quot;antal_pejlinger&quot; &lt;= 10" symbol="2" key="{231cd2f2-eb38-4158-a3a5-831da6dfeb11}" label="6-10"/>
      <rule filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;antal_pejlinger&quot; >= 11 AND &quot;antal_pejlinger&quot; &lt;= 50" symbol="3" key="{f920179a-4f09-4604-80a8-c7433cb09ac7}" label="11-50"/>
      <rule filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;antal_pejlinger&quot; > 50.000000 AND &quot;antal_pejlinger&quot; &lt;= 100.000000" symbol="4" key="{112c4d80-3a80-40ba-8849-c28031234039}" label="51-100"/>
      <rule filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;antal_pejlinger&quot; > 100.000000 AND &quot;antal_pejlinger&quot; &lt;= 1000000000000.000000" symbol="5" key="{f1ef449e-75cc-476f-88e6-88de8a5afe01}" label="> 100"/>
    </rules>
    <symbols>
      <symbol name="0" alpha="1" frame_rate="10" type="marker" force_rhr="0" clip_to_extent="1" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="1" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" value="0" type="QString"/>
            <Option name="cap_style" value="square" type="QString"/>
            <Option name="color" value="0,0,0,255" type="QString"/>
            <Option name="horizontal_anchor_point" value="1" type="QString"/>
            <Option name="joinstyle" value="bevel" type="QString"/>
            <Option name="name" value="circle" type="QString"/>
            <Option name="offset" value="0,0" type="QString"/>
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="offset_unit" value="MM" type="QString"/>
            <Option name="outline_color" value="35,35,35,255" type="QString"/>
            <Option name="outline_style" value="solid" type="QString"/>
            <Option name="outline_width" value="0" type="QString"/>
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="outline_width_unit" value="MM" type="QString"/>
            <Option name="scale_method" value="diameter" type="QString"/>
            <Option name="size" value="2" type="QString"/>
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="size_unit" value="MM" type="QString"/>
            <Option name="vertical_anchor_point" value="1" type="QString"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="0,0,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="1" alpha="1" frame_rate="10" type="marker" force_rhr="0" clip_to_extent="1" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="2" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" value="0" type="QString"/>
            <Option name="cap_style" value="square" type="QString"/>
            <Option name="color" value="227,26,28,255" type="QString"/>
            <Option name="horizontal_anchor_point" value="1" type="QString"/>
            <Option name="joinstyle" value="bevel" type="QString"/>
            <Option name="name" value="circle" type="QString"/>
            <Option name="offset" value="0,0" type="QString"/>
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="offset_unit" value="MM" type="QString"/>
            <Option name="outline_color" value="35,35,35,255" type="QString"/>
            <Option name="outline_style" value="solid" type="QString"/>
            <Option name="outline_width" value="0" type="QString"/>
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="outline_width_unit" value="MM" type="QString"/>
            <Option name="scale_method" value="diameter" type="QString"/>
            <Option name="size" value="2" type="QString"/>
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="size_unit" value="MM" type="QString"/>
            <Option name="vertical_anchor_point" value="1" type="QString"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="227,26,28,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="2" alpha="1" frame_rate="10" type="marker" force_rhr="0" clip_to_extent="1" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="3" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" value="0" type="QString"/>
            <Option name="cap_style" value="square" type="QString"/>
            <Option name="color" value="255,127,0,255" type="QString"/>
            <Option name="horizontal_anchor_point" value="1" type="QString"/>
            <Option name="joinstyle" value="bevel" type="QString"/>
            <Option name="name" value="circle" type="QString"/>
            <Option name="offset" value="0,0" type="QString"/>
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="offset_unit" value="MM" type="QString"/>
            <Option name="outline_color" value="35,35,35,255" type="QString"/>
            <Option name="outline_style" value="solid" type="QString"/>
            <Option name="outline_width" value="0" type="QString"/>
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="outline_width_unit" value="MM" type="QString"/>
            <Option name="scale_method" value="diameter" type="QString"/>
            <Option name="size" value="2" type="QString"/>
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="size_unit" value="MM" type="QString"/>
            <Option name="vertical_anchor_point" value="1" type="QString"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="255,127,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="3" alpha="1" frame_rate="10" type="marker" force_rhr="0" clip_to_extent="1" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="4" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" value="0" type="QString"/>
            <Option name="cap_style" value="square" type="QString"/>
            <Option name="color" value="255,247,83,255" type="QString"/>
            <Option name="horizontal_anchor_point" value="1" type="QString"/>
            <Option name="joinstyle" value="bevel" type="QString"/>
            <Option name="name" value="circle" type="QString"/>
            <Option name="offset" value="0,0" type="QString"/>
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="offset_unit" value="MM" type="QString"/>
            <Option name="outline_color" value="35,35,35,255" type="QString"/>
            <Option name="outline_style" value="solid" type="QString"/>
            <Option name="outline_width" value="0" type="QString"/>
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="outline_width_unit" value="MM" type="QString"/>
            <Option name="scale_method" value="diameter" type="QString"/>
            <Option name="size" value="2" type="QString"/>
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="size_unit" value="MM" type="QString"/>
            <Option name="vertical_anchor_point" value="1" type="QString"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="255,247,83,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="4" alpha="1" frame_rate="10" type="marker" force_rhr="0" clip_to_extent="1" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="5" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" value="0" type="QString"/>
            <Option name="cap_style" value="square" type="QString"/>
            <Option name="color" value="51,160,44,255" type="QString"/>
            <Option name="horizontal_anchor_point" value="1" type="QString"/>
            <Option name="joinstyle" value="bevel" type="QString"/>
            <Option name="name" value="circle" type="QString"/>
            <Option name="offset" value="0,0" type="QString"/>
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="offset_unit" value="MM" type="QString"/>
            <Option name="outline_color" value="35,35,35,255" type="QString"/>
            <Option name="outline_style" value="solid" type="QString"/>
            <Option name="outline_width" value="0" type="QString"/>
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="outline_width_unit" value="MM" type="QString"/>
            <Option name="scale_method" value="diameter" type="QString"/>
            <Option name="size" value="2" type="QString"/>
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="size_unit" value="MM" type="QString"/>
            <Option name="vertical_anchor_point" value="1" type="QString"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="51,160,44,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="5" alpha="1" frame_rate="10" type="marker" force_rhr="0" clip_to_extent="1" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="6" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" value="0" type="QString"/>
            <Option name="cap_style" value="square" type="QString"/>
            <Option name="color" value="42,163,243,255" type="QString"/>
            <Option name="horizontal_anchor_point" value="1" type="QString"/>
            <Option name="joinstyle" value="bevel" type="QString"/>
            <Option name="name" value="circle" type="QString"/>
            <Option name="offset" value="0,0" type="QString"/>
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="offset_unit" value="MM" type="QString"/>
            <Option name="outline_color" value="35,35,35,255" type="QString"/>
            <Option name="outline_style" value="solid" type="QString"/>
            <Option name="outline_width" value="0" type="QString"/>
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="outline_width_unit" value="MM" type="QString"/>
            <Option name="scale_method" value="diameter" type="QString"/>
            <Option name="size" value="2" type="QString"/>
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            <Option name="size_unit" value="MM" type="QString"/>
            <Option name="vertical_anchor_point" value="1" type="QString"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="42,163,243,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <orderby>
      <orderByClause asc="1" nullsFirst="0">"antal"</orderByClause>
    </orderby>
  </renderer-v2>
  <labeling type="rule-based">
    <rules key="{17a6918e-8b50-4dd5-a80f-0a82e50492da}">
      <rule filter="&quot;antal_pejlinger&quot; > 1" active="0" key="{14bd0e29-8dbd-4c49-971e-9fff484e627d}">
        <settings calloutType="simple">
          <text-style namedStyle="normal" textOrientation="horizontal" fontSizeUnit="Point" legendString="Aa" fontStrikeout="0" allowHtml="0" fontUnderline="0" fontWeight="50" blendMode="0" fieldName="boreholeno" forcedBold="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontKerning="1" useSubstitutions="0" fontItalic="0" fontFamily="Arial" fontLetterSpacing="0" previewBkgrdColor="255,255,255,255" fontSize="10" textColor="50,50,50,255" isExpression="0" capitalization="0" fontWordSpacing="0" multilineHeight="1" forcedItalic="0" textOpacity="1">
            <families/>
            <text-buffer bufferNoFill="1" bufferSizeUnits="MM" bufferColor="250,250,250,255" bufferOpacity="1" bufferJoinStyle="128" bufferDraw="0" bufferSize="1" bufferBlendMode="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0"/>
            <text-mask maskType="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskJoinStyle="128" maskOpacity="1" maskedSymbolLayers="" maskEnabled="0" maskSizeUnits="MM" maskSize="0"/>
            <background shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetX="0" shapeType="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="0" shapeSizeType="0" shapeSVGFile="" shapeBlendMode="0" shapeRadiiX="0" shapeSizeY="0" shapeBorderColor="128,128,128,255" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeSizeX="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeRotationType="0" shapeOpacity="1" shapeRotation="0" shapeRadiiY="0" shapeSizeUnit="Point" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeJoinStyle="64" shapeBorderWidth="0" shapeRadiiUnit="Point" shapeBorderWidthUnit="Point" shapeOffsetUnit="Point">
              <symbol name="markerSymbol" alpha="1" frame_rate="10" type="marker" force_rhr="0" clip_to_extent="1" is_animated="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option name="name" value="" type="QString"/>
                    <Option name="properties"/>
                    <Option name="type" value="collection" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" pass="0" class="SimpleMarker" locked="0">
                  <Option type="Map">
                    <Option name="angle" value="0" type="QString"/>
                    <Option name="cap_style" value="square" type="QString"/>
                    <Option name="color" value="114,155,111,255" type="QString"/>
                    <Option name="horizontal_anchor_point" value="1" type="QString"/>
                    <Option name="joinstyle" value="bevel" type="QString"/>
                    <Option name="name" value="circle" type="QString"/>
                    <Option name="offset" value="0,0" type="QString"/>
                    <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
                    <Option name="offset_unit" value="MM" type="QString"/>
                    <Option name="outline_color" value="35,35,35,255" type="QString"/>
                    <Option name="outline_style" value="solid" type="QString"/>
                    <Option name="outline_width" value="0" type="QString"/>
                    <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
                    <Option name="outline_width_unit" value="MM" type="QString"/>
                    <Option name="scale_method" value="diameter" type="QString"/>
                    <Option name="size" value="2" type="QString"/>
                    <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
                    <Option name="size_unit" value="MM" type="QString"/>
                    <Option name="vertical_anchor_point" value="1" type="QString"/>
                  </Option>
                  <prop v="0" k="angle"/>
                  <prop v="square" k="cap_style"/>
                  <prop v="114,155,111,255" k="color"/>
                  <prop v="1" k="horizontal_anchor_point"/>
                  <prop v="bevel" k="joinstyle"/>
                  <prop v="circle" k="name"/>
                  <prop v="0,0" k="offset"/>
                  <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
                  <prop v="MM" k="offset_unit"/>
                  <prop v="35,35,35,255" k="outline_color"/>
                  <prop v="solid" k="outline_style"/>
                  <prop v="0" k="outline_width"/>
                  <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
                  <prop v="MM" k="outline_width_unit"/>
                  <prop v="diameter" k="scale_method"/>
                  <prop v="2" k="size"/>
                  <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
                  <prop v="MM" k="size_unit"/>
                  <prop v="1" k="vertical_anchor_point"/>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option name="name" value="" type="QString"/>
                      <Option name="properties"/>
                      <Option name="type" value="collection" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol name="fillSymbol" alpha="1" frame_rate="10" type="fill" force_rhr="0" clip_to_extent="1" is_animated="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option name="name" value="" type="QString"/>
                    <Option name="properties"/>
                    <Option name="type" value="collection" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" pass="0" class="SimpleFill" locked="0">
                  <Option type="Map">
                    <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
                    <Option name="color" value="255,255,255,255" type="QString"/>
                    <Option name="joinstyle" value="bevel" type="QString"/>
                    <Option name="offset" value="0,0" type="QString"/>
                    <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
                    <Option name="offset_unit" value="MM" type="QString"/>
                    <Option name="outline_color" value="128,128,128,255" type="QString"/>
                    <Option name="outline_style" value="no" type="QString"/>
                    <Option name="outline_width" value="0" type="QString"/>
                    <Option name="outline_width_unit" value="Point" type="QString"/>
                    <Option name="style" value="solid" type="QString"/>
                  </Option>
                  <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
                  <prop v="255,255,255,255" k="color"/>
                  <prop v="bevel" k="joinstyle"/>
                  <prop v="0,0" k="offset"/>
                  <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
                  <prop v="MM" k="offset_unit"/>
                  <prop v="128,128,128,255" k="outline_color"/>
                  <prop v="no" k="outline_style"/>
                  <prop v="0" k="outline_width"/>
                  <prop v="Point" k="outline_width_unit"/>
                  <prop v="solid" k="style"/>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option name="name" value="" type="QString"/>
                      <Option name="properties"/>
                      <Option name="type" value="collection" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="MM" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowDraw="0" shadowColor="0,0,0,255" shadowUnder="0" shadowOffsetAngle="135" shadowBlendMode="6" shadowRadiusUnit="MM" shadowOpacity="0.69999999999999996" shadowOffsetGlobal="1" shadowOffsetDist="1" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowScale="100"/>
            <dd_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString"/>
                <Option name="properties"/>
                <Option name="type" value="collection" type="QString"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format wrapChar="" multilineAlign="3" autoWrapLength="0" reverseDirectionSymbol="0" plussign="0" addDirectionSymbol="0" rightDirectionSymbol=">" placeDirectionSymbol="0" decimals="3" leftDirectionSymbol="&lt;" formatNumbers="0" useMaxLineLengthForAutoWrap="1"/>
          <placement yOffset="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" centroidInside="0" maxCurvedCharAngleIn="25" allowDegraded="0" preserveRotation="1" quadOffset="4" lineAnchorClipping="0" lineAnchorTextPoint="FollowPlacement" geometryGeneratorType="PointGeometry" placement="6" fitInPolygonOnly="0" rotationUnit="AngleDegrees" geometryGenerator="" layerType="PointGeometry" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" maxCurvedCharAngleOut="-25" distMapUnitScale="3x:0,0,0,0,0,0" priority="5" offsetType="1" overrunDistanceUnit="MM" lineAnchorType="0" dist="0" centroidWhole="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" lineAnchorPercent="0.5" offsetUnits="MM" geometryGeneratorEnabled="0" distUnits="MM" repeatDistanceUnits="MM" polygonPlacementFlags="2" placementFlags="10" repeatDistance="0" rotationAngle="0" overrunDistance="0" xOffset="0"/>
          <rendering fontLimitPixelSize="0" obstacleFactor="1" unplacedVisibility="0" limitNumLabels="0" obstacleType="1" drawLabels="1" scaleVisibility="0" labelPerPart="0" fontMinPixelSize="3" scaleMin="0" upsidedownLabels="0" maxNumLabels="2000" mergeLines="0" fontMaxPixelSize="10000" minFeatureSize="0" scaleMax="0" zIndex="0" obstacle="1"/>
          <dd_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="PositionX" type="Map">
                  <Option name="active" value="true" type="bool"/>
                  <Option name="field" value="auxiliary_storage_labeling_positionx" type="QString"/>
                  <Option name="type" value="2" type="int"/>
                </Option>
                <Option name="PositionY" type="Map">
                  <Option name="active" value="true" type="bool"/>
                  <Option name="field" value="auxiliary_storage_labeling_positiony" type="QString"/>
                  <Option name="type" value="2" type="int"/>
                </Option>
              </Option>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option name="anchorPoint" value="pole_of_inaccessibility" type="QString"/>
              <Option name="blendMode" value="0" type="int"/>
              <Option name="ddProperties" type="Map">
                <Option name="name" value="" type="QString"/>
                <Option name="properties"/>
                <Option name="type" value="collection" type="QString"/>
              </Option>
              <Option name="drawToAllParts" value="false" type="bool"/>
              <Option name="enabled" value="1" type="QString"/>
              <Option name="labelAnchorPoint" value="point_on_exterior" type="QString"/>
              <Option name="lineSymbol" value="&lt;symbol name=&quot;symbol&quot; alpha=&quot;1&quot; frame_rate=&quot;10&quot; type=&quot;line&quot; force_rhr=&quot;0&quot; clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;name&quot; value=&quot;&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option name=&quot;type&quot; value=&quot;collection&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer enabled=&quot;1&quot; pass=&quot;0&quot; class=&quot;SimpleLine&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;align_dash_pattern&quot; value=&quot;0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;capstyle&quot; value=&quot;square&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;customdash&quot; value=&quot;5;2&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;customdash_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;customdash_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;dash_pattern_offset&quot; value=&quot;0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;dash_pattern_offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;dash_pattern_offset_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;draw_inside_polygon&quot; value=&quot;0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;joinstyle&quot; value=&quot;bevel&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;line_color&quot; value=&quot;60,60,60,255&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;line_style&quot; value=&quot;solid&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;line_width&quot; value=&quot;0.3&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;line_width_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;offset&quot; value=&quot;0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;offset_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;ring_filter&quot; value=&quot;0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;trim_distance_end&quot; value=&quot;0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;trim_distance_end_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;trim_distance_end_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;trim_distance_start&quot; value=&quot;0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;trim_distance_start_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;trim_distance_start_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;tweak_dash_pattern_on_corners&quot; value=&quot;0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;use_custom_dash&quot; value=&quot;0&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;prop v=&quot;0&quot; k=&quot;align_dash_pattern&quot;/>&lt;prop v=&quot;square&quot; k=&quot;capstyle&quot;/>&lt;prop v=&quot;5;2&quot; k=&quot;customdash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;customdash_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;customdash_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;dash_pattern_offset&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;dash_pattern_offset_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;dash_pattern_offset_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;draw_inside_polygon&quot;/>&lt;prop v=&quot;bevel&quot; k=&quot;joinstyle&quot;/>&lt;prop v=&quot;60,60,60,255&quot; k=&quot;line_color&quot;/>&lt;prop v=&quot;solid&quot; k=&quot;line_style&quot;/>&lt;prop v=&quot;0.3&quot; k=&quot;line_width&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;line_width_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;offset&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;offset_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;offset_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;ring_filter&quot;/>&lt;prop v=&quot;0&quot; k=&quot;trim_distance_end&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;trim_distance_end_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;trim_distance_end_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;trim_distance_start&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;trim_distance_start_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;trim_distance_start_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;tweak_dash_pattern_on_corners&quot;/>&lt;prop v=&quot;0&quot; k=&quot;use_custom_dash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;width_map_unit_scale&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;name&quot; value=&quot;&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option name=&quot;type&quot; value=&quot;collection&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString"/>
              <Option name="minLength" value="0" type="double"/>
              <Option name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0" type="QString"/>
              <Option name="minLengthUnit" value="MM" type="QString"/>
              <Option name="offsetFromAnchor" value="0" type="double"/>
              <Option name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0" type="QString"/>
              <Option name="offsetFromAnchorUnit" value="MM" type="QString"/>
              <Option name="offsetFromLabel" value="0" type="double"/>
              <Option name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0" type="QString"/>
              <Option name="offsetFromLabelUnit" value="MM" type="QString"/>
            </Option>
          </callout>
        </settings>
      </rule>
    </rules>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option name="dualview/previewExpressions" type="List">
        <Option value="&quot;row_id&quot;" type="QString"/>
      </Option>
      <Option name="embeddedWidgets/count" value="0" type="QString"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory lineSizeScale="3x:0,0,0,0,0,0" sizeType="MM" penWidth="0" penColor="#000000" width="15" backgroundColor="#ffffff" showAxis="1" height="15" spacingUnitScale="3x:0,0,0,0,0,0" diagramOrientation="Up" penAlpha="255" minScaleDenominator="0" spacing="5" labelPlacementMethod="XHeight" minimumSize="0" opacity="1" backgroundAlpha="255" spacingUnit="MM" scaleDependency="Area" barWidth="5" rotationOffset="270" direction="0" enabled="0" sizeScale="3x:0,0,0,0,0,0" lineSizeType="MM" scaleBasedVisibility="0" maxScaleDenominator="1e+08">
      <fontProperties italic="0" bold="0" style="" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute color="#000000" colorOpacity="1" label="" field=""/>
      <axisSymbol>
        <symbol name="" alpha="1" frame_rate="10" type="line" force_rhr="0" clip_to_extent="1" is_animated="0">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
          <layer enabled="1" pass="0" class="SimpleLine" locked="0">
            <Option type="Map">
              <Option name="align_dash_pattern" value="0" type="QString"/>
              <Option name="capstyle" value="square" type="QString"/>
              <Option name="customdash" value="5;2" type="QString"/>
              <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
              <Option name="customdash_unit" value="MM" type="QString"/>
              <Option name="dash_pattern_offset" value="0" type="QString"/>
              <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
              <Option name="dash_pattern_offset_unit" value="MM" type="QString"/>
              <Option name="draw_inside_polygon" value="0" type="QString"/>
              <Option name="joinstyle" value="bevel" type="QString"/>
              <Option name="line_color" value="35,35,35,255" type="QString"/>
              <Option name="line_style" value="solid" type="QString"/>
              <Option name="line_width" value="0.26" type="QString"/>
              <Option name="line_width_unit" value="MM" type="QString"/>
              <Option name="offset" value="0" type="QString"/>
              <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
              <Option name="offset_unit" value="MM" type="QString"/>
              <Option name="ring_filter" value="0" type="QString"/>
              <Option name="trim_distance_end" value="0" type="QString"/>
              <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
              <Option name="trim_distance_end_unit" value="MM" type="QString"/>
              <Option name="trim_distance_start" value="0" type="QString"/>
              <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
              <Option name="trim_distance_start_unit" value="MM" type="QString"/>
              <Option name="tweak_dash_pattern_on_corners" value="0" type="QString"/>
              <Option name="use_custom_dash" value="0" type="QString"/>
              <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString"/>
            </Option>
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="trim_distance_end"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_end_map_unit_scale"/>
            <prop v="MM" k="trim_distance_end_unit"/>
            <prop v="0" k="trim_distance_start"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_start_map_unit_scale"/>
            <prop v="MM" k="trim_distance_start_unit"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString"/>
                <Option name="properties"/>
                <Option name="type" value="collection" type="QString"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" linePlacementFlags="18" priority="0" placement="0" zIndex="0" showAll="1" dist="0">
    <properties>
      <Option type="Map">
        <Option name="name" value="" type="QString"/>
        <Option name="properties"/>
        <Option name="type" value="collection" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="rownumber" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="antal_pejlinger" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevelid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boreholeid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="intakeid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boreholeno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevelno" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="intakeno" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="timeofmeas" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="project" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="waterlevel" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevgrsu" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevmsl" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevmp" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="hoursnopum" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="category" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="method" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="quality" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="refpoint" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="remark" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="verticaref" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="atmospresshpa" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="extremes" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="situation" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevelroundno" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="qualitycontrol" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="guid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="guid_intake" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="guid_watlevround" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="insertdate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="updatedate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="insertuser" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="updateuser" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dataowner" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locatmetho" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="screen_top" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="screen_bottom" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="rownumber"/>
    <alias name="" index="1" field="antal_pejlinger"/>
    <alias name="" index="2" field="watlevelid"/>
    <alias name="" index="3" field="boreholeid"/>
    <alias name="" index="4" field="intakeid"/>
    <alias name="" index="5" field="boreholeno"/>
    <alias name="" index="6" field="watlevelno"/>
    <alias name="" index="7" field="intakeno"/>
    <alias name="" index="8" field="timeofmeas"/>
    <alias name="" index="9" field="project"/>
    <alias name="" index="10" field="waterlevel"/>
    <alias name="" index="11" field="watlevgrsu"/>
    <alias name="" index="12" field="watlevmsl"/>
    <alias name="" index="13" field="watlevmp"/>
    <alias name="" index="14" field="hoursnopum"/>
    <alias name="" index="15" field="category"/>
    <alias name="" index="16" field="method"/>
    <alias name="" index="17" field="quality"/>
    <alias name="" index="18" field="refpoint"/>
    <alias name="" index="19" field="remark"/>
    <alias name="" index="20" field="verticaref"/>
    <alias name="" index="21" field="atmospresshpa"/>
    <alias name="" index="22" field="extremes"/>
    <alias name="" index="23" field="situation"/>
    <alias name="" index="24" field="watlevelroundno"/>
    <alias name="" index="25" field="qualitycontrol"/>
    <alias name="" index="26" field="guid"/>
    <alias name="" index="27" field="guid_intake"/>
    <alias name="" index="28" field="guid_watlevround"/>
    <alias name="" index="29" field="insertdate"/>
    <alias name="" index="30" field="updatedate"/>
    <alias name="" index="31" field="insertuser"/>
    <alias name="" index="32" field="updateuser"/>
    <alias name="" index="33" field="dataowner"/>
    <alias name="" index="34" field="locatmetho"/>
    <alias name="" index="35" field="screen_top"/>
    <alias name="" index="36" field="screen_bottom"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" expression="" field="rownumber"/>
    <default applyOnUpdate="0" expression="" field="antal_pejlinger"/>
    <default applyOnUpdate="0" expression="" field="watlevelid"/>
    <default applyOnUpdate="0" expression="" field="boreholeid"/>
    <default applyOnUpdate="0" expression="" field="intakeid"/>
    <default applyOnUpdate="0" expression="" field="boreholeno"/>
    <default applyOnUpdate="0" expression="" field="watlevelno"/>
    <default applyOnUpdate="0" expression="" field="intakeno"/>
    <default applyOnUpdate="0" expression="" field="timeofmeas"/>
    <default applyOnUpdate="0" expression="" field="project"/>
    <default applyOnUpdate="0" expression="" field="waterlevel"/>
    <default applyOnUpdate="0" expression="" field="watlevgrsu"/>
    <default applyOnUpdate="0" expression="" field="watlevmsl"/>
    <default applyOnUpdate="0" expression="" field="watlevmp"/>
    <default applyOnUpdate="0" expression="" field="hoursnopum"/>
    <default applyOnUpdate="0" expression="" field="category"/>
    <default applyOnUpdate="0" expression="" field="method"/>
    <default applyOnUpdate="0" expression="" field="quality"/>
    <default applyOnUpdate="0" expression="" field="refpoint"/>
    <default applyOnUpdate="0" expression="" field="remark"/>
    <default applyOnUpdate="0" expression="" field="verticaref"/>
    <default applyOnUpdate="0" expression="" field="atmospresshpa"/>
    <default applyOnUpdate="0" expression="" field="extremes"/>
    <default applyOnUpdate="0" expression="" field="situation"/>
    <default applyOnUpdate="0" expression="" field="watlevelroundno"/>
    <default applyOnUpdate="0" expression="" field="qualitycontrol"/>
    <default applyOnUpdate="0" expression="" field="guid"/>
    <default applyOnUpdate="0" expression="" field="guid_intake"/>
    <default applyOnUpdate="0" expression="" field="guid_watlevround"/>
    <default applyOnUpdate="0" expression="" field="insertdate"/>
    <default applyOnUpdate="0" expression="" field="updatedate"/>
    <default applyOnUpdate="0" expression="" field="insertuser"/>
    <default applyOnUpdate="0" expression="" field="updateuser"/>
    <default applyOnUpdate="0" expression="" field="dataowner"/>
    <default applyOnUpdate="0" expression="" field="locatmetho"/>
    <default applyOnUpdate="0" expression="" field="screen_top"/>
    <default applyOnUpdate="0" expression="" field="screen_bottom"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" unique_strength="1" constraints="3" notnull_strength="1" field="rownumber"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="antal_pejlinger"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="watlevelid"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="boreholeid"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="intakeid"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="boreholeno"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="watlevelno"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="intakeno"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="timeofmeas"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="project"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="waterlevel"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="watlevgrsu"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="watlevmsl"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="watlevmp"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="hoursnopum"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="category"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="method"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="quality"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="refpoint"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="remark"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="verticaref"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="atmospresshpa"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="extremes"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="situation"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="watlevelroundno"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="qualitycontrol"/>
    <constraint exp_strength="0" unique_strength="1" constraints="3" notnull_strength="1" field="guid"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="guid_intake"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="guid_watlevround"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="insertdate"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="updatedate"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="insertuser"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="updateuser"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="dataowner"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="locatmetho"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="screen_top"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="screen_bottom"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="rownumber"/>
    <constraint desc="" exp="" field="antal_pejlinger"/>
    <constraint desc="" exp="" field="watlevelid"/>
    <constraint desc="" exp="" field="boreholeid"/>
    <constraint desc="" exp="" field="intakeid"/>
    <constraint desc="" exp="" field="boreholeno"/>
    <constraint desc="" exp="" field="watlevelno"/>
    <constraint desc="" exp="" field="intakeno"/>
    <constraint desc="" exp="" field="timeofmeas"/>
    <constraint desc="" exp="" field="project"/>
    <constraint desc="" exp="" field="waterlevel"/>
    <constraint desc="" exp="" field="watlevgrsu"/>
    <constraint desc="" exp="" field="watlevmsl"/>
    <constraint desc="" exp="" field="watlevmp"/>
    <constraint desc="" exp="" field="hoursnopum"/>
    <constraint desc="" exp="" field="category"/>
    <constraint desc="" exp="" field="method"/>
    <constraint desc="" exp="" field="quality"/>
    <constraint desc="" exp="" field="refpoint"/>
    <constraint desc="" exp="" field="remark"/>
    <constraint desc="" exp="" field="verticaref"/>
    <constraint desc="" exp="" field="atmospresshpa"/>
    <constraint desc="" exp="" field="extremes"/>
    <constraint desc="" exp="" field="situation"/>
    <constraint desc="" exp="" field="watlevelroundno"/>
    <constraint desc="" exp="" field="qualitycontrol"/>
    <constraint desc="" exp="" field="guid"/>
    <constraint desc="" exp="" field="guid_intake"/>
    <constraint desc="" exp="" field="guid_watlevround"/>
    <constraint desc="" exp="" field="insertdate"/>
    <constraint desc="" exp="" field="updatedate"/>
    <constraint desc="" exp="" field="insertuser"/>
    <constraint desc="" exp="" field="updateuser"/>
    <constraint desc="" exp="" field="dataowner"/>
    <constraint desc="" exp="" field="locatmetho"/>
    <constraint desc="" exp="" field="screen_top"/>
    <constraint desc="" exp="" field="screen_bottom"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="1" sortExpression="&quot;antal_pejlinger&quot;">
    <columns>
      <column name="boreholeno" hidden="0" type="field" width="-1"/>
      <column name="intakeno" hidden="0" type="field" width="-1"/>
      <column name="rownumber" hidden="0" type="field" width="-1"/>
      <column name="antal_pejlinger" hidden="0" type="field" width="-1"/>
      <column name="watlevelid" hidden="0" type="field" width="-1"/>
      <column name="boreholeid" hidden="0" type="field" width="-1"/>
      <column name="intakeid" hidden="0" type="field" width="-1"/>
      <column name="watlevelno" hidden="0" type="field" width="-1"/>
      <column name="timeofmeas" hidden="0" type="field" width="-1"/>
      <column name="project" hidden="0" type="field" width="-1"/>
      <column name="waterlevel" hidden="0" type="field" width="-1"/>
      <column name="watlevgrsu" hidden="0" type="field" width="-1"/>
      <column name="watlevmsl" hidden="0" type="field" width="-1"/>
      <column name="watlevmp" hidden="0" type="field" width="-1"/>
      <column name="hoursnopum" hidden="0" type="field" width="-1"/>
      <column name="category" hidden="0" type="field" width="-1"/>
      <column name="method" hidden="0" type="field" width="-1"/>
      <column name="quality" hidden="0" type="field" width="-1"/>
      <column name="refpoint" hidden="0" type="field" width="-1"/>
      <column name="remark" hidden="0" type="field" width="-1"/>
      <column name="verticaref" hidden="0" type="field" width="-1"/>
      <column name="atmospresshpa" hidden="0" type="field" width="-1"/>
      <column name="extremes" hidden="0" type="field" width="-1"/>
      <column name="situation" hidden="0" type="field" width="-1"/>
      <column name="watlevelroundno" hidden="0" type="field" width="-1"/>
      <column name="qualitycontrol" hidden="0" type="field" width="-1"/>
      <column name="guid" hidden="0" type="field" width="-1"/>
      <column name="guid_intake" hidden="0" type="field" width="-1"/>
      <column name="guid_watlevround" hidden="0" type="field" width="-1"/>
      <column name="insertdate" hidden="0" type="field" width="-1"/>
      <column name="updatedate" hidden="0" type="field" width="-1"/>
      <column name="insertuser" hidden="0" type="field" width="-1"/>
      <column name="updateuser" hidden="0" type="field" width="-1"/>
      <column name="dataowner" hidden="0" type="field" width="-1"/>
      <column name="locatmetho" hidden="0" type="field" width="-1"/>
      <column name="screen_top" hidden="0" type="field" width="-1"/>
      <column name="screen_bottom" hidden="0" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="antal" editable="1"/>
    <field name="antal_pejl" editable="1"/>
    <field name="antal_pejlinger" editable="1"/>
    <field name="atmospresshpa" editable="1"/>
    <field name="auxiliary_storage_labeling_positionx" editable="0"/>
    <field name="auxiliary_storage_labeling_positiony" editable="0"/>
    <field name="borehole_1" editable="1"/>
    <field name="borehole_locatmetho" editable="0"/>
    <field name="boreholeid" editable="1"/>
    <field name="boreholeno" editable="1"/>
    <field name="category" editable="1"/>
    <field name="dataowner" editable="1"/>
    <field name="extremes" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="guid" editable="1"/>
    <field name="guid_intake" editable="1"/>
    <field name="guid_watlevround" editable="1"/>
    <field name="hoursnopum" editable="1"/>
    <field name="insertdate" editable="1"/>
    <field name="insertuser" editable="1"/>
    <field name="intakeid" editable="1"/>
    <field name="intakeno" editable="1"/>
    <field name="intakeno_2" editable="1"/>
    <field name="locatmetho" editable="1"/>
    <field name="method" editable="1"/>
    <field name="pejledato" editable="1"/>
    <field name="pejledato_" editable="1"/>
    <field name="project" editable="1"/>
    <field name="quality" editable="1"/>
    <field name="qualitycontrol" editable="1"/>
    <field name="refpoint" editable="1"/>
    <field name="remark" editable="1"/>
    <field name="row_id" editable="1"/>
    <field name="row_id_2" editable="1"/>
    <field name="rownumber" editable="1"/>
    <field name="screen_bottom" editable="0"/>
    <field name="screen_top" editable="0"/>
    <field name="screen_topbotquali" editable="0"/>
    <field name="situation" editable="1"/>
    <field name="timeofmeas" editable="1"/>
    <field name="updatedate" editable="1"/>
    <field name="updateuser" editable="1"/>
    <field name="vandst_m_1" editable="1"/>
    <field name="vandst_mut" editable="1"/>
    <field name="vandstko_1" editable="1"/>
    <field name="vandstkote" editable="1"/>
    <field name="verticaref" editable="1"/>
    <field name="waterlevel" editable="1"/>
    <field name="watlevel_situation" editable="0"/>
    <field name="watlevel_timeofmeas" editable="0"/>
    <field name="watlevel_watlevmsl" editable="0"/>
    <field name="watlevelid" editable="1"/>
    <field name="watlevelno" editable="1"/>
    <field name="watlevelroundno" editable="1"/>
    <field name="watlevgrsu" editable="1"/>
    <field name="watlevmp" editable="1"/>
    <field name="watlevmsl" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="antal" labelOnTop="0"/>
    <field name="antal_pejl" labelOnTop="0"/>
    <field name="antal_pejlinger" labelOnTop="0"/>
    <field name="atmospresshpa" labelOnTop="0"/>
    <field name="auxiliary_storage_labeling_positionx" labelOnTop="0"/>
    <field name="auxiliary_storage_labeling_positiony" labelOnTop="0"/>
    <field name="borehole_1" labelOnTop="0"/>
    <field name="borehole_locatmetho" labelOnTop="0"/>
    <field name="boreholeid" labelOnTop="0"/>
    <field name="boreholeno" labelOnTop="0"/>
    <field name="category" labelOnTop="0"/>
    <field name="dataowner" labelOnTop="0"/>
    <field name="extremes" labelOnTop="0"/>
    <field name="fid" labelOnTop="0"/>
    <field name="guid" labelOnTop="0"/>
    <field name="guid_intake" labelOnTop="0"/>
    <field name="guid_watlevround" labelOnTop="0"/>
    <field name="hoursnopum" labelOnTop="0"/>
    <field name="insertdate" labelOnTop="0"/>
    <field name="insertuser" labelOnTop="0"/>
    <field name="intakeid" labelOnTop="0"/>
    <field name="intakeno" labelOnTop="0"/>
    <field name="intakeno_2" labelOnTop="0"/>
    <field name="locatmetho" labelOnTop="0"/>
    <field name="method" labelOnTop="0"/>
    <field name="pejledato" labelOnTop="0"/>
    <field name="pejledato_" labelOnTop="0"/>
    <field name="project" labelOnTop="0"/>
    <field name="quality" labelOnTop="0"/>
    <field name="qualitycontrol" labelOnTop="0"/>
    <field name="refpoint" labelOnTop="0"/>
    <field name="remark" labelOnTop="0"/>
    <field name="row_id" labelOnTop="0"/>
    <field name="row_id_2" labelOnTop="0"/>
    <field name="rownumber" labelOnTop="0"/>
    <field name="screen_bottom" labelOnTop="0"/>
    <field name="screen_top" labelOnTop="0"/>
    <field name="screen_topbotquali" labelOnTop="0"/>
    <field name="situation" labelOnTop="0"/>
    <field name="timeofmeas" labelOnTop="0"/>
    <field name="updatedate" labelOnTop="0"/>
    <field name="updateuser" labelOnTop="0"/>
    <field name="vandst_m_1" labelOnTop="0"/>
    <field name="vandst_mut" labelOnTop="0"/>
    <field name="vandstko_1" labelOnTop="0"/>
    <field name="vandstkote" labelOnTop="0"/>
    <field name="verticaref" labelOnTop="0"/>
    <field name="waterlevel" labelOnTop="0"/>
    <field name="watlevel_situation" labelOnTop="0"/>
    <field name="watlevel_timeofmeas" labelOnTop="0"/>
    <field name="watlevel_watlevmsl" labelOnTop="0"/>
    <field name="watlevelid" labelOnTop="0"/>
    <field name="watlevelno" labelOnTop="0"/>
    <field name="watlevelroundno" labelOnTop="0"/>
    <field name="watlevgrsu" labelOnTop="0"/>
    <field name="watlevmp" labelOnTop="0"/>
    <field name="watlevmsl" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="antal" reuseLastValue="0"/>
    <field name="antal_pejlinger" reuseLastValue="0"/>
    <field name="atmospresshpa" reuseLastValue="0"/>
    <field name="auxiliary_storage_labeling_positionx" reuseLastValue="0"/>
    <field name="auxiliary_storage_labeling_positiony" reuseLastValue="0"/>
    <field name="borehole_locatmetho" reuseLastValue="0"/>
    <field name="boreholeid" reuseLastValue="0"/>
    <field name="boreholeno" reuseLastValue="0"/>
    <field name="category" reuseLastValue="0"/>
    <field name="dataowner" reuseLastValue="0"/>
    <field name="extremes" reuseLastValue="0"/>
    <field name="fid" reuseLastValue="0"/>
    <field name="guid" reuseLastValue="0"/>
    <field name="guid_intake" reuseLastValue="0"/>
    <field name="guid_watlevround" reuseLastValue="0"/>
    <field name="hoursnopum" reuseLastValue="0"/>
    <field name="insertdate" reuseLastValue="0"/>
    <field name="insertuser" reuseLastValue="0"/>
    <field name="intakeid" reuseLastValue="0"/>
    <field name="intakeno" reuseLastValue="0"/>
    <field name="locatmetho" reuseLastValue="0"/>
    <field name="method" reuseLastValue="0"/>
    <field name="project" reuseLastValue="0"/>
    <field name="quality" reuseLastValue="0"/>
    <field name="qualitycontrol" reuseLastValue="0"/>
    <field name="refpoint" reuseLastValue="0"/>
    <field name="remark" reuseLastValue="0"/>
    <field name="row_id" reuseLastValue="0"/>
    <field name="rownumber" reuseLastValue="0"/>
    <field name="screen_bottom" reuseLastValue="0"/>
    <field name="screen_top" reuseLastValue="0"/>
    <field name="screen_topbotquali" reuseLastValue="0"/>
    <field name="situation" reuseLastValue="0"/>
    <field name="timeofmeas" reuseLastValue="0"/>
    <field name="updatedate" reuseLastValue="0"/>
    <field name="updateuser" reuseLastValue="0"/>
    <field name="verticaref" reuseLastValue="0"/>
    <field name="waterlevel" reuseLastValue="0"/>
    <field name="watlevel_situation" reuseLastValue="0"/>
    <field name="watlevel_timeofmeas" reuseLastValue="0"/>
    <field name="watlevel_watlevmsl" reuseLastValue="0"/>
    <field name="watlevelid" reuseLastValue="0"/>
    <field name="watlevelno" reuseLastValue="0"/>
    <field name="watlevelroundno" reuseLastValue="0"/>
    <field name="watlevgrsu" reuseLastValue="0"/>
    <field name="watlevmp" reuseLastValue="0"/>
    <field name="watlevmsl" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"row_id"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
