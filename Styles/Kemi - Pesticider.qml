<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis labelsEnabled="0" maxScale="0" simplifyMaxScale="1" symbologyReferenceScale="-1" simplifyDrawingTol="1" version="3.26.3-Buenos Aires" minScale="100000000" simplifyDrawingHints="0" readOnly="0" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" simplifyLocal="1" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startExpression="" endExpression="" enabled="0" limitMode="0" startField="" mode="0" accumulate="0" fixedDuration="0" durationField="" endField="" durationUnit="min">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation clamping="Terrain" extrusionEnabled="0" type="IndividualFeatures" respectLayerSymbol="1" extrusion="0" showMarkerSymbolInSurfacePlots="0" zoffset="0" binding="Centroid" symbology="Line" zscale="1">
    <data-defined-properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol alpha="1" name="" type="line" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleLine">
          <Option type="Map">
            <Option value="0" name="align_dash_pattern" type="QString"/>
            <Option value="square" name="capstyle" type="QString"/>
            <Option value="5;2" name="customdash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
            <Option value="MM" name="customdash_unit" type="QString"/>
            <Option value="0" name="dash_pattern_offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
            <Option value="0" name="draw_inside_polygon" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="114,155,111,255" name="line_color" type="QString"/>
            <Option value="solid" name="line_style" type="QString"/>
            <Option value="0.6" name="line_width" type="QString"/>
            <Option value="MM" name="line_width_unit" type="QString"/>
            <Option value="0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="0" name="ring_filter" type="QString"/>
            <Option value="0" name="trim_distance_end" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_end_unit" type="QString"/>
            <Option value="0" name="trim_distance_start" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_start_unit" type="QString"/>
            <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
            <Option value="0" name="use_custom_dash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="114,155,111,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.6"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol alpha="1" name="" type="fill" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleFill">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale" type="QString"/>
            <Option value="114,155,111,255" name="color" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="81,111,79,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.2" name="outline_width" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="solid" name="style" type="QString"/>
          </Option>
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="114,155,111,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="81,111,79,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol alpha="1" name="" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="114,155,111,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="diamond" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="81,111,79,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.2" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="114,155,111,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="81,111,79,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 enableorderby="0" referencescale="-1" symbollevels="1" type="RuleRenderer" forceraster="0">
    <rules key="{d9867680-2d93-4c71-97bf-2a415794bcf9}">
      <rule filter="&quot;status_indtag&quot; = 1 AND year( &quot;dato_seneste_analyse&quot; ) >=2000" key="{bac96c69-49c0-4ec1-aa91-fa02e241d854}" label="Intet nu, intet tidligere" symbol="0">
        <rule filter="&quot;indtags_bjergart&quot; ILIKE 'Kvar%'" key="{eb90a4a3-a50e-49fa-894d-264430e39fb2}" label="Kvartært magasin" symbol="1"/>
        <rule filter="&quot;indtags_bjergart&quot; ILIKE 'Gli%' OR &quot;indtags_bjergart&quot; ILIKE 'Ikk%' OR &quot;indtags_bjergart&quot; ILIKE 'Danien%' OR &quot;indtags_bjergart&quot; ILIKE 'Skrive%'" key="{ef76c03f-fa30-4133-b5bd-3117595fb2ab}" label="Prækvartært magasin" symbol="2"/>
        <rule filter="ELSE" key="{13769fcb-a9ed-4e97-8714-b2f5970eb081}" label="Andet/ukendt magasin" symbol="3"/>
      </rule>
      <rule filter="&quot;status_indtag&quot;  = 2 AND year( &quot;dato_seneste_analyse&quot; ) >=2000" key="{7119b1e6-1146-461d-a6e7-9c87d0d00741}" label="Intet nu, tidligere fundet" symbol="4">
        <rule filter="&quot;indtags_bjergart&quot; ILIKE 'Kvar%'" key="{cefeba3c-0396-444c-b267-e4ff2218e7c0}" label="Kvartært magasin" symbol="5"/>
        <rule filter="&quot;indtags_bjergart&quot; ILIKE 'Gli%' OR &quot;indtags_bjergart&quot; ILIKE 'Ikk%' OR &quot;indtags_bjergart&quot; ILIKE 'Danien%' OR &quot;indtags_bjergart&quot; ILIKE 'Skrive%'" key="{eafa0b50-a29d-4409-be88-26c7ee8adb87}" label="Prækvartært magasin" symbol="6"/>
        <rule filter="ELSE" key="{50d9fb2f-87d8-48c1-8c9c-294ff97cf026}" label="Andet/ukendt magasin" symbol="7"/>
      </rule>
      <rule filter="&quot;status_indtag&quot;  = 3 AND year( &quot;dato_seneste_analyse&quot; ) >=2000" key="{ef687e67-de06-4dee-be6d-1f44eca059ca}" label="Aktuelt fund under grænseværdi" symbol="8">
        <rule filter="&quot;indtags_bjergart&quot; ILIKE 'Kvar%'" key="{e903284c-3b5a-46e3-9ee0-4f403d40edc6}" label="Kvartært magasin" symbol="9"/>
        <rule filter="&quot;indtags_bjergart&quot; ILIKE 'Gli%' OR &quot;indtags_bjergart&quot; ILIKE 'Ikk%' OR &quot;indtags_bjergart&quot; ILIKE 'Danien%' OR &quot;indtags_bjergart&quot; ILIKE 'Skrive%'" key="{d7621580-9486-4989-8e26-36748690490c}" label="Prækvartært magasin" symbol="10"/>
        <rule filter="ELSE" key="{faac8cb3-6b0c-4841-badd-8b077fca749d}" label="Andet/ukendt magasin" symbol="11"/>
      </rule>
      <rule filter="&quot;status_indtag&quot;  = 4 AND year( &quot;dato_seneste_analyse&quot; ) >=2000" key="{f89c83c8-fbb6-4a1d-a341-fc1e70a14230}" label="Aktuelt fund under grænseværdi, tidligere over" symbol="12">
        <rule filter="&quot;indtags_bjergart&quot; ILIKE 'Kvar%'" key="{1f239a18-023f-4ad3-b068-7ea7be5a12fd}" label="Kvartært magasin" symbol="13"/>
        <rule filter="&quot;indtags_bjergart&quot; ILIKE 'Gli%' OR &quot;indtags_bjergart&quot; ILIKE 'Ikk%' OR &quot;indtags_bjergart&quot; ILIKE 'Danien%' OR &quot;indtags_bjergart&quot; ILIKE 'Skrive%'" key="{d1a37fcf-ffd4-425d-a79f-40a2f04bbed0}" label="Prækvartært magasin" symbol="14"/>
        <rule filter="ELSE" key="{80fb7ce0-6bbc-4bbf-9deb-40a208185711}" label="Andet/ukendt magasin" symbol="15"/>
      </rule>
      <rule filter="&quot;status_indtag&quot;  = 5 AND year( &quot;dato_seneste_analyse&quot; ) >=2000" key="{413262e5-8e2f-4d77-bbd4-ade65d067d89}" label="Aktuelt fund over grænseværdi" symbol="16">
        <rule filter="&quot;indtags_bjergart&quot; ILIKE 'Kvar%'" key="{681bf6eb-658f-4215-b840-a47a5db55cce}" label="Kvartært magasin" symbol="17"/>
        <rule filter="&quot;indtags_bjergart&quot; ILIKE 'Gli%' OR &quot;indtags_bjergart&quot; ILIKE 'Ikk%' OR &quot;indtags_bjergart&quot; ILIKE 'Danien%' OR &quot;indtags_bjergart&quot; ILIKE 'Skrive%'" key="{9b571ab3-35bc-4b03-95bb-39f498c593e0}" label="Prækvartært magasin" symbol="18"/>
        <rule filter="ELSE" key="{a70c7a38-05df-4915-adb2-98860e1d235c}" label="Andet/ukendt magasin" symbol="19"/>
      </rule>
    </rules>
    <symbols>
      <symbol alpha="1" name="0" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="1" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="20,41,227,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="0" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="20,41,227,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="0"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="1" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="1" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="0,125,242,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="triangle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="0,0,0,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="0,125,242,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="triangle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="0,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="10" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="4" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="242,229,21,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="diamond" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="4,6,9,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3.2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="242,229,21,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="4,6,9,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3.2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="11" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="4" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="242,229,21,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="2,5,2,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="242,229,21,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="2,5,2,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="12" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="7" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="242,151,27,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="0" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="242,151,27,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="0"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="13" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="7" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="242,151,27,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="triangle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="6,10,15,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="242,151,27,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="triangle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="6,10,15,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="14" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="7" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="242,151,27,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="diamond" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="4,6,9,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3.2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="242,151,27,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="4,6,9,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3.2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="15" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="6" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="242,151,27,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="2,5,2,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="242,151,27,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="2,5,2,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="16" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="9" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="227,29,34,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="0" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="227,29,34,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="0"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="17" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="9" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="227,29,34,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="triangle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="6,10,15,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="227,29,34,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="triangle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="6,10,15,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="18" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="9" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="227,29,34,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="diamond" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="4,6,9,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3.2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="227,29,34,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="4,6,9,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3.2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="19" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="8" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="227,29,34,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="2,5,2,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="227,29,34,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="2,5,2,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="2" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="1" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="0,125,242,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="diamond" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="0,0,0,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3.2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="0,125,242,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="0,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3.2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="3" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="0,125,242,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="2,5,2,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="0,125,242,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="2,5,2,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="4" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="3" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="73,168,40,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="0" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="73,168,40,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="0"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="5" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="3" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="73,168,40,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="triangle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="6,10,15,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="73,168,40,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="triangle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="6,10,15,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="6" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="3" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="73,168,40,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="diamond" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="4,6,9,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3.2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="73,168,40,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="4,6,9,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3.2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="7" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="2" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="73,168,40,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="2,5,2,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="73,168,40,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="2,5,2,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="8" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="5" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="242,229,21,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="0" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="242,229,21,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="0"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="9" type="marker" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="4" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="242,229,21,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="triangle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="6,10,15,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.4" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="242,229,21,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="triangle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="6,10,15,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.4"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option name="dualview/previewExpressions" type="List">
        <Option value="&quot;row_id&quot;" type="QString"/>
      </Option>
      <Option value="0" name="embeddedWidgets/count" type="QString"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory direction="1" spacingUnitScale="3x:0,0,0,0,0,0" penWidth="0" opacity="1" barWidth="5" width="15" maxScaleDenominator="1e+08" diagramOrientation="Up" spacingUnit="MM" minScaleDenominator="0" rotationOffset="270" scaleBasedVisibility="0" penAlpha="255" showAxis="0" penColor="#000000" sizeType="MM" spacing="0" sizeScale="3x:0,0,0,0,0,0" enabled="0" scaleDependency="Area" minimumSize="0" lineSizeScale="3x:0,0,0,0,0,0" backgroundAlpha="255" height="15" backgroundColor="#ffffff" labelPlacementMethod="XHeight" lineSizeType="MM">
      <fontProperties strikethrough="0" style="" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" bold="0"/>
      <attribute colorOpacity="1" label="" color="#000000" field=""/>
      <axisSymbol>
        <symbol alpha="1" name="" type="line" force_rhr="0" is_animated="0" frame_rate="10" clip_to_extent="1">
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
          <layer enabled="1" locked="0" pass="0" class="SimpleLine">
            <Option type="Map">
              <Option value="0" name="align_dash_pattern" type="QString"/>
              <Option value="square" name="capstyle" type="QString"/>
              <Option value="5;2" name="customdash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
              <Option value="MM" name="customdash_unit" type="QString"/>
              <Option value="0" name="dash_pattern_offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
              <Option value="0" name="draw_inside_polygon" type="QString"/>
              <Option value="bevel" name="joinstyle" type="QString"/>
              <Option value="35,35,35,255" name="line_color" type="QString"/>
              <Option value="solid" name="line_style" type="QString"/>
              <Option value="0.26" name="line_width" type="QString"/>
              <Option value="MM" name="line_width_unit" type="QString"/>
              <Option value="0" name="offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="offset_unit" type="QString"/>
              <Option value="0" name="ring_filter" type="QString"/>
              <Option value="0" name="trim_distance_end" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_end_unit" type="QString"/>
              <Option value="0" name="trim_distance_start" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_start_unit" type="QString"/>
              <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
              <Option value="0" name="use_custom_dash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings placement="0" dist="0" zIndex="0" linePlacementFlags="18" priority="0" obstacle="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="dgu_nr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="url_borerapport" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="indtag_nr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="aktiv_indvinding_jn" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="indtag_top_dybde" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boringsanvendelse" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dato_seneste_analyse" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="analyser_10_aar" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="status_indtag" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="status_indtag_forklaret" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dominerende_stof" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="maengde" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dato" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="url_tidsserie" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="url_raadata" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anlaeg_id" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anlaegs_navn" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="virksomhedstype_tekst" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="overanlaegs_navn" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="virksomhedstype_kode" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="kommune_nr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="kommune_navn" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mc_kode" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mc_navn" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="region_kode" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="region_navn" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boringsformaal" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="indtag_anvendelse_tekst" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="terraen_kote" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boringsdybde" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="xutm32euref89" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="yutm32euref89" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="indtag_top_dybde_forklaret" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="indtag_bund_dybde" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="indtag_bund_dybde_forklaret" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="graensevaerdi_max" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="stofgruppe" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="stofgruppe_tekst" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="indtags_bjergart" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="akvitard_over_indtag_m" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="magasin_over_indtag_m" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ukendt_over_indtag_m" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boringskvalitet" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="hoved_vandopland" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="udtraeksdato" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="dgu_nr"/>
    <alias index="1" name="" field="url_borerapport"/>
    <alias index="2" name="" field="indtag_nr"/>
    <alias index="3" name="" field="aktiv_indvinding_jn"/>
    <alias index="4" name="" field="indtag_top_dybde"/>
    <alias index="5" name="" field="boringsanvendelse"/>
    <alias index="6" name="" field="dato_seneste_analyse"/>
    <alias index="7" name="" field="analyser_10_aar"/>
    <alias index="8" name="" field="status_indtag"/>
    <alias index="9" name="" field="status_indtag_forklaret"/>
    <alias index="10" name="" field="dominerende_stof"/>
    <alias index="11" name="" field="maengde"/>
    <alias index="12" name="" field="dato"/>
    <alias index="13" name="" field="url_tidsserie"/>
    <alias index="14" name="" field="url_raadata"/>
    <alias index="15" name="" field="id"/>
    <alias index="16" name="" field="anlaeg_id"/>
    <alias index="17" name="" field="anlaegs_navn"/>
    <alias index="18" name="" field="virksomhedstype_tekst"/>
    <alias index="19" name="" field="overanlaegs_navn"/>
    <alias index="20" name="" field="virksomhedstype_kode"/>
    <alias index="21" name="" field="kommune_nr"/>
    <alias index="22" name="" field="kommune_navn"/>
    <alias index="23" name="" field="mc_kode"/>
    <alias index="24" name="" field="mc_navn"/>
    <alias index="25" name="" field="region_kode"/>
    <alias index="26" name="" field="region_navn"/>
    <alias index="27" name="" field="boringsformaal"/>
    <alias index="28" name="" field="indtag_anvendelse_tekst"/>
    <alias index="29" name="" field="terraen_kote"/>
    <alias index="30" name="" field="boringsdybde"/>
    <alias index="31" name="" field="xutm32euref89"/>
    <alias index="32" name="" field="yutm32euref89"/>
    <alias index="33" name="" field="indtag_top_dybde_forklaret"/>
    <alias index="34" name="" field="indtag_bund_dybde"/>
    <alias index="35" name="" field="indtag_bund_dybde_forklaret"/>
    <alias index="36" name="" field="graensevaerdi_max"/>
    <alias index="37" name="" field="stofgruppe"/>
    <alias index="38" name="" field="stofgruppe_tekst"/>
    <alias index="39" name="" field="indtags_bjergart"/>
    <alias index="40" name="" field="akvitard_over_indtag_m"/>
    <alias index="41" name="" field="magasin_over_indtag_m"/>
    <alias index="42" name="" field="ukendt_over_indtag_m"/>
    <alias index="43" name="" field="boringskvalitet"/>
    <alias index="44" name="" field="hoved_vandopland"/>
    <alias index="45" name="" field="udtraeksdato"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" field="dgu_nr" expression=""/>
    <default applyOnUpdate="0" field="url_borerapport" expression=""/>
    <default applyOnUpdate="0" field="indtag_nr" expression=""/>
    <default applyOnUpdate="0" field="aktiv_indvinding_jn" expression=""/>
    <default applyOnUpdate="0" field="indtag_top_dybde" expression=""/>
    <default applyOnUpdate="0" field="boringsanvendelse" expression=""/>
    <default applyOnUpdate="0" field="dato_seneste_analyse" expression=""/>
    <default applyOnUpdate="0" field="analyser_10_aar" expression=""/>
    <default applyOnUpdate="0" field="status_indtag" expression=""/>
    <default applyOnUpdate="0" field="status_indtag_forklaret" expression=""/>
    <default applyOnUpdate="0" field="dominerende_stof" expression=""/>
    <default applyOnUpdate="0" field="maengde" expression=""/>
    <default applyOnUpdate="0" field="dato" expression=""/>
    <default applyOnUpdate="0" field="url_tidsserie" expression=""/>
    <default applyOnUpdate="0" field="url_raadata" expression=""/>
    <default applyOnUpdate="0" field="id" expression=""/>
    <default applyOnUpdate="0" field="anlaeg_id" expression=""/>
    <default applyOnUpdate="0" field="anlaegs_navn" expression=""/>
    <default applyOnUpdate="0" field="virksomhedstype_tekst" expression=""/>
    <default applyOnUpdate="0" field="overanlaegs_navn" expression=""/>
    <default applyOnUpdate="0" field="virksomhedstype_kode" expression=""/>
    <default applyOnUpdate="0" field="kommune_nr" expression=""/>
    <default applyOnUpdate="0" field="kommune_navn" expression=""/>
    <default applyOnUpdate="0" field="mc_kode" expression=""/>
    <default applyOnUpdate="0" field="mc_navn" expression=""/>
    <default applyOnUpdate="0" field="region_kode" expression=""/>
    <default applyOnUpdate="0" field="region_navn" expression=""/>
    <default applyOnUpdate="0" field="boringsformaal" expression=""/>
    <default applyOnUpdate="0" field="indtag_anvendelse_tekst" expression=""/>
    <default applyOnUpdate="0" field="terraen_kote" expression=""/>
    <default applyOnUpdate="0" field="boringsdybde" expression=""/>
    <default applyOnUpdate="0" field="xutm32euref89" expression=""/>
    <default applyOnUpdate="0" field="yutm32euref89" expression=""/>
    <default applyOnUpdate="0" field="indtag_top_dybde_forklaret" expression=""/>
    <default applyOnUpdate="0" field="indtag_bund_dybde" expression=""/>
    <default applyOnUpdate="0" field="indtag_bund_dybde_forklaret" expression=""/>
    <default applyOnUpdate="0" field="graensevaerdi_max" expression=""/>
    <default applyOnUpdate="0" field="stofgruppe" expression=""/>
    <default applyOnUpdate="0" field="stofgruppe_tekst" expression=""/>
    <default applyOnUpdate="0" field="indtags_bjergart" expression=""/>
    <default applyOnUpdate="0" field="akvitard_over_indtag_m" expression=""/>
    <default applyOnUpdate="0" field="magasin_over_indtag_m" expression=""/>
    <default applyOnUpdate="0" field="ukendt_over_indtag_m" expression=""/>
    <default applyOnUpdate="0" field="boringskvalitet" expression=""/>
    <default applyOnUpdate="0" field="hoved_vandopland" expression=""/>
    <default applyOnUpdate="0" field="udtraeksdato" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" notnull_strength="0" field="dgu_nr" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="url_borerapport" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="indtag_nr" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="aktiv_indvinding_jn" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="indtag_top_dybde" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="boringsanvendelse" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="dato_seneste_analyse" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="analyser_10_aar" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="status_indtag" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="status_indtag_forklaret" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="dominerende_stof" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="maengde" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="dato" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="url_tidsserie" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="url_raadata" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="id" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="anlaeg_id" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="anlaegs_navn" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="virksomhedstype_tekst" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="overanlaegs_navn" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="virksomhedstype_kode" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="kommune_nr" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="kommune_navn" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="mc_kode" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="mc_navn" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="region_kode" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="region_navn" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="boringsformaal" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="indtag_anvendelse_tekst" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="terraen_kote" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="boringsdybde" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="xutm32euref89" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="yutm32euref89" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="indtag_top_dybde_forklaret" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="indtag_bund_dybde" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="indtag_bund_dybde_forklaret" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="graensevaerdi_max" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="stofgruppe" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="stofgruppe_tekst" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="indtags_bjergart" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="akvitard_over_indtag_m" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="magasin_over_indtag_m" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="ukendt_over_indtag_m" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="boringskvalitet" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="hoved_vandopland" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="udtraeksdato" constraints="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="dgu_nr" exp="" desc=""/>
    <constraint field="url_borerapport" exp="" desc=""/>
    <constraint field="indtag_nr" exp="" desc=""/>
    <constraint field="aktiv_indvinding_jn" exp="" desc=""/>
    <constraint field="indtag_top_dybde" exp="" desc=""/>
    <constraint field="boringsanvendelse" exp="" desc=""/>
    <constraint field="dato_seneste_analyse" exp="" desc=""/>
    <constraint field="analyser_10_aar" exp="" desc=""/>
    <constraint field="status_indtag" exp="" desc=""/>
    <constraint field="status_indtag_forklaret" exp="" desc=""/>
    <constraint field="dominerende_stof" exp="" desc=""/>
    <constraint field="maengde" exp="" desc=""/>
    <constraint field="dato" exp="" desc=""/>
    <constraint field="url_tidsserie" exp="" desc=""/>
    <constraint field="url_raadata" exp="" desc=""/>
    <constraint field="id" exp="" desc=""/>
    <constraint field="anlaeg_id" exp="" desc=""/>
    <constraint field="anlaegs_navn" exp="" desc=""/>
    <constraint field="virksomhedstype_tekst" exp="" desc=""/>
    <constraint field="overanlaegs_navn" exp="" desc=""/>
    <constraint field="virksomhedstype_kode" exp="" desc=""/>
    <constraint field="kommune_nr" exp="" desc=""/>
    <constraint field="kommune_navn" exp="" desc=""/>
    <constraint field="mc_kode" exp="" desc=""/>
    <constraint field="mc_navn" exp="" desc=""/>
    <constraint field="region_kode" exp="" desc=""/>
    <constraint field="region_navn" exp="" desc=""/>
    <constraint field="boringsformaal" exp="" desc=""/>
    <constraint field="indtag_anvendelse_tekst" exp="" desc=""/>
    <constraint field="terraen_kote" exp="" desc=""/>
    <constraint field="boringsdybde" exp="" desc=""/>
    <constraint field="xutm32euref89" exp="" desc=""/>
    <constraint field="yutm32euref89" exp="" desc=""/>
    <constraint field="indtag_top_dybde_forklaret" exp="" desc=""/>
    <constraint field="indtag_bund_dybde" exp="" desc=""/>
    <constraint field="indtag_bund_dybde_forklaret" exp="" desc=""/>
    <constraint field="graensevaerdi_max" exp="" desc=""/>
    <constraint field="stofgruppe" exp="" desc=""/>
    <constraint field="stofgruppe_tekst" exp="" desc=""/>
    <constraint field="indtags_bjergart" exp="" desc=""/>
    <constraint field="akvitard_over_indtag_m" exp="" desc=""/>
    <constraint field="magasin_over_indtag_m" exp="" desc=""/>
    <constraint field="ukendt_over_indtag_m" exp="" desc=""/>
    <constraint field="boringskvalitet" exp="" desc=""/>
    <constraint field="hoved_vandopland" exp="" desc=""/>
    <constraint field="udtraeksdato" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;Status indtag&quot;" sortOrder="1" actionWidgetStyle="dropDown">
    <columns>
      <column name="dgu_nr" hidden="0" type="field" width="-1"/>
      <column name="indtag_nr" hidden="0" type="field" width="-1"/>
      <column name="maengde" hidden="0" type="field" width="-1"/>
      <column name="dato" hidden="0" type="field" width="-1"/>
      <column name="id" hidden="0" type="field" width="-1"/>
      <column name="anlaeg_id" hidden="0" type="field" width="-1"/>
      <column name="kommune_nr" hidden="0" type="field" width="-1"/>
      <column name="mc_kode" hidden="0" type="field" width="-1"/>
      <column name="mc_navn" hidden="0" type="field" width="-1"/>
      <column name="stofgruppe" hidden="0" type="field" width="-1"/>
      <column name="url_borerapport" hidden="0" type="field" width="-1"/>
      <column name="aktiv_indvinding_jn" hidden="0" type="field" width="-1"/>
      <column name="indtag_top_dybde" hidden="0" type="field" width="-1"/>
      <column name="boringsanvendelse" hidden="0" type="field" width="-1"/>
      <column name="dato_seneste_analyse" hidden="0" type="field" width="243"/>
      <column name="analyser_10_aar" hidden="0" type="field" width="-1"/>
      <column name="status_indtag" hidden="0" type="field" width="-1"/>
      <column name="status_indtag_forklaret" hidden="0" type="field" width="-1"/>
      <column name="dominerende_stof" hidden="0" type="field" width="-1"/>
      <column name="url_tidsserie" hidden="0" type="field" width="-1"/>
      <column name="url_raadata" hidden="0" type="field" width="-1"/>
      <column name="anlaegs_navn" hidden="0" type="field" width="-1"/>
      <column name="virksomhedstype_tekst" hidden="0" type="field" width="-1"/>
      <column name="overanlaegs_navn" hidden="0" type="field" width="-1"/>
      <column name="virksomhedstype_kode" hidden="0" type="field" width="-1"/>
      <column name="kommune_navn" hidden="0" type="field" width="-1"/>
      <column name="region_kode" hidden="0" type="field" width="-1"/>
      <column name="region_navn" hidden="0" type="field" width="-1"/>
      <column name="boringsformaal" hidden="0" type="field" width="-1"/>
      <column name="indtag_anvendelse_tekst" hidden="0" type="field" width="-1"/>
      <column name="terraen_kote" hidden="0" type="field" width="-1"/>
      <column name="boringsdybde" hidden="0" type="field" width="-1"/>
      <column name="xutm32euref89" hidden="0" type="field" width="-1"/>
      <column name="yutm32euref89" hidden="0" type="field" width="-1"/>
      <column name="indtag_top_dybde_forklaret" hidden="0" type="field" width="-1"/>
      <column name="indtag_bund_dybde" hidden="0" type="field" width="-1"/>
      <column name="indtag_bund_dybde_forklaret" hidden="0" type="field" width="-1"/>
      <column name="graensevaerdi_max" hidden="0" type="field" width="-1"/>
      <column name="stofgruppe_tekst" hidden="0" type="field" width="-1"/>
      <column name="indtags_bjergart" hidden="0" type="field" width="192"/>
      <column name="akvitard_over_indtag_m" hidden="0" type="field" width="-1"/>
      <column name="magasin_over_indtag_m" hidden="0" type="field" width="-1"/>
      <column name="ukendt_over_indtag_m" hidden="0" type="field" width="-1"/>
      <column name="boringskvalitet" hidden="0" type="field" width="-1"/>
      <column name="hoved_vandopland" hidden="0" type="field" width="-1"/>
      <column name="udtraeksdato" hidden="0" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="Aktiv indvinding jn" editable="1"/>
    <field name="Akvitard over indtag m" editable="1"/>
    <field name="Analyser 10 aar" editable="1"/>
    <field name="Anlaeg id" editable="1"/>
    <field name="Anlaegs navn" editable="1"/>
    <field name="Boringsanvendelse" editable="1"/>
    <field name="Boringsdybde" editable="1"/>
    <field name="Boringsformaal" editable="1"/>
    <field name="Boringskvalitet" editable="1"/>
    <field name="Boringstype" editable="1"/>
    <field name="Dato" editable="1"/>
    <field name="Dato seneste analyse" editable="1"/>
    <field name="Dgu nr" editable="1"/>
    <field name="Dominerende stof" editable="1"/>
    <field name="Graensevaerdi max" editable="1"/>
    <field name="Grumo loop" editable="1"/>
    <field name="Hoved vandopland" editable="1"/>
    <field name="Id" editable="1"/>
    <field name="Indtag anvendelse tekst" editable="1"/>
    <field name="Indtag bund dybde" editable="1"/>
    <field name="Indtag bund dybde forklaret" editable="1"/>
    <field name="Indtag nr" editable="1"/>
    <field name="Indtag top dybde" editable="1"/>
    <field name="Indtag top dybde forklaret" editable="1"/>
    <field name="Indtags bjergart" editable="1"/>
    <field name="Kommune navn" editable="1"/>
    <field name="Kommune nr" editable="1"/>
    <field name="Maengde" editable="1"/>
    <field name="Magasin over indtag m" editable="1"/>
    <field name="Mc kode" editable="1"/>
    <field name="Mc navn" editable="1"/>
    <field name="Overanlaegs navn" editable="1"/>
    <field name="Region kode" editable="1"/>
    <field name="Region navn" editable="1"/>
    <field name="Rgb" editable="1"/>
    <field name="Sloejfet jn" editable="1"/>
    <field name="Status indtag" editable="1"/>
    <field name="Status indtag forklaret" editable="1"/>
    <field name="Stofgruppe" editable="1"/>
    <field name="Stofgruppe tekst" editable="1"/>
    <field name="Stofnr detekteret" editable="1"/>
    <field name="Symbol" editable="1"/>
    <field name="Terraen kote" editable="1"/>
    <field name="Udtraeksdato" editable="1"/>
    <field name="Ukendt over indtag m" editable="1"/>
    <field name="Url anlaeg" editable="1"/>
    <field name="Url borerapport" editable="1"/>
    <field name="Url raadata" editable="1"/>
    <field name="Url tidsserie" editable="1"/>
    <field name="Vandtype" editable="1"/>
    <field name="Virksomhedstype kode" editable="1"/>
    <field name="Virksomhedstype tekst" editable="1"/>
    <field name="Xutm32euref89" editable="1"/>
    <field name="Yutm32euref89" editable="1"/>
    <field name="aarstal" editable="1"/>
    <field name="aarstal_int" editable="1"/>
    <field name="aktiv_indv" editable="1"/>
    <field name="aktiv_indvinding_jn" editable="1"/>
    <field name="akvitard_o" editable="1"/>
    <field name="akvitard_over_indtag_m" editable="1"/>
    <field name="ammoniak_ammonium" editable="1"/>
    <field name="ammoniak_ammonium_edit" editable="1"/>
    <field name="ammoniak_ammonium_original" editable="1"/>
    <field name="analyser_1" editable="1"/>
    <field name="analyser_10_aar" editable="1"/>
    <field name="anlaeg_id" editable="1"/>
    <field name="anlaegs_na" editable="1"/>
    <field name="anlaegs_navn" editable="1"/>
    <field name="boreholeid" editable="1"/>
    <field name="boreholeno" editable="1"/>
    <field name="boringsanv" editable="1"/>
    <field name="boringsanvendelse" editable="1"/>
    <field name="boringsdyb" editable="1"/>
    <field name="boringsdybde" editable="1"/>
    <field name="boringsfor" editable="1"/>
    <field name="boringsformaal" editable="1"/>
    <field name="boringsformål" editable="1"/>
    <field name="boringskva" editable="1"/>
    <field name="boringskvalitet" editable="1"/>
    <field name="dato" editable="1"/>
    <field name="dato_senes" editable="1"/>
    <field name="dato_seneste_analyse" editable="1"/>
    <field name="dato_vaerdi" editable="1"/>
    <field name="dgu_indtag" editable="1"/>
    <field name="dgu_nr" editable="1"/>
    <field name="dominerend" editable="1"/>
    <field name="dominerende_stof" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="filter_top" editable="1"/>
    <field name="filter_top_mut" editable="1"/>
    <field name="filterbund_kote" editable="1"/>
    <field name="filterbund_mut" editable="1"/>
    <field name="filtertop_kote" editable="1"/>
    <field name="filtertop_mut" editable="1"/>
    <field name="fohm_layer" editable="1"/>
    <field name="forvitgrad" editable="1"/>
    <field name="graensevae" editable="1"/>
    <field name="graensevaerdi_max" editable="1"/>
    <field name="guid_intake" editable="1"/>
    <field name="hoved_vand" editable="1"/>
    <field name="hoved_vandopland" editable="1"/>
    <field name="id" editable="1"/>
    <field name="indtag_anv" editable="1"/>
    <field name="indtag_anvendelse_tekst" editable="1"/>
    <field name="indtag_b_1" editable="1"/>
    <field name="indtag_bun" editable="1"/>
    <field name="indtag_bund_dybde" editable="1"/>
    <field name="indtag_bund_dybde_forklaret" editable="1"/>
    <field name="indtag_nr" editable="1"/>
    <field name="indtag_t_1" editable="1"/>
    <field name="indtag_top" editable="1"/>
    <field name="indtag_top_dybde" editable="1"/>
    <field name="indtag_top_dybde_forklaret" editable="1"/>
    <field name="indtags_bj" editable="1"/>
    <field name="indtags_bjergart" editable="1"/>
    <field name="intakeno" editable="1"/>
    <field name="ionbalance" editable="1"/>
    <field name="ionexchange" editable="1"/>
    <field name="jern" editable="1"/>
    <field name="jern_edit" editable="1"/>
    <field name="jern_original" editable="1"/>
    <field name="kommune_na" editable="1"/>
    <field name="kommune_navn" editable="1"/>
    <field name="kommune_nr" editable="1"/>
    <field name="lith_in_screen" editable="1"/>
    <field name="maengde" editable="1"/>
    <field name="magasin" editable="1"/>
    <field name="magasin_ov" editable="1"/>
    <field name="magasin_over_indtag_m" editable="1"/>
    <field name="mc_kode" editable="1"/>
    <field name="mc_navn" editable="1"/>
    <field name="metan" editable="1"/>
    <field name="nitrat" editable="1"/>
    <field name="nitrat2" editable="1"/>
    <field name="nitrat_edit" editable="1"/>
    <field name="nitrat_int" editable="1"/>
    <field name="nitrat_mgprl" editable="1"/>
    <field name="nitrat_nitrit_eq" editable="1"/>
    <field name="nitrat_original" editable="1"/>
    <field name="nitrit" editable="1"/>
    <field name="overanlaeg" editable="1"/>
    <field name="overanlaegs_navn" editable="1"/>
    <field name="oxygen_indhold" editable="1"/>
    <field name="oxygen_indhold_edit" editable="1"/>
    <field name="oxygen_indhold_original" editable="1"/>
    <field name="project" editable="1"/>
    <field name="purpose" editable="1"/>
    <field name="purpose_int" editable="1"/>
    <field name="region_kod" editable="1"/>
    <field name="region_kode" editable="1"/>
    <field name="region_nav" editable="1"/>
    <field name="region_navn" editable="1"/>
    <field name="row_id" editable="1"/>
    <field name="sampledate" editable="1"/>
    <field name="sampleid" editable="1"/>
    <field name="status" editable="1"/>
    <field name="status_i_1" editable="1"/>
    <field name="status_ind" editable="1"/>
    <field name="status_indtag" editable="1"/>
    <field name="status_indtag_forklaret" editable="1"/>
    <field name="stofgrup_1" editable="1"/>
    <field name="stofgruppe" editable="1"/>
    <field name="stofgruppe_tekst" editable="1"/>
    <field name="stringno" editable="1"/>
    <field name="sulfat" editable="1"/>
    <field name="sulfat_edit" editable="1"/>
    <field name="sulfat_int" editable="1"/>
    <field name="sulfat_mgprl" editable="1"/>
    <field name="sulfat_original" editable="1"/>
    <field name="sumanoinscalculated" editable="1"/>
    <field name="sumcationscalculated" editable="1"/>
    <field name="terraen_ko" editable="1"/>
    <field name="terraen_kote" editable="1"/>
    <field name="udtraeksda" editable="1"/>
    <field name="udtraeksdato" editable="1"/>
    <field name="ukendt_ove" editable="1"/>
    <field name="ukendt_over_indtag_m" editable="1"/>
    <field name="unik" editable="1"/>
    <field name="url_borera" editable="1"/>
    <field name="url_borerapport" editable="1"/>
    <field name="url_raadat" editable="1"/>
    <field name="url_raadata" editable="1"/>
    <field name="url_tidsse" editable="1"/>
    <field name="url_tidsserie" editable="1"/>
    <field name="use" editable="1"/>
    <field name="use_int" editable="1"/>
    <field name="utmzone" editable="1"/>
    <field name="virksomh_1" editable="1"/>
    <field name="virksomhed" editable="1"/>
    <field name="virksomhedstype_kode" editable="1"/>
    <field name="virksomhedstype_tekst" editable="1"/>
    <field name="xutm32eure" editable="1"/>
    <field name="xutm32euref89" editable="1"/>
    <field name="yutm32eure" editable="1"/>
    <field name="yutm32euref89" editable="1"/>
    <field name="Årstal" editable="1"/>
    <field name="Årstal_int" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="Aktiv indvinding jn" labelOnTop="0"/>
    <field name="Akvitard over indtag m" labelOnTop="0"/>
    <field name="Analyser 10 aar" labelOnTop="0"/>
    <field name="Anlaeg id" labelOnTop="0"/>
    <field name="Anlaegs navn" labelOnTop="0"/>
    <field name="Boringsanvendelse" labelOnTop="0"/>
    <field name="Boringsdybde" labelOnTop="0"/>
    <field name="Boringsformaal" labelOnTop="0"/>
    <field name="Boringskvalitet" labelOnTop="0"/>
    <field name="Boringstype" labelOnTop="0"/>
    <field name="Dato" labelOnTop="0"/>
    <field name="Dato seneste analyse" labelOnTop="0"/>
    <field name="Dgu nr" labelOnTop="0"/>
    <field name="Dominerende stof" labelOnTop="0"/>
    <field name="Graensevaerdi max" labelOnTop="0"/>
    <field name="Grumo loop" labelOnTop="0"/>
    <field name="Hoved vandopland" labelOnTop="0"/>
    <field name="Id" labelOnTop="0"/>
    <field name="Indtag anvendelse tekst" labelOnTop="0"/>
    <field name="Indtag bund dybde" labelOnTop="0"/>
    <field name="Indtag bund dybde forklaret" labelOnTop="0"/>
    <field name="Indtag nr" labelOnTop="0"/>
    <field name="Indtag top dybde" labelOnTop="0"/>
    <field name="Indtag top dybde forklaret" labelOnTop="0"/>
    <field name="Indtags bjergart" labelOnTop="0"/>
    <field name="Kommune navn" labelOnTop="0"/>
    <field name="Kommune nr" labelOnTop="0"/>
    <field name="Maengde" labelOnTop="0"/>
    <field name="Magasin over indtag m" labelOnTop="0"/>
    <field name="Mc kode" labelOnTop="0"/>
    <field name="Mc navn" labelOnTop="0"/>
    <field name="Overanlaegs navn" labelOnTop="0"/>
    <field name="Region kode" labelOnTop="0"/>
    <field name="Region navn" labelOnTop="0"/>
    <field name="Rgb" labelOnTop="0"/>
    <field name="Sloejfet jn" labelOnTop="0"/>
    <field name="Status indtag" labelOnTop="0"/>
    <field name="Status indtag forklaret" labelOnTop="0"/>
    <field name="Stofgruppe" labelOnTop="0"/>
    <field name="Stofgruppe tekst" labelOnTop="0"/>
    <field name="Stofnr detekteret" labelOnTop="0"/>
    <field name="Symbol" labelOnTop="0"/>
    <field name="Terraen kote" labelOnTop="0"/>
    <field name="Udtraeksdato" labelOnTop="0"/>
    <field name="Ukendt over indtag m" labelOnTop="0"/>
    <field name="Url anlaeg" labelOnTop="0"/>
    <field name="Url borerapport" labelOnTop="0"/>
    <field name="Url raadata" labelOnTop="0"/>
    <field name="Url tidsserie" labelOnTop="0"/>
    <field name="Vandtype" labelOnTop="0"/>
    <field name="Virksomhedstype kode" labelOnTop="0"/>
    <field name="Virksomhedstype tekst" labelOnTop="0"/>
    <field name="Xutm32euref89" labelOnTop="0"/>
    <field name="Yutm32euref89" labelOnTop="0"/>
    <field name="aarstal" labelOnTop="0"/>
    <field name="aarstal_int" labelOnTop="0"/>
    <field name="aktiv_indv" labelOnTop="0"/>
    <field name="aktiv_indvinding_jn" labelOnTop="0"/>
    <field name="akvitard_o" labelOnTop="0"/>
    <field name="akvitard_over_indtag_m" labelOnTop="0"/>
    <field name="ammoniak_ammonium" labelOnTop="0"/>
    <field name="ammoniak_ammonium_edit" labelOnTop="0"/>
    <field name="ammoniak_ammonium_original" labelOnTop="0"/>
    <field name="analyser_1" labelOnTop="0"/>
    <field name="analyser_10_aar" labelOnTop="0"/>
    <field name="anlaeg_id" labelOnTop="0"/>
    <field name="anlaegs_na" labelOnTop="0"/>
    <field name="anlaegs_navn" labelOnTop="0"/>
    <field name="boreholeid" labelOnTop="0"/>
    <field name="boreholeno" labelOnTop="0"/>
    <field name="boringsanv" labelOnTop="0"/>
    <field name="boringsanvendelse" labelOnTop="0"/>
    <field name="boringsdyb" labelOnTop="0"/>
    <field name="boringsdybde" labelOnTop="0"/>
    <field name="boringsfor" labelOnTop="0"/>
    <field name="boringsformaal" labelOnTop="0"/>
    <field name="boringsformål" labelOnTop="0"/>
    <field name="boringskva" labelOnTop="0"/>
    <field name="boringskvalitet" labelOnTop="0"/>
    <field name="dato" labelOnTop="0"/>
    <field name="dato_senes" labelOnTop="0"/>
    <field name="dato_seneste_analyse" labelOnTop="0"/>
    <field name="dato_vaerdi" labelOnTop="0"/>
    <field name="dgu_indtag" labelOnTop="0"/>
    <field name="dgu_nr" labelOnTop="0"/>
    <field name="dominerend" labelOnTop="0"/>
    <field name="dominerende_stof" labelOnTop="0"/>
    <field name="fid" labelOnTop="0"/>
    <field name="filter_top" labelOnTop="0"/>
    <field name="filter_top_mut" labelOnTop="0"/>
    <field name="filterbund_kote" labelOnTop="0"/>
    <field name="filterbund_mut" labelOnTop="0"/>
    <field name="filtertop_kote" labelOnTop="0"/>
    <field name="filtertop_mut" labelOnTop="0"/>
    <field name="fohm_layer" labelOnTop="0"/>
    <field name="forvitgrad" labelOnTop="0"/>
    <field name="graensevae" labelOnTop="0"/>
    <field name="graensevaerdi_max" labelOnTop="0"/>
    <field name="guid_intake" labelOnTop="0"/>
    <field name="hoved_vand" labelOnTop="0"/>
    <field name="hoved_vandopland" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="indtag_anv" labelOnTop="0"/>
    <field name="indtag_anvendelse_tekst" labelOnTop="0"/>
    <field name="indtag_b_1" labelOnTop="0"/>
    <field name="indtag_bun" labelOnTop="0"/>
    <field name="indtag_bund_dybde" labelOnTop="0"/>
    <field name="indtag_bund_dybde_forklaret" labelOnTop="0"/>
    <field name="indtag_nr" labelOnTop="0"/>
    <field name="indtag_t_1" labelOnTop="0"/>
    <field name="indtag_top" labelOnTop="0"/>
    <field name="indtag_top_dybde" labelOnTop="0"/>
    <field name="indtag_top_dybde_forklaret" labelOnTop="0"/>
    <field name="indtags_bj" labelOnTop="0"/>
    <field name="indtags_bjergart" labelOnTop="0"/>
    <field name="intakeno" labelOnTop="0"/>
    <field name="ionbalance" labelOnTop="0"/>
    <field name="ionexchange" labelOnTop="0"/>
    <field name="jern" labelOnTop="0"/>
    <field name="jern_edit" labelOnTop="0"/>
    <field name="jern_original" labelOnTop="0"/>
    <field name="kommune_na" labelOnTop="0"/>
    <field name="kommune_navn" labelOnTop="0"/>
    <field name="kommune_nr" labelOnTop="0"/>
    <field name="lith_in_screen" labelOnTop="0"/>
    <field name="maengde" labelOnTop="0"/>
    <field name="magasin" labelOnTop="0"/>
    <field name="magasin_ov" labelOnTop="0"/>
    <field name="magasin_over_indtag_m" labelOnTop="0"/>
    <field name="mc_kode" labelOnTop="0"/>
    <field name="mc_navn" labelOnTop="0"/>
    <field name="metan" labelOnTop="0"/>
    <field name="nitrat" labelOnTop="0"/>
    <field name="nitrat2" labelOnTop="0"/>
    <field name="nitrat_edit" labelOnTop="0"/>
    <field name="nitrat_int" labelOnTop="0"/>
    <field name="nitrat_mgprl" labelOnTop="0"/>
    <field name="nitrat_nitrit_eq" labelOnTop="0"/>
    <field name="nitrat_original" labelOnTop="0"/>
    <field name="nitrit" labelOnTop="0"/>
    <field name="overanlaeg" labelOnTop="0"/>
    <field name="overanlaegs_navn" labelOnTop="0"/>
    <field name="oxygen_indhold" labelOnTop="0"/>
    <field name="oxygen_indhold_edit" labelOnTop="0"/>
    <field name="oxygen_indhold_original" labelOnTop="0"/>
    <field name="project" labelOnTop="0"/>
    <field name="purpose" labelOnTop="0"/>
    <field name="purpose_int" labelOnTop="0"/>
    <field name="region_kod" labelOnTop="0"/>
    <field name="region_kode" labelOnTop="0"/>
    <field name="region_nav" labelOnTop="0"/>
    <field name="region_navn" labelOnTop="0"/>
    <field name="row_id" labelOnTop="0"/>
    <field name="sampledate" labelOnTop="0"/>
    <field name="sampleid" labelOnTop="0"/>
    <field name="status" labelOnTop="0"/>
    <field name="status_i_1" labelOnTop="0"/>
    <field name="status_ind" labelOnTop="0"/>
    <field name="status_indtag" labelOnTop="0"/>
    <field name="status_indtag_forklaret" labelOnTop="0"/>
    <field name="stofgrup_1" labelOnTop="0"/>
    <field name="stofgruppe" labelOnTop="0"/>
    <field name="stofgruppe_tekst" labelOnTop="0"/>
    <field name="stringno" labelOnTop="0"/>
    <field name="sulfat" labelOnTop="0"/>
    <field name="sulfat_edit" labelOnTop="0"/>
    <field name="sulfat_int" labelOnTop="0"/>
    <field name="sulfat_mgprl" labelOnTop="0"/>
    <field name="sulfat_original" labelOnTop="0"/>
    <field name="sumanoinscalculated" labelOnTop="0"/>
    <field name="sumcationscalculated" labelOnTop="0"/>
    <field name="terraen_ko" labelOnTop="0"/>
    <field name="terraen_kote" labelOnTop="0"/>
    <field name="udtraeksda" labelOnTop="0"/>
    <field name="udtraeksdato" labelOnTop="0"/>
    <field name="ukendt_ove" labelOnTop="0"/>
    <field name="ukendt_over_indtag_m" labelOnTop="0"/>
    <field name="unik" labelOnTop="0"/>
    <field name="url_borera" labelOnTop="0"/>
    <field name="url_borerapport" labelOnTop="0"/>
    <field name="url_raadat" labelOnTop="0"/>
    <field name="url_raadata" labelOnTop="0"/>
    <field name="url_tidsse" labelOnTop="0"/>
    <field name="url_tidsserie" labelOnTop="0"/>
    <field name="use" labelOnTop="0"/>
    <field name="use_int" labelOnTop="0"/>
    <field name="utmzone" labelOnTop="0"/>
    <field name="virksomh_1" labelOnTop="0"/>
    <field name="virksomhed" labelOnTop="0"/>
    <field name="virksomhedstype_kode" labelOnTop="0"/>
    <field name="virksomhedstype_tekst" labelOnTop="0"/>
    <field name="xutm32eure" labelOnTop="0"/>
    <field name="xutm32euref89" labelOnTop="0"/>
    <field name="yutm32eure" labelOnTop="0"/>
    <field name="yutm32euref89" labelOnTop="0"/>
    <field name="Årstal" labelOnTop="0"/>
    <field name="Årstal_int" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="Aktiv indvinding jn"/>
    <field reuseLastValue="0" name="Akvitard over indtag m"/>
    <field reuseLastValue="0" name="Analyser 10 aar"/>
    <field reuseLastValue="0" name="Anlaeg id"/>
    <field reuseLastValue="0" name="Anlaegs navn"/>
    <field reuseLastValue="0" name="Boringsanvendelse"/>
    <field reuseLastValue="0" name="Boringsdybde"/>
    <field reuseLastValue="0" name="Boringsformaal"/>
    <field reuseLastValue="0" name="Boringskvalitet"/>
    <field reuseLastValue="0" name="Boringstype"/>
    <field reuseLastValue="0" name="Dato"/>
    <field reuseLastValue="0" name="Dato seneste analyse"/>
    <field reuseLastValue="0" name="Dgu nr"/>
    <field reuseLastValue="0" name="Dominerende stof"/>
    <field reuseLastValue="0" name="Graensevaerdi max"/>
    <field reuseLastValue="0" name="Grumo loop"/>
    <field reuseLastValue="0" name="Hoved vandopland"/>
    <field reuseLastValue="0" name="Id"/>
    <field reuseLastValue="0" name="Indtag anvendelse tekst"/>
    <field reuseLastValue="0" name="Indtag bund dybde"/>
    <field reuseLastValue="0" name="Indtag bund dybde forklaret"/>
    <field reuseLastValue="0" name="Indtag nr"/>
    <field reuseLastValue="0" name="Indtag top dybde"/>
    <field reuseLastValue="0" name="Indtag top dybde forklaret"/>
    <field reuseLastValue="0" name="Indtags bjergart"/>
    <field reuseLastValue="0" name="Kommune navn"/>
    <field reuseLastValue="0" name="Kommune nr"/>
    <field reuseLastValue="0" name="Maengde"/>
    <field reuseLastValue="0" name="Magasin over indtag m"/>
    <field reuseLastValue="0" name="Mc kode"/>
    <field reuseLastValue="0" name="Mc navn"/>
    <field reuseLastValue="0" name="Overanlaegs navn"/>
    <field reuseLastValue="0" name="Region kode"/>
    <field reuseLastValue="0" name="Region navn"/>
    <field reuseLastValue="0" name="Rgb"/>
    <field reuseLastValue="0" name="Sloejfet jn"/>
    <field reuseLastValue="0" name="Status indtag"/>
    <field reuseLastValue="0" name="Status indtag forklaret"/>
    <field reuseLastValue="0" name="Stofgruppe"/>
    <field reuseLastValue="0" name="Stofgruppe tekst"/>
    <field reuseLastValue="0" name="Stofnr detekteret"/>
    <field reuseLastValue="0" name="Symbol"/>
    <field reuseLastValue="0" name="Terraen kote"/>
    <field reuseLastValue="0" name="Udtraeksdato"/>
    <field reuseLastValue="0" name="Ukendt over indtag m"/>
    <field reuseLastValue="0" name="Url anlaeg"/>
    <field reuseLastValue="0" name="Url borerapport"/>
    <field reuseLastValue="0" name="Url raadata"/>
    <field reuseLastValue="0" name="Url tidsserie"/>
    <field reuseLastValue="0" name="Vandtype"/>
    <field reuseLastValue="0" name="Virksomhedstype kode"/>
    <field reuseLastValue="0" name="Virksomhedstype tekst"/>
    <field reuseLastValue="0" name="Xutm32euref89"/>
    <field reuseLastValue="0" name="Yutm32euref89"/>
    <field reuseLastValue="0" name="aarstal"/>
    <field reuseLastValue="0" name="aarstal_int"/>
    <field reuseLastValue="0" name="aktiv_indv"/>
    <field reuseLastValue="0" name="aktiv_indvinding_jn"/>
    <field reuseLastValue="0" name="akvitard_o"/>
    <field reuseLastValue="0" name="akvitard_over_indtag_m"/>
    <field reuseLastValue="0" name="ammoniak_ammonium"/>
    <field reuseLastValue="0" name="analyser_1"/>
    <field reuseLastValue="0" name="analyser_10_aar"/>
    <field reuseLastValue="0" name="anlaeg_id"/>
    <field reuseLastValue="0" name="anlaegs_na"/>
    <field reuseLastValue="0" name="anlaegs_navn"/>
    <field reuseLastValue="0" name="boreholeno"/>
    <field reuseLastValue="0" name="boringsanv"/>
    <field reuseLastValue="0" name="boringsanvendelse"/>
    <field reuseLastValue="0" name="boringsdyb"/>
    <field reuseLastValue="0" name="boringsdybde"/>
    <field reuseLastValue="0" name="boringsfor"/>
    <field reuseLastValue="0" name="boringsformaal"/>
    <field reuseLastValue="0" name="boringskva"/>
    <field reuseLastValue="0" name="boringskvalitet"/>
    <field reuseLastValue="0" name="dato"/>
    <field reuseLastValue="0" name="dato_senes"/>
    <field reuseLastValue="0" name="dato_seneste_analyse"/>
    <field reuseLastValue="0" name="dato_vaerdi"/>
    <field reuseLastValue="0" name="dgu_indtag"/>
    <field reuseLastValue="0" name="dgu_nr"/>
    <field reuseLastValue="0" name="dominerend"/>
    <field reuseLastValue="0" name="dominerende_stof"/>
    <field reuseLastValue="0" name="fid"/>
    <field reuseLastValue="0" name="filterbund_kote"/>
    <field reuseLastValue="0" name="filterbund_mut"/>
    <field reuseLastValue="0" name="filtertop_kote"/>
    <field reuseLastValue="0" name="filtertop_mut"/>
    <field reuseLastValue="0" name="fohm_layer"/>
    <field reuseLastValue="0" name="forvitgrad"/>
    <field reuseLastValue="0" name="graensevae"/>
    <field reuseLastValue="0" name="graensevaerdi_max"/>
    <field reuseLastValue="0" name="guid_intake"/>
    <field reuseLastValue="0" name="hoved_vand"/>
    <field reuseLastValue="0" name="hoved_vandopland"/>
    <field reuseLastValue="0" name="id"/>
    <field reuseLastValue="0" name="indtag_anv"/>
    <field reuseLastValue="0" name="indtag_anvendelse_tekst"/>
    <field reuseLastValue="0" name="indtag_b_1"/>
    <field reuseLastValue="0" name="indtag_bun"/>
    <field reuseLastValue="0" name="indtag_bund_dybde"/>
    <field reuseLastValue="0" name="indtag_bund_dybde_forklaret"/>
    <field reuseLastValue="0" name="indtag_nr"/>
    <field reuseLastValue="0" name="indtag_t_1"/>
    <field reuseLastValue="0" name="indtag_top"/>
    <field reuseLastValue="0" name="indtag_top_dybde"/>
    <field reuseLastValue="0" name="indtag_top_dybde_forklaret"/>
    <field reuseLastValue="0" name="indtags_bj"/>
    <field reuseLastValue="0" name="indtags_bjergart"/>
    <field reuseLastValue="0" name="intakeno"/>
    <field reuseLastValue="0" name="ionbalance"/>
    <field reuseLastValue="0" name="ionexchange"/>
    <field reuseLastValue="0" name="jern"/>
    <field reuseLastValue="0" name="kommune_na"/>
    <field reuseLastValue="0" name="kommune_navn"/>
    <field reuseLastValue="0" name="kommune_nr"/>
    <field reuseLastValue="0" name="lith_in_screen"/>
    <field reuseLastValue="0" name="maengde"/>
    <field reuseLastValue="0" name="magasin_ov"/>
    <field reuseLastValue="0" name="magasin_over_indtag_m"/>
    <field reuseLastValue="0" name="mc_kode"/>
    <field reuseLastValue="0" name="mc_navn"/>
    <field reuseLastValue="0" name="metan"/>
    <field reuseLastValue="0" name="nitrat_int"/>
    <field reuseLastValue="0" name="nitrat_mgprl"/>
    <field reuseLastValue="0" name="nitrat_nitrit_eq"/>
    <field reuseLastValue="0" name="nitrit"/>
    <field reuseLastValue="0" name="overanlaeg"/>
    <field reuseLastValue="0" name="overanlaegs_navn"/>
    <field reuseLastValue="0" name="project"/>
    <field reuseLastValue="0" name="purpose"/>
    <field reuseLastValue="0" name="purpose_int"/>
    <field reuseLastValue="0" name="region_kod"/>
    <field reuseLastValue="0" name="region_kode"/>
    <field reuseLastValue="0" name="region_nav"/>
    <field reuseLastValue="0" name="region_navn"/>
    <field reuseLastValue="0" name="row_id"/>
    <field reuseLastValue="0" name="sampledate"/>
    <field reuseLastValue="0" name="sampleid"/>
    <field reuseLastValue="0" name="status_i_1"/>
    <field reuseLastValue="0" name="status_ind"/>
    <field reuseLastValue="0" name="status_indtag"/>
    <field reuseLastValue="0" name="status_indtag_forklaret"/>
    <field reuseLastValue="0" name="stofgrup_1"/>
    <field reuseLastValue="0" name="stofgruppe"/>
    <field reuseLastValue="0" name="stofgruppe_tekst"/>
    <field reuseLastValue="0" name="stringno"/>
    <field reuseLastValue="0" name="sulfat_int"/>
    <field reuseLastValue="0" name="sulfat_mgprl"/>
    <field reuseLastValue="0" name="sumanoinscalculated"/>
    <field reuseLastValue="0" name="sumcationscalculated"/>
    <field reuseLastValue="0" name="terraen_ko"/>
    <field reuseLastValue="0" name="terraen_kote"/>
    <field reuseLastValue="0" name="udtraeksda"/>
    <field reuseLastValue="0" name="udtraeksdato"/>
    <field reuseLastValue="0" name="ukendt_ove"/>
    <field reuseLastValue="0" name="ukendt_over_indtag_m"/>
    <field reuseLastValue="0" name="unik"/>
    <field reuseLastValue="0" name="url_borera"/>
    <field reuseLastValue="0" name="url_borerapport"/>
    <field reuseLastValue="0" name="url_raadat"/>
    <field reuseLastValue="0" name="url_raadata"/>
    <field reuseLastValue="0" name="url_tidsse"/>
    <field reuseLastValue="0" name="url_tidsserie"/>
    <field reuseLastValue="0" name="use"/>
    <field reuseLastValue="0" name="use_int"/>
    <field reuseLastValue="0" name="virksomh_1"/>
    <field reuseLastValue="0" name="virksomhed"/>
    <field reuseLastValue="0" name="virksomhedstype_kode"/>
    <field reuseLastValue="0" name="virksomhedstype_tekst"/>
    <field reuseLastValue="0" name="xutm32eure"/>
    <field reuseLastValue="0" name="xutm32euref89"/>
    <field reuseLastValue="0" name="yutm32eure"/>
    <field reuseLastValue="0" name="yutm32euref89"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"row_id"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
