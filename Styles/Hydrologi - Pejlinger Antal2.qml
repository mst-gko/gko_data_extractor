<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingHints="0" readOnly="0" simplifyLocal="1" symbologyReferenceScale="-1" simplifyAlgorithm="0" maxScale="0" simplifyMaxScale="1" minScale="100000000" labelsEnabled="1" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" simplifyDrawingTol="1" version="3.26.3-Buenos Aires">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal limitMode="0" startField="" fixedDuration="0" durationField="" durationUnit="min" accumulate="0" enabled="0" endExpression="" startExpression="" mode="0" endField="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation type="IndividualFeatures" extrusionEnabled="0" clamping="Terrain" respectLayerSymbol="1" zoffset="0" zscale="1" symbology="Line" extrusion="0" binding="Centroid" showMarkerSymbolInSurfacePlots="0">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol type="line" name="" force_rhr="0" is_animated="0" alpha="1" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" class="SimpleLine" locked="0">
          <Option type="Map">
            <Option type="QString" name="align_dash_pattern" value="0"/>
            <Option type="QString" name="capstyle" value="square"/>
            <Option type="QString" name="customdash" value="5;2"/>
            <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="customdash_unit" value="MM"/>
            <Option type="QString" name="dash_pattern_offset" value="0"/>
            <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
            <Option type="QString" name="draw_inside_polygon" value="0"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="line_color" value="114,155,111,255"/>
            <Option type="QString" name="line_style" value="solid"/>
            <Option type="QString" name="line_width" value="0.6"/>
            <Option type="QString" name="line_width_unit" value="MM"/>
            <Option type="QString" name="offset" value="0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="ring_filter" value="0"/>
            <Option type="QString" name="trim_distance_end" value="0"/>
            <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_end_unit" value="MM"/>
            <Option type="QString" name="trim_distance_start" value="0"/>
            <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_start_unit" value="MM"/>
            <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
            <Option type="QString" name="use_custom_dash" value="0"/>
            <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="114,155,111,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.6"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol type="fill" name="" force_rhr="0" is_animated="0" alpha="1" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" class="SimpleFill" locked="0">
          <Option type="Map">
            <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="color" value="114,155,111,255"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="81,111,79,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0.2"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="style" value="solid"/>
          </Option>
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="114,155,111,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="81,111,79,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol type="marker" name="" force_rhr="0" is_animated="0" alpha="1" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="114,155,111,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="diamond"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="81,111,79,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0.2"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="3"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="114,155,111,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="81,111,79,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 type="RuleRenderer" referencescale="-1" enableorderby="1" forceraster="0" symbollevels="1">
    <rules key="{10966ace-abce-4ca5-ad15-f2a4e293239e}">
      <rule key="{651c50fb-6b83-477d-a7f7-d886bc74c4ca}" symbol="0" label="1" filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;antal_pejl&quot; = 1"/>
      <rule key="{3d5e1181-f220-49eb-b623-e3d79fdb7aae}" symbol="1" label="2-5" filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;antal_pejl&quot; >= 2.000000 AND &quot;antal_pejl&quot; &lt;= 5"/>
      <rule key="{231cd2f2-eb38-4158-a3a5-831da6dfeb11}" symbol="2" label="6-10" filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;antal_pejl&quot; > 5.500000 AND &quot;antal_pejl&quot; &lt;= 10"/>
      <rule key="{f920179a-4f09-4604-80a8-c7433cb09ac7}" symbol="3" label="11-50" filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;antal_pejl&quot; >= 11 AND &quot;antal_pejl&quot; &lt;= 50"/>
      <rule key="{112c4d80-3a80-40ba-8849-c28031234039}" symbol="4" label="51-100" filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;antal_pejl&quot; > 50.000000 AND &quot;antal_pejl&quot; &lt;= 100.000000"/>
      <rule key="{f1ef449e-75cc-476f-88e6-88de8a5afe01}" symbol="5" label="> 100" filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;antal_pejl&quot; > 100.000000 AND &quot;antal_pejl&quot; &lt;= 1000000000000.000000"/>
    </rules>
    <symbols>
      <symbol type="marker" name="0" force_rhr="0" is_animated="0" alpha="1" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="1" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="0,0,0,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="0,0,0,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" name="1" force_rhr="0" is_animated="0" alpha="1" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="2" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="227,26,28,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="227,26,28,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" name="2" force_rhr="0" is_animated="0" alpha="1" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="3" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="255,127,0,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="255,127,0,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" name="3" force_rhr="0" is_animated="0" alpha="1" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="4" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="255,247,83,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="255,247,83,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" name="4" force_rhr="0" is_animated="0" alpha="1" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="5" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="51,160,44,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="51,160,44,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" name="5" force_rhr="0" is_animated="0" alpha="1" frame_rate="10" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="6" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="42,163,243,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="42,163,243,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <orderby>
      <orderByClause asc="1" nullsFirst="0">"antal"</orderByClause>
    </orderby>
  </renderer-v2>
  <labeling type="rule-based">
    <rules key="{49894254-d5c9-49db-9fad-8360a66c1f2c}">
      <rule key="{ceaf07b9-f682-4aa8-8969-c3a1dbecd3bf}" active="0" filter="&quot;antal_pejlinger&quot; > 1">
        <settings calloutType="simple">
          <text-style fieldName="boreholeno" fontStrikeout="0" legendString="Aa" fontSizeUnit="Point" fontKerning="1" fontItalic="0" blendMode="0" capitalization="0" forcedItalic="0" fontSize="10" namedStyle="normal" fontUnderline="0" fontFamily="Arial" fontWeight="50" previewBkgrdColor="255,255,255,255" textColor="50,50,50,255" fontWordSpacing="0" isExpression="0" multilineHeight="1" textOpacity="1" textOrientation="horizontal" allowHtml="0" useSubstitutions="0" fontLetterSpacing="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" forcedBold="0">
            <families/>
            <text-buffer bufferJoinStyle="128" bufferSizeUnits="MM" bufferOpacity="1" bufferColor="250,250,250,255" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferNoFill="1" bufferDraw="0" bufferSize="1" bufferBlendMode="0"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskedSymbolLayers="" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskJoinStyle="128" maskOpacity="1"/>
            <background shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRotationType="0" shapeOffsetX="0" shapeBorderColor="128,128,128,255" shapeSizeUnit="Point" shapeRadiiX="0" shapeType="0" shapeRadiiY="0" shapeSizeType="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeJoinStyle="64" shapeOpacity="1" shapeOffsetY="0" shapeBlendMode="0" shapeOffsetUnit="Point" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRotation="0" shapeSVGFile="" shapeRadiiUnit="Point" shapeBorderWidthUnit="Point" shapeSizeY="0" shapeDraw="0" shapeSizeX="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0">
              <symbol type="marker" name="markerSymbol" force_rhr="0" is_animated="0" alpha="1" frame_rate="10" clip_to_extent="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" pass="0" class="SimpleMarker" locked="0">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="114,155,111,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <prop k="angle" v="0"/>
                  <prop k="cap_style" v="square"/>
                  <prop k="color" v="114,155,111,255"/>
                  <prop k="horizontal_anchor_point" v="1"/>
                  <prop k="joinstyle" v="bevel"/>
                  <prop k="name" v="circle"/>
                  <prop k="offset" v="0,0"/>
                  <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
                  <prop k="offset_unit" v="MM"/>
                  <prop k="outline_color" v="35,35,35,255"/>
                  <prop k="outline_style" v="solid"/>
                  <prop k="outline_width" v="0"/>
                  <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
                  <prop k="outline_width_unit" v="MM"/>
                  <prop k="scale_method" v="diameter"/>
                  <prop k="size" v="2"/>
                  <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
                  <prop k="size_unit" v="MM"/>
                  <prop k="vertical_anchor_point" v="1"/>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol type="fill" name="fillSymbol" force_rhr="0" is_animated="0" alpha="1" frame_rate="10" clip_to_extent="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" pass="0" class="SimpleFill" locked="0">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="128,128,128,255"/>
                    <Option type="QString" name="outline_style" value="no"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_unit" value="Point"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
                  <prop k="color" v="255,255,255,255"/>
                  <prop k="joinstyle" v="bevel"/>
                  <prop k="offset" v="0,0"/>
                  <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
                  <prop k="offset_unit" v="MM"/>
                  <prop k="outline_color" v="128,128,128,255"/>
                  <prop k="outline_style" v="no"/>
                  <prop k="outline_width" v="0"/>
                  <prop k="outline_width_unit" v="Point"/>
                  <prop k="style" v="solid"/>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowColor="0,0,0,255" shadowOffsetUnit="MM" shadowOffsetAngle="135" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetGlobal="1" shadowOffsetDist="1" shadowRadius="1.5" shadowUnder="0" shadowRadiusAlphaOnly="0" shadowScale="100" shadowRadiusUnit="MM" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOpacity="0.69999999999999996" shadowBlendMode="6"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format rightDirectionSymbol=">" formatNumbers="0" multilineAlign="3" plussign="0" placeDirectionSymbol="0" decimals="3" useMaxLineLengthForAutoWrap="1" addDirectionSymbol="0" leftDirectionSymbol="&lt;" wrapChar="" autoWrapLength="0" reverseDirectionSymbol="0"/>
          <placement maxCurvedCharAngleOut="-25" repeatDistanceUnits="MM" geometryGeneratorType="PointGeometry" overlapHandling="PreventOverlap" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" placementFlags="10" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" placement="6" xOffset="0" repeatDistance="0" distMapUnitScale="3x:0,0,0,0,0,0" fitInPolygonOnly="0" allowDegraded="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" lineAnchorTextPoint="FollowPlacement" rotationAngle="0" overrunDistance="0" lineAnchorType="0" geometryGeneratorEnabled="0" offsetType="1" yOffset="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" rotationUnit="AngleDegrees" distUnits="MM" layerType="PointGeometry" polygonPlacementFlags="2" centroidInside="0" lineAnchorClipping="0" quadOffset="4" dist="0" offsetUnits="MM" centroidWhole="0" priority="5" lineAnchorPercent="0.5" maxCurvedCharAngleIn="25" geometryGenerator="" preserveRotation="1"/>
          <rendering obstacle="1" mergeLines="0" labelPerPart="0" unplacedVisibility="0" upsidedownLabels="0" fontLimitPixelSize="0" obstacleType="1" drawLabels="1" maxNumLabels="2000" scaleMin="0" minFeatureSize="0" obstacleFactor="1" fontMaxPixelSize="10000" scaleVisibility="0" scaleMax="0" limitNumLabels="0" fontMinPixelSize="3" zIndex="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="PositionX">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="auxiliary_storage_labeling_positionx"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
                <Option type="Map" name="PositionY">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="auxiliary_storage_labeling_positiony"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="bool" name="drawToAllParts" value="false"/>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="lineSymbol" value="&lt;symbol type=&quot;line&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; frame_rate=&quot;10&quot; clip_to_extent=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer enabled=&quot;1&quot; pass=&quot;0&quot; class=&quot;SimpleLine&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;align_dash_pattern&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;capstyle&quot; value=&quot;square&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;customdash&quot; value=&quot;5;2&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;customdash_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;customdash_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;dash_pattern_offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;dash_pattern_offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;dash_pattern_offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;draw_inside_polygon&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;line_color&quot; value=&quot;60,60,60,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;line_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;line_width&quot; value=&quot;0.3&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;line_width_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_end&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_end_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_end_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_start&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_start_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_start_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;tweak_dash_pattern_on_corners&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;use_custom_dash&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;/Option>&lt;prop k=&quot;align_dash_pattern&quot; v=&quot;0&quot;/>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;dash_pattern_offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;dash_pattern_offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;dash_pattern_offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;trim_distance_end&quot; v=&quot;0&quot;/>&lt;prop k=&quot;trim_distance_end_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;trim_distance_end_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;trim_distance_start&quot; v=&quot;0&quot;/>&lt;prop k=&quot;trim_distance_start_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;trim_distance_start_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;tweak_dash_pattern_on_corners&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>"/>
              <Option type="double" name="minLength" value="0"/>
              <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="minLengthUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="offsetFromLabel" value="0"/>
              <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
    </rules>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="&quot;row_id&quot;"/>
      </Option>
      <Option type="QString" name="embeddedWidgets/count" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory spacingUnitScale="3x:0,0,0,0,0,0" height="15" lineSizeType="MM" opacity="1" spacing="5" scaleBasedVisibility="0" sizeType="MM" barWidth="5" diagramOrientation="Up" enabled="0" maxScaleDenominator="1e+08" minimumSize="0" backgroundColor="#ffffff" scaleDependency="Area" penWidth="0" rotationOffset="270" spacingUnit="MM" minScaleDenominator="0" penColor="#000000" width="15" penAlpha="255" direction="0" lineSizeScale="3x:0,0,0,0,0,0" showAxis="1" sizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" backgroundAlpha="255">
      <fontProperties style="" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" bold="0" italic="0" strikethrough="0"/>
      <attribute colorOpacity="1" label="" field="" color="#000000"/>
      <axisSymbol>
        <symbol type="line" name="" force_rhr="0" is_animated="0" alpha="1" frame_rate="10" clip_to_extent="1">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer enabled="1" pass="0" class="SimpleLine" locked="0">
            <Option type="Map">
              <Option type="QString" name="align_dash_pattern" value="0"/>
              <Option type="QString" name="capstyle" value="square"/>
              <Option type="QString" name="customdash" value="5;2"/>
              <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="customdash_unit" value="MM"/>
              <Option type="QString" name="dash_pattern_offset" value="0"/>
              <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
              <Option type="QString" name="draw_inside_polygon" value="0"/>
              <Option type="QString" name="joinstyle" value="bevel"/>
              <Option type="QString" name="line_color" value="35,35,35,255"/>
              <Option type="QString" name="line_style" value="solid"/>
              <Option type="QString" name="line_width" value="0.26"/>
              <Option type="QString" name="line_width_unit" value="MM"/>
              <Option type="QString" name="offset" value="0"/>
              <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offset_unit" value="MM"/>
              <Option type="QString" name="ring_filter" value="0"/>
              <Option type="QString" name="trim_distance_end" value="0"/>
              <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_end_unit" value="MM"/>
              <Option type="QString" name="trim_distance_start" value="0"/>
              <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_start_unit" value="MM"/>
              <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
              <Option type="QString" name="use_custom_dash" value="0"/>
              <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings showAll="1" obstacle="0" placement="0" linePlacementFlags="18" zIndex="0" priority="0" dist="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="rownumber" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="antal_pejl" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevelid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boreholeid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="intakeid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boreholeno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevelno" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="intakeno" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="timeofmeas" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="project" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="waterlevel" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevgrsu" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevmsl" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevmp" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="hoursnopum" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="category" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="method" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="quality" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="refpoint" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="remark" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="verticaref" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="atmospress" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="extremes" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="situation" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevelro" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="qualitycon" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="guid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="guid_intak" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="guid_watle" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="insertdate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="updatedate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="insertuser" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="updateuser" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dataowner" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locatmetho" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="screen_top" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="screen_bot" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="rownumber"/>
    <alias name="" index="1" field="antal_pejl"/>
    <alias name="" index="2" field="watlevelid"/>
    <alias name="" index="3" field="boreholeid"/>
    <alias name="" index="4" field="intakeid"/>
    <alias name="" index="5" field="boreholeno"/>
    <alias name="" index="6" field="watlevelno"/>
    <alias name="" index="7" field="intakeno"/>
    <alias name="" index="8" field="timeofmeas"/>
    <alias name="" index="9" field="project"/>
    <alias name="" index="10" field="waterlevel"/>
    <alias name="" index="11" field="watlevgrsu"/>
    <alias name="" index="12" field="watlevmsl"/>
    <alias name="" index="13" field="watlevmp"/>
    <alias name="" index="14" field="hoursnopum"/>
    <alias name="" index="15" field="category"/>
    <alias name="" index="16" field="method"/>
    <alias name="" index="17" field="quality"/>
    <alias name="" index="18" field="refpoint"/>
    <alias name="" index="19" field="remark"/>
    <alias name="" index="20" field="verticaref"/>
    <alias name="" index="21" field="atmospress"/>
    <alias name="" index="22" field="extremes"/>
    <alias name="" index="23" field="situation"/>
    <alias name="" index="24" field="watlevelro"/>
    <alias name="" index="25" field="qualitycon"/>
    <alias name="" index="26" field="guid"/>
    <alias name="" index="27" field="guid_intak"/>
    <alias name="" index="28" field="guid_watle"/>
    <alias name="" index="29" field="insertdate"/>
    <alias name="" index="30" field="updatedate"/>
    <alias name="" index="31" field="insertuser"/>
    <alias name="" index="32" field="updateuser"/>
    <alias name="" index="33" field="dataowner"/>
    <alias name="" index="34" field="locatmetho"/>
    <alias name="" index="35" field="screen_top"/>
    <alias name="" index="36" field="screen_bot"/>
  </aliases>
  <defaults>
    <default field="rownumber" expression="" applyOnUpdate="0"/>
    <default field="antal_pejl" expression="" applyOnUpdate="0"/>
    <default field="watlevelid" expression="" applyOnUpdate="0"/>
    <default field="boreholeid" expression="" applyOnUpdate="0"/>
    <default field="intakeid" expression="" applyOnUpdate="0"/>
    <default field="boreholeno" expression="" applyOnUpdate="0"/>
    <default field="watlevelno" expression="" applyOnUpdate="0"/>
    <default field="intakeno" expression="" applyOnUpdate="0"/>
    <default field="timeofmeas" expression="" applyOnUpdate="0"/>
    <default field="project" expression="" applyOnUpdate="0"/>
    <default field="waterlevel" expression="" applyOnUpdate="0"/>
    <default field="watlevgrsu" expression="" applyOnUpdate="0"/>
    <default field="watlevmsl" expression="" applyOnUpdate="0"/>
    <default field="watlevmp" expression="" applyOnUpdate="0"/>
    <default field="hoursnopum" expression="" applyOnUpdate="0"/>
    <default field="category" expression="" applyOnUpdate="0"/>
    <default field="method" expression="" applyOnUpdate="0"/>
    <default field="quality" expression="" applyOnUpdate="0"/>
    <default field="refpoint" expression="" applyOnUpdate="0"/>
    <default field="remark" expression="" applyOnUpdate="0"/>
    <default field="verticaref" expression="" applyOnUpdate="0"/>
    <default field="atmospress" expression="" applyOnUpdate="0"/>
    <default field="extremes" expression="" applyOnUpdate="0"/>
    <default field="situation" expression="" applyOnUpdate="0"/>
    <default field="watlevelro" expression="" applyOnUpdate="0"/>
    <default field="qualitycon" expression="" applyOnUpdate="0"/>
    <default field="guid" expression="" applyOnUpdate="0"/>
    <default field="guid_intak" expression="" applyOnUpdate="0"/>
    <default field="guid_watle" expression="" applyOnUpdate="0"/>
    <default field="insertdate" expression="" applyOnUpdate="0"/>
    <default field="updatedate" expression="" applyOnUpdate="0"/>
    <default field="insertuser" expression="" applyOnUpdate="0"/>
    <default field="updateuser" expression="" applyOnUpdate="0"/>
    <default field="dataowner" expression="" applyOnUpdate="0"/>
    <default field="locatmetho" expression="" applyOnUpdate="0"/>
    <default field="screen_top" expression="" applyOnUpdate="0"/>
    <default field="screen_bot" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="3" notnull_strength="1" exp_strength="0" field="rownumber" unique_strength="1"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="antal_pejl" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="watlevelid" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="boreholeid" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="intakeid" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="boreholeno" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="watlevelno" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="intakeno" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="timeofmeas" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="project" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="waterlevel" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="watlevgrsu" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="watlevmsl" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="watlevmp" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="hoursnopum" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="category" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="method" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="quality" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="refpoint" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="remark" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="verticaref" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="atmospress" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="extremes" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="situation" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="watlevelro" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="qualitycon" unique_strength="0"/>
    <constraint constraints="3" notnull_strength="1" exp_strength="0" field="guid" unique_strength="1"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="guid_intak" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="guid_watle" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="insertdate" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="updatedate" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="insertuser" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="updateuser" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="dataowner" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="locatmetho" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="screen_top" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="screen_bot" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="rownumber"/>
    <constraint exp="" desc="" field="antal_pejl"/>
    <constraint exp="" desc="" field="watlevelid"/>
    <constraint exp="" desc="" field="boreholeid"/>
    <constraint exp="" desc="" field="intakeid"/>
    <constraint exp="" desc="" field="boreholeno"/>
    <constraint exp="" desc="" field="watlevelno"/>
    <constraint exp="" desc="" field="intakeno"/>
    <constraint exp="" desc="" field="timeofmeas"/>
    <constraint exp="" desc="" field="project"/>
    <constraint exp="" desc="" field="waterlevel"/>
    <constraint exp="" desc="" field="watlevgrsu"/>
    <constraint exp="" desc="" field="watlevmsl"/>
    <constraint exp="" desc="" field="watlevmp"/>
    <constraint exp="" desc="" field="hoursnopum"/>
    <constraint exp="" desc="" field="category"/>
    <constraint exp="" desc="" field="method"/>
    <constraint exp="" desc="" field="quality"/>
    <constraint exp="" desc="" field="refpoint"/>
    <constraint exp="" desc="" field="remark"/>
    <constraint exp="" desc="" field="verticaref"/>
    <constraint exp="" desc="" field="atmospress"/>
    <constraint exp="" desc="" field="extremes"/>
    <constraint exp="" desc="" field="situation"/>
    <constraint exp="" desc="" field="watlevelro"/>
    <constraint exp="" desc="" field="qualitycon"/>
    <constraint exp="" desc="" field="guid"/>
    <constraint exp="" desc="" field="guid_intak"/>
    <constraint exp="" desc="" field="guid_watle"/>
    <constraint exp="" desc="" field="insertdate"/>
    <constraint exp="" desc="" field="updatedate"/>
    <constraint exp="" desc="" field="insertuser"/>
    <constraint exp="" desc="" field="updateuser"/>
    <constraint exp="" desc="" field="dataowner"/>
    <constraint exp="" desc="" field="locatmetho"/>
    <constraint exp="" desc="" field="screen_top"/>
    <constraint exp="" desc="" field="screen_bot"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="&quot;antal_pejlinger&quot;" sortOrder="1">
    <columns>
      <column type="field" name="boreholeno" width="-1" hidden="0"/>
      <column type="field" name="intakeno" width="-1" hidden="0"/>
      <column type="field" name="rownumber" width="-1" hidden="0"/>
      <column type="field" name="watlevelid" width="-1" hidden="0"/>
      <column type="field" name="boreholeid" width="-1" hidden="0"/>
      <column type="field" name="intakeid" width="-1" hidden="0"/>
      <column type="field" name="watlevelno" width="-1" hidden="0"/>
      <column type="field" name="timeofmeas" width="-1" hidden="0"/>
      <column type="field" name="project" width="-1" hidden="0"/>
      <column type="field" name="waterlevel" width="-1" hidden="0"/>
      <column type="field" name="watlevgrsu" width="-1" hidden="0"/>
      <column type="field" name="watlevmsl" width="-1" hidden="0"/>
      <column type="field" name="watlevmp" width="-1" hidden="0"/>
      <column type="field" name="hoursnopum" width="-1" hidden="0"/>
      <column type="field" name="category" width="-1" hidden="0"/>
      <column type="field" name="method" width="-1" hidden="0"/>
      <column type="field" name="quality" width="-1" hidden="0"/>
      <column type="field" name="refpoint" width="-1" hidden="0"/>
      <column type="field" name="remark" width="-1" hidden="0"/>
      <column type="field" name="verticaref" width="-1" hidden="0"/>
      <column type="field" name="extremes" width="-1" hidden="0"/>
      <column type="field" name="situation" width="-1" hidden="0"/>
      <column type="field" name="guid" width="-1" hidden="0"/>
      <column type="field" name="insertdate" width="-1" hidden="0"/>
      <column type="field" name="updatedate" width="-1" hidden="0"/>
      <column type="field" name="insertuser" width="-1" hidden="0"/>
      <column type="field" name="updateuser" width="-1" hidden="0"/>
      <column type="field" name="dataowner" width="-1" hidden="0"/>
      <column type="field" name="locatmetho" width="-1" hidden="0"/>
      <column type="field" name="screen_top" width="-1" hidden="0"/>
      <column type="field" name="antal_pejl" width="-1" hidden="0"/>
      <column type="field" name="atmospress" width="-1" hidden="0"/>
      <column type="field" name="watlevelro" width="-1" hidden="0"/>
      <column type="field" name="qualitycon" width="-1" hidden="0"/>
      <column type="field" name="guid_intak" width="-1" hidden="0"/>
      <column type="field" name="guid_watle" width="-1" hidden="0"/>
      <column type="field" name="screen_bot" width="-1" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="antal" editable="1"/>
    <field name="antal_pejl" editable="1"/>
    <field name="antal_pejlinger" editable="1"/>
    <field name="atmospress" editable="1"/>
    <field name="atmospresshpa" editable="1"/>
    <field name="auxiliary_storage_labeling_positionx" editable="0"/>
    <field name="auxiliary_storage_labeling_positiony" editable="0"/>
    <field name="borehole_1" editable="1"/>
    <field name="borehole_locatmetho" editable="0"/>
    <field name="boreholeid" editable="1"/>
    <field name="boreholeno" editable="1"/>
    <field name="category" editable="1"/>
    <field name="dataowner" editable="1"/>
    <field name="extremes" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="guid" editable="1"/>
    <field name="guid_intak" editable="1"/>
    <field name="guid_intake" editable="1"/>
    <field name="guid_watle" editable="1"/>
    <field name="guid_watlevround" editable="1"/>
    <field name="hoursnopum" editable="1"/>
    <field name="insertdate" editable="1"/>
    <field name="insertuser" editable="1"/>
    <field name="intakeid" editable="1"/>
    <field name="intakeno" editable="1"/>
    <field name="intakeno_2" editable="1"/>
    <field name="locatmetho" editable="1"/>
    <field name="method" editable="1"/>
    <field name="pejledato" editable="1"/>
    <field name="pejledato_" editable="1"/>
    <field name="project" editable="1"/>
    <field name="quality" editable="1"/>
    <field name="qualitycon" editable="1"/>
    <field name="qualitycontrol" editable="1"/>
    <field name="refpoint" editable="1"/>
    <field name="remark" editable="1"/>
    <field name="row_id" editable="1"/>
    <field name="row_id_2" editable="1"/>
    <field name="rownumber" editable="1"/>
    <field name="screen_bot" editable="1"/>
    <field name="screen_bottom" editable="0"/>
    <field name="screen_top" editable="0"/>
    <field name="screen_topbotquali" editable="0"/>
    <field name="situation" editable="1"/>
    <field name="timeofmeas" editable="1"/>
    <field name="updatedate" editable="1"/>
    <field name="updateuser" editable="1"/>
    <field name="vandst_m_1" editable="1"/>
    <field name="vandst_mut" editable="1"/>
    <field name="vandstko_1" editable="1"/>
    <field name="vandstkote" editable="1"/>
    <field name="verticaref" editable="1"/>
    <field name="waterlevel" editable="1"/>
    <field name="watlevel_situation" editable="0"/>
    <field name="watlevel_timeofmeas" editable="0"/>
    <field name="watlevel_watlevmsl" editable="0"/>
    <field name="watlevelid" editable="1"/>
    <field name="watlevelno" editable="1"/>
    <field name="watlevelro" editable="1"/>
    <field name="watlevelroundno" editable="1"/>
    <field name="watlevgrsu" editable="1"/>
    <field name="watlevmp" editable="1"/>
    <field name="watlevmsl" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="antal" labelOnTop="0"/>
    <field name="antal_pejl" labelOnTop="0"/>
    <field name="antal_pejlinger" labelOnTop="0"/>
    <field name="atmospress" labelOnTop="0"/>
    <field name="atmospresshpa" labelOnTop="0"/>
    <field name="auxiliary_storage_labeling_positionx" labelOnTop="0"/>
    <field name="auxiliary_storage_labeling_positiony" labelOnTop="0"/>
    <field name="borehole_1" labelOnTop="0"/>
    <field name="borehole_locatmetho" labelOnTop="0"/>
    <field name="boreholeid" labelOnTop="0"/>
    <field name="boreholeno" labelOnTop="0"/>
    <field name="category" labelOnTop="0"/>
    <field name="dataowner" labelOnTop="0"/>
    <field name="extremes" labelOnTop="0"/>
    <field name="fid" labelOnTop="0"/>
    <field name="guid" labelOnTop="0"/>
    <field name="guid_intak" labelOnTop="0"/>
    <field name="guid_intake" labelOnTop="0"/>
    <field name="guid_watle" labelOnTop="0"/>
    <field name="guid_watlevround" labelOnTop="0"/>
    <field name="hoursnopum" labelOnTop="0"/>
    <field name="insertdate" labelOnTop="0"/>
    <field name="insertuser" labelOnTop="0"/>
    <field name="intakeid" labelOnTop="0"/>
    <field name="intakeno" labelOnTop="0"/>
    <field name="intakeno_2" labelOnTop="0"/>
    <field name="locatmetho" labelOnTop="0"/>
    <field name="method" labelOnTop="0"/>
    <field name="pejledato" labelOnTop="0"/>
    <field name="pejledato_" labelOnTop="0"/>
    <field name="project" labelOnTop="0"/>
    <field name="quality" labelOnTop="0"/>
    <field name="qualitycon" labelOnTop="0"/>
    <field name="qualitycontrol" labelOnTop="0"/>
    <field name="refpoint" labelOnTop="0"/>
    <field name="remark" labelOnTop="0"/>
    <field name="row_id" labelOnTop="0"/>
    <field name="row_id_2" labelOnTop="0"/>
    <field name="rownumber" labelOnTop="0"/>
    <field name="screen_bot" labelOnTop="0"/>
    <field name="screen_bottom" labelOnTop="0"/>
    <field name="screen_top" labelOnTop="0"/>
    <field name="screen_topbotquali" labelOnTop="0"/>
    <field name="situation" labelOnTop="0"/>
    <field name="timeofmeas" labelOnTop="0"/>
    <field name="updatedate" labelOnTop="0"/>
    <field name="updateuser" labelOnTop="0"/>
    <field name="vandst_m_1" labelOnTop="0"/>
    <field name="vandst_mut" labelOnTop="0"/>
    <field name="vandstko_1" labelOnTop="0"/>
    <field name="vandstkote" labelOnTop="0"/>
    <field name="verticaref" labelOnTop="0"/>
    <field name="waterlevel" labelOnTop="0"/>
    <field name="watlevel_situation" labelOnTop="0"/>
    <field name="watlevel_timeofmeas" labelOnTop="0"/>
    <field name="watlevel_watlevmsl" labelOnTop="0"/>
    <field name="watlevelid" labelOnTop="0"/>
    <field name="watlevelno" labelOnTop="0"/>
    <field name="watlevelro" labelOnTop="0"/>
    <field name="watlevelroundno" labelOnTop="0"/>
    <field name="watlevgrsu" labelOnTop="0"/>
    <field name="watlevmp" labelOnTop="0"/>
    <field name="watlevmsl" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="antal" reuseLastValue="0"/>
    <field name="antal_pejl" reuseLastValue="0"/>
    <field name="antal_pejlinger" reuseLastValue="0"/>
    <field name="atmospress" reuseLastValue="0"/>
    <field name="atmospresshpa" reuseLastValue="0"/>
    <field name="auxiliary_storage_labeling_positionx" reuseLastValue="0"/>
    <field name="auxiliary_storage_labeling_positiony" reuseLastValue="0"/>
    <field name="borehole_locatmetho" reuseLastValue="0"/>
    <field name="boreholeid" reuseLastValue="0"/>
    <field name="boreholeno" reuseLastValue="0"/>
    <field name="category" reuseLastValue="0"/>
    <field name="dataowner" reuseLastValue="0"/>
    <field name="extremes" reuseLastValue="0"/>
    <field name="fid" reuseLastValue="0"/>
    <field name="guid" reuseLastValue="0"/>
    <field name="guid_intak" reuseLastValue="0"/>
    <field name="guid_intake" reuseLastValue="0"/>
    <field name="guid_watle" reuseLastValue="0"/>
    <field name="guid_watlevround" reuseLastValue="0"/>
    <field name="hoursnopum" reuseLastValue="0"/>
    <field name="insertdate" reuseLastValue="0"/>
    <field name="insertuser" reuseLastValue="0"/>
    <field name="intakeid" reuseLastValue="0"/>
    <field name="intakeno" reuseLastValue="0"/>
    <field name="locatmetho" reuseLastValue="0"/>
    <field name="method" reuseLastValue="0"/>
    <field name="project" reuseLastValue="0"/>
    <field name="quality" reuseLastValue="0"/>
    <field name="qualitycon" reuseLastValue="0"/>
    <field name="qualitycontrol" reuseLastValue="0"/>
    <field name="refpoint" reuseLastValue="0"/>
    <field name="remark" reuseLastValue="0"/>
    <field name="row_id" reuseLastValue="0"/>
    <field name="rownumber" reuseLastValue="0"/>
    <field name="screen_bot" reuseLastValue="0"/>
    <field name="screen_bottom" reuseLastValue="0"/>
    <field name="screen_top" reuseLastValue="0"/>
    <field name="screen_topbotquali" reuseLastValue="0"/>
    <field name="situation" reuseLastValue="0"/>
    <field name="timeofmeas" reuseLastValue="0"/>
    <field name="updatedate" reuseLastValue="0"/>
    <field name="updateuser" reuseLastValue="0"/>
    <field name="verticaref" reuseLastValue="0"/>
    <field name="waterlevel" reuseLastValue="0"/>
    <field name="watlevel_situation" reuseLastValue="0"/>
    <field name="watlevel_timeofmeas" reuseLastValue="0"/>
    <field name="watlevel_watlevmsl" reuseLastValue="0"/>
    <field name="watlevelid" reuseLastValue="0"/>
    <field name="watlevelno" reuseLastValue="0"/>
    <field name="watlevelro" reuseLastValue="0"/>
    <field name="watlevelroundno" reuseLastValue="0"/>
    <field name="watlevgrsu" reuseLastValue="0"/>
    <field name="watlevmp" reuseLastValue="0"/>
    <field name="watlevmsl" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"row_id"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
