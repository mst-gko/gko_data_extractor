<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis labelsEnabled="0" maxScale="0" simplifyMaxScale="1" styleCategories="AllStyleCategories" symbologyReferenceScale="-1" hasScaleBasedVisibilityFlag="0" simplifyAlgorithm="0" simplifyDrawingTol="1" readOnly="0" version="3.26.3-Buenos Aires" simplifyLocal="1" minScale="100000000" simplifyDrawingHints="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endExpression="" limitMode="0" accumulate="0" endField="" mode="0" fixedDuration="0" startExpression="" durationUnit="min" enabled="0" startField="" durationField="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zscale="1" extrusionEnabled="0" clamping="Terrain" symbology="Line" respectLayerSymbol="1" showMarkerSymbolInSurfacePlots="0" extrusion="0" binding="Centroid" type="IndividualFeatures" zoffset="0">
    <data-defined-properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="" type="line" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleLine" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="align_dash_pattern" type="QString"/>
            <Option value="square" name="capstyle" type="QString"/>
            <Option value="5;2" name="customdash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
            <Option value="MM" name="customdash_unit" type="QString"/>
            <Option value="0" name="dash_pattern_offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
            <Option value="0" name="draw_inside_polygon" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="225,89,137,255" name="line_color" type="QString"/>
            <Option value="solid" name="line_style" type="QString"/>
            <Option value="0.6" name="line_width" type="QString"/>
            <Option value="MM" name="line_width_unit" type="QString"/>
            <Option value="0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="0" name="ring_filter" type="QString"/>
            <Option value="0" name="trim_distance_end" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_end_unit" type="QString"/>
            <Option value="0" name="trim_distance_start" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_start_unit" type="QString"/>
            <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
            <Option value="0" name="use_custom_dash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="225,89,137,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.6"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="" type="fill" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleFill" enabled="1" pass="0">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale" type="QString"/>
            <Option value="225,89,137,255" name="color" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="161,64,98,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.2" name="outline_width" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="solid" name="style" type="QString"/>
          </Option>
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="225,89,137,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="161,64,98,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="225,89,137,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="diamond" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="161,64,98,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.2" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="225,89,137,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="161,64,98,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 symbollevels="0" referencescale="-1" enableorderby="0" type="RuleRenderer" forceraster="0">
    <rules key="{fd1a8f7b-c10f-4569-9437-0b47b41e0351}">
      <rule label="Anden erhvervsvirksomhed" symbol="0" key="{8de2f60a-f19c-4931-a2b8-129be195cddd}" filter="&quot;virksomhedstype&quot; = 'Anden erhvervsvirksomhed'"/>
      <rule label="Gartneri" symbol="1" key="{49228f5f-4a43-4577-aca8-dc60fb7e5f72}" filter="&quot;virksomhedstype&quot; = 'Gartneri'"/>
      <rule label="Grusvask" symbol="2" key="{92173e65-ae05-4242-b348-1fdd4e3c18ec}" filter="&quot;virksomhedstype&quot; = 'Grusvask'"/>
      <rule label="Havevanding" symbol="3" key="{c7f0e186-dafe-438c-a87d-e9554340f6dc}" filter="&quot;virksomhedstype&quot; = 'Havevanding'"/>
      <rule label="Hotel, Campingplads o.lign." symbol="4" key="{978971c8-481e-419d-abda-5d29d9130d85}" filter="&quot;virksomhedstype&quot; = 'Hotel, Campingplads o.lign.'"/>
      <rule label="Husdyrfarm" symbol="5" key="{2686a997-149c-4bec-844b-84130789d347}" filter="&quot;virksomhedstype&quot; = 'Husdyrfarm'"/>
      <rule label="Husholdning, én husstand" symbol="6" key="{9788bebd-b678-4145-b41f-2ec8f787d604}" filter="&quot;virksomhedstype&quot; = 'Husholdning, én husstand'"/>
      <rule label="Husholdninger, flere husstande" symbol="7" key="{9c119fb7-3665-4e1f-8130-96fef4c90fed}" filter="&quot;virksomhedstype&quot; = 'Husholdninger, flere husstande'"/>
      <rule label="Levnedsmiddelindustri" symbol="8" key="{b2422764-1be6-452c-a6f7-141445048bb6}" filter="&quot;virksomhedstype&quot; = 'Levnedsmiddelindustri'"/>
      <rule label="Markvanding" symbol="9" key="{659585c1-37dc-4b91-929f-e060778bbb0d}" filter="&quot;virksomhedstype&quot; = 'Markvanding'"/>
      <rule label="Mælkeleverandør" symbol="10" key="{7dd6bf0a-2b15-4cd2-be38-80bba5599bf5}" filter="&quot;virksomhedstype&quot; = 'Mælkeleverandør'"/>
      <rule label="Sportsplads, park o.lign." symbol="11" key="{e7d238b0-9390-4837-9959-e92a5bb202d5}" filter="&quot;virksomhedstype&quot; = 'Sportsplads, park o.lign.'"/>
    </rules>
    <symbols>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="0" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="112,204,211,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="112,204,211,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="1" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="106,27,203,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="106,27,203,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="10" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="216,174,48,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="216,174,48,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="11" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="255,62,85,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="255,62,85,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="2" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="127,167,231,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="127,167,231,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="3" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="47,229,174,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="47,229,174,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="4" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="199,101,210,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="199,101,210,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="5" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="255,170,32,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="255,170,32,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="6" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="237,50,184,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="237,50,184,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="7" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="213,33,213,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="213,33,213,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="8" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="219,250,65,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="219,250,65,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" alpha="1" name="9" type="marker" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="19,230,33,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="19,230,33,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option value="0" name="embeddedWidgets/count" type="QString"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory maxScaleDenominator="1e+08" sizeType="MM" penWidth="0" spacingUnitScale="3x:0,0,0,0,0,0" rotationOffset="270" height="15" enabled="0" labelPlacementMethod="XHeight" spacing="0" opacity="1" scaleBasedVisibility="0" diagramOrientation="Up" penAlpha="255" sizeScale="3x:0,0,0,0,0,0" spacingUnit="MM" width="15" direction="1" backgroundAlpha="255" scaleDependency="Area" barWidth="5" minimumSize="0" showAxis="0" minScaleDenominator="0" penColor="#000000" lineSizeType="MM" backgroundColor="#ffffff" lineSizeScale="3x:0,0,0,0,0,0">
      <fontProperties strikethrough="0" bold="0" italic="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0"/>
      <attribute label="" color="#000000" field="" colorOpacity="1"/>
      <axisSymbol>
        <symbol frame_rate="10" is_animated="0" alpha="1" name="" type="line" clip_to_extent="1" force_rhr="0">
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
          <layer locked="0" class="SimpleLine" enabled="1" pass="0">
            <Option type="Map">
              <Option value="0" name="align_dash_pattern" type="QString"/>
              <Option value="square" name="capstyle" type="QString"/>
              <Option value="5;2" name="customdash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
              <Option value="MM" name="customdash_unit" type="QString"/>
              <Option value="0" name="dash_pattern_offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
              <Option value="0" name="draw_inside_polygon" type="QString"/>
              <Option value="bevel" name="joinstyle" type="QString"/>
              <Option value="35,35,35,255" name="line_color" type="QString"/>
              <Option value="solid" name="line_style" type="QString"/>
              <Option value="0.26" name="line_width" type="QString"/>
              <Option value="MM" name="line_width_unit" type="QString"/>
              <Option value="0" name="offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="offset_unit" type="QString"/>
              <Option value="0" name="ring_filter" type="QString"/>
              <Option value="0" name="trim_distance_end" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_end_unit" type="QString"/>
              <Option value="0" name="trim_distance_start" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_start_unit" type="QString"/>
              <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
              <Option value="0" name="use_custom_dash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" priority="0" zIndex="0" showAll="1" placement="0" dist="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anlaegsid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="supplant" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="kommunenr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anlaegnavn" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anlaegadr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anlaegpostnr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dataejer" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="aktiv" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="virksomhedstype" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anlaegstype" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="indvindingsformaal" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="vandtype" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="url_plant" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="kommentar" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="amount" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="startdate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="enddate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="id" index="0"/>
    <alias name="" field="anlaegsid" index="1"/>
    <alias name="" field="supplant" index="2"/>
    <alias name="" field="kommunenr" index="3"/>
    <alias name="" field="anlaegnavn" index="4"/>
    <alias name="" field="anlaegadr" index="5"/>
    <alias name="" field="anlaegpostnr" index="6"/>
    <alias name="" field="dataejer" index="7"/>
    <alias name="" field="aktiv" index="8"/>
    <alias name="" field="virksomhedstype" index="9"/>
    <alias name="" field="anlaegstype" index="10"/>
    <alias name="" field="indvindingsformaal" index="11"/>
    <alias name="" field="vandtype" index="12"/>
    <alias name="" field="url_plant" index="13"/>
    <alias name="" field="kommentar" index="14"/>
    <alias name="" field="amount" index="15"/>
    <alias name="" field="startdate" index="16"/>
    <alias name="" field="enddate" index="17"/>
  </aliases>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="anlaegsid" applyOnUpdate="0"/>
    <default expression="" field="supplant" applyOnUpdate="0"/>
    <default expression="" field="kommunenr" applyOnUpdate="0"/>
    <default expression="" field="anlaegnavn" applyOnUpdate="0"/>
    <default expression="" field="anlaegadr" applyOnUpdate="0"/>
    <default expression="" field="anlaegpostnr" applyOnUpdate="0"/>
    <default expression="" field="dataejer" applyOnUpdate="0"/>
    <default expression="" field="aktiv" applyOnUpdate="0"/>
    <default expression="" field="virksomhedstype" applyOnUpdate="0"/>
    <default expression="" field="anlaegstype" applyOnUpdate="0"/>
    <default expression="" field="indvindingsformaal" applyOnUpdate="0"/>
    <default expression="" field="vandtype" applyOnUpdate="0"/>
    <default expression="" field="url_plant" applyOnUpdate="0"/>
    <default expression="" field="kommentar" applyOnUpdate="0"/>
    <default expression="" field="amount" applyOnUpdate="0"/>
    <default expression="" field="startdate" applyOnUpdate="0"/>
    <default expression="" field="enddate" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" unique_strength="1" field="id" constraints="3" notnull_strength="1"/>
    <constraint exp_strength="0" unique_strength="0" field="anlaegsid" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="supplant" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="kommunenr" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="anlaegnavn" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="anlaegadr" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="anlaegpostnr" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="dataejer" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="aktiv" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="virksomhedstype" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="anlaegstype" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="indvindingsformaal" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="vandtype" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="url_plant" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="kommentar" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="amount" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="startdate" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" unique_strength="0" field="enddate" constraints="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="id"/>
    <constraint exp="" desc="" field="anlaegsid"/>
    <constraint exp="" desc="" field="supplant"/>
    <constraint exp="" desc="" field="kommunenr"/>
    <constraint exp="" desc="" field="anlaegnavn"/>
    <constraint exp="" desc="" field="anlaegadr"/>
    <constraint exp="" desc="" field="anlaegpostnr"/>
    <constraint exp="" desc="" field="dataejer"/>
    <constraint exp="" desc="" field="aktiv"/>
    <constraint exp="" desc="" field="virksomhedstype"/>
    <constraint exp="" desc="" field="anlaegstype"/>
    <constraint exp="" desc="" field="indvindingsformaal"/>
    <constraint exp="" desc="" field="vandtype"/>
    <constraint exp="" desc="" field="url_plant"/>
    <constraint exp="" desc="" field="kommentar"/>
    <constraint exp="" desc="" field="amount"/>
    <constraint exp="" desc="" field="startdate"/>
    <constraint exp="" desc="" field="enddate"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column width="-1" name="id" hidden="0" type="field"/>
      <column width="-1" name="anlaegsid" hidden="0" type="field"/>
      <column width="-1" name="supplant" hidden="0" type="field"/>
      <column width="-1" name="kommunenr" hidden="0" type="field"/>
      <column width="-1" name="anlaegnavn" hidden="0" type="field"/>
      <column width="-1" name="anlaegadr" hidden="0" type="field"/>
      <column width="-1" name="anlaegpostnr" hidden="0" type="field"/>
      <column width="-1" name="dataejer" hidden="0" type="field"/>
      <column width="-1" name="aktiv" hidden="0" type="field"/>
      <column width="-1" name="virksomhedstype" hidden="0" type="field"/>
      <column width="-1" name="anlaegstype" hidden="0" type="field"/>
      <column width="-1" name="indvindingsformaal" hidden="0" type="field"/>
      <column width="-1" name="vandtype" hidden="0" type="field"/>
      <column width="-1" name="url_plant" hidden="0" type="field"/>
      <column width="-1" name="kommentar" hidden="0" type="field"/>
      <column width="-1" name="amount" hidden="0" type="field"/>
      <column width="-1" name="startdate" hidden="0" type="field"/>
      <column width="-1" name="enddate" hidden="0" type="field"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="abandcause" editable="1"/>
    <field name="abandcontr" editable="1"/>
    <field name="abandondat" editable="1"/>
    <field name="accessremark" editable="1"/>
    <field name="aktiv" editable="1"/>
    <field name="amount" editable="1"/>
    <field name="anlaegadr" editable="1"/>
    <field name="anlaegnavn" editable="1"/>
    <field name="anlaegpostnr" editable="1"/>
    <field name="anlaegsid" editable="1"/>
    <field name="anlaegstype" editable="1"/>
    <field name="approach" editable="1"/>
    <field name="boreholeid" editable="1"/>
    <field name="boreholeno" editable="1"/>
    <field name="borhpostc" editable="1"/>
    <field name="borhtownno" editable="1"/>
    <field name="borhtownno2007" editable="1"/>
    <field name="comments" editable="1"/>
    <field name="companytype" editable="1"/>
    <field name="consuborno" editable="1"/>
    <field name="consulogno" editable="1"/>
    <field name="consultant" editable="1"/>
    <field name="countyno" editable="1"/>
    <field name="ctrpdescr" editable="1"/>
    <field name="ctrpeleva" editable="1"/>
    <field name="ctrpheight" editable="1"/>
    <field name="ctrpprecis" editable="1"/>
    <field name="ctrpzprecis" editable="1"/>
    <field name="dataejer" editable="1"/>
    <field name="dataowner" editable="1"/>
    <field name="datum" editable="1"/>
    <field name="drforadres" editable="1"/>
    <field name="drforpostc" editable="1"/>
    <field name="drilendate" editable="1"/>
    <field name="drillborno" editable="1"/>
    <field name="drilldepth" editable="1"/>
    <field name="drilledfor" editable="1"/>
    <field name="driller" editable="1"/>
    <field name="drilllogno" editable="1"/>
    <field name="drilstdate" editable="1"/>
    <field name="elevametho" editable="1"/>
    <field name="elevaquali" editable="1"/>
    <field name="elevasourc" editable="1"/>
    <field name="elevation" editable="1"/>
    <field name="enddate" editable="1"/>
    <field name="envcen" editable="1"/>
    <field name="grumoareano" editable="1"/>
    <field name="grumoborno" editable="1"/>
    <field name="grumobortype" editable="1"/>
    <field name="grumocountyno" editable="1"/>
    <field name="guid" editable="1"/>
    <field name="houseownas" editable="1"/>
    <field name="id" editable="1"/>
    <field name="indvindingsformaal" editable="1"/>
    <field name="insertdate" editable="1"/>
    <field name="insertuser" editable="1"/>
    <field name="installation" editable="1"/>
    <field name="kommentar" editable="1"/>
    <field name="kommunenr" editable="1"/>
    <field name="landregno" editable="1"/>
    <field name="latitude" editable="1"/>
    <field name="litholnote" editable="1"/>
    <field name="locatepersonemail" editable="1"/>
    <field name="location" editable="1"/>
    <field name="locatmetho" editable="1"/>
    <field name="locatquali" editable="1"/>
    <field name="locatsourc" editable="1"/>
    <field name="locquali" editable="1"/>
    <field name="longitude" editable="1"/>
    <field name="longtext" editable="1"/>
    <field name="loopareano" editable="1"/>
    <field name="loopstation" editable="1"/>
    <field name="looptype" editable="1"/>
    <field name="mapdistx" editable="1"/>
    <field name="mapdisty" editable="1"/>
    <field name="mapsheet" editable="1"/>
    <field name="municipal" editable="1"/>
    <field name="namingsys" editable="1"/>
    <field name="numofsampl" editable="1"/>
    <field name="numsamsto" editable="1"/>
    <field name="numsuplbor" editable="1"/>
    <field name="plantid" editable="1"/>
    <field name="preservationzone" editable="1"/>
    <field name="prevborhno" editable="1"/>
    <field name="protectionzone" editable="1"/>
    <field name="purpose" editable="1"/>
    <field name="region" editable="1"/>
    <field name="reportedby" editable="1"/>
    <field name="row_id" editable="1"/>
    <field name="samdescdat" editable="1"/>
    <field name="samrecedat" editable="1"/>
    <field name="startdate" editable="1"/>
    <field name="startdayunknown" editable="1"/>
    <field name="startmnthunknwn" editable="1"/>
    <field name="status" editable="1"/>
    <field name="supplant" editable="1"/>
    <field name="sys34x" editable="1"/>
    <field name="sys34y" editable="1"/>
    <field name="sys34zone" editable="1"/>
    <field name="togeusdate" editable="1"/>
    <field name="updatedate" editable="1"/>
    <field name="updateuser" editable="1"/>
    <field name="url_bor" editable="1"/>
    <field name="url_plant" editable="1"/>
    <field name="use" editable="1"/>
    <field name="usechangecause" editable="1"/>
    <field name="usechangedate" editable="1"/>
    <field name="utmzone" editable="1"/>
    <field name="vandtype" editable="1"/>
    <field name="various" editable="1"/>
    <field name="verticaref" editable="1"/>
    <field name="virksomhedstype" editable="1"/>
    <field name="workingconditions" editable="1"/>
    <field name="wwboreholeno" editable="1"/>
    <field name="xutm" editable="1"/>
    <field name="xutm32euref89" editable="1"/>
    <field name="yutm" editable="1"/>
    <field name="yutm32euref89" editable="1"/>
    <field name="zdvr90" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="abandcause"/>
    <field labelOnTop="0" name="abandcontr"/>
    <field labelOnTop="0" name="abandondat"/>
    <field labelOnTop="0" name="accessremark"/>
    <field labelOnTop="0" name="aktiv"/>
    <field labelOnTop="0" name="amount"/>
    <field labelOnTop="0" name="anlaegadr"/>
    <field labelOnTop="0" name="anlaegnavn"/>
    <field labelOnTop="0" name="anlaegpostnr"/>
    <field labelOnTop="0" name="anlaegsid"/>
    <field labelOnTop="0" name="anlaegstype"/>
    <field labelOnTop="0" name="approach"/>
    <field labelOnTop="0" name="boreholeid"/>
    <field labelOnTop="0" name="boreholeno"/>
    <field labelOnTop="0" name="borhpostc"/>
    <field labelOnTop="0" name="borhtownno"/>
    <field labelOnTop="0" name="borhtownno2007"/>
    <field labelOnTop="0" name="comments"/>
    <field labelOnTop="0" name="companytype"/>
    <field labelOnTop="0" name="consuborno"/>
    <field labelOnTop="0" name="consulogno"/>
    <field labelOnTop="0" name="consultant"/>
    <field labelOnTop="0" name="countyno"/>
    <field labelOnTop="0" name="ctrpdescr"/>
    <field labelOnTop="0" name="ctrpeleva"/>
    <field labelOnTop="0" name="ctrpheight"/>
    <field labelOnTop="0" name="ctrpprecis"/>
    <field labelOnTop="0" name="ctrpzprecis"/>
    <field labelOnTop="0" name="dataejer"/>
    <field labelOnTop="0" name="dataowner"/>
    <field labelOnTop="0" name="datum"/>
    <field labelOnTop="0" name="drforadres"/>
    <field labelOnTop="0" name="drforpostc"/>
    <field labelOnTop="0" name="drilendate"/>
    <field labelOnTop="0" name="drillborno"/>
    <field labelOnTop="0" name="drilldepth"/>
    <field labelOnTop="0" name="drilledfor"/>
    <field labelOnTop="0" name="driller"/>
    <field labelOnTop="0" name="drilllogno"/>
    <field labelOnTop="0" name="drilstdate"/>
    <field labelOnTop="0" name="elevametho"/>
    <field labelOnTop="0" name="elevaquali"/>
    <field labelOnTop="0" name="elevasourc"/>
    <field labelOnTop="0" name="elevation"/>
    <field labelOnTop="0" name="enddate"/>
    <field labelOnTop="0" name="envcen"/>
    <field labelOnTop="0" name="grumoareano"/>
    <field labelOnTop="0" name="grumoborno"/>
    <field labelOnTop="0" name="grumobortype"/>
    <field labelOnTop="0" name="grumocountyno"/>
    <field labelOnTop="0" name="guid"/>
    <field labelOnTop="0" name="houseownas"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="indvindingsformaal"/>
    <field labelOnTop="0" name="insertdate"/>
    <field labelOnTop="0" name="insertuser"/>
    <field labelOnTop="0" name="installation"/>
    <field labelOnTop="0" name="kommentar"/>
    <field labelOnTop="0" name="kommunenr"/>
    <field labelOnTop="0" name="landregno"/>
    <field labelOnTop="0" name="latitude"/>
    <field labelOnTop="0" name="litholnote"/>
    <field labelOnTop="0" name="locatepersonemail"/>
    <field labelOnTop="0" name="location"/>
    <field labelOnTop="0" name="locatmetho"/>
    <field labelOnTop="0" name="locatquali"/>
    <field labelOnTop="0" name="locatsourc"/>
    <field labelOnTop="0" name="locquali"/>
    <field labelOnTop="0" name="longitude"/>
    <field labelOnTop="0" name="longtext"/>
    <field labelOnTop="0" name="loopareano"/>
    <field labelOnTop="0" name="loopstation"/>
    <field labelOnTop="0" name="looptype"/>
    <field labelOnTop="0" name="mapdistx"/>
    <field labelOnTop="0" name="mapdisty"/>
    <field labelOnTop="0" name="mapsheet"/>
    <field labelOnTop="0" name="municipal"/>
    <field labelOnTop="0" name="namingsys"/>
    <field labelOnTop="0" name="numofsampl"/>
    <field labelOnTop="0" name="numsamsto"/>
    <field labelOnTop="0" name="numsuplbor"/>
    <field labelOnTop="0" name="plantid"/>
    <field labelOnTop="0" name="preservationzone"/>
    <field labelOnTop="0" name="prevborhno"/>
    <field labelOnTop="0" name="protectionzone"/>
    <field labelOnTop="0" name="purpose"/>
    <field labelOnTop="0" name="region"/>
    <field labelOnTop="0" name="reportedby"/>
    <field labelOnTop="0" name="row_id"/>
    <field labelOnTop="0" name="samdescdat"/>
    <field labelOnTop="0" name="samrecedat"/>
    <field labelOnTop="0" name="startdate"/>
    <field labelOnTop="0" name="startdayunknown"/>
    <field labelOnTop="0" name="startmnthunknwn"/>
    <field labelOnTop="0" name="status"/>
    <field labelOnTop="0" name="supplant"/>
    <field labelOnTop="0" name="sys34x"/>
    <field labelOnTop="0" name="sys34y"/>
    <field labelOnTop="0" name="sys34zone"/>
    <field labelOnTop="0" name="togeusdate"/>
    <field labelOnTop="0" name="updatedate"/>
    <field labelOnTop="0" name="updateuser"/>
    <field labelOnTop="0" name="url_bor"/>
    <field labelOnTop="0" name="url_plant"/>
    <field labelOnTop="0" name="use"/>
    <field labelOnTop="0" name="usechangecause"/>
    <field labelOnTop="0" name="usechangedate"/>
    <field labelOnTop="0" name="utmzone"/>
    <field labelOnTop="0" name="vandtype"/>
    <field labelOnTop="0" name="various"/>
    <field labelOnTop="0" name="verticaref"/>
    <field labelOnTop="0" name="virksomhedstype"/>
    <field labelOnTop="0" name="workingconditions"/>
    <field labelOnTop="0" name="wwboreholeno"/>
    <field labelOnTop="0" name="xutm"/>
    <field labelOnTop="0" name="xutm32euref89"/>
    <field labelOnTop="0" name="yutm"/>
    <field labelOnTop="0" name="yutm32euref89"/>
    <field labelOnTop="0" name="zdvr90"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="abandcause"/>
    <field reuseLastValue="0" name="abandcontr"/>
    <field reuseLastValue="0" name="abandondat"/>
    <field reuseLastValue="0" name="accessremark"/>
    <field reuseLastValue="0" name="aktiv"/>
    <field reuseLastValue="0" name="amount"/>
    <field reuseLastValue="0" name="anlaegadr"/>
    <field reuseLastValue="0" name="anlaegnavn"/>
    <field reuseLastValue="0" name="anlaegpostnr"/>
    <field reuseLastValue="0" name="anlaegsid"/>
    <field reuseLastValue="0" name="anlaegstype"/>
    <field reuseLastValue="0" name="approach"/>
    <field reuseLastValue="0" name="boreholeid"/>
    <field reuseLastValue="0" name="boreholeno"/>
    <field reuseLastValue="0" name="borhpostc"/>
    <field reuseLastValue="0" name="borhtownno"/>
    <field reuseLastValue="0" name="borhtownno2007"/>
    <field reuseLastValue="0" name="comments"/>
    <field reuseLastValue="0" name="companytype"/>
    <field reuseLastValue="0" name="consuborno"/>
    <field reuseLastValue="0" name="consulogno"/>
    <field reuseLastValue="0" name="consultant"/>
    <field reuseLastValue="0" name="countyno"/>
    <field reuseLastValue="0" name="ctrpdescr"/>
    <field reuseLastValue="0" name="ctrpeleva"/>
    <field reuseLastValue="0" name="ctrpheight"/>
    <field reuseLastValue="0" name="ctrpprecis"/>
    <field reuseLastValue="0" name="ctrpzprecis"/>
    <field reuseLastValue="0" name="dataejer"/>
    <field reuseLastValue="0" name="dataowner"/>
    <field reuseLastValue="0" name="datum"/>
    <field reuseLastValue="0" name="drforadres"/>
    <field reuseLastValue="0" name="drforpostc"/>
    <field reuseLastValue="0" name="drilendate"/>
    <field reuseLastValue="0" name="drillborno"/>
    <field reuseLastValue="0" name="drilldepth"/>
    <field reuseLastValue="0" name="drilledfor"/>
    <field reuseLastValue="0" name="driller"/>
    <field reuseLastValue="0" name="drilllogno"/>
    <field reuseLastValue="0" name="drilstdate"/>
    <field reuseLastValue="0" name="elevametho"/>
    <field reuseLastValue="0" name="elevaquali"/>
    <field reuseLastValue="0" name="elevasourc"/>
    <field reuseLastValue="0" name="elevation"/>
    <field reuseLastValue="0" name="enddate"/>
    <field reuseLastValue="0" name="envcen"/>
    <field reuseLastValue="0" name="grumoareano"/>
    <field reuseLastValue="0" name="grumoborno"/>
    <field reuseLastValue="0" name="grumobortype"/>
    <field reuseLastValue="0" name="grumocountyno"/>
    <field reuseLastValue="0" name="guid"/>
    <field reuseLastValue="0" name="houseownas"/>
    <field reuseLastValue="0" name="id"/>
    <field reuseLastValue="0" name="indvindingsformaal"/>
    <field reuseLastValue="0" name="insertdate"/>
    <field reuseLastValue="0" name="insertuser"/>
    <field reuseLastValue="0" name="installation"/>
    <field reuseLastValue="0" name="kommentar"/>
    <field reuseLastValue="0" name="kommunenr"/>
    <field reuseLastValue="0" name="landregno"/>
    <field reuseLastValue="0" name="latitude"/>
    <field reuseLastValue="0" name="litholnote"/>
    <field reuseLastValue="0" name="locatepersonemail"/>
    <field reuseLastValue="0" name="location"/>
    <field reuseLastValue="0" name="locatmetho"/>
    <field reuseLastValue="0" name="locatquali"/>
    <field reuseLastValue="0" name="locatsourc"/>
    <field reuseLastValue="0" name="locquali"/>
    <field reuseLastValue="0" name="longitude"/>
    <field reuseLastValue="0" name="longtext"/>
    <field reuseLastValue="0" name="loopareano"/>
    <field reuseLastValue="0" name="loopstation"/>
    <field reuseLastValue="0" name="looptype"/>
    <field reuseLastValue="0" name="mapdistx"/>
    <field reuseLastValue="0" name="mapdisty"/>
    <field reuseLastValue="0" name="mapsheet"/>
    <field reuseLastValue="0" name="municipal"/>
    <field reuseLastValue="0" name="namingsys"/>
    <field reuseLastValue="0" name="numofsampl"/>
    <field reuseLastValue="0" name="numsamsto"/>
    <field reuseLastValue="0" name="numsuplbor"/>
    <field reuseLastValue="0" name="plantid"/>
    <field reuseLastValue="0" name="preservationzone"/>
    <field reuseLastValue="0" name="prevborhno"/>
    <field reuseLastValue="0" name="protectionzone"/>
    <field reuseLastValue="0" name="purpose"/>
    <field reuseLastValue="0" name="region"/>
    <field reuseLastValue="0" name="reportedby"/>
    <field reuseLastValue="0" name="samdescdat"/>
    <field reuseLastValue="0" name="samrecedat"/>
    <field reuseLastValue="0" name="startdate"/>
    <field reuseLastValue="0" name="startdayunknown"/>
    <field reuseLastValue="0" name="startmnthunknwn"/>
    <field reuseLastValue="0" name="status"/>
    <field reuseLastValue="0" name="supplant"/>
    <field reuseLastValue="0" name="sys34x"/>
    <field reuseLastValue="0" name="sys34y"/>
    <field reuseLastValue="0" name="sys34zone"/>
    <field reuseLastValue="0" name="togeusdate"/>
    <field reuseLastValue="0" name="updatedate"/>
    <field reuseLastValue="0" name="updateuser"/>
    <field reuseLastValue="0" name="url_bor"/>
    <field reuseLastValue="0" name="url_plant"/>
    <field reuseLastValue="0" name="use"/>
    <field reuseLastValue="0" name="usechangecause"/>
    <field reuseLastValue="0" name="usechangedate"/>
    <field reuseLastValue="0" name="utmzone"/>
    <field reuseLastValue="0" name="vandtype"/>
    <field reuseLastValue="0" name="various"/>
    <field reuseLastValue="0" name="verticaref"/>
    <field reuseLastValue="0" name="virksomhedstype"/>
    <field reuseLastValue="0" name="workingconditions"/>
    <field reuseLastValue="0" name="wwboreholeno"/>
    <field reuseLastValue="0" name="xutm"/>
    <field reuseLastValue="0" name="xutm32euref89"/>
    <field reuseLastValue="0" name="yutm"/>
    <field reuseLastValue="0" name="yutm32euref89"/>
    <field reuseLastValue="0" name="zdvr90"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"ctrpdescr"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
