<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.26.3-Buenos Aires" symbologyReferenceScale="-1" simplifyLocal="1" minScale="100000000" simplifyAlgorithm="0" labelsEnabled="0" simplifyDrawingTol="1" readOnly="0" styleCategories="AllStyleCategories" simplifyDrawingHints="1" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal durationField="" limitMode="0" durationUnit="min" accumulate="0" startField="" endField="" fixedDuration="0" startExpression="" mode="0" endExpression="" enabled="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation binding="Centroid" zscale="1" extrusionEnabled="0" symbology="Line" clamping="Terrain" type="IndividualFeatures" showMarkerSymbolInSurfacePlots="0" extrusion="0" zoffset="0" respectLayerSymbol="1">
    <data-defined-properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol frame_rate="10" alpha="1" type="line" name="" is_animated="0" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="align_dash_pattern"/>
            <Option value="square" type="QString" name="capstyle"/>
            <Option value="5;2" type="QString" name="customdash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
            <Option value="MM" type="QString" name="customdash_unit"/>
            <Option value="0" type="QString" name="dash_pattern_offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
            <Option value="0" type="QString" name="draw_inside_polygon"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="152,125,183,255" type="QString" name="line_color"/>
            <Option value="solid" type="QString" name="line_style"/>
            <Option value="0.6" type="QString" name="line_width"/>
            <Option value="MM" type="QString" name="line_width_unit"/>
            <Option value="0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="0" type="QString" name="ring_filter"/>
            <Option value="0" type="QString" name="trim_distance_end"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_end_unit"/>
            <Option value="0" type="QString" name="trim_distance_start"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_start_unit"/>
            <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
            <Option value="0" type="QString" name="use_custom_dash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="152,125,183,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.6"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol frame_rate="10" alpha="1" type="fill" name="" is_animated="0" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" type="QString" name="border_width_map_unit_scale"/>
            <Option value="152,125,183,255" type="QString" name="color"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="109,89,131,255" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.2" type="QString" name="outline_width"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="solid" type="QString" name="style"/>
          </Option>
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="152,125,183,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="109,89,131,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol frame_rate="10" alpha="1" type="marker" name="" is_animated="0" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="angle"/>
            <Option value="square" type="QString" name="cap_style"/>
            <Option value="152,125,183,255" type="QString" name="color"/>
            <Option value="1" type="QString" name="horizontal_anchor_point"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="diamond" type="QString" name="name"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="109,89,131,255" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.2" type="QString" name="outline_width"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="outline_width_map_unit_scale"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="diameter" type="QString" name="scale_method"/>
            <Option value="3" type="QString" name="size"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="size_map_unit_scale"/>
            <Option value="MM" type="QString" name="size_unit"/>
            <Option value="1" type="QString" name="vertical_anchor_point"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="152,125,183,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="109,89,131,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 forceraster="0" enableorderby="0" referencescale="-1" type="RuleRenderer" symbollevels="0">
    <rules key="{75e96109-0ecc-48f2-8bd6-cc04e565abdf}">
      <rule key="{7e214656-fc4f-4e3d-911d-7fb912ce48a6}" symbol="0" filter=" &quot;udgaaet&quot; IS NULL"/>
    </rules>
    <symbols>
      <symbol frame_rate="10" alpha="1" type="fill" name="0" is_animated="0" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="align_dash_pattern"/>
            <Option value="square" type="QString" name="capstyle"/>
            <Option value="5;2" type="QString" name="customdash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
            <Option value="MM" type="QString" name="customdash_unit"/>
            <Option value="0" type="QString" name="dash_pattern_offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
            <Option value="0" type="QString" name="draw_inside_polygon"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="228,40,219,255" type="QString" name="line_color"/>
            <Option value="solid" type="QString" name="line_style"/>
            <Option value="0.36" type="QString" name="line_width"/>
            <Option value="MM" type="QString" name="line_width_unit"/>
            <Option value="0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="0" type="QString" name="ring_filter"/>
            <Option value="0" type="QString" name="trim_distance_end"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_end_unit"/>
            <Option value="0" type="QString" name="trim_distance_start"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_start_unit"/>
            <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
            <Option value="0" type="QString" name="use_custom_dash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="228,40,219,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.36"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option value="0" type="int" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory barWidth="5" scaleBasedVisibility="0" spacingUnit="MM" penWidth="0" spacing="5" penAlpha="255" minScaleDenominator="0" backgroundColor="#ffffff" lineSizeType="MM" enabled="0" width="15" opacity="1" diagramOrientation="Up" lineSizeScale="3x:0,0,0,0,0,0" height="15" labelPlacementMethod="XHeight" rotationOffset="270" direction="0" sizeType="MM" backgroundAlpha="255" spacingUnitScale="3x:0,0,0,0,0,0" scaleDependency="Area" showAxis="1" sizeScale="3x:0,0,0,0,0,0" maxScaleDenominator="1e+08" minimumSize="0" penColor="#000000">
      <fontProperties bold="0" strikethrough="0" style="" underline="0" description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" italic="0"/>
      <attribute color="#000000" label="" colorOpacity="1" field=""/>
      <axisSymbol>
        <symbol frame_rate="10" alpha="1" type="line" name="" is_animated="0" clip_to_extent="1" force_rhr="0">
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" pass="0" enabled="1" locked="0">
            <Option type="Map">
              <Option value="0" type="QString" name="align_dash_pattern"/>
              <Option value="square" type="QString" name="capstyle"/>
              <Option value="5;2" type="QString" name="customdash"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
              <Option value="MM" type="QString" name="customdash_unit"/>
              <Option value="0" type="QString" name="dash_pattern_offset"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
              <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
              <Option value="0" type="QString" name="draw_inside_polygon"/>
              <Option value="bevel" type="QString" name="joinstyle"/>
              <Option value="35,35,35,255" type="QString" name="line_color"/>
              <Option value="solid" type="QString" name="line_style"/>
              <Option value="0.26" type="QString" name="line_width"/>
              <Option value="MM" type="QString" name="line_width_unit"/>
              <Option value="0" type="QString" name="offset"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
              <Option value="MM" type="QString" name="offset_unit"/>
              <Option value="0" type="QString" name="ring_filter"/>
              <Option value="0" type="QString" name="trim_distance_end"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
              <Option value="MM" type="QString" name="trim_distance_end_unit"/>
              <Option value="0" type="QString" name="trim_distance_start"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
              <Option value="MM" type="QString" name="trim_distance_start_unit"/>
              <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
              <Option value="0" type="QString" name="use_custom_dash"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" priority="0" showAll="1" dist="0" placement="1" zIndex="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option type="Map" name="QgsGeometryGapCheck">
        <Option value="0" type="double" name="allowedGapsBuffer"/>
        <Option value="false" type="bool" name="allowedGapsEnabled"/>
        <Option value="" type="QString" name="allowedGapsLayer"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="projektid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="gkoid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="kortnavn" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anl_id" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anl_navn" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="idenf_osd" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="aar_udpeget" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="kommentar" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anlaeg_url" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="rapporturl" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boringer" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="modeltype" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="modelkode" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dynamisk" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="indvtil" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tilprbor" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="udgaaet" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="magasin" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="opretdato" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="opretbruger" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="opdateringdato" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="opdateringbruger" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="id"/>
    <alias index="1" name="" field="projektid"/>
    <alias index="2" name="" field="gkoid"/>
    <alias index="3" name="" field="kortnavn"/>
    <alias index="4" name="" field="anl_id"/>
    <alias index="5" name="" field="anl_navn"/>
    <alias index="6" name="" field="idenf_osd"/>
    <alias index="7" name="" field="aar_udpeget"/>
    <alias index="8" name="" field="kommentar"/>
    <alias index="9" name="" field="anlaeg_url"/>
    <alias index="10" name="" field="rapporturl"/>
    <alias index="11" name="" field="boringer"/>
    <alias index="12" name="" field="modeltype"/>
    <alias index="13" name="" field="modelkode"/>
    <alias index="14" name="" field="dynamisk"/>
    <alias index="15" name="" field="indvtil"/>
    <alias index="16" name="" field="tilprbor"/>
    <alias index="17" name="" field="udgaaet"/>
    <alias index="18" name="" field="magasin"/>
    <alias index="19" name="" field="opretdato"/>
    <alias index="20" name="" field="opretbruger"/>
    <alias index="21" name="" field="opdateringdato"/>
    <alias index="22" name="" field="opdateringbruger"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id"/>
    <default applyOnUpdate="0" expression="" field="projektid"/>
    <default applyOnUpdate="0" expression="" field="gkoid"/>
    <default applyOnUpdate="0" expression="" field="kortnavn"/>
    <default applyOnUpdate="0" expression="" field="anl_id"/>
    <default applyOnUpdate="0" expression="" field="anl_navn"/>
    <default applyOnUpdate="0" expression="" field="idenf_osd"/>
    <default applyOnUpdate="0" expression="" field="aar_udpeget"/>
    <default applyOnUpdate="0" expression="" field="kommentar"/>
    <default applyOnUpdate="0" expression="" field="anlaeg_url"/>
    <default applyOnUpdate="0" expression="" field="rapporturl"/>
    <default applyOnUpdate="0" expression="" field="boringer"/>
    <default applyOnUpdate="0" expression="" field="modeltype"/>
    <default applyOnUpdate="0" expression="" field="modelkode"/>
    <default applyOnUpdate="0" expression="" field="dynamisk"/>
    <default applyOnUpdate="0" expression="" field="indvtil"/>
    <default applyOnUpdate="0" expression="" field="tilprbor"/>
    <default applyOnUpdate="0" expression="" field="udgaaet"/>
    <default applyOnUpdate="0" expression="" field="magasin"/>
    <default applyOnUpdate="0" expression="" field="opretdato"/>
    <default applyOnUpdate="0" expression="" field="opretbruger"/>
    <default applyOnUpdate="0" expression="" field="opdateringdato"/>
    <default applyOnUpdate="0" expression="" field="opdateringbruger"/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="id"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="projektid"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="gkoid"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="kortnavn"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="anl_id"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="anl_navn"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="idenf_osd"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="aar_udpeget"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="kommentar"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="anlaeg_url"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="rapporturl"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="boringer"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="modeltype"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="modelkode"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="dynamisk"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="indvtil"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="tilprbor"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="udgaaet"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="magasin"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="opretdato"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="opretbruger"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="opdateringdato"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="opdateringbruger"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id"/>
    <constraint desc="" exp="" field="projektid"/>
    <constraint desc="" exp="" field="gkoid"/>
    <constraint desc="" exp="" field="kortnavn"/>
    <constraint desc="" exp="" field="anl_id"/>
    <constraint desc="" exp="" field="anl_navn"/>
    <constraint desc="" exp="" field="idenf_osd"/>
    <constraint desc="" exp="" field="aar_udpeget"/>
    <constraint desc="" exp="" field="kommentar"/>
    <constraint desc="" exp="" field="anlaeg_url"/>
    <constraint desc="" exp="" field="rapporturl"/>
    <constraint desc="" exp="" field="boringer"/>
    <constraint desc="" exp="" field="modeltype"/>
    <constraint desc="" exp="" field="modelkode"/>
    <constraint desc="" exp="" field="dynamisk"/>
    <constraint desc="" exp="" field="indvtil"/>
    <constraint desc="" exp="" field="tilprbor"/>
    <constraint desc="" exp="" field="udgaaet"/>
    <constraint desc="" exp="" field="magasin"/>
    <constraint desc="" exp="" field="opretdato"/>
    <constraint desc="" exp="" field="opretbruger"/>
    <constraint desc="" exp="" field="opdateringdato"/>
    <constraint desc="" exp="" field="opdateringbruger"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" hidden="0" type="field" name="id"/>
      <column width="-1" hidden="0" type="field" name="projektid"/>
      <column width="-1" hidden="0" type="field" name="gkoid"/>
      <column width="-1" hidden="0" type="field" name="kortnavn"/>
      <column width="-1" hidden="0" type="field" name="anl_id"/>
      <column width="-1" hidden="0" type="field" name="anl_navn"/>
      <column width="-1" hidden="0" type="field" name="idenf_osd"/>
      <column width="-1" hidden="0" type="field" name="aar_udpeget"/>
      <column width="-1" hidden="0" type="field" name="kommentar"/>
      <column width="-1" hidden="0" type="field" name="anlaeg_url"/>
      <column width="-1" hidden="0" type="field" name="rapporturl"/>
      <column width="-1" hidden="0" type="field" name="boringer"/>
      <column width="-1" hidden="0" type="field" name="modeltype"/>
      <column width="-1" hidden="0" type="field" name="modelkode"/>
      <column width="-1" hidden="0" type="field" name="dynamisk"/>
      <column width="-1" hidden="0" type="field" name="indvtil"/>
      <column width="-1" hidden="0" type="field" name="tilprbor"/>
      <column width="-1" hidden="0" type="field" name="udgaaet"/>
      <column width="-1" hidden="0" type="field" name="magasin"/>
      <column width="-1" hidden="0" type="field" name="opretdato"/>
      <column width="-1" hidden="0" type="field" name="opretbruger"/>
      <column width="-1" hidden="0" type="field" name="opdateringdato"/>
      <column width="-1" hidden="0" type="field" name="opdateringbruger"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="aar_udpeget"/>
    <field editable="1" name="anl_id"/>
    <field editable="1" name="anl_navn"/>
    <field editable="1" name="anlaeg_url"/>
    <field editable="1" name="boringer"/>
    <field editable="1" name="dynamisk"/>
    <field editable="1" name="gkoid"/>
    <field editable="1" name="id"/>
    <field editable="1" name="idenf_osd"/>
    <field editable="1" name="indvtil"/>
    <field editable="1" name="kommentar"/>
    <field editable="1" name="kortnavn"/>
    <field editable="1" name="magasin"/>
    <field editable="1" name="modelkode"/>
    <field editable="1" name="modeltype"/>
    <field editable="1" name="opdateringbruger"/>
    <field editable="1" name="opdateringdato"/>
    <field editable="1" name="opretbruger"/>
    <field editable="1" name="opretdato"/>
    <field editable="1" name="projektid"/>
    <field editable="1" name="rapporturl"/>
    <field editable="1" name="tilprbor"/>
    <field editable="1" name="udgaaet"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="aar_udpeget"/>
    <field labelOnTop="0" name="anl_id"/>
    <field labelOnTop="0" name="anl_navn"/>
    <field labelOnTop="0" name="anlaeg_url"/>
    <field labelOnTop="0" name="boringer"/>
    <field labelOnTop="0" name="dynamisk"/>
    <field labelOnTop="0" name="gkoid"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="idenf_osd"/>
    <field labelOnTop="0" name="indvtil"/>
    <field labelOnTop="0" name="kommentar"/>
    <field labelOnTop="0" name="kortnavn"/>
    <field labelOnTop="0" name="magasin"/>
    <field labelOnTop="0" name="modelkode"/>
    <field labelOnTop="0" name="modeltype"/>
    <field labelOnTop="0" name="opdateringbruger"/>
    <field labelOnTop="0" name="opdateringdato"/>
    <field labelOnTop="0" name="opretbruger"/>
    <field labelOnTop="0" name="opretdato"/>
    <field labelOnTop="0" name="projektid"/>
    <field labelOnTop="0" name="rapporturl"/>
    <field labelOnTop="0" name="tilprbor"/>
    <field labelOnTop="0" name="udgaaet"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="aar_udpeget"/>
    <field reuseLastValue="0" name="anl_id"/>
    <field reuseLastValue="0" name="anl_navn"/>
    <field reuseLastValue="0" name="anlaeg_url"/>
    <field reuseLastValue="0" name="boringer"/>
    <field reuseLastValue="0" name="dynamisk"/>
    <field reuseLastValue="0" name="gkoid"/>
    <field reuseLastValue="0" name="id"/>
    <field reuseLastValue="0" name="idenf_osd"/>
    <field reuseLastValue="0" name="indvtil"/>
    <field reuseLastValue="0" name="kommentar"/>
    <field reuseLastValue="0" name="kortnavn"/>
    <field reuseLastValue="0" name="magasin"/>
    <field reuseLastValue="0" name="modelkode"/>
    <field reuseLastValue="0" name="modeltype"/>
    <field reuseLastValue="0" name="opdateringbruger"/>
    <field reuseLastValue="0" name="opdateringdato"/>
    <field reuseLastValue="0" name="opretbruger"/>
    <field reuseLastValue="0" name="opretdato"/>
    <field reuseLastValue="0" name="projektid"/>
    <field reuseLastValue="0" name="rapporturl"/>
    <field reuseLastValue="0" name="tilprbor"/>
    <field reuseLastValue="0" name="udgaaet"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"kortnavn"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
