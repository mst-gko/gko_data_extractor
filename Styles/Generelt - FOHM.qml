<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.26.3-Buenos Aires" hasScaleBasedVisibilityFlag="0" simplifyDrawingTol="1" maxScale="0" simplifyAlgorithm="0" simplifyLocal="1" symbologyReferenceScale="-1" readOnly="0" labelsEnabled="0" simplifyMaxScale="1" simplifyDrawingHints="0" styleCategories="AllStyleCategories" minScale="100000000">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endExpression="" durationField="" enabled="0" mode="0" fixedDuration="0" startField="" startExpression="" durationUnit="min" endField="" accumulate="0" limitMode="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation respectLayerSymbol="1" symbology="Line" clamping="Terrain" extrusion="0" extrusionEnabled="0" zscale="1" binding="Centroid" type="IndividualFeatures" showMarkerSymbolInSurfacePlots="0" zoffset="0">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol clip_to_extent="1" is_animated="0" type="line" name="" force_rhr="0" alpha="1" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleLine">
          <Option type="Map">
            <Option type="QString" name="align_dash_pattern" value="0"/>
            <Option type="QString" name="capstyle" value="square"/>
            <Option type="QString" name="customdash" value="5;2"/>
            <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="customdash_unit" value="MM"/>
            <Option type="QString" name="dash_pattern_offset" value="0"/>
            <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
            <Option type="QString" name="draw_inside_polygon" value="0"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="line_color" value="141,90,153,255"/>
            <Option type="QString" name="line_style" value="solid"/>
            <Option type="QString" name="line_width" value="0.6"/>
            <Option type="QString" name="line_width_unit" value="MM"/>
            <Option type="QString" name="offset" value="0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="ring_filter" value="0"/>
            <Option type="QString" name="trim_distance_end" value="0"/>
            <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_end_unit" value="MM"/>
            <Option type="QString" name="trim_distance_start" value="0"/>
            <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_start_unit" value="MM"/>
            <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
            <Option type="QString" name="use_custom_dash" value="0"/>
            <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="141,90,153,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.6"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol clip_to_extent="1" is_animated="0" type="fill" name="" force_rhr="0" alpha="1" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleFill">
          <Option type="Map">
            <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="color" value="141,90,153,255"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="101,64,109,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0.2"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="style" value="solid"/>
          </Option>
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="141,90,153,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="101,64,109,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol clip_to_extent="1" is_animated="0" type="marker" name="" force_rhr="0" alpha="1" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="141,90,153,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="diamond"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="101,64,109,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0.2"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="3"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="141,90,153,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="101,64,109,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 symbollevels="0" referencescale="-1" forceraster="0" type="RuleRenderer" enableorderby="0">
    <rules key="{904a0a89-1888-4910-877e-9a7ec1ad9930}">
      <rule key="{8000e190-7d2d-4ab1-8220-28cd93256e8f}" filter="overlay_intersects('Indvindingsboringer',null,longtext IN ('Offentlige fælles vandforsyningsanlæg', 'Private fælles vandforsyningsanlæg') and upper(use) NOT IN ('S', 'PA', 'M') AND  &quot;abandondat&quot; IS NULL)&#xd;&#xa;">
        <rule key="{c8265121-86b7-4346-bd92-64d7e9892fa1}" symbol="0" label="Kvartær Sand" filter="layer_num IN (0200,0400, 0600, 0800, 1000, 1200, 1400, 1600, 1800, 2100, 2400)"/>
        <rule key="{ce440c66-d7ca-4341-a4c3-88746abe3f9e}" symbol="1" label="Kvartær Ler" filter="layer_num IN (0100,0300,0500,0700,0900,1100,1300,1500,1700,1900,2200,2400)"/>
        <rule key="{68339492-27ca-48ca-b04b-91d6cde3d936}" symbol="2" label="Miocæn" filter="layer_num > 2401"/>
        <rule key="{b6e3315f-123f-498b-a562-3ef0d72f3e9a}" symbol="3" label="Ukendt" filter="ELSE"/>
      </rule>
    </rules>
    <symbols>
      <symbol clip_to_extent="1" is_animated="0" type="marker" name="0" force_rhr="0" alpha="1" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="229,150,14,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="229,150,14,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" is_animated="0" type="marker" name="1" force_rhr="0" alpha="1" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="29,214,45,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="29,214,45,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" is_animated="0" type="marker" name="2" force_rhr="0" alpha="1" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="26,163,255,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="26,163,255,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" is_animated="0" type="marker" name="3" force_rhr="0" alpha="1" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="0,0,0,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="0,0,0,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="&quot;ctrpdescr&quot;"/>
      </Option>
      <Option type="QString" name="embeddedWidgets/count" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory barWidth="5" opacity="1" lineSizeType="MM" spacingUnit="MM" showAxis="0" scaleDependency="Area" sizeType="MM" minScaleDenominator="0" diagramOrientation="Up" minimumSize="0" penWidth="0" backgroundColor="#ffffff" height="15" lineSizeScale="3x:0,0,0,0,0,0" rotationOffset="270" width="15" maxScaleDenominator="1e+08" enabled="0" penAlpha="255" sizeScale="3x:0,0,0,0,0,0" penColor="#000000" backgroundAlpha="255" scaleBasedVisibility="0" spacingUnitScale="3x:0,0,0,0,0,0" direction="1" labelPlacementMethod="XHeight" spacing="0">
      <fontProperties strikethrough="0" bold="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style="" underline="0" italic="0"/>
      <attribute colorOpacity="1" label="" field="" color="#000000"/>
      <axisSymbol>
        <symbol clip_to_extent="1" is_animated="0" type="line" name="" force_rhr="0" alpha="1" frame_rate="10">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer enabled="1" locked="0" pass="0" class="SimpleLine">
            <Option type="Map">
              <Option type="QString" name="align_dash_pattern" value="0"/>
              <Option type="QString" name="capstyle" value="square"/>
              <Option type="QString" name="customdash" value="5;2"/>
              <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="customdash_unit" value="MM"/>
              <Option type="QString" name="dash_pattern_offset" value="0"/>
              <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
              <Option type="QString" name="draw_inside_polygon" value="0"/>
              <Option type="QString" name="joinstyle" value="bevel"/>
              <Option type="QString" name="line_color" value="35,35,35,255"/>
              <Option type="QString" name="line_style" value="solid"/>
              <Option type="QString" name="line_width" value="0.26"/>
              <Option type="QString" name="line_width_unit" value="MM"/>
              <Option type="QString" name="offset" value="0"/>
              <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offset_unit" value="MM"/>
              <Option type="QString" name="ring_filter" value="0"/>
              <Option type="QString" name="trim_distance_end" value="0"/>
              <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_end_unit" value="MM"/>
              <Option type="QString" name="trim_distance_start" value="0"/>
              <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_start_unit" value="MM"/>
              <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
              <Option type="QString" name="use_custom_dash" value="0"/>
              <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" dist="0" showAll="1" linePlacementFlags="18" priority="0" obstacle="0" placement="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="boreholeid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="guid_intake">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="guid_screen">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="layer">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="layer_num">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="litho">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="time_period">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="lith_screen_agg">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="intake_pct">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="boreholeid" name=""/>
    <alias index="1" field="guid_intake" name=""/>
    <alias index="2" field="guid_screen" name=""/>
    <alias index="3" field="layer" name=""/>
    <alias index="4" field="layer_num" name=""/>
    <alias index="5" field="litho" name=""/>
    <alias index="6" field="time_period" name=""/>
    <alias index="7" field="lith_screen_agg" name=""/>
    <alias index="8" field="intake_pct" name=""/>
  </aliases>
  <defaults>
    <default expression="" field="boreholeid" applyOnUpdate="0"/>
    <default expression="" field="guid_intake" applyOnUpdate="0"/>
    <default expression="" field="guid_screen" applyOnUpdate="0"/>
    <default expression="" field="layer" applyOnUpdate="0"/>
    <default expression="" field="layer_num" applyOnUpdate="0"/>
    <default expression="" field="litho" applyOnUpdate="0"/>
    <default expression="" field="time_period" applyOnUpdate="0"/>
    <default expression="" field="lith_screen_agg" applyOnUpdate="0"/>
    <default expression="" field="intake_pct" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="0" exp_strength="0" field="boreholeid" unique_strength="0" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" field="guid_intake" unique_strength="0" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" field="guid_screen" unique_strength="0" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" field="layer" unique_strength="0" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" field="layer_num" unique_strength="0" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" field="litho" unique_strength="0" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" field="time_period" unique_strength="0" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" field="lith_screen_agg" unique_strength="0" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" field="intake_pct" unique_strength="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="boreholeid" desc=""/>
    <constraint exp="" field="guid_intake" desc=""/>
    <constraint exp="" field="guid_screen" desc=""/>
    <constraint exp="" field="layer" desc=""/>
    <constraint exp="" field="layer_num" desc=""/>
    <constraint exp="" field="litho" desc=""/>
    <constraint exp="" field="time_period" desc=""/>
    <constraint exp="" field="lith_screen_agg" desc=""/>
    <constraint exp="" field="intake_pct" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column hidden="0" width="181" type="field" name="boreholeid"/>
      <column hidden="0" width="-1" type="field" name="guid_intake"/>
      <column hidden="0" width="-1" type="field" name="guid_screen"/>
      <column hidden="0" width="181" type="field" name="layer"/>
      <column hidden="0" width="-1" type="field" name="layer_num"/>
      <column hidden="0" width="-1" type="field" name="litho"/>
      <column hidden="0" width="-1" type="field" name="time_period"/>
      <column hidden="0" width="-1" type="field" name="lith_screen_agg"/>
      <column hidden="0" width="-1" type="field" name="intake_pct"/>
      <column hidden="1" width="-1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="0" name="FOHM - Lagtildeling filter - Primær magasin_guid_intake"/>
    <field editable="0" name="FOHM - Lagtildeling filter - Primær magasin_guid_screen"/>
    <field editable="0" name="FOHM - Lagtildeling filter - Primær magasin_intake_pct"/>
    <field editable="0" name="FOHM - Lagtildeling filter - Primær magasin_layer"/>
    <field editable="0" name="FOHM - Lagtildeling filter - Primær magasin_layer_num"/>
    <field editable="0" name="FOHM - Lagtildeling filter - Primær magasin_litho"/>
    <field editable="0" name="FOHM - Lagtildeling filter - Primær magasin_time_period"/>
    <field editable="0" name="FOHM_guid_intake"/>
    <field editable="0" name="FOHM_guid_screen"/>
    <field editable="0" name="FOHM_intake_pct"/>
    <field editable="0" name="FOHM_layer"/>
    <field editable="0" name="FOHM_layer_num"/>
    <field editable="0" name="FOHM_litho"/>
    <field editable="0" name="FOHM_time_period"/>
    <field editable="1" name="abandcause"/>
    <field editable="1" name="abandcontr"/>
    <field editable="1" name="abandondat"/>
    <field editable="1" name="accessremark"/>
    <field editable="1" name="approach"/>
    <field editable="1" name="boreholeid"/>
    <field editable="1" name="boreholeno"/>
    <field editable="1" name="borhpostc"/>
    <field editable="1" name="borhtownno"/>
    <field editable="1" name="borhtownno2007"/>
    <field editable="1" name="comments"/>
    <field editable="1" name="companytype"/>
    <field editable="1" name="consuborno"/>
    <field editable="1" name="consulogno"/>
    <field editable="1" name="consultant"/>
    <field editable="1" name="countyno"/>
    <field editable="1" name="ctrpdescr"/>
    <field editable="1" name="ctrpeleva"/>
    <field editable="1" name="ctrpheight"/>
    <field editable="1" name="ctrpprecis"/>
    <field editable="1" name="ctrpzprecis"/>
    <field editable="1" name="dataowner"/>
    <field editable="1" name="datum"/>
    <field editable="1" name="drforadres"/>
    <field editable="1" name="drforpostc"/>
    <field editable="1" name="drilendate"/>
    <field editable="1" name="drillborno"/>
    <field editable="1" name="drilldepth"/>
    <field editable="1" name="drilledfor"/>
    <field editable="1" name="driller"/>
    <field editable="1" name="drilllogno"/>
    <field editable="1" name="drilstdate"/>
    <field editable="1" name="elevametho"/>
    <field editable="1" name="elevaquali"/>
    <field editable="1" name="elevasourc"/>
    <field editable="1" name="elevation"/>
    <field editable="1" name="envcen"/>
    <field editable="1" name="grumoareano"/>
    <field editable="1" name="grumoborno"/>
    <field editable="1" name="grumobortype"/>
    <field editable="1" name="grumocountyno"/>
    <field editable="1" name="guid"/>
    <field editable="1" name="guid_intake"/>
    <field editable="1" name="guid_screen"/>
    <field editable="1" name="houseownas"/>
    <field editable="1" name="id"/>
    <field editable="1" name="insertdate"/>
    <field editable="1" name="insertuser"/>
    <field editable="1" name="installation"/>
    <field editable="1" name="intake_pct"/>
    <field editable="1" name="landregno"/>
    <field editable="1" name="latitude"/>
    <field editable="1" name="layer"/>
    <field editable="1" name="layer_num"/>
    <field editable="1" name="lith_screen_agg"/>
    <field editable="1" name="litho"/>
    <field editable="1" name="litholnote"/>
    <field editable="1" name="locatepersonemail"/>
    <field editable="1" name="location"/>
    <field editable="1" name="locatmetho"/>
    <field editable="1" name="locatquali"/>
    <field editable="1" name="locatsourc"/>
    <field editable="1" name="locquali"/>
    <field editable="1" name="longitude"/>
    <field editable="1" name="longtext"/>
    <field editable="1" name="loopareano"/>
    <field editable="1" name="loopstation"/>
    <field editable="1" name="looptype"/>
    <field editable="1" name="mapdistx"/>
    <field editable="1" name="mapdisty"/>
    <field editable="1" name="mapsheet"/>
    <field editable="1" name="municipal"/>
    <field editable="1" name="namingsys"/>
    <field editable="1" name="numofsampl"/>
    <field editable="1" name="numsamsto"/>
    <field editable="1" name="numsuplbor"/>
    <field editable="1" name="plantid"/>
    <field editable="1" name="preservationzone"/>
    <field editable="1" name="prevborhno"/>
    <field editable="1" name="protectionzone"/>
    <field editable="1" name="purpose"/>
    <field editable="1" name="region"/>
    <field editable="1" name="reportedby"/>
    <field editable="1" name="row_id"/>
    <field editable="1" name="samdescdat"/>
    <field editable="1" name="samrecedat"/>
    <field editable="1" name="startdayunknown"/>
    <field editable="1" name="startmnthunknwn"/>
    <field editable="1" name="status"/>
    <field editable="1" name="sys34x"/>
    <field editable="1" name="sys34y"/>
    <field editable="1" name="sys34zone"/>
    <field editable="1" name="time_period"/>
    <field editable="1" name="togeusdate"/>
    <field editable="1" name="updatedate"/>
    <field editable="1" name="updateuser"/>
    <field editable="1" name="url_bor"/>
    <field editable="1" name="use"/>
    <field editable="1" name="usechangecause"/>
    <field editable="1" name="usechangedate"/>
    <field editable="1" name="utmzone"/>
    <field editable="1" name="various"/>
    <field editable="1" name="verticaref"/>
    <field editable="1" name="workingconditions"/>
    <field editable="1" name="wwboreholeno"/>
    <field editable="1" name="xutm"/>
    <field editable="1" name="xutm32euref89"/>
    <field editable="1" name="yutm"/>
    <field editable="1" name="yutm32euref89"/>
    <field editable="1" name="zdvr90"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="FOHM - Lagtildeling filter - Primær magasin_guid_intake"/>
    <field labelOnTop="0" name="FOHM - Lagtildeling filter - Primær magasin_guid_screen"/>
    <field labelOnTop="0" name="FOHM - Lagtildeling filter - Primær magasin_intake_pct"/>
    <field labelOnTop="0" name="FOHM - Lagtildeling filter - Primær magasin_layer"/>
    <field labelOnTop="0" name="FOHM - Lagtildeling filter - Primær magasin_layer_num"/>
    <field labelOnTop="0" name="FOHM - Lagtildeling filter - Primær magasin_litho"/>
    <field labelOnTop="0" name="FOHM - Lagtildeling filter - Primær magasin_time_period"/>
    <field labelOnTop="0" name="FOHM_guid_intake"/>
    <field labelOnTop="0" name="FOHM_guid_screen"/>
    <field labelOnTop="0" name="FOHM_intake_pct"/>
    <field labelOnTop="0" name="FOHM_layer"/>
    <field labelOnTop="0" name="FOHM_layer_num"/>
    <field labelOnTop="0" name="FOHM_litho"/>
    <field labelOnTop="0" name="FOHM_time_period"/>
    <field labelOnTop="0" name="abandcause"/>
    <field labelOnTop="0" name="abandcontr"/>
    <field labelOnTop="0" name="abandondat"/>
    <field labelOnTop="0" name="accessremark"/>
    <field labelOnTop="0" name="approach"/>
    <field labelOnTop="0" name="boreholeid"/>
    <field labelOnTop="0" name="boreholeno"/>
    <field labelOnTop="0" name="borhpostc"/>
    <field labelOnTop="0" name="borhtownno"/>
    <field labelOnTop="0" name="borhtownno2007"/>
    <field labelOnTop="0" name="comments"/>
    <field labelOnTop="0" name="companytype"/>
    <field labelOnTop="0" name="consuborno"/>
    <field labelOnTop="0" name="consulogno"/>
    <field labelOnTop="0" name="consultant"/>
    <field labelOnTop="0" name="countyno"/>
    <field labelOnTop="0" name="ctrpdescr"/>
    <field labelOnTop="0" name="ctrpeleva"/>
    <field labelOnTop="0" name="ctrpheight"/>
    <field labelOnTop="0" name="ctrpprecis"/>
    <field labelOnTop="0" name="ctrpzprecis"/>
    <field labelOnTop="0" name="dataowner"/>
    <field labelOnTop="0" name="datum"/>
    <field labelOnTop="0" name="drforadres"/>
    <field labelOnTop="0" name="drforpostc"/>
    <field labelOnTop="0" name="drilendate"/>
    <field labelOnTop="0" name="drillborno"/>
    <field labelOnTop="0" name="drilldepth"/>
    <field labelOnTop="0" name="drilledfor"/>
    <field labelOnTop="0" name="driller"/>
    <field labelOnTop="0" name="drilllogno"/>
    <field labelOnTop="0" name="drilstdate"/>
    <field labelOnTop="0" name="elevametho"/>
    <field labelOnTop="0" name="elevaquali"/>
    <field labelOnTop="0" name="elevasourc"/>
    <field labelOnTop="0" name="elevation"/>
    <field labelOnTop="0" name="envcen"/>
    <field labelOnTop="0" name="grumoareano"/>
    <field labelOnTop="0" name="grumoborno"/>
    <field labelOnTop="0" name="grumobortype"/>
    <field labelOnTop="0" name="grumocountyno"/>
    <field labelOnTop="0" name="guid"/>
    <field labelOnTop="0" name="guid_intake"/>
    <field labelOnTop="0" name="guid_screen"/>
    <field labelOnTop="0" name="houseownas"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="insertdate"/>
    <field labelOnTop="0" name="insertuser"/>
    <field labelOnTop="0" name="installation"/>
    <field labelOnTop="0" name="intake_pct"/>
    <field labelOnTop="0" name="landregno"/>
    <field labelOnTop="0" name="latitude"/>
    <field labelOnTop="0" name="layer"/>
    <field labelOnTop="0" name="layer_num"/>
    <field labelOnTop="0" name="lith_screen_agg"/>
    <field labelOnTop="0" name="litho"/>
    <field labelOnTop="0" name="litholnote"/>
    <field labelOnTop="0" name="locatepersonemail"/>
    <field labelOnTop="0" name="location"/>
    <field labelOnTop="0" name="locatmetho"/>
    <field labelOnTop="0" name="locatquali"/>
    <field labelOnTop="0" name="locatsourc"/>
    <field labelOnTop="0" name="locquali"/>
    <field labelOnTop="0" name="longitude"/>
    <field labelOnTop="0" name="longtext"/>
    <field labelOnTop="0" name="loopareano"/>
    <field labelOnTop="0" name="loopstation"/>
    <field labelOnTop="0" name="looptype"/>
    <field labelOnTop="0" name="mapdistx"/>
    <field labelOnTop="0" name="mapdisty"/>
    <field labelOnTop="0" name="mapsheet"/>
    <field labelOnTop="0" name="municipal"/>
    <field labelOnTop="0" name="namingsys"/>
    <field labelOnTop="0" name="numofsampl"/>
    <field labelOnTop="0" name="numsamsto"/>
    <field labelOnTop="0" name="numsuplbor"/>
    <field labelOnTop="0" name="plantid"/>
    <field labelOnTop="0" name="preservationzone"/>
    <field labelOnTop="0" name="prevborhno"/>
    <field labelOnTop="0" name="protectionzone"/>
    <field labelOnTop="0" name="purpose"/>
    <field labelOnTop="0" name="region"/>
    <field labelOnTop="0" name="reportedby"/>
    <field labelOnTop="0" name="row_id"/>
    <field labelOnTop="0" name="samdescdat"/>
    <field labelOnTop="0" name="samrecedat"/>
    <field labelOnTop="0" name="startdayunknown"/>
    <field labelOnTop="0" name="startmnthunknwn"/>
    <field labelOnTop="0" name="status"/>
    <field labelOnTop="0" name="sys34x"/>
    <field labelOnTop="0" name="sys34y"/>
    <field labelOnTop="0" name="sys34zone"/>
    <field labelOnTop="0" name="time_period"/>
    <field labelOnTop="0" name="togeusdate"/>
    <field labelOnTop="0" name="updatedate"/>
    <field labelOnTop="0" name="updateuser"/>
    <field labelOnTop="0" name="url_bor"/>
    <field labelOnTop="0" name="use"/>
    <field labelOnTop="0" name="usechangecause"/>
    <field labelOnTop="0" name="usechangedate"/>
    <field labelOnTop="0" name="utmzone"/>
    <field labelOnTop="0" name="various"/>
    <field labelOnTop="0" name="verticaref"/>
    <field labelOnTop="0" name="workingconditions"/>
    <field labelOnTop="0" name="wwboreholeno"/>
    <field labelOnTop="0" name="xutm"/>
    <field labelOnTop="0" name="xutm32euref89"/>
    <field labelOnTop="0" name="yutm"/>
    <field labelOnTop="0" name="yutm32euref89"/>
    <field labelOnTop="0" name="zdvr90"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="FOHM - Lagtildeling filter - Primær magasin_guid_intake"/>
    <field reuseLastValue="0" name="FOHM - Lagtildeling filter - Primær magasin_guid_screen"/>
    <field reuseLastValue="0" name="FOHM - Lagtildeling filter - Primær magasin_intake_pct"/>
    <field reuseLastValue="0" name="FOHM - Lagtildeling filter - Primær magasin_layer"/>
    <field reuseLastValue="0" name="FOHM - Lagtildeling filter - Primær magasin_layer_num"/>
    <field reuseLastValue="0" name="FOHM - Lagtildeling filter - Primær magasin_litho"/>
    <field reuseLastValue="0" name="FOHM - Lagtildeling filter - Primær magasin_time_period"/>
    <field reuseLastValue="0" name="FOHM_guid_intake"/>
    <field reuseLastValue="0" name="FOHM_guid_screen"/>
    <field reuseLastValue="0" name="FOHM_intake_pct"/>
    <field reuseLastValue="0" name="FOHM_layer"/>
    <field reuseLastValue="0" name="FOHM_layer_num"/>
    <field reuseLastValue="0" name="FOHM_litho"/>
    <field reuseLastValue="0" name="FOHM_time_period"/>
    <field reuseLastValue="0" name="abandcause"/>
    <field reuseLastValue="0" name="abandcontr"/>
    <field reuseLastValue="0" name="abandondat"/>
    <field reuseLastValue="0" name="accessremark"/>
    <field reuseLastValue="0" name="approach"/>
    <field reuseLastValue="0" name="boreholeid"/>
    <field reuseLastValue="0" name="boreholeno"/>
    <field reuseLastValue="0" name="borhpostc"/>
    <field reuseLastValue="0" name="borhtownno"/>
    <field reuseLastValue="0" name="borhtownno2007"/>
    <field reuseLastValue="0" name="comments"/>
    <field reuseLastValue="0" name="companytype"/>
    <field reuseLastValue="0" name="consuborno"/>
    <field reuseLastValue="0" name="consulogno"/>
    <field reuseLastValue="0" name="consultant"/>
    <field reuseLastValue="0" name="countyno"/>
    <field reuseLastValue="0" name="ctrpdescr"/>
    <field reuseLastValue="0" name="ctrpeleva"/>
    <field reuseLastValue="0" name="ctrpheight"/>
    <field reuseLastValue="0" name="ctrpprecis"/>
    <field reuseLastValue="0" name="ctrpzprecis"/>
    <field reuseLastValue="0" name="dataowner"/>
    <field reuseLastValue="0" name="datum"/>
    <field reuseLastValue="0" name="drforadres"/>
    <field reuseLastValue="0" name="drforpostc"/>
    <field reuseLastValue="0" name="drilendate"/>
    <field reuseLastValue="0" name="drillborno"/>
    <field reuseLastValue="0" name="drilldepth"/>
    <field reuseLastValue="0" name="drilledfor"/>
    <field reuseLastValue="0" name="driller"/>
    <field reuseLastValue="0" name="drilllogno"/>
    <field reuseLastValue="0" name="drilstdate"/>
    <field reuseLastValue="0" name="elevametho"/>
    <field reuseLastValue="0" name="elevaquali"/>
    <field reuseLastValue="0" name="elevasourc"/>
    <field reuseLastValue="0" name="elevation"/>
    <field reuseLastValue="0" name="envcen"/>
    <field reuseLastValue="0" name="grumoareano"/>
    <field reuseLastValue="0" name="grumoborno"/>
    <field reuseLastValue="0" name="grumobortype"/>
    <field reuseLastValue="0" name="grumocountyno"/>
    <field reuseLastValue="0" name="guid"/>
    <field reuseLastValue="0" name="guid_intake"/>
    <field reuseLastValue="0" name="guid_screen"/>
    <field reuseLastValue="0" name="houseownas"/>
    <field reuseLastValue="0" name="id"/>
    <field reuseLastValue="0" name="insertdate"/>
    <field reuseLastValue="0" name="insertuser"/>
    <field reuseLastValue="0" name="installation"/>
    <field reuseLastValue="0" name="intake_pct"/>
    <field reuseLastValue="0" name="landregno"/>
    <field reuseLastValue="0" name="latitude"/>
    <field reuseLastValue="0" name="layer"/>
    <field reuseLastValue="0" name="layer_num"/>
    <field reuseLastValue="0" name="lith_screen_agg"/>
    <field reuseLastValue="0" name="litho"/>
    <field reuseLastValue="0" name="litholnote"/>
    <field reuseLastValue="0" name="locatepersonemail"/>
    <field reuseLastValue="0" name="location"/>
    <field reuseLastValue="0" name="locatmetho"/>
    <field reuseLastValue="0" name="locatquali"/>
    <field reuseLastValue="0" name="locatsourc"/>
    <field reuseLastValue="0" name="locquali"/>
    <field reuseLastValue="0" name="longitude"/>
    <field reuseLastValue="0" name="longtext"/>
    <field reuseLastValue="0" name="loopareano"/>
    <field reuseLastValue="0" name="loopstation"/>
    <field reuseLastValue="0" name="looptype"/>
    <field reuseLastValue="0" name="mapdistx"/>
    <field reuseLastValue="0" name="mapdisty"/>
    <field reuseLastValue="0" name="mapsheet"/>
    <field reuseLastValue="0" name="municipal"/>
    <field reuseLastValue="0" name="namingsys"/>
    <field reuseLastValue="0" name="numofsampl"/>
    <field reuseLastValue="0" name="numsamsto"/>
    <field reuseLastValue="0" name="numsuplbor"/>
    <field reuseLastValue="0" name="plantid"/>
    <field reuseLastValue="0" name="preservationzone"/>
    <field reuseLastValue="0" name="prevborhno"/>
    <field reuseLastValue="0" name="protectionzone"/>
    <field reuseLastValue="0" name="purpose"/>
    <field reuseLastValue="0" name="region"/>
    <field reuseLastValue="0" name="reportedby"/>
    <field reuseLastValue="0" name="samdescdat"/>
    <field reuseLastValue="0" name="samrecedat"/>
    <field reuseLastValue="0" name="startdayunknown"/>
    <field reuseLastValue="0" name="startmnthunknwn"/>
    <field reuseLastValue="0" name="status"/>
    <field reuseLastValue="0" name="sys34x"/>
    <field reuseLastValue="0" name="sys34y"/>
    <field reuseLastValue="0" name="sys34zone"/>
    <field reuseLastValue="0" name="time_period"/>
    <field reuseLastValue="0" name="togeusdate"/>
    <field reuseLastValue="0" name="updatedate"/>
    <field reuseLastValue="0" name="updateuser"/>
    <field reuseLastValue="0" name="url_bor"/>
    <field reuseLastValue="0" name="use"/>
    <field reuseLastValue="0" name="usechangecause"/>
    <field reuseLastValue="0" name="usechangedate"/>
    <field reuseLastValue="0" name="utmzone"/>
    <field reuseLastValue="0" name="various"/>
    <field reuseLastValue="0" name="verticaref"/>
    <field reuseLastValue="0" name="workingconditions"/>
    <field reuseLastValue="0" name="wwboreholeno"/>
    <field reuseLastValue="0" name="xutm"/>
    <field reuseLastValue="0" name="xutm32euref89"/>
    <field reuseLastValue="0" name="yutm"/>
    <field reuseLastValue="0" name="yutm32euref89"/>
    <field reuseLastValue="0" name="zdvr90"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"ctrpdescr"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
