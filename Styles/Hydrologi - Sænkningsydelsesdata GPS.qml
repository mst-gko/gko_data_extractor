<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" simplifyMaxScale="1" simplifyDrawingHints="0" symbologyReferenceScale="-1" simplifyDrawingTol="1" minScale="100000000" readOnly="0" hasScaleBasedVisibilityFlag="0" labelsEnabled="0" maxScale="0" simplifyAlgorithm="0" simplifyLocal="1" version="3.26.3-Buenos Aires">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal limitMode="0" accumulate="0" enabled="0" endField="" startField="" durationField="boreholeid" durationUnit="min" endExpression="" fixedDuration="0" mode="0" startExpression="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zoffset="0" clamping="Terrain" showMarkerSymbolInSurfacePlots="0" type="IndividualFeatures" binding="Centroid" symbology="Line" extrusionEnabled="0" extrusion="0" respectLayerSymbol="1" zscale="1">
    <data-defined-properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol name="" frame_rate="10" type="line" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" class="SimpleLine" locked="0">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="square"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="232,113,141,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="0.6"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="232,113,141,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.6"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol name="" frame_rate="10" type="fill" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" class="SimpleFill" locked="0">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="color" type="QString" value="232,113,141,255"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="166,81,101,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="style" type="QString" value="solid"/>
          </Option>
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="232,113,141,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="166,81,101,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol name="" frame_rate="10" type="marker" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="232,113,141,255"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="diamond"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="166,81,101,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="3"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="232,113,141,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="166,81,101,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 forceraster="0" referencescale="-1" type="RuleRenderer" symbollevels="0" enableorderby="0">
    <rules key="{8360c652-9540-4c2e-87bf-ede65fd19708}">
      <rule key="{813124bd-01bc-478b-b054-f76af31de201}" symbol="0" filter="coalesce( &quot;discharge&quot; , &quot;drawdown&quot; , &quot;duration&quot; )is not null and &quot;locatmetho&quot; = 'DG'" label="DGPS"/>
      <rule key="{64c59110-86a3-49ba-91ff-f5a082fd1738}" symbol="1" filter="coalesce( &quot;discharge&quot; , &quot;drawdown&quot; , &quot;duration&quot; )is not null and &quot;locatmetho&quot; = 'G'" label="Håndh. GPS"/>
      <rule key="{38a85da9-8749-4ac1-9c88-d6ddca04d0ec}" symbol="2" filter="coalesce( &quot;discharge&quot; , &quot;drawdown&quot; , &quot;duration&quot; )is not null AND coalesce(&quot;locatmetho&quot;, 'K') NOT IN ('G','DG')" label="Anden metode"/>
    </rules>
    <symbols>
      <symbol name="0" frame_rate="10" type="marker" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="3" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="8,240,70,255"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="circle"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="2"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="8,240,70,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="1" frame_rate="10" type="marker" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="2" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="21,63,249,255"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="circle"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="2"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="21,63,249,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="2" frame_rate="10" type="marker" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="1" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="0,0,0,255"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="circle"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="2"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="0,0,0,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option name="dualview/previewExpressions" type="List">
        <Option type="QString" value="&quot;ctrpdescr&quot;"/>
      </Option>
      <Option name="embeddedWidgets/count" type="int" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory labelPlacementMethod="XHeight" backgroundAlpha="255" sizeType="MM" maxScaleDenominator="1e+08" minScaleDenominator="0" penColor="#000000" scaleBasedVisibility="0" scaleDependency="Area" width="15" spacing="5" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" enabled="0" lineSizeType="MM" barWidth="5" showAxis="1" sizeScale="3x:0,0,0,0,0,0" diagramOrientation="Up" rotationOffset="270" height="15" spacingUnit="MM" spacingUnitScale="3x:0,0,0,0,0,0" direction="0" backgroundColor="#ffffff" opacity="1" penAlpha="255" penWidth="0">
      <fontProperties bold="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" underline="0" style="" italic="0"/>
      <attribute colorOpacity="1" field="" label="" color="#000000"/>
      <axisSymbol>
        <symbol name="" frame_rate="10" type="line" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer enabled="1" pass="0" class="SimpleLine" locked="0">
            <Option type="Map">
              <Option name="align_dash_pattern" type="QString" value="0"/>
              <Option name="capstyle" type="QString" value="square"/>
              <Option name="customdash" type="QString" value="5;2"/>
              <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="customdash_unit" type="QString" value="MM"/>
              <Option name="dash_pattern_offset" type="QString" value="0"/>
              <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
              <Option name="draw_inside_polygon" type="QString" value="0"/>
              <Option name="joinstyle" type="QString" value="bevel"/>
              <Option name="line_color" type="QString" value="35,35,35,255"/>
              <Option name="line_style" type="QString" value="solid"/>
              <Option name="line_width" type="QString" value="0.26"/>
              <Option name="line_width_unit" type="QString" value="MM"/>
              <Option name="offset" type="QString" value="0"/>
              <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="offset_unit" type="QString" value="MM"/>
              <Option name="ring_filter" type="QString" value="0"/>
              <Option name="trim_distance_end" type="QString" value="0"/>
              <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_end_unit" type="QString" value="MM"/>
              <Option name="trim_distance_start" type="QString" value="0"/>
              <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_start_unit" type="QString" value="MM"/>
              <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
              <Option name="use_custom_dash" type="QString" value="0"/>
              <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" obstacle="0" dist="0" placement="0" linePlacementFlags="18" priority="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="longtext" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boreholeid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boreholeno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="namingsys" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="purpose" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="use" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="status" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drilldepth" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="elevation" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ctrpeleva" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="verticaref" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ctrpdescr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ctrpprecis" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ctrpzprecis" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ctrpheight" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="elevametho" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="elevaquali" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="elevasourc" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="location" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="comments" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="various" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="xutm" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="yutm" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="utmzone" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="datum" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mapsheet" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mapdistx" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mapdisty" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sys34x" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sys34y" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sys34zone" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="latitude" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="longitude" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locatmetho" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locatquali" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locatsourc" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="borhpostc" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="borhtownno" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="countyno" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="municipal" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="houseownas" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="landregno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="driller" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drilllogno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drillborno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="reportedby" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="consultant" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="consulogno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="consuborno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drilledfor" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drforadres" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drforpostc" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drilstdate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drilendate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="abandondat" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="prevborhno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="numsuplbor" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="samrecedat" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="samdescdat" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="numofsampl" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="numsamsto" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="litholnote" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="togeusdate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="grumocountyno" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="grumoborno" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="grumobortype" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="grumoareano" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="borhtownno2007" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locquali" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="loopareano" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="loopstation" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="looptype" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="usechangedate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="envcen" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="abandcause" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="abandcontr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="startdayunknown" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="startmnthunknwn" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wwboreholeno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="xutm32euref89" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="yutm32euref89" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="zdvr90" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="installation" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="workingconditions" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="approach" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="accessremark" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locatepersonemail" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="preservationzone" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="protectionzone" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="region" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="usechangecause" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="guid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="insertdate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="updatedate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="insertuser" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="updateuser" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dataowner" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="url_bor" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="discharge" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drawdown" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="duration" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locatmetho_2" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="screen_top" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="screen_bottom" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="id"/>
    <alias index="1" name="" field="longtext"/>
    <alias index="2" name="" field="boreholeid"/>
    <alias index="3" name="" field="boreholeno"/>
    <alias index="4" name="" field="namingsys"/>
    <alias index="5" name="" field="purpose"/>
    <alias index="6" name="" field="use"/>
    <alias index="7" name="" field="status"/>
    <alias index="8" name="" field="drilldepth"/>
    <alias index="9" name="" field="elevation"/>
    <alias index="10" name="" field="ctrpeleva"/>
    <alias index="11" name="" field="verticaref"/>
    <alias index="12" name="" field="ctrpdescr"/>
    <alias index="13" name="" field="ctrpprecis"/>
    <alias index="14" name="" field="ctrpzprecis"/>
    <alias index="15" name="" field="ctrpheight"/>
    <alias index="16" name="" field="elevametho"/>
    <alias index="17" name="" field="elevaquali"/>
    <alias index="18" name="" field="elevasourc"/>
    <alias index="19" name="" field="location"/>
    <alias index="20" name="" field="comments"/>
    <alias index="21" name="" field="various"/>
    <alias index="22" name="" field="xutm"/>
    <alias index="23" name="" field="yutm"/>
    <alias index="24" name="" field="utmzone"/>
    <alias index="25" name="" field="datum"/>
    <alias index="26" name="" field="mapsheet"/>
    <alias index="27" name="" field="mapdistx"/>
    <alias index="28" name="" field="mapdisty"/>
    <alias index="29" name="" field="sys34x"/>
    <alias index="30" name="" field="sys34y"/>
    <alias index="31" name="" field="sys34zone"/>
    <alias index="32" name="" field="latitude"/>
    <alias index="33" name="" field="longitude"/>
    <alias index="34" name="" field="locatmetho"/>
    <alias index="35" name="" field="locatquali"/>
    <alias index="36" name="" field="locatsourc"/>
    <alias index="37" name="" field="borhpostc"/>
    <alias index="38" name="" field="borhtownno"/>
    <alias index="39" name="" field="countyno"/>
    <alias index="40" name="" field="municipal"/>
    <alias index="41" name="" field="houseownas"/>
    <alias index="42" name="" field="landregno"/>
    <alias index="43" name="" field="driller"/>
    <alias index="44" name="" field="drilllogno"/>
    <alias index="45" name="" field="drillborno"/>
    <alias index="46" name="" field="reportedby"/>
    <alias index="47" name="" field="consultant"/>
    <alias index="48" name="" field="consulogno"/>
    <alias index="49" name="" field="consuborno"/>
    <alias index="50" name="" field="drilledfor"/>
    <alias index="51" name="" field="drforadres"/>
    <alias index="52" name="" field="drforpostc"/>
    <alias index="53" name="" field="drilstdate"/>
    <alias index="54" name="" field="drilendate"/>
    <alias index="55" name="" field="abandondat"/>
    <alias index="56" name="" field="prevborhno"/>
    <alias index="57" name="" field="numsuplbor"/>
    <alias index="58" name="" field="samrecedat"/>
    <alias index="59" name="" field="samdescdat"/>
    <alias index="60" name="" field="numofsampl"/>
    <alias index="61" name="" field="numsamsto"/>
    <alias index="62" name="" field="litholnote"/>
    <alias index="63" name="" field="togeusdate"/>
    <alias index="64" name="" field="grumocountyno"/>
    <alias index="65" name="" field="grumoborno"/>
    <alias index="66" name="" field="grumobortype"/>
    <alias index="67" name="" field="grumoareano"/>
    <alias index="68" name="" field="borhtownno2007"/>
    <alias index="69" name="" field="locquali"/>
    <alias index="70" name="" field="loopareano"/>
    <alias index="71" name="" field="loopstation"/>
    <alias index="72" name="" field="looptype"/>
    <alias index="73" name="" field="usechangedate"/>
    <alias index="74" name="" field="envcen"/>
    <alias index="75" name="" field="abandcause"/>
    <alias index="76" name="" field="abandcontr"/>
    <alias index="77" name="" field="startdayunknown"/>
    <alias index="78" name="" field="startmnthunknwn"/>
    <alias index="79" name="" field="wwboreholeno"/>
    <alias index="80" name="" field="xutm32euref89"/>
    <alias index="81" name="" field="yutm32euref89"/>
    <alias index="82" name="" field="zdvr90"/>
    <alias index="83" name="" field="installation"/>
    <alias index="84" name="" field="workingconditions"/>
    <alias index="85" name="" field="approach"/>
    <alias index="86" name="" field="accessremark"/>
    <alias index="87" name="" field="locatepersonemail"/>
    <alias index="88" name="" field="preservationzone"/>
    <alias index="89" name="" field="protectionzone"/>
    <alias index="90" name="" field="region"/>
    <alias index="91" name="" field="usechangecause"/>
    <alias index="92" name="" field="guid"/>
    <alias index="93" name="" field="insertdate"/>
    <alias index="94" name="" field="updatedate"/>
    <alias index="95" name="" field="insertuser"/>
    <alias index="96" name="" field="updateuser"/>
    <alias index="97" name="" field="dataowner"/>
    <alias index="98" name="" field="url_bor"/>
    <alias index="99" name="" field="discharge"/>
    <alias index="100" name="" field="drawdown"/>
    <alias index="101" name="" field="duration"/>
    <alias index="102" name="" field="locatmetho_2"/>
    <alias index="103" name="" field="screen_top"/>
    <alias index="104" name="" field="screen_bottom"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" field="id" expression=""/>
    <default applyOnUpdate="0" field="longtext" expression=""/>
    <default applyOnUpdate="0" field="boreholeid" expression=""/>
    <default applyOnUpdate="0" field="boreholeno" expression=""/>
    <default applyOnUpdate="0" field="namingsys" expression=""/>
    <default applyOnUpdate="0" field="purpose" expression=""/>
    <default applyOnUpdate="0" field="use" expression=""/>
    <default applyOnUpdate="0" field="status" expression=""/>
    <default applyOnUpdate="0" field="drilldepth" expression=""/>
    <default applyOnUpdate="0" field="elevation" expression=""/>
    <default applyOnUpdate="0" field="ctrpeleva" expression=""/>
    <default applyOnUpdate="0" field="verticaref" expression=""/>
    <default applyOnUpdate="0" field="ctrpdescr" expression=""/>
    <default applyOnUpdate="0" field="ctrpprecis" expression=""/>
    <default applyOnUpdate="0" field="ctrpzprecis" expression=""/>
    <default applyOnUpdate="0" field="ctrpheight" expression=""/>
    <default applyOnUpdate="0" field="elevametho" expression=""/>
    <default applyOnUpdate="0" field="elevaquali" expression=""/>
    <default applyOnUpdate="0" field="elevasourc" expression=""/>
    <default applyOnUpdate="0" field="location" expression=""/>
    <default applyOnUpdate="0" field="comments" expression=""/>
    <default applyOnUpdate="0" field="various" expression=""/>
    <default applyOnUpdate="0" field="xutm" expression=""/>
    <default applyOnUpdate="0" field="yutm" expression=""/>
    <default applyOnUpdate="0" field="utmzone" expression=""/>
    <default applyOnUpdate="0" field="datum" expression=""/>
    <default applyOnUpdate="0" field="mapsheet" expression=""/>
    <default applyOnUpdate="0" field="mapdistx" expression=""/>
    <default applyOnUpdate="0" field="mapdisty" expression=""/>
    <default applyOnUpdate="0" field="sys34x" expression=""/>
    <default applyOnUpdate="0" field="sys34y" expression=""/>
    <default applyOnUpdate="0" field="sys34zone" expression=""/>
    <default applyOnUpdate="0" field="latitude" expression=""/>
    <default applyOnUpdate="0" field="longitude" expression=""/>
    <default applyOnUpdate="0" field="locatmetho" expression=""/>
    <default applyOnUpdate="0" field="locatquali" expression=""/>
    <default applyOnUpdate="0" field="locatsourc" expression=""/>
    <default applyOnUpdate="0" field="borhpostc" expression=""/>
    <default applyOnUpdate="0" field="borhtownno" expression=""/>
    <default applyOnUpdate="0" field="countyno" expression=""/>
    <default applyOnUpdate="0" field="municipal" expression=""/>
    <default applyOnUpdate="0" field="houseownas" expression=""/>
    <default applyOnUpdate="0" field="landregno" expression=""/>
    <default applyOnUpdate="0" field="driller" expression=""/>
    <default applyOnUpdate="0" field="drilllogno" expression=""/>
    <default applyOnUpdate="0" field="drillborno" expression=""/>
    <default applyOnUpdate="0" field="reportedby" expression=""/>
    <default applyOnUpdate="0" field="consultant" expression=""/>
    <default applyOnUpdate="0" field="consulogno" expression=""/>
    <default applyOnUpdate="0" field="consuborno" expression=""/>
    <default applyOnUpdate="0" field="drilledfor" expression=""/>
    <default applyOnUpdate="0" field="drforadres" expression=""/>
    <default applyOnUpdate="0" field="drforpostc" expression=""/>
    <default applyOnUpdate="0" field="drilstdate" expression=""/>
    <default applyOnUpdate="0" field="drilendate" expression=""/>
    <default applyOnUpdate="0" field="abandondat" expression=""/>
    <default applyOnUpdate="0" field="prevborhno" expression=""/>
    <default applyOnUpdate="0" field="numsuplbor" expression=""/>
    <default applyOnUpdate="0" field="samrecedat" expression=""/>
    <default applyOnUpdate="0" field="samdescdat" expression=""/>
    <default applyOnUpdate="0" field="numofsampl" expression=""/>
    <default applyOnUpdate="0" field="numsamsto" expression=""/>
    <default applyOnUpdate="0" field="litholnote" expression=""/>
    <default applyOnUpdate="0" field="togeusdate" expression=""/>
    <default applyOnUpdate="0" field="grumocountyno" expression=""/>
    <default applyOnUpdate="0" field="grumoborno" expression=""/>
    <default applyOnUpdate="0" field="grumobortype" expression=""/>
    <default applyOnUpdate="0" field="grumoareano" expression=""/>
    <default applyOnUpdate="0" field="borhtownno2007" expression=""/>
    <default applyOnUpdate="0" field="locquali" expression=""/>
    <default applyOnUpdate="0" field="loopareano" expression=""/>
    <default applyOnUpdate="0" field="loopstation" expression=""/>
    <default applyOnUpdate="0" field="looptype" expression=""/>
    <default applyOnUpdate="0" field="usechangedate" expression=""/>
    <default applyOnUpdate="0" field="envcen" expression=""/>
    <default applyOnUpdate="0" field="abandcause" expression=""/>
    <default applyOnUpdate="0" field="abandcontr" expression=""/>
    <default applyOnUpdate="0" field="startdayunknown" expression=""/>
    <default applyOnUpdate="0" field="startmnthunknwn" expression=""/>
    <default applyOnUpdate="0" field="wwboreholeno" expression=""/>
    <default applyOnUpdate="0" field="xutm32euref89" expression=""/>
    <default applyOnUpdate="0" field="yutm32euref89" expression=""/>
    <default applyOnUpdate="0" field="zdvr90" expression=""/>
    <default applyOnUpdate="0" field="installation" expression=""/>
    <default applyOnUpdate="0" field="workingconditions" expression=""/>
    <default applyOnUpdate="0" field="approach" expression=""/>
    <default applyOnUpdate="0" field="accessremark" expression=""/>
    <default applyOnUpdate="0" field="locatepersonemail" expression=""/>
    <default applyOnUpdate="0" field="preservationzone" expression=""/>
    <default applyOnUpdate="0" field="protectionzone" expression=""/>
    <default applyOnUpdate="0" field="region" expression=""/>
    <default applyOnUpdate="0" field="usechangecause" expression=""/>
    <default applyOnUpdate="0" field="guid" expression=""/>
    <default applyOnUpdate="0" field="insertdate" expression=""/>
    <default applyOnUpdate="0" field="updatedate" expression=""/>
    <default applyOnUpdate="0" field="insertuser" expression=""/>
    <default applyOnUpdate="0" field="updateuser" expression=""/>
    <default applyOnUpdate="0" field="dataowner" expression=""/>
    <default applyOnUpdate="0" field="url_bor" expression=""/>
    <default applyOnUpdate="0" field="discharge" expression=""/>
    <default applyOnUpdate="0" field="drawdown" expression=""/>
    <default applyOnUpdate="0" field="duration" expression=""/>
    <default applyOnUpdate="0" field="locatmetho_2" expression=""/>
    <default applyOnUpdate="0" field="screen_top" expression=""/>
    <default applyOnUpdate="0" field="screen_bottom" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" field="id" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="longtext" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="1" field="boreholeid" constraints="2" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="1" field="boreholeno" constraints="2" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="namingsys" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="purpose" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="use" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="status" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="drilldepth" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="elevation" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="ctrpeleva" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="verticaref" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="ctrpdescr" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="ctrpprecis" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="ctrpzprecis" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="ctrpheight" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="elevametho" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="elevaquali" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="elevasourc" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="location" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="comments" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="various" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="xutm" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="yutm" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="utmzone" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="datum" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="mapsheet" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="mapdistx" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="mapdisty" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="sys34x" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="sys34y" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="sys34zone" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="latitude" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="longitude" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="locatmetho" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="locatquali" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="locatsourc" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="borhpostc" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="borhtownno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="countyno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="municipal" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="houseownas" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="landregno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="driller" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="drilllogno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="drillborno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="reportedby" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="consultant" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="consulogno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="consuborno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="drilledfor" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="drforadres" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="drforpostc" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="drilstdate" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="drilendate" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="abandondat" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="prevborhno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="numsuplbor" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="samrecedat" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="samdescdat" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="numofsampl" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="numsamsto" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="litholnote" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="togeusdate" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="grumocountyno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="grumoborno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="grumobortype" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="grumoareano" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="borhtownno2007" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="locquali" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="loopareano" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="loopstation" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="looptype" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="usechangedate" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="envcen" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="abandcause" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="abandcontr" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="startdayunknown" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="startmnthunknwn" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="wwboreholeno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="xutm32euref89" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="yutm32euref89" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="zdvr90" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="installation" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="workingconditions" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="approach" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="accessremark" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="locatepersonemail" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="preservationzone" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="protectionzone" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="region" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="usechangecause" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="1" field="guid" constraints="3" exp_strength="0" notnull_strength="1"/>
    <constraint unique_strength="0" field="insertdate" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="updatedate" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="insertuser" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="updateuser" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="dataowner" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="url_bor" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="discharge" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="drawdown" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="duration" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="locatmetho_2" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="screen_top" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="screen_bottom" constraints="0" exp_strength="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id"/>
    <constraint desc="" exp="" field="longtext"/>
    <constraint desc="" exp="" field="boreholeid"/>
    <constraint desc="" exp="" field="boreholeno"/>
    <constraint desc="" exp="" field="namingsys"/>
    <constraint desc="" exp="" field="purpose"/>
    <constraint desc="" exp="" field="use"/>
    <constraint desc="" exp="" field="status"/>
    <constraint desc="" exp="" field="drilldepth"/>
    <constraint desc="" exp="" field="elevation"/>
    <constraint desc="" exp="" field="ctrpeleva"/>
    <constraint desc="" exp="" field="verticaref"/>
    <constraint desc="" exp="" field="ctrpdescr"/>
    <constraint desc="" exp="" field="ctrpprecis"/>
    <constraint desc="" exp="" field="ctrpzprecis"/>
    <constraint desc="" exp="" field="ctrpheight"/>
    <constraint desc="" exp="" field="elevametho"/>
    <constraint desc="" exp="" field="elevaquali"/>
    <constraint desc="" exp="" field="elevasourc"/>
    <constraint desc="" exp="" field="location"/>
    <constraint desc="" exp="" field="comments"/>
    <constraint desc="" exp="" field="various"/>
    <constraint desc="" exp="" field="xutm"/>
    <constraint desc="" exp="" field="yutm"/>
    <constraint desc="" exp="" field="utmzone"/>
    <constraint desc="" exp="" field="datum"/>
    <constraint desc="" exp="" field="mapsheet"/>
    <constraint desc="" exp="" field="mapdistx"/>
    <constraint desc="" exp="" field="mapdisty"/>
    <constraint desc="" exp="" field="sys34x"/>
    <constraint desc="" exp="" field="sys34y"/>
    <constraint desc="" exp="" field="sys34zone"/>
    <constraint desc="" exp="" field="latitude"/>
    <constraint desc="" exp="" field="longitude"/>
    <constraint desc="" exp="" field="locatmetho"/>
    <constraint desc="" exp="" field="locatquali"/>
    <constraint desc="" exp="" field="locatsourc"/>
    <constraint desc="" exp="" field="borhpostc"/>
    <constraint desc="" exp="" field="borhtownno"/>
    <constraint desc="" exp="" field="countyno"/>
    <constraint desc="" exp="" field="municipal"/>
    <constraint desc="" exp="" field="houseownas"/>
    <constraint desc="" exp="" field="landregno"/>
    <constraint desc="" exp="" field="driller"/>
    <constraint desc="" exp="" field="drilllogno"/>
    <constraint desc="" exp="" field="drillborno"/>
    <constraint desc="" exp="" field="reportedby"/>
    <constraint desc="" exp="" field="consultant"/>
    <constraint desc="" exp="" field="consulogno"/>
    <constraint desc="" exp="" field="consuborno"/>
    <constraint desc="" exp="" field="drilledfor"/>
    <constraint desc="" exp="" field="drforadres"/>
    <constraint desc="" exp="" field="drforpostc"/>
    <constraint desc="" exp="" field="drilstdate"/>
    <constraint desc="" exp="" field="drilendate"/>
    <constraint desc="" exp="" field="abandondat"/>
    <constraint desc="" exp="" field="prevborhno"/>
    <constraint desc="" exp="" field="numsuplbor"/>
    <constraint desc="" exp="" field="samrecedat"/>
    <constraint desc="" exp="" field="samdescdat"/>
    <constraint desc="" exp="" field="numofsampl"/>
    <constraint desc="" exp="" field="numsamsto"/>
    <constraint desc="" exp="" field="litholnote"/>
    <constraint desc="" exp="" field="togeusdate"/>
    <constraint desc="" exp="" field="grumocountyno"/>
    <constraint desc="" exp="" field="grumoborno"/>
    <constraint desc="" exp="" field="grumobortype"/>
    <constraint desc="" exp="" field="grumoareano"/>
    <constraint desc="" exp="" field="borhtownno2007"/>
    <constraint desc="" exp="" field="locquali"/>
    <constraint desc="" exp="" field="loopareano"/>
    <constraint desc="" exp="" field="loopstation"/>
    <constraint desc="" exp="" field="looptype"/>
    <constraint desc="" exp="" field="usechangedate"/>
    <constraint desc="" exp="" field="envcen"/>
    <constraint desc="" exp="" field="abandcause"/>
    <constraint desc="" exp="" field="abandcontr"/>
    <constraint desc="" exp="" field="startdayunknown"/>
    <constraint desc="" exp="" field="startmnthunknwn"/>
    <constraint desc="" exp="" field="wwboreholeno"/>
    <constraint desc="" exp="" field="xutm32euref89"/>
    <constraint desc="" exp="" field="yutm32euref89"/>
    <constraint desc="" exp="" field="zdvr90"/>
    <constraint desc="" exp="" field="installation"/>
    <constraint desc="" exp="" field="workingconditions"/>
    <constraint desc="" exp="" field="approach"/>
    <constraint desc="" exp="" field="accessremark"/>
    <constraint desc="" exp="" field="locatepersonemail"/>
    <constraint desc="" exp="" field="preservationzone"/>
    <constraint desc="" exp="" field="protectionzone"/>
    <constraint desc="" exp="" field="region"/>
    <constraint desc="" exp="" field="usechangecause"/>
    <constraint desc="" exp="" field="guid"/>
    <constraint desc="" exp="" field="insertdate"/>
    <constraint desc="" exp="" field="updatedate"/>
    <constraint desc="" exp="" field="insertuser"/>
    <constraint desc="" exp="" field="updateuser"/>
    <constraint desc="" exp="" field="dataowner"/>
    <constraint desc="" exp="" field="url_bor"/>
    <constraint desc="" exp="" field="discharge"/>
    <constraint desc="" exp="" field="drawdown"/>
    <constraint desc="" exp="" field="duration"/>
    <constraint desc="" exp="" field="locatmetho_2"/>
    <constraint desc="" exp="" field="screen_top"/>
    <constraint desc="" exp="" field="screen_bottom"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column name="boreholeid" width="-1" hidden="0" type="field"/>
      <column name="boreholeno" width="-1" hidden="0" type="field"/>
      <column name="namingsys" width="-1" hidden="0" type="field"/>
      <column name="purpose" width="-1" hidden="0" type="field"/>
      <column name="use" width="-1" hidden="0" type="field"/>
      <column name="status" width="-1" hidden="0" type="field"/>
      <column name="drilldepth" width="-1" hidden="0" type="field"/>
      <column name="elevation" width="-1" hidden="0" type="field"/>
      <column name="ctrpeleva" width="-1" hidden="0" type="field"/>
      <column name="verticaref" width="-1" hidden="0" type="field"/>
      <column name="ctrpdescr" width="-1" hidden="0" type="field"/>
      <column name="ctrpprecis" width="-1" hidden="0" type="field"/>
      <column name="ctrpheight" width="-1" hidden="0" type="field"/>
      <column name="elevametho" width="-1" hidden="0" type="field"/>
      <column name="elevaquali" width="-1" hidden="0" type="field"/>
      <column name="elevasourc" width="-1" hidden="0" type="field"/>
      <column name="location" width="-1" hidden="0" type="field"/>
      <column name="comments" width="-1" hidden="0" type="field"/>
      <column name="various" width="-1" hidden="0" type="field"/>
      <column name="xutm" width="-1" hidden="0" type="field"/>
      <column name="yutm" width="-1" hidden="0" type="field"/>
      <column name="utmzone" width="-1" hidden="0" type="field"/>
      <column name="datum" width="-1" hidden="0" type="field"/>
      <column name="mapsheet" width="-1" hidden="0" type="field"/>
      <column name="mapdistx" width="-1" hidden="0" type="field"/>
      <column name="mapdisty" width="-1" hidden="0" type="field"/>
      <column name="sys34x" width="-1" hidden="0" type="field"/>
      <column name="sys34y" width="-1" hidden="0" type="field"/>
      <column name="sys34zone" width="-1" hidden="0" type="field"/>
      <column name="latitude" width="-1" hidden="0" type="field"/>
      <column name="longitude" width="-1" hidden="0" type="field"/>
      <column name="locatmetho" width="-1" hidden="0" type="field"/>
      <column name="locatquali" width="-1" hidden="0" type="field"/>
      <column name="locatsourc" width="-1" hidden="0" type="field"/>
      <column name="borhpostc" width="-1" hidden="0" type="field"/>
      <column name="borhtownno" width="-1" hidden="0" type="field"/>
      <column name="countyno" width="-1" hidden="0" type="field"/>
      <column name="municipal" width="-1" hidden="0" type="field"/>
      <column name="houseownas" width="-1" hidden="0" type="field"/>
      <column name="landregno" width="-1" hidden="0" type="field"/>
      <column name="driller" width="-1" hidden="0" type="field"/>
      <column name="drilllogno" width="-1" hidden="0" type="field"/>
      <column name="drillborno" width="-1" hidden="0" type="field"/>
      <column name="reportedby" width="-1" hidden="0" type="field"/>
      <column name="consultant" width="-1" hidden="0" type="field"/>
      <column name="consulogno" width="-1" hidden="0" type="field"/>
      <column name="consuborno" width="-1" hidden="0" type="field"/>
      <column name="drilledfor" width="-1" hidden="0" type="field"/>
      <column name="drforadres" width="-1" hidden="0" type="field"/>
      <column name="drforpostc" width="-1" hidden="0" type="field"/>
      <column name="drilstdate" width="-1" hidden="0" type="field"/>
      <column name="drilendate" width="-1" hidden="0" type="field"/>
      <column name="abandondat" width="-1" hidden="0" type="field"/>
      <column name="prevborhno" width="-1" hidden="0" type="field"/>
      <column name="numsuplbor" width="-1" hidden="0" type="field"/>
      <column name="samrecedat" width="-1" hidden="0" type="field"/>
      <column name="samdescdat" width="-1" hidden="0" type="field"/>
      <column name="numofsampl" width="-1" hidden="0" type="field"/>
      <column name="numsamsto" width="-1" hidden="0" type="field"/>
      <column name="litholnote" width="-1" hidden="0" type="field"/>
      <column name="togeusdate" width="-1" hidden="0" type="field"/>
      <column name="grumoborno" width="-1" hidden="0" type="field"/>
      <column name="locquali" width="-1" hidden="0" type="field"/>
      <column name="loopareano" width="-1" hidden="0" type="field"/>
      <column name="looptype" width="-1" hidden="0" type="field"/>
      <column name="envcen" width="-1" hidden="0" type="field"/>
      <column name="abandcause" width="-1" hidden="0" type="field"/>
      <column name="abandcontr" width="-1" hidden="0" type="field"/>
      <column name="zdvr90" width="-1" hidden="0" type="field"/>
      <column name="approach" width="-1" hidden="0" type="field"/>
      <column name="region" width="-1" hidden="0" type="field"/>
      <column name="guid" width="-1" hidden="0" type="field"/>
      <column name="insertdate" width="-1" hidden="0" type="field"/>
      <column name="updatedate" width="-1" hidden="0" type="field"/>
      <column name="insertuser" width="-1" hidden="0" type="field"/>
      <column name="updateuser" width="-1" hidden="0" type="field"/>
      <column name="dataowner" width="-1" hidden="0" type="field"/>
      <column name="id" width="-1" hidden="0" type="field"/>
      <column name="longtext" width="-1" hidden="0" type="field"/>
      <column name="url_bor" width="-1" hidden="0" type="field"/>
      <column name="discharge" width="-1" hidden="0" type="field"/>
      <column name="drawdown" width="-1" hidden="0" type="field"/>
      <column name="duration" width="-1" hidden="0" type="field"/>
      <column name="screen_top" width="-1" hidden="0" type="field"/>
      <column name="ctrpzprecis" width="-1" hidden="0" type="field"/>
      <column name="grumocountyno" width="-1" hidden="0" type="field"/>
      <column name="grumobortype" width="-1" hidden="0" type="field"/>
      <column name="grumoareano" width="-1" hidden="0" type="field"/>
      <column name="borhtownno2007" width="-1" hidden="0" type="field"/>
      <column name="loopstation" width="-1" hidden="0" type="field"/>
      <column name="usechangedate" width="-1" hidden="0" type="field"/>
      <column name="startdayunknown" width="-1" hidden="0" type="field"/>
      <column name="startmnthunknwn" width="-1" hidden="0" type="field"/>
      <column name="wwboreholeno" width="-1" hidden="0" type="field"/>
      <column name="xutm32euref89" width="-1" hidden="0" type="field"/>
      <column name="yutm32euref89" width="-1" hidden="0" type="field"/>
      <column name="installation" width="-1" hidden="0" type="field"/>
      <column name="workingconditions" width="-1" hidden="0" type="field"/>
      <column name="accessremark" width="-1" hidden="0" type="field"/>
      <column name="locatepersonemail" width="-1" hidden="0" type="field"/>
      <column name="preservationzone" width="-1" hidden="0" type="field"/>
      <column name="protectionzone" width="-1" hidden="0" type="field"/>
      <column name="usechangecause" width="-1" hidden="0" type="field"/>
      <column name="locatmetho_2" width="-1" hidden="0" type="field"/>
      <column name="screen_bottom" width="-1" hidden="0" type="field"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="Pejlinger - Status på filtersætning_borehole_locatmetho" editable="0"/>
    <field name="Pejlinger - Status på filtersætning_screen_bottom" editable="0"/>
    <field name="Pejlinger - Status på filtersætning_screen_top" editable="0"/>
    <field name="Pejlinger - Status på filtersætning_screen_topbotquali" editable="0"/>
    <field name="abandcause" editable="1"/>
    <field name="abandcontr" editable="1"/>
    <field name="abandondat" editable="1"/>
    <field name="accessrema" editable="1"/>
    <field name="accessremark" editable="1"/>
    <field name="approach" editable="1"/>
    <field name="boreholeid" editable="1"/>
    <field name="boreholeno" editable="1"/>
    <field name="borhpostc" editable="1"/>
    <field name="borhtown_1" editable="1"/>
    <field name="borhtownno" editable="1"/>
    <field name="borhtownno2007" editable="1"/>
    <field name="comments" editable="1"/>
    <field name="consuborno" editable="1"/>
    <field name="consulogno" editable="1"/>
    <field name="consultant" editable="1"/>
    <field name="countyno" editable="1"/>
    <field name="ctrpdescr" editable="1"/>
    <field name="ctrpeleva" editable="1"/>
    <field name="ctrpheight" editable="1"/>
    <field name="ctrpprecis" editable="1"/>
    <field name="ctrpzpreci" editable="1"/>
    <field name="ctrpzprecis" editable="1"/>
    <field name="dataowner" editable="1"/>
    <field name="datum" editable="1"/>
    <field name="discharg_discharge" editable="0"/>
    <field name="discharg_drawdown" editable="0"/>
    <field name="discharg_duration" editable="0"/>
    <field name="discharge" editable="1"/>
    <field name="drawdown" editable="1"/>
    <field name="drforadres" editable="1"/>
    <field name="drforpostc" editable="1"/>
    <field name="drilendate" editable="1"/>
    <field name="drillborno" editable="1"/>
    <field name="drilldepth" editable="1"/>
    <field name="drilledfor" editable="1"/>
    <field name="driller" editable="1"/>
    <field name="drilllogno" editable="1"/>
    <field name="drilstdate" editable="1"/>
    <field name="duration" editable="1"/>
    <field name="elevametho" editable="1"/>
    <field name="elevaquali" editable="1"/>
    <field name="elevasourc" editable="1"/>
    <field name="elevation" editable="1"/>
    <field name="envcen" editable="1"/>
    <field name="grumoarean" editable="1"/>
    <field name="grumoareano" editable="1"/>
    <field name="grumoborno" editable="1"/>
    <field name="grumoborty" editable="1"/>
    <field name="grumobortype" editable="1"/>
    <field name="grumocount" editable="1"/>
    <field name="grumocountyno" editable="1"/>
    <field name="guid" editable="1"/>
    <field name="houseownas" editable="1"/>
    <field name="id" editable="1"/>
    <field name="insertdate" editable="1"/>
    <field name="insertuser" editable="1"/>
    <field name="installati" editable="1"/>
    <field name="installation" editable="1"/>
    <field name="landregno" editable="1"/>
    <field name="latitude" editable="1"/>
    <field name="litholnote" editable="1"/>
    <field name="locatepers" editable="1"/>
    <field name="locatepersonemail" editable="1"/>
    <field name="location" editable="1"/>
    <field name="locatmet_1" editable="1"/>
    <field name="locatmetho" editable="1"/>
    <field name="locatmetho_2" editable="1"/>
    <field name="locatquali" editable="1"/>
    <field name="locatsourc" editable="1"/>
    <field name="locquali" editable="1"/>
    <field name="longitude" editable="1"/>
    <field name="longtext" editable="1"/>
    <field name="loopareano" editable="1"/>
    <field name="loopstatio" editable="1"/>
    <field name="loopstation" editable="1"/>
    <field name="looptype" editable="1"/>
    <field name="mapdistx" editable="1"/>
    <field name="mapdisty" editable="1"/>
    <field name="mapsheet" editable="1"/>
    <field name="municipal" editable="1"/>
    <field name="namingsys" editable="1"/>
    <field name="numofsampl" editable="1"/>
    <field name="numsamsto" editable="1"/>
    <field name="numsuplbor" editable="1"/>
    <field name="preservati" editable="1"/>
    <field name="preservationzone" editable="1"/>
    <field name="prevborhno" editable="1"/>
    <field name="protection" editable="1"/>
    <field name="protectionzone" editable="1"/>
    <field name="purpose" editable="1"/>
    <field name="region" editable="1"/>
    <field name="reportedby" editable="1"/>
    <field name="samdescdat" editable="1"/>
    <field name="samrecedat" editable="1"/>
    <field name="screen_bottom" editable="1"/>
    <field name="screen_top" editable="1"/>
    <field name="startdayun" editable="1"/>
    <field name="startdayunknown" editable="1"/>
    <field name="startmnthu" editable="1"/>
    <field name="startmnthunknwn" editable="1"/>
    <field name="status" editable="1"/>
    <field name="sys34x" editable="1"/>
    <field name="sys34y" editable="1"/>
    <field name="sys34zone" editable="1"/>
    <field name="togeusdate" editable="1"/>
    <field name="updatedate" editable="1"/>
    <field name="updateuser" editable="1"/>
    <field name="url_bor" editable="1"/>
    <field name="use" editable="1"/>
    <field name="usechangec" editable="1"/>
    <field name="usechangecause" editable="1"/>
    <field name="usechanged" editable="1"/>
    <field name="usechangedate" editable="1"/>
    <field name="utmzone" editable="1"/>
    <field name="various" editable="1"/>
    <field name="verticaref" editable="1"/>
    <field name="workingcon" editable="1"/>
    <field name="workingconditions" editable="1"/>
    <field name="wwborehole" editable="1"/>
    <field name="wwboreholeno" editable="1"/>
    <field name="xutm" editable="1"/>
    <field name="xutm32eure" editable="1"/>
    <field name="xutm32euref89" editable="1"/>
    <field name="yutm" editable="1"/>
    <field name="yutm32eure" editable="1"/>
    <field name="yutm32euref89" editable="1"/>
    <field name="zdvr90" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="Pejlinger - Status på filtersætning_borehole_locatmetho" labelOnTop="0"/>
    <field name="Pejlinger - Status på filtersætning_screen_bottom" labelOnTop="0"/>
    <field name="Pejlinger - Status på filtersætning_screen_top" labelOnTop="0"/>
    <field name="Pejlinger - Status på filtersætning_screen_topbotquali" labelOnTop="0"/>
    <field name="abandcause" labelOnTop="0"/>
    <field name="abandcontr" labelOnTop="0"/>
    <field name="abandondat" labelOnTop="0"/>
    <field name="accessrema" labelOnTop="0"/>
    <field name="accessremark" labelOnTop="0"/>
    <field name="approach" labelOnTop="0"/>
    <field name="boreholeid" labelOnTop="0"/>
    <field name="boreholeno" labelOnTop="0"/>
    <field name="borhpostc" labelOnTop="0"/>
    <field name="borhtown_1" labelOnTop="0"/>
    <field name="borhtownno" labelOnTop="0"/>
    <field name="borhtownno2007" labelOnTop="0"/>
    <field name="comments" labelOnTop="0"/>
    <field name="consuborno" labelOnTop="0"/>
    <field name="consulogno" labelOnTop="0"/>
    <field name="consultant" labelOnTop="0"/>
    <field name="countyno" labelOnTop="0"/>
    <field name="ctrpdescr" labelOnTop="0"/>
    <field name="ctrpeleva" labelOnTop="0"/>
    <field name="ctrpheight" labelOnTop="0"/>
    <field name="ctrpprecis" labelOnTop="0"/>
    <field name="ctrpzpreci" labelOnTop="0"/>
    <field name="ctrpzprecis" labelOnTop="0"/>
    <field name="dataowner" labelOnTop="0"/>
    <field name="datum" labelOnTop="0"/>
    <field name="discharg_discharge" labelOnTop="0"/>
    <field name="discharg_drawdown" labelOnTop="0"/>
    <field name="discharg_duration" labelOnTop="0"/>
    <field name="discharge" labelOnTop="0"/>
    <field name="drawdown" labelOnTop="0"/>
    <field name="drforadres" labelOnTop="0"/>
    <field name="drforpostc" labelOnTop="0"/>
    <field name="drilendate" labelOnTop="0"/>
    <field name="drillborno" labelOnTop="0"/>
    <field name="drilldepth" labelOnTop="0"/>
    <field name="drilledfor" labelOnTop="0"/>
    <field name="driller" labelOnTop="0"/>
    <field name="drilllogno" labelOnTop="0"/>
    <field name="drilstdate" labelOnTop="0"/>
    <field name="duration" labelOnTop="0"/>
    <field name="elevametho" labelOnTop="0"/>
    <field name="elevaquali" labelOnTop="0"/>
    <field name="elevasourc" labelOnTop="0"/>
    <field name="elevation" labelOnTop="0"/>
    <field name="envcen" labelOnTop="0"/>
    <field name="grumoarean" labelOnTop="0"/>
    <field name="grumoareano" labelOnTop="0"/>
    <field name="grumoborno" labelOnTop="0"/>
    <field name="grumoborty" labelOnTop="0"/>
    <field name="grumobortype" labelOnTop="0"/>
    <field name="grumocount" labelOnTop="0"/>
    <field name="grumocountyno" labelOnTop="0"/>
    <field name="guid" labelOnTop="0"/>
    <field name="houseownas" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="insertdate" labelOnTop="0"/>
    <field name="insertuser" labelOnTop="0"/>
    <field name="installati" labelOnTop="0"/>
    <field name="installation" labelOnTop="0"/>
    <field name="landregno" labelOnTop="0"/>
    <field name="latitude" labelOnTop="0"/>
    <field name="litholnote" labelOnTop="0"/>
    <field name="locatepers" labelOnTop="0"/>
    <field name="locatepersonemail" labelOnTop="0"/>
    <field name="location" labelOnTop="0"/>
    <field name="locatmet_1" labelOnTop="0"/>
    <field name="locatmetho" labelOnTop="0"/>
    <field name="locatmetho_2" labelOnTop="0"/>
    <field name="locatquali" labelOnTop="0"/>
    <field name="locatsourc" labelOnTop="0"/>
    <field name="locquali" labelOnTop="0"/>
    <field name="longitude" labelOnTop="0"/>
    <field name="longtext" labelOnTop="0"/>
    <field name="loopareano" labelOnTop="0"/>
    <field name="loopstatio" labelOnTop="0"/>
    <field name="loopstation" labelOnTop="0"/>
    <field name="looptype" labelOnTop="0"/>
    <field name="mapdistx" labelOnTop="0"/>
    <field name="mapdisty" labelOnTop="0"/>
    <field name="mapsheet" labelOnTop="0"/>
    <field name="municipal" labelOnTop="0"/>
    <field name="namingsys" labelOnTop="0"/>
    <field name="numofsampl" labelOnTop="0"/>
    <field name="numsamsto" labelOnTop="0"/>
    <field name="numsuplbor" labelOnTop="0"/>
    <field name="preservati" labelOnTop="0"/>
    <field name="preservationzone" labelOnTop="0"/>
    <field name="prevborhno" labelOnTop="0"/>
    <field name="protection" labelOnTop="0"/>
    <field name="protectionzone" labelOnTop="0"/>
    <field name="purpose" labelOnTop="0"/>
    <field name="region" labelOnTop="0"/>
    <field name="reportedby" labelOnTop="0"/>
    <field name="samdescdat" labelOnTop="0"/>
    <field name="samrecedat" labelOnTop="0"/>
    <field name="screen_bottom" labelOnTop="0"/>
    <field name="screen_top" labelOnTop="0"/>
    <field name="startdayun" labelOnTop="0"/>
    <field name="startdayunknown" labelOnTop="0"/>
    <field name="startmnthu" labelOnTop="0"/>
    <field name="startmnthunknwn" labelOnTop="0"/>
    <field name="status" labelOnTop="0"/>
    <field name="sys34x" labelOnTop="0"/>
    <field name="sys34y" labelOnTop="0"/>
    <field name="sys34zone" labelOnTop="0"/>
    <field name="togeusdate" labelOnTop="0"/>
    <field name="updatedate" labelOnTop="0"/>
    <field name="updateuser" labelOnTop="0"/>
    <field name="url_bor" labelOnTop="0"/>
    <field name="use" labelOnTop="0"/>
    <field name="usechangec" labelOnTop="0"/>
    <field name="usechangecause" labelOnTop="0"/>
    <field name="usechanged" labelOnTop="0"/>
    <field name="usechangedate" labelOnTop="0"/>
    <field name="utmzone" labelOnTop="0"/>
    <field name="various" labelOnTop="0"/>
    <field name="verticaref" labelOnTop="0"/>
    <field name="workingcon" labelOnTop="0"/>
    <field name="workingconditions" labelOnTop="0"/>
    <field name="wwborehole" labelOnTop="0"/>
    <field name="wwboreholeno" labelOnTop="0"/>
    <field name="xutm" labelOnTop="0"/>
    <field name="xutm32eure" labelOnTop="0"/>
    <field name="xutm32euref89" labelOnTop="0"/>
    <field name="yutm" labelOnTop="0"/>
    <field name="yutm32eure" labelOnTop="0"/>
    <field name="yutm32euref89" labelOnTop="0"/>
    <field name="zdvr90" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="Pejlinger - Status på filtersætning_borehole_locatmetho" reuseLastValue="0"/>
    <field name="Pejlinger - Status på filtersætning_screen_bottom" reuseLastValue="0"/>
    <field name="Pejlinger - Status på filtersætning_screen_top" reuseLastValue="0"/>
    <field name="Pejlinger - Status på filtersætning_screen_topbotquali" reuseLastValue="0"/>
    <field name="abandcause" reuseLastValue="0"/>
    <field name="abandcontr" reuseLastValue="0"/>
    <field name="abandondat" reuseLastValue="0"/>
    <field name="accessrema" reuseLastValue="0"/>
    <field name="accessremark" reuseLastValue="0"/>
    <field name="approach" reuseLastValue="0"/>
    <field name="boreholeid" reuseLastValue="0"/>
    <field name="boreholeno" reuseLastValue="0"/>
    <field name="borhpostc" reuseLastValue="0"/>
    <field name="borhtown_1" reuseLastValue="0"/>
    <field name="borhtownno" reuseLastValue="0"/>
    <field name="borhtownno2007" reuseLastValue="0"/>
    <field name="comments" reuseLastValue="0"/>
    <field name="consuborno" reuseLastValue="0"/>
    <field name="consulogno" reuseLastValue="0"/>
    <field name="consultant" reuseLastValue="0"/>
    <field name="countyno" reuseLastValue="0"/>
    <field name="ctrpdescr" reuseLastValue="0"/>
    <field name="ctrpeleva" reuseLastValue="0"/>
    <field name="ctrpheight" reuseLastValue="0"/>
    <field name="ctrpprecis" reuseLastValue="0"/>
    <field name="ctrpzpreci" reuseLastValue="0"/>
    <field name="ctrpzprecis" reuseLastValue="0"/>
    <field name="dataowner" reuseLastValue="0"/>
    <field name="datum" reuseLastValue="0"/>
    <field name="discharg_discharge" reuseLastValue="0"/>
    <field name="discharg_drawdown" reuseLastValue="0"/>
    <field name="discharg_duration" reuseLastValue="0"/>
    <field name="discharge" reuseLastValue="0"/>
    <field name="drawdown" reuseLastValue="0"/>
    <field name="drforadres" reuseLastValue="0"/>
    <field name="drforpostc" reuseLastValue="0"/>
    <field name="drilendate" reuseLastValue="0"/>
    <field name="drillborno" reuseLastValue="0"/>
    <field name="drilldepth" reuseLastValue="0"/>
    <field name="drilledfor" reuseLastValue="0"/>
    <field name="driller" reuseLastValue="0"/>
    <field name="drilllogno" reuseLastValue="0"/>
    <field name="drilstdate" reuseLastValue="0"/>
    <field name="duration" reuseLastValue="0"/>
    <field name="elevametho" reuseLastValue="0"/>
    <field name="elevaquali" reuseLastValue="0"/>
    <field name="elevasourc" reuseLastValue="0"/>
    <field name="elevation" reuseLastValue="0"/>
    <field name="envcen" reuseLastValue="0"/>
    <field name="grumoarean" reuseLastValue="0"/>
    <field name="grumoareano" reuseLastValue="0"/>
    <field name="grumoborno" reuseLastValue="0"/>
    <field name="grumoborty" reuseLastValue="0"/>
    <field name="grumobortype" reuseLastValue="0"/>
    <field name="grumocount" reuseLastValue="0"/>
    <field name="grumocountyno" reuseLastValue="0"/>
    <field name="guid" reuseLastValue="0"/>
    <field name="houseownas" reuseLastValue="0"/>
    <field name="id" reuseLastValue="0"/>
    <field name="insertdate" reuseLastValue="0"/>
    <field name="insertuser" reuseLastValue="0"/>
    <field name="installati" reuseLastValue="0"/>
    <field name="installation" reuseLastValue="0"/>
    <field name="landregno" reuseLastValue="0"/>
    <field name="latitude" reuseLastValue="0"/>
    <field name="litholnote" reuseLastValue="0"/>
    <field name="locatepers" reuseLastValue="0"/>
    <field name="locatepersonemail" reuseLastValue="0"/>
    <field name="location" reuseLastValue="0"/>
    <field name="locatmet_1" reuseLastValue="0"/>
    <field name="locatmetho" reuseLastValue="0"/>
    <field name="locatmetho_2" reuseLastValue="0"/>
    <field name="locatquali" reuseLastValue="0"/>
    <field name="locatsourc" reuseLastValue="0"/>
    <field name="locquali" reuseLastValue="0"/>
    <field name="longitude" reuseLastValue="0"/>
    <field name="longtext" reuseLastValue="0"/>
    <field name="loopareano" reuseLastValue="0"/>
    <field name="loopstatio" reuseLastValue="0"/>
    <field name="loopstation" reuseLastValue="0"/>
    <field name="looptype" reuseLastValue="0"/>
    <field name="mapdistx" reuseLastValue="0"/>
    <field name="mapdisty" reuseLastValue="0"/>
    <field name="mapsheet" reuseLastValue="0"/>
    <field name="municipal" reuseLastValue="0"/>
    <field name="namingsys" reuseLastValue="0"/>
    <field name="numofsampl" reuseLastValue="0"/>
    <field name="numsamsto" reuseLastValue="0"/>
    <field name="numsuplbor" reuseLastValue="0"/>
    <field name="preservati" reuseLastValue="0"/>
    <field name="preservationzone" reuseLastValue="0"/>
    <field name="prevborhno" reuseLastValue="0"/>
    <field name="protection" reuseLastValue="0"/>
    <field name="protectionzone" reuseLastValue="0"/>
    <field name="purpose" reuseLastValue="0"/>
    <field name="region" reuseLastValue="0"/>
    <field name="reportedby" reuseLastValue="0"/>
    <field name="samdescdat" reuseLastValue="0"/>
    <field name="samrecedat" reuseLastValue="0"/>
    <field name="screen_bottom" reuseLastValue="0"/>
    <field name="screen_top" reuseLastValue="0"/>
    <field name="startdayun" reuseLastValue="0"/>
    <field name="startdayunknown" reuseLastValue="0"/>
    <field name="startmnthu" reuseLastValue="0"/>
    <field name="startmnthunknwn" reuseLastValue="0"/>
    <field name="status" reuseLastValue="0"/>
    <field name="sys34x" reuseLastValue="0"/>
    <field name="sys34y" reuseLastValue="0"/>
    <field name="sys34zone" reuseLastValue="0"/>
    <field name="togeusdate" reuseLastValue="0"/>
    <field name="updatedate" reuseLastValue="0"/>
    <field name="updateuser" reuseLastValue="0"/>
    <field name="url_bor" reuseLastValue="0"/>
    <field name="use" reuseLastValue="0"/>
    <field name="usechangec" reuseLastValue="0"/>
    <field name="usechangecause" reuseLastValue="0"/>
    <field name="usechanged" reuseLastValue="0"/>
    <field name="usechangedate" reuseLastValue="0"/>
    <field name="utmzone" reuseLastValue="0"/>
    <field name="various" reuseLastValue="0"/>
    <field name="verticaref" reuseLastValue="0"/>
    <field name="workingcon" reuseLastValue="0"/>
    <field name="workingconditions" reuseLastValue="0"/>
    <field name="wwborehole" reuseLastValue="0"/>
    <field name="wwboreholeno" reuseLastValue="0"/>
    <field name="xutm" reuseLastValue="0"/>
    <field name="xutm32eure" reuseLastValue="0"/>
    <field name="xutm32euref89" reuseLastValue="0"/>
    <field name="yutm" reuseLastValue="0"/>
    <field name="yutm32eure" reuseLastValue="0"/>
    <field name="yutm32euref89" reuseLastValue="0"/>
    <field name="zdvr90" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"ctrpdescr"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
