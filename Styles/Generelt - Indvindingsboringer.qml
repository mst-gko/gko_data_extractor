<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.26.3-Buenos Aires" symbologyReferenceScale="-1" simplifyLocal="1" minScale="100000000" simplifyAlgorithm="0" labelsEnabled="0" simplifyDrawingTol="1" readOnly="0" styleCategories="AllStyleCategories" simplifyDrawingHints="0" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal durationField="" limitMode="0" durationUnit="min" accumulate="0" startField="" endField="" fixedDuration="0" startExpression="" mode="0" endExpression="" enabled="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation binding="Centroid" zscale="1" extrusionEnabled="0" symbology="Line" clamping="Terrain" type="IndividualFeatures" showMarkerSymbolInSurfacePlots="0" extrusion="0" zoffset="0" respectLayerSymbol="1">
    <data-defined-properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol frame_rate="10" alpha="1" type="line" name="" is_animated="0" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="align_dash_pattern"/>
            <Option value="square" type="QString" name="capstyle"/>
            <Option value="5;2" type="QString" name="customdash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
            <Option value="MM" type="QString" name="customdash_unit"/>
            <Option value="0" type="QString" name="dash_pattern_offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
            <Option value="0" type="QString" name="draw_inside_polygon"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="141,90,153,255" type="QString" name="line_color"/>
            <Option value="solid" type="QString" name="line_style"/>
            <Option value="0.6" type="QString" name="line_width"/>
            <Option value="MM" type="QString" name="line_width_unit"/>
            <Option value="0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="0" type="QString" name="ring_filter"/>
            <Option value="0" type="QString" name="trim_distance_end"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_end_unit"/>
            <Option value="0" type="QString" name="trim_distance_start"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_start_unit"/>
            <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
            <Option value="0" type="QString" name="use_custom_dash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="141,90,153,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.6"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol frame_rate="10" alpha="1" type="fill" name="" is_animated="0" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" type="QString" name="border_width_map_unit_scale"/>
            <Option value="141,90,153,255" type="QString" name="color"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="101,64,109,255" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.2" type="QString" name="outline_width"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="solid" type="QString" name="style"/>
          </Option>
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="141,90,153,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="101,64,109,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol frame_rate="10" alpha="1" type="marker" name="" is_animated="0" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="angle"/>
            <Option value="square" type="QString" name="cap_style"/>
            <Option value="141,90,153,255" type="QString" name="color"/>
            <Option value="1" type="QString" name="horizontal_anchor_point"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="diamond" type="QString" name="name"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="101,64,109,255" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.2" type="QString" name="outline_width"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="outline_width_map_unit_scale"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="diameter" type="QString" name="scale_method"/>
            <Option value="3" type="QString" name="size"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="size_map_unit_scale"/>
            <Option value="MM" type="QString" name="size_unit"/>
            <Option value="1" type="QString" name="vertical_anchor_point"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="141,90,153,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="101,64,109,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 forceraster="0" enableorderby="0" referencescale="-1" type="RuleRenderer" symbollevels="0">
    <rules key="{904a0a89-1888-4910-877e-9a7ec1ad9930}">
      <rule key="{2d8b644e-2ca5-4e03-8a1c-dd6d56d9e500}" label="Indvindingsboringer" symbol="0" filter="longtext IN ('Offentlige fælles vandforsyningsanlæg', 'Private fælles vandforsyningsanlæg') and upper(use) NOT IN ('S', 'PA', 'M') AND  &quot;abandondat&quot; IS NULL"/>
    </rules>
    <symbols>
      <symbol frame_rate="10" alpha="1" type="marker" name="0" is_animated="0" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="angle"/>
            <Option value="square" type="QString" name="cap_style"/>
            <Option value="245,182,8,255" type="QString" name="color"/>
            <Option value="1" type="QString" name="horizontal_anchor_point"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="circle" type="QString" name="name"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="35,35,35,255" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0" type="QString" name="outline_width"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="outline_width_map_unit_scale"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="diameter" type="QString" name="scale_method"/>
            <Option value="2" type="QString" name="size"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="size_map_unit_scale"/>
            <Option value="MM" type="QString" name="size_unit"/>
            <Option value="1" type="QString" name="vertical_anchor_point"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="245,182,8,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option value="&quot;ctrpdescr&quot;" type="QString"/>
      </Option>
      <Option value="0" type="QString" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory barWidth="5" scaleBasedVisibility="0" spacingUnit="MM" penWidth="0" spacing="0" penAlpha="255" minScaleDenominator="0" backgroundColor="#ffffff" lineSizeType="MM" enabled="0" width="15" opacity="1" diagramOrientation="Up" lineSizeScale="3x:0,0,0,0,0,0" height="15" labelPlacementMethod="XHeight" rotationOffset="270" direction="1" sizeType="MM" backgroundAlpha="255" spacingUnitScale="3x:0,0,0,0,0,0" scaleDependency="Area" showAxis="0" sizeScale="3x:0,0,0,0,0,0" maxScaleDenominator="1e+08" minimumSize="0" penColor="#000000">
      <fontProperties bold="0" strikethrough="0" style="" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
      <attribute color="#000000" label="" colorOpacity="1" field=""/>
      <axisSymbol>
        <symbol frame_rate="10" alpha="1" type="line" name="" is_animated="0" clip_to_extent="1" force_rhr="0">
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" pass="0" enabled="1" locked="0">
            <Option type="Map">
              <Option value="0" type="QString" name="align_dash_pattern"/>
              <Option value="square" type="QString" name="capstyle"/>
              <Option value="5;2" type="QString" name="customdash"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
              <Option value="MM" type="QString" name="customdash_unit"/>
              <Option value="0" type="QString" name="dash_pattern_offset"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
              <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
              <Option value="0" type="QString" name="draw_inside_polygon"/>
              <Option value="bevel" type="QString" name="joinstyle"/>
              <Option value="35,35,35,255" type="QString" name="line_color"/>
              <Option value="solid" type="QString" name="line_style"/>
              <Option value="0.26" type="QString" name="line_width"/>
              <Option value="MM" type="QString" name="line_width_unit"/>
              <Option value="0" type="QString" name="offset"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
              <Option value="MM" type="QString" name="offset_unit"/>
              <Option value="0" type="QString" name="ring_filter"/>
              <Option value="0" type="QString" name="trim_distance_end"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
              <Option value="MM" type="QString" name="trim_distance_end_unit"/>
              <Option value="0" type="QString" name="trim_distance_start"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
              <Option value="MM" type="QString" name="trim_distance_start_unit"/>
              <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
              <Option value="0" type="QString" name="use_custom_dash"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" priority="0" showAll="1" dist="0" placement="0" zIndex="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="longtext" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="companytype" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="plantid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boreholeid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boreholeno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="namingsys" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="purpose" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="use" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="status" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drilldepth" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="elevation" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ctrpeleva" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="verticaref" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ctrpdescr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ctrpprecis" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ctrpzprecis" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ctrpheight" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="elevametho" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="elevaquali" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="elevasourc" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="location" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="comments" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="various" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="xutm" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="yutm" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="utmzone" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="datum" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mapsheet" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mapdistx" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mapdisty" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sys34x" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sys34y" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sys34zone" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="latitude" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="longitude" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locatmetho" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locatquali" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locatsourc" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="borhpostc" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="borhtownno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="countyno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="municipal" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="houseownas" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="landregno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="driller" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drilllogno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drillborno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="reportedby" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="consultant" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="consulogno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="consuborno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drilledfor" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drforadres" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drforpostc" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drilstdate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drilendate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="abandondat" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="prevborhno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="numsuplbor" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="samrecedat" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="samdescdat" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="numofsampl" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="numsamsto" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="litholnote" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="togeusdate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="grumocountyno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="grumoborno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="grumobortype" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="grumoareano" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="borhtownno2007" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locquali" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="loopareano" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="loopstation" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="looptype" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="usechangedate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="envcen" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="abandcause" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="abandcontr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="startdayunknown" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="startmnthunknwn" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wwboreholeno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="xutm32euref89" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="yutm32euref89" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="zdvr90" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="installation" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="workingconditions" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="approach" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="accessremark" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locatepersonemail" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="preservationzone" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="protectionzone" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="region" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="usechangecause" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="guid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="insertdate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="updatedate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="insertuser" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="updateuser" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dataowner" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="url_bor" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="id"/>
    <alias index="1" name="" field="longtext"/>
    <alias index="2" name="" field="companytype"/>
    <alias index="3" name="" field="plantid"/>
    <alias index="4" name="" field="boreholeid"/>
    <alias index="5" name="" field="boreholeno"/>
    <alias index="6" name="" field="namingsys"/>
    <alias index="7" name="" field="purpose"/>
    <alias index="8" name="" field="use"/>
    <alias index="9" name="" field="status"/>
    <alias index="10" name="" field="drilldepth"/>
    <alias index="11" name="" field="elevation"/>
    <alias index="12" name="" field="ctrpeleva"/>
    <alias index="13" name="" field="verticaref"/>
    <alias index="14" name="" field="ctrpdescr"/>
    <alias index="15" name="" field="ctrpprecis"/>
    <alias index="16" name="" field="ctrpzprecis"/>
    <alias index="17" name="" field="ctrpheight"/>
    <alias index="18" name="" field="elevametho"/>
    <alias index="19" name="" field="elevaquali"/>
    <alias index="20" name="" field="elevasourc"/>
    <alias index="21" name="" field="location"/>
    <alias index="22" name="" field="comments"/>
    <alias index="23" name="" field="various"/>
    <alias index="24" name="" field="xutm"/>
    <alias index="25" name="" field="yutm"/>
    <alias index="26" name="" field="utmzone"/>
    <alias index="27" name="" field="datum"/>
    <alias index="28" name="" field="mapsheet"/>
    <alias index="29" name="" field="mapdistx"/>
    <alias index="30" name="" field="mapdisty"/>
    <alias index="31" name="" field="sys34x"/>
    <alias index="32" name="" field="sys34y"/>
    <alias index="33" name="" field="sys34zone"/>
    <alias index="34" name="" field="latitude"/>
    <alias index="35" name="" field="longitude"/>
    <alias index="36" name="" field="locatmetho"/>
    <alias index="37" name="" field="locatquali"/>
    <alias index="38" name="" field="locatsourc"/>
    <alias index="39" name="" field="borhpostc"/>
    <alias index="40" name="" field="borhtownno"/>
    <alias index="41" name="" field="countyno"/>
    <alias index="42" name="" field="municipal"/>
    <alias index="43" name="" field="houseownas"/>
    <alias index="44" name="" field="landregno"/>
    <alias index="45" name="" field="driller"/>
    <alias index="46" name="" field="drilllogno"/>
    <alias index="47" name="" field="drillborno"/>
    <alias index="48" name="" field="reportedby"/>
    <alias index="49" name="" field="consultant"/>
    <alias index="50" name="" field="consulogno"/>
    <alias index="51" name="" field="consuborno"/>
    <alias index="52" name="" field="drilledfor"/>
    <alias index="53" name="" field="drforadres"/>
    <alias index="54" name="" field="drforpostc"/>
    <alias index="55" name="" field="drilstdate"/>
    <alias index="56" name="" field="drilendate"/>
    <alias index="57" name="" field="abandondat"/>
    <alias index="58" name="" field="prevborhno"/>
    <alias index="59" name="" field="numsuplbor"/>
    <alias index="60" name="" field="samrecedat"/>
    <alias index="61" name="" field="samdescdat"/>
    <alias index="62" name="" field="numofsampl"/>
    <alias index="63" name="" field="numsamsto"/>
    <alias index="64" name="" field="litholnote"/>
    <alias index="65" name="" field="togeusdate"/>
    <alias index="66" name="" field="grumocountyno"/>
    <alias index="67" name="" field="grumoborno"/>
    <alias index="68" name="" field="grumobortype"/>
    <alias index="69" name="" field="grumoareano"/>
    <alias index="70" name="" field="borhtownno2007"/>
    <alias index="71" name="" field="locquali"/>
    <alias index="72" name="" field="loopareano"/>
    <alias index="73" name="" field="loopstation"/>
    <alias index="74" name="" field="looptype"/>
    <alias index="75" name="" field="usechangedate"/>
    <alias index="76" name="" field="envcen"/>
    <alias index="77" name="" field="abandcause"/>
    <alias index="78" name="" field="abandcontr"/>
    <alias index="79" name="" field="startdayunknown"/>
    <alias index="80" name="" field="startmnthunknwn"/>
    <alias index="81" name="" field="wwboreholeno"/>
    <alias index="82" name="" field="xutm32euref89"/>
    <alias index="83" name="" field="yutm32euref89"/>
    <alias index="84" name="" field="zdvr90"/>
    <alias index="85" name="" field="installation"/>
    <alias index="86" name="" field="workingconditions"/>
    <alias index="87" name="" field="approach"/>
    <alias index="88" name="" field="accessremark"/>
    <alias index="89" name="" field="locatepersonemail"/>
    <alias index="90" name="" field="preservationzone"/>
    <alias index="91" name="" field="protectionzone"/>
    <alias index="92" name="" field="region"/>
    <alias index="93" name="" field="usechangecause"/>
    <alias index="94" name="" field="guid"/>
    <alias index="95" name="" field="insertdate"/>
    <alias index="96" name="" field="updatedate"/>
    <alias index="97" name="" field="insertuser"/>
    <alias index="98" name="" field="updateuser"/>
    <alias index="99" name="" field="dataowner"/>
    <alias index="100" name="" field="url_bor"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id"/>
    <default applyOnUpdate="0" expression="" field="longtext"/>
    <default applyOnUpdate="0" expression="" field="companytype"/>
    <default applyOnUpdate="0" expression="" field="plantid"/>
    <default applyOnUpdate="0" expression="" field="boreholeid"/>
    <default applyOnUpdate="0" expression="" field="boreholeno"/>
    <default applyOnUpdate="0" expression="" field="namingsys"/>
    <default applyOnUpdate="0" expression="" field="purpose"/>
    <default applyOnUpdate="0" expression="" field="use"/>
    <default applyOnUpdate="0" expression="" field="status"/>
    <default applyOnUpdate="0" expression="" field="drilldepth"/>
    <default applyOnUpdate="0" expression="" field="elevation"/>
    <default applyOnUpdate="0" expression="" field="ctrpeleva"/>
    <default applyOnUpdate="0" expression="" field="verticaref"/>
    <default applyOnUpdate="0" expression="" field="ctrpdescr"/>
    <default applyOnUpdate="0" expression="" field="ctrpprecis"/>
    <default applyOnUpdate="0" expression="" field="ctrpzprecis"/>
    <default applyOnUpdate="0" expression="" field="ctrpheight"/>
    <default applyOnUpdate="0" expression="" field="elevametho"/>
    <default applyOnUpdate="0" expression="" field="elevaquali"/>
    <default applyOnUpdate="0" expression="" field="elevasourc"/>
    <default applyOnUpdate="0" expression="" field="location"/>
    <default applyOnUpdate="0" expression="" field="comments"/>
    <default applyOnUpdate="0" expression="" field="various"/>
    <default applyOnUpdate="0" expression="" field="xutm"/>
    <default applyOnUpdate="0" expression="" field="yutm"/>
    <default applyOnUpdate="0" expression="" field="utmzone"/>
    <default applyOnUpdate="0" expression="" field="datum"/>
    <default applyOnUpdate="0" expression="" field="mapsheet"/>
    <default applyOnUpdate="0" expression="" field="mapdistx"/>
    <default applyOnUpdate="0" expression="" field="mapdisty"/>
    <default applyOnUpdate="0" expression="" field="sys34x"/>
    <default applyOnUpdate="0" expression="" field="sys34y"/>
    <default applyOnUpdate="0" expression="" field="sys34zone"/>
    <default applyOnUpdate="0" expression="" field="latitude"/>
    <default applyOnUpdate="0" expression="" field="longitude"/>
    <default applyOnUpdate="0" expression="" field="locatmetho"/>
    <default applyOnUpdate="0" expression="" field="locatquali"/>
    <default applyOnUpdate="0" expression="" field="locatsourc"/>
    <default applyOnUpdate="0" expression="" field="borhpostc"/>
    <default applyOnUpdate="0" expression="" field="borhtownno"/>
    <default applyOnUpdate="0" expression="" field="countyno"/>
    <default applyOnUpdate="0" expression="" field="municipal"/>
    <default applyOnUpdate="0" expression="" field="houseownas"/>
    <default applyOnUpdate="0" expression="" field="landregno"/>
    <default applyOnUpdate="0" expression="" field="driller"/>
    <default applyOnUpdate="0" expression="" field="drilllogno"/>
    <default applyOnUpdate="0" expression="" field="drillborno"/>
    <default applyOnUpdate="0" expression="" field="reportedby"/>
    <default applyOnUpdate="0" expression="" field="consultant"/>
    <default applyOnUpdate="0" expression="" field="consulogno"/>
    <default applyOnUpdate="0" expression="" field="consuborno"/>
    <default applyOnUpdate="0" expression="" field="drilledfor"/>
    <default applyOnUpdate="0" expression="" field="drforadres"/>
    <default applyOnUpdate="0" expression="" field="drforpostc"/>
    <default applyOnUpdate="0" expression="" field="drilstdate"/>
    <default applyOnUpdate="0" expression="" field="drilendate"/>
    <default applyOnUpdate="0" expression="" field="abandondat"/>
    <default applyOnUpdate="0" expression="" field="prevborhno"/>
    <default applyOnUpdate="0" expression="" field="numsuplbor"/>
    <default applyOnUpdate="0" expression="" field="samrecedat"/>
    <default applyOnUpdate="0" expression="" field="samdescdat"/>
    <default applyOnUpdate="0" expression="" field="numofsampl"/>
    <default applyOnUpdate="0" expression="" field="numsamsto"/>
    <default applyOnUpdate="0" expression="" field="litholnote"/>
    <default applyOnUpdate="0" expression="" field="togeusdate"/>
    <default applyOnUpdate="0" expression="" field="grumocountyno"/>
    <default applyOnUpdate="0" expression="" field="grumoborno"/>
    <default applyOnUpdate="0" expression="" field="grumobortype"/>
    <default applyOnUpdate="0" expression="" field="grumoareano"/>
    <default applyOnUpdate="0" expression="" field="borhtownno2007"/>
    <default applyOnUpdate="0" expression="" field="locquali"/>
    <default applyOnUpdate="0" expression="" field="loopareano"/>
    <default applyOnUpdate="0" expression="" field="loopstation"/>
    <default applyOnUpdate="0" expression="" field="looptype"/>
    <default applyOnUpdate="0" expression="" field="usechangedate"/>
    <default applyOnUpdate="0" expression="" field="envcen"/>
    <default applyOnUpdate="0" expression="" field="abandcause"/>
    <default applyOnUpdate="0" expression="" field="abandcontr"/>
    <default applyOnUpdate="0" expression="" field="startdayunknown"/>
    <default applyOnUpdate="0" expression="" field="startmnthunknwn"/>
    <default applyOnUpdate="0" expression="" field="wwboreholeno"/>
    <default applyOnUpdate="0" expression="" field="xutm32euref89"/>
    <default applyOnUpdate="0" expression="" field="yutm32euref89"/>
    <default applyOnUpdate="0" expression="" field="zdvr90"/>
    <default applyOnUpdate="0" expression="" field="installation"/>
    <default applyOnUpdate="0" expression="" field="workingconditions"/>
    <default applyOnUpdate="0" expression="" field="approach"/>
    <default applyOnUpdate="0" expression="" field="accessremark"/>
    <default applyOnUpdate="0" expression="" field="locatepersonemail"/>
    <default applyOnUpdate="0" expression="" field="preservationzone"/>
    <default applyOnUpdate="0" expression="" field="protectionzone"/>
    <default applyOnUpdate="0" expression="" field="region"/>
    <default applyOnUpdate="0" expression="" field="usechangecause"/>
    <default applyOnUpdate="0" expression="" field="guid"/>
    <default applyOnUpdate="0" expression="" field="insertdate"/>
    <default applyOnUpdate="0" expression="" field="updatedate"/>
    <default applyOnUpdate="0" expression="" field="insertuser"/>
    <default applyOnUpdate="0" expression="" field="updateuser"/>
    <default applyOnUpdate="0" expression="" field="dataowner"/>
    <default applyOnUpdate="0" expression="" field="url_bor"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" constraints="3" exp_strength="0" notnull_strength="1" field="id"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="longtext"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="companytype"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="plantid"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="boreholeid"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="boreholeno"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="namingsys"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="purpose"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="use"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="status"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="drilldepth"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="elevation"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="ctrpeleva"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="verticaref"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="ctrpdescr"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="ctrpprecis"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="ctrpzprecis"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="ctrpheight"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="elevametho"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="elevaquali"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="elevasourc"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="location"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="comments"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="various"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="xutm"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="yutm"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="utmzone"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="datum"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="mapsheet"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="mapdistx"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="mapdisty"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="sys34x"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="sys34y"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="sys34zone"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="latitude"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="longitude"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="locatmetho"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="locatquali"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="locatsourc"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="borhpostc"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="borhtownno"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="countyno"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="municipal"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="houseownas"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="landregno"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="driller"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="drilllogno"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="drillborno"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="reportedby"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="consultant"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="consulogno"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="consuborno"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="drilledfor"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="drforadres"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="drforpostc"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="drilstdate"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="drilendate"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="abandondat"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="prevborhno"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="numsuplbor"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="samrecedat"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="samdescdat"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="numofsampl"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="numsamsto"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="litholnote"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="togeusdate"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="grumocountyno"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="grumoborno"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="grumobortype"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="grumoareano"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="borhtownno2007"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="locquali"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="loopareano"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="loopstation"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="looptype"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="usechangedate"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="envcen"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="abandcause"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="abandcontr"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="startdayunknown"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="startmnthunknwn"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="wwboreholeno"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="xutm32euref89"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="yutm32euref89"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="zdvr90"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="installation"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="workingconditions"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="approach"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="accessremark"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="locatepersonemail"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="preservationzone"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="protectionzone"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="region"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="usechangecause"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="guid"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="insertdate"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="updatedate"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="insertuser"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="updateuser"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="dataowner"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="url_bor"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id"/>
    <constraint desc="" exp="" field="longtext"/>
    <constraint desc="" exp="" field="companytype"/>
    <constraint desc="" exp="" field="plantid"/>
    <constraint desc="" exp="" field="boreholeid"/>
    <constraint desc="" exp="" field="boreholeno"/>
    <constraint desc="" exp="" field="namingsys"/>
    <constraint desc="" exp="" field="purpose"/>
    <constraint desc="" exp="" field="use"/>
    <constraint desc="" exp="" field="status"/>
    <constraint desc="" exp="" field="drilldepth"/>
    <constraint desc="" exp="" field="elevation"/>
    <constraint desc="" exp="" field="ctrpeleva"/>
    <constraint desc="" exp="" field="verticaref"/>
    <constraint desc="" exp="" field="ctrpdescr"/>
    <constraint desc="" exp="" field="ctrpprecis"/>
    <constraint desc="" exp="" field="ctrpzprecis"/>
    <constraint desc="" exp="" field="ctrpheight"/>
    <constraint desc="" exp="" field="elevametho"/>
    <constraint desc="" exp="" field="elevaquali"/>
    <constraint desc="" exp="" field="elevasourc"/>
    <constraint desc="" exp="" field="location"/>
    <constraint desc="" exp="" field="comments"/>
    <constraint desc="" exp="" field="various"/>
    <constraint desc="" exp="" field="xutm"/>
    <constraint desc="" exp="" field="yutm"/>
    <constraint desc="" exp="" field="utmzone"/>
    <constraint desc="" exp="" field="datum"/>
    <constraint desc="" exp="" field="mapsheet"/>
    <constraint desc="" exp="" field="mapdistx"/>
    <constraint desc="" exp="" field="mapdisty"/>
    <constraint desc="" exp="" field="sys34x"/>
    <constraint desc="" exp="" field="sys34y"/>
    <constraint desc="" exp="" field="sys34zone"/>
    <constraint desc="" exp="" field="latitude"/>
    <constraint desc="" exp="" field="longitude"/>
    <constraint desc="" exp="" field="locatmetho"/>
    <constraint desc="" exp="" field="locatquali"/>
    <constraint desc="" exp="" field="locatsourc"/>
    <constraint desc="" exp="" field="borhpostc"/>
    <constraint desc="" exp="" field="borhtownno"/>
    <constraint desc="" exp="" field="countyno"/>
    <constraint desc="" exp="" field="municipal"/>
    <constraint desc="" exp="" field="houseownas"/>
    <constraint desc="" exp="" field="landregno"/>
    <constraint desc="" exp="" field="driller"/>
    <constraint desc="" exp="" field="drilllogno"/>
    <constraint desc="" exp="" field="drillborno"/>
    <constraint desc="" exp="" field="reportedby"/>
    <constraint desc="" exp="" field="consultant"/>
    <constraint desc="" exp="" field="consulogno"/>
    <constraint desc="" exp="" field="consuborno"/>
    <constraint desc="" exp="" field="drilledfor"/>
    <constraint desc="" exp="" field="drforadres"/>
    <constraint desc="" exp="" field="drforpostc"/>
    <constraint desc="" exp="" field="drilstdate"/>
    <constraint desc="" exp="" field="drilendate"/>
    <constraint desc="" exp="" field="abandondat"/>
    <constraint desc="" exp="" field="prevborhno"/>
    <constraint desc="" exp="" field="numsuplbor"/>
    <constraint desc="" exp="" field="samrecedat"/>
    <constraint desc="" exp="" field="samdescdat"/>
    <constraint desc="" exp="" field="numofsampl"/>
    <constraint desc="" exp="" field="numsamsto"/>
    <constraint desc="" exp="" field="litholnote"/>
    <constraint desc="" exp="" field="togeusdate"/>
    <constraint desc="" exp="" field="grumocountyno"/>
    <constraint desc="" exp="" field="grumoborno"/>
    <constraint desc="" exp="" field="grumobortype"/>
    <constraint desc="" exp="" field="grumoareano"/>
    <constraint desc="" exp="" field="borhtownno2007"/>
    <constraint desc="" exp="" field="locquali"/>
    <constraint desc="" exp="" field="loopareano"/>
    <constraint desc="" exp="" field="loopstation"/>
    <constraint desc="" exp="" field="looptype"/>
    <constraint desc="" exp="" field="usechangedate"/>
    <constraint desc="" exp="" field="envcen"/>
    <constraint desc="" exp="" field="abandcause"/>
    <constraint desc="" exp="" field="abandcontr"/>
    <constraint desc="" exp="" field="startdayunknown"/>
    <constraint desc="" exp="" field="startmnthunknwn"/>
    <constraint desc="" exp="" field="wwboreholeno"/>
    <constraint desc="" exp="" field="xutm32euref89"/>
    <constraint desc="" exp="" field="yutm32euref89"/>
    <constraint desc="" exp="" field="zdvr90"/>
    <constraint desc="" exp="" field="installation"/>
    <constraint desc="" exp="" field="workingconditions"/>
    <constraint desc="" exp="" field="approach"/>
    <constraint desc="" exp="" field="accessremark"/>
    <constraint desc="" exp="" field="locatepersonemail"/>
    <constraint desc="" exp="" field="preservationzone"/>
    <constraint desc="" exp="" field="protectionzone"/>
    <constraint desc="" exp="" field="region"/>
    <constraint desc="" exp="" field="usechangecause"/>
    <constraint desc="" exp="" field="guid"/>
    <constraint desc="" exp="" field="insertdate"/>
    <constraint desc="" exp="" field="updatedate"/>
    <constraint desc="" exp="" field="insertuser"/>
    <constraint desc="" exp="" field="updateuser"/>
    <constraint desc="" exp="" field="dataowner"/>
    <constraint desc="" exp="" field="url_bor"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="1" sortExpression="&quot;purpose&quot;" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" hidden="0" type="field" name="id"/>
      <column width="194" hidden="0" type="field" name="longtext"/>
      <column width="-1" hidden="0" type="field" name="companytype"/>
      <column width="-1" hidden="0" type="field" name="plantid"/>
      <column width="-1" hidden="0" type="field" name="boreholeid"/>
      <column width="-1" hidden="0" type="field" name="boreholeno"/>
      <column width="-1" hidden="0" type="field" name="namingsys"/>
      <column width="-1" hidden="0" type="field" name="purpose"/>
      <column width="-1" hidden="0" type="field" name="use"/>
      <column width="-1" hidden="0" type="field" name="status"/>
      <column width="-1" hidden="0" type="field" name="drilldepth"/>
      <column width="-1" hidden="0" type="field" name="elevation"/>
      <column width="-1" hidden="0" type="field" name="ctrpeleva"/>
      <column width="-1" hidden="0" type="field" name="verticaref"/>
      <column width="-1" hidden="0" type="field" name="ctrpdescr"/>
      <column width="-1" hidden="0" type="field" name="ctrpprecis"/>
      <column width="-1" hidden="0" type="field" name="ctrpzprecis"/>
      <column width="-1" hidden="0" type="field" name="ctrpheight"/>
      <column width="-1" hidden="0" type="field" name="elevametho"/>
      <column width="-1" hidden="0" type="field" name="elevaquali"/>
      <column width="-1" hidden="0" type="field" name="elevasourc"/>
      <column width="207" hidden="0" type="field" name="location"/>
      <column width="-1" hidden="0" type="field" name="comments"/>
      <column width="-1" hidden="0" type="field" name="various"/>
      <column width="-1" hidden="0" type="field" name="xutm"/>
      <column width="-1" hidden="0" type="field" name="yutm"/>
      <column width="-1" hidden="0" type="field" name="utmzone"/>
      <column width="-1" hidden="0" type="field" name="datum"/>
      <column width="-1" hidden="0" type="field" name="mapsheet"/>
      <column width="-1" hidden="0" type="field" name="mapdistx"/>
      <column width="-1" hidden="0" type="field" name="mapdisty"/>
      <column width="-1" hidden="0" type="field" name="sys34x"/>
      <column width="-1" hidden="0" type="field" name="sys34y"/>
      <column width="-1" hidden="0" type="field" name="sys34zone"/>
      <column width="-1" hidden="0" type="field" name="latitude"/>
      <column width="-1" hidden="0" type="field" name="longitude"/>
      <column width="-1" hidden="0" type="field" name="locatmetho"/>
      <column width="-1" hidden="0" type="field" name="locatquali"/>
      <column width="-1" hidden="0" type="field" name="locatsourc"/>
      <column width="-1" hidden="0" type="field" name="borhpostc"/>
      <column width="-1" hidden="0" type="field" name="borhtownno"/>
      <column width="-1" hidden="0" type="field" name="countyno"/>
      <column width="-1" hidden="0" type="field" name="municipal"/>
      <column width="-1" hidden="0" type="field" name="houseownas"/>
      <column width="-1" hidden="0" type="field" name="landregno"/>
      <column width="-1" hidden="0" type="field" name="driller"/>
      <column width="-1" hidden="0" type="field" name="drilllogno"/>
      <column width="-1" hidden="0" type="field" name="drillborno"/>
      <column width="-1" hidden="0" type="field" name="reportedby"/>
      <column width="-1" hidden="0" type="field" name="consultant"/>
      <column width="-1" hidden="0" type="field" name="consulogno"/>
      <column width="-1" hidden="0" type="field" name="consuborno"/>
      <column width="-1" hidden="0" type="field" name="drilledfor"/>
      <column width="-1" hidden="0" type="field" name="drforadres"/>
      <column width="-1" hidden="0" type="field" name="drforpostc"/>
      <column width="-1" hidden="0" type="field" name="drilstdate"/>
      <column width="-1" hidden="0" type="field" name="drilendate"/>
      <column width="-1" hidden="0" type="field" name="abandondat"/>
      <column width="-1" hidden="0" type="field" name="prevborhno"/>
      <column width="-1" hidden="0" type="field" name="numsuplbor"/>
      <column width="-1" hidden="0" type="field" name="samrecedat"/>
      <column width="-1" hidden="0" type="field" name="samdescdat"/>
      <column width="-1" hidden="0" type="field" name="numofsampl"/>
      <column width="-1" hidden="0" type="field" name="numsamsto"/>
      <column width="-1" hidden="0" type="field" name="litholnote"/>
      <column width="-1" hidden="0" type="field" name="togeusdate"/>
      <column width="-1" hidden="0" type="field" name="grumocountyno"/>
      <column width="-1" hidden="0" type="field" name="grumoborno"/>
      <column width="-1" hidden="0" type="field" name="grumobortype"/>
      <column width="-1" hidden="0" type="field" name="grumoareano"/>
      <column width="-1" hidden="0" type="field" name="borhtownno2007"/>
      <column width="-1" hidden="0" type="field" name="locquali"/>
      <column width="-1" hidden="0" type="field" name="loopareano"/>
      <column width="-1" hidden="0" type="field" name="loopstation"/>
      <column width="-1" hidden="0" type="field" name="looptype"/>
      <column width="-1" hidden="0" type="field" name="usechangedate"/>
      <column width="-1" hidden="0" type="field" name="envcen"/>
      <column width="-1" hidden="0" type="field" name="abandcause"/>
      <column width="-1" hidden="0" type="field" name="abandcontr"/>
      <column width="-1" hidden="0" type="field" name="startdayunknown"/>
      <column width="-1" hidden="0" type="field" name="startmnthunknwn"/>
      <column width="-1" hidden="0" type="field" name="wwboreholeno"/>
      <column width="-1" hidden="0" type="field" name="xutm32euref89"/>
      <column width="-1" hidden="0" type="field" name="yutm32euref89"/>
      <column width="-1" hidden="0" type="field" name="zdvr90"/>
      <column width="-1" hidden="0" type="field" name="installation"/>
      <column width="-1" hidden="0" type="field" name="workingconditions"/>
      <column width="-1" hidden="0" type="field" name="approach"/>
      <column width="-1" hidden="0" type="field" name="accessremark"/>
      <column width="-1" hidden="0" type="field" name="locatepersonemail"/>
      <column width="-1" hidden="0" type="field" name="preservationzone"/>
      <column width="-1" hidden="0" type="field" name="protectionzone"/>
      <column width="-1" hidden="0" type="field" name="region"/>
      <column width="-1" hidden="0" type="field" name="usechangecause"/>
      <column width="-1" hidden="0" type="field" name="guid"/>
      <column width="-1" hidden="0" type="field" name="insertdate"/>
      <column width="-1" hidden="0" type="field" name="updatedate"/>
      <column width="-1" hidden="0" type="field" name="insertuser"/>
      <column width="-1" hidden="0" type="field" name="updateuser"/>
      <column width="-1" hidden="0" type="field" name="dataowner"/>
      <column width="-1" hidden="0" type="field" name="url_bor"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="abandcause"/>
    <field editable="1" name="abandcontr"/>
    <field editable="1" name="abandondat"/>
    <field editable="1" name="accessremark"/>
    <field editable="1" name="approach"/>
    <field editable="1" name="boreholeid"/>
    <field editable="1" name="boreholeno"/>
    <field editable="1" name="borhpostc"/>
    <field editable="1" name="borhtownno"/>
    <field editable="1" name="borhtownno2007"/>
    <field editable="1" name="comments"/>
    <field editable="1" name="companytype"/>
    <field editable="1" name="consuborno"/>
    <field editable="1" name="consulogno"/>
    <field editable="1" name="consultant"/>
    <field editable="1" name="countyno"/>
    <field editable="1" name="ctrpdescr"/>
    <field editable="1" name="ctrpeleva"/>
    <field editable="1" name="ctrpheight"/>
    <field editable="1" name="ctrpprecis"/>
    <field editable="1" name="ctrpzprecis"/>
    <field editable="1" name="dataowner"/>
    <field editable="1" name="datum"/>
    <field editable="1" name="drforadres"/>
    <field editable="1" name="drforpostc"/>
    <field editable="1" name="drilendate"/>
    <field editable="1" name="drillborno"/>
    <field editable="1" name="drilldepth"/>
    <field editable="1" name="drilledfor"/>
    <field editable="1" name="driller"/>
    <field editable="1" name="drilllogno"/>
    <field editable="1" name="drilstdate"/>
    <field editable="1" name="elevametho"/>
    <field editable="1" name="elevaquali"/>
    <field editable="1" name="elevasourc"/>
    <field editable="1" name="elevation"/>
    <field editable="1" name="envcen"/>
    <field editable="1" name="grumoareano"/>
    <field editable="1" name="grumoborno"/>
    <field editable="1" name="grumobortype"/>
    <field editable="1" name="grumocountyno"/>
    <field editable="1" name="guid"/>
    <field editable="1" name="houseownas"/>
    <field editable="1" name="id"/>
    <field editable="1" name="insertdate"/>
    <field editable="1" name="insertuser"/>
    <field editable="1" name="installation"/>
    <field editable="1" name="landregno"/>
    <field editable="1" name="latitude"/>
    <field editable="1" name="litholnote"/>
    <field editable="1" name="locatepersonemail"/>
    <field editable="1" name="location"/>
    <field editable="1" name="locatmetho"/>
    <field editable="1" name="locatquali"/>
    <field editable="1" name="locatsourc"/>
    <field editable="1" name="locquali"/>
    <field editable="1" name="longitude"/>
    <field editable="1" name="longtext"/>
    <field editable="1" name="loopareano"/>
    <field editable="1" name="loopstation"/>
    <field editable="1" name="looptype"/>
    <field editable="1" name="mapdistx"/>
    <field editable="1" name="mapdisty"/>
    <field editable="1" name="mapsheet"/>
    <field editable="1" name="municipal"/>
    <field editable="1" name="namingsys"/>
    <field editable="1" name="numofsampl"/>
    <field editable="1" name="numsamsto"/>
    <field editable="1" name="numsuplbor"/>
    <field editable="1" name="plantid"/>
    <field editable="1" name="preservationzone"/>
    <field editable="1" name="prevborhno"/>
    <field editable="1" name="protectionzone"/>
    <field editable="1" name="purpose"/>
    <field editable="1" name="region"/>
    <field editable="1" name="reportedby"/>
    <field editable="1" name="row_id"/>
    <field editable="1" name="samdescdat"/>
    <field editable="1" name="samrecedat"/>
    <field editable="1" name="startdayunknown"/>
    <field editable="1" name="startmnthunknwn"/>
    <field editable="1" name="status"/>
    <field editable="1" name="sys34x"/>
    <field editable="1" name="sys34y"/>
    <field editable="1" name="sys34zone"/>
    <field editable="1" name="togeusdate"/>
    <field editable="1" name="updatedate"/>
    <field editable="1" name="updateuser"/>
    <field editable="1" name="url_bor"/>
    <field editable="1" name="use"/>
    <field editable="1" name="usechangecause"/>
    <field editable="1" name="usechangedate"/>
    <field editable="1" name="utmzone"/>
    <field editable="1" name="various"/>
    <field editable="1" name="verticaref"/>
    <field editable="1" name="workingconditions"/>
    <field editable="1" name="wwboreholeno"/>
    <field editable="1" name="xutm"/>
    <field editable="1" name="xutm32euref89"/>
    <field editable="1" name="yutm"/>
    <field editable="1" name="yutm32euref89"/>
    <field editable="1" name="zdvr90"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="abandcause"/>
    <field labelOnTop="0" name="abandcontr"/>
    <field labelOnTop="0" name="abandondat"/>
    <field labelOnTop="0" name="accessremark"/>
    <field labelOnTop="0" name="approach"/>
    <field labelOnTop="0" name="boreholeid"/>
    <field labelOnTop="0" name="boreholeno"/>
    <field labelOnTop="0" name="borhpostc"/>
    <field labelOnTop="0" name="borhtownno"/>
    <field labelOnTop="0" name="borhtownno2007"/>
    <field labelOnTop="0" name="comments"/>
    <field labelOnTop="0" name="companytype"/>
    <field labelOnTop="0" name="consuborno"/>
    <field labelOnTop="0" name="consulogno"/>
    <field labelOnTop="0" name="consultant"/>
    <field labelOnTop="0" name="countyno"/>
    <field labelOnTop="0" name="ctrpdescr"/>
    <field labelOnTop="0" name="ctrpeleva"/>
    <field labelOnTop="0" name="ctrpheight"/>
    <field labelOnTop="0" name="ctrpprecis"/>
    <field labelOnTop="0" name="ctrpzprecis"/>
    <field labelOnTop="0" name="dataowner"/>
    <field labelOnTop="0" name="datum"/>
    <field labelOnTop="0" name="drforadres"/>
    <field labelOnTop="0" name="drforpostc"/>
    <field labelOnTop="0" name="drilendate"/>
    <field labelOnTop="0" name="drillborno"/>
    <field labelOnTop="0" name="drilldepth"/>
    <field labelOnTop="0" name="drilledfor"/>
    <field labelOnTop="0" name="driller"/>
    <field labelOnTop="0" name="drilllogno"/>
    <field labelOnTop="0" name="drilstdate"/>
    <field labelOnTop="0" name="elevametho"/>
    <field labelOnTop="0" name="elevaquali"/>
    <field labelOnTop="0" name="elevasourc"/>
    <field labelOnTop="0" name="elevation"/>
    <field labelOnTop="0" name="envcen"/>
    <field labelOnTop="0" name="grumoareano"/>
    <field labelOnTop="0" name="grumoborno"/>
    <field labelOnTop="0" name="grumobortype"/>
    <field labelOnTop="0" name="grumocountyno"/>
    <field labelOnTop="0" name="guid"/>
    <field labelOnTop="0" name="houseownas"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="insertdate"/>
    <field labelOnTop="0" name="insertuser"/>
    <field labelOnTop="0" name="installation"/>
    <field labelOnTop="0" name="landregno"/>
    <field labelOnTop="0" name="latitude"/>
    <field labelOnTop="0" name="litholnote"/>
    <field labelOnTop="0" name="locatepersonemail"/>
    <field labelOnTop="0" name="location"/>
    <field labelOnTop="0" name="locatmetho"/>
    <field labelOnTop="0" name="locatquali"/>
    <field labelOnTop="0" name="locatsourc"/>
    <field labelOnTop="0" name="locquali"/>
    <field labelOnTop="0" name="longitude"/>
    <field labelOnTop="0" name="longtext"/>
    <field labelOnTop="0" name="loopareano"/>
    <field labelOnTop="0" name="loopstation"/>
    <field labelOnTop="0" name="looptype"/>
    <field labelOnTop="0" name="mapdistx"/>
    <field labelOnTop="0" name="mapdisty"/>
    <field labelOnTop="0" name="mapsheet"/>
    <field labelOnTop="0" name="municipal"/>
    <field labelOnTop="0" name="namingsys"/>
    <field labelOnTop="0" name="numofsampl"/>
    <field labelOnTop="0" name="numsamsto"/>
    <field labelOnTop="0" name="numsuplbor"/>
    <field labelOnTop="0" name="plantid"/>
    <field labelOnTop="0" name="preservationzone"/>
    <field labelOnTop="0" name="prevborhno"/>
    <field labelOnTop="0" name="protectionzone"/>
    <field labelOnTop="0" name="purpose"/>
    <field labelOnTop="0" name="region"/>
    <field labelOnTop="0" name="reportedby"/>
    <field labelOnTop="0" name="row_id"/>
    <field labelOnTop="0" name="samdescdat"/>
    <field labelOnTop="0" name="samrecedat"/>
    <field labelOnTop="0" name="startdayunknown"/>
    <field labelOnTop="0" name="startmnthunknwn"/>
    <field labelOnTop="0" name="status"/>
    <field labelOnTop="0" name="sys34x"/>
    <field labelOnTop="0" name="sys34y"/>
    <field labelOnTop="0" name="sys34zone"/>
    <field labelOnTop="0" name="togeusdate"/>
    <field labelOnTop="0" name="updatedate"/>
    <field labelOnTop="0" name="updateuser"/>
    <field labelOnTop="0" name="url_bor"/>
    <field labelOnTop="0" name="use"/>
    <field labelOnTop="0" name="usechangecause"/>
    <field labelOnTop="0" name="usechangedate"/>
    <field labelOnTop="0" name="utmzone"/>
    <field labelOnTop="0" name="various"/>
    <field labelOnTop="0" name="verticaref"/>
    <field labelOnTop="0" name="workingconditions"/>
    <field labelOnTop="0" name="wwboreholeno"/>
    <field labelOnTop="0" name="xutm"/>
    <field labelOnTop="0" name="xutm32euref89"/>
    <field labelOnTop="0" name="yutm"/>
    <field labelOnTop="0" name="yutm32euref89"/>
    <field labelOnTop="0" name="zdvr90"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="abandcause"/>
    <field reuseLastValue="0" name="abandcontr"/>
    <field reuseLastValue="0" name="abandondat"/>
    <field reuseLastValue="0" name="accessremark"/>
    <field reuseLastValue="0" name="approach"/>
    <field reuseLastValue="0" name="boreholeid"/>
    <field reuseLastValue="0" name="boreholeno"/>
    <field reuseLastValue="0" name="borhpostc"/>
    <field reuseLastValue="0" name="borhtownno"/>
    <field reuseLastValue="0" name="borhtownno2007"/>
    <field reuseLastValue="0" name="comments"/>
    <field reuseLastValue="0" name="companytype"/>
    <field reuseLastValue="0" name="consuborno"/>
    <field reuseLastValue="0" name="consulogno"/>
    <field reuseLastValue="0" name="consultant"/>
    <field reuseLastValue="0" name="countyno"/>
    <field reuseLastValue="0" name="ctrpdescr"/>
    <field reuseLastValue="0" name="ctrpeleva"/>
    <field reuseLastValue="0" name="ctrpheight"/>
    <field reuseLastValue="0" name="ctrpprecis"/>
    <field reuseLastValue="0" name="ctrpzprecis"/>
    <field reuseLastValue="0" name="dataowner"/>
    <field reuseLastValue="0" name="datum"/>
    <field reuseLastValue="0" name="drforadres"/>
    <field reuseLastValue="0" name="drforpostc"/>
    <field reuseLastValue="0" name="drilendate"/>
    <field reuseLastValue="0" name="drillborno"/>
    <field reuseLastValue="0" name="drilldepth"/>
    <field reuseLastValue="0" name="drilledfor"/>
    <field reuseLastValue="0" name="driller"/>
    <field reuseLastValue="0" name="drilllogno"/>
    <field reuseLastValue="0" name="drilstdate"/>
    <field reuseLastValue="0" name="elevametho"/>
    <field reuseLastValue="0" name="elevaquali"/>
    <field reuseLastValue="0" name="elevasourc"/>
    <field reuseLastValue="0" name="elevation"/>
    <field reuseLastValue="0" name="envcen"/>
    <field reuseLastValue="0" name="grumoareano"/>
    <field reuseLastValue="0" name="grumoborno"/>
    <field reuseLastValue="0" name="grumobortype"/>
    <field reuseLastValue="0" name="grumocountyno"/>
    <field reuseLastValue="0" name="guid"/>
    <field reuseLastValue="0" name="houseownas"/>
    <field reuseLastValue="0" name="id"/>
    <field reuseLastValue="0" name="insertdate"/>
    <field reuseLastValue="0" name="insertuser"/>
    <field reuseLastValue="0" name="installation"/>
    <field reuseLastValue="0" name="landregno"/>
    <field reuseLastValue="0" name="latitude"/>
    <field reuseLastValue="0" name="litholnote"/>
    <field reuseLastValue="0" name="locatepersonemail"/>
    <field reuseLastValue="0" name="location"/>
    <field reuseLastValue="0" name="locatmetho"/>
    <field reuseLastValue="0" name="locatquali"/>
    <field reuseLastValue="0" name="locatsourc"/>
    <field reuseLastValue="0" name="locquali"/>
    <field reuseLastValue="0" name="longitude"/>
    <field reuseLastValue="0" name="longtext"/>
    <field reuseLastValue="0" name="loopareano"/>
    <field reuseLastValue="0" name="loopstation"/>
    <field reuseLastValue="0" name="looptype"/>
    <field reuseLastValue="0" name="mapdistx"/>
    <field reuseLastValue="0" name="mapdisty"/>
    <field reuseLastValue="0" name="mapsheet"/>
    <field reuseLastValue="0" name="municipal"/>
    <field reuseLastValue="0" name="namingsys"/>
    <field reuseLastValue="0" name="numofsampl"/>
    <field reuseLastValue="0" name="numsamsto"/>
    <field reuseLastValue="0" name="numsuplbor"/>
    <field reuseLastValue="0" name="plantid"/>
    <field reuseLastValue="0" name="preservationzone"/>
    <field reuseLastValue="0" name="prevborhno"/>
    <field reuseLastValue="0" name="protectionzone"/>
    <field reuseLastValue="0" name="purpose"/>
    <field reuseLastValue="0" name="region"/>
    <field reuseLastValue="0" name="reportedby"/>
    <field reuseLastValue="0" name="samdescdat"/>
    <field reuseLastValue="0" name="samrecedat"/>
    <field reuseLastValue="0" name="startdayunknown"/>
    <field reuseLastValue="0" name="startmnthunknwn"/>
    <field reuseLastValue="0" name="status"/>
    <field reuseLastValue="0" name="sys34x"/>
    <field reuseLastValue="0" name="sys34y"/>
    <field reuseLastValue="0" name="sys34zone"/>
    <field reuseLastValue="0" name="togeusdate"/>
    <field reuseLastValue="0" name="updatedate"/>
    <field reuseLastValue="0" name="updateuser"/>
    <field reuseLastValue="0" name="url_bor"/>
    <field reuseLastValue="0" name="use"/>
    <field reuseLastValue="0" name="usechangecause"/>
    <field reuseLastValue="0" name="usechangedate"/>
    <field reuseLastValue="0" name="utmzone"/>
    <field reuseLastValue="0" name="various"/>
    <field reuseLastValue="0" name="verticaref"/>
    <field reuseLastValue="0" name="workingconditions"/>
    <field reuseLastValue="0" name="wwboreholeno"/>
    <field reuseLastValue="0" name="xutm"/>
    <field reuseLastValue="0" name="xutm32euref89"/>
    <field reuseLastValue="0" name="yutm"/>
    <field reuseLastValue="0" name="yutm32euref89"/>
    <field reuseLastValue="0" name="zdvr90"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"ctrpdescr"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
