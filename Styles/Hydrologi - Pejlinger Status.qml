<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" simplifyMaxScale="1" simplifyDrawingHints="0" symbologyReferenceScale="-1" simplifyDrawingTol="1" minScale="100000000" readOnly="0" hasScaleBasedVisibilityFlag="0" labelsEnabled="0" maxScale="0" simplifyAlgorithm="0" simplifyLocal="1" version="3.26.3-Buenos Aires">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal limitMode="0" accumulate="0" enabled="0" endField="" startField="" durationField="" durationUnit="min" endExpression="" fixedDuration="0" mode="0" startExpression="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zoffset="0" clamping="Terrain" showMarkerSymbolInSurfacePlots="0" type="IndividualFeatures" binding="Centroid" symbology="Line" extrusionEnabled="0" extrusion="0" respectLayerSymbol="1" zscale="1">
    <data-defined-properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol name="" frame_rate="10" type="line" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" class="SimpleLine" locked="0">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="square"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="114,155,111,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="0.6"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="114,155,111,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.6"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol name="" frame_rate="10" type="fill" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" class="SimpleFill" locked="0">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="color" type="QString" value="114,155,111,255"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="81,111,79,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="style" type="QString" value="solid"/>
          </Option>
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="114,155,111,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="81,111,79,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol name="" frame_rate="10" type="marker" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="114,155,111,255"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="diamond"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="81,111,79,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="3"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="114,155,111,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="diamond"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="81,111,79,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 forceraster="0" referencescale="-1" type="RuleRenderer" symbollevels="1" enableorderby="1">
    <rules key="{10966ace-abce-4ca5-ad15-f2a4e293239e}">
      <rule key="{524ee12d-0773-4363-aad0-6305ad1c3492}" symbol="0" filter=" COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;screen_top&quot; IS NULL AND  &quot;screen_bottom&quot; IS NOT NULL OR  &quot;screen_top&quot; IS NOT NULL AND  &quot;screen_bottom&quot; IS NULL " label="Delvist usikker på *placeringen af *filtersætningen"/>
      <rule key="{29eb5b0f-3fd6-4450-9017-d0aba1dc1e4b}" symbol="1" filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND  &quot;screen_top&quot; IS NULL AND  &quot;screen_bottom&quot; IS NULL" label="Usikker på placeringen *af filtersætningen"/>
      <rule key="{f66239f8-8417-4b05-861c-60467d75216a}" symbol="2" filter="COALESCE( &quot;situation&quot; , 0) != 1 AND  &quot;watlevmsl&quot; IS NOT NULL AND &quot;screen_top&quot; IS NOT NULL AND  &quot;screen_bottom&quot; IS NOT NULL" label="Sikker på placeringen *af filtersætningen"/>
    </rules>
    <symbols>
      <symbol name="0" frame_rate="10" type="marker" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="2" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="255,247,83,255"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="circle"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="2"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="255,247,83,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="1" frame_rate="10" type="marker" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="3" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="227,26,28,255"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="circle"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="2"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="227,26,28,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="2" frame_rate="10" type="marker" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="1" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="73,226,62,255"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="circle"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="2"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="73,226,62,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <orderby>
      <orderByClause asc="1" nullsFirst="0">"antal"</orderByClause>
    </orderby>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option name="dualview/previewExpressions" type="List">
        <Option type="QString" value="&quot;row_id&quot;"/>
      </Option>
      <Option name="embeddedWidgets/count" type="QString" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory labelPlacementMethod="XHeight" backgroundAlpha="255" sizeType="MM" maxScaleDenominator="1e+08" minScaleDenominator="0" penColor="#000000" scaleBasedVisibility="0" scaleDependency="Area" width="15" spacing="5" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" enabled="0" lineSizeType="MM" barWidth="5" showAxis="1" sizeScale="3x:0,0,0,0,0,0" diagramOrientation="Up" rotationOffset="270" height="15" spacingUnit="MM" spacingUnitScale="3x:0,0,0,0,0,0" direction="0" backgroundColor="#ffffff" opacity="1" penAlpha="255" penWidth="0">
      <fontProperties bold="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" underline="0" style="" italic="0"/>
      <attribute colorOpacity="1" field="" label="" color="#000000"/>
      <axisSymbol>
        <symbol name="" frame_rate="10" type="line" is_animated="0" alpha="1" force_rhr="0" clip_to_extent="1">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer enabled="1" pass="0" class="SimpleLine" locked="0">
            <Option type="Map">
              <Option name="align_dash_pattern" type="QString" value="0"/>
              <Option name="capstyle" type="QString" value="square"/>
              <Option name="customdash" type="QString" value="5;2"/>
              <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="customdash_unit" type="QString" value="MM"/>
              <Option name="dash_pattern_offset" type="QString" value="0"/>
              <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
              <Option name="draw_inside_polygon" type="QString" value="0"/>
              <Option name="joinstyle" type="QString" value="bevel"/>
              <Option name="line_color" type="QString" value="35,35,35,255"/>
              <Option name="line_style" type="QString" value="solid"/>
              <Option name="line_width" type="QString" value="0.26"/>
              <Option name="line_width_unit" type="QString" value="MM"/>
              <Option name="offset" type="QString" value="0"/>
              <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="offset_unit" type="QString" value="MM"/>
              <Option name="ring_filter" type="QString" value="0"/>
              <Option name="trim_distance_end" type="QString" value="0"/>
              <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_end_unit" type="QString" value="MM"/>
              <Option name="trim_distance_start" type="QString" value="0"/>
              <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_start_unit" type="QString" value="MM"/>
              <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
              <Option name="use_custom_dash" type="QString" value="0"/>
              <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" obstacle="0" dist="0" placement="0" linePlacementFlags="18" priority="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="rownumber" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="antal_pejlinger" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevelid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boreholeid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="intakeid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boreholeno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevelno" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="intakeno" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="timeofmeas" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="project" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="waterlevel" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevgrsu" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevmsl" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevmp" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="hoursnopum" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="category" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="method" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="quality" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="refpoint" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="remark" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="verticaref" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="atmospresshpa" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="extremes" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="situation" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="watlevelroundno" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="qualitycontrol" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="guid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="guid_intake" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="guid_watlevround" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="insertdate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="updatedate" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="insertuser" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="updateuser" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dataowner" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="locatmetho" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="screen_top" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="screen_bottom" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="rownumber"/>
    <alias index="1" name="" field="antal_pejlinger"/>
    <alias index="2" name="" field="watlevelid"/>
    <alias index="3" name="" field="boreholeid"/>
    <alias index="4" name="" field="intakeid"/>
    <alias index="5" name="" field="boreholeno"/>
    <alias index="6" name="" field="watlevelno"/>
    <alias index="7" name="" field="intakeno"/>
    <alias index="8" name="" field="timeofmeas"/>
    <alias index="9" name="" field="project"/>
    <alias index="10" name="" field="waterlevel"/>
    <alias index="11" name="" field="watlevgrsu"/>
    <alias index="12" name="" field="watlevmsl"/>
    <alias index="13" name="" field="watlevmp"/>
    <alias index="14" name="" field="hoursnopum"/>
    <alias index="15" name="" field="category"/>
    <alias index="16" name="" field="method"/>
    <alias index="17" name="" field="quality"/>
    <alias index="18" name="" field="refpoint"/>
    <alias index="19" name="" field="remark"/>
    <alias index="20" name="" field="verticaref"/>
    <alias index="21" name="" field="atmospresshpa"/>
    <alias index="22" name="" field="extremes"/>
    <alias index="23" name="" field="situation"/>
    <alias index="24" name="" field="watlevelroundno"/>
    <alias index="25" name="" field="qualitycontrol"/>
    <alias index="26" name="" field="guid"/>
    <alias index="27" name="" field="guid_intake"/>
    <alias index="28" name="" field="guid_watlevround"/>
    <alias index="29" name="" field="insertdate"/>
    <alias index="30" name="" field="updatedate"/>
    <alias index="31" name="" field="insertuser"/>
    <alias index="32" name="" field="updateuser"/>
    <alias index="33" name="" field="dataowner"/>
    <alias index="34" name="" field="locatmetho"/>
    <alias index="35" name="" field="screen_top"/>
    <alias index="36" name="" field="screen_bottom"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" field="rownumber" expression=""/>
    <default applyOnUpdate="0" field="antal_pejlinger" expression=""/>
    <default applyOnUpdate="0" field="watlevelid" expression=""/>
    <default applyOnUpdate="0" field="boreholeid" expression=""/>
    <default applyOnUpdate="0" field="intakeid" expression=""/>
    <default applyOnUpdate="0" field="boreholeno" expression=""/>
    <default applyOnUpdate="0" field="watlevelno" expression=""/>
    <default applyOnUpdate="0" field="intakeno" expression=""/>
    <default applyOnUpdate="0" field="timeofmeas" expression=""/>
    <default applyOnUpdate="0" field="project" expression=""/>
    <default applyOnUpdate="0" field="waterlevel" expression=""/>
    <default applyOnUpdate="0" field="watlevgrsu" expression=""/>
    <default applyOnUpdate="0" field="watlevmsl" expression=""/>
    <default applyOnUpdate="0" field="watlevmp" expression=""/>
    <default applyOnUpdate="0" field="hoursnopum" expression=""/>
    <default applyOnUpdate="0" field="category" expression=""/>
    <default applyOnUpdate="0" field="method" expression=""/>
    <default applyOnUpdate="0" field="quality" expression=""/>
    <default applyOnUpdate="0" field="refpoint" expression=""/>
    <default applyOnUpdate="0" field="remark" expression=""/>
    <default applyOnUpdate="0" field="verticaref" expression=""/>
    <default applyOnUpdate="0" field="atmospresshpa" expression=""/>
    <default applyOnUpdate="0" field="extremes" expression=""/>
    <default applyOnUpdate="0" field="situation" expression=""/>
    <default applyOnUpdate="0" field="watlevelroundno" expression=""/>
    <default applyOnUpdate="0" field="qualitycontrol" expression=""/>
    <default applyOnUpdate="0" field="guid" expression=""/>
    <default applyOnUpdate="0" field="guid_intake" expression=""/>
    <default applyOnUpdate="0" field="guid_watlevround" expression=""/>
    <default applyOnUpdate="0" field="insertdate" expression=""/>
    <default applyOnUpdate="0" field="updatedate" expression=""/>
    <default applyOnUpdate="0" field="insertuser" expression=""/>
    <default applyOnUpdate="0" field="updateuser" expression=""/>
    <default applyOnUpdate="0" field="dataowner" expression=""/>
    <default applyOnUpdate="0" field="locatmetho" expression=""/>
    <default applyOnUpdate="0" field="screen_top" expression=""/>
    <default applyOnUpdate="0" field="screen_bottom" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" field="rownumber" constraints="3" exp_strength="0" notnull_strength="1"/>
    <constraint unique_strength="0" field="antal_pejlinger" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="watlevelid" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="boreholeid" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="intakeid" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="boreholeno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="watlevelno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="intakeno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="timeofmeas" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="project" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="waterlevel" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="watlevgrsu" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="watlevmsl" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="watlevmp" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="hoursnopum" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="category" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="method" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="quality" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="refpoint" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="remark" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="verticaref" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="atmospresshpa" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="extremes" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="situation" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="watlevelroundno" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="qualitycontrol" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="1" field="guid" constraints="3" exp_strength="0" notnull_strength="1"/>
    <constraint unique_strength="0" field="guid_intake" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="guid_watlevround" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="insertdate" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="updatedate" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="insertuser" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="updateuser" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="dataowner" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="locatmetho" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="screen_top" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="screen_bottom" constraints="0" exp_strength="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="rownumber"/>
    <constraint desc="" exp="" field="antal_pejlinger"/>
    <constraint desc="" exp="" field="watlevelid"/>
    <constraint desc="" exp="" field="boreholeid"/>
    <constraint desc="" exp="" field="intakeid"/>
    <constraint desc="" exp="" field="boreholeno"/>
    <constraint desc="" exp="" field="watlevelno"/>
    <constraint desc="" exp="" field="intakeno"/>
    <constraint desc="" exp="" field="timeofmeas"/>
    <constraint desc="" exp="" field="project"/>
    <constraint desc="" exp="" field="waterlevel"/>
    <constraint desc="" exp="" field="watlevgrsu"/>
    <constraint desc="" exp="" field="watlevmsl"/>
    <constraint desc="" exp="" field="watlevmp"/>
    <constraint desc="" exp="" field="hoursnopum"/>
    <constraint desc="" exp="" field="category"/>
    <constraint desc="" exp="" field="method"/>
    <constraint desc="" exp="" field="quality"/>
    <constraint desc="" exp="" field="refpoint"/>
    <constraint desc="" exp="" field="remark"/>
    <constraint desc="" exp="" field="verticaref"/>
    <constraint desc="" exp="" field="atmospresshpa"/>
    <constraint desc="" exp="" field="extremes"/>
    <constraint desc="" exp="" field="situation"/>
    <constraint desc="" exp="" field="watlevelroundno"/>
    <constraint desc="" exp="" field="qualitycontrol"/>
    <constraint desc="" exp="" field="guid"/>
    <constraint desc="" exp="" field="guid_intake"/>
    <constraint desc="" exp="" field="guid_watlevround"/>
    <constraint desc="" exp="" field="insertdate"/>
    <constraint desc="" exp="" field="updatedate"/>
    <constraint desc="" exp="" field="insertuser"/>
    <constraint desc="" exp="" field="updateuser"/>
    <constraint desc="" exp="" field="dataowner"/>
    <constraint desc="" exp="" field="locatmetho"/>
    <constraint desc="" exp="" field="screen_top"/>
    <constraint desc="" exp="" field="screen_bottom"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;screen_topbotquali&quot;" sortOrder="1" actionWidgetStyle="dropDown">
    <columns>
      <column name="boreholeno" width="-1" hidden="0" type="field"/>
      <column name="intakeno" width="-1" hidden="0" type="field"/>
      <column name="rownumber" width="-1" hidden="0" type="field"/>
      <column name="antal_pejlinger" width="-1" hidden="0" type="field"/>
      <column name="watlevelid" width="-1" hidden="0" type="field"/>
      <column name="boreholeid" width="-1" hidden="0" type="field"/>
      <column name="intakeid" width="-1" hidden="0" type="field"/>
      <column name="watlevelno" width="-1" hidden="0" type="field"/>
      <column name="timeofmeas" width="-1" hidden="0" type="field"/>
      <column name="project" width="-1" hidden="0" type="field"/>
      <column name="waterlevel" width="-1" hidden="0" type="field"/>
      <column name="watlevgrsu" width="-1" hidden="0" type="field"/>
      <column name="watlevmsl" width="-1" hidden="0" type="field"/>
      <column name="watlevmp" width="-1" hidden="0" type="field"/>
      <column name="hoursnopum" width="-1" hidden="0" type="field"/>
      <column name="category" width="-1" hidden="0" type="field"/>
      <column name="method" width="-1" hidden="0" type="field"/>
      <column name="quality" width="-1" hidden="0" type="field"/>
      <column name="refpoint" width="-1" hidden="0" type="field"/>
      <column name="remark" width="-1" hidden="0" type="field"/>
      <column name="verticaref" width="-1" hidden="0" type="field"/>
      <column name="atmospresshpa" width="-1" hidden="0" type="field"/>
      <column name="extremes" width="-1" hidden="0" type="field"/>
      <column name="situation" width="-1" hidden="0" type="field"/>
      <column name="watlevelroundno" width="-1" hidden="0" type="field"/>
      <column name="qualitycontrol" width="-1" hidden="0" type="field"/>
      <column name="guid" width="-1" hidden="0" type="field"/>
      <column name="guid_intake" width="-1" hidden="0" type="field"/>
      <column name="guid_watlevround" width="-1" hidden="0" type="field"/>
      <column name="insertdate" width="-1" hidden="0" type="field"/>
      <column name="updatedate" width="-1" hidden="0" type="field"/>
      <column name="insertuser" width="-1" hidden="0" type="field"/>
      <column name="updateuser" width="-1" hidden="0" type="field"/>
      <column name="dataowner" width="-1" hidden="0" type="field"/>
      <column name="locatmetho" width="-1" hidden="0" type="field"/>
      <column name="screen_top" width="-1" hidden="0" type="field"/>
      <column name="screen_bottom" width="-1" hidden="0" type="field"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="antal" editable="1"/>
    <field name="antal_pejl" editable="1"/>
    <field name="antal_pejlinger" editable="1"/>
    <field name="atmospresshpa" editable="1"/>
    <field name="borehole_1" editable="1"/>
    <field name="borehole_locatmetho" editable="0"/>
    <field name="boreholeid" editable="1"/>
    <field name="boreholeno" editable="1"/>
    <field name="category" editable="1"/>
    <field name="dataowner" editable="1"/>
    <field name="extremes" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="guid" editable="1"/>
    <field name="guid_intake" editable="1"/>
    <field name="guid_watlevround" editable="1"/>
    <field name="hoursnopum" editable="1"/>
    <field name="insertdate" editable="1"/>
    <field name="insertuser" editable="1"/>
    <field name="intakeid" editable="1"/>
    <field name="intakeno" editable="1"/>
    <field name="intakeno_2" editable="1"/>
    <field name="locatmetho" editable="1"/>
    <field name="method" editable="1"/>
    <field name="pejledato" editable="1"/>
    <field name="pejledato_" editable="1"/>
    <field name="project" editable="1"/>
    <field name="quality" editable="1"/>
    <field name="qualitycontrol" editable="1"/>
    <field name="refpoint" editable="1"/>
    <field name="remark" editable="1"/>
    <field name="row_id" editable="1"/>
    <field name="row_id_2" editable="1"/>
    <field name="rownumber" editable="1"/>
    <field name="screen_bottom" editable="0"/>
    <field name="screen_top" editable="0"/>
    <field name="screen_topbotquali" editable="0"/>
    <field name="situation" editable="1"/>
    <field name="timeofmeas" editable="1"/>
    <field name="updatedate" editable="1"/>
    <field name="updateuser" editable="1"/>
    <field name="vandst_m_1" editable="1"/>
    <field name="vandst_mut" editable="1"/>
    <field name="vandstko_1" editable="1"/>
    <field name="vandstkote" editable="1"/>
    <field name="verticaref" editable="1"/>
    <field name="waterlevel" editable="1"/>
    <field name="watlevel_situation" editable="0"/>
    <field name="watlevel_timeofmeas" editable="0"/>
    <field name="watlevel_watlevmsl" editable="0"/>
    <field name="watlevelid" editable="1"/>
    <field name="watlevelno" editable="1"/>
    <field name="watlevelroundno" editable="1"/>
    <field name="watlevgrsu" editable="1"/>
    <field name="watlevmp" editable="1"/>
    <field name="watlevmsl" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="antal" labelOnTop="0"/>
    <field name="antal_pejl" labelOnTop="0"/>
    <field name="antal_pejlinger" labelOnTop="0"/>
    <field name="atmospresshpa" labelOnTop="0"/>
    <field name="borehole_1" labelOnTop="0"/>
    <field name="borehole_locatmetho" labelOnTop="0"/>
    <field name="boreholeid" labelOnTop="0"/>
    <field name="boreholeno" labelOnTop="0"/>
    <field name="category" labelOnTop="0"/>
    <field name="dataowner" labelOnTop="0"/>
    <field name="extremes" labelOnTop="0"/>
    <field name="fid" labelOnTop="0"/>
    <field name="guid" labelOnTop="0"/>
    <field name="guid_intake" labelOnTop="0"/>
    <field name="guid_watlevround" labelOnTop="0"/>
    <field name="hoursnopum" labelOnTop="0"/>
    <field name="insertdate" labelOnTop="0"/>
    <field name="insertuser" labelOnTop="0"/>
    <field name="intakeid" labelOnTop="0"/>
    <field name="intakeno" labelOnTop="0"/>
    <field name="intakeno_2" labelOnTop="0"/>
    <field name="locatmetho" labelOnTop="0"/>
    <field name="method" labelOnTop="0"/>
    <field name="pejledato" labelOnTop="0"/>
    <field name="pejledato_" labelOnTop="0"/>
    <field name="project" labelOnTop="0"/>
    <field name="quality" labelOnTop="0"/>
    <field name="qualitycontrol" labelOnTop="0"/>
    <field name="refpoint" labelOnTop="0"/>
    <field name="remark" labelOnTop="0"/>
    <field name="row_id" labelOnTop="0"/>
    <field name="row_id_2" labelOnTop="0"/>
    <field name="rownumber" labelOnTop="0"/>
    <field name="screen_bottom" labelOnTop="0"/>
    <field name="screen_top" labelOnTop="0"/>
    <field name="screen_topbotquali" labelOnTop="0"/>
    <field name="situation" labelOnTop="0"/>
    <field name="timeofmeas" labelOnTop="0"/>
    <field name="updatedate" labelOnTop="0"/>
    <field name="updateuser" labelOnTop="0"/>
    <field name="vandst_m_1" labelOnTop="0"/>
    <field name="vandst_mut" labelOnTop="0"/>
    <field name="vandstko_1" labelOnTop="0"/>
    <field name="vandstkote" labelOnTop="0"/>
    <field name="verticaref" labelOnTop="0"/>
    <field name="waterlevel" labelOnTop="0"/>
    <field name="watlevel_situation" labelOnTop="0"/>
    <field name="watlevel_timeofmeas" labelOnTop="0"/>
    <field name="watlevel_watlevmsl" labelOnTop="0"/>
    <field name="watlevelid" labelOnTop="0"/>
    <field name="watlevelno" labelOnTop="0"/>
    <field name="watlevelroundno" labelOnTop="0"/>
    <field name="watlevgrsu" labelOnTop="0"/>
    <field name="watlevmp" labelOnTop="0"/>
    <field name="watlevmsl" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="antal" reuseLastValue="0"/>
    <field name="antal_pejlinger" reuseLastValue="0"/>
    <field name="atmospresshpa" reuseLastValue="0"/>
    <field name="borehole_locatmetho" reuseLastValue="0"/>
    <field name="boreholeid" reuseLastValue="0"/>
    <field name="boreholeno" reuseLastValue="0"/>
    <field name="category" reuseLastValue="0"/>
    <field name="dataowner" reuseLastValue="0"/>
    <field name="extremes" reuseLastValue="0"/>
    <field name="fid" reuseLastValue="0"/>
    <field name="guid" reuseLastValue="0"/>
    <field name="guid_intake" reuseLastValue="0"/>
    <field name="guid_watlevround" reuseLastValue="0"/>
    <field name="hoursnopum" reuseLastValue="0"/>
    <field name="insertdate" reuseLastValue="0"/>
    <field name="insertuser" reuseLastValue="0"/>
    <field name="intakeid" reuseLastValue="0"/>
    <field name="intakeno" reuseLastValue="0"/>
    <field name="locatmetho" reuseLastValue="0"/>
    <field name="method" reuseLastValue="0"/>
    <field name="project" reuseLastValue="0"/>
    <field name="quality" reuseLastValue="0"/>
    <field name="qualitycontrol" reuseLastValue="0"/>
    <field name="refpoint" reuseLastValue="0"/>
    <field name="remark" reuseLastValue="0"/>
    <field name="row_id" reuseLastValue="0"/>
    <field name="rownumber" reuseLastValue="0"/>
    <field name="screen_bottom" reuseLastValue="0"/>
    <field name="screen_top" reuseLastValue="0"/>
    <field name="screen_topbotquali" reuseLastValue="0"/>
    <field name="situation" reuseLastValue="0"/>
    <field name="timeofmeas" reuseLastValue="0"/>
    <field name="updatedate" reuseLastValue="0"/>
    <field name="updateuser" reuseLastValue="0"/>
    <field name="verticaref" reuseLastValue="0"/>
    <field name="waterlevel" reuseLastValue="0"/>
    <field name="watlevel_situation" reuseLastValue="0"/>
    <field name="watlevel_timeofmeas" reuseLastValue="0"/>
    <field name="watlevel_watlevmsl" reuseLastValue="0"/>
    <field name="watlevelid" reuseLastValue="0"/>
    <field name="watlevelno" reuseLastValue="0"/>
    <field name="watlevelroundno" reuseLastValue="0"/>
    <field name="watlevgrsu" reuseLastValue="0"/>
    <field name="watlevmp" reuseLastValue="0"/>
    <field name="watlevmsl" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"row_id"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
